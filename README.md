# aotra

### Project name
«aotra»

## Abstract (English)
«aotra» (-or «bubble-CMS» as unofficial name- written «aotra» or «0-CMS», all characters in lower case, and pronounced «Aotra-Code» because «Aotra» means «Zero» in Malagasy,
also stands for «Animation-Oriented Thoughts Repository Aotra» because recursive acronyms makes it really look more geeky),
or also «Animated & Organizable Thin Repository Aotra» or «Aire Organisable de Travail et de Réflexion Aotra» in French. 
is a yet another CMS (ie. a tool for creating websites) that respects these principles (inherited from the «décroissance informatique» philosophy) : see below.

## Résumé (French)
«aotra» est un CMS proposant une nouvelle présentation des interfaces web, basé sur des unités de navigation appelées «bulles» disposées 
sur un canvas infini. Ce CMS essaie d'implanter une approche graphique et heuristique dans la construction de site, proche des topogrammes, 
le tout en réduisant le nombre de fichiers nécessaires au déploiement d'un site web, afin d'offrir une expérience utilisateur radicalement nouvelle et ludique.

## Details
«aotra» is a full stack of components : (each one are designated under a recursive acronym, because reasons).
Front-end :
- aotracodex : stands for aotra-CODEX for Organization, Diagrams, Edition, and eXtensibility : a full software featuring an infinite canvas on a single page to provide a topogrammatic interactive virtual surface. 
- aotrautils : stands for AOTRA Utility Tools In-Line Source
- andrana (formerly aotratest) : (means «trial, try» in malagasy) stands for aotra-TESTs Executions and Statistics Tool : an embedded and simple yet complete unit and integration tests framework.
- rajakotest : (means «monkey» in malagasy) stands for Randomly Automated Javascript aotra Kong-Oriented TESTs : a plugin for the aotratest tool that generates random user tests and allows to create very rapidly an automated tests base
- lalao : means «game» in malagasy) basic gaming utilities
- lakile :  (means «key» in malagasy) encryption provider & management
Back-end :
- aotrapetra : stands for aotra-PETRA External Treatments Running Asynchronously : a set of features making a link between client-side and server side, seamlessly for the developper and much more reactive for the user.
- aotrasome: stands for aotra-SOME ORM Made Easy : A very simple ORM framework, embedding a full clear jquery syntax-inspired API and embedded features like objects versionning.
Each component is facultative, starting from aotracodex (excluded) to aotrasome.
Each component inherits the licence of aotra.

## Project goals
- First : Providing a convenient tool for inner understanding (ie. psychological self-analysis).
- Second : Providing an innovative tool for creating new-paradigm websites.
- Third : Promoting the Humanitary License, and especially awaring people about Burmese people situation.

## MAIN «DécInfor» PRINCIPLES
- If an information resource is no use in learning anything about you nor about the universe then do not use it
- Information quality is a resource, whereas information quantity is a waste
- Information tools must serve humans, never humans must serve tools (the core «décroissance informatique» principle)
- Ask yourself why you want to use an information system before doing it (related to first principle)
- If something is fun to do and allow you to discover something on the universe or on you, then just go for it !
(more details at https://alqemia.com/#bubble6)

## Project principles (derived from «DécInfor» principles above)
-Infinite canvas
-User is active in the navigation
-Images prefered over text in content
-Graphical elements over text in interfaces
-Use UI to build site instead of code
-Smooth animations
-Zoom exploitation thus no responsive design needed
-Lightweight engine
-Content limited strictly to minimum
-Minimal infrastructure (no database server nor heavy multi-layered application server or application container) required for persistence or publishing
-Content over container
-Easily understandable, customizable open-source code versus oversecurity obscure proprietary code
-No users nor roles, just editor/viewer roles separated by passwords
-Embed as much as possible in page (code and media), to allow single-file portability
-Plugins-based and open architecture, to allow further features additions

## About

## Project licensing : (Dual license)
-Front-end part : using HGPL license, an open-source and free software, everybody is allowed to use, copy and modify code and sites made only with the front-end part.
-Back-en part : using HLGPL aotra backend stack license, an open-source non-free commercial software, if you want to be allowed to use convenient advanced back-end code and customization features, commercial agreement must be concluded first.
(Please contact admin@alqemia.com for commercial information.)

#Project licensing details
HGPL License : aotracodex, aotratest, aotrautils (front-end part, javascript code)
HLGPL Commercial license : aotrapetra, aotrasome (back-en part, PHP code) (contact admin@alqemia.com for details)

## Project license text
see doc/HGPL_LICENSE.txt file

## Author name
Jérémie Ratomposon (massively helped by his native country free education system)
## Author email
info@alqemia.com
## Organization name
Alqemia
## Organization email
info@alqemia.com
## Organization website
https://alqemia.com

## Version explanation
Example : 215_13012013-0939
first digit is major version : 2
others digits group before "_" is minor version : .15
others digits group after "_" is last build timestamp according to date format "ddMMyyyy-hhmm": 13 jan. 2013 at 9h39 AM 

## Language notice
This project is coded and documented in English mostly, only for worldwide understanding reasons, however author is Frensh-speaking, so there may be mistakes, that I encourage you to report to the main project's email. Thanks for your understanding.

## Libraries used

## Embedded other internal JS libraries
aotrautils.build.js											https://alqemia.com/						(H:HBP-GPL)	: utilities and convenience methods
	-sub-components : 
			* andrana   : unit and integration testing framework
			* lalao			:	basic gaming utilities
			* lakile 		:	encryption provider & management
multiplicity (aotra plugin)				https://alqemia.com/						(H:HBP-GPL)	: sim-city like game

## Compilation / deployment scripts external executables libraries :
nodejs					https://nodejs.org/en/											(MIT-License)	: pure javascript runtime environnement
uglify-js				https://github.com/mishoo/UglifyJS2/blob/master/README.md	(BSD License)	: javascript minifying tool

## Embedded external JS libraries
prototype.js						http://prototypejs.org/							(MIT-License)	: object programming in javascript
//UNUSED md5.js (hashing)								http://pajhome.org.uk/crypt/md5/			(BSD License)	: md5 hashing
sjcl.js (hashing) 		https://bitwiseshiftleft.github.io/sjcl/				GNU GPL, version 2.0
jquery.js							http://jquery.com/										(MIT-License)	: advanced DOM management
jquery.cookie.js					http://plugins.jquery.com/cookie/	(MIT-License)	: cookies using
jquery.caret.js					http://plugins.jquery.com/caret/		(BSD License)	: caret handling
//nicEdit.js							http://nicedit.com/									(MIT-License)	: text editor
	('-> Maybe switch to more modern «scribe» editor ? https://www.npmjs.com/package/scribe-editor)
Google Maps API v3 Library		https://developers.google.com/maps/		(Google terms of service for Google Maps API v3)
detectmobilebrowser.js			http://detectmobilebrowsers.com/			(Public domain : Unlicense)
raphaeljs.js						http://raphaeljs.com/								(MIT-License)	: svg graphics and animation
raphaeljs icons set				http://raphaeljs.com/icons/				(MIT-License)	: nice UI icons
//(Excluded for now :)
//TODO : find another speech library handling French
//mespeak.js							http://www.masswerk.at/mespeak/			(GNU GPL)		: speech synthesis
annyang.js							https://www.talater.com/annyang/			(MIT-License) : vocal recognition
	//UNUSED : FIXME : library jquery-ui is unusable since it has compiling errors in browsers...:
	//jquery-ui.js					http://jqueryui.com/									(JQuery License ~ MIT License)
	//UNUSED :
	//html5-qrcode.js				http://dwa012.github.io/html5-qrcode/	(MIT-License)
## Called external libraries or services
html5shiv							http://html5shiv.googlecode.com/			(GPL v2)			: IE and legacy browser compatibility
Google Maps API v3 Service		https://developers.google.com/maps/		(Google terms of service for Google Maps API v3)
Google Analytics Service		http://www.google-analytics.com/				(Google terms of service for Google Analytics)
	//NOT USED ANYMORE (because I came to the conclusion that it makes no sense to make the whole code heavier for all just to repair a just one non-standard browser that is MSIE) :
	//excanvas.js					http://excanvas.sourceforge.net/		(Apache License v2.0)
socket.io							http://socket.io											(MIT-License)
roundsliderui					http://roundsliderui.com							(MIT-License)

***

## aotra minimal html file

<!DOCTYPE html>
<!--writePasswordsHashesSalted:-->
<!--viewPasswordsHashesSalted:-->
<html>
    <head>
        <script src="https://alqemia.com/aotra.min.js" charset="UTF-8" type="text/javascript" data-keep="true"></script>
    </head>
    <body>
        <div id="aotraScreenDiv" data-keep="true"></div>
        <script data-keep="true">aotraScreenJSFooterInit();</script>
        <script id="xmlModel" type="application/xml" data-keep="true"><aotraScreen></aotraScreen></script>
    </body>
</html>


(Written partly while listening Dante Bucci hangdrum performances)

***

## DEVELOPMENT NOTICES : (non migrated, pre-backlog todos)
###		aotra
#### 	BUGS TO FIX (in French):
1) Dans l'historique de l'advanced navigation provider, il faut prendre en compte les bulles effacées/renommées !
3) Le drag ne fonctionne pas sur les fenêtres utilitaires.
4) Corriger le problème restant de ratio de background quand on prend une valeur qui est la width ou la height

## 	TODOS (in French):
### Prioritary
1) Dans la mini-map, faire un hover ou une selection (pour les mobiles) afficher dans une infobulle un aperçu de la bulle voulue...
2) Dans le buttons links provider : -
	-Afficher un aperçu des bubbles quand on hover sur les boutons de liens (y compris les anchors dans le contenu !)
	
### Non-prioritary
01) PLUS TARD, car pas encore implémenté dans tous les navigateurs...: Utiliser les CSS shapes pour le texte des bulles
02) Pouvoir faire en sorte que le background d'arrière-bubble puisse ètre configurable de par sa portion d'image utilisée pour chaque bulle...
04) Lié au bug 1) : rajouter un mode de aotraScreen navigationMode="mobile" ! ("desktop" par défaut...)
05) Avec le simpleBackground=webcam d'un plane, linker le move avec le delta Image si on met moveAnimation=webcam (Webcam dynamique mais navigation traditionnelle sinon) ... :
06) Ajout d'une option pour uploader et setter facilement l'image de background interne d'une bubble
07) Ajout d'une option pour uploader et setter facilement un son au double-clic (?) d'une bubble ?
08) Ajout d'un rendu personnalisable  dans le champ de recherche pour trouver des bubbles en cherchant dans : le nom, le content, les tags 
09) Ajout de couleurs prédéfinies dans un menu drop-down dans l'édition d'une bubble.
10) Cascade delete des bubbles et de leurs sub-bubbles après une confirmation + Possibilité de détacher une bubble de ses ascendants et descendants
11) Thumbnail des bubbles (avant <content>)
13) Animations de Links dans les links molds
14) Layout de Plane pour ses bubbles (spiral, grid...)
15) Drag and drop pour le déplacement des Bubbles
16) Embed des jscripts : javascriptFunctions des bubbles (après le <content>)
17) Ajouter une classe "includeContent" et data-src dans les content des bubbles pour embedder du contenu distant en lazy quand la bulle arrive dans le champ de vision. 
21) Dans les bubbles, pouvoir indiquer une color de background qui soient des dégradés simples
22) Pouvoir gérer les conflits quand on édite en même temps à partir de plusieurs ordinateurs différents
23) Quand on arrive sur une bulle, mettre à jour l'anchor dans l'URL pour pouvoir "bookmarquer l'URL" une bubble
24) En mode édition, quand on clique sur du vide, cela doit déselectionner la bulle sélectionnée.
25) Pouvoir spécifier des bordures de bubbles spéciales (diapos, fuzzy, animés, etc...)
26) En mode édition : annuler / redo et historique des actions d'édition

###		aotratest
// 	TODOS (in French):
#### Prioritary
1) Pouvoir spyer les fonctions (quand elles sont exécutées et quand elles renvoient des erreurs)

#### Rajako module
// Tout.

####		multiplicity
// Tout.

####		tsypetra
// Tout.





require("./aotrautils.build.js");

class Model {
	
	constructor(objects=[]){
		this.objects=objects;
	}
	
	
	// ***
	/*OVERRIDES*/getSize(){
		return getArraySize(this.objects);
	}
	
	// ***
	/*OVERRIDES*/getAllObjectsIds(){
		const ids=[];
		foreach(this.objects, obj=>{ids.push(obj.id);}, (obj)=>!!obj.id);
		return ids;
	}
		
	// ***
	/*OVERRIDES*/getObjectsIdsWithinBoundaries(boundaries=null){
		const objects=this.getObjectsWithinBoundaries(boundaries);
		return this.getObjectsIds(objects);
	}
	// ***
	/*OVERRIDES*/getObjectsIds(objects){
		const ids=[];
		foreach(objects, obj=>{ids.push(obj.id);});
		return ids;
	}
	// ***
	/*OVERRIDES*/getObjectsWithinBoundaries(boundaries=null){
		const objects=[];
		foreach(this.objects, obj=>{objects.push(obj);}, (obj)=>(!boundaries || (boundaries.min<=obj.index && obj.index<=boundaries.max)));
		return objects;
	}
	
	// ***
	/*OVERRIDES*/split(){
		const middleIndex=Math.ceil(getArraySize(this.objects)/2);
		const firstHalf=this.objects.slice(0, middleIndex);
		const secondHalf=this.objects.slice(middleIndex);
		this.objects=firstHalf;
		return new Model(secondHalf);
	}
	
	// ***
	/*OVERRIDES*/replaceBy(otherModel){
		this.objects=otherModel.objects;
	}

	// ***
	/*OVERRIDES*/clientMergeWith(otherModel){
		this.objects.push(...otherModel.objects);
	}
	

	// ***
	/*OVERRIDES*/clientUpdateObjects(otherObjects){
		foreach(this.objects, object=>{
			foreach(otherObjects, otherObj=>{
				object.label=otherObj.label;
			},(otherObj)=>(otherObj.id===object.id));
		});
	}
	
	/*OVERRIDES*/clientAddObjects(newObjects){
		this.objects.push([...newObjects]);
	}
	

	createObjects(){
		const newObjects=[{id:"object20",index:20},{id:"object21",index:21},];
		return newObjects;
	}
	
}
// ***
window.Model=Model;





class Controller{
	
	constructor(model){
		this.model=model;
	}
	
	// ***
	/*OVERRIDES*/setNode(aortacNode){
		this.aortacNode=aortacNode;
	}

	// ***
	/*OVERRIDES*/getNode(){
		return this.aortacNode;
	}
	
	// ***
	/*OVERRIDES*/interpretInputs(inputs, subBoundaries){
		const modifiedObjects=[];
		
		// We check if this inputs changes concerns our objects :
		const modelObjects=this.model.getObjectsWithinBoundaries(subBoundaries);
		if(empty(modelObjects))	return modifiedObjects;
		
		if(inputs.label==="XXXX"){
			// We modify the model's objects of this node here : 
			foreach(modelObjects, obj=>{
				obj.label=inputs.label;
				modifiedObjects.push(obj);
			});
			const modifiedObjectsIds=this.model.getObjectsIds(modifiedObjects);
			return this.getNode().sendUpdatedObjects(modifiedObjects, modifiedObjectsIds);
		}else if(inputs.label==="YYYY"){
			
			const newObjects=this.model.createObjects();

			// We modify the model's objects of this node  here :
			this.model.clientAddObjects(newObjects);

			const newObjectsIds=this.model.getObjectsIds(newObjects);
			return this.getNode().sendNewObjects(newObjects, newObjectsIds);
		}

		return modifiedObjects;	
	}
	

}




class View{
	
	constructor(model, boundaries){
		this.model=model;
		this.boundaries=boundaries;
	}
	
	// ***
	/*OVERRIDES*/getBoundaries(){
		return this.boundaries;
	}

	// ***
	/*OVERRIDES*/setClient(aortacClient){
		this.aortacClient=aortacClient;
	}

	// ***
	/*OVERRIDES*/getClient(){
		return this.aortacClient;
	}

	// ***
	/*OVERRIDES*/sendInputs(inputs, subBoundaries=null){
		if(!this.aortacClient)	return;
		this.getClient().sendInputs(inputs, subBoundaries);
	}

	// ***
	/*OVERRIDES*/display(){
		// DEBUG
		lognow(" VIEW DISPLAYS MODEL : ",this.model);
	}
	
}


(async ()=>{
	
	// SERVERS
	const model0=new Model([{id:"object1",index:1},{id:"object2",index:2},{id:"object3",index:3},{id:"object4",index:4},{id:"object5",index:5},
												  {id:"object6",index:6},{id:"object7",index:7},{id:"object8",index:8},{id:"object9",index:9},{id:"object10",index:10}]);
	const controller0=new Controller(model0);
	const node0=getAORTACNode("node0", "ws://127.0.0.1:40010", [], model0, controller0);
	controller0.setNode(node0);
	node0.start();
	
	await sleep(5000);

	const model1=new Model([]);
	const controller1=new Controller(model1);
	const node1=getAORTACNode("node1", "ws://127.0.0.1:40000", ["ws://127.0.0.1:40010"], model1, controller1);
	controller1.setNode(node1);
	node1.start();
	
	await sleep(5000);
	
	const model2=new Model([]);
	const controller2=new Controller(model2);
	const node2=getAORTACNode("node2", "ws://127.0.0.1:40001", ["ws://127.0.0.1:40010","ws://127.0.0.1:40000"], model2, controller2);
	controller2.setNode(node2);
	node2.start();

	await sleep(5000);

	const model3=new Model([]);
	const controller3=new Controller(model3);
	const node3=getAORTACNode("node3", "ws://127.0.0.1:40002", ["ws://127.0.0.1:40010","ws://127.0.0.1:40000","ws://127.0.0.1:40001"], model3, controller3);
	controller3.setNode(node3);
	node3.start();
	
	
	node0.connect();
	node1.connect();
	node2.connect();
	node3.connect();

	

	// CLIENTS
	await sleep(8000);
	const modelClient0=new Model([]);
	const viewClient0=new View(modelClient0, {min:2,max:6});
	const client0=getAORTACClient("client0", "ws://127.0.0.1:40010", modelClient0, viewClient0);
	viewClient0.setClient(client0);
	client0.start();

	await sleep(5000);
	viewClient0.sendInputs({label:"XXXX"},{min:3,max:5});
	await sleep(5000);
	viewClient0.sendInputs({label:"YYYY"},);
	
	
	await sleep(8000);
	const modelClient1=new Model([]);
	const viewClient1=new View(modelClient1, {min:1,max:2});
	const client1=getAORTACClient("client1", "ws://127.0.0.1:40001", modelClient1, viewClient1);
	viewClient1.setClient(client1);
	client1.start();


	
	
	// TRACE
	await sleep(2000);
	viewClient0.display();
	await sleep(2000);
	viewClient1.display();
	
	


	// TRACE
	await sleep(10000);
	node0.traceNode();
	node1.traceNode();
	node2.traceNode();
	node3.traceNode();
	
	
	
})();
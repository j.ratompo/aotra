# Aotrautils library server-side

***

The aotrautils-srv library to be used in plain javascript or nodejs projects. Provides useful functions like foreach, the aotest automated testing framework, and the flat map to json and json to flap map converter utils.
Plus some useful server-side exclusive functions and methods.

## Project license text
see doc/HGPL_LICENSE.txt file
## Author name
Jérémie Ratomposon (massively helped by his native country free education system)
## Author email
info@alqemia.com
## Organization name
Alqemia
## Organization email
info@alqemia.com
## Organization website
https://alqemia.com

## Installation
  `npm install --save aotrautils-srv`

## Usage
  `require("aotrautils-srv");`


## Contributing
At maintainer's discretion.

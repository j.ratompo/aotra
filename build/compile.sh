



# IN ORDER TO MINIFYING TO WORK, «node» or «nodejs» EXECUTABLE MUST BE INSTALLED !
isMinifyed="false"
isUnstable="false"

# (Excluded for now.)
isSpeechSynthesisExcluded=true


isCompilationServerSide=false

if [ "$1" = "min" ]; then
	isMinifyed="true"
else
	if [ "$1" = "unstable" ];then
		isUnstable="true"
		
	else
		if [ "$1" = "srv" ];then
			isCompilationServerSide=true
		fi
		
	fi
fi

isUglifyjsInstalled="true";
isNodejsInstalled="true";
command -v npm >/dev/null 2>&1 || { echo "Script requires command «node» (from «nodejs» executable module) but it’s not installed. Aborting. Please run «apt-get install nodejs» as root user in order to fix it." >&2; isNodejsInstalled="false";}
if [ "$isNodejsInstalled" = "true" ]; then
	command -v uglifyjs >/dev/null 2>&1 || { echo "Script requires command «uglifyjs» (from «uglify-js» executable module) but it’s not installed. Aborting. Please run «npm install uglify-js -g» as root user in order to fix it." >&2; isUglifyjsInstalled="false";}
fi
#*********************************************

#relative to compile script location :
LICENSE_FILE="../doc/HGPL_LICENSE.txt"
VERSION_FILE_LOCATION="../doc/VERSION.txt"
README_FILE_LOCATION="../README.md"
SRC_DIR="src"
OUTPUT_NAME="aotra"
JS_ROOT_DIR="../$SRC_DIR/$OUTPUT_NAME"

#relative to JS root dir (JS_ROOT_DIR):
OUTPUT_JS_FILE_PATH="../../build"
OUTPUT_JS_FILE_NAME="$OUTPUT_NAME.js"
if [ "$isUnstable" = "true" ];then
	OUTPUT_JS_FILE_NAME="$OUTPUT_NAME.unstable.js"
fi
OUTPUT_MINIFIED_JS_FILE_NAME="$OUTPUT_NAME.min.js"

# Utils library names config :
UTILS_COMMONS_NAME="aotrautils.commons"
UTILS_COMMONS_FILE_NAME="$UTILS_COMMONS_NAME.js"
# OLD : UTILS_COMMONS_FILE_NAME_OUTPUT="$UTILS_COMMONS_NAME.build.js"

	# CLIENT LIBRARIES :

UTILS_CLIENT_NAME="aotrautils.commons.client"
UTILS_CLIENT_FILE_NAME="$UTILS_CLIENT_NAME.js"
# OLD : UTILS_CLIENT_FILE_NAME_OUTPUT="$UTILS_CLIENT_NAME.build.js"

UTILS_GEOMETRY_NAME="aotrautils.commons.geometry"
UTILS_GEOMETRY_FILE_NAME="$UTILS_GEOMETRY_NAME.js"


	# SERVER LIBRARIES :

UTILS_AI_NAME="aotrautils.commons.ai"
UTILS_AI_FILE_NAME="$UTILS_AI_NAME.js"


UTILS_SERVER_NAME="aotrautils.commons.server"
UTILS_SERVER_FILE_NAME="$UTILS_SERVER_NAME.js"


	# To be merged into :
	UTILS_NAME="aotrautils"
	
	if [ $isCompilationServerSide = true ];then
		UTILS_NAME="$UTILS_NAME-srv"
	fi
	
	UTILS_FILE_NAME_OUTPUT="$UTILS_NAME.build.js"
	UTILS_FILE_NODE_MODULE_DIR="$UTILS_NAME"


	

UTIL_LIBRARIES_DIR="util"
COMMON_LIBRARIES_DIR="SHARED"

##OUTPUT_JS_FILE_WITH_SPEECH_NAME="$OUTPUT_NAME-speech.js"
##LOCAL_DEPLOYING_PATH="../../$SRC_DIR/html/"
JS_EXTERNAL_LIBS_ROOT_DIR="lib"
JS_EXTERNAL_LIBS_UTILS_ROOT_DIR="libutils"


########################################################################################

echo "Compiling to file «$OUTPUT_JS_FILE_NAME»..."
echo "" >  $OUTPUT_JS_FILE_NAME
##echo "/*BUILD WITHOUT SPEECH LIBRARY*/" >>  $OUTPUT_JS_FILE_NAME
echo "" >>  $OUTPUT_JS_FILE_NAME

# VERSION ADDING :
echo "	- adding version to file..."
compilationDate=$(date +%d/%m/%Y-%H:%M:%S)
VERSION_HEADER="/* JS File compiled on date «$compilationDate» with new version : "

echo "$VERSION_HEADER" >> $OUTPUT_JS_FILE_NAME
cat $VERSION_FILE_LOCATION >> $OUTPUT_JS_FILE_NAME
echo "*/" >> $OUTPUT_JS_FILE_NAME
echo "" >> $OUTPUT_JS_FILE_NAME

# TODO : 
CALCULATED_VERSION="$(cat $VERSION_FILE_LOCATION) ($compilationDate)"

# EVENTUAL UNSTABLE NOTICE CONTENT ADDING :
UNSTABLE_NOTICE="CAUTION : this code is cutting-edge developments, but is reputed UNSTABLE, and should really not be used for other than testing and/or studying purposes. You can use it, but at your OWN RISK."

if [ "$isUnstable" = "true" ];then
	echo "	- adding  UNSTABLE notice as header to file..."
	echo ""
	echo "/* $UNSTABLE_NOTICE */" >>  $OUTPUT_JS_FILE_NAME
	echo "" >> $OUTPUT_JS_FILE_NAME
	
fi


# README CONTENT ADDING :


echo "	- adding readme to file..."
echo "/* README Information : " >> $OUTPUT_JS_FILE_NAME
cat $README_FILE_LOCATION >> $OUTPUT_JS_FILE_NAME
echo "*/" >> $OUTPUT_JS_FILE_NAME
echo "" >> $OUTPUT_JS_FILE_NAME
echo "" >> $OUTPUT_JS_FILE_NAME


#### WITH SPEECH FEATURES :
##echo "Compiling to file «$OUTPUT_JS_FILE_WITH_SPEECH_NAME»..."
##echo "" >  $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "/*BUILD INCLUDING SPEECH LIBRARY*/" >>  $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "" >>  $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##
### VERSION ADDING :
##echo "	- adding version to file..."
##compilationDate=$(date +%d/%m/%Y-%H:%M:%S)
##echo "/* JS File compiled on date «$compilationDate» with version : " >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##cat $VERSION_FILE_LOCATION >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "*/" >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "" >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##
### README CONTENT ADDING :
##echo "	- adding readme to file..."
##echo "/* README Information : " >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##cat $README_FILE_LOCATION >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "*/" >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "" >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME
##echo "" >> $OUTPUT_JS_FILE_WITH_SPEECH_NAME

########################################################################################

# COMPILATIONS :
cd $JS_ROOT_DIR

echo "Normal compilation..."
compiledFilePath="$OUTPUT_JS_FILE_PATH/$OUTPUT_JS_FILE_NAME"

echo "" >	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT

if [ $isCompilationServerSide = false ];then
	# CLIENT LIBRARIES :
	echo "	MERGING $UTILS_CLIENT_FILE_NAME + $UTILS_SERVER_FILE_NAME + $UTILS_COMMONS_FILE_NAME INTO $UTILS_FILE_NAME_OUTPUT"
else
	# SERVER LIBRARIES :
	echo "	EXCLUDING $UTILS_CLIENT_FILE_NAME && MERGING $UTILS_SERVER_FILE_NAME + $UTILS_COMMONS_FILE_NAME INTO $UTILS_FILE_NAME_OUTPUT"
fi


echo "	- 0.0) Concatenating common (=server and client) COMMONS utils library into merged file and version adding «$CALCULATED_VERSION»"
echo "" >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*utils COMMONS library associated with $OUTPUT_NAME version : «$CALCULATED_VERSION»*/" >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*-----------------------------------------------------------------------------*/" 			>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
cat $COMMON_LIBRARIES_DIR/$UTILS_COMMONS_FILE_NAME >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT



if [ $isCompilationServerSide = false ];then

# CLIENT LIBRARIES (always EXCLUDED from common file !) :
	
	echo "	- 0.0.1) Concatenating common CLIENT utils library into merged file and version adding «$CALCULATED_VERSION»"
	echo "" >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
	echo "/*utils CLIENT library associated with $OUTPUT_NAME version : «$CALCULATED_VERSION»*/" 	>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
	echo "/*-----------------------------------------------------------------------------*/" 			>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
	cat $COMMON_LIBRARIES_DIR/$UTILS_CLIENT_FILE_NAME >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT

	
	echo "	- 0.0.2) Concatenating common (=shared) GEOMETRY utils library into merged file and version adding «$CALCULATED_VERSION»"
	echo "" >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
	echo "/*utils GEOMETRY library associated with $OUTPUT_NAME version : «$CALCULATED_VERSION»*/">>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
	echo "/*-----------------------------------------------------------------------------*/" 			>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
	cat $COMMON_LIBRARIES_DIR/$UTILS_GEOMETRY_FILE_NAME >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT

fi

# SERVER LIBRARIES (always ADDED to common file !) :

echo "	- 0.1) Concatenating common AI utils library into merged file and version adding «$CALCULATED_VERSION»"
echo "" >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*utils AI library associated with $OUTPUT_NAME version : «$CALCULATED_VERSION»*/"		>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*-----------------------------------------------------------------------------*/" 		>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
cat $COMMON_LIBRARIES_DIR/$UTILS_AI_FILE_NAME >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT


echo "	- 0.2) Concatenating common SERVER utils library into merged file and version adding «$CALCULATED_VERSION»"
echo "" >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*utils SERVER library associated with $OUTPUT_NAME version : «$CALCULATED_VERSION»*/">>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*-----------------------------------------------------------------------------*/" 		>>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
cat $COMMON_LIBRARIES_DIR/$UTILS_SERVER_FILE_NAME >>	$OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT







echo "	- 1) compiling all JS files from «lib» in file name order, EXCEPT «measpeak» LIBRARY ..."
echo ""
echo "/*1) Files : " >>  $compiledFilePath
if [ $isSpeechSynthesisExcluded = true ]
then
	find -name "*.js" | grep "/$JS_EXTERNAL_LIBS_ROOT_DIR/" | grep -v ".*mespeak.*" | sort | xargs echo >>  $compiledFilePath
else
	find -name "*.js" | grep "/$JS_EXTERNAL_LIBS_ROOT_DIR/" | sort | xargs echo >>  $compiledFilePath
fi
echo "*/" >>  $compiledFilePath


echo "	- 1.2) compiling all JS files from «$JS_EXTERNAL_LIBS_ROOT_DIR»..."

echo ""
if [ $isSpeechSynthesisExcluded = true ]
then
	echo "Excluding speech features in library..."
	find -name "*.js" | grep "/$JS_EXTERNAL_LIBS_ROOT_DIR/" | grep -v ".*mespeak.*" | sort | xargs cat -s >>  $compiledFilePath
else
	echo "Including speech features in library..."
	find -name "*.js" | grep "/$JS_EXTERNAL_LIBS_ROOT_DIR/" | sort | xargs cat -s >>  $compiledFilePath
fi
echo ""



# (to avoid a SJCL client-side inclusion bug:)
echo "1.3) compiling all JS files from «$JS_EXTERNAL_LIBS_UTILS_ROOT_DIR»..."
echo ""
echo "Including speech features in library..."
echo "/* INCLUDED EXTERNAL LIBRAIRIES" >>  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "*/" >>  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
find -name "*.js" | grep "/$JS_EXTERNAL_LIBS_UTILS_ROOT_DIR/" | sort | xargs cat -s >>  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "/*END OF INCLUDED EXTERNAL LIBRAIRIES*/" >>  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo "" >>  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT
echo ""



echo "(copying aotrautils library to the node packaging directory)"
cp -f $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NAME_OUTPUT  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NODE_MODULE_DIR/

echo "(copying aotra license file library to the node packaging directory)"
cp -f $OUTPUT_JS_FILE_PATH/$LICENSE_FILE  $OUTPUT_JS_FILE_PATH/$UTILS_FILE_NODE_MODULE_DIR/

echo "	- 2.1) compiling all JS files from «$UTIL_LIBRARIES_DIR»..."
echo ""
echo "/*2) Files : " >>  $compiledFilePath
find -name "*.js" | grep "/$UTIL_LIBRARIES_DIR/" | sort | xargs echo >>  $compiledFilePath
echo "*/" >>  $compiledFilePath
echo ""
find -name "*.js" | grep "/$UTIL_LIBRARIES_DIR/" | sort | xargs cat -s >>  $compiledFilePath
echo ""


echo "	- 2.2) compiling all JS files from «$COMMON_LIBRARIES_DIR»..."
echo ""
echo "/*2) Files : " >>  $compiledFilePath
find -name "*.js" | grep "/$COMMON_LIBRARIES_DIR/" | sort | xargs echo >>  $compiledFilePath
echo "*/" >>  $compiledFilePath
echo ""
find -name "*.js" | grep "/$COMMON_LIBRARIES_DIR/" | sort | xargs cat -s >>  $compiledFilePath
echo ""


echo "	- 3) compiling all JS files from «classes»..."
echo ""
echo "/*3) Files : " >>  $compiledFilePath
find -name "*.js" | grep "/classes/" | sort | xargs echo >>  $compiledFilePath
echo "*/" >>  $compiledFilePath
echo ""
find -name "*.js" | grep "/classes/" | sort | xargs cat -s >>  $compiledFilePath
echo ""

echo "	- 4) compiling all JS files from «aotra.clientplugins»..."
echo ""
echo "/*4) Files : " >>  $compiledFilePath
find -name "*.js" | grep "/aotra.clientplugins/" | sort | xargs echo >>  $compiledFilePath
echo "*/" >>  $compiledFilePath
echo ""
find -name "*.js" | grep "/aotra.clientplugins/" | sort | xargs cat -s >>  $compiledFilePath
echo ""


##echo "Compilation with speech library..."
##compiledFilePathWithSpeech="$OUTPUT_JS_FILE_PATH/$OUTPUT_JS_FILE_WITH_SPEECH_NAME"
##
##echo "	- 1) compiling all JS files from «lib» in file name order..."
##find -name "*.js" | grep "/$JS_EXTERNAL_LIBS_ROOT_DIR/" | sort | xargs cat -s >>  $compiledFilePathWithSpeech
##
##echo "	- 2.1) compiling all JS files from «$UTIL_LIBRARIES_DIR»..."
##find -name "*.js" | grep "/$UTIL_LIBRARIES_DIR/" | sort | xargs cat -s >>  $compiledFilePathWithSpeech
##
##echo "	- 2.2) compiling all JS files from «$COMMON_LIBRARIES_DIR»..."
##find -name "*.js" | grep "/$COMMON_LIBRARIES_DIR/" | sort | xargs cat -s >>  $compiledFilePathWithSpeech
##
##echo "	- 3) compiling all JS files from «classes»..."
##find -name "*.js" | grep "/classes/" | sort | xargs cat -s >>  $compiledFilePathWithSpeech


# Adding an eventual javascript code «UNSTABLE» watermark
if [ "$isUnstable" = "true" ];then
	echo ";;log('UNSTABLE code');;" >>  $compiledFilePath
fi


########################################################################################

## lib directory does no longer exist :

## # RESOURCE FILES ADDING
## echo "	- deploying all resource files for JS scripts from «lib»..."
## cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.t*xt $OUTPUT_JS_FILE_PATH/
## cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.c*nf $OUTPUT_JS_FILE_PATH/
## cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.xml $OUTPUT_JS_FILE_PATH/
## cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.j*g $OUTPUT_JS_FILE_PATH/
## cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.png $OUTPUT_JS_FILE_PATH/
## cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.gif $OUTPUT_JS_FILE_PATH/


########################################################################################

##// (DÉACTIVATED FOR NOW :)
##// #LOCAL DEPLOYING (OPTIONAL) :
##// echo "LOCAL DEPLOYING (OPTIONAL)."
##// cp $compiledFilePath $LOCAL_DEPLOYING_PATH/
##// cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.t*xt $LOCAL_DEPLOYING_PATH/
##// cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.c*nf $LOCAL_DEPLOYING_PATH/
##// cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.xml $LOCAL_DEPLOYING_PATH/
##// cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.j*g $LOCAL_DEPLOYING_PATH/
##// cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.png $LOCAL_DEPLOYING_PATH/
##// cp $JS_EXTERNAL_LIBS_ROOT_DIR/*.gif $LOCAL_DEPLOYING_PATH/

##cp $compiledFilePathWithSpeech $LOCAL_DEPLOYING_PATH/

########################################################################################


if [ $isMinifyed = "true" ]; then
	if [ "$isUglifyjsInstalled" = "true" ];then
		echo "Minifying compilation..."
		compiledMinifiedFilePath="$OUTPUT_JS_FILE_PATH/$OUTPUT_MINIFIED_JS_FILE_NAME"
#	OLD:	uglifyjs -c --mangle --screw-ie8 $compiledFilePath -o $compiledMinifiedFilePath
			  uglifyjs -c --mangle $compiledFilePath -o $compiledMinifiedFilePath
	fi
else
	echo "MINIFYING IS DEACTIVATED."
fi	
if [ $isUnstable = "true" ]; then
	echo "UNSTABLE IS ACTIVATED."
fi
echo ""


########################################################################################

echo "Done."
exit 0

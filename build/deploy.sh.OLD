
# ARGUMENTS
# (If ssh login is present, then it is a REMOTE version : it is DEFAULT BEHAVIOR)
SSH_LOGIN="$1"
isUnstable="false"
#BACKUPS_SERVER="ubuservone"
WEB_SERVER="alqemia.com"

options="$2"

# (Minifying is deactivated by default)
isSource="true"
if [ "$options" = "min" ]; then
	isSource="false"
fi
if [ "$options" = "unstable" ]; then
	isUnstable="true"
fi

compilationParameters="min"
if [ "$isSource" = "true" ]; then
	compilationParameters=""
fi

if [ "$isUnstable" = "true" ]; then
	compilationParameters="unstable"
fi


# USAGE
echo "Usage :" 
echo "	sh deploy.sh --> for local versions"
echo "	sh deploy.sh <ssh login prefix in the form of «username@» WITH NO SERVER NAME > --> for REMOTE versions"
echo "	sh deploy.sh <ssh login prefix ...> unstable --> for UNSTABLE, LATEST REMOTE versions"
echo "	sh deploy.sh <ssh login prefix ...> source --> for SOURCE CODE (ie. non-minified) versions"
echo ""
echo "	(DEPENDENCY : This script uses compile script «compile.sh»)"
echo ""


# PREREQUISITE (= SCRIPT DEPENDENCY)
sh compile.sh $compilationParameters


# SCRIPT VARIABLES AND CONSTANTS
LOCAL_BACKUPS_DIR="_OLD"
PROJECT_NAME="aotra"
#REMOTE_BACKUPS_DIR="~/BACKUPS/$PROJECT_NAMEBACKS"
WEBROOT_HOME="/home/user/sites/"
REMOTE_PUBLIC_WEB_HOME="$WEBROOT_HOME/public/"
JS_SCRIPTS_DIRECTORY_NAME="src.$PROJECT_NAME"
PACKAGE_PROJECT_NAME="aotra"
VERSION_FILE_PATH="../doc"
SRC_DIR="WebRoot"
COMPILED_LIBRARIES="$PROJECT_NAME"
COMPILED_LIBRARIES_JS_FILE="$COMPILED_LIBRARIES.js"
if [ "$isUnstable" = "true" ]; then
	COMPILED_LIBRARIES_JS_FILE="$PROJECT_NAME.unstable.js"
fi
COMPILED_MINIFIED_JS_FILE_NAME="$COMPILED_LIBRARIES.min.js"
ENDPOINT_FILENAME="$COMPILED_LIBRARIES.php"
ENDPOINT_SERVERPLUGINS_DIRECTORY="$COMPILED_LIBRARIES.serverplugins"

# OLD : UTILS_CLIENT_LIBRARIES="aotrautils.commons.client"
# OLD : UTILS_CLIENT_LIBRARIES_FILE_OUTPUT="$UTILS_CLIENT_LIBRARIES.build.js"
# OLD : UTILS_COMMONS_LIBRARIES="aotrautils.commons"
# OLD : UTILS_COMMONS_LIBRARIES_FILE_OUTPUT="$UTILS_COMMONS_LIBRARIES.build.js"

UTILS_LIBRARIES="aotrautils.client"
UTILS_LIBRARIES_FILE_OUTPUT="$UTILS_LIBRARIES.build.js"

## UNUSED (but keep code...)
##COMPILED_LIBRARIES_JS_FILE_OTHER="$PROJECT_NAME-speech.js"
##ICONS_FILE="$PROJECT_NAME.gif"


########################################################################################

# START OF SCRIPT
echo "Deploy started at $(date +%d/%m/%Y-%H:%M:%S)..."

# STEP 1 : version file update
oldVersion=$( cat $VERSION_FILE_PATH/VERSION.txt )
oldVersion=$( expr match "$oldVersion" "\([0-9]*_\)" | sed -e "s/_//g" )
version=$(($oldVersion + 1))

timeStamp=$(date +%d%m%Y-%H%M)
echo "1-Version file Update : $version""_""$timeStamp";

echo "$version""_""$timeStamp" > $VERSION_FILE_PATH/VERSION.txt 

########################################################################################

# STEP 2 : files backups
echo "2-Source code backup for version : '$version'";

# -STEP 2.0 : tar file creation
archiveFileName=$(echo $PACKAGE_PROJECT_NAME)_$version.tar.bz2

# In case of local version :
if [ -z "$SSH_LOGIN" ]
then
	echo "(Version is local version)"
	archiveFileName=$(echo $PACKAGE_PROJECT_NAME)_$version-local.tar.bz2
else
	echo "(VERSION IS REMOTE VERSION) : $SSH_LOGIN"
fi

echo "archiveFileName:$archiveFileName"


echo "	2.0-Archive file creation : '$archiveFileName'";
tar --exclude="../../$PACKAGE_PROJECT_NAME/.git" -cjf ../../$archiveFileName ../../$PACKAGE_PROJECT_NAME 

# -STEP 2.1 : files backups to local storage
echo "	2.1-Files backup to local storage in directory : '$LOCAL_BACKUPS_DIR'";
cp ../../$archiveFileName ../../$LOCAL_BACKUPS_DIR

# In case of REMOTE VERSION :
if [ $SSH_LOGIN ]
then

	## DEACTIVATED :
	# -STEP 2.1.1 : files backups to remote server
	#echo "		2.1.1-Files backup to remote server using SSH login : '$SSH_LOGIN$BACKUPS_SERVER'";
	#scp ../../$archiveFileName $SSH_LOGIN$BACKUPS_SERVER:$REMOTE_BACKUPS_DIR/

	# -STEP 2.1.2 : libraries publishing to remote server
	echo "			2.1.2-JS Libraries publishing to remote server using SSH login : '$SSH_LOGIN$WEB_SERVER'";
	echo "			2.1.2.1-Adjusting local permissions:";
	chmod 777 -R ../../$PACKAGE_PROJECT_NAME/$SRC_DIR/$JS_SCRIPTS_DIRECTORY_NAME

	echo "			2.1.2.3-Copying to remote server:";
	scp -r ../../$PACKAGE_PROJECT_NAME/$SRC_DIR/$JS_SCRIPTS_DIRECTORY_NAME $SSH_LOGIN$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
	scp $VERSION_FILE_PATH/VERSION.txt $SSH_LOGIN$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$JS_SCRIPTS_DIRECTORY_NAME

	# -STEP 2.2.1 : Compiled library publishing to remote server
	echo "			2.2.1.1-Compiling libraries into one single file : '$COMPILED_LIBRARIES_JS_FILE'";
	echo "			2.2.1.2-JS compiled library publishing to remote server using SSH login : '$SSH_LOGIN$WEB_SERVER'";
	scp $COMPILED_LIBRARIES_JS_FILE $SSH_LOGIN$WEB_SERVER:$WEBROOT_HOME/
	##scp $COMPILED_LIBRARIES_JS_FILE_OTHER $SSH_LOGIN$WEB_SERVER:$WEBROOT_HOME/

	if [ "$isSource" != "true" ]; then
		echo "			2.2.1.2.1-JS compiled MINIFIED library publishing to remote server using SSH login : '$SSH_LOGIN$WEB_SERVER'";
		scp $COMPILED_MINIFIED_JS_FILE_NAME $SSH_LOGIN$WEB_SERVER:$WEBROOT_HOME/
	fi

	echo "			2.2.1.3-JS utils (CLIENT and COMMONS) library public publishing to remote server using SSH login : '$SSH_LOGIN$WEB_SERVER'";
# OLD:	scp $UTILS_CLIENT_LIBRARIES_FILE_OUTPUT $SSH_LOGIN$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
# OLD:	scp $UTILS_COMMONS_LIBRARIES_FILE_OUTPUT $SSH_LOGIN$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
	scp $UTILS_LIBRARIES_FILE_OUTPUT $SSH_LOGIN$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/


	echo "			2.2.1.4-JS aotra source public publishing to remote server using SSH login : '$SSH_LOGIN$WEB_SERVER'";
	scp $COMPILED_LIBRARIES_JS_FILE $SSH_LOGIN$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
	
	
##	echo "			2.2.1.2-All icons file publishing to remote server using SSH login : '$SSH_LOGIN$WEB_SERVER'";
##	scp $ICONS_FILE $SSH_LOGIN$WEB_SERVER:$WEBROOT_HOME/
	
fi


########################################################################################

# -STEP 4 : push du fichier d'end-point et de ses extensions

echo "4.1-MAJ du fichier d'end-point «$ENDPOINT_FILENAME» : ";
scp ../../$PACKAGE_PROJECT_NAME/$SRC_DIR/html/$ENDPOINT_FILENAME $SSH_LOGIN$WEB_SERVER:$WEBROOT_HOME/$ENDPOINT_FILENAME


echo "4.2-MAJ des extensions du fichier d'end-point «$ENDPOINT_FILENAME» : ";
scp ../../$PACKAGE_PROJECT_NAME/$SRC_DIR/html/$ENDPOINT_SERVERPLUGINS_DIRECTORY/* $SSH_LOGIN$WEB_SERVER:$WEBROOT_HOME/$ENDPOINT_SERVERPLUGINS_DIRECTORY/



########################################################################################

# -STEP 5 : cleaning
oldArchiveFileName=$(echo $PACKAGE_PROJECT_NAME)_$oldVersion.tar.bz2
# Tests if there is no regular file beforehand (https://tldp.org/LDP/abs/html/fto.html) :
if [ ! -f "../../$oldArchiveFileName" ] 
then
	echo "old version was local."
	oldArchiveFileName=$(echo $PACKAGE_PROJECT_NAME)_$oldVersion-local.tar.bz2
else
	echo "OLD VERSION WAS REMOTE."
fi

echo "5-Clean : removing '$oldArchiveFileName'";
rm ../../$oldArchiveFileName

########################################################################################

# END OF SCRIPT
echo "...Deploy finished at $(date +%d/%m/%Y-%H:%M:%S)"
exit 0


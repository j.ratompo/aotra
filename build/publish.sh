



isCompilationServerSide=false

if [ "$1" = "srv" ];then
	isCompilationServerSide=true
fi


packageName="aotrautils"
if [ $isCompilationServerSide = true ];then
	packageName="$packageName-srv"
fi


echo "Starting publishing $packageName..."

cd "$packageName"
npm version patch
cd ..

tar -czvf "$packageName.tar.gz" "$packageName"
npm publish "$packageName.tar.gz"

echo "Publishing done."


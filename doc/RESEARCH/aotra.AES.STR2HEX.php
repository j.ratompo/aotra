<?php
// Server salt key :
// This key must be the same that was used to generate the hashes to stamp in the html file.
// Basically it's the ONLY THING you're STRONGLY RECOMMANDED TO CHANGE in this file.
// It can be whatever UTF-8 supported characters string you want, in a size between 8 up to 128 characters almost.
// =========================================================================================
// $SALT_KEY="aslhajkasd97yas87d6as786asghdIUGAUIY789asHJKHKJh128123907uifhdjkhsKJHjkh88732jdh";
// Must be exactly 64 characters (that leaves 64 for the password):
$SALT_KEY="aslhajkasd97yas87d6as786asghdIUGAUIY789asHJKHKJh128123907uif2J5Y";
// =========================================================================================

// Information :
/*
 * # Description
 * A simple back-end file to achieve persistence on an aotra providing server.
 *
 * # 3 server configuration Prerequisites :
 * ( 0 : We assume that a simple php5+ server is properly installed, configured and running.)
 * 1) File system access rights : Be sure that your PHP web server whatever it is has the right to write to aotra files location.
 * 2) Apache configuration :
 * For a proper functioning of this PHP server component, please set the following parameters in your
 * «/etc/php5/apache2/php.ini» file :
 * file_uploads = On
 * upload_max_filesize = 100M
 * post_max_size = 200M
 * allow_url_fopen On
 * (disclaimer : Although it should not be critical, we do not guarantee the results of this script running on an environment NOT configured like above will be consistent with what expected.)
 * 3) Apache configuration for SECURITY : (it is of UTMOST IMPORTANCE to make your server understand that it MUST NOT EXECUTE php files under *_html/ directories !)
 * «/etc/apache2/apache2.conf» (+ if not already, activate the apache «alias» module with following command : «sudo a2enmod alias;sudo service apache2 restart;» )
 * RedirectMatch 403 "^(.*)_html/(.*)\.php"
 * (Note that any PHP server will do, but we provide you the configuration above for an Apache2 PHP server. Please adapt your server configuration in consequence.)
 * 4) Install MCRYPT : (openssl DOES NOT WORK.)
 * sudo apt-get install php5-mcrypt ; sudo php5enmod mcrypt
 *
 * # About
 * -Project name : «aotra»
 * -Project license : LHGPL(Help Burma-Humanitary LGPL) (see aotra README information for details : https://alqemia.com/aotra.js )
 * -Author name : Jérémie Ratomposon massively helped by its programmating egos legions
 * -Author email : info@alqemia.com
 * -Organization name : Projet Alambic
 * -Organization email : admin@alqemia.com
 * -Organization website : https://alqemia.com
 *
 * #POSSIBLE INPUT PARAMETERS :
 * actionType (possible values :
 * "WRITE","UPLOAD",
 * "GENERATEPASSWORDHASH","DECRYPT","WRITEENCRYPTED",
 * "MAKEMEASANDWICH", "SUDOMAKEMEASANDWICH",
 * "PING"
 * ; all other values will be ignored and throw a 0104 error)
 * pageName
 * writePasswordHash
 * viewPasswordHash
 * data
 * otherServerURL
 * saltKey
 * clearTextPassword
 */

// aotra server-side configuration :

// Configuration for this script. You should'nt have to modify these values...
// This amount of seconds is the time user has to wait between each write/view password validation requests.
// (It only exists to make brute force attackers a little more annoyed...)
// It also can be a little annoying for user, because he won't be able to save during this freeze time,
// but you must understand that if we reset freeze time to zero when password is correct, then
// an attacker only has to brute force until the freeze time is reset instantly (meaning he/she found the right password),
// thus making this freeze time totally ineffective. So please set this value to a non-zero seconds amount at your discretion.
// (10 or 5 seconds are good values)
// DBG
$FREEZE_SECONDS=1;
// $FREEZE_SECONDS=10;

// The log filename :
$LOG_FILE_NAME="aotra.log";

// $CIPHER_NAME="AES-256-CBC";
$CIPHER_NAME=MCRYPT_RIJNDAEL_128;

// These directories are the name of the directories where to store uploaded files according to their types :
// (MUST MATCH CLIENT-SIDE UPLOADING FILES STORING CONTRACT)
$IMAGES_TYPE_DIRECTORY="images";
$VIDEOS_TYPE_DIRECTORY="videos";
$AUDIOS_TYPE_DIRECTORY="audios";
$OTHERS_TYPE_DIRECTORY="";

// These are the characters separating password hashes :
$ACCOUNTS_SEPARATOR=",";
$ACCOUNT_DEFAULT_NAME="username";
$PASSWORDS_SEPARATOR="=";

// These are all the message coming from the server in response of requests :
$MESSAGES=array();
// #POSSIBLE AJAX OUTPUT CODES :

// - SPECIAL CODE :
$MESSAGES["0000"]="0000 DO NOTHING:Nothing was done.";

// - SUCCESS codes :
$MESSAGES["0001"]="0001 SUCCESS:Data were correctly written to file.";
$MESSAGES["0002"]="0002 SUCCESS:Files were correctly uploaded.";
// Easter egg #1 :
$MESSAGES["0003"]="0003 SUCCESS:Okayyy.";
// Presence check
$MESSAGES["0004"]="0004 SUCCESS:Back-end file here.";

// - 01XX Errors are errors of missing or incorrect arguments :
$MESSAGES["0100"]="0100 ERROR:No parameters received.";
$MESSAGES["0101"]="0101 ERROR:Server expected HTML file page name but none received.";
$MESSAGES["0102"]="0102 ERROR:Server expected data content name but none received.";
$MESSAGES["0103"]="0103 ERROR:Server expected writePasswordHash but none received.";
$MESSAGES["0104"]="0104 ERROR:Server did not understand action type.";
$MESSAGES["0105"]="0105 ERROR:Server expected an action type but none received.";
$MESSAGES["0106"]="0106 ERROR:Server expected a salt key but none received.";
$MESSAGES["0107"]="0107 ERROR:Server expected a clear-text password but none received.";
$MESSAGES["0108"]="0108 ERROR:Server expected viewPasswordHash but none received.";
$MESSAGES["0109"]="0109 ERROR:Server could not find encrypted aotraScreen content.";
$MESSAGES["0110"]="0110 ERROR:Password is too long. (64 characters max)";

// - 02XX Errors are errors of normal treatment failure :
$MESSAGES["0201"]="0201 ERROR:Write password is incorrect.";
$MESSAGES["0202"]="0202 ERROR:View password is incorrect.";
$MESSAGES["0203"]="0203 ERROR:Referer URL is not coming from localhost.";
$MESSAGES["0204"]="0204 ERROR:Write password freeze time hasn't passed yet (".$FREEZE_SECONDS." seconds).";
$MESSAGES["0205"]="0205 ERROR:View password freeze time hasn't passed yet (".$FREEZE_SECONDS." seconds).";

// - 03XX Errors are errors of system failure :
$MESSAGES["0301"]="0301 ERROR:SYSTEM CANNOT OPEN FILE.";
$MESSAGES["0302"]="0302 ERROR:System could not write to file.";
$MESSAGES["0303"]="0303 ERROR:System could not create directory.";
// Easter egg #1 :
$MESSAGES["0304"]="0304 ERROR:Just no.";

// Additional message
$ADDITIONAL_MESSAGE="\n This file is not available for free download, and is released"."\n under the terms of the Humanitary LGPL Licence. Permissions to copy and use it are granted"."\n by author Jeremie Ratomposon on a per-case basis on today (11/2015)."."\n Please contact for commercial information at info@alqemia.com";

// POSSIBLE DEBUG
// phpinfo();

$isDefaultFunctionningOverrided=false;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@ CORE MODULES SECTION @@@@@@@@@@@@@@@@@@@@@@@@@@@
// A module overrides default functionning at two conditions :
// 1- If core module is enabled
// 2- If the GET/POST call to this endpoint file has at least one of concerned core module parameters specified
// Else it simply will pass the hand to the next module

// ----------------------------------------------------------------------
// aotrapetra server-side core module :
// The words locution «aotra petra» means «what has zero limits» in malagasy, that is to say «infinite».
// I found it appropriate, since it is the server-side counterpart of the aotra, with «aotra» meaning «zero» in malagasy.
// It formerly used to stand for «Tsypetra Superior Yield Programming Environment for Total Representation Aotra»
// or «Technologie Supérieure tsYpetra de Programmation pour un Environnement Total de Représentation Aotra» in French.
// This module provides server-side treatments with a strong all taken care of client-server ajax exchanges,
// in the style of modern J2EE frameworks like ZK or GWT.
// (The choice has been made to put this module into aotra.php default core
// to have this functionnality without having to create a file dependency to another library,
// what would be against the aotra philosophy for minimalistic infrastructure to operate.)

// TODO ...

// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// aotrasome server-side core module :
// The words locution «aotra some» can be reduced to «aosome», what means «what is usually inspiring awe» in english.
// This word is really over-used on the Internet, and when designating a product aonly shows that its author is completly lacking humility
// (Of course, it is not the case for the author of this module).
// It stands for «aotra SOME ORM made easy», because recursive acronyms are mandatory to be cool in software industry, at some point (Some things just are the way they are as the song says).
// This module provides server-side ORM mapping infrastructure with a strong emphasis on simplicity and out-of-the-box developper usability.
// in the style of modern J2EE ORM frameworks like Hibernate.
// (The choice has been made to put this module into aotra.php default core
// to have this functionnality without having to create a file dependency to another library,
// what would be against the aotra philosophy for minimalistic infrastructure to operate.)

// Implementation details :
// Persistence :
// Aotrasome is intended to support several types of low-level persistence solutions (as long as they support UTF-8 characters encoding) : single PHP file (default), single CSV file, SQL database, SQLite, etc.
// For this, aotrasome uses a single table with the following definition : (5 columns)
// aotrasome_obj (fields order matter and should not be changed)
// * uid (positive integer) | module (-255 chars string) | type (-255 chars string) | attributes (+255 chars string) | version (positive or negative integer)
// -uid (Primary key) : contains an identifier (globaly unique) among the whole ORM table (this is to allow different modules to interact with each other following certain rules such as exposed objects rules)
// -module : contains the name (globaly unique) of the module where is this represented object
// -type : contains the class name (unique for a given module name) of this represented object
// -attributes :
// °For non-primitive objects : contains a string of «,» separated couples of attribute name (as named in the class) and attribute uid (Foreign key). The separator in the couple is «:»,
// and the «¢» flag at the beginning of the couples means «cascade delete» of this attribute, that is to say is deleted when parent object is deleted. This is marked with the «@Cascade» annotion in the PHP classs.
// example : «¢wheels:123,gpsDevice:234,coffeeMug:235,¢doors:124»
// There are primitive types defined, such as Array (corresponding to a PHP array), Integer, String, etc.
// °For primitive objects (String, Integer, etc.) : contains the value of the object, if necessary, for example if object is an Integer, a String, etc.
// -version: contains the version of the object. If an object is «deactivated», then its last representing row will have its version value turned into negative (this operation is reversible).
// Mysql databases initializaion commands :
// # create database aotra set utf8 ; use aotra; create user 'aotra' identified by 'aotra'; grant all privileges on aotra.* to 'aotra'; update user set password=password('aotrapass') where user='aotra'; flush privileges;
// # use aotra; create table aotrasome_obj (uid int primary key auto_increment, module varchar(255), type varchar(255), attributes text, version int);
// API details:
// Aotrasome provides an hibernate or jquery-like API, for CRUD operations to allow a code that keeps being minimal while understandable :
// examples :
// $SOME=Aotrasome::initPersistanceMysql("localhost:3306","aotra","aotrapass"); // uses an interface for object «Some» that is implemented by «Aotrasome»
// $newObj=$SOME->create("MyPHPClass")->fromExisting($clonedPHPObject);
// $objectsList=$SOME->select("MyPHPClass")->where()->and($SOME->eq("myAttr1",$value1),$SOME->or($SOME->eq("myAttr2",$value2),$SOME->gt("myAttr3",$value3)))->sortBy("attr1","asc")->limit(10,20);
// $object=$SOME->select("MyPHPClass")->where()->and($SOME->eq("myAttr1",$value1),$SOME->eq("myAttr2",$value2))->sortBy("attr1","asc")->first();
// $object=$SOME->select()->where()->uid($foundUid);
// $updatedObject=$SOME->update($updatedObject);
// $SOME->delete($object);

// TODO ...
/*
 * interface SOME{
 * // TODO : public initPersistanceMysql($serverURL,$username,$clearPassword);
 * public initPersistancePHPFile($filePath);
 * }
 *
 * class AOTRASOME implements SOME{
 *
 * // TODO : public initPersistanceMysql($serverURL,$username,$clearPassword){}
 * public initPersistancePHPFile($filePath){
 *
 * }
 * }
 */

// ----------------------------------------------------------------------

// @@@@@@@@@@@@@@@@@@@@@@@ END OF CORE MODULES SECTION @@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// Default functionning :
if(!$isDefaultFunctionningOverrided)
	echo main();

// FUNCTIONS
function main(){
	global $SALT_KEY, $MESSAGES, $FREEZE_SECONDS, $ACCOUNTS_SEPARATOR, $ACCOUNT_DEFAULT_NAME, $PASSWORDS_SEPARATOR, $ADDITIONAL_MESSAGE;
	
	// Possible redirection :
	$otherServerURL=isset($_REQUEST["otherServerURL"])?$_REQUEST["otherServerURL"]:null;
	if(!empty($otherServerURL)&&$otherServerURL!==getServerURL()){
		return redirectRequestToOtherServer($otherServerURL);
	}
	
	// Case no parameters at all : (simple GET request)
	if(count($_REQUEST)===0){
		return $MESSAGES["0100"].$ADDITIONAL_MESSAGE;
	}
	
	// Required parameters check :
	$actionType=$_REQUEST["actionType"];
	if(empty($actionType)){
		return $MESSAGES["0105"];
	}
	
	// Presence check
	if($actionType==="PING")
		return $MESSAGES["0004"];
	
	// Easter egg #1
	if($actionType==="MANMAKEMEASANDWICH")
		return $MESSAGES["0304"];
	if($actionType==="SUDOMANMAKEMEASANDWICH")
		return $MESSAGES["0003"];
	
	if($actionType==="GENERATEPASSWORDHASH"){
		
		// localhost check :
		$refererURL=$_SERVER["HTTP_HOST"];
		if(trim($refererURL)!=="localhost"&&trim($refererURL)!=="127.0.0.1"){
			return $MESSAGES["0203"];
		}
		
		$saltKeyParam=$_REQUEST["saltKey"];
		if(empty($saltKeyParam)){
			return $MESSAGES["0106"];
		}
		
		$clearTextPassword=$_REQUEST["clearTextPassword"];
		if(empty($clearTextPassword)){
			return $MESSAGES["0107"];
		}else if(
		// Because we use a 256 (number of bytes = number of chars * 2)
		// key for encryption :
		128<strlen($saltKeyParam)+strlen($clearTextPassword)){
			return $MESSAGES["0110"];
		}
		
		return getHashedString($saltKeyParam.getHashedString($clearTextPassword));
	}
	
	if($actionType==="WRITE"||$actionType==="WRITEENCRYPTED"||$actionType==="DECRYPT"||$actionType==="UPLOAD"){
		
		$pageName=$_REQUEST["pageName"];
		if(empty($pageName)){
			return $MESSAGES["0101"];
		}
	}
	
	if($actionType==="WRITE"||$actionType==="WRITEENCRYPTED"){
		$data=$_REQUEST["data"];
		if(empty($data)){
			return $MESSAGES["0102"];
		}
	}
	
	$wholeViewPasswordHashSaltedStringFromFile="";
	
	// Write password check :
	$writePasswordHashSalted="";
	$writePasswordHashParam="";
	if($actionType==="WRITE"||$actionType==="WRITEENCRYPTED"||$actionType==="UPLOAD"){
		
		// First we check if the asked page is writePassword-protected on server :
		$wholeWritePasswordHashSaltedStringFromFile=getWholePasswordHashSaltedStringFromFile("WRITE",$pageName);
		
		if(!empty($wholeWritePasswordHashSaltedStringFromFile)){
			
			// If it is : then we check against received writePasswordHash :
			$writePasswordHashParam=$_REQUEST["writePasswordHash"];
			if(empty($writePasswordHashParam)){
				return $MESSAGES["0103"];
			}
			
			// In all cases of write password correctness, if freeze time is not complete, we throw a «freeze time is not completed» error :
			if(abs(time()-getFileDate($pageName,"modify"))<$FREEZE_SECONDS){
				return $MESSAGES["0204"];
			}
			
			$writePasswordHashSalted=getHashedString($SALT_KEY.$writePasswordHashParam);
			
			if(!validatePassword($writePasswordHashSalted,$wholeWritePasswordHashSaltedStringFromFile)){
				
				// If write password is incorrect, but freeze time has passed, we touch «modify» the file :
				touchFile($pageName,"modify");
				
				return $MESSAGES["0201"];
			}
		} // (If not, then no password was ever required, and that's OK...)
		  
		// We get the view password hash, if it is present in old file :
		$wholeViewPasswordHashSaltedStringFromFile=getWholePasswordHashSaltedStringFromFile("VIEW",$pageName);
	}
	
	// -----------------------------------------------------------------------------------------------------------------------
	// Actions treatments :
	
	if($actionType==="WRITE"){
		// Then (or if page is not writePassword-protected), we write down received data to file :
		// return writeDataToFile($pageName,$data,$writePasswordHashSalted,$wholeViewPasswordHashSaltedStringFromFile);
		return writeDataToFile($pageName,$data,$wholeWritePasswordHashSaltedStringFromFile,$wholeViewPasswordHashSaltedStringFromFile);
	}else if($actionType==="WRITEENCRYPTED"){
		// Then (or if page is not writePassword-protected), we write down received data to file :
		$viewPasswordHashParam=$_REQUEST["viewPasswordHash"];
		if(empty($viewPasswordHashParam)){
			return $MESSAGES["0108"];
		}
		$viewPasswordHashSalted=getHashedString($SALT_KEY.$viewPasswordHashParam);
		// return writeDataToFile($pageName,$data,$writePasswordHashSalted,$viewPasswordHashSalted,true);
		$v=$wholeViewPasswordHashSaltedStringFromFile;
		if(strpos($wholeViewPasswordHashSaltedStringFromFile,$viewPasswordHashSalted)===FALSE){
			// if we don't already have this view password hash in the view passwords hashes list, then we add it :
			$v=$v.(empty($v)?"":$ACCOUNTS_SEPARATOR).$ACCOUNT_DEFAULT_NAME.$PASSWORDS_SEPARATOR.$viewPasswordHashSalted;
		}
		return writeDataToFile($pageName,$data,$wholeWritePasswordHashSaltedStringFromFile,$v,$viewPasswordHashSalted,true);
	}else if($actionType=="DECRYPT"){
		
		// First we check if the asked page is viewPassword-protected on server :
		$wholeViewPasswordHashSaltedStringFromFile=getWholePasswordHashSaltedStringFromFile("VIEW",$pageName);
		
		if(!empty($wholeViewPasswordHashSaltedStringFromFile)){
			
			// If it is : then we check against received viewPasswordHash :
			$viewPasswordHashParam=$_REQUEST["viewPasswordHash"];
			if(empty($viewPasswordHashParam)){
				return $MESSAGES["0108"];
			}
			
			// In all cases of view password correctness, if freeze time is not complete, we throw a «freeze time is not completed» error :
			if(abs(time()-getFileDate($pageName,"access"))<$FREEZE_SECONDS){
				return $MESSAGES["0205"];
			}
			
			$viewPasswordHashSalted=getHashedString($SALT_KEY.$viewPasswordHashParam);
			
			// if($viewPasswordHashSalted != $wholeViewPasswordHashSaltedStringFromFile){
			if(!validatePassword($viewPasswordHashSalted,$wholeViewPasswordHashSaltedStringFromFile)){
				
				// If view password is incorrect, we touch «access» the file :
				touchFile($pageName,"access");
				
				return $MESSAGES["0202"];
			}
		} // (If not, then no password was ever required and it's OK...)
		  // We get the encrypted part :
		$encryptedAotraScreenContentFromFile=extractEncryptedContentFromFile($pageName);
		
		$seedKey=$SALT_KEY;
		if(!empty($viewPasswordHashParam)&&!empty($viewPasswordHashSalted)){
			$seedKey=$viewPasswordHashSalted;
		}
		
		$decryptedAotraScreenContentFromFile=getDecryptedContent($seedKey,$encryptedAotraScreenContentFromFile);
		
		return $decryptedAotraScreenContentFromFile;
	}else if($actionType==="UPLOAD"){
		
		for($i=0;$i<count($_FILES["uploadedFiles"]["name"]);$i++){
			$fileName=$_FILES["uploadedFiles"]["name"][$i];
			$fileTmpName=$_FILES["uploadedFiles"]["tmp_name"][$i];
			$fileType=$_FILES["uploadedFiles"]["type"][$i];
			
			if(!receiveUploadedFile($pageName,$fileName,$fileTmpName,$fileType)){
				return $MESSAGES["0303"];
			}
		}
		return $MESSAGES["0002"];
	}
	
	return $MESSAGES["0104"];
}

// ==================================== UTILITY METHODS ====================================

// ------------ Passwords protection ------------
function validatePassword($passwordHashSalted,$wholePasswordHashSaltedStringFromFile){
	global $ACCOUNTS_SEPARATOR, $PASSWORDS_SEPARATOR;
	
	$passwordNamesAndHashes=explode($ACCOUNTS_SEPARATOR,trim($wholePasswordHashSaltedStringFromFile));
	
	if(count($passwordNamesAndHashes)<2){
		if($passwordHashSalted===$wholePasswordHashSaltedStringFromFile){
			return true;
		}
	}
	
	foreach($passwordNamesAndHashes as $passwordNameAndHash){
		if(empty($passwordNamesAndHashes))
			continue;
		$s=explode($PASSWORDS_SEPARATOR,trim($passwordNameAndHash));
		if(count($s)<2)
			continue;
		if(empty($s[1]))
			continue;
		
		if(trim($s[1])===$passwordHashSalted)
			return true;
	}
	return false;
}
function getWholePasswordHashSaltedStringFromFile($passwordType,$fileHTMLName){
	global $MESSAGES;
	
	// Format is : <!--(write/view)PasswordsHashesSalted:user1=hashhashhash1,user2=hashhashhash2,etc...-->\n
	
	$wholePasswordHashSaltedStringFromFile="";
	if($f=fopen($fileHTMLName,"rb") or die($MESSAGES["0301"])){
		
		fgets($f);
		
		// View password hash is on third line :
		if($passwordType==="VIEW")
			fgets($f);
		
		// Write password hash is on second line :
		$line=trim(fgets($f));
		
		$s=explode(":",str_replace("<!--","",str_replace("-->","",$line)));
		if(!empty($line)&&count($s)>0&&($s[0]=="writePasswordsHashesSalted"||$s[0]=="viewPasswordsHashesSalted")&&!empty($s[1])){
			$wholePasswordHashSaltedStringFromFile=$s[1];
		}
		
		fclose($f);
	}
	return $wholePasswordHashSaltedStringFromFile;
}

// ------------ Password view protection methods ------------

// - Encrypt:
function extractClearContentFromDataString($content){
	global $MESSAGES;
	
	$result=getDelimitedString($content,"<aotraScreen","</aotraScreen>");
	if($result===NULL)
		return $MESSAGES["0109"];
	return "<aotraScreen".$result."</aotraScreen>";
}
function getEncryptedContent($saltedKeyHash,$decryptedAotraScreenContentFromFile){
	$result="";
	
	// //DBG
	// error_log("]]]]]]]]]]]".$decryptedAotraScreenContentFromFile);
	
// 	// Chained : (not really a good idea ! because craking XOR gives the clear text message AND the key, that is used to for both !)
// 	$resultXOR=encryptXOR($saltedKeyHash,$decryptedAotraScreenContentFromFile);
// 	//DBG
//  	error_log(">>>> RESULT resultXOR :".$resultXOR);
// 	// DBG
// 	$result=encryptAES($saltedKeyHash,$resultXOR);
	
	// DOES NOT WORK
// $result=encryptAES($saltedKeyHash,$decryptedAotraScreenContentFromFile);
// 	// OLD : WORKS
// 	$result=encryptXOR($saltedKeyHash,$decryptedAotraScreenContentFromFile);

	// OLD : WORKS
	$result=encryptXORNoPattern($saltedKeyHash,$decryptedAotraScreenContentFromFile);
	
	
	// //DBG
	// $result=encryptXOR($saltedKeyHash,$decryptedAotraScreenContentFromFile);
	// $result = encryptAES($saltedKeyHash, $decryptedAotraScreenContentFromFile);
	
// 	// DBG
// 	error_log(">>>> RESULT getEncryptedContent :".$result);
	
	return $result;
}

// - Decrypt:
function extractEncryptedContentFromFile($fileHTMLName){
	global $MESSAGES;
	
	$content=file_get_contents($fileHTMLName);
	if($content===false)
		return $MESSAGES["0301"];
	
	$result=getDelimitedString($content,"<encryptedAotraScreenContent>","</encryptedAotraScreenContent>");
	if($result===NULL)
		return $MESSAGES["0109"];
	
	return $result;
}
function getDecryptedContent($saltedKeyHash,$encryptedAotraScreenContentFromFile){
	$result="";
	
	
	// Chained : (not really a good idea ! because craking XOR gives the clear text message AND the key, that is used to for both !)
// 	$resultAES=decryptAES($saltedKeyHash,$encryptedAotraScreenContentFromFile);
// 	// DBG
// 	error_log(">>>>getDecryptedContent RESULT resultAES :".$resultAES);
// 	$result=decryptXOR($saltedKeyHash,$resultAES);

	// DOES NOT WORK :
// $result=decryptAES($saltedKeyHash,$encryptedAotraScreenContentFromFile);
// 	// OLD : WORKS
// 	$result=decryptXOR($saltedKeyHash,$encryptedAotraScreenContentFromFile);

	// OLD : WORKS
	$result=decryptXORNoPattern($saltedKeyHash,$encryptedAotraScreenContentFromFile);
	
	
	// //DBG
	// $result=decryptXOR($saltedKeyHash,$encryptedAotraScreenContentFromFile);
	// $result = decryptAES($saltedKeyHash, $encryptedAotraScreenContentFromFile);
	

	// Just to be sure that we return the string in the UTF-8 encoding :
	$result=mb_convert_encoding($result,"UTF-8");
	
	// DBG
	error_log(">>>> RESULT getDecryptedContent :".$result);
	
	return $result;
}

// ------------ File writing methods ------------
function writeDataToFile($fileHTMLName,$originalData,$writePasswordHashSalted
		,/*OPTIONAL*/$viewPasswordHashSalted=""
		,/*OPTIONAL*/$viewPasswordHashSaltedAsKey=""
		,/*OPTIONAL*/$encryptData=false){
	global $SALT_KEY, $MESSAGES, $LOG_FILE_NAME;
	
	if($f=fopen($fileHTMLName,"w") or die($MESSAGES["0301"])){
		
		$data="";
		
		$data.="<!DOCTYPE html>\n";
		
		// Passwords writing to file :
		// Write password hash is on second line :
		$data.="<!--writePasswordsHashesSalted:";
		if(!empty($writePasswordHashSalted)){
			$data.=$writePasswordHashSalted;
		}
		$data.="-->\n";
		
		// View password hash is on third line :
		$data.="<!--viewPasswordsHashesSalted:";
		if(!empty($viewPasswordHashSalted)){
			$data.=$viewPasswordHashSalted;
		}
		$data.="-->\n";
		
		// Data writing to file :
		
		if($encryptData===true){
			$clearContent=extractClearContentFromDataString($originalData);
			
			$newEncryptedContent="<encryptedAotraScreenContent>".getEncryptedContent($viewPasswordHashSaltedAsKey,$clearContent)."</encryptedAotraScreenContent>";
			
			// OLD (DOES NOT WORK)
			// $originalData=str_replace($clearContent,$newEncryptedContent,$originalData);
			
			$s="<script id=\"xmlModel\"";
			$index=strpos($originalData,$s);
			
			$originalData=substr($originalData,0,$index)."<script id=\"xmlModel\" type=\"application/xml\" data-keep=\"true\">".$newEncryptedContent."</script></body></html>";
		}
		
		$data.=$originalData;
		
		fwrite($f,$data);
		fclose($f);
		
		return $MESSAGES["0001"];
	}
	
	return $MESSAGES["0302"];
}

// ------------ Files uploading methods ------------
function receiveUploadedFile($pageName,$fileName,$fileTmpName,$fileType){
	global $IMAGES_TYPE_DIRECTORY, $VIDEOS_TYPE_DIRECTORY, $AUDIOS_TYPE_DIRECTORY, $OTHERS_TYPE_DIRECTORY;
	
	$typeDir="";
	if(strpos($fileType,"image")!==false){
		$typeDir=$IMAGES_TYPE_DIRECTORY."/";
	}else if(strpos($fileType,"video")!==false){
		$typeDir=$VIDEOS_TYPE_DIRECTORY."/";
	}else if(strpos($fileType,"audio")!==false){
		$typeDir=$AUDIOS_TYPE_DIRECTORY."/";
	}else{
		$typeDir=$OTHERS_TYPE_DIRECTORY."/";
	}
	
	// If dir does not exists, we create it :
	$dirPath=str_replace(".","_",$pageName)."/".$typeDir;
	if(!file_exists($dirPath)){
		if(!mkdir($dirPath,0777,true)||!file_exists($dirPath))
			return false;
	}
	
	move_uploaded_file($fileTmpName,$dirPath."/".$fileName);
	// DOCUMENTATION (PLEASE KEEP CODE) :
	// $fileName = $_FILES["uploadedFiles"]["name"][0];
	// $fileHandle = fopen($fileName, 'w') or die("SYSTEM CANNOT OPEN FILE.");
	// fclose($fileHandle);
	
	return true;
}

// ------------ Request redirection (proxying) methods ------------
function redirectRequestToOtherServer($destinationURL){
	$url=$destinationURL;
	$data=array(
			"actionType"=>$_REQUEST["actionType"],
			"pageName"=>$_REQUEST["pageName"],
			"writePasswordHash"=>$_REQUEST["writePasswordHash"],
			"viewPasswordHash"=>$_REQUEST["viewPasswordHash"],
			"data"=>$_REQUEST["data"],
					/*AND NOT otherServerURL !*/
				"saltKey"=>$_REQUEST["saltKey"],
			"clearTextPassword"=>$_REQUEST["clearTextPassword"]
	);
	
	// TODO : FIXME : For now, it's just too bad for https... :-/
	// use key 'http' even if you send the request to https://...
	$options=array(
			'http'=>array(
					'header'=>"Content-type: application/x-www-form-urlencoded\r\n",
					'method'=>'POST',
					'content'=>http_build_query($data)
			)
	);
	
	$context=stream_context_create($options);
	$result=file_get_contents($url,false,$context);
	
	return $result;
}
function getServerURL(){
	$serverURL="http";
	if($_SERVER["HTTPS"]=="on")
		$serverURL.="s";
	$serverURL.="://";
	if($_SERVER["SERVER_PORT"]!="80")
		$serverURL.=$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	else
		$serverURL.=$_SERVER["SERVER_NAME"];
	return $serverURL;
}

// ------------ File methods ------------
function touchFile($fileName,$mode="access"){
	if($mode==="modify"){
		// DOESN'T WORK :
		// fclose(fopen($fileName, 'a')); // File will be created empty if it doesn't exist.
		touch($fileName,time(),getFileDate($fileName,"access")); // File will be created empty if it doesn't exist.
	}else{
		// DOESN'T WORK :
		// fclose(fopen($fileName, 'r'));
		touch($fileName,getFileDate($fileName,"modify"),time());
	}
}
function getFileDate($fileName,$mode="access"){
	if($mode==="modify"){
		return filemtime($fileName);
	}
	return fileatime($fileName);
}

// ------------ Cryptography methods ------------

// XOR implementation : (sucks)
function encryptXOR($saltedKeyHash,$decryptedAotraScreenContentFromFile){
	$result="";
	// Converts string to array :
	$s=str_split($saltedKeyHash);
	$i=0;
	foreach(str_split($decryptedAotraScreenContentFromFile) as $char){
		// OTHER WAY (PLEASE KEEP CODE) : $result .= chr(ord($char) ^ ord($saltedKeyHash{$i++ % strlen($saltedKeyHash)}));
		$result.=chr(ord($char)^ord($s[$i++%strlen($saltedKeyHash)]));
	}
	return base64_encode($result); // normal text string to base 64 string, to «prettify» the output
}
function decryptXOR($saltedKeyHash,$encryptedAotraScreenContentFromFile){
	$encryptedAotraScreenContentFromFile=base64_decode($encryptedAotraScreenContentFromFile); // base 64 string to normal text string
	
	$result="";
	// Converts string to array :
	$s=str_split($saltedKeyHash);
	$i=0;
	foreach(str_split($encryptedAotraScreenContentFromFile) as $char){
		// OTHER WAY (PLEASE KEEP CODE) : $result .= chr(ord($char) ^ ord($saltedKeyHash{$i++ % strlen($saltedKeyHash)}));
		$result.=chr(ord($char)^ord($s[$i++%strlen($saltedKeyHash)]));
	}
	return $result;
}

// XOR implementation : (sucks a little less)
// We hash the key several times to not repeat it :
function encryptXORNoPattern($saltedKeyHash,$decryptedAotraScreenContentFromFile){
	$result="";
	$keyChunk=$saltedKeyHash;
	// Converts string to array :
	$splits=str_split($keyChunk);
	$i=0;
	$keyLength=strlen($keyChunk);
	foreach(str_split($decryptedAotraScreenContentFromFile) as $char){
		
		$keyByte=$splits[$i];
		// OTHER WAY (PLEASE KEEP CODE) : $result .= chr(ord($char) ^ ord($saltedKeyHash{$i++ % strlen($saltedKeyHash)}));
		$result.=chr( ord($char)^ord($keyByte) );

		if($i%$keyLength===($keyLength-1)){
			$i=0;
			$keyChunk=getHashedString($keyChunk);
			$splits=str_split($keyChunk);
		}else{
			$i++;
		}	
	}
	return base64_encode($result); // normal text string to base 64 string, to «prettify» the output
}
function decryptXORNoPattern($saltedKeyHash,$encryptedAotraScreenContentFromFile){
	$encryptedAotraScreenContentFromFile=base64_decode($encryptedAotraScreenContentFromFile); // base 64 string to normal text string
	$result="";
	$keyChunk=$saltedKeyHash;
	// Converts string to array :
	$splits=str_split($keyChunk);
	$i=0;
	$keyLength=strlen($keyChunk);
	foreach(str_split($encryptedAotraScreenContentFromFile) as $char){
		
		$keyByte=$splits[$i];
		// OTHER WAY (PLEASE KEEP CODE) : $result .= chr(ord($char) ^ ord($saltedKeyHash{$i++ % strlen($saltedKeyHash)}));
		$result.=chr( ord($char)^ord($keyByte) );

		if($i%$keyLength===($keyLength-1)){
			$i=0;
			$keyChunk=getHashedString($keyChunk);
			$splits=str_split($keyChunk);
		}else{
			$i++;
		}
	}
	return $result;
}


// OpenSSL AES implementation : (sucks less)

// DEFINE our cipher

// DBG
// $arr=openssl_get_cipher_methods();
// $arr=mcrypt_list_algorithms();
// foreach ($arr as $a) {
// error_log($a.",");
// }
function encryptAES($saltedKeyHash,$decryptedAotraScreenContentFromFile){
	global $LOG_FILE_NAME, $CIPHER_NAME;
	
	// mcrypt is stupid, it only handles 9584 max length strings.
	$BLOCK_SIZE=8192;
	
	$result="";
	
	// # --- CHIFFREMENT ---
	
	// # la clé devrait être un binaire aléatoire, utilisez la fonction scrypt, bcrypt
	// # ou PBKDF2 pour convertir une chaîne de caractères en une clé.
	// # La clé est spécifiée en utilisant une notation héxadécimale.
	// $key = pack('H*', "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3");
	$key=pack('H*',$saltedKeyHash);
	// # Crée un IV aléatoire à utiliser avec l'encodage CBC
	$ivSize=mcrypt_get_iv_size($CIPHER_NAME,MCRYPT_MODE_CBC);
	$iv=mcrypt_create_iv($ivSize,MCRYPT_RAND);
	
	$length=strlen($decryptedAotraScreenContentFromFile);
	$numberOfBlocks=ceil($length/$BLOCK_SIZE);
	// $lastBlockSize=$length%$BLOCK_SIZE;
	
	// // DBG
	// error_log("(encrypt) $decryptedAotraScreenContentFromFile:".$decryptedAotraScreenContentFromFile."\n",3,$LOG_FILE_NAME);
	// error_log("(encrypt) length:".$length."\n",3,$LOG_FILE_NAME);
	// error_log("(encrypt) numberOfBlocks:".$numberOfBlocks."\n",3,$LOG_FILE_NAME);
	
	for($i=0;$i<$numberOfBlocks;$i++){
		
		// # Montre la taille de la clé utilisée ; soit des clés sur 16, 24 ou 32 octets pour
		// # AES-128, 192 et 256 respectivement.
		// TRACE
		// error_log("Taille de la clé : " . strlen($key) . "\n");
		
		// $plaintext = $decryptedAotraScreenContentFromFile;
		
		$portion=substr($decryptedAotraScreenContentFromFile,$i*$BLOCK_SIZE,$BLOCK_SIZE);
		
		// # Crée un texte cipher compatible avec AES (Rijndael block size = 128)
		// # pour conserver le texte confidentiel.
		// # Uniquement applicable pour les entrées encodées qui ne se terminent jamais
		// # pas la valeur 00h (en raison de la suppression par défaut des zéros finaux)
		$ciphertext=mcrypt_encrypt($CIPHER_NAME,$key,$portion,MCRYPT_MODE_CBC,$iv);
		
		// # Encode le texte chiffré résultant pour qu'il puisse être représenté par une chaîne de caractères
		// # On ajoute le IV à la fin du texte chiffré pour le rendre disponible pour le déchiffrement
		
		// OLD
		$ciphertextBase64=base64_encode($ciphertext);
		// // $ciphertextBase64 = ($ciphertext);
		
		// !!!
		// $ciphertextBase64 = str2hex($ciphertext);
		
		// # === ATTENTION ===
		// # Le texte chiffré résultant ne contient aucune intégrité ni d'authentification
		// # et il n'est pas protégé contre des attaques de type "oracle padding".
		
		$result.=$ciphertextBase64;
	}
	
	// // DBG
	// error_log("RESULT ENCRYPTED :".$result);
	
	// // !!!
	// return $result . ":" . str2hex($iv);
	
	// OLD
	return $result.":".base64_encode($iv);
}
function decryptAES($saltedKeyHash,$encryptedAotraScreenContentFromFile){
	global $LOG_FILE_NAME, $CIPHER_NAME;
	
	// mcrypt is stupid, it only handles 9584 max length strings.
	$BLOCK_SIZE=8192;
	
	$result="";
	
	// # --- DECHIFFREMENT ---
	
	$key=pack('H*',$saltedKeyHash);
	
	// # Montre la taille de la clé utilisée ; soit des clés sur 16, 24 ou 32 octets pour
	// # AES-128, 192 et 256 respectivement.
	// TRACE
	// error_log("Taille de la clé : " . strlen($key) . "\n");
	
	// DBG
	$encrypted=mb_convert_encoding($encryptedAotraScreenContentFromFile,"UTF-8");
	
	// // DBG
	// error_log("(decrypt) encrypted : ".$encrypted."\n",3,$LOG_FILE_NAME);
	
	$delimiterIndex=strrpos($encrypted,":");
	
	// // DBG
	// error_log("(decrypt) delimiterIndex : ".$delimiterIndex."\n",3,$LOG_FILE_NAME);
	
	// //DBG
	// error_log("(decrypt) ciphertextDec BRUT : ".substr($encrypted,0,$delimiterIndex)."\n",3,$LOG_FILE_NAME);
	// // DBG
	// error_log("(decrypt) ivDec BRUT : ".substr($encrypted,$delimiterIndex+1,strlen($encrypted)-$delimiterIndex)."\n",3,$LOG_FILE_NAME);
	
	// BUG :
	// DOES NOT WORK :
	
	// OLD
	$ciphertextDec=base64_decode(substr($encrypted,0,$delimiterIndex));
	// // $ciphertextDec = ( substr($encrypted,0,$delimiterIndex) );
	$ivDec=base64_decode(substr($encrypted,$delimiterIndex+1,strlen($encrypted)-$delimiterIndex));
	
	// // !!!
	// $ciphertextDec = hex2str(substr($encrypted, 0, $delimiterIndex));
	// $ivDec = hex2str(substr($encrypted, $delimiterIndex + 1, strlen($encrypted) - $delimiterIndex));
	
	// // DBG
	// error_log("(decrypt) ciphertextDec DECODED : ".$ciphertextDec."\n",3,$LOG_FILE_NAME);
	// // DBG
	// error_log("(decrypt) ivDec DECODED : ".$ivDec."\n",3,$LOG_FILE_NAME);
	
	// # Récupère le IV, iv_size doit avoir été créé en utilisant la fonction
	// # mcrypt_get_iv_size()
	$ivSize=mcrypt_get_iv_size($CIPHER_NAME,MCRYPT_MODE_CBC);
	
	$length=strlen($ciphertextDec);
	$numberOfBlocks=ceil($length/$BLOCK_SIZE);
	// $lastBlockSize=$length%$BLOCK_SIZE;
	
	// // DBG
	// error_log("(decrypt) length:".$length."\n",3,$LOG_FILE_NAME);
	// error_log("(decrypt) numberOfBlocks:".$numberOfBlocks."\n",3,$LOG_FILE_NAME);
	
	for($i=0;$i<$numberOfBlocks;$i++){
		
		// # On doit supprimer les caractères de valeur 00h de la fin du texte plein
		$portion=substr($ciphertextDec,$i*$BLOCK_SIZE,$BLOCK_SIZE);
		$plaintextDec=mcrypt_decrypt($CIPHER_NAME,$key,$portion,MCRYPT_MODE_CBC,$ivDec);
		
		$result.=$plaintextDec;
	}
	
	// //DBG
	// error_log("RESULT DECRYPTED :".$result);
	
	return $result;
}

// // // STILL IN DEBUG
// // //DBG
// // error_log("=========================================\n");
// // error_log("=========================================\n");
// $LONG="<aotraScreen title=Page d'accueil du projet Alambic - «Alqemia» moveAnimation=linear navigationMode=bubblesOnly zoomingAnimation=instantly serversURLs=alqemia.com,192.168.0.102,localhost mobile=true><plane name=accueil parallaxBackground=multiple{'transition':{'name':'linearFade','duration':'2000'},'backgrounds':{'default':'index_html/images/eclipse4.png'}} scroll=heuroptic><config>{bubble:{color:#444444,borderStyle:none}}</config><bubble name=bubble1 position=0 0 size=228 228 mold=circle fontSize=12 color=#222222:0 borderStyle=none openCloseAnimation=linear openedOnCreate=true homeBubble=true zIndex=1000 parallaxBackground=index_html/images/ceiling-437054_1280.jpg><content><div style=text-align: center;><br /></div><div style=text-align: center;><br /></div><div style=text-align: center;><br /></div><div style=text-align: center;><br /></div><h1 style=text-shadow: -3px -3px 0 #000, 3px -3px 0 #000, -3px 3px 0 #000, 3px 3px 0 #000;><div style=text-align: center;><span style=color: rgb(255, 255, 255); font-family: helvetica;>Bienvenue au&nbsp;</span></div><font face=helvetica color=#ffffff><div style=text-align: center;>Projet Alqemia</div><div style=text-align: center;><br /></div></font></h1><br /><br /><br /></content><content language=en><font face=helvetica><br /></font><h1><font face=helvetica><span class=speechable> Welcome to Alqemia project </span></font></h1><font face=helvetica><a href=https://blog.alqemia.com title=Blog target=_blank><img src=index_html/images/aotraLogo_CUR.png style=width:40%;height:40%></a></font><div><font face=helvetica><br /><br /></font></div><font face=helvetica><br /><br /><br /></font></content></bubble><bubble name=bubbleaotra position=0 972 size=400 400 mold=rounded fontSize=12 color=#888888 borderStyle=ridge 2px black openCloseAnimation=linear openedOnCreate=true zIndex=1001 parallaxBackground=index_html/images/honeycomb-622556_1280.jpg><content><font face=helvetica><br /></font><a target=_blank title= href=https://alqemia.com/aotra><h1><font color=#FFFFFF><font face=helvetica>Qu’est-ce que</font></font></h1><h1><font face=helvetica>l’aotra ?</font></h1></a><font face=helvetica color=#ffffff><br /></font><p><font face=helvetica color=#ffffff> C’est un <span class=link data-to=bubble_5>cadre technique</span> («framework» en anglais) permettant de créer des sites web en se basant sur un nouveau <span class=link data-size=50 data-mold=littleBeam data-to=bubble5>paradigme</span> . L’aotra Code respecte également les principes de la <span class=link data-to=bubble4 data-mold=invisible>décroissance informatique</span></font></p><font face=helvetica><br /><br /><font size=4><b><a href=/aotra style=color:#ffffff title= target=_blank>L’aotra en détails...!</a></b><br /></font><br /><br /></font><span class=link data-to=bubble_6 style=font-family: helvetica;><br /></span><div><span class=link data-to=bubble_6 style=font-family: helvetica;>L’aotra code, un CMS (système de gestion de contenu) ?</span><br style=color: rgb(255, 255, 255); font-family: helvetica;><font face=helvetica><br /><font color=#ffffff><br /><span class=link data-to=bubble_20>L’aotra utilise la license HGPL, ou license humanitaire</span></font><br /><br /></font><br /></div><br /><br /><br /></content></bubble><bubble name=bubble3 position=0 -1313 size=420 420 mold=rounded fontSize=12 color=#333333 borderStyle=ridge 2px #111 openCloseAnimation=linear openedOnCreate=true zIndex=1002 parallaxBackground=index_html/images/wood-668884_1280.jpg><content><br /><br /><font face=helvetica color=#ffffff><br /><br /><br /></font><div class=text><h1><font face=helvetica color=#ffffff>Manifeste de la décroissance informatique</font></h1><p><br /></p></div><div class=text><font face=helvetica color=#ffffff><br /> La <span data-size=50 data-mold=littleBeam class=link data-to=bubble4>décroissance informatique</span> , c’est quoi ? <br /></font><br /></div><br /><br /><br /></content></bubble><bubble name=bubble4 position=0 -1972 size=400 400 mold=circle fontSize=12 color=#444444 borderStyle=none openCloseAnimation=linear openedOnCreate=true zIndex=1003><content><br /><br /><br /><div class=text><font face=helvetica color=#ffffff><br /><br /><br /></font><h1><font face=helvetica color=#ffffff>Tout d’abord c’est un constat :</font></h1><font face=helvetica color=#ffffff><br /><br /> La logique de croissance infinie, absurde sur une planète finie, a infiltré tous les aspects de notre vie pratique et intellectuelle : <b><span data-size=50 data-mold=littleBeam class=link data-to=bubble7>l’informatique n’y échappe pas </span></b> . <br /><br /><br /><span data-size=50 data-mold=littleBeam class=link data-to=bubble6>Les principes de la DéCinfor </span><br /><br /><br /><br /><br /><span class=linkBackwards data-to=bubble3>Retour...</span></font></div><br /><br /><br /></content></bubble><bubble name=bubble5 position=22.903993559546365 2174.1263172697813 size=400 400 mold=rounded fontSize=12 color=#777777 borderStyle=none openCloseAnimation=linear openedOnCreate=true zIndex=1004><content><br /><br /><br /><font face=helvetica color=#ffffff><br /></font><h1><font face=helvetica color=#ffffff>Le paradigme</font></h1><font face=helvetica color=#ffffff><br /></font><p><font face=helvetica color=#ffffff> Avez-vous remarqué que les sites web se ressemblaient presque tous ? </font></p><p><font face=helvetica color=#ffffff> Certes, certains d’entre eux sont très beaux, très réussis et surprenants, mais la plupart <span class=link data-to=bubble_2>s’inscrivent dans la lignée graphique des magazines, des pamphlets</span> , et rares sont <span class=link data-to=bubble_3>ceux </span> qui semblent vouloir nous faire prendre conscience qu’Internet est un media particulier. </font></p><font face=helvetica color=#ffffff><br /><span class=linkBackwards data-to=bubbleaotra>Retour...</span></font><br /><br /><br /></content></bubble><bubble name=bubble6 position=1145.171545355245 -2257.97175584468 size=400 400 mold=rounded fontSize=12 color=#333333 borderStyle=none openCloseAnimation=linear openedOnCreate=true zIndex=1005><content><br /><br /><br /><div class=text><h1><font color=#ffffff face=helvetica>Les principes de la décroissance Informatique</font></h1><b><font color=#ffffff face=helvetica>Principes premiers (abstraction) :</font></b><ul><li><span><font color=#ffffff face=helvetica> - Si quoi que ce soit est inutile dans l’apprentissage de quelque chose sur vous-même ou l’univers, alors ne l’employez pas.</font></span></li><li><span><font color=#ffffff face=helvetica>- La qualité des informations est une ressource tandis que leur quantité est un déchet.</font></span></li><li><span><font color=#ffffff face=helvetica>- Les outils informatiques doivent servir les humains, et les humains ne doivent jamais être utilisés par les outils informatiques.</font></span></li><li><span><font color=#ffffff face=helvetica>- Demandez-vous pourquoi vous voulez mobiliser des ressources informatiques avant et après l’avoir fait. </font></span></li></ul><font color=#ffffff face=helvetica><br /><br /><b>Principes seconds (implantation) :</b></font><ul><li><font color=#ffffff face=helvetica> - L’efficacité informatique doit être privilégiée à la puissance informatique. <br /> - La notion de <span class=link data-to=bubble_4>progrès</span> n’est pas synonyme de notion d’augmentation. <br /> - La qualité d’un système repose sur l’intelligence de sa conception, pas sur sa complexité. <br /> - L’utilité d’un système est la première si ce n’esGRU seule mesure de sa valeur. <br /></font></li></ul><span class=linkBackwards data-to=bubble4><font color=#ffffff face=helvetica>Retour...</font></span></div><br /><br /><br /></content></bubble><bubble name=bubble7 position=-1105.7341374312614 -2257.97175584468 size=500 500 mold=rounded fontSize=12 color=#777777 borderStyle=none openCloseAnimation=linear openedOnCreate=true zIndex=1006><content><br /><br /><br /><div class=text><font face=helvetica color=#ffffff><br /></font><h2><font face=helvetica color=#ffffff>La logique de croissance infinie s’exprime de différentes manières :</font></h2><p><font face=helvetica color=#ffffff> Vous avez tous déjà entendu parlé de <i>Big data</i> , de <i>data mining</i> , d’informatique <i>exaflops</i> , ou encore de <i>peta-octets</i> de données échangées, recueillies, stockées...Ces termes sont à la mode&nbsp; ! </font></p><font face=helvetica color=#ffffff><br /></font><h2><font face=helvetica color=#ffffff>Au lieu de penser en Zeta-octets on devrait penser en termes de nega-octets !</font></h2><font face=helvetica color=#ffffff><br /></font><div><font face=helvetica color=#ffffff>Comme nous l’indique la Décroissance, le principe selon lequel la meilleure ressource exploitée est celle qui est économisée, un octet qui peut être épargné est le meilleur octet !</font></div><font face=helvetica color=#ffffff><br /></font><h2><font face=helvetica color=#ffffff>Mais pourquoi ? Est-ce que la DécInfor veut nous faire revenir à la Pascaline et aux ordinateurs à relais ?!</font></h2><div><font face=helvetica color=#ffffff>Pas du tout ! Quoi que....avez vous remarqué que de nos jours la qualité des logiciels a globalement baissé ? Et il n’est pas compliqué à en comprendre la raison</font></div><font face=helvetica color=#ffffff><br /></font><h2><span class=link data-to=bubble_1><font face=helvetica color=#ffffff>Le mythe de la «Loi» de Moore </font></span></h2><h3><sFIN";
// // $e=getEncryptedContent("bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3","clearText mouarff lol hihihi héhéhé xptdr");
// $e=getEncryptedContent("bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3",$LONG);
// // error_log("<br>[[[".$e."]]]");
// error_log("------------------------------------------\n");
// // error_log("<br>[[[".getDecryptedContent("bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3",$e)."]]]");
// $D=getDecryptedContent("bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3",$e);
// error_log("LENGTH".strlen($D));
// error_log("=========================================\n");
// error_log("=========================================\n");


// $LONG="IWHppjYsSNYH9JDRAVf6/NyfgFaM6OvaohcP7O/r9/Q8FswF5Kz3GYqkmK8YX+sylYbAOGgOCo1QIItgFtWjw02WAsELCn1pKq/do7vPupRx7BGqmeJBy90If9g5PAC7pyiSMiLknqPfHefd0AFVWODY/dH/kwvduXE8cGQqgzcYDfgChxMi82Q/cYPcyFB0DTGAN1nn95vwAxsCuTNNK6xWcxXOtbDd3AOnIXHILc/xhJPxvSlpmDy8G19WNN7XQ12VLCRKwp1njbhQi8fkDUM0Qzv6K7w2pbRgZhPpLYBS0zTBq3HeDBeTv3BWgyRnBidnG2tF99nVNqErXin4vCgvqMJWMPXdYcvnrXKPXH0k0wTkRGr51ejMX1YvROjP0RKTJLgIj6a8khzcBvFHIiti5va/ePJbuTEMtx9mFOxWquGD3uc6oGW4dqZk5mrOfsvP4692uaAKGtJd44WkbeNsXTE+7cad7QGNuRGOUNGMGfLDhRhMUo0DJ/X16ChoS1CLMc9Quf/y0w7Hfi0ZmmgPBMQMKPha7LsDGBHyAkaV1Wf9Lvoy87j1VU/bOuxpbPIYt07Lv/HxHm1+K8syPGxV1cSd5TJjrPnNVZ+UzQXpDipG6BLh4+p5KSY+rIr1hd9hwuvx+g4Z6Z/eXZ1STlSYYywVk41EuVmm5cUnw9DFfXnapRdSYEfF2T/EWA/PmIgtvuMh+VvS5y3j3C7o/vnJWveKOY8ZUObtQbJK9cQqS9T3EPntO6jo7PMBTJQN7/rPg4LMH9hb81LegFg5QYpP+FbIysllNRZEVHfXRcZzTo8CmlbVPwGGLzj6fg3zEA/36j0tkyYyX5A5jKwxhbua4Hse5wt9o6iEqlGy1NoS60T4A5deIvB7zYzWkcQYr2YyNbruZxvggm9BDfSW7Lii62ttVhMwLjbPZZuj/yl2Ubq1LN0naWfhZbKNPb0eaTDG4/FzEV7HcEIa97uxWlYZU8QwWB4s5sWnm2RkgYQryHYnUUbeLrtme+l0ssLEdwU/p34K2ppxihCZQxTqaGGec+ThverD69CJNR9ktLHHyJu/oN7Q1LT8l9uhkq+GtepJzXjRM+Kt5Ex9lVnOZ2AsXWXxwzhzJV+OwfG0I+JVF3dPM67rYy6LWANbq3vWO5ty7mn7PuvUokt/AR9wR0Fyri8ud+vt9/zHEjNTkXrkM2ep/jiBD/wfSd5llA+oW6YXS/76Upu3y5y+vzj4c4128xRq8irZIgpTcLcKQLAQxLZGqwNW9NPIPTYqL1KruWjeafmUZ6yjZxTqGVbtrVP+GKG5WLSt8WW26si0mkWc2Qdq+pbxGGl/G9wcyGfLwfsV2HD1mnSi0GeTNbGLx6SZE6qw0uDGfbqtRxTOXB2qRhHGWUe/GxWzgzk2HIbhFfcJ/Yr4AJvJWeNJ3uCNjCb/C3qPffBlDcFuMaZMR0Qizda+2rjMfhnQMiRtxm0vHLDxC+HnuV/dGUyjlADht2LzW1Fb7asEIvUtaiZSBdmv1M8uRJKshXr6OGmBzdKxLFx0dI96kdVWSHfsbwUHb+sL3G5PhAgpgukfISvbWqyvtORJJb+zNX2GvSNqq84aS9o4tzIZYNHLPD0XWHl49bS1ibGOcWbUsSkFEsbcNiyM6V9x0IfRy2eWhgansdKM3siuaxGkVE7CKnYTOLa51vF2S9KpNaT/0QqAW/6fW6uA62kug8kS7lGMgSlfId7LBHcIM7ImwrGNcM9ACYgTccz2a1hx/8r1TPzwiBL06+zjl2vStVpVeaO3MDiYAuhwDJ42qtfI46oB9DMs8JaHLesk/gGFEo1U7qNuyhEALZhhgaAAXlJvLmuuV35Eh3HryvemApocMlUuFG1s2xIw8cX2MsNP3Q0pyleiX27RuGE7odF2h8+LgI80gRKMFj9MIW17vUJ4zIvXZ4IfdEqtt3arFjFjSDa5kIkiQRdYNtYehEdNyGoD7wD3xIGNzW0i/0jzmAES4HPtUKCwVfNAU9IqYshJeC/zvgZ/R1hOdbbhraClpOPKbHckDsSqf1cIKXK9Ymlc/hPZ5mssWyGSiIJPz5Zw/Sbp7TXkh/p+ibhBHcJmhORf2JcbBs633N0YgESI4AB5YpqQPJlAVQu0Sf9Ria0mD0ZCb/NF6twN+PlU9kRZmpPm3FEtjDONEMmbMqs7GIxPpWvj5XGSysmmw6WvKenNyQj0jpaRd8WAcnhdEAOiPVuS4tnO00/vooWsJlqHzspYiP+LVcNmwkJ9XL4PiJHHRXKQ6dnUUOEVGt5hjbHgQ+cXPP3kGW3PJHZ3SN85RYD2jHcT2sGL07l60k8qTS0hvmqAzdgLFmQRBR6jwZFKiKxCBfDQds5UXLqTcDiVVJAkASbXd/N8f+3+sdjUED/0/RdTNvbt+oUw3xmgUw/fOQQo+0maC2KMaexaIYOhIogBEdIP4U3qh9uHGoIiyAzYFlKs/sYASEInc20noXiTL0QDaYMIMDglpE6rLq1vsFLTHKDYqpa9fI34aZq+luzmMQY15IwQdHa07OhExccXNwsY+ZdTNMJpriZH3Kul/NIBiXXc6jiUwbq/ClZMKWRD+iOuvkqkwvAwipnjUWpmpygtwwfae7NKvLcUacboyi2rYfSI+Uq1kelzjw+Xidfw6pKUsRNwAXR0Fi6VCMki58f7QMPHvpj1S5Q++vE3VpLuL2yePibw/WDg8ExXsXMzA4A8vpHWqF3LSZt/pGyECwrKi9DUHdzcCrlk6Wugov2z+C1DG9Oa4yPlU68ykXIMTlrBTUijMIZoDkNXEPZmSMLxoNUksmU3dHy7jhxPkgFkFhsSFBMV9aXbIQgbk1LbQAh4kS07OvRK6/0wR1c5VISDGrzC4Mpjc4TqRqzAmhEe2uwjnCMb7Pm6tJZbXs/6vSupfg/9GX9eZu7Cf7MFsoOfIk8KCYaiTc2qvsqG9Ub2ALNh7swxTFiNdAT5qojA+wZu6kD0VXHv2W3kUcbtEyKaNRZCNjvxAaLgtCLricEmlyieB9QMTcte3CT0g3q3BeV6W/kIK/J+L8yja0FG/ORqYxpjDj9dQaJi6LPYSRND3dGjnZrN74NJUXH66leg10r1rXOQDC3mQKbAotqIF7nuhSQepYSWEMTtvDmOoMlXfVDhL5UpqTaX+W4lQczEVXniYkzA/5tZ1dd8rVibfVfQGNrus2PvKRN8H4PakhKdhuqyZKBi7iGUgIguaDU2syFaC80Qtpl4f+BwJYaA8wWbUJbop8GpH1Gij9ZWVufvg29DMiK/MTY2AYb64iEOv/4gx8Ucj+SFVKlxP38DtYbk3t6bxG9s+7jlYdB0wHoQc0JX1JvxOp6vC3J4qfYoPV9KfX/pzYk4EAqe3LCTP6OyPgjTvtI5aiAXUd6bpF1LcYBqMdrlumdV1lwJIzf27D3GAxlOX3eGMTDkGL8vjNWzHlDumGkhqkWdENtvJqgVxbeObQpdTqqjr8yuXuV5ihHSSxy0lVsNbL50KskSpln44xAK9gM6b72l4kyvwkKbaQUcVKm7cgzhol8h5gHyAkkWjwClfmFUsuksUbiiw5KE4BdpfddUYSRsIsnJFCKVLzMbY6nbre8hXVer9+mgnexKTeVGBcmZsjPMysQd6+/jxqrrGhrM9LrR0AeWmowbXuUWf8LTyAwauqOnbSllRc7ebhIjsNQnRRe1dbUpZLLkLkNg5PoqFHyyHvcmYsoHIAxwfxpUXM5FxiVihCp2rzyPJC/GCiaI/7FCMKbtUG+lMp031aX3HDHoyrB7aglKztuc/JPVgFZMt5ehT5NRdLZqMnw1SCto+eO/2ofG3EPhf1JQpaEdH03oRnU22Hv5N3DALpVRIY/FMfNuLr9LcjFzpjSuTp+iAXRJgRIcdz6GdEldrPJNsf9jamvdjASGRuMDzGJYaTkKDv/i390xkUwZztXdgjiVmofJzIpM7m9SR2g7uw+wx5mvvXJyIymZZF8Yud/ADeiWnKGzNpsVErhIC5BlC6P++64BaiK8ak5zAaLcSZqPI0N4Jhw4DEmpLKsD+ismBQH0Zjbismi/h1c4OrvuSXpLezAkQoDJrOmTacs46p/UEknNjHYXcXTZdb1rajAdfwfJ5SUyT0NFyzkGD32XoN3aY5rOh2JCIZ/DTgEsypYMmfelDkFNkTJyGyxHs6RwQQuyhx21L/N4dhumEON7VVVINtenx124BpA/KQxhrcWhNYuwSLQjmmIh5w6Ahgczuoqh9Cqi7Car+icG91NCLsm4BsPRCaRxrlId7BtFxE5kd+H0AVa8B7Rb8vMHsqrI2vKlSgRvxs8wLeZCyKv5PQnsDmjVIN1JqhUIGYspOfg3/3JIOowvvHc3p2OFHsUsOIg1zPSBnSnsPkyDnCPKEY/QaW0zQfDEDU04SypLPtPsi0QLx4ybng7LQzt1VOvz2cm/le98FFPUC+tyC5rcbTOEHt/3eGotgoL1oFEXNEqkKaprU5cPeKI2utN4Ie61/RO31AX7e4jxXPWxEKHD5sKSPu7qUvH75JS4BTVLMHh+7RG+o5/BtotYxogukEp7UMI/BxACAUW6jpUJEzWje4BJmpj4hnq4aM98yhHnw+QOKH3Zxgh0/gJSDqCXC7NSf3OV9liEKB3//eJCXKeyZwtjXYZ0/BpqwVotAdFtTVrJwICdjWWu2Ga9Trx+hnBxUQaUpjMWjqdj33pb4MDocV3JeXjJN8wFUZCevpz1zlTvVF+RWY6Ei18HSRyezbH0uWPdWf91339sw3i1JaOLwUy1LdWe5MB1/kkQRjyXe4wxmYRJV/9izEK+ikzMAVBaU0/yB7Dwvj8Wt5yPK7xHbAcrjc0p6yjtaAFXGOnWFSLiqAC6AGCr57uDHQ+wdPCHhsZW+I0flmX05tYa2aVL/UtArpUHHJ9MJUmIpJaPj+8EpmRX+ztZE2rC6fNppo7MluOtN6FgkU5YUsfVYNVSrBtYQs89j4Fpkh7vWRow/6As/XbdjMdPDVEU/s0ZzKZmD9nEd01aM0xW4HYrXGyGDPoo7BRClqZAtykQRG80KKgty67116oMIEyJQMrKbGzuRQxXGjqHSbsnrVzoB3APJx6HZwENLqnZYxQpC/zSNAYXvjWpx/eMCVCO97F9PY7O48jUTuKmMjR3f+mGp7a/i41aLKjyR4304kG0zkCPJ53sHEgD9Lw0IuYZVd9omC1O1pU9eEOpJ8fM7C//DcfRXzQDKwDygywwR61E1BZWRgNRZb0j9nGxlN7NFH43y3FZY/ptuAAjIPgaT9oPkNwtzD9bBGECJUy3y00mkkAef0O3mZ50x2I+llRnDGLUTdwc2yylrwGILQOr4LPx72yRAAdHfrwwpRKhxai/RIncoOabFIAnLXAingLYbxgXAWzojjDgVs27QDV1uIHvwXKmgV88Bkajcogg9BkIRYaQKYv7SS6VinRJ7VauqryLL3fq3LyuctRVoiMCb6aC6YvJj4iyR6SQeseKSoaya0TyMUWyvk4RmjMG7uehPfpYl8JC/W2t9f/eP9XORak+mEXmtkPsh/KyaSTKXEyfzGhJYGp+UKGEe8emTj6BmBoy/49u8UyK6kk9lorHr03ClJ1j5W22AFMWtp9Ch7Q50eb0BVD7dfXT2sXJ3ECyQuQIhkDcaysjMqJko7UYzurruxhQDksFNFIZcAgVIcPb2SNSrZJ9vLC2Xg1OTMmJnk2sRnGhIPt15SkWsz0awHDivuUSnGZ9LVvWHEG2hbECZd1ZBMMYRwlcwIQCKOCGVNdMRu56PmV2sQCvHxg0G6Zjczhss8yeEWPMNuR6HN5bT045Xzm1jnRWVtu4+67EzYiPQi+ZvqsOafHwMPV0S2/PA1un1YZlD2Ht2jRjMyMchgb36d8bZU04jz7mFYobhuJVxyXnJiYLOo0vCTBZCDj2pLGfFh61i5+msHan8iqS7DYA8wlYYW0Clhz1g8fC8YT/k6z+gY1LtSN5LvWRgJEe4tsmcRbnav2c1x5lD0j3A8QhJDLMOlmzIrNO6Jrtr9mJRA8OIJC/RL9l64R+/D8GQmJMa3eO4eeVjkL5TRhkGCR2wT+IkhB4V+NcgcXiR/xk18uefxr4UxDPQK2M1vlZK3XoYPf7Tw8ANLLiMuF5xoDzdqri7uLBRpZ46qK3Lj43kHsUCVHEnlnZk/tQza+8yFlFa15GG1+I2Nzh2O4AwBeEeH2hdf8KIalmffyJy1jcX8MjJVoUN5LHaZPrgdTyVVCdy/mtr1merI+2NW8Q1iLUASBZlrXT55ZPHBQUkzxOZxHdPlO4EN1FUoP+SkYfYUP7Gq6Nw82LecaCJOAkboDrfn34z/1JHGUeJ+3X9hnhVWoi6sJMu4QJPkI4K8/WlF2yPevxcMuIiD+f35B7D4u8UJ5995EJ8szZQVBdiPpg2+jlg82ppiSBQKtj75iOVPOcUzuaeiSW2p2toqnSS+/GOwxTYQTaunDSFssEUcqib882y2Bm9WEUK07U1Wvsp4l8qB2cFDFg1Jt0NGgIA1wT7sgZvJK0vS6CT3DQ00CWSSBEXdT1zVXj/7OvgMHKnOOnnqQ04/RGD4MLWQBkHSqsPcIILSIs1YmQSzvsxsqJydxPB8kZgNVrON6vveijM7g7iwzJLPuNZLmPjSheJQODJZ5NJJ0m4cSEuVTm0WXnJYsQmdmJfG6h+BKIGhJMt8rHGAWVEuJAIFeED2RVMnJDneYAe/Q99WHQJ445542PDHUU+z2aKfhhqAuTRhk3+lUXCFN+OvISXKs7yr/Q4kTeAmKMMorEZV9WgBnefGcCjvYDQ2ggryH9+coGI4A7rP5SA5snlKQm2ydd+Cy/FH4b/o9ZfxrQOhfmKJcSHTf1o4e2EuVuDbyPd/xJvpQeAYsUTdiBujNsPoRzln0XFSe4xdm+zyyb3XIqzXzTlOJ9gUjb/PcdKNbGZxzq/H2S93hF7rGSvfErjFKQif0fwjAAhZD+FtS1l/pQpRhUfnKVrMjRTqg/e1I1Fg48ooibqpzRbrjRhiOYglc4jlthfxJfh6DcFTkvzjnSu+Cc5BtCEanUshCNCN/uoF4dc5Y5lcjE0S+fr48bovMdf0z+EziRFLBUl2hkBsC3artklmUGokvSoH7wBTBR/e0xekZ4+zDl8+0srTA2g3qzbQST9kZslsxzGDVpT8E2vH/i6VEZOOVk/HEzULkG9uxmMuJ4OpISj/AmaVI1lT9kuifcfMiZYlkETL33n9TnRIxcHAdHdlwK9/FHTOfvhkckAaqPAgZDlOfJCe1ISeRgAdhZmwzskVS9U/8dmYtuXnmbYpdHF+4gH8cU2LdQrlt8d9BQL6D5rJXE/d0Ekw3KbiFLXokH3FosF8zKMFP8pT9Uf0qvkr7HOKg+oJQ+tw5RBEC0mKjiI7XVntU+1WdFArLNZXQA+M4PjR7Gv8mm6NhIkkpgOccncY9rk6WSHDceiQ2aGyYcTEQt5nKnpL4+gtUR3zZdQpzFGKcm6e1CLbqhF07WNAXbOwTCnarJ38dsRav6rGxS7YgGNMyJ6bWprdbdP7bVtEa16dLaVo94G/XR8+Fc0L62ZIG5ngQ8RtWwN83Ox29NNJ6CDcvRO7Q8RPyDLjhMEWw8gxmq4oVxgGrS0YIb53K3FevUGbYfNllVD6y3jTeICH9RTqEuurSs+U+7X8pC6tLE4JXtWQrFRER82p2RuTbrfGAlWmGRs8GkJxrK0LEOpNT25XdI326Lh1TJbM0u1j5r16wkEmbCGcm6PbfDONv5+gMmuoszhr35zuKk8Nuuz2QKxNQzLYr3NA2RXN1VEw4Z46DLk3qlL6c2Za7yrJz5fI03gss+J71IVfPxqqvlaAOmcP7Umdo68LYAVe8SerrMo8pTdf1rQfWAU7UV60/d2EbTkYRyNwGYPrpvzz+cthf0U4Z42nTv/jMs41L5RPgL/fAqF9nBn7CnhMRjEk28r0iLHobdUeldjZktI10wNYNHa+m+FfQHy+e8izWque89XY0cAf+SNVVn44jwRsHlgd6Nt9Xjww/R8X1Jzc1uWF/8qJ/SOfnH8ZnNdt+jX671emRsg6NxcmUjpJ4DbUtdTWUYA7kvyUA/d2Dx5XEXakGTQJPFDpDHGa/y/3pOuiRGtiXtb8JC3mvFEMGcjvp60QViCSmfEjm7Vn+Kgo78JVL0jJ3XzzVxmruypLst7xATJb0U3L3+xAq3ee2RYlOLRqElvnJwHlQ+y1mjlUSDy8Ksd9qsLg/wZfP4pGtEnMURXOrit5VwU4KnztkgkJlF0KFHqa3MoBobuzGHmRNcEdccZH1EU0MHfEucRNSIQlj2VM4DP13tzrddqk9HHCXdTD5Qou2o912eAwLTojiBVfrvo01WNwKQIubfI3KLaO9crgp0zjY6NBawNLkv7rHm439wbZZTKOqTgwiSCgO6D6kYJjWXhRPZ+bNmKywXkAdFvHr2Zi2+F42bly14P1NCROYYj6s3jA3J2UDFw6Y2BpckOr1DmHE90b8MPqPgw+TUCEjektbCCxm+WnOrf1ykwwOPnRqvBh4sQ2kCmK59glY6VcommuRMHYfs+Bpjciv1tAxFEEb14kg1w1mYpnlO/ORdw49ctL6vbCPuc5HZCKuwwF7tQCDujAglWiCaKRRQ/gSO6LwS63w/wdunAe7W12qmXi7gPaolSQkkqVOtTr3eLePQkwXMoVhOVQX3Ea/16Jswl1KZGFydiZSUG9NuM6S5oO/91/E2UiLfgsudjpi3RGp9KU/YNkjUu+mGQm35kPg021ZpeyNs7fVCsUYAXmBCFSFQS/M5gVi3nmHcb2hoDrDTL2hXUFKEhdsrvQyc44exrKUcyhDl1edkE7uboNdhFjm3xca8sSrzacs6ymHQSNI/EurNYgdR2iA81egYoKh6J72J9wC7n5ciBAoMNp9g0s23JJZty9EF3+U7skKiEprxWPC2JQXeydSWj6Hmj/TgG7KFPAspoGs6BYTgLZHnjdd7KzSzD31pLwVYjXurkq3bV9tsfgLbSUQX45hYL0NpEX4I+dU33H7LYWA4nU1slXcaBGB0Z3DOgAcYV+hkn3NNLCW9Gi/nA9LYvolY2TQVHOcYGazUrjscsGjcgeeaPpWUAgXBcdBUf9XP8psOgvKEqJnPkIfv9qAZqzO8VEQ1jQbsB22XtO564bCYy/LDpDBnYEPX6W4gYhXAkqYLy/yIjhbBrJ1dq/HI0HR1iUKHUG2r7WhSAWU8zuXbq1wIY4d0UBxgK/ktJyvjPWw+sBXH5AKFmZ38K83xtEYr63wE563EF3IpE70K/8xFRHNewIYrP8YnN3CZ55vpI+vUg3nzceanLt85U7dN1ga1tZK5zdVA3TAp5QJbk1AsfQ6oB/jgM7SdmALEAFrfhUh+oV6xyPaEBChk8H+GkoQn8/x67vFnUW7Opox8M8heBVKNmlzRBWlPeExV+RUgPyY51e1rTyqrjS8sb9a1S9fbPKBli+e3/BhnPLdcJoJB/9qjm7VJhAXX0AxspWWjtoSOiNwEXut5IYUc5fQVlm7aHkj9pzNRz7otpf2WXZ2x4STpEnmCS0i7dXEP1ycknHjx+9r1eFyk1qRTCNOZpTMvoOdk5LgWRimBGhFjmnNUcXZO9p4PiuHXniIUCBAW9wUoTcUQre8eksdSY+5W/0fGAKdTOkpiympqA/dinG7ZoNvDvZf+Yu1qIP7m66wY82AU03ZJO0Xi2cuIidntsoOhMfhHhSexD8hEh38FSx5JOtoMGgqYoX3FB+YqyIkLa24v+tRkVz0bz/T/aU1eaAP/nGI1GuiF22S/FkRJTQj8iqn2MGO0XF5Eis9Nat/ZnH8H3VcMTJW/RkfRlQlUuYZKuk3TOZ3p3Hj22/jvxvQeGDrr5qa+LuZDLazomJo8J3BSvAv4TZeShefd5OkkppLDcRtVfjAgCxQjhVCXZxb7tiXzq9DBADiIlOBT6n5q8Noyb6kO3PQzBgfHaW0x+6WYftjMYtl6pDLFGECp6mhWyXREV3wRfLQfkR4y1+5CrQ1p3dFZuKeiz+Ja/DshVZvsM4E6cNY6w6PzAutUUQiA0SEhOkQXd+H9PtdxRIysFKjPuri54DMQYpeBWZB+6AX59bnUUe0TNo2kb8X7pPXJcIYxTYH/a8ZGjIw2fCr2q/MeyvFnQBEvzU4jtArEAySy65DZ10FYWxmdS5Btl06/wmmZuclDIxGjwcUhLiICIbNNrGnVfG+pwTC8ffgF7tQceZ9uC7+QOE+z0Uhect7pejnf/h7lD9KvRc90Wp2BJsGWpikxi/v/nzdbr69hUWFBWjzedlgCfZbm6MB6XPpj5WCMXKRgvCgcirNyinczXaAb5OMLcKrqR69sKFAuFzVDtdgdyVoOIh+IA2HshSDGx/mSE2grEjn1vrR69CIcPhyg+5X9Pwmgf5GvmyCVgULBHnuj77LAmXrzFELP1AX6yiP4i08ynV1m1NM2SYQBeYN5TG7kWH4kgYZI137CQEt6bBCY40hRNZFo97w9Ha5BTiarTYtz49iZd1DK6zkqnMzjpobW96qJEBRJmQTETs7iLmA7zEs4YHUX1XBS9MRNHNQoXhf7uhbg8rq5XoYFwo0nXG2sJz32FqqMPQqGuM2Tpm75U2DC/V0+GPlycCzLS6WQpNj/vpij6UTf8b7c7Jzt5yxCbWmW29rnAsqdOsXPALFfvPa6z8g11N349UDDlxVifZDJRAN9K9o4IIy33PMOATnsOfkqxDWp4iYj/LpzoTut96It5rH/Dilv4lCA3d2nQGZkggK2AlnuibJVQpFl+PlfU8Qyksor9L1jxNvdMW/cKfOZgYrKUfgLro+QbJ0e7tKhluDr8nqnOAnA9uwewtLiuSjg6JENLFai9i8nk+DTAJRB/hC0uqU9BaHNwPr8Tlu7/W3btzJfN8BkSsz+K60=OdL5BGUitDKKM1H4IoVgyZOgUqBVTDOsyv7lIIj+jWtOqC914akM6hV0P/1KF2QEYqv+p1zBorEOoJBmKl0PDTIokPTUZsAy9t7nNdl+0eR1HokFnhsqj0/GQF5qkHESJwnxrMnbdRdxwr+mbIH9vpCeJxGVI3q5ER2chzlioxy1YtPnusjP13hYLLLL8lcznZxnYsjxExOUkApdffOjq/t3ngnUQnTs8WbtdqqO6MNHyNL+EK6hrvrMd1GIEtR6HoFAMXMFSzYY3a28sjjxdNhZx7w8FNDUz6eTjtC5XmUoyWFSMNmJGJBGZ9ZKfojnwPmt00CZWW1OJ4m/Un/rhX88PpeGqln5vPxW20y2/brLQCVryYPMn/y9Nziwq2QTdI8W9bp6/v6GtuJRSApAjZnjGGXiZo4rF1hAeOzUG63oUfDFiBv38D5dohFDmL8aPhNSH3fjciU2FzM9Pn3ybLp4EDq+Y5ODltgW58FunOaaBzMOe4Yt5zK4knOFxIFjJIn4jQ6deg7oN+ER1HbKAkiAtcaEZ5vaAWFoKVUOBxJzBF0Enqi9WdZaGuGNdsHKMyd/Qm/UvKBVs6Hj0zSaUAMefHiIcKhxgLcObd4hKlRdMLnka2J6A0lfH3s+cMuayEz4UDwOCkFSU20m3ZrG7orZrXKc0gy2p3S5ia2aTl0PsDvgG6tEmyGmTRT/KQIQRNhIaO4EkDAzNXPCxd/sbAYPF563iefTayRn2IeKz7kmM5d+SgjExjRqxxDvfHL5nLkD85XyVWHjrnQDnQPE2PZLw4q9t8LccJ5YIl8cL13NiarN948FInfjDFoWHo6tDYCR/ZJpfLnYSioOBSTtsCznAHkXfUPj/IZNuJ3bAuqLrmqdTpmlX+w0SijhEiPaz4Lr4vJ5S84TWi+k68oRTOABzVcHMi5lD+6TqybGfyaCVNPB3zBh2n+ocSjS5DbsFHIc/ibEzWyXhblACRcd9ul/5AaeZNruOMB8fPBXMY7VzSat895ElkRaNI6twUdzxQFh73VwH5/+YEqjsf8e1wtLclpJ24m3ruGJAeNh/53nEwFqxdbWqgk/rC11CQW6KJOAjdkT8AE9hqR5n5ih41v6/MeBemQvZ8kjyAwGnTbKwixZO3lYqKanTWWwkAsGcZSBbh2eSggSZmqMke25vtU4urn+PDjIzwNDK64NNteM7L4PsTm4aGuj1FE+FznpStazfa7vom3K/ZH4euZJOjcRANwmEPUKmb2ucbgf2tZRUqyPS3oDyb+S7ZxRvp3tkPKOIQLJZYlAfXeKC1VHYQGuNOqXeUFNoCkt2MgbzLEnlqdS+y2QzaxOYJZLFFVrswtRV77hDWia3x3H//dzEpXmnuqUOOfK6Kiweubtf7gt+SsG23tiPWd/ybUfoYYW4ONE2BEHh/vdbZWGrY++376Xrma0Nif9hp5VKJ3R0UKfh0QDiBzX0rh20PJzHMU7tobU/uapcLTDA34kwgHKaJx9jJNLJZ2vYMOjUdxgGSZxWi6Fv9O4wCQyFL8KcRXKBCjUw/6hHbaCXVfK9wBn5+s3V/mkum3cMxIVKedG+L0mWfvC1+QUGZUXt2xV0NA7cV/wNq4xY2GLHtlIJU2kvmCgwMhX9QnVWmJPjbMeove14QBDEX6eZ0fHYW9UUmfSjUUqjuQLnpp955Yjv5f5k3ji3VtPFZkhcm0XoKhbChv8/lVTzT4AYRGMpeLWgCACrlbqj0/UmYXRshTcRnHVEuDV+fpGPkVHixT7HbVcLd49/rYAuEQItg+RV87K2zsNRFRy91qD06QlHIPbizAdLxkz5BJ4XGypQRd/dJs/dvN7MZIrENFTqyNnaI0G09uzoFklb8VEq8zzqwjUAs1aCgasPQzWUtddnQPH34ZA4NxvB9oO5YBATMMXPxiIlxvOrh2NcBgi1PIkX63nem2JW+5kB17/nrPxAkefET06/LVyIdMDCYJD4Rc3i8gc0NXJzcV556EGFccWaHos9jmOwGSOQxxrItZRPrRIUhW5D+2qdvo0kMwlYe241+6Um+SOwtjmu9XTicjid4PYzYbrT9+dgvUXOCSuTNyAWe7QW6eLSuvTWqgBb9lHuD5zEZE4OJO2bRjLoZDmDRUPkFWBNWO7r0XkT9fPa9k7A4284DJTPznUlr97vH41ll0T7rh3AKjvY3cIq5vuFQ4NAh/HlrFmUCnhKjC8JATm2f+EnVBcIXjsV4ajuL9OQIh0b7nYHDcoiK4XVD4jMLmxaauRIYJ7ZuENTpXraJ/vYG8m5ZIdKo+dDqHAfE/nhKqFz0yhg5mj+ThAPoTXcXfUKzMa4Yay+OYBnljbcdfFmSb5JzjqRt0birZ/3/GX6VZJ7bOJCLgz9btA2nqusMKzq9VMwXe5zAeaexeybx39c++CTh6qJ6brhtsjzZ8Kigh0aSpVpJzJiosDsqhu97FLtE5e3vg/xU0wlNvjYCNMZyuBwlkLpm/k6FxC+/A9gNHPj3rJ4CK5AYbX30aL+Vj7BU1l2OyinfBCvmHfxka/2jpWxBZzsER2edMU9iGqYtqTaVQ+VL6oOVBNOXzlqatIN9aR4bVa5hbC9hcbgztY/Re+G0UpKsu+lqVZFzTBY0AZOtmGyWhswlYnaRd0mlUOdYqXKccGFVjkS11s8Wv80WLsoHTVhKA0VNNUCxpI4OH1b9PXJOLl8eZqq6zTBsa668V4K3qGsun2XeCyIL888N8UFtnO/lIgvkDR10w50mBAOyqTf7Kj1eYPq3ShRQJECiYqP7G8qdKQYZ0T+8pnyOhZBnjWKcDyAFOZnqOe2bU2jygR3OFwIqlWXLYduJEizUE6hUx9TREN+Z0N4uMYFjkljv1lKZKUam8Aaw4Ll16k227W9/ixaA9HmKJ6voK4QmRhAgIA5jF4rFQKuswM1tBinbF30vzPeBS6/RT74vxbgi7Q9HWVwaI4YJlzzLdSvZsjFfTnfDLKajsmamBKssvX0bmHTz2eo5fA90H7tT3s243BUv1K+vLn4MWDfq0M+k8mDsV/AvfbdoFCE4e8SrjtwWvVcYJtuXmL397yMSbYdQwvGQsPbZodRjBfDVh4OFhbJcLgCJ+YLBY/68SaTUhIh4be9IBdUbZ8wPvHiCCuOO4ZVtThC9K0LjBxaMb99Ml1o+mQaMrAY0Ef+eLO+fEmucyl0TGRVWfVkZjIBguARR4dFKkWlaKyVy0gSxBVMi4HOvEs8ssvgoGPttkL1YjvT+Djt8qqtaRSUigRKWMl15tpOqlWH8NTQtuZzh1AnUb5f4itw6gA+xeh7z8fDf6SnUfv64mLo+FkoOW0DU1MXCm22u8HSbVs/P8dE9RuxA/0yJ+93v/wjvuvsMg8yASATBj/DAlr1tDf3rmu/3qhW49NSF5F0OJHArEAx0ITbhF0zl23nosAHp/JzCe0FhiTUEJdiSjCbag/ZLaB0SMMJ29tL0TnWqwyaVvIkX9pDH1E5DmklESlYwxj7wcC1Ro8Ot6OXhYuKcpr98aTVbhb/0YTiulMSu0fLOboT071ouopsd1jT8J8UMLcBGeGlNS1vbGTMxQNtu2O5IxS0KkW7VwUfkQLKyJncqUVvLInyMfjfsuAYw4mKJVGwWm34TcbpWpqQpBJNtkKq+qNkdGtbu9mHHPYp3jmd/Ls/9NLDTLjzTFZ1yB/BupXGhIrCKF+3LowRUDUNYcgEQPWOTqb3h1jVaX5Oy3PlIoZkG1X6X+59OSWH+YvB+ENBzTIc4zWop7z3SaAD9rsA5AtzbkjtAylTXkhLOLn6ZMbbgHtPoN6HRaAoZGojovF5splbwHGTi/3feIAXft3lor1yuxWgP/52rqW4tPgSuxbqzudMR4k6lTrDyC9se+bPyHPSdyPXLkt8oA4t02NIARonVI5k6D7l2HqKs3/z6b45YvXCLdvwcQz/98i7dKsWJq3hg/85v5zGF0gZNb0l0pzeVQnLEsWBCMz2jR2Q2+9PviHmUXhRfrOIvXlb/vWScnh3J2fO2Jjmkq2tiyM6xwbsGBwUok8bNV9HGSkBcpHKgVbM1YSZ5b+L3BbDxKeJlolOg8Ed9yHmNmv0XhLK1PBFHN0hmROKDRJJmGH5Svs5tM0NQTGoX9fE99g7dDoI+GQ7tWg+c0p3Ws5wxCNo2QG+qPCWDMgWN61v/l0Laz3gtFlUu9JVIFNKriDGYBWZ72HmLXDVg9+TLoqo+nXCYDO2mKKeNYVFxwsIJM0VkrZ94/nI65cwx2l1Z+XRaQ4E5X5R9wO9Va8GrZpfLwDs9pRcO2czGmxXAZ6bDIhaGZgzmXQ4ZrtnBMSgURnPp6o8wRuimss4k7rKsz2uTXa/TYIBnh65FqI0hLty/b2KFPVojKRUydUT43PXsqQLyMPyN9KVYbb188tw1iF6i8olD+rshVfU4A9VbDyNKAK9CS/SI46i30Axuz/+pK7W+nYjgk3kNOK/0ySOCFKyg1T+dyO323AkuRZ6seHcUH4uelzLluh0j5v6iuK3ph5d4NKwyxujBeJyEXYw5WN/9G3KZk4NsibZbxIor814TTcErBj3va5Yv0nplUCAxKoKvPkf+yBP0l4cPNPuHcZ35+qHydby31en+Xs7oeFwFLfeITcuF8trBpt+ZWSwyeCXdCUtCD6peJJ7ZQjhBDdOgyWsNAtaVtYI2UX+Cg86XAQ//VvzZ+Jg6BKwpo+++zOrUHxQ7m6YgjSQ1j57q60Fs5wA5OT+b6oWPj017JOowlJD07+Z+lAIs/IPMKv0hEuJJB2oYtYP0OMlxVuuSMPxtnJb120mvxH2uB5WLRI9pmlF1aFaWR9UZBy5q2HhNg0YPB4UsT3xVbvflz3gbU/Cd+33yeoyYN+xyJ1Z+EMuDECqd0ao3sIiMN4/5CilG3tlPiZBkJWlaoHyg6zOKNDTvbXKyskaVpgpwGi/Fu/bNCWTyAb6CUFdYSIEv+f+Imgp4e4JFVd2g/pSc79lzLsk/aBrj5g2ThXis7k90bbCGWzwMm7C67FZCDxrjrK8p/jmn3jfBm2totVt8Z42cxkAyYr+QlHjKjjf92W06OY0/YFY69+z0ooVvltD+kPQfHFvVG0h0xGmyZIrJH0BElvHKRdf6H2iQldx27GjLbzIWyERYFa/l7Rx13MJQaFzHGdZblyFg0jID6HpFL8eV51mTFnrK9wcZ1ePvtAkYN+JLk9BBqKvHvLF4MNSrh99MXAbguqAbijJVSYnVenXRRiRfqZcAWcN31UqWC8BtPln7ZtwynAHq1g7nrKypzlKm5uxL24GfQX7Cm7AF6lBZFwVhYBdEzk3rgQjxyMtA82H7TH52/G6cs6++VITmao77M88RlndA4BSutlvgL4xLHhuZcFHcfcsQsbMe0nJLE3kr9HUBDgyXR+wxV33kaTNXZAZlpy3QuKdv8i6T6639azr0+O6lemNEvLQeJkTCwho/7r7vYBBwbZRiEOYizcLvQv0LPj8g16QPOy6oxgg1hrXpPpCsRHpsLQzXrBjQHtkYdUXY8m9u0UeITqaScy/x04BI10Cvu22hHIqu5x98/oVKxeigbMegO7bclw+nll3FUJuyDYmk+FsmidMUO/ClrSamq0/ULBn/+GaCAuX9MGoUkGq1ytFfc/HvGG7yX9xTsbfOA+BoXdjv14HTUz8EA6L7CSavkdberjZXD/yLa4hSHiKuGPvwGAD8PAoWvBECL+Ok/LB5WKD/+OtuUDJDvi0giZU4nlDTgofmV72hoeO6UUvVQda6EX0BQs6V7Qn3JRJTzkNOajFBXRDW/azA51LWVv2msgMMAdRd7/dhLAkMdlccLRyWrhxs8qARjIMzYjbFCWTqPS1CpAEKrmaasDBmELOm8WtnGKGZFjDY81jJOH2WbjYZ/k3ckVytjxTgLlIRPxGNBcfzak8Ex+7Iu9/hKQtc6wtUkX5yCe0igup7wbX785B05z2ztgvkkg1PA9eT6ZS0sBhxx0i0MR1V/B+YJluDfughEfgOzwxhwoZvlnfozoDkS20iOoel1GccLkXltLODCgZ2CV0ApkkN9uYkZGaQIV1s/898uMV47E08fWdqs09+TZopl+mrl9sUCXBGYbMs5YGest6QXLIsHlCdziu1ib6MAjSH5XrNClnyxxANtpHdcLqo3vZFWebReLKq/T8HjNTglev4jHf8QPqltXcmsjgzS/81JJQvFX9Yhdx4/asjhYhpTBHyj2KmI/4wdyUvvwrNBsBr3YJltedxp6TtZe2T/FIyQ4t2taDtGSQ/QP2uaiDqc9XzYQ27hg5bEJBO1KMJmrEmmOVN9L3p3eay7sa9tU4FCh9C6HMaDAAQW0dG1fnTzx4MXjBxP81nWzGVK09UVacLnoYiX8qOqGlfSME34/DvPexcKyhrLh4naTc0x6dAZnxHvqQ0I0FcU1/mRQPmUv97DSommqZvGRajF6BaViHhxNwLV47mkMDotxa86v4OrUyliI36SwF+urA7DaGWtjkf8qXTWm8dBndBcWzdDFUo6s2KpfDHUTZfjYABUcPzgo76k9wP+Y9BKcHYBfcPC7Oq07Hm3Z7CvvCGctGp22snuGZezjiSXCkBoUMPscS6qtvCJihxblu9mgfvcgQSXeFSb4IQJciBfRNYfn8f5EnRdal3uOYwaXqK0JDoglHPZK61tes1PINBcfB1g44/5xmI3A/UvBLTahAC6wnExEWVYSntc43ureKHX57r6sLaAS8hrLwWaLWZKX9EJZsCKeMj+2AkRz9bsMzd2P8QrJpgVe21qnSEXX9Zc7TL+ZFWQt3S7pvhupJPDSHWadxsZZQmeREzdHUZpdmLoZUVTVDwnqsQVxQDeZ/FhZeVLYLlEJpAuJYB/1paDokeYBD8ebxtUG4HOBie4IbckFQhmPeo58Kf/KCp3aUKvBGPFMx1DOjmf5s5N+MkPFeuwLbmnKqjCj7r3cJ77gHuXkoOiBYgnjAXPfUFCGgq3Mzwqlt9+73TYuyKHQzWdb+0MUePiUgpHtzaWTE2FmH+z9POm+UcT7NvZZVTMRIUnsGNFEjcipuNiMbOeQVMUIlCHoG49W+4+aqKdF6pecuadsFxm9/+a2HidZ356JyZ06DILReCTfr6SmxA/zgsXr/9QEhr4BFvq/PLFWdKI8UKAKoEoRAR6jZLpq0l+hfAbjIMxE9DbJOopEvVa0/634goRZm1sauKVdT4jdbIvEFavIH681SRWffrIZ2Dv7dJabyDTWjDFsy2liiHaRFlzj3Cvo8P4H0crzh3vFshtf7yS15HBwUmxGuhqHZa3bN+7auRPk2N4VeNHGqFHxxd+RBadGBQrWyWixfpT47Khd8eUVaOvlbqTCTQTEmGG3tNJ/D7I0WwRJyVpLFLls8t0/U1K/9sRYnEUs7PQeyZj9ndqRg01GoEvgd07E7Zv/M/FHHaZ9aSsRVj4kDyWW6Xx+xhItx6QsCxRbhr/scp0NQseTSclkZyH13vZv0xGRu8CNeJBBTE54hhuVgIG3x1QY2GTstUlpBTJrggUKEjMGMnCLTN7KKABfnfA7Ox0Whgk/8H2PlL7qHz/Ic6wWkyCxmDStgKzawRgLNSG8MiYbnoVpQmDO1XwARXpuNnzcd2ulMzEIcap8s0TTS31iC8G/mHMnR9ISVOKRrM5/l1462sa5+l6XSt5hSRT9H06R9c7wMySS6rdoEdyowGKUlFIwgy1sPRUb77Doc4bKWX80YQen+1xRrE12iMLEuoORoNLctVEvIsweM6tuNVDlNKV+G1utzuUrrfa02svd4/31QRJN9HjjyeQuuAu+MJ6IKG20z3QmqJj93OGUiLg+Za5IuWzfNzu/ezex+N4LFOVkpTMUYQSxxMbVyh1MKujiSZqxm734OClJ4LgVTbfy73l7h64W36uhjPX3jOy8eZXcLf31XYGxsqxp4j8ZHZHYX4mmKb58KhNLdDm44usAraNFd8R6Y3xacp4Pnkx439I/QDW6ZimLZ5HflXriPp3LNfUqoOxljxP/UpcInRPcCvZE0Fnf9ucyEaAEWRRC9h2+CACWYacuVqHZwc+0Vm9p+rI9Jgp1OYHpwPB7P2m1zzF/1YMMtNZ8TCFCXBMCYKl2iPo9Y9xoewjPUs6+7KMaDypb1OCTdIVb+QmeLQ4RfPfNMOd8nU770CCr6iUU/EYC78PvPTb9UD2CXB95XOOPMyq3i2LD/JsyT54cfw+d2ysGrgkXBLbCyTToxPTsDuFuGwWLN7oslLXLT8vPBfU5xdtVNkpzZmAGEWjK3Ye7TVA2JdW2BDa/eDLbdIHt/VWLwVFcfO8+pz3kV2cy9JQMQ38svcjBAmITNggk0elGG7JcA+fE+a02aDAW2EPXagNtsyYUH5WjHuEQCDYtD5o3OGaAOSpWLctVWg1kb4aBngYOeoGWXfrM9NAMlg3+V4zA5j5f8Us5oHQRWMGLWV6Qe2c+YN3CRTH/WAGvkjWfPYV4TahIuvGSm1GD01Ky7SOLqVGgpKFrkRLi+PFfpDcUYJcGRXGwSL2C/+TyUlGShxbe5l1A4Nun7Jf5O6rbdINngbTLgmTT4ltz40DvhxEPaVU+/MWIJMcU1JfnYxOwpP0gPyx/ZquCzOz8bBh5L31lpnfV5CDcFd4FEKiF9MV2X8lGGKgKwWCDSsqqw9qboWXA43K7wyhMow/IRNLXQy/45EAzHdeGB7Fh1KgGTQLAL0y3ZcwSXACItwDjSVvayK2ZXPIUpvoHkC8Dh21t4/iP+IXjORnv+FqsErW8YJQ0bJczT92EC6OQkaJqtWBMceWis4fWJsx6huvkbofnKmE38GRhfenYNavVVnJDXaoDC9MMk7jSTGUmEZmjgOOTQjXuTZCeMgHRGAsjLJNWFSX+iKtbyUPkQkHhXcOxM366LoLphMiEUhytn+M9W5h0eYBym3wl1RSxA6AJD83CWNGFZprWWC54iyfVki6BdeDMN8Fu1YFr8BYVXKf5ngjElvCSFCLZqRpYNDOW6ElqTNUwh5XwzpYoMr2qDgtifa8LNMOkv5JOfvcXJpO0MlEhGVw4Yac0r/XZQqRpJ8fzN88euvx8uCHVwLQhLeY6fgTTCLulMMgRcZn20RJ3ptdfFLs2CTHRPIwIJ/P4XfSRB0Li8d8QRBtPYuDetw3rGEjGhb/aagDImTVCAshJ9L3FuTY2caDtpBc7+LphT8EhecJ00K9TRcLxak9qRWgSqBhAsHPpKlPGZDpppyLKW0i+3ZrYqeV+lxRVjnM8LGqfwXy0KiiMFhjii1lYb12DF8U7dk4T0OA6c0drzcby1MsGQ+vRvPkqqLdwX+LImmebT/1UArlJVIjbsK4pzaTlUu+cgyABqx1BH7Kk9kFPCmJb7reEx/DqQjJFQt2hRQH15NjOsIj5jxIk4rfdj+fe9HrbXFmSwBllDcklPslBgh2aW9o/mvPSvJdMl7FOxNvxucgloLtf3xw6YlqHr5LAkGPSsd7azrKGmbzbIYfRcV+nzw8dOJ06I2KleRHXgpPuGE9p6uHI2oi6V5WbNXD41OejZCh/DfsrysX/5FIgSIU0Hs0P6zZ7D/ko1351E5RwjZM/JpwAP7HWavSIWriYJNQR8pUdA+3w1RCGDDXWapVi6AlYnYVvoMCfiy6+9Kmmgefv4zhD4zzYdUfBp7SaPchoKUGalHGNqdxV72ovLS/CP1OosTS1x1ZTQKcmoUF0LkoWft4m4nEbHT2dKh5YkY1i53ucoJKO/LCyF88S+J+4m4jKxZ+2Gi7WW8XInjMWhf4R0pt5Xbny2nTYqNl7DW8iN6+x+UYco9t0dz9K8GkgQaHO6V3tWaIs5xTxQYUKmI9AX1T8XLEjXCN0P3UBMx5dF+qWBulznqOJ238lz+ZKTaD8z8wH6TzG/fIYT1rcjVpdh13MFjlKYt0d8AR4OBixkkhvGSI4XUh3OqUSWIOHVFpcBmcl+4+u68JbbgJa8+Xt3fTFRx5M20kljl33L+ZrBMPpP1JaL3QDVueewJZholFsLPVG4R3fUD79fnNmACkmN+Xkxfwknl7GlGJiBR/g4NsE5+wNJuFZ/o50dy1i68aLerxfi3AQkh/N6YTb9igFH9pNjKXzrM34Aw2m2VteZLd+bWaPTF70nty1aqIQqQ+Aa83GdAF8anqtbHJZxSHcl5WXtf7VToSbP3FL7o9anlMK2nrsbazzNsaxjh/4iXb+IxKByA4JkEOUXaRxyU6rZzadz5sJbmDFoUz0k+1pCoV2mv6stVdiGGWzg16GCYamNQU04deHII0EH2r0Gl2+9j369ouvNDxiJrsi5Qqnk2GSZCpkL0qHXb3UWJgSJPuj/5DnO/2nC0rAc/T0QeNDJJDqbLiRgxiMzOgL4qdUD0mAyB5lbC7tq5nmMVcPfw+QSUPSkBSM0wvCP/sDOLkSakHAeGstUXSvZ8vzf93seAfePrAXbJstChFxluTj8u64TQj166bfc9qYKIk3rjo5cI90GJi8homMlQR9urYGzCZQatgvODrVdBbcUf9R2kkQUVzYcrA/BAzRE6r9HNyHu57RP/uiWsFbPvswbdTBIcwLclI/E6yqGbO73nUvoBgN+mN8LZeM2oVvKR80hVNQ9YnP5XfV3spahFMkhbtBlFbBE5YK9oFdOY+B8emeoNurBVL+tywLbwOyamqGAscr77C/L5qGZFKkX1BiQCg2mih2Q6inVMZgxOQTbLwogk7Zc+n7wfYsQlPlbk0jCK5ttgLe71e6fQpR4ieLsKfRCzql/QPZSsMyceBXr3y2kRIsPnT+IQO5E4oj7BFw4UjSE9b/l3Fw+9/aYrIPAy6MqYgTxysW3GQw3O3NSjvb+DTOda8gY5zTKoJ+TL6zZA6Msj1oF2n+VDHaltZeInrerH8X6b7EhQ/ymvOh/2iCTbuVddZ8hV3BL9lSrwRKS+HzKCWb1N2IlTQ=9cl3QsYKYXvH2dD/heOCp+M8/cC+C6tJcp3wGPPrzPQxraE2Tw7wTgH3f4P+G68MoJURKadBKS0QxW2IPP4EAfT42yGtptiOPap7df0lKXUTlenlIQWV96oJunWGn1qCpFIWSlY801kBr1qJcpZL9y44xPDk/CuF72Y4VADJ6k94fgOUbkoBtuzMTh/5xaOHB/J/lNOgd0+PP+pobq6uWDCFbil4cJ1/oYdd0WQaXxcPhXKnj0s2pIzHn/u2Qe5D0bjSWEpqPkiIu3Ku9bbk4qEYXKLUegG9bf3jrNMdixzbq0vgxdMfmYBNsFmhlwduhQX2zQmqeH46V/UANXW+MdIMBQ4/gB9x8T9zFbzF4UBRqAxNqEm9lIMdtq2AcmL1yfRAhVdsbDN7vKoz2d9cvVbBghmRNdV4YL+OFDHWrxisg4KfoRWptbe64rfgiTZuF0D4yolMgkqcud2eTMR4is04h5IlrFK/DGHnBq1eKofotPWt9WPUjEHvXmWIfrpfA6vGqSRYjr9isA6zZm6zc7uPN8hspg/6VTX7/q0S4MqRJCa+OOCTpvZ5J9xAfOIkHJptzik7IRrUREDN6JYZ9zL7yWZfZgijMASP6bSUZkFwqPO+xTXXD0wqCkybrA+jlPscqcW9sOHUbEAff/MMCNG4DhrcChJBjhYrAH51WEj92uifbRZps9//tnLA10En7gTQuEi+8KN9shF9xykTxdP2vGxc3S9LUAwEl1mqIWYQkLy6J7YISmBLRrvMPGZWRvw5M8gTCt1o5FQWCtsoIuZKd1z7UJ6iGTgFFz+zW+WoU01CLvn76SwoHLIOs/8GPFhlFErt/McP9Ovbg53R1Nc65mcXJA1AhfRWY+jcFjHqxi0bMdpPto7MUGq59R7VEOZFiX+56WF5E73Wbfzsknnca9p07mQwtLyx89YxDIx91UHJvutltk0U8xNmcWwohlySCeNxLOC9zXSglTA6eRaA5lTqna+SgsH5G+pS2SBwCLtD1ICtdS+LyJ4O9nQipR5CcD2KNzZC9uwQnzP+LoQd8G4D9VA7VmUl44OCeshJCWgLaU4u6s9nQEf9ozNvr5o869iytQdw0XLk+DWbtxJUJhy0rOTBW7Yrd/ACIL8rqb99/Ijunmh2lwyVAMgh8YawMNEGCPsp+t+NCLdTIyzUyttW1H9DwkIpxy3KGxW07Y1wjWLuA/X+tT3MP8CBuvzk9Yn6FRGCEGktscB1cxSVy9o1z1vwRNmT3aWDtsExp7/nwOIRQVNzIyMl4I1lMkw/DMhxoCEy6/2DgkEYzR9o/mCpcAcTellMlsq8RhS2fYt6jYvcLdOUWwDkNmHKHX/pcxruARY5GhGnKv1wN4ejLd0kvOGYv31Q6gWQxSwnU2SSZYKirePo0WCS7vTkVD+OR/HggRVNq/LCiK6npg8AZUX7jzI1P7JBanWTZb3DuxOi0kv5JeJgDsQtCxbAtfACiaAXuXjEGAVaSMTjkXqqcj2DKwxRI7sxWzyDECHNofPboxwiNk5gnczbY9Ud8bJasOc570pPKY7ME2BT1dbf0AgHjt66gJc3VcTJGVUiU30s0IsFpzy55u1IRlzZTo4SfNHwJGHfC/GSfYSKq+kn38jEbdwOrw/rFqEP/qJA+Zg7d9qNjcq8muuFWB7wyFu0wVLZvXYZ9SsDG4LxMzXpRYMKYw1Y0ZquaEFWYE0vYDL8EqZ7yOJ9SyCYRWQfyDsodxcfyEOcVATxAfB29MwcafLKu7eD0at6HLmvhpzb6JgHBWGMtpr5Sg3Gr+U5V+ppjVqTvQ9E80+KjCYAbFodi1GYSLvTBeVTEgiuwhqSsLCk+oNHE7iMcqFZCV1XLWLtgV031q5/U9T0I3g5kVofxaRhGVSMwsLI+ioTjSUIEVPeFC2R40oa/Lxb+QU2CedvNeuQHCBJIRU0zyU1ZYgLSuCwlcez5MyFXzb/4apZPNDEjC4HCuCqTliaHaAvCCY27MK21JyKgZ9YKJg2MNspPoBZUKw80HeeNUuEiVNbdqbvoTl5FtWAu1ymPB0jgFCR/3h9kvxiv1roBOd69O5Gwp+yrOqSc/q2Gn9xoZ2i0uwpDVLyWE1+jRX2EbM4m2lQA4PvNMGVXpFa6M2Cirbc6kEkMaU20jXuA027y9dV8+6qOZtp5XJWrqIzZNeL0bs/GXnvA9tYLME2BqO8cgZnA9DIDXSxLcWRk6+ZJxd6KWPWFQu9oI5ASmXDMMOFfRleJiBfm364ocyOPUpY3OjAsdFSRuJIzIgOs434NuMDesWhAPTobZH6ZQQ01Q2Xvf7Py3scJwzPJNlNSjc8Zhs9LNMZDff8KivaycFKcAiY1jUNp5SnO1ffcCxs5pwhi3B2/yFelVDoAUseaVQB7iql1j8mOdXjKB5i5WjsZr8eGPe6gmtfH8nQv1usEg/M/UZ673+oqGPdAa41kCsD2eLCEsC3GTDey35nX+WDMhh5jfSnt1FmOC0hEcC/OSK1aHuvZrtMj9BRamtfF/VJYd+2rywHL85VcPNsU6C81q3OTLqjpIvDr2caaLNs8u/oAqUUhOFkVSjKJGMWQd/10LdShFCc7WSIlBTTqMRfHn6U0FDWpDdAhM2ZxpcZmuS0rnxJorV2PvnnUMzCa5BF9yb2ENa8zwrR1gyQfuHlOItSciXoN+AO/xWz6ubCq0vivsZMVf065XABp+pNMI06ivSr59h99NcxhIOqs1w6Wa4U0w9+zorLaPz2EU15FoGfCCSwlU7C0a/IslLLTP81aJ2a5mXHVFMpY39QexcbPO3ZNiT/5oAypH1wCHl8g08NZ6BKzpXylMfmSDIEP2hujhBDBjY5245IrwZYgEGRGMRRbB6VJKqCDbHmX1bCMaK9T4/qJHGCMIOGRX6lIFPxKP7I1GeK0I0HQRAFtyG7fG5HQbeVCUSq5hs226bJhdQ3TO3NI+EoxYFjJhAbNVX+46F1koPfYCl/ssDgn9Laz1hzT7tOp1yyIMTNq6veR3bjUGrmBLFGdwHiBXILxVH2GwwlwxqFmipSdZf8q/OGfhF5zHtrlhDFhUszo+uVmyD1jXbL9y1y0mgVRQ+faufWuOB1PmXuJZrrwOFQ+CXyldyWB+LEvqskV3jiUZFwMqOdIsp/eHjmguGsfZCu1xrT1rg3lgzKUTAXmv4nD/5387uCRcuKHXFF99PploCGe9bL1CCFdk+JMgW8VpbHUIuxjMb3bUiGWDSdhVHdC1CC0QABZqYR0bNjci/Z2h8Mvyjt1Nu3ec/d6PkVC1MiwrcQTNFPwJU37ea/RlrXDhXQywRyzDtt22Afj78+OIv1g0YXcPPsK5ASEAdINamPOURItcUjqMunyBXF6vCvmPYZCH5TP24S60uKOKE+8nNCvZKaIjt7Vp4Cba3fkVSgOaH6jCB6EJdBvRnihNe47Dszcvo3lYTnWVs9/EczYNXyynacuHLpfZFQW2jvn19zbxbTjLtZ604nm7c5GrcRMCkRmuGwaOht2vUJAOMcJDAH5/jlU67AakkhE9DUJUiOKze7HuO3ujpIZK9bNa6J5ELx2ksFhnV7Eu5wYfMs7AotvpZ8bI3WhE713JhAqGqqm+WzjLZW0BEXHh5FYJP87sm2QI/qt5GxeqMaeiO4jgN4g7jH/0somgoIsQL52Nn+LDxKEjIKo0FIc2TfYHMQMKDeMiZtGObS2lePNujyQZ1yQ/JE/citRDezrmljThDYiVdILfw+xxuiuN1CJa1tGXP6QKwI/ZAW7b371va4py3XdLEw9wSCilKsw2k2BOozWltUiQO/HLE2IhCZD8lpsrTrTRoItD+70HvquEptDFcehdRUhaGHi7QguNKnxeHy+/2wIzuKq4gsC2KeSctqLzRUTH3AMkMdpRUDE4SAsSyW/66cQ+mjg70i+/x+7sxp935+CcqRhCRHB1qJ4Bu5XfUFcgSRmdX0B8XqCkiAZ1P89ePk899j26azr9J/nQH+t6UR98XEwp1bqjEbdDAfmGrEQgRGEbx3/0qRtIsFqlc3OSW7KTTBmrGOBzAfUyfjfQAJXc+9vN06T6Tv+wSLGUNMUBB+b9B2M4hnD1mtFC2kjv8tSwHoEjJa8BBPO6DhG1RKRM3f5qcOP+rkbb/3F0B8nzq0sFM5In8qOjWhzjJKr37RmOe1gg0pdm/y+WZrreiuy1QXRAU4abO6OGgAkPkDP6BCfmAYHFp6Dr95cD++QpLChRiXqdfRguj1ST4DRNgb5qPECTfJ2Xy1plKHlogqmzPHE3p2lProF3fmJlQ7SnzEau40dg04E8QeafZVeGxE9rVZUMGWxJX+Nj28dNS4cPxdg4fU93ikYYPa9WZl1qeD0Z/ylEukj/iWmAAJjhu3K231K6O8A2FSEbxH/Zn6uywK3ZiOSgSB0ibuY7jP3BlqX82SOGlS7q8XhiH9OK1lkbWsZd970w4f5qxch8wNv620oWMGgKzhj9gJZvKwpDoS4h3yJcvC5zbcRLRwyJi5maRWabkv5fJBppY2+oN8ulX7maVMoc5ihn4ZRdkpKDHWlLQMKyBFQ3X1eBGPUxSjRUOz+j0dOPXyXcEFmLIkbOPWLK/HBdba8C0mUZhT0Qz2AjptoUwDoyTghxpA4Hu0s2oY++6DtPHjFDtL142MARhqb4LHHJXqJhzUY9ZGPxqYidAckdRpxR/HDvyD8V2UGYPPdGw7IUWZx3Ed61n4FunO5TxZj6CNGPgCi0VH5GLmAHb/HaU96rRYA7g77g3YE3+AgBq18hbTlDM2fPr3ZNctqi6tDgH0O0AOxePq9oGxG1TGsuS1SiB6xmJCxfqoiW0W58F2q2oOi+kn192asilJ/0t3KPv0k52UkKRnQp7vWH7ws47LwXuy/FR4VAWbV70PfT9pkHhA7GfuKybRCltInDhUevZqORkH230YPLmZ0u/CSnL00qas7MaL5sREco+n4OPHwtTBO1s87ietm21B6yfl5lmdLO8IdSLZ1dkWgB2SXKEI/BhF78+MobHGOfkbIRUUUbEgUU63KOq1IWPrXrQXitm43CthkiHIQq0MxChPDkLoIX4WNNDK8ISE2hK5GHqiy65gyFMN27AlxjuCn8rx/u0VYPzfPt5KozgigmZmKLiW8CRXD+sepmb+alrVWN5msEMbeswZ5ytiFFSyvbepJJ816tA8D9TErhzj7qxUe/9zaDNqGZtWUf+BGfLQu1FpGqiskPPW33W8q0W8qfg7T7EghkRGOhKNxvumwBC6DRtDWpoFmZr7cquE0+n67jTQwc0OhE2PQqfBWddUbkziYmuIE+usQNcBNBhW87YaB0DGat9RsHiouXHMVn56A/98Iw7YqmiODN6vSAQ/8bdeXuSJ0CX6VnuvAVs9rwexmKmd8EGm5tqQa3bAHdf3hz/lE05u+wOO2oWlqMinyAcY/NPBBrwlUofnaSkDony5B45iGRcTn1gsJ5IunP5+Ib6QVpeXvJY3yzMxb+ZxGwjW+/l2E0+iYi1HyzcaqXKH0hkxXohKPHWYK1dhQOY2VQuT1OM6BmirMem4Tvt4eYFN3KnmkjenG8ZdAr9pi+AgbiZAvT5iVh27bT+Gl+g/neIbd/n3OqOvkyB7aCoBkhiUxE1WLoV5gGx8l5tiNgtKDJkV9OX30kysat7Yf0Yq0AesGxn5NSHqdtOjx0/Q8ffbRi024x0oRjutTqx6JXuhIWO51Pv7VZwjOUF6HvcwQnKWhQG7lja4I+kLhyGIjAOdKhIoa6AYkFrmHIbWFIe2zo2rFtXnMIXJbF8oaCZFzpqI4Hzwu0180v2TH3+MKQqIMCMTABCR98qTdESdjNnjvmHiw8J12AhOv/IxN1opcm1w1vrfJFoQygLFMk8sas8vdYKFtpfGMAbNvLSIxzp9UcQFiHzNm8AtWe6lh2AfqN6OogpJ9yc1zLixzLDO803Sz80GdFHRibI90f/Qyz40bP3rPJbhgwN/aRHoEINjyPy+wPzEYuvicfPf0bUmiXwtJ7eeGSA0NMCVGVZpCaKdb1vXNnJ1Iq49jY62jq6lBLBd8frkNvRIjEJjy4z4Jr1Pk413W4pslb5csvWWCuHuw65bDIcTU3AqjDqbr9W+nciOWkpZRROQDE54m6xe8xL4mt0UGsjW69LNFP4arLCUw0c9DdYkbEYHHvvT5bGmjDOqWGbUf5XIGdJlBiI5lqx3UkDXmDWxjZsVcSbFqb8+NtalXHPAAWbb8vkjlDmp/eVzTDAg2GqJbbK19WXc77vGOUSmeQ5WYQB2n2T6fVIHEqQg6bv1G4lTJDSxYMgGfzz0ITOxF5elmneGNzk4zVqCoNKBHI4kCRh/PnQdMTK/qDW5MbWxT9sdpHwu0m4vOJe36BDk3wXY5065fVHuXFYhsb4JW4v1kaI6OLpmeB8Lpd29jIQGrhHgJZbOgBU/d5IX2pWBFFrn9100+KD3kDkR4BHJN8aKFJ16g7cFPxT3fzbnJlaoMo+cASdtvR05RNi14NVvk/hg1pcQkpyUMffrgqsxFKfDIYSH9MiyA58J+zfslinnrOh73HqYeIUFOVDyqCHoi51Ai0ttT9H490vSNLlXh55XDY0y2ACOtm2PQ2nfrvnkmzN4AZS5JgjmkZ05Luh77E8Vnjg3SyISmW6NtA65U85aQV6guuOooNxd2AkDkTByZL9kvMr63VkC26YYkaHUlBOoGgusSh95N01xmb70q1CmCOvMxn0pfoQ9h7kjg2RkJWn5lhQNSm2+irsylIUxneW0aI8wRDuCWpGHhC5gz+DYRUdbpViz4tsyCKWD4U/HqLFViz+4Cv0UxTnHRHFvsUee2n8SDXnb+U3dBUjnkcwpXlQozh+Ysge3rKvQOAClroxeB7iFJjTLYNIlagzNBKC75uJQi92gbNxAD2jKDUBTAXf4oOqvnaqSEuIrxsFo1mVXb/9SH7R49PebiskPnq9q3h+QNm4BH5qZhNVJpCBOfM45rLJS/YCOINXdtgmpJ1qXupniwSyWdMBauQaVP/9pgvV+CUrHXdJsIEtyDWA6fMiXsnU8/sFEcl2RMcXxt8KORP2m+o301gEtuUGlFDRrFcfddGkTi0NuiMizuv4iFZo/TZe4Sm9YAxoU5hlF123eEwsHSd+bz3CdFypUIb7fRMWL/xRR8WvNIc3jxuUoFzzuaNtgs1FP6qG7hQivnSpS+0S52qgKX/qOHyG44OIhhBWf8cv14hApSkc2yGaQhFWVFRynHZiE1jhNEYE9VfZAggz/HzvW8sSyTfbBIZWWoOWkwtMILGtMZYOBAdZ0K3kwhUvCyAVySd+v4Lk2MVGwU9xf/ixk0JSgjobRUg27rEUsRGRxc45+3i3UH6FEChx0mFJeO+Nkv0DwdBowStrJOhO0x66BhqWjdaDd78KwhaxOd4GMl1BFDgRczOzUPHLZ4iqtbG/Or/P0K+lGh7nDAzu1wL1eMMDfV0D5MGrGT2g7LTO/XZrYWms8MiZQ+9wr5YNspNEFWpHfkCx1VYrR2VCwLvZliIcQNewRKOaV3PvpQEseJf2BX4GDAOlkFSMM1n7W7fFQg0ofEbEKhYHdIQ/ZLcl4E6aVEk6a8JkJowBr8r8DT6+bWL+JhoD5Ck8cfpmx1xCX44DQia0K9EZ2WWOfdjXSAS76tcCcQgeMlzYp1O/Dk7aTPlTxqz2qet9ZVeozsSCJdg+LiNpqLJzOGIZ8PXbbJ6fH/vVbu0uyqSZJEKqyW8Rdqsv3oG9U658R68lkHWjrvfTP6NURyP2d0zlhBqtzJYGOs2SxOIOezLh4UtzS3Y8OGkoAX710N1uL92UmqDcZRMqO2REyO6eb4Vr1FSYPWEiE6KVrO3wr28A/gbx/e5K4uakoxijevrjC88D8PHuLUW5ZzphNZmJbDbHJTSa5A+GRLC3Wq0ZWSYxZYGtq8zCxuHxOsyBBWrzPJs8BdqG7Q1gq3/PoJjpLt+oDjSgo0lh20ooCviEbNF2vrTLYpMyzsQuKv4KGME+pJev2gUoL0ihX9IpU386IjhLaD5CLmt0GRYzUjPBvWdRsS/wMUKekmJzt9R0ehvPcL25u15e6dVibqVS3p89BixMO8vU9Bxn1SaUz+rVW5hlWKyjDE0Le/WzUTmuDY6AqF3Mfd73UKnvufP4LNCUSZcx0rJue6hCkjR6ChC3eWz1sIqtGgtC3IMEnEK3xNI2DyleMi/fUYZ2/7l+x8ZocuIseyEd54TKrALkoA7P9ItMxQ6nWf4zezXeGNaewJ4x0vSTC4veQL1q2/g3JtLZlR7oubmWWm0Ei2WTP5Ba5IeSbmau04kxjr+IU6yHisFntQuLdQuXz6b8CPw2MXswJtmyT1PTS3b4kvzDdE9fDzbsPxetnBwjHbnYOvDoR9k9Iu+1/rsv6h1rb91fo6q1uJK82JTeYDlw/yibxIQ02BhhXQNTtZJjMyPcoOh4PHOuRFy0R7FCOwFcNJ4FAm130SippC/zSMI1Vy6RNAU39LsTob+JidznijFVelfI7w1YUya+P2jtyzqdLUsnYeGhZFX7L0amsMH7zbYQaTMH6XhCrJm2cEqXBC5e0CrmVU+2hnGsh3WZjdQ9U0S/sdtH4hApnJeNd0Jxrwvd9iabs+2OsnA0DzTMPH6u1E2hiEYGreAWCOcX9WYVgDsF40FSVPRIbgFrCFz03I8evsUfhb3/5w8HsMSCb67/zo7Ayt4sriD6y06kEK6PHYkLQjP0ZZoghMX4YUr3n6+jtuJST23jq+qaVNsCSBJL+GglHwf5YKvWPLmzbrxiTDmTsuqI9qnZHLRzmK48kSipWqOmxtehMT7SrD3sHkH95tLfi2wNFHfp3Ur2C0k+omXos+TZWRmbS6+io9qKFwrXZGRFOJpotT0E/1kv3uQ6ryo7sqly4ybeeutBAZ0X9qV9JP8vPk/xMbIcmDE+7usoDal/JATZvJpbHNSUJCp6TCRH+fmNj0Waxrkbk5G/U7lYmBBZ1OwAlAMUQRTB1AK3XitZ9wWu1Ap8MtuxFZuU0kqu5aTON5iSOGaOeK4FXJwoHCLi+BsWxVl5bwmBNi3o4SWvH6vbCt2Y1tZuDAjewfeRDKYmmUZQcPxZBNk7Djn0goxVA8NLQs3Nz+z8GK/An30vYG1YZgM3mE5IfrkCz2kUw0lsTWgyNBt5l24o3Q4Dmb9rVJsjhf0Dt/XNDjlRkCI5dMihsJELt4+CQOj/pUbASWyMBdc2QlG+tWYGw0YYXkTxsCi7F+nUKRskFMOnBJiQjBoTmSMYhvqd6mqTatutllNWCtYRQLvySAklQjo3ZqxPU+pZcdvTbWF+cR9IwB6XcXGjCpmr16nPTeeFVQfEaOn1IJsL4VvaMVTIep6I4e9wgu6S6miFKgj8JpCuzr0UagtWgZYzd4SvGDOJHj1iNhO2uQFEdXY7rRVcRhvYCeJ0/4RXQgOU9zreEdLorCtTpTde4gs6325njt2m1zZS4mT2JMgTXcYRkzFk7bEpZexhxPNbt40M63rNlKCBcChR+4QIs4phDBNavT3qJdWvUGbcXo7206BEc5hGLGMAKsr6yRkAzSj0fPOGJ3WhMJVuET1erVRqmreEjEeLK1kf92UY/1TolRlf9mnK4EPpt9YQ/w9eNecdaO6yWqOcLf9BXAZtjkeNmLTxzBFTRXMXoTnrA2J4XpCh5qbJHJZ5gjSCZP9m+dGr+4lQg1Tqi5tqrW7b+W+GC/Xamta92IbtqLDszSHKPtSOIwSG/LRBouQGI3Sdo97OseVzfJGEchBUwa725H9m8pMr1r4cMMv5p7gjWyrgDN9O9O6qut9IzTI8J1OFifP22wSRKyfXZiHDj4/Z1TXWB6bEhoXvn+SnghLUxQZViX9Pi+CZy11QmaxEbZIkKlgFDRzSp8JnlQkvenQJ1nC2WxfTdHNqrntSMg5sioWQTqEagWBKgDDhYjwmST+yAXAvn823Wi3N2mVdGUbW4c3RtSuHgXrui70NAZR7tZHSbWLl1Sav+agubsY/MZTxCMdNlXad13YFJ1TCAneNPf+n152kBEZ5WdB4JfJyEOCwokIo3OO/DOhOVXOx/ZMxFuLEp+jgeEQbd7RPIJ1Msi1uyETCFqPHxjQsgR8n1Z3V52lwLwVrgm0pWnOBECI5OupBE0ue8zY5eDEU8Stnh7ySD2MiYwkRpFZQLA+A99EIpJ2sFC9hf0hcXgLt5U2AGWJA/ESdF2KSq05U7Ljas0+OlJ9+D8F0oOsFjivwSJcsil6vao/NJrAif8h6dvPX9fe9M0NoyuwZkdg4Dr+HzMZUYXMMjyvJ1srkcXCLZLnwBXN5pcJxDHX8pgKb1GlDvGuLkfOBpdGW1bAjG0e0DcifjRx3KBV9uGmZ6LhuMxpzEANwNcDLFr0tuSceQMdJ9YFuFkcrtEXj8GHo40JSBUB6ru/96mvPATpBmZ5C+2ZiS+OYD0KvFhUjFeG+doHA+qzX65VT42Bfe+rNt8eO1qEiCLjUKUHh2O6vkhlmbq8a7/6149IU5xzIvDQEHPbjuyE74B0Jlc5viKROgbHkoV9aGy25Mdxm+4Xwcs5fhqddiUBHg5XL/C0DuFLlnnxDlUobqG1VLf78YaJRMKiEYYBazF3votkRMrQzKjQiKHlbRllPivhFoSCc6eLwE7O+z2XfBUCf87KFy9NDcdHdEmTayEn9+IQLhJfb7EAokA/EEztYEHCjuK0TkF7o52Um77jfeM7rN7tXck1j5hNwJU6RkcMTnmfXl3V1DW8QrWRJeJal2MbhB/HeajNIC8ErVEWDrdWFYwraY33CpogpAhwrse9iRbhw17DiCWKOS3FeX2TwvH08mHZizl6MPDcAFq7jK/vTfM4w1i38GKd78atLEBGY+KaTi5AY/ctHK904L7WRcj/y0VD3jo/Kd+tCHv6aiY5QBbmAgQIa33z9qo8E=t0yRm9Bmr9b77s/Q2fBtpsCq7XedDGN0vtdY2iJBkF7G6oCAhypydzryeBT0CQmRp+Eh1Wa4IORvR2R30akKutBFxjjrax67c7cBiiUaIEODAnQg7tETUlRKyFb7jlmoYS0gWgzDTijnZXs5LrFe9L55e+6t39IxV3X67mtt2quOMH8RVsYFz86eVFC1KN6EesoxEiH2ZMmE2wcdql1Ra3bKSsoi/ULAf2TULG8v/4VrMbmMXBPDjYptIKR3RHaEuc7g9rFvK1MddhuJOuLDTiFpcZ6aAdN3MTk67MBp3G1a40zUxnOaJiDXdI+sJMLI6ddG4kGvSZmw7ehiOzfEzG7f9plCpJ2+Dhxru7Knohw80rCjhHQGGQJdns0DY57fblkQtUbRpBXLqdkCNMnkBEf010q42b9/ns1H8j/TgQyiE72v3dLvP3RfmNVBFEsSKeX8fZqX0SR0dGsbSN4C1WGYLNiQ65pBlj2+6qe1jUoPGxHKJ1MFDhS8conJLNMqr2DP8Ap/kmSr2uI0rRZLTjuwCspRyt3fORKAqwWEukYP86bic7VTgnUVNq7ekw6N6c075sc6s5JAFqSDYJEhymw6UI687xtB3Fx3iM3dx9RpgWXeVVdXlzYxwMZWhis2WVmFiNncZbwo6vqXi0PtHwLkp0rmQKgqXyylVj8ZNzUqhOjyXB4HaDqmZBF3vG/ljU0T4IJB/sQ5oiRVoPUAFe//b1xmymLfSEBHhcmkcfM5u8gLqkFgrNdIhpYI7pQmsVxhIbHva+CieTTis96LUiQ+dmD6StCLbtZG2pmwCKBKKfXpRKsTgT8ni6nhJTk1O457f5+NKfrXAaHpsusZqTDLRp1zTLli7RBdfc5IQZ1KxkInwV062+D36KUC5Qb+D4+kgMinDS9y6rqgHp3BoBzUVoGVHQeMhLsn/Pb8S4JEjR/V+BpNlgp4EStvyMhkMXgKj94VTtptp/Qx/rGnlnj7W81gTBoKQ4q/IKG/MVlkJRBGPGgxYLl2ie7lmyxrqmnx/fp/KPnW3b4NdyzS1uoj5J+hqt2UfJfEUzO8+GExe6wYd0878dgv2gs6QLczuCvMV4sO9TSPCkph6PFB9AYf5aWN60ySsMdPGeStuT8jSX7utnlyrhdvCHbtRp9gwUBNWDhD8ekljelZ5AzMBqXapmmlU8KVP4UnrhVF/qqAiJHJ1q2pJOowtfWJYH7Sflzsqjz2Dv6cLvfduprB3AfISTeByYP2j19OEtN52qitOvMRlYVfPm81EYXTfkxjV61ELuYmYDYJ5U9RtrxoPrYSpI6x5r3H0oKJpYLoUnfoTw01Li3UGpoG37u0MHSIbdnAOu4k0BRE5zzW+u7gXpzoCY3T50qDpeAJfcn40a//sexT6aG6/8hHn6sCd3HsFj9I6HJ6vL5VVqi7LbDFDXMHCkekq3JqBioEMVktm1431V6LfJVVd01aGbKEYYASEJPTWx/2E3Bg2rFu98fw4Gq8yG9+gl0FYXWevUdjwkcVZmr4uQ6ZHfCTUvr/IHLfHkB0JIZWZ1ecZHZXzAsZkHNYJ6dHMvhV4VO/Sup3RHe51GDZSWvDzKU+3fvdIbx/rK1NouxB8TPWV8i9cFFSWgJRtuXyejtpw8lpeSM12vuD6iZqAFan4RrjYGL5s4a1CAm6MzYjhxl3NAaflPQj8QnxDV7A7PDb72ujSA38Nz9QKeqIvoYD7vHX/QvnCsrjyu7mZe3LTY2S6hjlELqbGZHSyQ6+N/XQyCiA5vt5jHHXdIlXRudWYJHMrTqVRDSrLOKfpr/zAy4HRPQ1Z2Ye5dVIF3q7L0y/afYopPSn8nYCHsf5LLPyYUC0EXym+RQ+hMURfFIvwdBc0xp2nT795LZ+zYMFGU38jf0fVImEj8qTS/xsiCsPUYQzdKk6O6xUCved9hcsqOyKFEWZ2FPup8QkikbsH8j+Zm0+hHgBjUdgslzTmIxUGNj/5l0HjH4T+2+0dsUwQ1x15rAckB4lDfmSwCJ6UcH2/cM3ygQkkv2Ogs7nMr2kok79b7DPzILLVRhsawigRrvnZ+g/5pcJjYi6xnoOFf1MPhNJEi7/PUcREmUa/7hVvrwobfMRsFepY4Go95DrggSKZMAvId95nG47rO46nT7ovRccgI0GZydqDFooFFHZ6nZ++awCh9UQZICEUlHjl6O+Oth9yWwrlUqJ24VdWaMvj+uRN8mVn01p9vxY++nOvrfVmJk8P4tA7NsbUUXDtTarPHSkV6LdUnZAYAtdmBFUbGpXpFenaFqqU9f1lufVZ7wyFFGRoeWioCOBYelBwCQ8cQwgITSMPzRxJzJkT3vQHr6wppr4yIyTGhrrIHEofWQ0GQ+J2Pre86AvHWFbngWqcC9ll4Q9aPBVOzZ4kiBuOZ3iYH0Ectd3QZ0JomgDnwodyocgd9tp5XzPLFIQemGhkPkh2NF0wKJJlwvx6DQWOD+u9zwfqyCnOgpvtbDfNNmv8FT56+s3x22mvk8ktxBAlWZITLqqfZP32V+o+4WMyzdBsRyVx9T5oxbg2kP8wx/NExUilz9pcOHfLjqsk7YNQTafl8DxcISo6mHfFnpRypuny0TBicCXrI7jEyrZHzM4/5pnV7qi174FC4BuWA0wEW3PSnLuHBIdIGieThz8+U2NwP2hZe/5pleJ7G52ju4UA7gDAKVXdUdpIyIB3X14afHJMRRgv3BsK7RcJL7jssJ6uoZaqBLBvv4eKAVhUFqiQKpdx2bqey94TWGs6tC/zXi6Vepq65OFsF+w0H9+zZgKTYuWwALl9PCH5NOyHX3eh4VuHbaJmxYEn4MrYapeh4eFw6xIse+PWPfpFgvh4YTVKZsuxcFgYWub1xTq9AqOCx8kKw7CzRQ2yz+y+5KmKBfo+I5WByZZ9pL24cE9YAmSOuFnh71pzKLMOmBE3R+0u52Jw5Dd/1PGCk1iqHwESh6W5Gf0g1f4LFxXy6TzDdSNCFjlS2jRU4ksevojlK388RhdzuP+FJI9TJqoBZLGE6TJ/ZDvSo07P6OhDrmx9FX18RR5a2KDBvoLMv/b5JZkiJEkrMnqd/KIDV86s0N/4Y3njardMjLoSqJRh/qMswI1dzkMHZuTJN7TAZIdJHsg5912s132DcU+/VsYgdRUGwfAi1Nnmanx+X/NSiuAjuQe09YeyCm83H2K/jU92kZN7jPWyKSbG3c+VBDr9/U815z/Wpn36Nq6QFTAtsDYZKAeMXnC2dLwlOSNZh/mixObUOIpGQ48BCwN6aY0mTUC2V1ER3y9erls+Ln8Z0afrj0x6JEMi8zOiA0XUm66b1UncOVKVHeEIHf/KZr0adrzLq5Nnw4rfThpM7FWXatw/XIyOnWBsIwtPhNsSOiYh2YomUlyie0oYYSwB/9DpZCEmP/g0kXvWUCNmRk3i9zw4zJejzF58iMZNNHSpnwJUtNndLBE2BKTMDAYxs3q9L1EGbOyY+w76fzRU9cmFFDi9okIUQw89OKjw7cEfk15ytomQYShRPS6jv6jfu9/2B4MirJAbislJJmNbGNCFPWrVMfS7s1q65eIaKdahJHrI0MB85X8gfDm831N3i/y1MTw2RpfE0cUSBDygl2Q2PfBNFKhT7PaqeNygqYcW+9vvmmFw54bSf0G5V2ojOCcxszmqdpo4I/4Ub7i+rdQJRwoEHNyu9DdUoDSZMenMjtIdsZn0kl2OZvEuyy4HnLU3U8osZw0xAMasj5T0PztMWakHhi3ETCMqfaMU0aH+HrYkdGv7B3RuDpvD5fQ1Buv/fJlo2AtxLEbZGUksOuR+1MXwOl20azwgDT95Khlys1jbuah9ml4IYU84RCBap/FVGFrfqyldepdrDIF1pfu7iT7ShQ6ldo1FmdMz81hcKxINQvNNiLeitic9Boioyhtqqi/96XtnAwFlo8ruuWnrxubVDNLThBXJpqFVO/7GsOduYkNYFaw4k/jHxV8XCzwZANxcifimI7QL60L8++mn+nsBxX+76rzXixHP+sKEFnroWBPTc7SitxV+XOx+3JnIYoC6lok+m2H1QtSrjFqMmd6S0WaEIkl5pOOmW8fK4jy8A4UlRkN57DcYSP8/m76Qv3msrCwOHWwdlbEkPl9uRrKid9uvpuEFECzVOdrDjOLJ09E3BMeSvGbVwyS0BTkxd8cJEqZpioghs9SJ2PcWzascl0QfulFWYM/8n4DfI05gpUTMt5ZystUPwnpYeeMrWe80zfLoJImlzXtE3evt9rR31iM730y7r60cxhVYdRDOZjA02mAtnP81oU/OZdfOVnRdtrdLapuilWQY1UhMfFNn57ahaJIhcbf91xpgsompN5aqncRCHEh6QuzhJMvniECFqKuZXfeOqt8sKNXUDWSY3iRJJkkDBn39k0zRTrhAksChGdVH2/GekEH2reyKxh3xEPOt56M96yXLDEx2bYxFpfdRLBPx8+ShDzxv1Xo5FhlYvCzbRNdIPa9IELVdL2fZzKXpCndoSJVy1+eZ0YK0HNs5LPWOpuAlD0iI5fnZtUNPSJNGFwo1/UWEYK66iMhbhbNKOo2ZSrspaiFFoTvTy/EPb7/9BgYeTCOi625dLyY94xLAmF0Gf06s1r//fU96MJyXQ+Jn9nHoCKVdJhRfxkQJIvJWAjJsjzEJABhk6Fk7dfeVZC5ERfzYXKsQaAhpR9Q5PZAem1Vl++gQ+WXNU9HU2AwiWEXRDEULHFuFWVkPJxwXWSj4EBzdPViFshffrRZ+8IKUXI7fic2Sgbxmc/uUKtG4yyugHmoUXV2x998IAYLT0E5ILStCiMIWtzcKYfDM6KVwl+CrCN3JS2Z7s/c7bp93aT5K/bf+Eft9bhhjQec75ljYgM5ocMyIwFCwbxEgS1FR7tU0kfOsiIQa4UwVhVu0F9TGO6m6lFwAEBILjd0bxO2zSoxOtNdi1VCE+AiJJwFdGJwwrpaccv2iYheluEGybwzFkL3CdgGL/O93yPUlw7qZnF7rH7tVWpD5bB6V8bJmpBn61OKsX2L9iwY48VLmnHAmHstvEEAqwdtbcwKMZcbQnHUcm+GsR5nbSJ4Af/z8LGiBgrS4poxjYwNYFONPmNgAPVfvsmXGJb9lduwmstbJe/mwZpsxBnE4A2XcXMjPr29TwcrpZ481hN3EsuTg9l2vyMvDl0cXCc+fpDp+v+LZZiHHYkM1439jNzRyvfoFzxq5+CnDHD135EPXEaL+9t3PEllOm9tolsycMJdt0b1D4CGUh0pgmsWwBeIQJ9gq7byvAZdscBFt/qvUgQ7jtdXQrXnq6DQp3DMSfixZNhKfaHx9WX0TO8x6DLmSDof31Z0nkXpa+Ud1h5wuN89eMUTNok8yuEJQWJ9lzrbMF5Ess3Uzhl9j9+WvBpQL1iJEC3vTiESgOVV5gGKpbLDTt3PODZswD7jqhohw42LU7eKZuQTyasKCiajKeQqZs1X1V8cwq0fgUnvMHEScQRjvfVzSbalIJo24SN3BJ2HiPqzWZpAzAidCGalRpawfGgkzboJKNChLIWZjj08Djx2X0DpCvtXPDTlMMZNjJO5rW3fx/4hQ5H3VnUC2hUAWbrlznE38X5oVWSeZRXpJ72gCbLQKfHMl+iWN9R6tCmV9PjJHKWlhLstg2BvZ46kBDRGyuCha5V6vIja9ADMvhZmsGblrmWfyAAHCF5L95I+INRv7YJZFggwaxNEgUR386Qx3EmM+UhztyBS/Ckkm3tM/6Q/L720x0MEPAsWJhYTblP7vVKRO9frsgNvL1i/o8M1JMCgm5gWL+EF4pYf2O03YQh2fnkdokpsbdlw9+esRGswLxtcbYV+1aoJpMmkKtaIoMfWOClQwc+szZxYdMpQWvLJDU8hTglM9PSYpIaXjoZ1Spb4Xi/zjUaONT+R4m2i1tiuUsxjCmmKDu0F2c0EBcxyhjZoj4XWuK6Ni6Uhh4nqU44s1sGSan91Cz9uzfn7yYNxJm+NDXbKfl+9cTg7qZGAStdCK4dVktdhdv4Ilu/feGFDdpTRB4QfAFGuUWDZPWjTdQsWUOYmwCK+EahoS0gwFn/GKoLqoaj8hz3mRYaKnUtd4lJA1lvqiW1aY+5FkZpWBhZGHJr9FbH7xld40/GL1ak3ivaCT0JmT5Amguj7FT6gUcgo8RZkbseStRViUOdglStxzA63mhRQYM1/1igszhsHpQcs6oJnrjQiI3+VX//ASi2wTHW13sSNjIvAHywsj0b80+5pZNjQ8Z7QuVjxTzBV8eKbvwazxyyaewKzhBdFITRIUER49IpwK9+824FmQ2i5fvXgHBp0FDsoW7yrBrmiPxs8rj9LjoM5UyDk4XQoSNzWpHAu3uYsdMmHBJLkOqasIqgAGAr/fybhGqYEa7cbInawt1fWmYqszdJBVepZyCaivc1GaTcRO+LXyNQbxGPOqH4DpXPmOHLTNO5/Pm2XwnI9N2hhDaYRMfjZ0uoNEigXin0yA2t6oSkjaMg+bYAdiiCKd7adom7mkvAF0iqffjbpjoXrD9G6Ls9EmYJ+vCAxlXLKpNgQ3Ucf7crVlvLvd9pCzm9Y33ewWug3ooj/ekjtXI53NFi9ETVEqaUpnkntPIEA0c898Sb4NQ3/sfCG7e3qdp1smesm+UD23heWq51P8f+IX94eusQhxZLiyuoFWTl0PCJccFhNhQxPGWeFltkvQq/vHh6YpZ/sKN6JZpBF9dWumM+Yv/PcTToCJfz9XMNTnF56xt8YQ/DJkStnC/8xZhBAmorUEmXCXoOK5H4UDb7InQmp3Pjr1ISGf4zo40y/BtyUca4D5S6f/NPjPcEhrY87TnO7dCHY06jTvuXI1SB8AZSfPcL3qt1ur2Ad/Lg/hXbJXllaBs4O9CfYEDy2Zj6B3nWI9tpr4BSEFjCEhGVvwiL69OWTqEJf1Bx16Bmro7JfmPl+0meZ95iAVIgQDBe2ngogTJbyeCpsuRJuLkB9wmY40oYmKyK5IxUO2pufS+SCSF5kYepFkaF4A7UkAFB0r6/6HAvXZ37s4yTZIRPogxk18c6hcq822rJM7GcBVwsdA+0BCCZaud8+WB+ep9aZMNBrNzO6zoOUOpKJxQmzadQoZFRjoYbYjtgLbGwY1g+FagxV444BEoURHBPDMRZVoK2puGQDL1dcgoLu7JJc3PEkvNx5/tL5IB6rnxE/173mHafTHlf9bJVzCONewaF4wmkzq3h5Eau2+GB06qtko/25RFN+ba8tV9T1QXf3SXH1AfxA1VC6d6KJGHBzj47eSr0GRBEj8IrFvVJe2XJJWbbqvZmxCuw3Q1D2tECOhw5siccIN0OicvIBVM3XTrJy1Ro+5NP8dGHWrGE9Gj0gfn8uQHOygcZRtmiDEGsHUt1kYE19MDuhrrrz73tFnEZAxDpfH6gip++9kFE8pxBqcsF5PSn3NZhGJwWzLaGfNPx84nQI5Oc+O7pvwQCpRESDR0P8fq1V6lZpU0hRl4sI3sbmtNeqigb+mqaIORe0NvQpDYdaBe+YozkYPQRBIKJCQTwRkL0tb9yU66ZfvUW8pSYmqdFN7WbXmEfy2C6F1E32j2oeM/RaScLloaztzsl9RC31QjVUzJVXg1BSiNWK3vKBSHOMnPIU6B66QsxS4pF/6qD2NCQs7jTyPhAUOJpy9SA/fiBQTY949zV6Z058vtCk8gMaOpSjrjLHnbNqn4nq/iOF+7DklKQ9jRJxrLF+N7KxvGKiiNib1HMPGP8/g3d2DdgZmL/NQM03hxhlrIKL3XcaX3NBY/clpdzvg+Rv5eq0czVA+rgXHS2MgUw3C7wVADZGDQ1p+fhxgyriwvf4qSPGf1qGj4ZFpdyQTxT7Tft/9WeMud9q5fnH/qyvENcSW941GxepzK9dw6ots07v5AJLxml5pQWDqaFU2p+QTNTy+TSCQZDZT8WAErfdjlCVet5f+HcoXaEqRCEFsd3GFcDcLCL3S7+0KoS+f3KW7bQ79/Roo2uXMat/WoqMTdaY2wu39Gz9FQci2Nm2AeYaiGnS8yu+EtCdV9TTej/Wd4tmja+1twRqVXdK7zW237eqCiiMvOXK7Ox6npVD4Mhk56630V9ixJ+ZcZ4/CpRb0cbyXzZc8ogkbXPgpAVHhyW25SA7Dx/XMQsIKY66zNxsUMtOg8L7+8gwfMuPzgPP7zfdyMsENfa8zxAFVe8tWgxXWSe/EPzdtRTkHtFd/0Gp8thGq89+W55/944y0zokdQsGwBWZSmcp1YSnI/JsGV3Dj5E1v0eCrZdfcG2Dizsox4zBEo2chFZbwdgxoOQYPizqQ9AdvAudjJ5SCRfghzno8dqh20C67TeQ7U91QsaGCGqbRrVtABnfuZFQ+/eSZcQTDUGuQGHXPq9u5KMf+WJ9HnQqV7mTvhTwizVe4nsoZqMP7/SfLU5FuckbW4yQtUnhPz/Xt1OI109D1mfF16DdnLjSBVcPRC2rZURO6x8m2PDFhkfdupn0+bczOZmnyXmPXqgJOJTf9cnsAnSdOHe5VGrXoQgZ5AAW5oLm9roivy1IUJLLbi00+Oihy4Dx0xUeo7yaSbQfcd0IFlbwmilTzzIC6N4TkiqBJsE+gcerCqvN/L8J7dC0dX87TZo/WdYcMLtK+xRyo8trZK9nSLkR4DtkS+M/IC0cctVOUt9mHv1scEzRODBPfZdXQOv7mJaJkKSCzaffATvcvKshyQ4UW1APiEFD9FEOT7q565s17wcQuHnmekf45WE8fiDbEnXBy/V7sVo6QlvcO2FuAM2aFlV9wTPdJtkdRZzmi+uhuIPxhAqEwFeAZRe98hY0I5nJ34EM7ZpOSQR3znd23yNe+I55Sq27HaHL+hNpYAKZ0/Xcg9Zd4CuDbk9lKOmcHY93qoM+ISBs0JR7T3+yEWP3UOEiYccT5HulMYsqpS2xSfVgsq5AUmuRNiApiYCb3Fv4cnuNaTPnvNsXWNQMxeqmOtUqB8a39LWQuA7x4m+Pi1YlXvdWo3RfKb3ac3tiABYz4/mk7YrEC9iMfq7NfWAc92eMcV5+lr0RbvMIrGs7BWFwYAa9xWAjdI8RmR9yGA0UOs8abTlr8f1Bbdj21ZG3Ah3ESTO3FjrxTdSEskoMiy8nrrcw/bkLM4M8YCeIPLpD5w6gnLzdGkC+HyXjVRFmnW/TcVxnQQJNW6LsazvJpoBL7Md+R1xQrzcWZ1po1HZlHC2E8lsZv5d/SGioAMwEBdYStptqe9I/9dqHpSPo/1mqj9FtZD7F146fLB1UIBBJ6azCfgey8WGPulC0O5RXmIKpyW8vBzQgdktTCW7GaxlVcVKv1YhuoY3DO+jZQC5Fr6pILhlNYkVEsplPPnAy8dlB8GUTdLAyqlICtqEllsnzWaGttpkRimQTqOla0TGaCXLZWzBC9BQ3KQYvoNa8K3faa7amawffUriqR4h4KMOFgTurK2t17qUrfvHRXynT8xmgaHJ1Yo2WfMTgs79YDbonIVKqpb1Jfogk4bDbvnLHnDuQVnjCzj+XWliDp9+00gd4SvDYLYcu/nADJcvspoeT2410stJkBZNDze99JgBzBeyCQ04xK0CjxGA5ce84CQlaWpzN+0lVIfoF3rsY0avkxCX3MM44SfuIuGxCHQKaXfl5heLu7nAnnzg6Mu+dYE7dPj2+vqOoP/83AXB3UumskHN3imTHWsJpqy5V4Pbo5R9DAPb2WhNhsqLmcWuuvoSrlKKCeg6QpLq4XrUMl+c6Fzg6311w+x/l55GVt1nuGOIGk7FFR34M7F+6gWq6KACVbw3nt4fTC4RAazTyfrWzcA2ikYveb9j2DlD9h2bIKAoYw+Or2cXmrZRUlZTXIDSSjTp3V3U/vnx+uB97RZce3wlq9i96AxfaMdsmvinLBnoMAOyX/JJVvEyXo0v1L93KPRaayXgcw/lMF5/jdc5VlKda/qNl1KvdAyn/aoW/zqx5x4V7uApzVBcY/Ge3pF36+LqaSHxGLU66Q/2C55sh2q/uzjuEWl13tnRQfEgE8h3PhTe37IQ93CMl+rb4svD2RDYKZFrcIMcy6SbZ7Xb7WjszVmD4JCtAuledITH1t3LgU5V2vGiPOgkWwGC6z/ppdDm6znfToidnroNd6aXPLyAa3lFyvB2hIhRGICaARDSntRnaXJtI/tROSaOh/BBdLs2kYIKXp6u5NS1wKA1q/xH5zJOiBC80O0f25taRKbe8MTJE3wY42kPE+AwX7HH/TA/cqvp8wO3GkgkA/6ZseJ5yox8u+5Lhs0hsiGi9FyY5VmEsZWRB1pnbT7+T+2U2N9LRPdPp9qsowVylq5z2ypUpVoxQIyh3KRu0HcFtScctdP8JyG3ZOGgrx9v5ACXk1v9nTHCPxWrb0OldvwriddbnWUJhJnfW16F7aJ2CTw9OXuPcnJVSmS6hrFMS8AY3LtXKKkH8YDTfGjHGjoxa2VObyPJU2jFb1vN1tpwaME2FzJCS3LGdv/vyyX6DK3igkQqo2jOZERqUm521pcPzmNxHdOcHoZS8rnwNbc1VYsuFL3xNuMqqKtvQfxXe+mzA+EyQwyMYVARTZdTQmfC/HKCT0PsjRW/5vlyEGYMQLQVLudlsv/rveGjtPjp1ZwrseeLG/rHJCRwAJ3Xdc1Kwx3z2/Y9EQM9cIQqmSo4ZXsC/cWthBJq1IF3PxSlY/nogPAoVNUMzCiNPbdkrKhSd5ap8ytAyegWmAXM/MLlMMTOIK9kBe9CkALOS7Tdq4Rcx3YamNn4twJsRXMuh5CjGDwR4XwWB87KtSAnZNBUOuBzdxZCUI6MHC2aMBeJd2awu16NY/OeAaIxloI5M6yfaeVJ9Vf9bLjHXlVmto4s9Ra9c8ZxnIEmHDG+FHGMPTwAkUw2c32Tv00fsVAVKftmd+dKBj/3ZW76B+Dyf1KermtPzNevnA7Ah3F53rkiRkR8=8R2MCJqlz17jDyUJ5F3wMQC8nMPHBuMViurk7/wue1q41SFG+kKKnT7KB5BLN8JZT9Tf/yER+5/+An4u7Xo/As4A3MiJ5M+k5DhYY+A0ZSjGQSQHNn4CTA0/tBr/K0O8AlmrpFL5HEPZOxv34RwNT0QEP/NLwxX+yUp+j+twBzOQEbO30N1HvNSdISqCSJRTTMZo/alWjPWLx73p2XsGMxSPfgsSVFLXoxZXsOmAJKiQY1fEHdrjXiyXH/IB/ikSMPQCNkmSXov23gEaviYys1PTDvyXFB987BFAFmxdqAg0yzB60FyTNaqy9rRDUz+bAfwlialAlIDL1bpqEUqkg/uXHBR8v7WvoDcUTmZyuAOMK7W9/MPw4lHCqSJiXRggSEdu54S0FU6Z1WswLNMZL0rt+HUK3OtkcHLXo4nxGMuSk1GE8mf/OsmevL1VObhpvWHoULfgwjndsE9xmgE+RTa+y5GRaK+U6yKr3KrJ1iRyJkPKaKPuDK9iWO+DUWIS0DuZ7HQLm0nFqTOj29AlAE3I9NnIVbRKPfJTL54SCAzRpx9+1oWZ+EgVMQ5/yPk2Mr2MCT7Y6gF77eUj+NZ6nM3uahrW5kAoGKVdMfOlfo6Qeen87OM9LeGQ49lQhIfJ1HBjT6ZiiCD1RRHQGktQC1uUTGJB1/FHSQDohirIrsmUBvfpYfEU/Bl+ajTBNyA9QMu0zIAF/n4KcWo78LPnEdLJvR3aO2etY7Raj0w0nhYsRNYr1IwTSp4qc19IWz6KQJ1P9xai2/BPetXMlPpZFQLym0EeeggIwgZZHjZxshzB3kWiy4OgkpGiFEv59GY5MEIW6VtTwFu7D7RuT3JNSa8Ntr7HIZctEEIYQRMFtUEV23TuRTALXMyTl6HAU6uysv75rkLUXOjvAMUcUK3fRIDa6mS5mR54XAl57KVLv3LbHLKk9vwKo6crEC1NlLG4I76WyqRPMRxJTmwSYOcN86r4x0jLB2/TkwG99gYyoOHCKqdw0UM7neNqFonibU3mp0o0sVlAEBitvIVKQ0BtSYvgV1YbERZ2c/rMcKniWP4LjpZ2k0Yg9Fn0nQwUd1dWoBHugbTn6QNjwo7BIRZB1lLkVON7SOhFcMNpDnoUphrggf6zhQnQNpbcqQSL/pU5eorA1qzccLgNKI1qhfZLGsg2Ns0fbh/LTIWwPkylctCcRKx1wHIaAZ8s8k4sDU6Fo0szPvFaHa/d6L547lY8vdOxrYQaRmyTEQ/pYTyQKQHA6xh1hYgin6kNbOC96CIz7ISMSYGUmMTrcoaqOtSSMe27zMrMhSOJ3/tLK7qLlo8Qcs8GFRM9gtKuSYjIgFJPMpbJ9tIZRXvlEra+6ow5LYJUhR5FAWlCrCQ81PIprD8TPVkboE992mtRx6wHXfPohmtMCUusLm9TyV+6pvG35XZ58P0/pdAc1d/tK3LrTFQAHEjialdclGYoPhB2Kn+6+V6xbyOwgU0UxCoPzJmyxmSLqG9/pXip0GiXlF+DrEk9F/EMRDAmKn53vXa82h2WjvQPQRM0mVYYH+ArrlbUt+m1tFFq3xzA2dkpyYCbCU58sq3bQe/mJdiqZd4KeFnMzMa1B3beDatUpy7BpPX/tTN7aJRnKpb02dcs6t3Dp9KawFkJDlzFAri3feOE1uz/ZLrV6qngW159gROKomrGmWVpwQx5bgswErC+zgX0+oigKjEbLEwpNzQaLw8YX5lBwLCQvFXscFeXUme4BN8s7qNCcGDfWvh0wjY1gXiDCEjykP0aZUA63E0tBLVA5whQtmygZe+RoD6GGN4Gost2tjqMme2L9v1liyqFUKiXUSeZlalVE+pSRaO3HBPydPgqSHAY83siqIUSh7tG9GKo6XJi/czELVQQmvKGo26DaIrcPRX5aCwh2yt8NIxMsOFxdpm+5zzG2kIiuFcK4OudeV3QMALjjSoplwpaS4FXSDSKoft8Hqmic1Jqp5LqqcNgYeDvtfT5HU4wvBnrhzInSH7VZUvNb/5oZoV0uSY66Jb8yRVYj7BYi5RysaP8K+KXjeTxXfct3FLPUx2SYyvt3xCR/rnylFG8EBIE01WYHTcuzpZnD5rB8I7HT3Aqmhp3CSB1AyHnkc1kwVebWnWNKX6RDlDntBkj44tBdpbXxcGzSzEfS4sYJdnXiimzrYqoPdB2ZxZWGU6YkS+N8zYnI6oyAycyU3p35wCb+7LZspuiJnI+G1BD1wWT4zNWJBVmZgDAZKrg8lpgbpBPJq1QggOPI6GS58RUXNvJqA65TG16OL3tqyR9C6cXFIVgdTXSyLs3OtoZY5pOM7SC6YR/fwjgEnX5/Kh+xf1aUOJHgApGGEg/m6R/xySHh1AtT98sMfZQjivBRYpxI4cOSpyqun3cBuP6J6a7QvaE4UNF2lacz6oJeztQKFRd4vMewBdiCWzgwMLyuzktpIG1ognerjELbUm5Z/xlB8vvYoOf+lvngFSCxIRbaNciVeNbrvthrE1ao3QwSi/uKC67WtD3WRZ6vQObA6lppoP6QnrsR3qbVgGVm+RSpQiLwLaIuWKMQFjDHKZzcAxP6mSgnW/rQ+ybA/IBuOB8ZkvWZTUy3s0iLAakO0vsakNqOSH5TJGV9TvDBVhcMMQ3WJlVl+fVRM6x38q4cI8ASG8pD0zQ5X76IAAogQ1pTrFDRD9WHl5H397p70CPqMxEJvqC9kNKV1gJbJ/A/amh+6t07k4gfo/Tc+yh7ROM33EDyQXHGgI00wf5S1VXAYiFfbGJFOnOgIGWSv469KOtRg5pSf2GFVrN0Qt6za3wPQC2iJ/41W8iNOCm6gb3Nbq7Y8UUJlsFrdWDHWMgcubB9qIgS2VwqkkArq+HJGhomU0cwCyvgQD+fMqw8LtXLD6/Ut2tBQlp6wzC0pFNmTC17GZyq3q15ELjMSF3IpM2pMYs53iEeq5COcAHAPJaxMVZWYrAobYZtlBD3FcysNEMsYlnxxfDFpo1wpGfGM+rt3pzUxELkFnrVskXMufAYXaypLF6qy3YGjhG+L246xdHjJc7CmBXrNwT1mDPwdvbgLfJJ8uJKpnmhNgOnMH9exmWfQJ9y55Z0BYC4xjZqkJG3Ua2St3KvqIPbeox/w6JrfLVXkupFm4dOp2fpF/3ehI+AqI3RyBk+e9saiW+t7fNGF0FU6UszaYvpbhr/lEb/evINKWy0jS0fCa/UIWoexHPMNLNfAUFS8yjOGU6m+QF1U/0IIK95JM8LebruxnXmomH5skY2pzkP2/w7ZLu0MGJxvPp3mu5G3No3AtropOiIzF+zKTnVSGC74k+yEyqVMCInE9HwfEpKyLUE7xOrogbdNTgOHzOlWTb61diqnfyvLIbCac7hQ+w3+g0eyVSIE1nZwmtNVDncYDjoPVMXoARRKWWkopZwN1qXrxzGu2e6muGB+eKcavOSuAgTR1tOh47yFZayPWZURSHlSSvVPUCxrZQDjC/u7Fr13VjrUV44/GpeAiwqSeVgqn61CxJ0XUaUvvN7zf9TqvCgWZowIvFw9KnpL8+fhQFpUNjHYULIWBc5Sn+XrRBkqjk130DL7tgg09c0NOpXnVGthD8DVVmCQQcgDLQW/bBFOviV4eyrilQBXOYZuz61K7ZXV3NCvUtKlqZgMP2AOeCzij5ffSSB/mcYimKTlvsGbvtp+W/iUTz+cElmQ4RKewbEQo2BHalzlCS2mTbrW4I+/1+aJPyH5QOAKbplmmqQNTQSXFUX7OUwBWJfMy5BP1/wjOfeILRRAk28Pe3+ENlhNPXrEyOxbZQQYNbzf7eiEhXAGTtvkHOa0wrehZvVdsqw8co6GW9fxFsa7Oj9rZvZqgEfJcUxEtHXEubHnUwLtiGRN3Kq2sbvIVoO5haOf7gXODW7SgAbtYcWO7n2w0B0KPNniLmCv1pPq6w/bpKq2wcPdpmzWEvT0YaIDX6/jWmrINSM1toPe/H7WcKN9wwvnwZEHgw8h++ZP4/LhjTp9Xax8qSrMwweY6d/TNRw66p+n+1DnadGKO78prlcNA+RABjdYaNAm9Y3wS7144NnkSpmTln3MK/zAVXFFW7OIqYMPQJhs2XYWhJZqxbaeqmYrX2NqTIZ2e83z8ELGQkqODuHFbthkZh8z1PndDVNMtXTLG5TVvfKNDtVVMIsx/lifRYwOM26+6A1HeEd6j7cfutWRWzVjgNu68Jx8fum+jRvzke9peRTyYLUpdH4f42upJtlyZOAQgdZ038hwyjWg52ACTwBb//FDslSLjR/JpiLhyIlPcNXW2rVKDN7ip8NEi2CffUUZl3pU7EKHrk7eWZkZwNMZQ1g0nEd+AaqhDIMFes9MApdA0sRuJpH1sM8iUhtQiwIrV5hJkgZCkfrh+Oe9DjjS9qYOUU+Q03tnD5KLvULXjMgdvZ1QDCIuqGjGat82FVdUG0tQuHoohoQNmodzP0SV2kpD80XyRRL2Pv0VLzAbLHEl9f9Zaqjnm3fgwwJ9H/GdbieSwoktxym19gaqgTyBYreamGGpYliIndJZY2TrJZyTcLZhfkhhD0S4JTiy7SsHH8F2G0I3vhME2eCGpCAw5C2ZvsSuRw2YQBW8nDt90sNkkOYllt1nyKfrji2ZKeXUVIimsBXaRACu5AF3g6ppXMFUXPcFrAbolDy9xrJ66dAhqVeKHAm5qnhQBcN4vUnTwYR+BhVFvIQUfeZC8AYNjNCBPaazOuwsfKJ0zUV67uxtqqwGmvhabc0POmNAT6AtMGM8trUq4S3PznsdH+atVm4PhcNLeBuArw+C/yOFnMdWp2b5vvW/u08Ods0P54OArI1SGhyOjzfwk2XYLqn92BA88XEiCa6iLoWRuz4PC0NZCvOTxS9ET4PXYEOVw96MztCMCHUf2sGoBr/jN7Xvs0Rax4tpb64LmJoueejJ9DWmLVQX/pRid+hRgSahQLv0xYCGRVt8XrsHmXtnecxX8M1R2LLSWHGyGKcLouk3KQsXbPRwABRnZ73G+jiIplRAlns2BO3RVLd5W70QbLnwgxXJLWap8fPPuwVGJk/ezrpyi1IYVYm8HFsd/IP1pucbmrG4bn143vibTd6c5VA3dyn6YYbVuOCQ6HFeP4/GlJ1ZBwXbH6jy61ZL54tb7sRIAxJEogc+LESW1Lj33V1GgYAkHPo1u3oATbp73Aa6sv0KwaSHGB0Z2+WNMP4rlCGZZz3r9xS7AJZ1DEfZrYNCV+9rKl2+IngwbFpo23YbLuM67M40Ck9Iq6LcmX+NcyOU4l+5MKU7wodSarvF+j95xZVfcNjzXgg7uBAMIwIObN+XH/uN6hq8OmZlIZn/vji3sHjKSqcB4kwgzpVr5uCTuJRmcgn2WTkYslI1sdWQ2F9hmIeNbnGTbo27mQ6vVQ/ClpMQV3f6EGM5Rhii66b4koKvzgU48W5jun1YwmrwTYI5JGBK3hrZsXIjYDBVIN2xmwU8MlHAK64mowtPd3ZLQXOtoBmxntuvYePZuBTjwlV7WG9wSPGkjnn44vkERFkOeh39Tmdgj7djm4tipQyDj1wYrcomrBmCGixFglYICOkee1UDc2KBaoC0PIXLTvcxw/9GhOVMMbEHe4XJRphRwPQuZ2MHHhTua/7poFz+evYxLAARTQg3c7wCIN1p4XWlsDDbn4O1u/w4+QFygmM/ulGRx6o5sr8gv4VMrt9USfcGJzfJLXgu08EWHPBDA4yY6QR5X4EWpNdrmqTGpiEAHz7gF5f5Z1uDMazXh2D3RurT7yJhAW+aCZOepmx6zyOZjHWd5Us5f/ypj54vWMyizTN9cI4pkyo2oqzDp4Uv9Xw4A4TEt6+MoQ3ToMyutTd1gzA4DiAitq18DQCWjDafjs0RxnlJuVsKT5O696+K57lYb0eusllCDzlavU0lXcHZBeJ37/ZXQjp8Zaj9smDv/QRvGzVtYM0wWMSzoGoRK/3JNoCmFECjP4mYUXqvyFBVx9vGzSuHEsM/o0PBn9wMDVr5rKbjHupClOKeZjBLqSGCNj4+78L0cE96imnarmfuEpDFgl74lHnGYOWgQqfHItBWhCH9vblBADQ2AbyIBCo58PVS9E9LZEUkA7T6t0SPRSpPctDP+1s5Or/T0FuYpVA+AkTRNoUmam5hHji6gg/zvHqZBgPNnOPOjKoz+xUOEDsHnls/zezFQoeSUIsbmt7Kudcd1t/MErPDsNp6MKM1Yms2mECJJqfdsz7DLMo0Gi0pVRM1Rda5sjedZ+HISD04w8ygzEc46KGqDyfFxqGIVSnAR5O+JWPwXDJE1GuXe1FMxUIHuYKQ/VIEexV1Ou+OK8auskUJSZsPWFPbwPVhI2mV9dnCQk1DdnITIt9Qes7f7ccF1D1N7O4dykmc/vpyxgnXFaxr4prihMoXMwlkNxIjlYIrEzXicXAJ1MsjsBhiQWd0752X40G2e6YtxPTWx5hpMDXc7jlBCdJFd9ss3RJiU/aOLbC67S7t7ohzBY6WMwQZYilrWWUpTWqkjyFm8dq9VcOFWJqyEJwTQQ+HzTsV9BCh5tN5UOqX8H7J1Cf9Tld1b7HtjyTriGcPMQ7OQgkWd9jmrdNVSUKJdU2gIWuSUFIT6Cd4xZ4q/bOsyAnK6aUuVOrtMttksBgvRia6acCiGDUL3bICzzBiUv+Pl6pKOgnbLUSVnqqWpgrUMHdSLXghT1wapGwfiMN7Y2VFUevXiC/mm9tFE73LEbMxUkWYsNqPn6B6d4Ab64m4lDu4sQyUqw8RESfXIyXoCFv9y9gh1UzLaVeQOS8EUhLZzkbNRMtUwMUte/NnpDeICmCpbDjx8O7XINoEAsgFxVGJViz6khEeJ+fVPtIpXaa2ggF5M+BIlwHFV/4AxwB1Po8AE/PmEJYMLumWcnp3A/FGgRKD+Yb32MsfpZWrZihNrOKIDsbVAHI+4eFm21eGgFP/ovP0ufAYQbqX2RptBQMwsPDPfSCI6xqV0uLAM+8FHB0khbL+uYu8abtSODSJ3U7s7rh9ZJ2TSmG4JM9w/FkXtN5odCOR5bN2vx8B+CzJ1eAlxJrOWO+b5FcQIkDFVUYJe+wBcKHBRPK6Nwois6fYCJJH4c8sw7d4WYJ2Tof7kQ1K1uItIl5AXEifu6Z6Ce9vMDJ7HTRsUzOY3vUelmvBR5h0kgKN5nASUBWSxDAo2gojuAeOtWSYZKyyY/cQu72/EAzlZK/AttF4mw01M/uq5N34VmBj4yjKYpTdGET8wgVchDu7j4MGz7Kz5Fgra2NZa2cr9oo6to4YEGSH1nN6l3jOjegnEA0QcuqevQL0kQTLnUxJUtH/0IFK+kh9NFLxpsI0/HMGW7N8YRNVfnu/sLM06iWNqQv6JFZeGlIKyw4J24UVIiS4hQCO5SYqrjwiiij7ajdQxiNoSPnAIjz+Jo/NSSBJi4nLtcaLJXtSlh011SwDF2/bv84+TPeFLOtRgoD+ez4pExrMdre9ycIxxbERXrJ/+F4Moc8BdBSwzNEiNJVzHUr0zIw9o/a5mXwr+kInYzWAMPkhOhTvSUoVopeuXxbpJcuoIotPECK7QCzAlsbe8iG0r9e/wCEm7/fcPBtb1ZvLksCGwSkeYCfFIre6lC1FKQoFgKLrf8RLIqDqRh4HjuEiDP4BpDXUz2mWWgtm72ZB6IkzNEMxPQO6DoxkuYNePW27jnKF2s7ChQt7UViRrewPuP73wfWqrL7z5FylebcyCHz5DIOg4x9cxHgehwvW8b4wKs6AF5sL3B/RuoEurSPBpW3oY5VRBW1yicWKcaGIIo4f4rQove4PPVt0tQQ4YOmnvpcQAJCebgxPPunc8ijczDWxa0cmFQvDP7nTYwJTlT+sCmmIsDkOajfrOZGYpnj2lZ1z+525j7WHPaKfYyfiy5cXHulJUTmN1JQQ9Inl2RGyrhNrdYKZGfHPD+2oXSWDHuHTvIh8p1T9EBY7o/W6QGXyk2IjF69xoUMfnnu7Ct/F60Fef9X017sf6koWy5/rPN2Ee98+vjDBI4mJFdoIgHGlnCURUQwh8iH6eol990JhHor15QRd6SzYOhN2fTsk6rMY1IQoLV8X7xif19oAXVZCVCv9lYuA4fBO3V/RlczQGp+blTdbrzS36Y7rsFWLIT1LuWYmVE3fXq2tEvLc7DxE/FoqW61GOAOgurKld5TL2uBzOke3D9rlZnxXKf5gS6KVDy/9G4Uuitv7BmGU2tcNLmPZnEBLgAv4jLr9UeiBh6ekAVRB33wJHsmtEZLa9xVpZFmstjyaWeMJtVfjpQwMruvmQTQhRT7Xguz0VLzY2Cu0gi4cvWaMrybyYT/p06ehbkT9tWk9qIOg7+vnP2GLlRXJZgDIb1YL0vCAkb/eTm5nM79G/lnN71VW+UiBy4sLbC1A+1qoVKnBq5aO9YvjgZPYFww0P7sD3UUkn60o3Tbhq8HnZB/lcoVlpKyqSBjqQoEEJFjd1ebG8STqRRYraFNz2YvSNPqPFkzbEVDtmM8ySvm5wqShLhFuD2DOuXjydMuN4v5M7t+xVVXGuMVsNnvbzUPiXX55itGxPYFo9cqgLF6FsZei2v/xvoj0Ocjg6jtu6xMKNnq1RsboimSpH6vyTuC30uy+Snr1+ugQv2+cBVy29FlGFybW9HSvlxL5xkaST7LOl0vIleYr3yy3yXtID/iOJ6r+Dawvqw5tt69/JhZWmBiSQzfVnKUBoZzIzjqbq5pM8dNM81w5cakXfFxgNXca+ziH67/C948amjYue+5Mis5YetCN+tmY5Dz9d6Q0rQWEF9+ceiALojD0+NwjpYrj7GLa0DV2feKHhnilIaJSr4qCd1WKYfVSiP7aDqJUuM14Yq6mtfAURTmv3tNdlSo6lovs875kiPs/yBs6bGGx1G/DSzoGMCdRT+QPcAiazWheHIpx8WLN1ouYMnEhbP49LiLNtaIshGxLBQBeusoCLeEhyA3McPh5gPajnhsqgN6kkX3b07aqY1gBdCKYZc1pszGwQ5A5J537yLptnA8jsUcYZ+eHjfASrdj4gU10XGTesC33iMZfS/gryVov608x4WqmDNzN7/vqR9G8OSlRtJ8n6UNbb3989iKXbWhIXiEEd7onUMUGYfBFTkUQnKwqJkINxzS4XX9UQRvIYRjU+hzRKNvMv/s0D7OLeht/yl4WGzcq8bexx4/NHzBo0dMSpwbVKC2clAQPLTyibwIhJM+5Ee1vXBcf5m9SC+GrFj0f6rUG7UCLJ5aRLaiaw1T2xXHnH6y4SqTmqRbcPfz2Q4BG6pWO+J5enOi4LEdWDavcJ3ovynRc95X4bVR5Yv76LaI7LGi2idaeaB2v9sNKMlMGT+qvRo5fGhNk7ZYx2IsAXVl3TIiiCFMD/npE/lUUc5pfHQQHFRVl4cQv71vu4xLAR6vfP/wm1IpkOVLye8ToMCCS4uH3dOLC22u1+PwsD/OX1O9rrS2s5pgbjqf/OF6VfN2Q/j6q3CFE6wEX2vNmyksm4KKtGRGGJW+AgH7qjgQB66+H5JOxLKxoEPVzQl4BvhkSb8A2h49kQATA0nLsKmMcCZcJ8HO5c8lckJ+EawsRY6FYVIGapGMM5FEeM5BL6f5QaT1WTPNUcOXZAnNbt8TL1R89waVLRTNGkPsDCQii4l3PlTw9o1rGmtB0kyoW9YCyCZ48n7RE0FiixT2PEM++Qzbz1hr9coUEJc/Y0RdSdt1t8klPB9UXi4DSL8LfejnGvEK4xrSZZT4gyJ6s3fUYnLho+yClGUznddpYxhDafIeB9dBT+3F1YSzg/oEp9gfNlaXNZTljJrVVs8uvjgYQ9zSYIA3OOtIPVx6Y2qEFodgHMYQ8S/O51S8wv/itYg0585M1TO9OjTy17z0lPiJeEbqzb3MFfltuyZH3nwKgXNUmVHbgSdrSi3z9ZZt/sslcTlrUIiW7d1hzgMy8n/8Hg4bugYJ0MopXNfYHyOu/p41XqcAKGLLtc/8lWqYUZQ5+Ou0KDd+5iTvk4EHPklaFanoXZMY+sHs0MLqh+PEf9cx3CJ2CSMiiVXShwm2+S6KDo42+U+aRCBslc/V6z+YH5twPn51rPh5Ltt+ZRWVqtLgv7ygoWHEtG01ptbDPZAS7oJ+X6GmduIZPJD5TQRhbqVmZSL32IbFAg4Bokcjh5O3dvsyCqnbc7Lq28btcYEIlCzzHJc5b0FHupOley8Ivu3/n/Ryhg9dYKVYwnBysRfPZ9v1cYIWCRkqzD3L3vv0WX2FuzSK6ER8itBGW5HI4FfdoGgo4fmvB8+EBvhmjcU+j3Gw9UaWMjhYMRtZsB9+Ipm4HbG+zE8otPl4D9NcGC7pwaOVSS5kce3JDaj7fnYpJ5bStWu9sCPeLaYs3jY0r3kt4xYUPkuSjihz3JuEEfFfQgyNELpT1+7kz3DS8ZXyMBUEs9olRqpOokadOVfFsj2vk3Z6uYpOFUOKpNWGHd9QmlFhleKv1fjOG4FYHqwtI5/Q2MHMTE+Yi9baZSzFWfnGaVXb4hoKDKYiSinw/b34d2hSTTo0XNhftyMMT/2+cPupwC9r5/FrjQgSe0IX4DvdSHOhtsqb4DaXeN156hL9jgiVQmtQSLpwgV7zd1utA1gZYnc42mllwbyjuACAcD1ByZ0qWupt6QxxWZMMsIQxXuIhkXDCDEAWxFkdn/mbKiQ4eJRuxJzU0JfTtBqef9Sc2012VzoDyTDlv5AkUUPDnprQhA8NZ+KmjlWow7g6v+1a1crpVIV6uoyUpv0hKuKTC475iEaJIUazJPNzLWeZygiS5OsIcUSRb+balXYlAnn92ocVwWbkcRyarnpKA/pa0hZ240BaHhmsPt9/r7EFfJeXvtW9ielDDkj5BU8Sj/olcjuRq45Fz5DPQIg9nm0OpC29sbnbHj62q90ZbHYsYkxkRzMlAtpking2rPeKcVcPWwJe6Djeat5GtY=SFigSxeBYQzD/tPavBpZ7yZ+tpiT0lZrWKXieh0LAFHCFb6d/bUd2BhvZY84OmtXQf6uz62Oe+jqB62PJISegLm9DQyxeL2rcnA/NxydpF3YNDHfZRYEmhL6/ipsYQEfxVuD3EOQT5vIxS4umDW8KVfBP9XJVT/p/pfYhtIza7wjICGahnNiW3wEaCjeqhl5GLlk/8XXEOC3wMRVN1DcPbwC9gTpV2cmcUbXHzDTkOEo3rhSdgBKCUoi9ftRDsCeLYPtiF1ZVh6260cbzd9pRosDb+PHJvlyBNofQzeXkZx62UjCkEe114FBT9tAmn6R+nalVsEzyqWiir4fTiMYtecuudKtfkoYS27pD1mHVgXVlZlGhfbpJ8RQWIw/aSWsK2fQiUga+bgqt1p+nwHIFl9ptvP6nnfKnEGfDTnBHUdNwyVfJK6Tamv+81KEyEpz1K0zeKuxyoCo6AT2FXlVUYkEkACHsNm+Rw0p6aoqq8ZjgAblo3vq/i3Qbrcnzy8VuFNLtuYlgH42Z3G61sXFleMMZuukEnRRKefdo24FrXvFCj0+1F/RuokF5E+gMoelh9usQj7ZdZYlCL+d6I6Gu8xDrNJvQWV8spyrRo0EdmLevNUwB+XVeIKpFNXE+dIT11omo1nph6XBdd2Cm0lpg6+zK58kNjoroEcjOEUwpT/hueqxjQ/FyWNvTRRkxUrwG+DFLuBZwLy2kds9q8qPiZVHkvLMGXYlip5QxWjh/2S/tPK+J6hkMoevQiCZcCS+XhzuE7IpdO00waHZQc/USnYj+5N/OVZtKr5YHviXmfzt2J/tv05H80KV+sRRlFLdI++EBBJYkrWYT90KsCXM0eWM6vs9RmM1Zb5a9szvptgY9NnUToRLnTcvoFqvDQPMD8JKdm3UqOp8mZSg+Embc+rnI/c/evdt8W97slep3foLcyJ0dETblXqbyqxRrovaSb3ezDpTwDD91SiTW6t9rbevnr3c8xO+/efZfzogSUlE8uH8ioBwUW8Qiw5CsaoOeAYk7PsjRORcgxrm4uzLL9QzYnZMnDAJ4+C6V28VLQDAzfL5p14KSSjm8YXjWHBMwjabExCq9mOwbmfikfo4uXQz5VcSZmLxhBox6FebFWJrVn97HxZ48brbRomluCKnomQ+2RHB0MN40Bn5inG2RR32ITcODlhwvGM37IVBDSR/QSi+VmgUvgWF6nrzuzq4waIMSwTVs36dhBHTr/6TmKcWmodq4dOlARl3ZRW6P/Dz3CBNge3bjUVH0h/0dAFvk2laK/JVKT+97qrZOeL5jpmZEDYP00U0JFWxQ2ZLfkiliKKgT8GRzyuIb7OSpt1VsrsQimvhR5i+iqBcHJnLFmBYhFr0ziCd3/w3oti8DNab9Ic7LfIuuL43I1PqQC8XTBv261MRkl8EekLT6cK0nsKwuFcWOSYNuyCpjLQtWIJgKaqqiLVBqzt8RLXRS3KtXpa+mvC/AYyOMktIOLxPyCO4L/25W0jsaqNgLJZZKxXfcGxlqgpifurDv3eiFvYrHLmpswe8Zl7W2dm3e0PTXbjpBDeOcpt0UlF13my53oAE5apZN8zqQXjPlcPYu1q3pzohvVoSnjHpBOvHs6SrktrCmvaP0U8/SmxsNTNpODxA6d+wgFIWz5MJC7jGZ0Bau+gaii+SGGBnpNvcECeM3oDY2KJdjREP+CVSwwAi+cSsyIWQDPKJ122VUlUmjwNQZeOTEe/1x05z06Rj9VV71W+WiE54MxCWNnmvaXoKng2ZiF1sLm+vJ4IEruVIJIZSH8h2lqZsuys1yE8RsYTbUJfmkW1kw7TTigYMMZsSJPyocPx4zRbuBNb1uFg2nLKmAJozwibTqVdafg5NPn/RBKhnp6kHrLCtqurtIjm0HRZm2xqpBIAYgVML7Tt+PMPJHwPJGmsvkz8TMC7t956wfKylc9caefaVJAGb5EosLnubhDURH/PL9BhaF1+KOBFyrJ2wD3mqErz2Eu/TbIiXJyDTyurur9YT8eF6DryxlWSNjADb5dy0KuGvlrl5FZtabpzw4e4cI2h94xAzN0vn/TGugO7fIDONGGsbB/n8MhjD1UYKos5bGDrE0wPGeXo5U8eQJjk/Lkcl+x6SovKDiZTw6KnASap8fd0EoZQZrhf4A8Zoa2GH/6a5Ewq8cAlH0uoa+vjC2hx9lt6LhRlZSNz2WWt1yzter2CyN+V7+EDCfaPesr8RoZ2MLacQEygHTDtOpblWfjwovNf/brARHBpyfjRAFHdCCC7rNBJN2WO58geSlTIjWivTJGh/ZybKOTi/yzkPUwREvejFBpkcHRO38v5lnWlIFSLUIKJ53N5xDYpgrsM3lAR1AYn5pCNa+/cNDqSCKhJYnKmMinbRYfI5P/BcZSjSiOQaEnX/LEk0C9ZQ0M2Z6lXd2Pll2C2ra7NHdQAVor+diVgep9tuV4BXspllN92QjkdOR6yLh6Tg0iQmtsOR5OraZwYucBBswGkIC7EViRnRlSqzb5L+8yFXx92rtKwIUJX01IM+PIgZqQu2oxhvhhdsButJFTm2rhI7/+PHH3k0E6DFM63be1ESem6LvDXswONh/Oa/6RUiPmuXUfrVUpvfqsL5BAK7rYEL4FX7+OWmG4Fk36bwTvjZb8r9nJdvvoImiP7p5/w50AjpVUpNV2oRN2eCuEqYF816pu5Wx1TA+szr8eGwzmgnCdA65VQOOUr/3BkLqbAYV5yd+rl8hN2nlR45ySjbw8zcOVKzTTLJetGy01afe/1zNVK0W43TpxQyNpGbkSSOd31aWJrcZfFDzhYQDS8B7u5uw1nFXBxvTaRfDKB6Oc/nDyxM10dF1AdHpc+nhWlni8UGGCGveGfk+ruxGYCXx3qQQPzRhBfMo3SOERy6uAyOF4qXaNTw2I+0xuMqoY0TTEV4/++y9AZqUGW6MkMuyyK0jKvRc4oJu4R8o52O03ORoxDY/Oq7sgAWFgtQpwpT6Nu99dk9uHU3AY0rQP90q7LjboR/KvzTcP1GnwCpMc5PsVKOSr/Gx++LQlY3FAYUf0IcoYB0fATJa6GlxE8mzSfvfyLaRAhMKx+XT9bbis3vuDqEh1aRMQFeFOVp67TUHnXhblmStKoFfiftntEu8wyJHoi5plYQSrdzPQeQiV6XHi0yDHHciz1EXHV5nSe1tBfjFKqlFCa7MxKv7sF0Xo7hj7X0rsQVCZvnvJd3GU+099A+O1j2IrxTHvHHO+bOCInEnUjJ4Kat5qoq8ldBoHabNiaPx5hbKMDoUAsm6gQiUu9AXmDj0yB1OowBXQmWua00nNoTGQEEh1FQkwi5tb5GK6TIgIy37yG9kc6xHNKYv2ll0iPNuOrxrYv5BgxFDjdlGYL52SDMuRkc+GM/WZX2sGp5/eQU/f37ltvIN9zQ+h8s8wXCLLpjbaxFQFYVVc5PuFYuukgVAY6f1E+1d8D/tBtehtSvVoipaPOVoD8ZJcDnckcSTo6tMx27MVMBBpvU8J0rCa6QHgTETUgFMcKromyxgyacyvM7QiSY5ObLnorwauwB7cez8p4isiQCn6kGUqyXmzq1nWMuuPvXyHnRUfeHqjHcX9Za8sNPen0VUmQcKtTrSkdDV6JLMEekKzcgupvNuQcIksKQN/rxwFCmVzQEwSldT3cr1/zjuk04K3JmMvrZV8vV+CeFB3TdhQUaiGaINa2i7E9qO90a9jPc4mWv3EwByomM+NqFTPLpl3OXReHIOUI/wa1DUN6NKuGJaso/JtHAWhWCxeiPQ2g27B8jdau1Qe0NV59rEjpagOzN4+MVGmUYjiu+dVlv2dQBPkKd2Z23tdH2ssS2QetLtMF0oCP1S4KbgpiBlYWbrpvK3nyl3xQkRzrpFvZeqKJzFehAyRKecPqT7ih0ip0LsBkZx1sXyxyaa0EUmFaaajFEj/iaRzGQ+43zPW3+UPGg8pMN+QYRo5XlbG0zbnyUirQkpuIrqmg6Pnq8J2y8a0cwfQzRqsUoBpgThOIJBWlIj4PQPKF6EZzjZUxFxABjo568x/SFzkFi8NGK3B5CTVymmli91i1rsRm9xiAB8erfGJBCi7LJf4n6srUbEDxS/fQnNpKEoCW3/YtJtAhRclkzCEKCLu36N5ouTjArE4f2Imq87gWalOOiQzwe4UthoilgfkPd48/g8A2Xj749eUiEavJU58up+IkWOU+Dbm1uXHttFbV3LBfnpJQasOH9DHvHVRtf7KtFDOOksQsgfjFl91m+lX+5SmU3Dmdx6gL4H9KsZytW+bsiqPxIuQoxTtl/C3sJ+5HQGky5UeQTHR0YDThbOqHX41kzU9D/NVbpx/ZatO0RZC4VB60+1e+KaR28TrkhkE5vYaBroUsTgpoRploPFn82uWxWaQlMbSmAH6VuthyaqsKQuv7yBah6QqdXeJNZJ0xN3e53coB8IgxeN7933b3IKR97bAsBC+kBNw09EFu+jKjMdoZfFJqF4Xx7axJMCMsG8Bd2tsMixYsJT6vHaLxa1454uELgQpwFGc8nc0wncaxR7jNrSdAep0ArCZkvE2wl/2H0ESimSOJWzWO7L9/iop7cCPMLiShgFvUIQiPnXopUjFXerZPRYLY3X2czZHzZ09MD+oIfxOo4cw70EKxC8CYJgKHFiVcD+ON2111wGz/BtPVmck5YTIuTOjLfBq28wt8DWPk+FVxwhmTsXmIgL6xYLh5ijxGO4zzxBUmDpqrHkTx+C2HTYOMFLuHNBVdEOAIr9Rt7aH3PJyW1iECFr3Ye6u4Jpph8fjlkgKwgKtW6UDuqHUObgSH67qKZWS7raCAEF7Zr6z+zAvQB6FRgVHl8CNz7DhKEWtOB2AIntrr6w/XbNtfXDA9mruYsjeYZJ1nWhKsivwX3oQ/cCSpQ6sYcg3pS8IHgDgUjf/VKcQSQE9Sxhnp/9YyHR6jbGTEXARgnyPnGJb26rVU50JSfYq+PorFZktQSWNsjCgnLTtp7H6S8TAGdC2r7/R1OolDhyPfZOLmmwhbkg2bmlwAtcRdqorouMTRhX+9Cedxs9IOwzQ69lBReRMTYvjFyDu0pCoujBRy2jsbZowXWXTXBAXOAofqcLpjcjgN6jPksSPIEE1qEgvVc8CpCWbhFb6Zj2jvN5xZ8NySSJsLT358351tMayzndnVUH4SeVXJoBd9MNi731GormOSXENGhvZo0bwoINN+ta95FjdO6sJqvGJwdac/IfUB2hHsiD5mAnNqtrpZqKRfg1DUhIlyS3+85M5+Ae3LLkxQTfpHWXb4cS5/xl46sLmFPcMkI58/XUSYj/lYyldOi1rXYy290KVKDZi4zQtgHNaHUnJrJ8xa3OwWeTZTeLZStwyK0ixiXMCFSBOhLsfxFzEYAZYOU5ds6OfsrnFP5+yhE5vPeUb4NATfPdz1w0XzlJDtWAs7GySfOAvqGBYZ4m3t4Yklq14PA06ZVw7jN/PZ3J2GDN9p2ccmaVsUmmc//FtoNwRx08IeGfEKrILNw/GfWHIDCkvjAHu409OIcIkR9fxlWz1X4eT0pXdOYIAIHDeqkKH6bYIN11PTIZA2s+72d+hlJ6KCR4qqhucepy+5d75em4TVEtmj5/3L/dDDsq4q/yncuuZpHJ4H+zzU1PFoNpBGCZPp8okrzp1eAaewXythL1T/dIJClsIt+xQFrwo7uh/nq+vTP3AofzOj4J7+D71UIEAbC9r72N6+MbU7Or90QWo0paZQE772FigJWaveCvLBMFIrgKtNFnPlp6UqBmuNtamTKLnlt6OyJsM26QD4ojHXB8Ovn+SjXuv9fw6taunh2xOdqGzvbEZJMwdQ6dlFkvVC3XTTyAHGBWlQpXdBxWq2JR8V02kdT3KE+Wxl8nWEpLH2qywSw24oaQRBQ3n7vNHUPJWD8vSHasU/IdpbB14gxMa2KWqHY4/8uNhL4zkURgmOP1QVRNm3Jtj1UDIB/hLealTkaTK3LsJY9iQ96civ3dm7sx1dHiZq1PJbodspajUhzlN4ikFuFFC+wWsl1ZBlACXLUzPiZxn0FcVLP6mzem4DntAks4UeOdKNylFWdvjCu7rrn0AJiMiZVk+LlUD5x97B0p01v7g7TZgUTsnS1pDZfZfhGrsd5mUP+dJOUMrh5BHumuRLx8O/cHmvS3qKdfGqxHbMmo1/4GPHaDXHSh7WoId36o5xu9SS2obNLkIJ/EN8RdsyaTWDqiEryVhnB3R50MQ+BDTbi5VHnhLsYiF86GeXrUL4MU4vj/ViXHmPnHVi0UYNb3wkTTZ3fQGsqfvCEt22Wva+H438oF1wz3PWnH62Ks1+euJ3aqtSlS93rmciqnfCI5HBElLe0+YoCqNFtKkauOpQJgLEKgfVki2cnGKsZdWsvoqs3x0BjCTE5vhKoOiXYyfIyr3aDyqP29mdoogRveATTw0bPJGUBGZ1KkbglP97qr7+Ma5vU+XWJhgb32OoermOAh2au9QFzJZU109UyJJ5s/3UaVk44AETxLbXFbEFgpJuFWWty1j9iaEiTV2LqV5dLNNrE6pQrW2Nh2bcqwpF1K4StJLGCpIJHHfxaJV3GOoD6HaQjNpHTLJR3rfUwU0VbUsS8sGq6zCj/DKC80HirJ3E6m54iXPPB95PUemTgsVvtmP0GzCwQbEWOo6uUC5M3Vy5ZT1y3qwZhgOX2mlL1DrLOFt5y1Rb6K5bGDgDov1WrcEIxEenv05GV03Oa8KvvSVERttyGJQSGFtJOOi+HR7nCFuWvk/ZogB6vB+MUSYVKC+Di+ba+TL+uYwdwMrurs7unBlS0Fpiz89ZUqMLy+LdfPWUggo2Cn8mPDx/hJaXwLhWXybttm6HjAbedUYSfgfxgcRCf0P37S5b27AdZnlyWhc5fzJ7cc0p0loPM1pD/CfteOeujXmRtBbYvF+4KgiL1xlfshl+uKAwfl621qfO8KDP2QRpl4D1vF35U8HR/GKY31/y09WWyJ6IP3dzFFbRG7jK6OWz4H2IMqto0RsHVLncFLVLJY+A4Dxg6lhi1H+hBXghM1Kfs8W8rrDjBOFbN4tUN3nC4ihJ+a7QepkKkkOHW0oq7wDa/CYwJarZUBTV0/iwXxeWcwW41VpptPkDO5agSufM7UxE5oAcJmdyHOwk6pPhUeyP5Tf8S2yLYsr8daEYaDNTCNIeEkiL2T17nxfY5Bst71sjs57O1eRbAaeRGEhIOQ67Yvi7Oa4IHtob8efdP+ttOq2zFIwS91ZUJrugkSSFh5KxCWYj0Loui4NtL+sb+NKmWlYHPnlLYEQwfbhQEPCyIYw4Bdryn9jWEdtkHggllfeeATpGBH0g1zTaCpmiHRRaOYNjn8Q2EVxwdcHiXxpzsu9H3BiL5y654xb44xR0rOVMKHAlrmtaeKLwj45g4soA+HGQKjOhdd1lmLtrhTZ6JhBGsRCrHkc+XQ6A3yddcxYyA0X1mYQUzHDHDj/LVoOO10lWKs/Ax29ikAOqzUkQYn0Vr1L7HpsmaepAbBleqzma70x45gc2gdkPrSxt1h0DYsEUN10YDGxGkQ5U732VI9/RwlvIg9ssHxBQnkQ2n2LqEzkSXKJN7IsnHGB/q37T5y9qKN+PSIH+wmNCQEIm48HQ5HtEhLjvBzftkjKUSiDuvrauzEGWWYwwCGuQ4yJOyk9CvZKCsWDxH1nL6vdE5GxmdgjhXUYY6e2npHwD22uWMvDbpwOqMh2kiTMBitnX5FoDR7JiwYusmI9waF+BOUJ8OUdjflLt54JQBxd6BlDvJmG4/HNyBcM1VseHVJnaCkSLNP2jLBbtfqwcr4a7sC1DLZtKjn5C7YkCc0hHcqcSO4i3ZF/Cs09sVDv9wMhzN7IGs98olQu78W9qwzhWtiL0M+mKCLlQtjHeTGn3BmTGK8Lb4C44WYPGEQY+bY0KjXgXjT9u37g/Hky23oOUB+qTVTEkwXwBoDEsw2fv19RbzOGaulSSZ5BNZckN8pA2tjPEspMN47DTA9aiCbX306l7BijFKNcT/EdHafsVqBvINJzvpcUuMCmW2iKhqSRrOzkdldueaa7RD9QMt0hcvUB1WdCzqpMox7jk62iBDuem3w+hnzsVdLAvt3zrHjGwW2/9VWvExWmf1cjSoY4ELLvirdk5KWWo9GCo+ZSMbNtu86lSuIRkIbNzoSWbCbDWrWMnM/K0OxFR0KN7MO/2Cm/EPD60Tmhgh5Y8nyuRUih86jV4msieGqrCMsCYg2oui4YLxv1SsiyS6r8w982KAXpOCW/HkPO6WQLUitKVaaEz5yhT+B/w7+KCJfOgTPXS+Wr+taHcfkdGR9VGDTLYEk9J9MGDmHymW51oW/CL7JGax32HQ+h6aSfTxKGi3Xzi/bH5fE6RVijD4A/lNWqcFi3E0Mr3yjrWirz/WkcRNciWSJ5+5Mx0iVKm4XIHM7Tva7oqSeMD07EMlj1GEbk6/bmTewz6EoIK7a394bNmIK3vAu1MEMXC306uaK17k1Z2tviipK/xdw8uHWLCgAkvUTjRcTEyC/v7HZofpAe9HpxAq1s6h/bkLr+12adrQ47cqaJ/LTVSxGy7V4cyFP5smh2xvmmqx50NAlfTPKCBK1LAl7Rn3V3uEfJGFAyr2CVddjkfBu5DI6wwzziNqteCZ7nh41FH5z3NhzeuDGKlK6fISs84JJPCd27TUmmlnq2oAXUNDdl5QlHHCy/1MzYiBfY3KzIBkXEjE1Zq54MH4+oyRt7QDSQNUiffdg8RXDjBrREyIx91ZZkvxLJitFbjEMczUYNrF1PaEOFFuMGGhLg+KncYZIiXAEaFQMLxzuW06O50z1TqqV8VdXDfEv1q9tgpitNEkhdfFcEBO5US3Q4Tuwf/w6xYeMUbhG/Z2hXFRhj1+lHR+A2dsbcB6sqkp+i2Ar6NAPIPYcD+rGkX2lA0vBiqoYQkHGVwURajQD4r8DM4piIkGiherC/HURZF8Wz7CKtat/vSJqmeA6CPCT47dNS+IYrrj8cC7Vu+oMNGPFgl5TizXypp9AnzedW/dUNMz+au3p6h7X/WZ2FmqqKDtc5epvOephVXvMjRgcL+xz/HQ5Wv6CwzZ9v6hK88NBTlxsIqq6dGVc8rhLwbOng+aE2EpwhL7kDdQG1Wt30kFYJFcXsaFev1ohRmWbgxV8crncpCzR5j1XVEVHQ703TWUphmPuyEwFqunwyohnN8ZTwuMkj48VoTG/sI3JiuLEQxbZLblrUplhnSAC5aoAXUu2n5XvTwPtmytQVKQD9EScmW2rjj8NHdsCk3SfvneHrKl2CwA+KF44cWjh8nItyoct6W4q+I9vqxMefnIyckN4NpIgQEE+btlPpu7dRVqqUXDxqRzgsa3EhVyrPm9OG3mauttG+o6qOxClm5b0Kg+QOr7tg7JA7nZtU5asC3VXelhwbCuXo9qR0Xw2jJFrDWFRdL6s+dMBQLI1RfCceJ2ro/B2UhomwYCDpH4V1EAZcLNHzaD/NrJHwSq/5ok15o+txXTant82VsvfmmZqoe2OkFm4VIbrXFd6okoQ4MOQ0ssXXWGTxNuFA1cJaWxg+EuXK3cNo3+E3bgl1yr27pgJuv5smVTS0mcYFcYIUxWdC9ESFd4cZxznjSwyQ4UsNVbabNDP5F1auAxnRduLVHxjH5MBaUY5MRi2dwU2We60kGJo3rmiBLl1rcmeBCYDxourpY8JagOEy+b5We56kEXN9o8FmncQPQxUZNCjqXtFxay3Nycdgo4JcjQX7Qetsw6h7QhNwB0QRZ3ao2cHnVBneKI1/GmlFTHCAcV9lliXSJAGi5MQ3gDZkgjuD6ySRsIKj3TPlJ+doBh0qxC1nx/8mgDUm5X1kiYGIyCEKWJ4NqRy5vunaBjPrVCk+RQCFff4yfxn10EKxEYa44fzvBb8+4lEqIX6HQCX1iXguxn9XrQZppVNq7QnNOnC7K1vT/n0NBNe8LE/698VyPx8R2WZBBK1zwkJL8eCY9CIuCDoe32PeVIqQNljkesNSx/ln88tpj+y04AoP1W7jJ1NTYlBEnCAIzzHWHBTcfxCcaOW/y6z7wsNl4EPmxtxka5nB1cv7uE6lCpqcQrBziw4syQMNo97k00yb3EoMAb0b31gqayQBY8BeNQBTl8lA81MegHjE1ZoUEeCLWGJ2AzQcfE4iFQQachFv+DTAQKsocWn//qQdiE+iY8u+vwMvBTL9spPg6NRWkhFa0AQGkDCA6Oe11SFsmxFd4UNHLJh1EvsOKCIEgVQGWlfP58Bj283FwcGULbXyAxWUrCz2fKW6OO4lP+V5x9DBbc8qH1MlWk1TsEMgHH963CEFOx4F+1OYm/nz61g+6yzPBlfguW549XGCPh2SxGOfQmab1T6ZAbrvBks3IOaO7JPHoVFrUD7iDVrj/ojeadhwKbBFCULMpYg+NaRXCXLmJQ77WVxc/RQSanROijYa0CbZd4bOeeF/8mHP8g5Na3ivtX7E6UtLp7aeE30d9ze4duPcVukwpzZ+BrJyKQ9+tvnE0jYQmqFO8ZO8KqCAybc0Dk31P1aUfgmPNro8bb3eFqhGN4pblXtKsKNkA/cLjVS19DYNHzl7IXGcRqfSsd+5RN5jyZXfBP6qhfEriPwTJnK+rYG+pxyZk4GpOUvmJaCttq5K3xAOLIXKHkmdfYiOcka3MIh+plFqftZZv6KD+tQgEMbNHWJ3qneny3mezDuvkcx2VVZeGpcX36y4Vy9uCqpo+wHNHME0bLvAci8sXkHr3MsmPodS6lOwUVWV3iicO64tmsP9DHbITNMxwrPewGkAyHcIrPDJzjTnbcYbgUNR/S6CiPrZQPduqPPhmykG6ZGHVKqC4A8EBKUFBEOX170pu27OnhiAhg2iQvkMYGkkG8m/SGma9XuAEN5ziy7kkExkcMlHAAapbf18kuduH0wVKvCJI=sSg5hK6bWQK9EHmqnUe8Kk836qjkqsLwEZz7J43SJ6w/ZC3iWKnCUu7fsFAHNejXdvJrS5C24IzK9pBzD8XQL9Jfi53wNurJ1n6z0LZikNnVFJx2CSjxB5zfuhFsuach8RgEMdMGmYyQqVTL70mE2OVA4X1M0TmUc+lvw4BG9gUHJkczaqazzk0ccA0O0xgKeiTOmgjp48DS1BreG3BLi+lZoISaw+qJ+L5c8i/tdHF69OcUkk/mlzmwYeH2J7iJH7cSyFSitx4dFgU0EqaZfeAT9PuLG/h5afeCrLmW8NYuOeVBl0qhBLHQdgS+ac155DlwfBIIk34B7NQwsOc7J1tybaEkoU7+rBCat4+u2uI7XumYAOrJ+5Hi5Y8HOlZF8Is3l6mo/cHf+LbSPzfyEh+QHa6JvGPfbjZOE1gd9pkkc3wjAFnbq6utxqSGTeaLuaJojDq6bZ97X1VWoxhBGOPO4IgZ8c4vTlRrSv07mrc8ZPexAZrklhzmLIsJBULmDQzCQW0nRoqZroYydtF0jgAli+ETCERn2Wxf8cftMRBLQkcCNLeRdAToXcR1Oooci69utkc79a36fBhju25VF3thuLUMpq59391AF/p9fgXh9zPzUyGfoxtW5u25XE+D20GFJYSQ3Vafks1ih2T6nCeGzvsl2nDBCVZG39XPcWEM8q6vlLJoF5PBShzclEXdqvFyAeowaiQOmlJS8HwQB5520csuZFvNifJ1EcEUavmlXZ7ZJLaYbcEwAixEXTDQB9firZnOkyvXI+tkQVQ0gJEcT+QX/yemQBeTFZ6S2M2dgxNFJ5sJ5i2Z0LD1/6ZpD67+HwiqfGcSDK/q+5hkDPf0axLjYJO5G1D0gdbhCM0kvZdSkAfP5MvjD2td68v8sdRh4D7baW3hczJ9utErWMZ/41JYWlpjozNfwftSs3H1gDi/3ZwlCT48OFHwMWISmradvIUBMlEZyah3DQnsjMrPIsEu9kX8yv6KZdvHeZSiSzo2sVbaUybjeRYPIxE0nLExg6V4hOnls3q7BznEt9mThvJ/x1+Ps2EjkuuObLXEQQSkRFt+0+OroPVJe0D7poUquRS6+H5gkwBmcX3Kd8+IfIYbM4KgpyJm27cm+0P/s0kWE5PB5xgpVdeMg3RU1owOfaB45vpeXcbTkP+fsVce6sb6+xt5xlrebdpisVfzttBSAXx/yaWKg9z2RpKbFOnFOKXeP8jcp3aUW3dPjLHf+G5agQp+fcfqDOwZHZndCZJr9nP0gNtV7CNyJ/dS5vUs66fh/j0/VFmonQ3RYiHG06sWJ7lWatB9Q8iIVZKUbMDRd9CoxcxiqRWNkp37TSrDDPw6XGUcLRUnKXbu2B+bfSI6h+XojsHEeE2wfttdLgyvW+kVytUZDW94TwODAjTKGgngZrRRl89MkwnBMX3hSE3mSx8UzqhL7JqTn2wVI6BhZMr2LLKiXhWhTH7/cgpBONdd440NDycjPAEy+Nc+Jq9JET6dSpXMOWArs+/5oxZyeDYLYwXgC6KCZgXc4AmcvI9wXJNpCq56UKda/jY8fNw3V4Swp6KgKIBnz4fML5pUAEZvt76V00J1v6lt3W6xm09dtbaF8f3xQoUCbNdtrsMNu/l0OjTPLQEQ5nWpjso1ubltjQt7Lt4pIUMhncVdLBzFm1u1ojOV6d/abuUezyIpzXsJB7XS1PqEVfQopHypd6WbqwqaovVUd/z1BlHM/N+Vwj9dE/5XDU7zcxamu8bLXmYpcZmAwdUGfbsDyxBXAA5dmIO7NjmlH7WApT64UctCKDLBQopm07ZFydKuZmjZj7m9Fv6FtblRQW9jZJvA215DgI6Ue1CBQlu2RCVUhVtnlUs8aKDwIh3YhydWqFh5XOGj45tLDOqQkmwVZJP83UCHCi386bzZEFukzJBS2xUb03yBBEpluy6B2KbfmsRxfIrEYyzuc0KFB7rYqbMfbmpQl4e6It6Vr3ZpKUgFqjp/a1xjHJuGUbb4H2ASePeATwygTHxGOCQ11NlokComCmn9KNUeL3O6ZvgN34o7kkV9lTv9gSj/Xar32bfUcTR0Ptq+Zc/WBOARzjhU4+Du8SpQ3yC2VsQKXjpHDyI8459Q67EmHP+2J7ymVF4Yc9Sx+BSzg1oAEWYXGvR/D0YEDXdIDc2wTtwg8zBUDeZ3E8Mm8pS//u4CXf7FTzwQl7VjWHfBOETVQSO2m0Lb+RbeNc4ifOSjitYvhr22vbyqI3yDKLzZ83IFWHGcj/OG/Ey4Si3NGZC2jndia81mUq6T50k2u2/QJug/1lZRProeKG3luDwXbkVTgEqKVOej5P8+Nb9m7jRqYAlQy6PSbkzDi9PDN5lkjOIfRWRsvFtjNMnKu9EZxWsiUKiAfwGv9W06M3KnRjkadGpZ6Emc95fGEORrsEOCxwfWvJdSswXL6ytrCCDLTSa9URSuNpuP7XhpDj+87J1+j9raDoVuruq0gGltjk58EffNebhrbucsxK/KB5weeE/1FczevG9KrfOzzlrBEKSOMzBb39DRD+Q+XFZj2jous+EAfGlq/0EuELgAXoYrlOT4Shvi5YkELECgSgNlBsRVzWGcNs6xDp5psQvUhg5SEFHkAqviPa9VZMD8W++Dg5JWxaaEIgQKQ6OK/AmEdA09EKrkbWAn6IbNqAuHhCUxPqpH7Thl44XSoup3Uo0PYgW8Lb4EMR0j0clTklLrQG4WpAXEhGEIuljcsskddVOUK0ZUrXcDW5ollBJYGHk5H65gXBhx+afYJxw4W1v/fNtA51bSwOnnS4byoIr7bbwzgpfTufJAwHM+0xQEX195/sWEnjPOiSVlV/8e3LY/fNqGJ8C+dBjIqkwq1SyXwoczKDwVOeCxsSYXgoayzm1alHRrG+zMEzbrvLvp7g6vwtFLR7TfPvDOu0ZQ7N/I+YtDzXmT5VMIF9OqkcjLKWtv0wAKWjMG0aAfsQ1rqs0Rt6VuemCBuwwxVAw4FWbMqXyTHJb8NHRHBE6H2W644LYo8LYF6MpEPsWDKprwiSFnC2iBsNrEodITIlzdPtFejhcivfFQC5ISNhn+YRAi9ygbL1LwYeF3E4v61tY8bZyqrzFLe1n6bGdpJkGrbcBcjZ3fQln++EBmN34H/zcuz5qmya6wkuCRIA0nH26d16xrRGVB5C+VKI09M3pbelYvvwIIZbXau41xOUYuqBshHNxor65iuouQ2UCVJqZNlNKpu+/PqjPv4HyGdcfI8mSJBEFiqIqx+/fLdr+0WZa2n3BbWYtzgMVjcFpkbEMais3pUl48dL4CXgb1xMAPxghmPnkWASabpR8ZXr709HLS913TVbgBTnyQTMKE76XGfVfWx2sKAy5U2Fgd7HdQx4+OiQP121nayLkFk5AqRvpTBygAPt/X5eZM4qvK3UxqAN1IWjm88BImNxNAdvlNcLrGQYbjfwvheIv0ayF+t/qO7Yv2GWCDzuSq/UEEgD4K6MRuYm8zaFtETIp/RCC0hjja4r5cbTbkcZXAehK4w9zyrlCx0y6wGUOxHOKHZV2DRIneOtG0/1DDf3o+wphY8mmBFN9/twELS4MCtpl9ttNKuRyvTzvYgAbGc1VlNV6bMxzg+42E0UgKL8xEz7UmsHo/ezuw2Jqp1AsGzeuqp3cAPnAY8FrW1WWlbU55lMIr4oAa1E52V2cR59bVpgIVx0HC6AUX1ktlFPO/TiFtIhuN9f2GBJgSsfDEuRe/BO5t9bL1VJ0A5f/wPjwXeD0bOXKa0uX81FhJG5KUc85W8grm3pltH5MkpqHf/vxtThNHzVb7eKItWdvIyb7msOLfrIQnQrHT9zpZ8Z9iuOd4ShD7DwvAuLXEuhQQTGBB94mXAW7yUfTUwcLO9NyTptAAChCenmtJ0N/R13/kuKh/PJRoRfQ3Hvl8N4usmzS+KAWtJZfG7TeWwyJ95eCSudyH/fT/rAI/FvTYQdyOZsfMFYyc7H71gpkdE/Fbg2bdsfbNKjQinjLrUtqyXEl8HEXnaMISjSO1FK1iGdmZIoo6mXCKlSwJbsCufFT/GPN5uLVP506c8Q4A+FiDd9rPkyXq8I7Lg+t32vda8VMvkABonEqz9hEzYjma7IpOxU3jSt8gmGfX+EtHcl8vr4/iWoZqyG2ncQ56/yOwo3QRVVaifk4zziO2zhcbOHCmIVIR1wRZLR8NjyerI3fKEJlyvCMwKq4HVlQZxBWgzKbPIie8O5pK0rLbF6t93msZaR/+/XnkZaYMy1rq59Kn4Fz9OWuMkF+R+usTNiFtMfx13LqzGlBiZOti+Ih8+LCv09+Li9NSzuoQTdIe9fndS1H1j9R/m50KVXWkzO01MerDuxWDboLpkeHktooD5RRGlE0aUKiIf8Ou5q7MzzRzJwXe3GCSrbhxQrLLXfRknQYhbJPpsLcfnsnkI+boMWOjpIRjt9ECs231PUqhEB5MHs/jpqAnNVHeisrD18MuUb2QeCGeV0FBD8WWi1TFRbSC/dmFNAsWtg7FUXVSsp7gsEIDexpdrXzUVKHwbdVcPBh2IqZZHaiIROWmgV0lcQAutREvbH2MZyGLXMM5uRSwnf3/YiUajzZ13Clvt0YMmxMBa1cl/N3XChzmEq5qSChjX8D7//KdWlifASrwvVIfs5jm5vFMkTG7Ik/iaft0hqvD4zwHPnHLRhH8XNUbtX6pyhwXcaaQux6Ezqu8iWjs4vGhseXov2DktnWwzDFj9oauz2Yj8+ZPL0vDJIRl5lqu3+y3vwsxfrdnec9sewo+9XgQo/wGaqc3e6GbUiYvF64YbGXOzXGT1Dn+yIb+b0+7ENSPqaJzUdkfxhIBPx8x1OgmeBDArVc4aU24qCsAuP1u5KsQUXVB2sOrt483AjuZXqtTPe7jB1v6LW2FhWDWMITOrcJyuI9O3jmyuWKF1QxU7VrOPkEi2WPLl0ZLdI4Ti3arZuqsAIbDKvH6KswZsoigyZp1H/PaZRt/kT+wH0V29m1ZmaAvQxjS1uyZjIoBeVW2UAjQVwvhGubVWtU4oiR848xF1h6m3kbYzy4lqzUtXDgYnx/+uUFzb2XGu8pc30c8n4DAymafYDbh7JGFyP/yvyhTsuP0HSbNkUKPi+zYZbXIAIWnPzfn59sG+rEdRZeydTJH2xKzDdLd3BiDBb0ZHKr1kJXopclri9pnwMrRp0EbGm1sWA+X3O2cOjmpK9wITZAKMf+tBkhTKLEWNijdIl7DLlz5jAGbZ63l+4JcGrH4SyIjp9zUifEyDpXGviKCXKQd/LKnNKCnAWMNopkFei4YDvKyBnhjo/PYdw21AKYdQaaMPhK1gh57KISRpP8POv90KwRk9U6Oqqjrn0WjRrcL98mdB6OZNDIVZGJmNTjzik95A0Trw0tJjEsIZqaBDZ/dewU88O+H8q/szFKYrfF+J1IIfVgOGvT87EEeGf/aRqf11uYpIhrssx/BRfFuo2l+brzlPV/eYh8cEW85fLEGFNWRw7wIgn6cdkefXbDyJcL/7ZmxhtvlRZ68/5VbUz5OwGXN9qAthtib4zkLS1yaxUk9MSvizaoYIeGk7PacnxgsHWATswKKs++BPB78rs7kVCusJrdJ7pxYgMguIGL7g4kYy/Og9Gq+L3XwEe6d0YIoezzScHBpmmjpWfxJd1bsSBxbBxAQl+obtXVpAdKd15YfF9gKc8Qn6BJiR+HHicbJ2aRVgTkVBd9sihwzCFOERNrn11ArUgr6SqFDYH2q7E1Ea/gPF4IHk3YCCPXUuSrrPO9K36o6JxG3oCR4Era/jQ/PC9RkHHHP9r/Xe7nEmxVvkhtTEwz8Sx8vtK84De5nGOblfQSTf+IsFQB3XLxsdjfoZEgEoLrKuE+vQu7lGsUrBguKK3Sb3uOSKkz05Mwq2jL2l8RG8iMCwkPWeUV+84A2jyzbUJ6WJnU2cnBuLtl7sjwCWiuKNbvnOJ9B+gAjZIJbSn3urWtgZ9mNtqYLDKsIvwiZT8M2zSTrO3MalD8nKaSI3VApDVWtWHUH1P9+Ke8CVPNrRdtrDBrm96jveJ8b/C9dm2q6DSUeCxwLBRRQNY7l2oKzKYwO7tOXn9a9e2y25SrpB4Gjr81r/PsdbpN37GJC/3doYo2P23GFqYmrXWiddeuJ2dVfUQONCn7vqQ1PHUbGDqYJnyt+Lw11pfETpHB0OJc2hjuruIAa8jGd5lTXM7jJN3sQswtx3tjqgnrI7pDT2Dt1xlk4CfysvMvllmvEVhkyvSCU/Rli8Dgl59NBgYxfPP8yM+qYOGO9xO+hFgFCyJ+ezrYsVrkgM3TA1Lkn64TZIFV7T2VT5TNvekR41AXYUpxS5VkBBgVqKXV50NwJuTQ5dD1N/O/gpmtP9th1BrwpQ/xmaCETwoZ0frBIkAvtmCApZQGsUXgVmJSZDqBTiEsnfXxO9hOI94wNX9IKrGk2EcjRjkfh8RlPu+qF1z8HMhHp5RxWRG0U/xVK/ezAEoLwDr8G3DdWhs/0a7D8+IuxS4rELV69fYEpkfJUZKNz2rYf57uEce7qvZrOlJF+pN+llw7Y1JkhrmIBIxI2/L7n4kIR6CWuThyEEQJ3SlGrSZN595tG2+fPEiM2XtshbJ1aAJogvegSU5PtPrGcIyjohsDqlaCucdN7CaBRrM2pgkbHpUaegpSDSH71VIjZ4/IdUEzKanD2fXeJ5WzWGhncF4D9B5o9UYqdBMJacdTaAV+TvOlCwbt7/xGnAncJ3xeNY8ozEpbcgM21Tn09QSuCX657izP+d9UJ29gZXDXGoa56jL1I+nvJ8Zy+K68tLPQ4zcAfPNsnKSMC2j5FenIuRUSRT6PIy0lhvv5Zl8z9J9U467qx4lXDtdgPPQ80+9O49jNWWVk5aZwACVgVrASMZrLmflTRzeO0Nfk8ZdIYgEjQg1W/7DrwDe4cNXarMyMQ1hMiGMocPsBRA+xcY55r5/kUdCGu7mQLRNQ5/OmQS/++Fkyr/B54mrtRsfl2e+MgBaTl05F3nMv23UvHdamElNt8Hm4szeAKb9ZQvvqEmVWq2EKdFNSwo6oq1OiBPPkjwfqS4ixHEE8zLReSs3V15BWkvTmxc9QEu0+QZdDLy831eJHzrFOVTxyFp00wZnhm+i0jsPwuMdewOgryddKrng/vss2rjAgfXzMDsPQN5jQAkos95HczyJWkHIRXtOOODWKdQUjOWwyq0Aaw1M7XMOvMhZADU+bYH9/h/j2fMoQ5XUwpQUIOWsfsM0VlBQrmYXyjMLF1X6/OuJxFXWDIJE36dvsBtU3LfAtOwyPR3aak9sX0uWgGarY+5igAuAPWgpYU1uWexJ5a8IbcXqNz5wlw2SbXbv/pH/plI9Ltz7B2+pRMQGrUhtBpxv8aUkzwmT56eUj8qkDyqedTHydgHPU/89Ar60T5hxP+GrlIuYghSz5cPTn7KBnkJSUTeuD/XyGFhXIc4uviS0aNNFccuYBdY6otgpoBfKEiR82Pp6Q3+sJxmG0zEW8HKFAFEncO/8fMqtd96WzJ0Ges9D2zcEym9wUxlBln9PgI3uiG+Jc+vTdw/+nMQD3qC/XBgKWfmm2YW+ozA1LFy5UkgmtA3u/88rSlxIYvkwGa6ipxog3YTWYngGtj3RPISwSDu/YClpewyNmBR56EuTtsirfbWreR20sKd+ztxNeCyJT4c9vvapCbUC9VZmarShnDHgxzp1ZQ8wCv8/n38Hs2si+2Ip8nLE2qhurMra3P3cFqwExVfyEboDiYN/gTmFZbx/053NUibpZ5CMQMblRX04OS66MamZiWLnJtIg0wE9z8V1s26JH7AwXYz0a86QuPtQBKj5t0WaT+ZdkXvuPOqwnl4Py/gSFswaZRm0k4CiY1wF8k10MmzL5RfqqPqPUfCScy6gStRkmS8djr/69OykzAluHbsNrVrVGYoXki9OAC7Lg7fd1xJA8SiNJfUG39vTnhR0FuDLPy3NplPswwC0PORdT34aMAHsqAeyIoU8HcRoAX4Jc5z/w2sp6SKg/0rH4XYfjgmQFn7Eq/iUxbElKjsryEXtHB4Y+6QPpwEAHyEJ76g+7X1dhYNKLb4tCNDv8VBjk/GyHlBNSF2U61o07+1v98KDN5/L7Ok/agB2dWz0WeQFd74bKaR6DBtNIJGgwyRWw//H8O1J+4F1gvjlCdfvnXDAMIF/euj/9pOQBGjAxFSxAlFNA/oUZ+HrG1bbMoXWCBER09FxUszav1SqrURoISUYqJWo59hx3pJt+4xGnrvBUqICjOb9Vzu0rPYUXxNLf/bllZFI4y/OR819D5YyWbUlyn9mf8HMyoX4kuJhEzBjiBQPVuwU+OiBjsY7IWj3i2PvKQYThjO6TF86PpyAnlpPIEehMOBOZbXE+E5+fhTrLhJ6GjDV/XXeFXEKCgf6jyGqyTYb6UO8R2Wmg/KkepNAuZc6sFgzV+0CaxuKoz9J+uGXDTEkPAVQcgV7hZgYCuTvIgbuR7Xm1Ju9WN4E42pmWWsZCtPv2RkdaKoasRPwBEnY/mqcwXW4ZmJz8DIyIPNaNbN7RsgO2d02PHkrPMTkGUOcmwIL0iO5zo7nidDDb7umcIYbdmdFSIbicEhjCRnLyjZ8TWVjMMRgOqHWSiVPZosEAMYmDqs+81t9BnYyf1sw3DHb8K2830kzOM3ABpXZGFfTwRIujQA8XwO+fKIVbsq0cgOyECGepCz150Dpyg5tOtHodATSC5LGwRVyYC+Yfu65AuI743T1HwGgZL25ET5c0TmyfWYsBTBEGpnlARuJAFOtQz5JOOD2eimmfovHozDnttKlerrqoyQHPuMzpTK2/RP/lEvYAxsWpLj8Dvxj3ZJCDOSYXxIH5aIlGIrfHePcNMvZg6U8w8Zb4oZx20IA8WS8k3wYesm70T65Op+bTyothrx6FiPIemLxduBo/gUCLpJLBw2SZGbU/pnTPf5Ad6JoMXyMCyvZVHHKmrUAToofTo8qGoI/eC4ao+mhGmbdvX9Jd0V6l8KtaYBf2PKvTLz66KBOz0DM3zcjxDgX7n90kF7c4HKb8ZbFLIw2r1XhJvvx0iiPHd/IcgJg2htAAcHfkjy0g84fDn0yDYbhws0JBIj6LDxrBnfDZQDYd4dtQdfWXqJlr21BR2TFbQ7OYG0UYZ3frJ91jwMTgpmLfLYcSs7rRcRWti1LZdVbvh77NQUTLWouU91ND1enQydunzioUr20L6EttJU+s036So9DpL2j6JwpqR6OKMToloWUp/0NW7DCHuC9Y+FJAJWY6Wf1uRH5aoY4yJcgJ5zrqBra3VmoqY8S4NmNq2V2/+p0hNLJu29JLYwvlnXHi1qK4KB/mKJoBr5V54Bu0RCK2Hy/Qkkp5Ki82DYvCfyb9htXLtYVq7pLKgkYBWg/19qqXKa+CTqPC/doY0AyCxpoohFkXU2qC8jpfH08huHeC3iaZhlg0qXBoyEJ2LkYTc5W+chXsVm2h8Rz9MnoDI8qpz5B09jFUaYz2bgDXU/a6AUKwC/DapMyV2Puo7J3ppjfkyJmrWKq6I0hNV1viKQ23NVn0EogICwCVSiY0SztSIMl/tiR7xMlLhO2Uu0PMJQC0jgqv02hZFraAIBVqqbH1dq1Yf7JqAdiKzyCc6GBZdyVsUQ4g0i338dBhw6VGoW4iM7gv2DFYrjTtCpn0YjJpf5mhWySGxzCp7ICMxorZw9b/M05nfRW6thHWN9+PUIQLl78CVHQGtz4OmFFKvbBWw4HVv+wR2x/3/hMsHGJ9aT6k4VpnBMlhsLwtPvItqB3TrMsMXXKki3FZb8Q2rPUE2+B+WYY58itih8DIGUzzrM0IbbFpy6cf1djyTjYmS5qK2hQ2LxwLAbUAPMawLXwq9kQBbmSdyf13dSpwW+QjJLoS1itwktj6rqE8RyV7I7alXyTPR6xswZ03Ul2Sxgjik1O1qLncDNaaF/aiuupOggbqEzHUDndlDRvPLm76nSlCqm6FbFREOMlPjs2Hf6/p3JneFJj4VVaHC7k1r1Y8kLoBAcigutrFyJTkX6gvZ+fAD141tRx7dWjA+kBMEAdIpql2LMDb5wJQd5AzDRgUSCi6092NqIk9Pnum/8eYFiby2TabRgwxwYz8d920G3grF/D8VDaPukGGD3eBkOI83NFsZQhO09CStmPjA01H7UxygzWwQXEE9FJGXlstoSZ49O9HnSHkp5/2AtMwePEwLXpwYyvC7H/JWNyBJkGqXpQso3RHrQ1yfj2+lwYHSq6GaHL6ZWnXylf9rYFFGkl5ohp4zIufvasiPRENV8SuCmgwi6PxCMxCwCtSJAQdNN8KKh1j9nkjtLC64C0mp5H43MJYF6GzHiQYr4eQY19QHqfW2swURrGO8l38Qq/ct/4LtHJvTXN4rHlGAVqWQ8pEdqSPdTPif3IhMhfgY4+t66mjAsTCV8ktkZnUyQefJq48mOBh535wiHUCwgyYZFnYYsY0wQCsookwl38rURBmlEkvvPnlbqzw6cn2ITKHeyG6+UTrmxTN7S961TGRRIoE/Pnws7PqwdI2FDCwY2CnVzCze3Lv+gD0Hfcy9pvwa0srzUM5fuNyTJ4bbKpyDCyPrEwgOr4uXt3vywDJ9aCj2eLamqOxus6todlyRnFBd+Hvu4L4or0Q700DGUlBvNXKQnBgIMbomDHrIc9fVmDvqXVfph4ajWH5h+VXRX9ocOYC1k/XDHfKcIdzTQBCGKpO9btA/NbHtXjwcpFJLJhlh4hGds39NU4uxpbvVG9c/DHeWgXr5q7xqTpjHNXj8ndKF0HFy60hbiQmuTOO21sExno7IzCrHWyNyD7T7h/KgTdrgQs2FtBpilGNK8ulEOGDGeBw+t5aME5szga56FjemfQYQ9JWyIoMro+jcZNUQPbO8vepIsI1HOjfCwPEXcdUBqlO9a1juAEnuS/k=BqmalG5tqaomcQvvWwvGR6wfrTWYGElLYoBnX3WhEbi2gN7KacrivAenccKOWlnk7TJdYXVUtWru1xZHmS2cAiN6jCiRIUGoEbijU2pId8sVjBXRNUuAO3MvTxWs+jT2Up0yZ9rC+/mwYigXITX77Ew10BwHYLuyRUvyLyi8BnWXGwgmNx4QFFid3FseHoMYw25ckAJkZNUEwReSbchfzOIk2qpSW+qCYNEP774ytkyp1sLPevXUAJorMYmdohHnjwspzolHxrB+TBfp9Fq+q2Ir4nTf51oUua8yACSWc0G5Pjo8WUNTwK24WzuDf1YMlMLmNN9WvuJc8UDvooskgwZXklVsvEWXDC+XlP+Cl2bZxu7zc2kLsC4VVAcJeRjgpz5wIkVArSgQwFmkv0RL+AD3SlI/j4MTFrd7nSGUYmSOgSrOSiN8PL7F9ABbbZU7DPOYCw7RKN4Tz4mZ4qoGFN06nexmp1hWYGk9UNjfOgTNVVE4zQbLIE04blZ02yGXY04TrnMDyOMBw2kTfLIY59TYDsVZ86pbgcPQ0RWGDQgYyf/zwf1WzpMADPPrXWLQ3BOy9hWKJc/K9jpVF8WJU03PxG3ZijpFjtVsmO+I8Gwepe4KqdiL+lCe4+tzsGAr42jLIdA32J4YHawzLiDTvTtx2IyV6YtxS+f3jXa3B3G08ItnUGTnHM08ZmwfFi8be6kzIN/g2fTDhWzDdK/v5+pRIUOe9KF+SU9flNO/UlUDWyIAmPZLjign4OjAduHwNv8cCbZqyTN7WcTMFo/AnBo6ozdQF7Ve7bKgVf/CXeEX85WKNsA5Dqm1vSSYZpvhqaT8YxnCiUGYYED/Ym0FSvyFrXTWc/R7FYHIyzY78N/d+E9ocrViaR6gcjMGZNplXJtJ2X/a5rGXR+221y1RIem+ehwQTjI6u/02q1ak27PuvM5wGrbRE+T0Ti/NkN5f4FzQWq7Na2J1JrVBj00CeuF7KVEikhSSMm3yUPiIIYTaWLsYS3/L8CFlNwJd/eLdGim+G3eG4eLLTodx6rXDOWZy0Eh52XoMb4/0VScuT8cdFicO5jmaHTySIudne3iT07h1doMZycjEKFe2yi2wlLqscS/goCC1E8CO0/qXWwZ/m6nX4kBKKW98yiPXEGk/qJ5cJmElw3PcRoggfzMTf1I1qV4+wvUbnkweYLBWnRrV2i5a3TUCuY2wbtfIyNVePQvXnDy1TdUZ1rv0OO9IyFMPbqmWhNKOLxZ5uJISbuQh4GSIQcB8eH4EA9RalAaKIrmxdaHmrGurWFr5GUTKnMj8chbxzSutOwL66WE071Ad+9OVBHDW38xUTySrQ1j9cAOgrLCYaFj4BUdcDGzZUXonSVLTbqDZgNvW8lB2a5OfsdPmq+Sd0RnylCuOVu59EYilrhoXRVl2gkeiGq134/Rg+o++UScuEjqt8/RCWIya3KvQ4u8kGh5Mbk7qEZOsO2Y5bwfCd9/wEqmoEfBVf+sEjbFFkyAoputMvK736MbpfUXHI9G4yfVJGqHbhDJiB+luCKhZPOMi31AHkaapd+JmUo9hBz0sexp6XIproCQJTlecyxVn3x1elKkFdUEUG3fca61Jk6nMDh/0iK52wtS+8vXNPjzjanbEzlzCmt0Naj3wkUEvnuDKxBM+v713uBCjG7EkSnDgrc5/WGuD3+HhU0GhmpKdc2FvQMgKT7o8UmUHxB0rLQ4Nt2C7KhbIKG1vE34NMbNmzYcYB3v9AIOVIVyBHoMIaSpUXWDrX1+/rSOpw2ta/C+Sxg35JDIcHXzKGlpxC3WUa7TMoLUgMcvkDYqRkFqtJqYtY95pk/3uVZ+tN8VFw8QKT7ruoAKVz5jnyYMRykKHwk39io2UExkwJ6QqlDdjpp663iixPGJto7f1DLFS1zSswZjnJ5bXrkkMtGelSjdDhF/Fx+r8C4ryMfk3vZ5nDX7zCTmzjRwpMoXLQSNv1ZsejLM6xZ0uV/0/7jvZz9BRjmNtNQNGzejbUGldLmWmS+O6aGLWWZZ6gF3sP7XtGAfiZc+PSRsySzZlABwr99ohazU8aSDGluWpqBRlCf0SB0qHOSYLOAxhH42qv+cLbRYi9XBieP9dCSap7+IFV888+3yVBTj/6A3ByOyMjpuWHmKYx40SzQlsTXBO/nZAQZmcbqlwc5JObnJDFBicIY0hBuYpIqEmOU+1Qd5LIJTSpDj8xwGmcBiuAWi2Mjy04tLkzFudkTgMOUXx2vACfvmpovmHZgb9eEMPy5nm4R8Te1opx5ariRF3RWMSxYmp7ii6XQqfSqCI15ncgIIw0nN7RUhhcwYLIVo1xlQKqwMzFbOKIM3rin0HzmKLeqjJihsv2/FYhhXQVeGCMY85+PPUUfNnotbQPVeFzAczo7JM8UdzkbQWHDFZ6n0eiz/e+43F2lR+1jfRypq97f7kQT+PXvV8T/3Wj6/TyPaXvrj/1Ob9uVaDFP4Rms2+O9+Wo6WeOg2NB/YGWQqMABKmF7Stt1+FZ7f7blHYWYMawDsk50ffl7o6CzFFknLuriG2lntLZoTpjCJQHSS3taaPf60JHW9UrgmuYY3ov+ATvGGZZkbmjSNOZabufRf1bmR+cHoM0jC1kxBfr7Dy5A2EkinqBhHTyqVKAyHSZG9B6j89+m1biZvLD41ebgvMGXnPBeWmLHQEr0D+Y5ni7Wwxrm/YcyDFwJGdopBuJZwGra2YYKOmRHbxHlsffMi+4shywwrswm5NDL7YJMjUlZfPxGv3HmUX5jH1pSzI3IxoO/f+6IMTun1HKvSYz4SPWu/ZzZCh+NLbLKow1iKkbHm52XjrTNoBViwyfDP4E/8DBcwgN9+t8OAELZ/GJnigNM+wEACtELZ48txuDlqnz7bKZaX9yztzdauyD8dsTBxMYf/wdh0elxo3G7vK5ES9jdkj+kURcY4VdRlL52QxlsA4HIP7XREji4PVxLFojmnpZa0P8rC9g1if3ZI1wqFFHu9BilleaPdcFBkAkLNPgM/9U6orZgPhoa3Mo3204SI0gnIDuLW2qlRR4stK+LtUILvCZEIFcCe8YbG8Ast4a60ZWrbTfFMlrezNPB6/82OYa7kBinbr+9BJkjQhMGULoQjV4Vec1f4oQOwmXTmWFpec41PnqeFQvJwnEyjeMKh0dv2w4AccFDemw4qX3b30nv6JTJBpqpzNklJnj54V7lf4smpub+IddHGhtrbtgxd1+h57ExkmToxoEhZu/1dxfrg4fEilSx7iiGRELlHZPjYRkwXoIgtAD4hFVkdUOpBQHeMMckFb3URQDyk2XQpONy7dfkKzCqQPX2tyZM4IIMXd41NbtxFVi4OMi8bmPRN72xZzXYz6fjgBNy7nw3RbekWZkcxnpf3SkDCzOZ1oCN1qwn/2nwgDYCtDJGi6qAJpxNbWOsDscptZ9PWwknTmqie0rDex5ItkopkwDUfix+5Ukb1uEHj9M8GJAWwGHNG4hh/WD0BYbRywuX9rFJNRVwLQrYE2wVJA7VEAldzSkbpT0JPVvatZXlMTlCYdyxqCKUDl1ec6qIIFHJLoo5nm5n3Jz6XDMqqHGVCK2X7Yl5NqaVPi/w2CMHdkX1Uq4CvSgP5LA25pXpgSe3dEkxGgXEcgiREYvNNxjvgAR0FqW/EL3+sUt1IWuRzjGYXAytKcTGpiO7sbnZRSp78Dv6xWA2JK4WXbkJ/4JqTP9dJdsPLz98uNmlvy1jPurBnvUliJqQtBheAl6ocIA2xemXP8XnXb7g9QTAuuneU4yokub1qjkufy6RXfXaH2xruPXeA+UL02QTEzyY1qF7/yGNKUCcwxxKI3gPmPvtjtwZIHVJ7I0niHhMuCfnvpCObVxPz7sAybxTFQb8Hm4+8KvLLJo0G1STM1RtcuyidkUN1PestP7j3ZtKaj8mTM+wDQz5PWZ0msWmYYnwtpKyoNo9Mzwwt8IvbQYkZsP3CX2gBIThmCxwygiS2RcAMGBSZHeV9KuNKQDeu8GBvkCLweCEbdVCiT1VxDHAP4pGTcTO1vlKrKS2/7R+He6MGVFZNQ/Ks4OAd1FGNB/9kpstdZQUyiB7+QrQSnxplNlc0zvLSc/305bK85KWRSuDS/IKO4j4wP1uA9D+/Gus/S6Lg1UJpNY0+rxvWx6uMH4u/Oq7/wzlXEqwMG5iIhfHC7qEq/wIQrrwsKpKONMoZyUtcrvuYuOYEtlw3ctXRspH6Fo7Kkkwg2kubbG9QgQV3E9RrSIkQD5LcEL+hmpKT46EdDreZC5YrXVi4xRxwX+uidn3Swa2qmsPwY401Y7MxvVQYpUUiXE3datRsOBfqMdPvAoo2EaSUg7WJ7l3ukQbXNvjo0I7ngDHGLKC51CI6y/hgsM1B8G2J7zda44kXVgHavVXSJYHEHMOeTNSYGv2o6RP0n+0RiLe97+r+vIukseSoTpwionnJuGl3eecRXIszCs8xwqib67XN572mmXAtPUD+2TsNPoE5jbiYzC1CeuJVzUPpeVpSzZd88g23lmYfOwxBTd1rFJWGLEAfvXQZS5e4iHq7h9ToXsygp1EuKCbjGby4tqXoHX6j/bBIC0hXj8RHjGKXODsAta23tz5xas9U2YmzQOOskQDbyMN/11ufQtMSjxcv5i9h8BXhdyBAqkyBsR/GNhmwjUjIP15+ewbSmBM2teWbfU2qijkqFUa5ilnPas7bivvB2KAshfKAc15L3vNJWcExYxMpb+vB+7eSx5PhO/wFfTWOW5r3T8QGoHnOltOOkdRpIytKWiLIDXCW6iH9uYYt3Oype1BP5V0a0/kllBrI3xaEDxpsYhhtxvnWuibhlcmYP85FdIwQkA2HVTSg26odcXvOVI3pn8LHyETJilMjwLOmZXtlgu91IXy1ZDiifqXdI+EBv4KfOyEaSv5ma98iIWvFW1THae/7ah3L5hAy088vpWvqpfrgd+5JAmAOPlZ14YmvS6SwKzBXlraO5rW5X1dx/8HwcfDd2ic0QLVVH/uw8S5WRtgwiKaBY1K5kIMYQlS4FDSOAAT/CC98Qpd0j4QmL+KWNUvX+rcXsfsMWo0OTWL0fXsOUPsTigGMJ5Mq7p+Atw4lk3aDHWxa5CnUyamlvL8ogS6GqReEQMOfsZ56LJY784ku43qBzl5Djakq7o6QSneu9KPloMOJ35XEZ3DC/cIdF/u/f7faPTWrmlnugZwtZyFW/icv/t2yjsleuMag8cQPSl5q9ITT2Gaa803u+md8dePzZpTVaV/66A8ze+pwumKlOK24PQzwDjXjZQ+EKI5J3SL0ZEUnASS02l5TTWUyx5pRyDGtF24l0c2FS+cGo8amF7yl0MldR9rRFjBo07IFHhvzeFheW6R60pQBPNyCPurToew7ojJI2g362A3WGvurVvoKxy2HEp6W8Vmbx1ljUG1y3rVLAfKNx12evTy5TpA4rF6wrwCed2vmIWlsyfsp3ObFWVpjqnIcCB5wlKvUyXR5w3ag0Q1HaCfegWR0yo6NATsZyN7F/Fjm4QPSXFO4Db1GHYVOLOYp5RCgbpWRFgq2EXMGdjtHHd8YsO0y6N7Tm55xyWlJWuywDTVMf1l67mLwW6mHHV616A+WwKGKmqMy6aOLA6zCTA3no2gZqCL8vDq51tXTJqB9ojjq0EuoSTOUdiCsRLyx2iqQO4QXSY3sLsrCK0OXV6A2BTkDxtw3hhzdxrGUpmR3F1G67SDIy2CrWeJdQqqbIK/8r7U8OFFg7W60YToXKpsN4AhXt+ki7q5rSDMDnUj4Z7pdnPFpdECuzZZ6NO1xE6Q3hs4Y8x3fHU61ky4e5MArp2KmuDeMGqtsEnkvDv2AqSN4nYiB7P9+Nw26d4LmEp8tgMHPJWu60fp3j0Di773kFScyzLA5B6MbvXsaJ5lWwFkIFfrgv4BXAMBaB8c6c/hDpN1vkA4To0dNVaoNr+Hi7wuc+Upxrry2/bOIbn2xReYTZuOLbW9ovr34ghvm4fTNWSPtqoLdYIAqkXCyng5ry4hDmivS4ZlZuKOVaDsOqM3BPDJl2nVyxPeJQn55S3XpKSZudiTyENudDA977CAiM9ERT8XkFjkrHML6VMYYS8qU5ktL4XyIFkDE5dkSX618ayGVA/xD1gJ+Hj/cXN4CanrcXHVjAnXfKTxu8Fj9FXUGI0BQrD0crDBcJbfL5YL0NKZ2fHKg1b9ANtXkMfyajSpbMIQo1cLeM00lzWwHen5oVcB1kYd+7SniHiN6YmP6aNarq3d8BaSb3BKQEjd+SeBjMqOymww6dLldgSHqQt69mG6H7swCKELYJoEo7FyGbTOIvudm/ZQKQLZVyhv4E2tAFwKSnW5AcjZmzBPoOPM7LfgzQqBPpGcxmufJBPLBo7hY55EWBYPXRKNfDycycbslohv7GCUlgHrbqY7N934llIuu3G5Uo1RqWqCmDsT3vYsXhut4s3Lmfbkk25N1LImj6KYUV7kqK6WeP0cofkBHrwPPbVi6RIOr4uSgHdX/YcoOZOWksy1zfvsy8KIJ/X8aiJeMwnoGApQ8OGW+ap6+nwIojwYLeekWV1Gxo23vSlKCyunMLAt66i6vHfrfyr0u4cmue/g8UXUrjbcmZpj9fMeJb/LWFRhbiR9QP0n7in/Th7PTmIrrF4thLIhEZZc18W8aapfQ3y7Q0XA7P7bYY6hFwF1hJg8i00gVDxg/lj56EijkP+5yLyLCXMZwB1c6D7eRVERiqGoYg4VyxMLo/0OOxLZzJXBmdF4LbdQyZrImOAK2N2w+ACednZp3OhpKo1PN0R4CoEFuJ6oN8GwGkjVV4RvIz9H8tSAd3O21GvPHij9EnfDCHfhfkm0NL6a4tM0Q3ogc0s+mZhN8X3/Qe5BCx8i7xPV7eOHTJ1b0NySuZbVpVxg3IClRqMItjc3XEWjZYH0sijZB8nUEqHceUd+2vSFelwohRcDDAYRMQQokUOiySGrUun9PRsZ1f2/F9KEDkRkXRsDNiCWq3dSQmb1K+wmkaYLziSGapihUhUihZUlQHQQqUiOFFPwTs2J4xBs9nRMtFRdfaDTz1rIjr9xUDF3vJKt8RufaZFQSERXEmblSScFKnuxlsGHoxs8JmCvLXiaVE/uS7jq2cWtHtxUyATTQUZ8wZn4N2e+2O6fRdvZRQ//KnJxbMZuXa2tuehxde1SmKX1URjyltEnD9Mth2fJ2uHP2ZMxHIk38KNW2jndxseOh7yhg+xFblUpG5ahKRFpZ/mrfU0HCpZ02pbX/dX3yEJiFuWzpJCyltZ/oe2Z9o0LckBXFfnAck8e3+ZORUhjGUeZC7617xDFnFOlvDg0hg/1kg4F7LVPL3I5J31jhEldUOwQAEE4EWmVqa7ih84JB/yo5lQWrwHcD5EFzfuC+gQfx6QNoEtBhszZ+LV7AqnUpI1Ityfzr0DQp/L6WN3a3mk8BFsnj49Hoo5cpn0lA8UlUrADtbnfD4oFwJYw2fAWMZ7gtuK4F15AVPtPG2GCf4FQtzWEwXKu02l0N0p/gndzMpuL52+QQjLuybvyRmqph38/hyAxErHXptOHkDx0SOmAcEBHy65y5wZVM7NtR2DmgFnYTGNmNqJzDcCeZnPVO88HjPNNSdGWp01N+C570ntikRLNT9D2Xd7ev24NaQGuY/lfWOyWwJLxFoQ6Y/nVvjXqY+ecpGsKKAMzVhx6YL7iqMJYHjVqXQ0Sd2tH4wa931lZtHHNqPLH/1d5ED8BAGKWSgnOsDaGox1HiaTnMYF9UDXmE2/N5mNpxm9lg/TUHtI7iD0zSSIM9/Trb2oodnEVqAs8kLoiFXCmxhcMjKHk9xwahbHdgtk3dcaHBG8VLoYxfFaRZCIlYlsi+/PmMT4syNBMnzCOd4h42d4TDVETAFz9wNLhgD0ZmsVUjRS/i8wR3SrQ+mHhVj9zVYZEirzm1Xh4AQVHnaaxOb8DH4+86r0F+SGN0ug9E9mwUd+xS0qJNpNXwTVvv5ieCTzoXXEWcisxm6QRTbiJT87KWGYnogGoK/MzKoH/n41inX+gXJ4ym5oRcxhokh6m6beUen0sSuVbUsDGZS2dCjKehwMM1c7kfp8mhmk7zCiQwnsbJyyyZrsk2GI1wSw5FwZ1sKw32SD9yuPsdPqBGaTfO5AK84kjL/hkVkeK1sP3bHAVr1Ye6e2QSpA8xs5YHKrViLLIsx1e6AMqL2JgIzE7tI1sGxyF2lmHUjZ50d7mMW8ssKK/GTtHbvqnS9F+CMhkNQZMsK9DA4/8Dyu82UVLL35tbcnmLN6F48HtvwU9fpQfDjj9oC0PPuY8oqwTVH941fwCheS0hEp+WCjSr3cEhKpW9TWInMbfidxmYZB0cQl3MPem42YDSBW69BqfZLVfSWTnm2Zz84eDLN2jIO/YtC2z6a5Fc4SQvLgeryhoTDTQ63r+EJNpJzjYMOg8R7RQujruuHZhNbZ0L7Nj32d5GEh3SXcJ7A2FBYSvXPnoUjdddyI4wx+y+fjKNi6kcAY3Y5b2UhkehNbcHNDxc3sf6mlwN3VZFklwsOKoi6cih6ppQ3tn6AXcE821w3/a0tfjMW524gsR6d/yZBRBK7kSTPFPpmwZlzm5Bd06H7fkfkHUzflJ6ZepPdjUGFfrYVErRmxX9saLEszppxzWCHMDHmRgB9defXTL/pPcNHY0IiLWHURrzP3ek1jXlVp35KHI1VhR69xKEp1mMXwK7hjereLXxvxh4aME/QC9xoxf8Wrr96gok210mPeQb3JzzNvWsmNf81U0hcDiEvWP9swdlz70f21xSPTHB0AAcinQUtVtfIspweDQFztTg30yhD1kZM5AIgZVqkuhNqH/e3MoqPeMvxY33V8PgI0l8XzttX1UTphKgcbH8NUAQCR9hL1Weupg428qYLOaM1J+owm3Vwb78tS5bV7NlJV0EmfHLn7xcG3c8uPctbPZhkIJwOAQQwFMQLwt8UIUtLM/kiZ78NPQT1+Z0drKW2YwD7kOHlMn9/+wv77SdFvOXop+obZSVCFiCyMtBIyeWYz7vatZ6wf5TrrNyEVrsipAWK205BgozCSwZIAHV0vTnctl3kACFTVD8T4aBiBRKAxERVlL0NUUFV8dyybgLmxg0YCV6EGr7zO3ZZYZfo6WCefnMunEli+WWbG6MH8V2cCCc3+qebfuRrDfrw5UoM3Z9FWpWk/KFRfKOB1wRefCnsWTqpDUAjDqBDb22/79hd4XTZlskWpIy/9gfY1VkHp6CzV9bqfJTx4YYb/VEqe8h9XZViZDGmWlm5ARdBQlDmBWjndCNX1QE6E4mvaxcQs57oCwWrDkn8rAktduWuSt1JY+B0sdPh8ZF9EQ8yD7MnUhodjsNF+J2DFjtFY6nqzrWEzJ6rN6hL3jgtt6M2qwIzLUVeakKB+xICwalyDZL2rjIyjz3gn+7D+2Yy7HzRTCOyIGUp/5TdpUMiBcTums7QtEa0WWcQjRF+kJFX9mqfIL6Degq4UctsQZdw+lt461f+hhsWveqKtmlVfkZpsBG1TBVEH8O1wpuervz5Rm5It3NuV6qwXDuy7ckom5vJxiuTUuhaELvcuz3FCTJojmYNBD5p2jORiAqKXuIM+pcuhZE3uqFJ+ei/SC4Z9ZMowWFHV8h0Mv7GxabSsyJRUsC3isUDdam8avhQ0urJsMCN3+HO3n77pIP1dGexKAYDD/zIf7Kx7zvdf9Y45jc1G0Xfz5o6L3BCHJS4SuJhANpe3uuOBDO7sLawUKvijha7jzarq6p6q4lSckOnfsr2fB7UEsmODlFC0H61qCDElH3+AFgFuVI6fiZkkq6q0I+cLC9/AnZNFQqaMwEYrI9QRQ3eutHPc0SxQc3vlFxPURDB1tHjfR++fvUzW6q0/ZC3F5fxq548oX+EYQ9yvUYZ6LB9R4BwyZoRcJcJStyt0keNV0bmZ6PYvnMVSAnABuFkZM4Vldu15TjNIlg9DXtjdDRmUImVyhucsiC+h7bO/8pzz5BfqqMeY+PHBW+eVWI+CrNU71s1GFp64XGhP8TJggoJkXgHUlQDBcBvnrnFTBTfxppt/hCt4Vbnuo6h+V1HsDPTIx5GfKYvARMvmWxxHQmnifcZm9AIOJJ9RM4/yUiBcy7Fk5HI8fNHcM6Hrcte3hO7zYmk4KceYxN6mtZigcyu8mamumdRxyLS36yPsEgJR0azgHcm4eM9tKZybtoO6asbtEGJqj1NByuGHRwZ504ctn4jb0MpNWp2P/3gK8IQZB7mEs4jtRWwG3vA3CUO2IRa86uaoK4pZ4n6vbzz3uzDzun/butmoUCllVyySGWGOqAV5d9L1mk96lruR4X4UfZDlXp3D5nIYAwPFpjkGzlICHjf7VrFcpaVH1+uBbJprM5h5DjCG0plEvGd+ovieINwLaGHYD2u3QkN7OuTi819ChPynT5/Trz9CkzZhJo23wIdnXFo8E7ugyVaCGZ9+p3cUvE26bI4YOjHLwir1X16HFsBh/dzvCIAEkKQu6mxBTt5u3plpTuyPrJnzMxPjc/xtxkq+iw+vsvIOpqhxaY6/dy0LtG6BTuVQgJ4BgLZNC8TrQvlLk1ufPEE8VyYE41wOgfWLWOeL5jK7mxosljREZUFsgffyVCGiyzhuC+0iQWHx8ookmoJQW4doqQVfGsUIYlGjfPxCOgvjrva2PyMAHopgRKkE77jX2vtZhVaN4ceaug/Mtme8FBEUdlZFyK1COQKEjQh6ZtPjt0weD7knX4XjWSp7Cz+E5OAbcDeO9xtuaPstrUZHDy4uTm6qC3Eu/aWKvXMW/EGbOwC0dgEaojyunP7P8RJUiEpU9fEe8mUsm8KMPzud6ssQj6NqEgWw6hp+u48Cr1knhyYdkHPJ+l/JGAx8YuZQ8TpJ704/M7UwjN9AeOpLGjYPHk19YClXIhbS/XTTyqQFDFdXnA=hAMcg2hF+jaCQL5IwvyNBDItxhChZNyKmR1uNxlThqfEV9ZOfpvw2Tc3aRBJSaN3PnzLAmFxg+jNLgHXorVGds/MT5gBStCeq9p40baIWGxVv5bffKVIrNap36bYLbcc8ppl8w4W0ouFfVWkwG1aRh/aSw6VBoSmPJV+ZAVLJtXGw0OZQ8hir2R/AykdK/E4BHZ4AYdu2kQpdpr2A1u0v5fdcdXkLCBQCXAEDo+LhSb3sTyDm9X16ulruP+57ycfxpgu+B8iZQEV2gik64LBfWGoF3aPF6Ulyy5CWNCMzskQ4oz7p5D9S6Arn+yzcV4dp9eYhQ2x2bCU7dmE1dyENWUwfO1JGKyB/nZeo1qIou1uM8QmAmZHCFOfXx6JR/C2rqO94VOf7YuZk9B69QMWPVfaqOiXNW6eYFgbPnB2Qn4M5g2jxTSjqSjLvrxYbdXl/b1mUV//BFfrl1XlG+RCm5QdSxF2QNU5OlnFolYSoLP07eiPbr+B/99gOY7QQ+6A7KncHsQL+NS4WP2yFhKJVu0dVlqnnW+GbozQWvfBIzBcz7Vua0L095Sz3f182XJrrJUF4THI/qdVd7a9Vf+K9NFyi37lIAiPNKvJqiDCt6pmp/raxcZDJPJ40m/AMEjE+jP6SDq+ycHCpwiYUZoRV2HfHHoPLJyJqB5hlTmoaKa5n0MOUVf+IrFK6Cyh/J6pAwL/QzDKMy9ol2xsBdmcbOXohLn4kox1L+VAwOY9RIpib/CqU9ideYgcxO2iH0AIlPPzNKBu59bxfp26eH0A+DGqx4fnanBVJL6c9WKis7+FlbyfkZcPo5thiQsLevSAxQPDVQe2KPA8ZPTrz5j0nvoizT5uK+FY0/d9lmwfVFnfyqYWC/K5zUFPH2aIon3CZmnF0DrCPKV89ubSCLm/zJf2vybUJNi9cRdwptdZF2wQl6Qw+sy6Ls0WEpQebB56o6xOndmGOHHJH2P+nyEJ8+7ouROQnp8K/IKwgu5RvOajR7QlLyGzrjaxmygCFadQq0x3nOTZQpCP6MZuX1IhAUo3YBk7FxdXQZJzeXbglmPqB3izBa8/Dzkzjh0O7jXo7nVtFkHm6HluMYgk81KdozXh31nBw0HZF0ZCnKtcYjIoaq1IHszuRWAr9QrwU4xM6DrnOajnvFRwLO1CWmH1fBexHpo0wHxu/uYpiH6Y1tsITTRSiMYeC9j6/sADUvRpilLej0A1Tf+Ds6Neoo13uxOwYBbyNfx+TGRlnPZhB72+tsjU7fkeySXvXZQLgN5NA1fCoSV9TJKSoN+NQwG896rPR5vmxVmQ/AUvv92h3tMI0PcEwMLjo03qT9G2LZan23vpcnmRap6v0XNO5JAElDIKEsAFxGGdloIkJnla7TSlNiXYgWDgES8h1CMCh9+9NY/RUtCAkwEImKSrb5NWMJ1rbzdt9+vt+XrVJ0RHYNU0s4pmlLMg0rsZF1D/ZvgVsl+c9jSFPpjLHoSKwKGge7CUMfKvInwjPQ2aCeyBil9sNhAUqGwABDLj0H4hvVRLhTE3gJYAR96R/yfVFGyP5a9HOsJpnQ8R/DEY3fd7S9S1H2Sxz5zleNTSywpsy41ZoMy9HE0/44+UP7/7eLw4igDIVeZzqgEvqMG6n+H6KWtoM5VEqVr/zonZgx6TCkXtUKR+R2iseJu2DRl3d1CAEvKxCQyA8f9WVqUWun5+Li0UZoF9ueQxXKNmuINHa/zvmyzGX2vhwFHVKruWe5oRLblaImrkoRgkhG4VnNUvZyAwgdsYyrXk6xUojz6qn4hVr8S09mV8xMwv1SQiu1lGQcpXCRnJdWB6k+ob7gfAVO8mBRcFX1hlWmRe9UqhikY5qicv1j9sq5aELRjjCV2lTWcug3SFvB7OlBNygOsm2xKQ2DoxBlP3XFRj3AXZoOK49lrRQtVzDYLS+36CFMkVI/0JCfi9oLx08JBRHTmPv3e/LuTrcGpYtImAzsE7wtsc9kE6bWVZXJL96mdMt/oSo4jtbNe3X6MyhufnIuId1yU1DFX6IeGy7dQomrDWqkl/NT/9aTajuQIhBfd4kXoXnERWpfw7YdRqTBbhY6TaUgEJO088MRXmkxOdjNX+7CKh9MsxyqAGWnWVFh0NXjy6bhShOwzmPzCEhAxrYFqdJGDHqvDJJm8YbYXOl+uAStTTHbqwVqQQspGx0RQJdD2xyAzB7tqaJxxGMP76CpKpXpgsNMEz8LGozZifaQtd1IVJCisqLyk8ljHm8ogvMWhY2tQ1vqQQ2GEUsAojR+oIdh4k4160FxdQrLWTYRP7FFwV+8mSw2BdEmksxymjdZcfmDgiqcUQZBOAdbL2PEvKbfkMxuZHB2ALrBXeqc9YJX+7cP2CD3O77Sy9i1UJYM8ZN75qSblhDKzfn8G1NRcC08IL25SIMYS7si+POaHovXj3HIye2G3hJJg7nw9JNMOTCz2bRVdu/7k4CQwdYo7ChBdCct6HILrCbhINDUfiWTt9LhWHd+EMbqao8neUGvnYRweHz+nTdHttGEFPsj6JyE/O4VNYLDVwl7KTVN45Llirs7pJ/46txWggygpyqohCwixpg/QK+etfDU+hhPL0pE3ue61W+juzltkRr8f+bU2Pv4w2sri9Fxuz+A03G7siXf0Y+FqjqIkt5nkSE0YOki2KRdBVR4CO6x+/B441GH6X3ZO5v2WHy2Q84ukpG7QO46unq5u7Mw0BKqhuhN4CF98ikC9QvrwZg0H3mzVMj6tEnW9IZ3+suM3sM54q5bkce2Q5Wf+kzmQXcgO0Ka66Q9NarnU4Bm47rxh6zvYmY4Xx5ruq+/OjzVZJ/4ecyg1yr5CDT7kKFbCtDNRHPStiNJKlTVdTL4+qW4R0Ri4A/iMkiqa3/cz6jFzY+KLUi4ptOwug/tuE3Vvb29QHizGZiMZRMMxIf21M4ivXAnMXTt08Zybnp2MzTrSbQsk+W063i9vesV2tE97huheXUcUmFE31+bJllN1nXhzZIZZTnz09X5pGxnLmb7UBU1Xx5HuXWVv6tKvMjFiHdsfCvVUUo6bLT31IyhK5JVbcUDfOL/lXzj3Z8PcrtLDastHh3ti2lv6LOBlsx8lcuO8QhXCzMFpsIXnSZTwgcrn5dQopSZSKCctWKjQ1FzZBURKTck2nFzMEnPXcwEmoPmwgFhv3POdl4xm1wg4FGZBuLyBL/nwyysn26C+kNmtfqU59I4NrVcjTrKrsGLtGVG9+0dtqtarIlicoUXDz1bjIhJpgF9if9sSlRV8hR/YbrrDqb5idyeUn2cIPWY92mjzqWzckWvcK0yunghM8fdE0b0Cw4HwPxc/HKQ1dEtPmAPz+GyxzxE0n9Lz0XqB1/S4/GFpSgNd25lJnLcrHewL9btUUVuawcmUqH6eEJ29NJcX4NoIOMgnnbDeYDEQ0VwshnkZVgxoEKaFgmMcMr8Mb6WIHCqqjW7hK+XccI2uong+5G7FuED+cq9Wk6glOo++ZAsVF/0Qm83MIUKERbW/G8plHhfPiaTsb8g5M4lAHziv7EWq5OEfbGzVEK5g6JCclshj2vWtgx18ZlFmGw4h7A8w2bbvFhpKY6HekBQ0bk4x2PqDaOH7+HG4mzqn3lsV82FnGZxOSFNP8KPuP+jrxFYuwmm7gs94R8r5W9rwQ2q7gkBHO1+iO/hoD7POl1b/ggm4x3iecCmXYl3yrRSaOs2sFJeyr/6hDnKQctocwoJ2P2ShvfiOms+otwcc3Sy1Qfux8GcboFJHVq7DoSRe64HUl+ZcMqqaTeA3Cnu//EHFZxo+xZCPGoYRK+Tb6i2Pc7n9nL32HW3n1Ljr2aKaZUFbjK4N7j+Mf1jv6Syp2Qh8yc657Z6ZmtuTehBSf9LAS/CyehYhVd/lCYQ86FSgos51BuCSP8MJE07JzuAWOeDo7wH6RYom/ZOzOvrH46HB0gXrXHMxdzs/GK9ovq9/Hev7atX+y58Cq99I2WXnfkWP9t+0jk1d6oNaMLXZ5cq6mUdlchU/MKTs1N0QThqWZdIVTuYdVD9RbLrZNz769AWGSzEtsU/Kcp2hAgjCv0Tc1unkEeeNrAsrPOMa0JMmrC9QlFVPTnPD63i1+ljxyISwiq+DM5shBI21FbW3NE4YpIvzbXXYt50WdPsN/rSI58aoe9jCXKBURSGLPXg8NL1yRFuMAZAJJSRD9o7J7/HweKCzHXTbSiM2Td+kSUpXMjwhPYnG4tN6wM9X9na1Qhjh156GsN+gdBY8P70O998aePRKCs6YJbrlV7hPTemz6nAznyMDe1ADvGbTwLY5fHkfwUA90KkeC17+YnR/RlMVx6FJdPBQ8o5kflOCEg6pi8dZSWqQPHHnGSGdM5efcQTmccj+PqRu3Q6OosHmndTXtAxkfj9to/VHL0WvAI933+tWgV8hkVyJXPfoq4CP6Z4Qa5V/pQqeMF85fOxI5WI69YBwgIinf3JUKa5u/bUB5Y/KApmA+oIcqJYuNIwWaHlQwqEZmDF+KR9uE4CZ9hZKhyOCBvfVNiUL87fLMFcKiOrQ312aav4CfcmESjU+V8MzOX6gWGSERPI3kSDlZI5iwFRBmrinuF5O4YE6EKFC22fiWMXaavPGdvIa4lDzRDZi1krYwjKodhsR69qWLVB9PkLYnpwuq17mkt8e4QHQ42yNyNbBs2DhWNvLIqxeA5zRe3WogY3PybD9t2xk5udq2LDBDuhwmuQMrWM+65OjCkbGLpO9SL/O/KvEg8rQx+1TGgzBbPRrw60rBSM8BDYZJCy4WhaMNjXr3t0n/oEvJqw3mgUMcaNQXgyU13vVaMwJjb3XmIJhTgx7MCNKQbedpoVwKfCBV35etympdf/D72pJZU28kYAE9PCTbXGbm2ZIc7wughrbxHu2MqJJEpjvauKMGv8ssu0PNQ2HtMApZTzye40rv2RlLhKY+2Ocg3jw9Zpv4HtffY4k/ORQ0JGldlj8YQJtjz3Cu+cvW2yZbwxouOV0icvNv8gqBEerIdhi4Hsndmt3Dtrk/CPRJn5kdX7iwAj9onOW6Y1P62liwSmlHeiawe1Dul8o236mTmN+Td/JVr+elXUj1/QqBVmF+0AgDxhkStUmRIW7BXuI+WTIhiQaP3VyVL7yFeJrXISK+GVSYcEhNnJ2bQ9f9msGhR8EOpGhVHjq+lM13zpfebvQZtsgurJ/R99DKdRgcYLaXpQTvWQ1WyrqGpwD1dr9jruAMbpN3LVKgTiY5d6uubrB/lI42lYz68u+1kP/JNVoLAbnCN14YVt17N8RuITnCRfgbEIFEXd8vI2sjBjfg2R5HeGT9o1JJXRGbWMq02BoRsiC8gbSD785KTDNmeN/K0Sqb7erA2qruG7COF9UXY/d9hYJbKQj+iFUrJ2WKGFZgKwPlWlwBUjOiUpXaBkodPa5m/tkif9KmC4bTgoZSQ1j5om69+UDksA3/3eSbIHqzBjauR4lZJXEfD3HTeg+zsEyyH2FVg2P9jc4+JnHY+H1bG4iLPexZ+84bi3DoVZ9bM3mwxgo/9PmwiN8tLRbouioxvgVKBWmW0N5iHyV+OOWe98z1bzvzfT1I9j0ZHogmEJt2PgpwQMlgNV5MrXNWTOJzWTX0AMP5L3IzgbblYzlxlko1LHoMXWY8S7P9I8wMTZcsI0sHCJ9GVbv91nATV/jzRQUlKmOEerXxhKI6bsnKHPToOq5MxLeoffmggoQ393BO261Tu2gJ+4kSz5fWnvmqccPznfIaKRgPTmPZEyqqtxAjteYurKihGkxV5g0zDrhpARq8oQi0gb6B4XqmPWPDdtuN+EGkQg998xG/Dd5q2gotu4w8iYyQe/hmZwHEglUC+VZ60Du+EnQVGe1/A3kPslcCv/oa9zFqrVk75NI+dBZ2riXbw50SJmjb6dURzB0Sbafyh1k07JeeJ3lkHCs9gfhgQJl6WH0/XhLyxC4Q0foXOn1LsXTDo5nEyTqwPzjx438ldCX7q+BRLaXL4ixn06lbKfYLfKmUoKUby+ly5cDSP7U2FmorOOnx6KrJMpE29nSXJXFRU3KcA8siDRHqSsGR4yiLIC5rbKHa9Ynl4Hyp8blgQ7IVbTHz/BDcE2cdaqKWz4sk+p62z6lzUOzUTbfD1r51Twn/vXucGWBmjKOn7YvTmgjWzWrq8lWE257Lrefu6c1oXNkvLX9AuVtKfxxRH5HWHnRHgQv+nWzC0HQysRbLvfl2A7RP0pMbMPhvvv48U/omWx0YLVXDN32fAgbCN/VYC5sK8i5pcRJgpcGk7oNicug8QgA5zNFt2U6f/5jN1++H4fMkTpLfStuxyQ1c5fA6r+IuSnKbKIe+MQzoeYT7lWzSRxef/tuhLCqzBNK03twBKVH6HCtFikcd2cnUODhdma3+NrOlmryZMmH+uhLkFCGwtbevXuzC16rdpJgjwZfVJLHE4VodzWayOit5PNwSW2u90mkGiMwc6pm/acKMA6Mx7Wv54sEWmtFKx54e/fnUpidXeDq3nj0z2RLt/kwEsV+VNH8VHDaw2NP4rjS3L9OOB3qbBlZD9/x0bN10xBvXzMTMwOolqu8Z9GLuzdRvyag5DRFmJF7G3n/S0pD0KYs/xnua+jXHwbsleUMjD2Xa2dRJw17j7tCcopc3TOm4EQSbvigy8s/ptaMnplyVh3WJ9MWj/zrEA4VtCpyUmKPN2Gk1WULKS5a9/s1UzulpahDJyCN0mnQ1XS6odXNIJxYDl8ULMV9zol4KmfTs3+t3sJ9dXHe8Nm7dJSam2Lfb8c3gAjCXJKqoJe9/ViV5T1yyjVZJxyw1BdhmPg15FyqZbJfu4A57zPtB+nO9yIQPSYMmKpWY6wn2qMhwQ+Ygxz9LYXZNR5K6vc+t/xKHKZRwYYewVq59n5k5uHRu7t2ERygjrP6pgATOktGAHMhg4Hf5AooFNHVm7DAoq7C4Ndp3FOzPzK6fXLE71JQe58x8Q0x/bu64uS1vYnVfoBY7Cml8+GVOekB4wsrVYlMtQA1Rr8r3zYDElx88S5Va7fAPQU+Urs+CnWRgWdwNyEW4PA932byM2/VmM0JEef+PCBN6jH1pBdLaZaRJrc47GYbAJFHeNauAq7+NgwiQDQB54R79G4WwJ/Kj598nWoboyEmky5gLu+CRPWb/FolPDzpmL/+MxOQ9rvUvrWxykBLUHOHjWs2IVtqMSb/UKz+iTYZHZacuiHiFfkLF0zv4Dh6h0a5SMsq9QX6qZf0EIkRgaDZ2FVVmCy5oOCw3V/mPu6Y6mFjhtJsXTDEdrKdtBdCHu9MLD0KuSUUxFGfM3M0IjNnHqeBiBa9wXkSrAnfecWGoPqC72Df+jk+VR4YCFm75UYBZ/9uW4VfBZyAHWiJSNOCTuEPenYLSbrb1n7CPUeObHvqQNApBA16blxoz+nYHadX+Cfk/AuHMtMyPI/vBTSD7K0zH2WfVVautjJfkdL8DlWJT7IR3fMfdaqbIiiWZQwznQmM8O6LXdVSx+KidjgSBGmDg9Y0pQJGoqqZKive94wKclLHuoNsqFVEhfaEthCHWZkcj5JcGfW0NH8bZQJ080SSKwMDFChdm9uTlwWRA36MkrxldiF22hQTs2ABQLMnBT8R28B8CubfcEyae5yPQtGS/uyFfc89vNlxd/Y0TOm0PpomI/5N0xTuYDqfPbPQLhrPz0ULIN8t+xO5JSTyiw7CHSq0HQR+G4tK+YwNQxGEppAuifEP8oS4CBAh8SdSW7J1YTTlPEnvLw0VTFu1z9/Y4R6O6vEvjUe+zTdo+jobd6Ugn55wFXsWK9l4oWA97F0mo/kKAmPPtQSIorQJZcxt51LMGJhZKA+Ksg47Vma6pP9IQehqATd648Yd2QPpVFeL4tSScUloP38JHgrfVGa1Uc6aX47uuAjbdvyqVXnevtzYSj1r3rk3zELv3Fa+A2xZcndp9h0hkx9iNLcPjxtrRQPEYZXEyY+waf5+wrZFHiDHt1sdQteAimGCdWPgNGQF+1WnaQ2h6iL3RgZWJuDFS9/QpPQhwg+m2Mz7TFV9eBPGny7TslQtZl1jCKpORpFiSAgt8DUwKuxengiCc+Ewn+n/xwxhtwG8FaFcbpkda8Ui+toXlZFC4HSAPMqG3Xc/FnadiGAEOOeLXuBWfaOIo1I9Q29nQe9sPusrUn2MKAiD4xBk1tFBi+sOom+vR+TYUsWSNaKpWupHSFleTTa99yWbbKB5c8Qy3FQJ7BJuo3r6TkG6q3FcGHXV6PZUL3uQl7XivoEtqPHNCMXe+0x/0BAeAM9c8R5a++nwu+RfzwYzACNj7OczqlcTAb6a4h/G/em0iOl9Fk1tbfXsO7BELFzcDDAl+kEEX1u11yERB7HjLc0KimsZda0F6OG3DOr/RReOO6agVcDDzUk3PP3GsL83CoAVYzoJ+iTdQfL90wLJ7dlg1LDgaAFFuCtDDjZ7hjp9x5bgVqb8mrqAXeHrVaV+f/+WfuW21rJVs286C8NztroRwO+aSFR/BfpwqU4sZxrGkDqBspl8BDXWdz904kK3XlqsUr+oS2gBBwRR2yTqPTqSVrMkWIdfqmYWsTgD8pDobjBWIDWtTlzhDbmSbU3mZ3WsBIfy47wZi4si2rg5M5iBrLFZTEiHkj3qsn8BAr87DEoLrY+MyMRnldgyPwby9GHxxtyQKiIwHLWqdG57+rAZf53F7EtnWkFRMSV5HoZjeunGzV1rCA/ijElBs9N1NzLadyE1Ykfkkz23dKu9m1iiKT3F9N4rliHsYkB54dRDJ2M9zLFy4xczXQ6Q9XSB8GxjYY9/RqIX9jc/pgVH1Z1samQK1pWner/BPRrUXUH6uXBdfTgnR5XXY9H2GENxFRq0K6TIKFO1b0w4WFeVCtGn8ra4bl5Wjo0jhy0GzE9EH2GotfHoKR2S/n3xn+18YZoTi0CEMfBUkVI2F7npY2+iipIBkVLBpADUO8GcD9yO7A9U8xjKUXrdAyxBBku7Av+ojIbbt0FHQqimH8OiWFa9ebPj7JM3TOtcPLQsE5jERAJk4n+uN8g7uY4/a6K89Kf/ZMTp52KusArvZbUmI2LbaySXPz7sw16R+a+Yoy6luvUu+S3IPLSntRXg/NwuIpmcgdEpuVmgsFvZsyJkD6cYAZ8s4r+2zSqGuw3V6S8uZFF7rZ8kB2grRpdPj6x6L34HSVThB5I6swkyzGyeSs04og8IbL8bS3pSf0QdNnMEpuDvn6k6Dv+/W13me0mEnLfP4HG1+yi0p99Es7VurE0iEV5QpSPsXPLdVCFPgnuYc5cPF0jn/GFYPL5g3F5e0KG+zvce0ZVbwFAcMNLkDRxClgFnFl4PItAFsUfPl6kAmQRg3h6788uN476FeSq8L3qXSvh4abmlHZ3KJo5RgdB3xHLwHyxJvgTIMqrE8Nm1jD+hc3ao1BzekFj9QtTvmeKe3matXiQG5iQWyq6xvuUZLiYvZXesl505GTRqPsn5LQQW7RzRZeizklHQD0TL3V0zqdrYCCnU6xEfC+tSgmAKuADJwqHSmZccZnyVL9iSZ5ZGo1pvoKMLYFsxObil1yQvqkvaFvBPXEyFAgwMvNHpIERUE1OEF+iRyaqN05BsMX+XdaletAEoEhPFTEjlpDQVlBeF31wKGroN+9II/Ga9XV2WfSeKVhZaedK/Pw5bGVma8YkDxo8J9ddeg+UWSZCQbBhe9YvCJn//9/PUzutrpLErkoxcpOMZeRPTnLipQIdG5REJ/MR9Cra6mId9W5Ts9OaF6+WmFKV4ksY35DF3keeo+OP7KqsKjv0SEAKLpUoT+79PDre2/0REcNSU0ZnL2N8S6fqplrQq/B6uXOr+5vAZQFbnONpJT3zPQDeadUzTAhOzKaBnRiTV1dMZtRu0noqmDur/6s/Q4doKzRjP5HkdS1W63q/U5v0u9B0ACVgiG6f/FKzOYNCf55jbw62durntbfrOpOBKBltlM8dUfO5MXUKBgPz14FVlwzqb6NPDiX97uBfpgldjtVAIwodsEVAza0RwfGkRpx5mFWpFfRKailr2/NnondmNKPQ9LQI/ZYDyyyGL4i7Ojcn0Ii/IM5tpk0vYyAPCZ5bO81AYoj1kla8KsdLoE69zORLPEcDdHLnWkFniHI8QYt9drx6yHONsklDwv1MBxKIrQEQEuqviqJ6zpxjYC8sXuLwTbOPLcIW29gnu6ITVvqLsok2qOhigz3Wu5H+FSlfk0hfQsD846+4y3YWg0iW4stAxXJTlHGS5rHD6LEAmncU7ZXj3RKkiUAkwS0OL08OR4fRLN5Tr/m7VJ9GuB7Ty3sjV3xiMPvd301Gr2LzPvP3ZhR748IVlwvkiNCi93yK0bV2uLJKONobodkFfDN63hXzI0UmmFe3Mn56mjMETo8tQSSU/ZYHuFtAaD4Mc/uAv6YjrF6Ark7x2imMBhDhaoHpYc5XJtietVbiBl/GvOOGvuGunjfnolirF0Ou47lyQ8eh72npKbJdFfdxksCf6K9WZyRMyxAjUXLU3+xTPQcgNobQtFEvuQsdlIf61iWg0REAt8WS2H/0UZosR2FLpHZ4TtL/KukeDR0MP+K+Q+uH7l9REGN9/gGM3Eya6DpOHuQ7wWG8fcFsfTWdw2D+l14bAilXK2iwOMsLPa3rUT8skZZGAPDt9qr/QdvJlEDphri+az96K/UQmoTJHmZ/2wFvLZOOEAtTXyWdZq5cj+rE8ELFSI5MgadzLtwNR5zjZLtQeVUNAfKaBzjCK0wPV84cOWCECqwDbrFdGt3GKi2OTU3p0vDvAjC3wL49dBKDopFURG26FsKCVoW3+fB1Xjpem5PchWa/HtWDv8Isu66umeodOjFl+ACULrYtXKxSVGOXnrO9PDtOhDvRlNVySHB7rQu5/r1Nej5Q04YtxRvcinWmXHo4ka+FO3LSWBz+cEPMqe7a0=3CjLA/Q0IWNHLjwyYZ98kGnqWWG4KVN0QSDOI1wRHgfCj7uMR1+xOf0kqKLgmK5dJ/zvcdZYhoIn6OGNhkEaA1LR6/Cv0tkSC1DwUsvFCo7w4JNjSPQY7wz5eKlaQEM47UIAwfS8EwA40KyYd2PHeUhqG++ZLi1UBqLbPei1L3beK5lAhUXwiRx043FOMBtySq91tptZI+Ir18s4K5n/zxtqoHVSZBkktNT4WkLwNU6I2WdUVf9/g+LjxHBdN+wLCwT5xyFX3xa9Ry0pYb+H99oC0UjeffCqfdY7XKAWqlM1czcVcHgiKapN1UiArR5NuGIrEZW6kdki7fORdwoAJOp5An+pPTMzcsdjIxi2ULiVoRz1qpSLKvf+KYapH/AINDnNb3yjxsi0/KGUpAj8xWwMIxuQ6b+TUAO8ly7auVwPqADjpgW+kX9FVlX30IRf1Mv2d+pVec+QsLp+m6VtFcrRZHDsXr/7iMn5U8ioojVewYpSxoc7czhO+Mr7ZLnpjWcPodRSiPrqJrn0kfmnFbho3f+pl35UMSpyNJxAGpMjXReLr3wE3l4pHGzy6ddtb8nYNqlpl9EdqzC6k9pWj1oSSzW5R6dZQd/Ua29kdTONe5bvLDeoMvguGWc4R6p3cuxDZVKmUbHR/jQStFu/PlPOadLGCUBp8ybYUq1JaJoXN85v1P6+f2DW/cQCnzm7lkFTzYj2cl9B400WfevyYHFKvIJkeOOyNC3MdQSxbnDOjM5KocC8vUd8tW30gicJXNzv64ZcSsd4kSOGx1sW3kPrIn/ztDAEHJhmKppCRwRjKwmovU8FY4m1JGC2hgbenki5UrDMbjQwu87MbYUmGLsVZhgjIge6QU2y9p6NLSY+BDz7u+QBrZdlDxg2TF6dDYf2poyCkZwn/Yy7uXJrLwNnAjEDl70AnMDFBjAXS1uE7ZgO/MMjg5OAhSLyiw6KI0ppa5I1NUsOk/hbt+Gc5VOUxMVoFl8RcMX98RiNKgZaJDnJScmoYPbRJMDgMkau5rNLBQYQdYGX9WW7MFqfUO3s1Fashu2MUno66UE04RyBuEQgm413ZJ85uKWjMQE40d8xQLVOLdZJK9XxHwM6YdzCBzEX+CSTdc+vTJMV9rNBgN6LF+SQKWOFY8UJrRzegNT0xBVbXU3qN/eOut6ILacKt4/16b5RnSetCQQFiduPgswhBmXVLrGDiBha/aqrXTNXJx5ViZrMByajlqPg0Lgnt1TJOU35ax0R+ugrOmja40ZRbtFQQMf8jOhlQSy4WBO9J1GM+9+tCKlb2iePzMRgR83RsJmhbF1Qwp02dDTxYVs/0WNu+fYDYVGOzBSs1BkpD/ujUNQC15MZg8UYaCJ4+k4pJYxQyn1T50Dz/Ye8vpB24MUo8s4EMWYlywaXA7WQEf50/uz85d4B/LZ0uWNwkxlitBshoNcv8c07pa06Su2qwCFrIEPP6mNVmAzE6U8kdgEfK4dTSiK9Z7kBgPMIPOm9qFG60EzQPmOq4Uwk0dkamFaKDN109KOgrDjD2NPogx4RKG6OSfQqHUuZyx/XcMtRWG42sb9JQYrz1mi7LpT/Y53M71BaRgQVW89LKxIuakx7TkwYDolfv0YLFUl58qULjnCb2P3CdPUo9xOv1KGwL8bI12Gt/w0RCYve5TfbyAUDOSNC/RCto89n6tjYLLk5gUQPLbXtrLLxu/3wGbD3qnPYk/730tzzrgJiwpcgP44uxudVjtocMP05yAGOEB4brb5N9IlFCWLDhow5ruXOX+qTslEmX76WdupLC/6/I26atCXAhru0djRD7IVHvFHfylbmCSunxvwaRvKdrk5Frc1UbrMHxJ5wf5q7Vz7IlN6qMFvSOQvVb29PO2XX1NVxkLikrfPCruR020KqV0/5WbCCx2KLcgt8tU53rIZMyKZAlaVpRsmgEsgz2L/X/aaDZKtZclmsJp37O++ZkYjEKk7OM/DWFm5HNIao9JMdjGL8nkKPQG/+R6Sks94dqJ8gS9SjM8P99iwdSQxz12WaqawdN1CiifRtMVQjkinWV2KQgVpxQwmkUHPpfvDDCdMiSXdUE/oj7KM0wgnY5Rmf4w5f3MBIGSCHnzMHmRo1+rXoXF5HSLp7nXSFD8PDk9xPSPNiz362HDJzFCUdfSUrsIQF0mw716tiYxoOzElZw0GiAQFWe4SF/dJKf3nTY00vixYFgM+mi4hdVqdbIwfzZ6P+qNQ5yM7In4Gt+dIOSrAtWTaAe+Bxps1J8IDybgw+82A+2V3MhUY36eYF/z6acUoYbtolbYlKCti4JkEB9lefdH8Rf1jP8EDoXVi73BszJUicI7WCMU5n5zQfmHNyZjtUv2EhXpjYWuuQ683j6+QgI0G78ns9nEYoUnb7kyOBSYqYw9M2vGdvnybD9sAUs8gwL4osM1ls7Xqfc0qSOyNFvter8vlRvwDRkW0J4ia0h/diF9197D/8ALYSESfi06MRoo1O9IqPSDao9bL+QzZUdJByboyKZMDY1g4zjZVq0HrTMMo/LIfQnPxcnCy0nU03QGGqDVdV9HSezcnAFZ6DRAWwP4LBrLYUgSTvsu/HxNrFaJOxlIkKf9MzVZ4oAaQCDmvCHLyWI2OpHmFR7hi2Hi6LATh5hw4S4JWUHEtvpq32Rw1/Sa0B1dFXtZ7EH9V1rtbo3xDoLDp1UER8dNzjh6yL5bQz7xEBadZ0pOUp64Qw0y48ObkQ4hXL7r1Ig1a1P+mwKih6YcdVckk9K6S3x18NW6ntiZR/eGX2LNqNo5UdMjYpGnya/FMp5asG4yY1sSCWkqDoa/+iJrGtO28aw7puz8cXb6h3ByhSHx6ksMbU/dTrpvp+lTc/f6LfCcrBnbnd3spk9USnKoEyLzQMqjOFVhGK7lwxbhQJ2t0zMpmQ2vGdZa5unPyUZZ+S37gYKph1qj9xuGImMIyFS23xDvcEDvcmIdW/5xRL/JCpEc3BNZBXEc3O8uSbZmJaXrjTIwxhcxeBI8q97hFGpR95sSJW/tkT7Vr+lvnIjc+kTznvIOni4+es/kqJbdrs8i6DzEMlSMf3Evhsew7v3OayCgTMf55WeSBiiBs5YYVbreAwVqYcZvRyb91V5QE3c8mHVq+HHMVRCl3hMce70kKObdkyxrvaetfaBAGOvQ+ICi+mdpetE3PZlrL4m8AgbxuJaoUSBSChDY9ql8AYe+ExYiFHLm+zSOB9IGtMLoYcPOC+fiEQtQHQZkpImXb37bYaQrmP9ixjv8c9pYlGScS6gYWM9+jxQGqMBfQO6JIQHzwsLcHT2hYPsf2WTkvTyThWsNP9m76Pvi/3DbWkzLyGMc/2Fuvl594U23nQZ5B/NipCUqshijM5zwAwNStK/AsY6ptGMtHv/dkPx5NASL3EXAqeEA86ThHev+yNnrElGr/ACdHpmRECCuJqqyOw0v36aq0ugfNRSUnq9uG1vfq1MtG2bxKBEzIfi9pk/RMD4DxY5Rqjvc6btQxyA5cBVpLAtgsF9TgC8Ed+GAwxnRHoYp6OeMmVUmz0qkxs65DDOYCglmACZzs20lZqEjTnklvlkswDHl2rGgOVBnY2KqAJhzg9cLQHTBGXoNNzg871NdpnJ3yhO/Zy63sWIAKWi+EariXK4/ofwgOvnu93sMm2QRp2p4/ngtQxi2ByLWQpdstGXcPIA1hL7rnOlx3WiINgMTlXhCYhxOSvOV7k5grbOB5O28h2CUjthiSuZ+jPmlchMQ6KU5Chr+Uv8KaOwORVpIA5X60Gl6NDdvBbvDGKPzLyKfZsnQ9OwmJFVK8ptxjEr3sSQ5bO+zqPs8PJ+/i4a3Aq/qlkVkDlCGqLdO3/5UImQb+5+Ih4Lk2rsqWL4Od/9/AfrPLd7earN213/yIBZDTrQl9dp8x8tUmuHOydzypfUaFMp3H/agg3d/Os9QyMgT0akzFKQ4Kqba1UAYCDYjs2YPoC1n+vp8V6zx3asj/xTDeouPU+K2LywbC81PKRLBjpNMB763MjJJIoVMSxJMLceSVDN7CouY1siNEyyotQuaTRyP4JIoqElAujfzKf21e3+rxlJ/J9H3yE4odnIiZbhBOoedigqHF/UioDOH4UZPnNcE1Sn7g+Irs3OBkaESYGzHhml7YR6XqJFdI2v05STbPnXkToKnw42ad4mWsQtn2yjeuOMq/rcB8g9p9MKnGzk+HnPKTgs5Ck7AEVdAI6Zq3ZmbgNox1lo/5vtjHtZnEzv3ltcolhfw0bHmhnoOaGWJubNy/bkLODF8mkMADM2hcgJESduQBnnjh14mouYhYzW57s9TuDTzJXjFx8oNT9LX4V+p5wE0jJLTZ7aQwmmNgCv7GyX/DBhWojV84KyBFNDhWPTLJkUWKf1NY8bNO6WByO1l1zHzqAfhPBlM4Q73sJSylkAg9WXZ1blzfGQ7SgdDWgz1OxuDK3aF/UOHu2jM3SzYa4zztJt24V+RlQwEtfZBP9XorqzVPaiiaIyPGHqVteclZpqJWlrunQm5R07q3xaded3Qjl6+zZ54Qyx2n7TVd31yv2C3xNb8bljoZIigbo18Fl/XUBeHLbBHMmcJk69TlHooICTbnKzR1ZbCTYxrvtXxNx11PQuEmN2sM55rRCb57gbkv8MMjzCNh93Jf0E3yJCBeg6ezM0uSUXXKqRdsNONAI78MkXB5Uz3g4S9Bj22pUX4bRF6dKBA8KosJdHM7UCLTzPYhCai+YWengY3DrKGthBRIuvpO7+ueQ5YouXk6kmNVnzPyztQ5YHWHrrW4A91WzDRTmMAWXYH3DA7nNIIK0ji/OAh3OxbORdbcglzXDAThhCihb4OxuMESG/DQoXTGy6C4xYne/L25Ijb0a+86MXHb4Y4iRvwGko3suBL+MR72qN4jrH+EhcRpilzfnowQ+77Cjp9DA4CJk1WhYHwdeGWVvICtFejKGjfbmG90v/hudQWVxARimSujucDcRP6bcjUv0L2eqhOIEL70tHgJIZCVu/JZdkI2OkFGZ6UEJrgUKI3LroPKJrxjV/1sGAqpqKZMvUJSazRcN7xuHvCKqz04SPhMWVvgbAKqKvTHzY+75uPDTNY+kpVVKv9RvxH+WPaRQnRFP29kFLxVkTJKhPjB4tYlGJnMWLBvYxSWTWoB0tCAVCnQXXuRh5L1p8m2xWOaOqd0eujWwXGCykPjO0t3Y0mUK40iV8ZQS4iAaNJOlgm3pyt7WMSpgFNw5k5d0QPhr2jInVmAABpgjnwmiHVOyJtJ0BeyHYX5Tq45dUEK8LR6u0HzXaIEmq/T21Bu7QdCOphWaPinhE8389Vhlklqj787dDNumcPM5JpRbhUj9lSZj4dM8aqnwzVNJIWnMl/zpbgcwOraAyiHxKMZa5lY8lRfHUUbsX8hELesPtYTfd3cVGoE4Ogpr771AW5bLrWLlaEHApaLdrehUiojSyDKmGNWc8WerJsNfm6sBXni/NMKSEobSsFc05kWiZx4boZaCWjqEckkNCYjFt9X796BWgi4W5HOhMVcCDgVAczRXFiqhLzxxPXmoYkkgLF4a++9dguwS8Wmb90JjePeBPR4RAlH0uHxnefr+6ZdGrBkvNMmNIZNRrT8bp9yS5BjT2Yr0lzrxhXp19WjuQCO5jETl2aNZeTbMNLJgUu1MqPXBeI5Xef58/nfXkFK0YtAo52zruHn3hyl4v+oU9Ae3FZNUyR57w/sWWy5q/osMfMiAoWf01j0ZxTWVaUZOmL3sj8SIVISHacNeK7srN0mDwRxXEZmsJG6p/w0DpcWp/wrJYxk5hEMoLk3UfyCSklZjuT+eZkOfs6NcXuiCg/wVgKNw+WQXJygyMWesayidCuKhnxAh72wt4pBswIHd4gGa5snlVeWCocNtM632AtDMGmqo2m1b+c40h5xp876HN3thc6F49oh/tAOJFMPyLeuoVR2y3765jY5Notb+2JuywmA0zrpudeD0Rz2t12MMmORWb/jQX4oHf2dJsLXOHlIg4gsPfh/Tzg5DV1uRkjJMVEB2LNWKMa6q8f8wUaBhj7B9YeA+fYBIq/juxFW9vWxt4uG33RYoskmgFPqpx6YxDVlpmwdVSR46jSuKZon7LFen+UJFO1n8tFlJkElbdyhmc4mR4ZpjOecuDhKWw43KpHGbaOnOgxrDRyHpfCVWWi932rWnIBna/UntluHlamHurrW/znfPmCLDwgNacIN5UthkCMHgkA7+NRzS5Se0SxLvXRdzWoI6Hc24pQ+JjRgqimlCLjtrkW7gA7AQ//mcRbXLnIa6rJY2Wk2zYUzaC3rM+DvgmN/3w4S19cWHWWh4Q6KyHhF0FQU8YfU0W0XDnmb+oaF36KbOIixBQjJdiaHk1VZclCB+LKimRIRCd7Wf0TSMhmzL50d5ChgOqC2XS9gfW+bODmGV9XnJ14yiKK7XxyMcwepIWDm3+7XXoN1rinxBiccyzQV6pCIVvqqrV8MFHRXxF5dygQ8GC6zEGAcnsldQdFPY5oQ8wGAqHk13dXkMHgwWptaI90MmNl23XgDiHD4plohzn3iHdHbOTsvqnXYVxPjaNz4k8NEaVelLMdNwvUS/4KXHOcQ+N/5fidZgDraYQi+LVsjTXp8Ood+rkXjwCbIrMbCJsTKm/6qvUHvvRxGlcgW3H0hhMXC2gyuADagWioaP2wDNRpxwxlcEte7AN3GFByrm8suyhk3BD/dYQcUvOQ3M5RpQtO8/Pi9xO2XNToqlHax+nyltit1iAah8nf8hrk0DVtB9hRx/rOHtf/XW32NFSfUpi5D2dVsqQcmrt9NAqYZY5z59y4QZzSuk1wvY3NlW8JbEBgeiXbhF4OXIYJEn+WaxChnO8tqKoUXsN0kgIRNYP8fyIidLJKc6Mu7Q5Vy2YJDJWUsa/Hu5sXNDJMsTtnfEfOIxWUhEsS+n0nC3CppWwp7diJBQzjCiPw4VhWbHQX55KXnVLG4olTLhT+83npeSQvxs2J1W4bTHzqxQlXRSVgtu00VupqPN4vIhpRbQR7LTGoLYGwYA1y0ft0Rp66VopGDYWdT5zHPhoA4zXk2k9Pt5pAfsRbsTHKyB2tVGYZlsOMe0xx8Nigixl8b4625iVfa7QYt8V3xJDv3htkUzxkiP6acEmrIVPzP58B2Gc+Aqs3HhSEhTQt+gyxVQZsuAErpRkU+0Py6U7oDSX7RsTK3nY1Frs6FdJOZRun+ea77ZEfpTx01xoq+MG2eK53K3RlaoDW0nboGfaniCFM0PcXUwSiOUxJzzjZXt5Dq2C0g2r+fpucQ+tCFgU3/+lqZTe17ztjM3zWnP6NGSS71qPOOD3ZWo8xH89ZtaH/O5MWMsx1yBKMzziiKCuXylsDSQ2LzyxpASJn248wPKwtNzoLwbK1jvt80p9UfMblMLo1Av87krTGaoO1JQSu6m/v3BMvIASl6oJtlCNTSxCQUW2/nbo8eF9mjPXI7vwm6rFwvTOSPJMHKpmh6Gl+G/vCVd+wnUMKrT7iV9tU0pSYiqQH4FQxIQk7Ga0kJxOyihbC6/MXUljH4OvEOb6GN7bCAIYIyMPiXTfIjGNvfb2RGtTXGvUII4k3rSumG/boxuEl7Jzzx68n8Xz9fJZUHJr/TOUvOMaCWMUfH9FUst8RItcay7faz5AdMDLxfaXP5mNTZNrl0lnqb8H1RVCurn5OPhhD5SG6yjV+VRk6oEy8AZ3piHYmmZpp72TgvahSFRD2T7OnHI8bhByGImMsZVxhaPiAhKjSIN6eFn5mm5AGZOHCcYwQHLnZCy5Ss/+KXkiFBxPuwQJQAjTLHgTVRtZwL54L8XtKdmplqrwgM99d5v5d9a9LwCDbDT8vWMNXzNMN1F/Yz5UdjFdm0/5m4mnsWOWDQUdyVfH92iAV31cQfM57eY/SsYgVf2BJQiX1MpUh0DzZ0tUsSt1qRk8TwUNfKaNgd5vmlYU91ptHHrzVdWyhUdhBW5/ojbfmmwfpslSF+UGXIJ3RB2/KRAEyw2/xnKj6kxImXLWnXXskBfeL0eMT9kW3sewbkGG4wnO8JwR/c1WGIw5GzGqcMjlMu6m9y4K2jNcoie/dVXCeCQxAWpKjc9Tj4HMo41N8ojWslLBJh4L6n3Yv3N1d0q7byPWwmklxrcdd/Lbv/PR0nHIb/vLjjumHYN0GBAna580L1NsunQP24K29YEgZKUhVe3iC/j2aF/IcW+9m69ECay9Rf+wgKqCOwif9cmHfd5SDnNakjYz9VRaKNFQdF8sX3u6gpaB6R7H+IMJmbc0/y9tzYEh7NUFacBT+4vqM0Ov83IIcIrZaGEldCwlTJ1UR0LTXPutm8S6TEjejQKthFhEkuGQfMPkN6fZ3abLUlp93B06swweUo4JR7NwutCpsufykmmfo87g/JJivNBxsx6brwBklBeQZ07NpbZd/OshG1tFHKuqHqmC4DTusVb0eh8REBXZbOCV0Sw4oWoaR8eF7p1qewiukhjEpNe0wXCslPNb63daS8/jiA3SfmqNu8eAzjTVENSm2hocJmgnu/He35z3p6Im9hA4L1tyGMJpFv8xHhUZPn4FOtbQf5WV9YjFKuH7ecNKhGFQtSZvlTvBIbhezbwjEKwjIS76fmr0LtC3ljKVwTd0UKTVXHHjgrocfc1tScEIHiOA4RRZxg/nbSDTWytdHM69qgxr/Bzyy+J3XFU1FvlNmyGpofyUMf2brRj40lA04Le/JsgiIWJxOHlBtxGGeTQTKA3vqPRR6BDyMKT14M0K4Ghw/l6Goxb1wSse/AkZeiC+Cr7lo+/yLUOpfNCPEN0oUuQP7b0ElMwU6QRPBE9sjKKX9l+QiATV+10rla1b5I/FSI4/FczyFhQh5YhsbBORsQ6Ijn8BRRmSKwU0mChwX2cy0EassHytRFXSE3FN8NKNES9ZJBR94GztIHqmHidNoJ/t7dqJDz3/MvfsjXdAzbs84Whx+N3naPHZmXpgcJ/DwXjMbW9TwX29W4ZTNZxnIXa/8qAFeudT2bGcqOn08DeerYiL1SED36L6NsUHtcxGEInc5X48uGD1Mi+zn5yn4FGWCQtuFCGAuCDAg1zcp3470E8tYlD3hPtd/i50tOn+QQ+m4LNfyw82tKqGZwEpAsZvFsnMHRV2fn33SJMOV2wfNEzFgWYK0/x9qirkX+QN3HC8esmsD6MzqF//ZtCDQhJfS0JbeRo8+iZv5KNRwpn5Dym0NSnGVyFFC6hcXAngDhmJOkqjvz95Mkp+fNJABA+ME5FH+o8lZ4OUixDLoNaNtXgNVhVOvfMARni0lh8sZeEoLUor+LBDrYK/ZVHiOLHcQoWgTO7xZZMvV8lTIHn+HXXq1ldOfQFy1MejL3DiaR5N0kPk/I1ObrnOIXwXEr2qXI2Y4oS6VCQf1hUCi8QuLdSaTTEVO7RMf3I+aUL3vu+hGUcyGkBdNGp9Lz33rvfq3K8eSpNW7lK6RKAQKJijhqx/ZZkV84VBfHB5aYBDOYwRSEsj1mkJEqQBzYLqFtD+s+vLEu2IimGMpb37r+vDroiFyG0uCTso2CMmpLxM8dq8LUpCl43rFkGRu/3K00RPrO7CqPwXVuajb5H8HsVbmxBFt5mQl8F7N8ozmEk+UpGfV9BnooXmhzs7ZsOQ1ThYyzvpTqO3WREpMP7AmZ/UxH3ut37LiTOMN7+s6DoGKZjDZoP4zxTNegAxA/+t0HuvqpcjjZIQnnF7Xr1+jv5uFeFkdvQLnLHuEmpGRD9UlU5p4ZlAVtBVr1PP4lTZ+cmiyUBLRlJ3uYZGXhmoUT23noL/m/b8VVyPQBIBYIfJOh/K83vNw6JIqSXAqwsXq4Ozn5pJnVBXtmc5uhW71XOYh7FNK1cGs7+g3B3BoEcsdK/XlROSG/nrK8PWLHmITndcqHCL4M8rL0B8jdfcxbdg2KrP64QTwfdkF963Rd1TE2YqtSJ8394UmHYonr7167mdLACvl7SfV7w8jZXdAXmj4n+SEka3euE/zQAjWDkiaF0WSgxqZMyfMbOeOyyyHlfXG09aRjN10sGS3VDnBBfa8SysI1/FbvApecEhmltGu6+bdejZCjNUPG+kNdAeM7fs0/ExPADE4OPfmkL58I3BJgemOMOU3+/7F2lzzoEfVDDex78Uv6v3EgidUMxzvoVSh2Q4RLkuOl4hU4/76JcJOiCCe3HKEABk9Xtak4ITMnzcD8Wg3589iTydbnl1Knsy4A8VzFjQH992w9YZHvs/e0DkJWx14VmZRoBcNi4wfZjXn4TD/PfkavmrsGZGQe8dVChfe+f7VuXpbtwaNb4YbclZGpXG4dN/vo+42ydA4c2B25byCjRfZqL5ivcsGOC7/gUyYTo8ZpPvd1EQztCwHpG8a0A7gc5Xy9uPc2DXcvg4+moD4D08e3X2LRAgonO7En5qVxKd2Dy0sZkP4cPDwZEY0C4ukZprYZJzZpWdCSbrXxshda3DeFagMsJimxbcf3SFd95P0EIMOtZdnOuxG3qS0O9ALK0CpjWo80YucygBvTX8Iyw/MtIpmAi/geymLKVtOe/x44ZPu0kmKThHPv3vfXZYWaABKqT73c47Ab9f58AJFCgH2hFrKcbXAIL69w+fyqYjnaj9eu+Vj20tJkEnxZQ5fix7USNCRMQY26HqHFEIHLeVSolItv4o+x0P1Mrcmqlp4UAXVL56lKb8GuOYx2k4jl6sD3CxO13D4bV1UCm0f8uf0rPHJvhDv9EKYGKK4loY0rVwTfvzZkUJpC9Jhgb9dOVimbI/Bk8hp/Mj3eZy9Hk5bmp20xVRQX76TMDdJAzgCl02Z6f2mO+WrNDiktIFIO73S/ggSQw0pfXAV1KkJqdckxrkOEUtak234uj2GDw9ESDaMhZafxQ8/qA/yMZkORG07+5dOoIcm7c2+C4AQ5ejhGVK4FNx+yTt878CBXLDc0=F2OZQHajXbZBJFrs5boI6EzMbC/Wz5WX5x0gydB7Jog6lNKeqrEcgDDXjmWRct0dYZOJrrXmrEBa6oDg0F6zk3U5uj/nkUuK4oXieGXPbIyM+GxtagYO7iyKOK3V8N6E7YJUmbSAIcQZBBBYPlOc2BIUgMKi1RTiHtgpyzqeZAoclmnFzpYWsWtcbG/6VxLDh6DAU45JHDmcaiMlWZUQPqmy7dx3Hkh112lVrgjsfz1a6VOJuBAw3eg84SQ2altnWSP05u8xMxcAYvQ2vYg4hY6z9jCRV+seoWq12RoJmdiX0VuQDDphCijPD1buxyZ9CSSPO5VeuUJwqDTjtbgguQVAGqIvueiyQXKuxfd9xrUWSb42OKrMG3rXTWlNjUKCCAf7c5ANmLagn8ttDpa2/DjQwNYE6ImplOCQpu70LG+Jz8tuElT/uEEZ2kkbllbOH7uHqUYoGcgmgO8OBmPaBp5o+/UOlhL7P0kY/NA7wwPegbvYF9n4lTBjPFwBxhPsq5zjgfO72P86zEjwIvO4Nk2GbhsgYaYFJy5yRD1Rm37ycrvlctfmvrir7LNCpJRQd8EQD7EIEgEABdFg5qkEdFrRhAYmOZR22QYzWl/njNcqovE9wRQdRao0onoiW31OAH8EOreAiW5PTjDuq1/ud92yvBkMZxaZiVL0g0f4uy4cdh2Lr1oKKw5eMGgSQN/eK6n5FCE3F1E6IscJYISw8mjOSx7NWDfLKbBHsW5mvbn+Pp3dLy7k1yf0Hom25/Rq1Q6JHnQ/sHAKdU85BO/VfUj9QCF1uCkgtTqV8eUi2xeJnxUfMOTdbreaVRElr+S2h1JNjkYD6OO4i7+082QBgEGFi5s3j6TSOpBO5sUDVxX/0Yggz7QYKhg8X1EeK995eCT81Dn2Xy55czMhPi+GCr7gs8poSOQK84xt0TKWKlXYkXHGM5P1A3nwTpccv4ajeGYmVZSwqUjWODcqdz0a5uHjaA+gpkw1bs+FWyGDTOaJZpTyzjK0ile8z+DExWhbXxAjYubbS21E2wtRCImPPyLnnPdma0Se47iP+UaO3Z2xT4yKQ2EHdLpKlFz0oURoFaRc1UKSEmfyj+4JmDZjJjF8SrbBwf/VUPC4Ikpqtok+6pNU+oVDcwPPx939hT3jqauW7I4WCFo8aYlcfcZnk00gJoKPYNi5kvWLFNSo+pEAuNi1y+I6C//r95mOssOu36ycO2mwfNVaXZbfCk0uMVYz10+Gkim9OKOoZQ4K+cNWuTColzyvBYvluQx05aZoOSRhf2r3RvvFWQmvj2XgvLNrW7pTRUioXQEMhADmzRsdDe8bs/lMmwuWHRMgtzXGicTfK97YcKAsHZQaIPQD/eiQmjkSo04ZryY7GnWk0Lv/aAHbVu0Xy5tHYxTVSrU+73bZmIdu9G1StCeOYi77qGXd4KfZzR7/bnN82mXq9lmN0yvWTbYeCoS6mc159kNUohgVx7NtOhQQbQUo/M7CkG0yRxXedwY1LWz8SueWlkJC+yfW6JybUQ7jstlrt5zv1Ouh05Ft9Do6CRMWTqtv5UZLUnbxTLwplEpzzjEhFyKbBYSiW7oR9AgD4iSS0gSCZDNB+B8DBzA1kTYgH0G2ujd7Amud8Q8LpV8bigQ5U0o4Ou9BignWosYQz+fzDqj5oqibKYd/L5KAQQ74U48P4FDAVVuGlq+4OoIoiUL/cwqgNR8RBhJBQRiLv0ClCZOYA/NxXTxjsBrOX0Ll0smpYqPuKIttnRZNCE2McsfQ69L2cKQlY25glRIFAIFdqD5Yr2KJxnkjkKOFrHRRzYbesunOR1l6/eNIJJJchfanjoszfbjRU9NfA3mpD25tIRgD5TF2hnBx66A4C2DjR6na77jTGQH4CmRzUS7iidpv2JDP+NVIsMpTqpOrrFT8I0dwcIAG5cK/K4yYM4L8zzuC8ohSqIeOXFl/rUdFVy9PaO+2wCqXSDP78Wjd/3IhC+B3q/ure7GkShXkfFi57guNgv5RSG7Nd8Stzebm+E85YlKQJkAVfZSc/FTD9ucOM6nCktgWYUNfjOt8Ww7L9Lzhx2L5UXxW04LLBWn8yWL0TMQtXk9CUrFg8Mz7bwLrCVGGvtxt9htEDvLBRgq7qPPNSkw3zQJtkDWo5ZgbY+cUvuZA3QQrxOIANv1k64L+3oTofZVdtGxIRzh4xAtCzXrUNjKXKqwS4pry0EzqR73PsFMvcVf0veUWfflYwadlYKlYfHz9a0COjrUbsVCMsmLn3NUsd/VI5DzJbaUxBT9jbplxKVk06ohCfDwhRLPf0opV1gOAchKQMEqQLMG6vTXCcTbF4YdiUV4XoX052PZoE1oeW98NPEk0boqjownVeNgaF/vDaZf3RXVCepz+qfLsgWAG7xAqSVvNetCD1By5CWCysWe2odzgo8yEnGBTDF+YurpO1xhdz4RTLv7vF32kaRLwruQMwsp8DujngSq2c/p80I4BUUJOPIAZFcweIvDLJcS0KPiQyxFWzV+WYnqCCA0WZLLwEy372IDis+PSRl0n/FNbz/2J7iHIjd8J91fsi1muL+JenRpcDnNM5SJ2dINdMALaYglo+oxVn/Qw2ny8xHtEeQy3rQAPT98WMuW/VMzkkRTXJie3uiPVotJoc4mqkVRH+GWefX+CvfS46BWEerVLtAJu4aAUxA1qbE32baI7KDwWa7oppzDAXwEI04V0U8Mgz/LxwLgxmp4H+mOl9pan0pXJyuftjezJ6gVBTUX769K0JxCCa/T5VmPKP0LNUkrHFaMfmU+XmV94jJtZFZdYzMlDFVMTW2fREESJjnOS9usezWS2HUwH7+r2HQqb7M7yyDYY6VHuRsRi2ENT/oC0AjW3rRI7dp2GL4oxkm3lfyIGAxkQcRbZKqPA1m+kcnPDVgnPEBiUScrNffJf9jw5gDuuxD+ZfQX7hmyuZ1uE9XkxQy5fhMtqrac1nhZcD8jSBJ8C8tDO/q7Ij5emcdol5DBdg4FUTKlwuctBYVqQ7yi1kH3fHmnsOXaZR0FCpXyFyYdzLpYEm+vxS+Asm1oqpbNH9XG7z2M0egr2AeNEXlA1JvUcKJc3bQZ3ym6MEsm/Y/HihFzNuJ77/M1oofqcL7DQvBrhfeIoFoQAJ7/aqNUn0sBvm6Hugk1LsDHwnBNj5M2wilkeE6LrKue3QD6CqCf1VnHxBsmT1jLdGI0R5knICVZL26ps5RC4iFDrAozPzoQCN7LmQ23ipUj2JsFHYprHCeIpsGf18BiMcnijlz2iylvnOiKJxqSA5/4WPaPLUdj6U1FOp2fhObcP8WbX9SaV1fahqsTkNra54+C5xW+ejZkqWn09n4umVNU47AsYLIfQawf8A5rqAnVRqub3TenVOlLjxMAW+gSmpaQ2kTdy5f3XDYueeJKZnBm4adgFGlZs4IYF30O8Jr94V0vBwTbHOcprO1GS1eOkEBh6ON9kKbvuJO4Up2l06r+x9h0HArbuKGcm8LhMeEU4Lh5I6LqBtKCDxpLXFChEheMY68pUt83DqCZ45keR2Akp9s1Uk0Kvx0zdoWhWrOWGU3lzt9Ou4oqRz5SSsmN9phWHiFx/jHx9EAqYahhdqZQ9HxPuWe+Cxn8ymaS/PTV7Kifxq6JC2qszyx81N6OihJYaLjWyZAxmLq6fsOWkkNPSEwfL4yRTQQBQiJPPAdS3CbO/0XIxVFKSqhfhJcEtspjEXezRVlYTa5qUIPhvMdWzS8iFC185JXEw7C6Jhu02BbXfpR7+KLFY3nDJw9C2L58tU/O60XF6hum24bWjAegkh3i+mtsP5vGrWOzTI7M5oR9dZxm0/bSgm3+YR53m7criUQ7ZSPnC0PIFJtESapow0EEtebtsKCoq8wwvNDJkukfu/81z7Y9nG/9VfFenWrFXIzYjF5qemQSBGvybKdGW8kjdAT12ZPn3TDshpv57EmisJt3LqlBfTtAAh1B6+ZaL2F1IWkp4AKrG+gTKwgZ+Kn60I/sV7fTPh4csLe7YbnAk+GCOBAzZOB6jE1awYsEJBWteBaiK5PWmKAPKCHGUj7/FfKADo8U8VzyLJXQVe28/ruAVmcBBRZN9xd/d99Vs3EH2romc8K2/sJXUEmq5kNyT0ahWQLwLGr9TOCj9nIRZAzcJJLYtMPzRq50oRRb+tCQ5DME+9bmKsz0q0kaR+DA8wXOsfIkaDJL7SooWwBlWCkS6cVnTN8ffRG6iDQ3CKI2ButedyCFCZjwf/pmkUEfqwcOnS/kGcene1EM8QiOYIK0xKIHCXnJvkQSNlrrHv33jvcnjeXerSgbz4kEwwWxLSffBcmf2dBMkNDmPqof3qP5jOP04HKynt73mS6zNQyfv4rWevIno3NbrmCef/TL17Ojn4aOge3SADdIQJ1r7t+wnLrATLoq4dAXxJ2phYW9BsUuIONHU3Gr+s06JMcQorMgVAmNcWZorHkwjTh1FryZHJR+JPTt7mpRGgSshekSfuK3KNCQ0whVtuz+vJaMg/T+nvOp9qjHijAWaZWl6tg+pUMIhcUiX2aYHsB00YIReLNt+SFqf2le0c5zCGPFIUBim6H2xre/3zXOxdbXm8JjvoI15zTyVTU3OGktV3hsPjS59NlhWrmSHXT1tiqrvxQE1QeyUi7Hr0ZHN5zucTqmJEyhEp39y3c+w3Iaxz06JOrlTB11bonAP2KcG/4pc6aJbCvQf9x4pWkY58e3q3mG4K2u/DIM6nzE9WqBib6MZCl8KL3VqCqnO7b8YXDhVIPMUjMJa5gfurcdmDdr/Iw+EXeKweSgXU1y3NJ8rQxYWHGUeH8g2ynmLoRIWnO9enOVJFD5mZVgL6lLAhEq3PuNde1GiGxPsW/A42JaHUxta9EOdtUV+Z80DkT6Gt1njlWqEhPgMqkg1mcmotKqRU+ypvzY4zQDrT6Yo3iPBR618/td19KMQM/fZqqIXlBDGTHMyeKExItoMT/4qDjcZKYTHfx5FkomV2vreEy8dyjkP9WHxwId8dNCbd3JQ819RHQMqxjLH3aKzghQL2HpG5YVcan1LpV6BSKW9AKPDudi0stD/41AA7du2Mz9r71vEroM6QduvPRwWmJPScXvSamFR1dMt11D0Nl74OVLKaZDQeWSU9sjcw8E/EFlx+DrSTFnUUgVkVjzvUaR6bkHctDL2iuco/958Pz2ob/TddD41uFrX5tIQt+AElX6CFfmio5vr1CtP8Lo9IndoeVEdbNLReGUmG7Pj3hHNG3GpZ9YRvRVP64cCXvWDB6lJRQDTA/rcBuUL9eqWKLH1wyb2oDIcEjccb6Nqo63dQ/llPn3iPB1yvTa9/P7x4kw4V5YAxuiahCxqeM1RYwRoYKgnYovMbFIGh2IGvEfdQxAJ8CcwEvYh1bzf4z84umNdn+PmMth+FrQ6q3/t7RpZwiNJgiEeO0Lla1Ic01Qpga/y+81Dt4a/Mr0Xhh55NC7r3idyUtCEzTaelZ+CLdv4T/0hrV2oS3lqZaK/pkYcpp7ZPGaWD2021ubJnj8NfCzXpp5aX48Px2VAK3O02n+YaB+pI2fPf8L5J2L26PbvffXBXRCIuQKnmrxkKJg5sarZTv1eazMsv/Mjk4kLXDIUG11kEdn777kO8G4ZLYbF8yT+VcfLVPWl2JniDyuddXjjusFWK2Bim8BGeTksuyREuYXZ62+deteevXiIFs4UAABxOIc1VTkxcs9tfDlBXGlR+h6Tq6Az3UeC+ktBsBbqZAUe/19P+5hyNls3it2REiAD32CEShkysqPOvk0pwDBkx2NmSd8B+3QXhhgi4LLe1tfoTsdVKV4tyHGqxLlmKFl1ahnpWFSY+tyUL1/zf4iJvsdhKsSGe8eQo28uX4AfqhSHis5/fZDllc/KavQeFLhZXCLPmAxrd+s/Xv1fS9psce/xwY65DMhebncQ5HAsT39A3JDIyn9SMG0BAdn3awdYNAqetXqUj+gwLK10waIghkeIZ9SL1+aLj6AEvsu6aMCwuzncm3LxWUWZJF366Ls1vNNoH4mIBgQ9tI35g3kreDpLxY1+uzWr+SzBFCXTmuBixb3C+T+saXbApodXptYxPMn5Lt8VoF5c0cDTQAmhj1zJH9zAIPPxZsVOGkLG2eBgQ5qYbjM5f43nDR668y7oDCbiKk/JiZwZ6XUrSoGnZYK9dF45UDuKnhTlPYz7NvxcPFcTKt0QcQl5a4aXq/DVg7IGVwaknA/PqXjPlVcjUCKEhMWsqsNWtiYAU+u870X2Mj/16BYcYPfanGtIuoV0K329Z5A+MVTAXPpw7D2FtycyQgtP6vbSsVOFUXeovq+2kZxoEv0zZBxniiU6J7dPNq2pGJplSPWm89ChcWf1iDhkXuCelbfAgIHxlatid1qyUue1AIig+gAFYqpnPCS4U4Xsf8jJTtXr0lAKaRtprH7336HOvyQbOFqB4TbELidmF4F5jx15viySqKl68aChGpQD9wtmcgwcuovBlhiDg2gA9y0t1vSH6/8QTMfEjOy7kYzgO7yLtDCdgjwionqaxjjbBABzS0HvpzLGYl9YH5QNR4HWfwUOc/8MAHRyx37dkFyqigaeBol86UHWqiEi3grmxDdIGWEWeQ0N1tU3qZsP0bwgfskj+LUVOvODCe5sO/L0mlfwjqffuORoqtjH+Ou6rLvd0Kdi6DkpX21eXnC20Ni09sWs2OZJ8mL/8s5LkMeNtRxLNiJRbg0FpoqMz7pqhB3L6Pu0TouWY8IBfkao02DJaziovvivKZE4kPIT79ZioHJQz6uocJOvQ6p3Jq0FRMI4AMJ5JQ4K/n+TYA9te7g+nThy91dhrUcfy80A+oEzfttOhTfiuuSh6/2A0ALQ3FPTk5oZfrluQUXmFdaTTChR1nB/57ggZB3qaHRjoyN2WygRYvnrXrj6OxskEQwx++Eo5RxSIjYjT7W7M7e+Ao8T4cMxd4oxHpu2PIEgIpQtMDuaPpps9x8skLbMGmjyaIWF/TTbnHUIh7YA6MARHZlOmZLEqzsP8mIT3lGkpMswAtnL9LPXm2FlZj8p3ysIKROEZGiFp1qrM7dSc1KxDEyEzEYaeCmY2wYjwS5YCBOV8F4cgZuZt3ajg5+AV7lSl1hRdRE6pbqlLa97iO42bsUKhW14jgPjCJqAlCr65u6P00BdViRESf9OEdFdZA6D85vtot6NrzQPZ7kvs3pPN1zhDyeZ/ebXTR3thAwW1xJx2oqjtGfczxbbuqC+EFtDPj/vTO0vukOr6bMB12glxU27skenLgwys4K6+Sx/7oyO005QuwZ+7Z2MyeWtoOXFEl2+z/slGjaIG6AweFbTKWDCHQs3pNew/1JZ1mN8yXJfsZnTp8AnDj+Btiu6UCnEpJs+ofWQ2reldmOzYZsQEqaXs7fUyD5vV9Ka1R7ZApqZxBDmFLVNSUAzxGZIMQdtaOkBasJs4x+rZTo80L0KFG5LXCzDmyylGrdmeo/d3pqxzvAQYUFdBQWA7zplHHqovxKzC74d4RamhqNAyquivJWe70A73nkxNTeHfPpu2TLIHZWwj+TIaq59Mye/TXEhsTel7qo2R35Jbjhpu8wo7kimQk00Bs7oPTp1wH2P6FN8dBsEqO7Iz6dcc3HB75ImwGdKhB+mPBDSu3ru2XDjs5EFCZpHw0vjZfO0ip4ZellXIvd9p7kccUCpFQho3yFTm8hp3b6dS2A1wAwO3+LVOoO6GSP4rL1aBtFyks4nXo1Iij0wRndIlKU0yYR8PU5K/6UGotfMU06zZsGB32Zk4X+QLTNMWDnJyc944q8rsZJKOK/rqp/YqHfP+wAI484S65fbKBuekjUgMBlV5UrWRGbi9T8v2isj96IPEHo8Y2TQBc+p63WcHP5BAQLnTUbFv+Q69U1Fr9TEh7msIuGO4e8q/Ta1cz7TCA6fY2VLOLwGzKdtRf/4l34MWRZh17chab+q+9GbbdyJ6/E7KbN0t0GagVE9TryliHTuHj5mvXZInMTxEiIywRbQiSBcTOMXQHc7YIlOLscO0NqCImoc2UifyHARXHYboUolY/DUVfdbdEEgR/F2gS9y7aQKraezTPwSGKxRDBj2MfPGKpgPWtvQZtglKawDTX22XxIwpEpN6yyl1xysXRthpYfobHGApxUhsUL7F6XZ3Krz3zEfxZ2JIF55BiW/mpol/XLWYaMG7MsR8UkJd0ZRjnHCB4ef4H+zdjhGLlVAM6h8ohsBbO/LNhDnDWLh6YKmLKm0ToLuxUQftaGqXa7fZWx5lynM3ZUYuR5FYu26sdWT0AQWiIouJ2g7kgbKmrLwwJc/k06mLucn4vgw+8YwSVtpXAyvZtYDBxodB9dWSqHp1aL9yTut6WMJp0MBSA3TtGt9bl2lQu3G/ssLCm8WNioY9rXCEckvPG+4Ps8plY563rroE+TpqEEDIujM0IAo4wQvRrP0fhyXhebH4pdkfzEumM8oyMDYpPGnXvpv5/9M+64HdBdFKOgbFV3zQVIOVffIyf79rS2Ux4R/ZsKgop8oXkEJxKXcpnN6Ejiln/epBVrGE3YESM6hjFEK5M8BiRX6pJjfOJsPWeM59EEMqLQcbXOfe3twYgPlnmlSRqYu+tT3k8oZDzYLfy/hicerMUh7+HFrMUBhV6u1PRyjw1UNjItuwAorSJyEwWb4xlW9QOLj8Dh/KUmDvA+AtbCD6wNER96q66z2T2FBjh2OJqCzwhUgZ6lg4jfjrdQ5S0Nwjg8Pnt9g1H04/8J0mZ/KYP04b51krEnPQgtO7lI3RqMWFaK6HO9l7t9exqSjlFx7/D5faFmKV6woK5AgWVwNWN2GbNCXGBsQ+fyP9BNFqYSefcoeGTVA+BJRLwcfshtwyDPWstSzSaTq2JatQWjysRfkP9EDqZZbysQBSQkiUPc1WNwqHTtvJc0kDcrrSTzRenYAfeotlvo0wBcRAABV1E1dZovGGAsGIF8f2uV7doVgCTZEcoITHNV28adYGeIG8nzTg5dCR8Y2bdOqHQD2KMTkTIHd/cnfXjqTowslCCsvie8aM0MbQLGo7ou3Zc8WdkC5GW3c/AWLYgJJjH8/uLgXKBwSFgYpiAXkdvVOgo8VHN6TAGCCZwecwbPH80dwBwV/cD+jNPoOmkLMWG93jTqXjZ37QvslHtiedKMGoZVP7OV4AMmbNMHvX9Y1MDwOR6CVJTocuhk8sMQeu5ycHskcNWpKxh1i+Yp048BimDakxxAntleNkoTHlvnPvSDHXmzWv2YjkwDu8E4P3vmVgjKhBGv/m1+5Yw7xPQDLoAEyFvhklkIVa9ycGrx2G6N99Rq1HxRs0guW6KSvoWuPcycvosg/ffy4zcXjSTa7J4hwweXxINDknvCKDmS3NQ+iTx9KdOD/4e4n3OJ8Pu93QsH2lqFQo2G26iPAZ56PUwdPEP+WLtQHEJhJ/zry5rdfeerfSrc1s60aTTFnuYpBfWG/B7zIhqUTJnJnXjrKSaCkPUOl4g/OUN4Ov3tdBEjxrgQlR23cgfm7wvjr+cBQyvDWYG3qBOFRfo1r/vm8LbUBhhumX0Lia3UhJ2ctC/SC/eG08idKjRkdcCLaYFs8bOsTZoNSgrMvicihQH0WNpwfJos57QAO6dqmBLdBEiSt/YGrMvDtaluuBGz9K85bPRVqukrnR4Cxh2d3Ug4wfHTwoJdPJ77kUTMPIFOlb+bC3J2rj+uLwxrvFt3vvxRfzu/lcIRlY7Ki0KYocIO3Y+QMla2KzlRK0DSxQUONBeHWGrDcsI7m1TspM2rnRt10UuCABLfOUHYX7Xy3GkkhTFY/t5N8fLgp9dlzA0/fkMHqGn/ZAL+Uju8yQ7rTZCkTb7bB6qeBOk2gmsjZsI0ol96yaloKfoIoAO1Nt70lnJvGnLY22Eys09MYxwE/UzsDIxtKaQf+bJhwrOnJFEDUluH++vy9iAcTWGEAE1JWFSESxbjSOmFfwhVGwSdVbBAfGlveFBgX8yQfzILLraooMKQPtgYuTS45swedwsYXUt3OHkfB9Fb4mUb/qOHimbX0hoD/0anE209yJVWiGDioZFpDn4AmRAV2swd//P4/fANAUJsWKAhTXu8dVnz6dhAWaB0yO1vZK/DdQ71fDY/g0E/y+JDWfgyaqGM8co2oqHZscks9+CW1FjYOeVr1SMbGNu1TY1sldcQSwYyfrM5Hwff4R2FcPVaiZRfNgY2F2wlGstk0cpY5RYqbKj1AqTEXifogUUUD16lP9yWrkmRr/quQa2Fe0JW2PFWe4+XH/2OoPftOl6gMvdsUZGBvjF+ZODpFIdNLVmlbimEjJdG+6efbb5gU9woudZDVCDrqZcnKI1wWsW6MeLy1aDuEwiuwHYfk7+Mwz0jUVLlRJB9eXbOkaX0CQYMsH9g9E5hhRUJEAzYC90xlkzYbxeBaWMiuQqSjT7Cnan0cibEJXhzjzxH14Y0lwNUXwvJ12yulmHt4E4lp2UiNPU/qARu5qmqIEC1/0lHGOXynjPtutDoZC4ZnKcFKuj3QfTFYvdiJfejYqxynUOcJEcKo6EKQMorxfgH4MtGy0ZEhRmmJrFI96OaL3d3XBuM3coqLpBTMlZ33bWf2FBNNzBFTWBHlpAyEGKkioPFxaADy2KrPgOWvJbIlnAgpx573RKY0DJGcDX/ach9GPD2K1YBaOpHYwFz7uBrziP6onnoF1ZDGrwRBa5l4WHmJfB3a0H8as0VD1UUPXLAKPXhBYGzsJHVYU13aMDEKC+c0E7UNEO0QbczfWLOw0+AoE0rOjARP882ueTRAz50MQChMXgaGtdw68z4ELOccU6n4bNrU82UDY0SA9sDcHUj2exc8dw9UsmQuOyRWlMVAg+o+oWIagAGI9by3TVsJ1AWMtPXczjvOGJe9opdZ0RLC5DHbDmEZOpMyFRbaZTyRlozQsdSUgiJm4mM=6qQpPuSf55uL4Xb8MTGY7XkQ0yUIBntSOwGjQUEP8+UN3UCXgCeIrf3fuBBWw0wrM2Rp/++cOLj4LBdLpLsx10KKFXyjEt1NmxC3wbkxubQ96xtScQZrCieLweXeD4iT2Qmv7mERr17KWjT6fOCYBjRi9V7C0je+sEsUUToUaSVyHhCRe5iXZ5ldEz9jLi3Ja8YEdByjHa38yINru8LDNsAeVMyd+QH6IPNTDKCuMIMBQIR8HFQNfVlfJ/d7q3gg92IezWvmDoieJ5jbFpvpWZX+L6xDeo6dcjck1jwBHyQq+uLGcKl+w48L/0EOqlAD2nw0YDI+IAsJYq+d6eiY/bh+ztiw8Md1OKG2iZmsmYjpSV9Px4OdnfRaZHCAd/qAHEZDq2D5sSnv/Q007M3AvZFhSwVtnGORgomQyYhG/6CR7zRITPGiOGMRkdoT0vafnIdmI1Z+gtUzjEJ8D8TuJu3PDPhDRF2SpLOx4GFvoKdr6uupwUWny8glyYYEKp+t52Lvf7PCZ2Ei7+pHLoGb5S3VXRCBoTnOkfKPymRv/+7+2ijzDjJbiQo2ax/7aMlMHHoNS1rmdMiZxEz1lEqZ/SiyUUftab1/KxHWXfMIkAQmZn9fjMwDx06jtnGjkjU09UvfDnCJZ8UhGC0rKXlfofADTaAF3EV0IT4M6WrFW5x/AnUkw4gwKuZa+LdSF9W1CyFaM+tgWFN3JSuVteAaNKidUDR8lt8gtaaOs5x4wdMFfeHejk8jp7t/m7scT3xqggQoj5e30Xr7iWvHTeAgATIo0G9Xa3uAsb7f1qqydmT1VEG5Y/Vvrv04DLtLUx5yokFPxNs7PZw23E+cyBSlqAMD5DYt12Tu3WH5KUb+NmypeUuC94nIHqxWhikAZ/eAKTL2N3SDDDBvRDopMgwsTmXOm957x3hobKX0ukANBmI47yLRtS5erFrnW4Xt8J4pmj8YmTb+NKtEo0mFZSfRY8WditlZ4BkRKErxaDcErnLLvk+cXeimdHxI1+tZRajl7E4zv2cRv1eloCiLcAUmJVf2t83wR8cFPZOBhX/V3icgflAWHpn17Ub7bAymv0W8qsRXzvnvuUiQqpAsf6P6gVfKs9q49+mXdcXpkQLFklxT6ynXaZvsm0ZtJQ/+/9/I1XDih10RxL4d56ri7DW4oQwCd6kdsCGY+o5JwKnKPrsU70mhYChVArRroU9eQurhO5BuKjjqkapNlm+oDVGTQq7LhVXHLi9TqPHdxBRvM5cGJvKgffB95DNCskKq5Bh0UCNYha0Rq4m+D0yuoHZoPBqMxSOBqT6p7UA/BnZm7WSKZJbogxTFHTPnlzLju32Goo6R1sxhzdOTjsIgC9bsKBTfYLVjTxCGu6OGHbCl9RmRt2HIkerngnbsIZHQyKugnxWCk7FvJxHMjEZ3I4uya+6oy65xtNpgO3vluceYvjCEreeiAX1j6iK6czNbiNjRs2FQm7ZsEJqndju80QojkihuB2A+syHPbI0srPTcCim7La0fmW26SsYFtIQLGFkqgMa5iqJQmGNB47sFUSbYLfoC4ACKrZt+8NTk5CCcDn5UNZ8yd8t8PUsnLXMlIT8L6ULoyyPOWg6/OwRWsmQ/J1EU/bRZPjc/iARpSKYitj0QkkPvj6wk3WOnEB7n1y3xC2F1kbr0VTHVHG2o24EH3wLFISsA14EQyWfX/9RTpKBcxKEN4+WHgeAvrayDnikeP9hmEj/VJffnZYbxDWWxCnQTckcHjiLCI7NxxuD1U0OBRr8JDF8mCFMGNCMxXQPp7IYXpXT7cuXQXO7B+suF7Hv12VEQ5fg+1mMIPWj4RcuRFMuaAedA9wqnHCRJ2umsL1bW79W+RHIDo2eedNz61//rr8/1jtp9orYKE/ko69Ou4LmsjxX6cnKqYlm0VGB84xJwtCFQe1KYZ5i+VhnxIjArVWfmvdicH2G6Eo+jz5pCFyZEY1h78v/KkP/6qY3UwTal3Pgf74tc6LeoHc+T1YV8iCVDdzNyscY80JvTU2q7m1shAN9eMt9+BPbYN6fhRuujbWwAN0y+E9wjvQd4Wk9fDccOfWGA4IWKJv5RuQTvzMkdSidFqYXhauq4oYpKV1/Ofg4SWwtjbb4k8BQxDUMdzHAVWzw3C5uNBJD0NMYa2jzQGRhkfYKeoDxNho/H2mxOAz5DDdJxhKkh0CChZM2cBPZrD7c8VJX5d/9gMt+oe2/CughrT7+mAzSQ4L6FT5o9gNdRmwhsW8O9PTIQNXy3TQ/9wg9tCDFtD7pJ0SF/0f6iuwhWwicBVmM4LIl6doFfBOWFlwNcldg95xWOy9f4WOdEi+pJuThM2+aB52lwq7NS3dt9FvWCmEB2s0X7bNpNG7NH2lL7HzIHF4b5gsE9eX8jy1MppWks+Zb50BbGAV9dc5yOKjkSppcZD2TxzWS3YdM31gW5HEba4qiN8r/+QJwaVK0DNyF4r6YwSQYOXjz7Z6mMR/VjCbwCrxbQz/9aYRD2Yy2Gzo93833n2O5l+4OlUXMiQ3b4Z7SXXJOCQch/uEHb2KCszEvYHYeKGtE61R7yBjimrN832zYoYIokogdVU6DKbutKLLDIbieE1cdR/vIp2FKTRn1TxSHvw7Fd+UuJflcZEP9IFSWPAT4pNfgQD0+E4AOxKLs0KCayGGnq5iFBLk5l3NFrWbfaqBhszeS2+yYUxMi9tDv399hNztnHbeSriQkunZ59QTzovWYBlKTkI5MUkvghjDuh9Y4N70Rt6TelmrYBjNyBIeoV5QSw7d1PMNwYAFDd2qhK0wgtpWoqsB6Ntoo2ZUqd+joAYQYFDAPvRBCVEFOODenjrO/PshxY7QlhAk4aKL2yYbMHQUcEY9JbLTRHyUJ0DkVoM9O6QoZoiTsEjN3MkFPoep85jyIH5dW8WPISQnWF+USH3XHsEGi58SXnd6cpXM5yg3fbBJd1WpcXoxrR6O5avB4XAg8WblAkG534PtS3/j1vnWINZaQnf66Kkep29fOYlBTz+RWla9du0PJdGkXLC/BN6IuqQo09boDhQN6RLdoFFqoILCuTIAY9yQN0zMYCG0q7389mSneeIDChaIGva00YwQ6FRmg2yJqZvxmp0+2SMAhk24pC+3hxBHW1qZxTSSu1z3ZfhkRX8IK3BCwkWjB58IPQ66JSj9MUq43xyybKvn3AbJBJRH/9D5A20hdpYhzack8wfstr3Sk5jsRS+Il/URE/cA0gprTNhs9km0mHS6YeH0q4NIhC/3BYqQVDwtoJSaCIn2jo7ZmC2Fvt6VORB1fFj8nD/oVumu7QZ2fEX34wSRi9u506cWf+9Go86HPYWlsLHVYnu9EVGZOZE6If7YVSPCcaY0V8yvBrKjV9l0zFsw7I7GCi3guyzPd0hg1iJODS1DxyzDmCPffcAs+NYKouUP0Y9ljgrEbVsl3eBnxs+bVRRGEGLW84PeV+EafEDw+537ttcmFzvxmV8YRYNlOeeWv4cW0jxqox9CFzOuCiE+A3X12gOSW7sAbNuSTqd8BFXygXmwoXxG7f4U72+QfSKrUaKVO6M4l3Xn8BpEyv38gKbAzUMbwuzBUdEulcp28NewS1eEUOOP5BNZOnHw8e2pC3hbWd6dA+MHwEBsjWqjU7eEuYsvhgs+zWKW8Ksj2ew/OLNAEwijOiqYYYjQEq5LgTMLVBwdrZdD+CWpfcthbof16HmwdaPGY3PNOWrGrfNntt09Ykgln2nF9UIb1TvxVz4aXeUcpGjmHdYdx9Fi4R8AvnZBSUVJEJnvMlOqzF3zBQhdYqbRTWzBIbxT1HTzTASYmPeM1XUT6w23/iAH/hZiJYiRzxtyOLVLuWdsUbm9NfR/wHwmYyDo7EQFNwEe3kZlaMn+DsYHyXaSHZ+ugCF/ierzR0WZ3odLws9oAJ3CVgE4MkmmUqQE4k+jMA+DqEi4527w3CJtCsLnwbwfhsQiwdZPSipKlJlL0/LRDEgWXOBdj0Jw5MgnNctYbbIcbYOvXED80ZE4/n8z5FwkLMS9WBRv/PQEjv/YROA6vrwXPefTRM1Ban4ixJ6ViSJTSIFhjAqcf1o4SfqDKlWLSlvHRZri5yfDBtk8JKc3qfOHe9H5I7qOk5GBvwQfHh39xMQi2F7PlMqfROH5MsxmR71NLK9Qn4tXlErJVXaQ4aNYEG9kQ6AdIBlb323gciwIlxO3Pcu2ypTHZ/Dh1xoPy0GqhvSPb/FNL51lroAP8ra6tPQ4/uvXLsl4Afi47v2w4Mlo781rw7ZI5Fb4vVx8LOrw78LTaX+PlzGBQeX2DdMRgcqNn05NBfMspkOm17zKrclxHip78AIwvGo5NikwDLcJ8YTYVp4WJYCklZb/SeiW6GKZ3uPgmV/ILlZtJeJiLd5akv/L1+C9Q9IzmPsuRlI0VoLhR62ANtrxk4P9BUHPFRLp1sp4t2MaT9BBzUNO+Y59eqvDQAGzuE4FRZU6x5aLPE8ILIaT3bmVLREPujsU5Osxqwj3Odgr5r9pSfqdyFUYgYQw/1Rn6MHXooUQRCs3b+2NFQje11xwT/3hVcNHebibUPTJfwwqmmm++PZPQXaSUhYxUYvRSMCPi5kApgXJjm+fPMwLzoV5DjSrTy/gxQ2u9a4xWcvXND25iLv2XSxeMbpYTMx33l8dGrTKVNedT9PUD5YS7p0cAst/sBakdjXykAL2mCqIyiUAOimYWaGNamHqICN21UT6znGGnoXs2rlTHlY09GTKzCjyi39dyU7adJQwj/GMNhBq1TECp9wE2NBtPfL7QA1sMs+LcU+0uBvlMmELE7RwtRZbELhEiL4RoJ6BViE5l1vqeJrpzgOM++wSa0KFX1ntcQTFoxTmMC3nA9SVXcbYIohWODPv8IUCtbolMJ3X/ohlXKUY7a4LW1D48qN1QWi6c3wnKJ9PPAGrSbKxew3lYHeWJ2M5mlqUfRTcOG/dXY3m513lFqc+ACTTthmpZ+R+Lj4eOrOAiq4gmK/40ure/RIbvAyvVbdoZNrqMLzHcEhWBIQkq6UE7X1yeByVeYZzqVpBEaAIEQ3d+CF+7I7DQC5l5mbEfo91WYcRPF+DA7rpTkakN+Sj+NyJMWmpkgxl+knrwJoONanAdUCpd11SjREgADx7MoW9yEHFVdeJhRhSdzWRYk7HzJgzg8ZehJKgNkskmJJLeL6lFNhRFP8UY17UMCk6JF5m/f78TyopJ26X42CcA1FQBifYCQ0fWPTzd0c0wX9ajTUS0mRXrWWsurqRC07KJSfJDZBOP+X0fADTZ9zEW7qWF17W/CR60Thlp5Z0k3/5b2NgWlYuz/Y3v3KlZiZ3Ki8hNM+qkr9z+juIJzf0nHPZkzho6sFTajlVo3PJIGFML987CXvMaXf2O1bKODhNJKCAr8ACR33au+5SChTziIzZySOmuU4m4j723R8M61YBrjB1MexCr4EZVxtY7Iuu0beE8Cz9PRoOyRn+MkEaBp7kvFH1+53j2qM1J73oZwMGEg9EGoE8F1dtTgzw2L1MtNaQjNC63GWMHmojHBb1HwdCZNt7kRnHD1aLPsu/UDeylqlEerLw3rRWlk7xpxK1OcUbRJYuuVhZXmArWN/1X4nZQEA1YHvYYic55yXTz9gXNyjsz+QFk9UQfBUMzrldcZAjh1xsylJY4Fwe2bAJsVkEZ9c+GDeNXOdDGQS0jMsEQK+4/16PQyZWCeEQPDeMouBiYc3C4n+NsbAtL99ev4Y8Pi4EnmkKGEy474G0Xarp5TFEGY8GyL8HiCujzuiEEx9JsPA16/9xN/i3zDtkIRUhxKwUZ/olfwHvbNWkZaxUARNbWQUBhgFtXfhx//EinsjumRj8sGwycEBKUT3TK0BT/6w+hoYFkC8y6CocnbhMEDwSYlCxzTClCF03pwzQhq3Rv1tGYf9mqB4iBpX9AtxVbCdxgMorL9mG0fadHrGr4N7ZuV5TzkK00LihZxTfGIRHd+e5saTLyIwpz7e81EOGwVRvMNHYtP+knRRo1lwKc10tZxQNr9KmEz3v1bEm+lMsNJ3x4VRIYWMYhJpRquihM0mZoLu5rc+jtgGzNt1Wf0xAuLADAyLEtWPxejOr5DbfC1OUymMv/98hHEef2wK0g4JjCp9FKK/igCaA3JwFqGQzyjvm9uFWa35MsITjTh/Capu/G6NyICuzAkyZm1pldzKkNqLsfG3E81GklYLlpcnpAHVQi9ulAGRx81aBCSv7u0e57+bYANNrFhVUPtftb+hUcwGCLJHH7bQ6e6ggbn5fNWvRYxZeNI1fwcgfaI6NUGmn5MS3WSScmH+VkA5Nv5TBDKsAwHjoxQKKXRS3JWPCMn9eXqd3wrf/vgRI+7HfBZZ5KF+bbfA5Z56JlRZSNVs79JDdrSY67OCv2atdp6MsFP0oqbPdHNUDJH5T8+6BVV67Iz2fKUbRdinwnwPuvwAw7nKzeXL5RvFYI7gigXsx0ZJcd4AMTsE1ke3eVb0IRKSuL78Ra4MKvyCWzKZbY8JfEGM19f7PxvO4RdGItB7lfBirs3r3bvxaariJ+Sh62FXdNywunUWjk5KC3pyS6jyhT+GSNScuialkNlrQG4ctN32iEAmWS2HOr+lrB+dCuJF0gD2Hc0Jy8b0dUnbuKkCElVszoy+DyC/1ckQCEd5PTJk+7wLmkK2QOZ/P3CwcNPAKBZkFe2cWC+n9llvccghX/F6hp62eISjYE2VoFJbqH/FrC+duOFP4bbml+dCVrCYc5JzQYKGNlOZPra0t/1RKHobiZy/2EXq8lk2ajSMtTBCti80g4CLSTQnsHIkuXKPrfMAUvlcjZ4D225lPEiBGX3rqeFoRNiYOV1npEE+3EsHfw2yDZ860yAPavmTNT8bNG1NaXaUqrDpZifhhek0qr8kzwqjANOV8NwJcrGc7PapQ5NZ8qZLBd2GGX4PDlZoowugirB9a5KsLZ6sIBUUBMp3IP2ACX/c95mJGl33lDTAWSmEzUaiH7gC13uasicKY8JjBA1Hs3hGdSmOGyEUiQcfbbSKUVTcPsv+kq8+TSas+kHAhGulzwGW5kq0OzRHz8yxHLewkNm0rzAMa7ta/NwAts7IVyXtp7McCJAyHd4Hr20bQCh6DHMiT6Gv2gGcVj2ao6WlIaKRiv2eXP0pWMKE0+xLdFs41pylxvgk2z+elQsdEeeQxHV2k7nWqY8Cr9c8KzB2iuszQYOcH9ZixJAW/ip+/AY0kmIExIuwNzeH0ev/mFU9kPSCrcMIGPBe7bNQcfnhuX/BLrsQPlEPzNCKrWWfSW0ozWxA0bMuMGCP0XuOwqjUpwlvMOzwbIeLEdU12OgF1ZDcKzCBn962ts6hEZshD+F1al5JV+XmZ00ikEnYtFy+v2JbyWuFTvI4Yw2UkPk1QyaoTCKRkdz3q8ZYNjljAXMRCLd9G25OP0H2bPya8xgAUZ/tKz6enyvRfuXDhwNXO7k7SYdIf7g7pScP+6R0RwPis58Noix20/LsMz8BtBzoN2hzrU8iEeE1HNo93dINUBz1DIhESAYAvN7iPxzi1lNhnJvN//hlLZGPibRapvwDDH7EhBwSmILwdKt+4K8Wr+uh3O2h4swRRPXBZMXF2YDw6mNn5U+SXKATQFiL513UZ8YrtQh5oacN23/uwTRdOYimEoKo1Z7r7/UTjh79S8OoIJrQPdZJZ9OUyUV3WBM7FbRrfnUKn6Gw2v4tmUKUzzO8pzZSDu8fhUC1Ms8tMe4S5Lr0fZ6bNUq78nd0l5pLPXvE6xqAH6c7QisGRpklQb2f7VEhtruXCTLgsvWIrJWv/IwB1bhmxxBCzH8DYcwu5aZfXSRcFMx+k9zuk6FtwhfAwAgY39pUt3Kt2c1MICEn1Wuwkl0fvZAEOWTizuvOgUWrNOnxHQfejTUVB7qYXx0F5s1adfd75SOrK469FsmlbuHba5qVw/JOGWr4Q4Xyu9A24Orib/hBL9NtABQC1yziZQlseuins3Q/xsoQ6g1+v7ZW4160WO1xFs2UHqUp9sBBUjeOvcrMgClp6dXBlTQSZiXMp47cm4G36zqS2g6RowmorE1ZKx6958KAJB/mVdFwuEzZM6+QZYWu6JR73D//N5IQPOh8JeGT+tK4samoUHnj2jZium9mY787kcFzg5aBLNARhR4JARsdSXyDbJeY+cTKwuPznmanOw6XX0Pmg+0HGdyoCm0+qnbmidoPXCDPeaT3FNutgCU6+/1J2EoFrf8Wr7+jYCkDnbc9s0JK6//3rnA6yLhSGuo1/eTxngEBjLUDvdUlE2Sy8GBEkEj+b1x5Hb825bHBnoREmfO71WIxPLZu64mZbt/US21e+humhQIJ3wnPZTyLhERJgRd4lbC+6ZqImpJbcNhOK+bBO1yPJjuReGgM7Lmgs8sGWQlhFWS1u5/O3SVGEphLnxZyuy1bmjC+Y2+vIlwwcUJpNPNtnKdxv/BqF6P0j5ZHkrrIAQD4QVHCO67V+Q/xMwfcnjd1SNxc+/i5SsYdEzk3JEEHgMtorPBf7CHph7hwE0vOrzgeH5mO0UWuruhVPjDoJznv9RhuDMsQfWbcoZ13fmN3//ZB6yk4dmQLjRFPewudeXg511CuArAaqo2NnsZgRsPCHXLS0iNPpKMigEyy2tmtGWsKlwy5K5gnRnyRlet0BM/eLP8dkYxwH8AHZWaouHBISWZPbxU8iGz5zYJyx0GPxTz6L0TxCXDPn4aA+CFSla/DsbQRrt2nP5C2nNVAJGlNGuE2zf6N7mosDE/yWDqJACQjnpa7Z6mPV9LCmCLSvQbONoo2YSm7PekYM0BtiRJj1lugSQPtXIPD25aLy6zuy+JXt2uPNYj9e4vwxGT4TnzbYEqe6DMQN8uo/ULhiRPs2ihKdp4aMqfb8zl+pLWpWzXBJQBeyI5fZH8QoxMPCyu17sj0EmL0vMAvKUu5zCBmRh2RO1qdACxCQj7hBXZxcK2KuRC8o2Dxn3lysI/tl0FEoKLFKUkJtSID0espix9j0xC1wWNNCRn0G+DnGlYrczl/hFDNst1q8MJlhkPMY3oXhu+FLy1AtsaGzRmV9T+VbVJHAzV+S65ncFNCy6axWLXE1nd+ZhVyDzoJIApxUJcF10QFxK9Jw+q/R0UywDCwrdao3PgXeq/y1rbEmEDY4eBYw71RTVCqN0PRrNo0ji8Gx2nYJQJjbMZvoq22Li3hZIJFykSoAfHA78ALpwxLACGfR8FFl3PmirgSNNOSRce5GnuaGzLwP5eJq3/1hb5/xEB6NTlEdMafDQlSCM0dWzfD5aAyYWC0uwgDoMh9TzKgz21FwEgFFsxCwYFVtl6i8c3DIurKailkY9nwnBYUoWlByFwUYVHPWYa0XemFjpGvMncEAg60rxwQ69GVkVoQrF+bGlfPnGuxUB76oQtadZW1hlf441q+SkwYL51w39HjUPUCI5OuOFdaJAGYerZ6qnvO4Cuhv+KdqT60QM22D5MsBd4yvK9zFkvv7jFyizGSH5z8CUSuImCD5nZxY6RIlfb44vBRwtOXsHx7bwH0gyz8F81uuI80AISnkwY0geY4dbNZ384DlsTQHTduBSCqOxn007IWMPwyNYiQ02nYZR+YwykaL6ohBcBqxowxZPpO3UBvoC+r32fBtoWxqyH4M0F1Uebt2QBjSMFzOJ0R2sUT+GyrQ3A4pJDFwsxpCViI5GD7jISVboaTCuGV/3mtPULZcDaypE8Ckatw2k//aY9yOLV3hCQprhz98knK87lTAV7RZD1IBRkkN/mHUhq2eWBwnxOostAnK+86b3TNnBdkI/g5Y9m0fAMoUuGGacfuq+lQkmFm/BKxQ/r8u7uOPZ+ZzSPXpTR3LynnDjjIrP/SgEqa35agp2TGLNF6GrLZBM1zz9eEqnaIvLHZklbtenSmsmeZev4cSkOOzrJ1NTPXA5yEBoKSK3l8gJDdaAbkSoR+w6gBep1uIEQecN4CQ11R5Qg/Sk8NXpiTOmXGgQFLKt+jhD70rgnjSF2TFIsue1Ad1yLShHP4rK+svjpgpmPu3SqUVCkNcKOBgqO2dH5I3UM3DEvfdxVTJODbAvks1HwlGK4zCEBtF+glvIe3Wl15EfgycOg93ovHImAnhlcfVpXXr2b3z3Rwu+1lXPzGR+GFdmlWCj4ZB0AJ3fEaA8ToBhnsLgMTrKyTcIrms2+uQ17w5+dfxBYGf32e+1wQXXX88f41Jrq99Zp188SfJYVIdJX2A6I3x38Cl3QERHvRWzk5/KB1SSSnLOTm7pjfM+KkskIJEosYfTwEuKRrYz/sxlTFxgfV9Agn225h2aQ2CWSiF+LTNkZjmlhWc+jBpWZNQM2tNwEzMUgcgztXIT+ilTx9mjRIfm3W86TDVR0yIVCCmqh3M4Gq3fGh0EMNH46+vter+9XN1EuhLZQXTl5N9zdK3S8AANTt3wT2emWlLVRAHRpG3yADs6m6+UmBvBFWYwhKw/deSWwG839I/sMOQ+tPC97R/Dsf/MqlzDjd/uCKjgAmaKotH2vZ4it6Wj0drRMR7VLa2DLgW1a8nVyg+ENxmBwNdwkA9dQ6oHgqp0ldkreeAbdWDZigXb67J3QYkGX6zYMKa8oXV7kIWG57W/EeZ3tb3n+PCZR8WhJxi/efFISs9HJqOwxjbe8iIxOzeO5XHrYmfRFyiV17lVIIPhyg5fMhR1PttlmKv5ZYes3idEcRc1GmNIEvRPqsW7OclG0ATYLufwBH+aDCxiHm8Ds8cC9QpI3mx/c/gQNa8sTOgoy3eb+H6qqNxdTpOJ9+jDCY533aim18SUeI/uhEfps1yiQ8AWs/ArBjaH8P3bOKGW/Vv7HEy1glKEXBcckzpt7pCFxBZOpfGfuMV9+61Jz8nHKoW9kP7fP77tKjTu1oSwogsuFpTRnsXw+4x4mpPAFXj4MrSY0cXod8Y53O3E=vNS52CyDtzyEC9Pa1g1vcpBBb/zRywPH7odlXIGEjR8PWmjrRhYQ8YjozBp/YNUZNhhWycdmU0awtCd/OkPBmPgRZ7RRlibUNALCFXNXMU1YlUdAjdnxkGp0q9E7/1NQvmKoRF7YFm6dmIQcwUV/n16XrM8kZH97nAo9IrWSQfOLZZSbj37Ezd6Lln4o5w3ML1z/JtLtYb60p1scs/ezLIk0BYXCltS83Zp5u3cKtFveK0Y5rL96GMnR3by0a/pjw8hebBTn51eQloL7b6wUMk8uAzC1Q5ceuiHai2WvBn46CY2l+jlgHpiPLxQTr/qTsThY2jx2jvGUnWYMRCQErkcJ68F1tW6r8upPapW8aPs5IjN6uPFVJTlaio1aQ7AxtDxFgacjMwflNe6LEiKMsrRt84BmnDR5937c0MZNooMkYk30qY1sNpKZUEgSCX2f/ftTS+9vHq1nFg/fUTejqBlEiNTAd3IIJqLmIoWbpCp+x24ysYIACFC75StxBklF9hEtkb8kximawXYm2g7rHlXrIkXjrki/Cdn5Dx9nbcD7FiM43u3gYLEp3g0EKHy3bAWokVsRXCkIafpA6yTEybCTbI/dBoHbyYp/+90PQwrPpmTtetU2pRqOQw42qcYPCoJmErHQ5k1EPUY/o44xHQA9qjFiDY7fRKmhdOh6h0e/kjBwNKdpiq0tuI4gpr2ZeQdFxM9XHXBOqprbdwDp1dj8bdA/Ko7EhkcFzeK7zBfeJbDP6XWRAn6zWDTcbLEO2ATxJamr9koS7HR9PB9CiE1xlmVW1wPBOmdjRMCH+2eWteFxr3Ccf2Cd/eXfmzU8+FDIGWERQ/gNxBs7ADw0QMsUKy7opezf2MS+M5Vp3j15CcgodFdHK+Bl4czsCGNARXdPCH+0/wTOh0Me4W7NGax6U1gLlHyNTvEyzv1ZfZwO/oxmOMiR6kuFSLcqQ0qmFg/4I1S0nbPbKVOzEHd66Ccj66Ge9+bm1AYCeveVvGsgXnNYaTBcC6t2N2jJstH8Zw1UzfEUddLRKUbW3/vo3xQ9hF+L+ayVAccsooThufdhVyrM+tz0akIpWf95DTlGaDY7bJUCghuYxOPQdEUh5qFcECclxLivWrO1iJGnNXZ2DrpaD5xXEag4AAhR3qgQcqnssaqEsz6NxJ+65i/l6sjtaS/EVRTSdWbHKnDqa9UYc7n2lDam2bSp+v6xgxAhm6M/G+p6nde/f1t4P0laG76Z7REj5cThelOIOpG5Xs+k4lkuxXsIOHcPOob3E6v7x7Jv565x624K3yX9Q2nuUdmRaaoELKos/babxaxpOUpDsw5QbD6/00ZfkG5c5inyLX8S43ki3SMdprg6L/IqhJySDo2/7TC6bOpRB0uwnlns5MsF8Ci/NvoH73cqpkfYRchGfMGwJ+bWmfonlWp2a1zO+GzpzKaz8gZ5cDBM1gGfU1mQEzGRWIUn5X6p/RnCCekpwsIWoToUfzpgkQpiW2ljB3/m9XTvWaQZynf1Y/Rf9hObSya1K0LnsLeUC9of+cQDPpHOuYCE257Rac6s3Ya0yEjmp+kpAM9oG6F56waAgoHjJhlG9NYcJwM0DM0Thf8oQL2lT7g/ab8uz7SKJs8rPfbqle5QApT0jBEFoxuXd9DlZ9R8Dyp2YgzMc0GIHKrkqd4eH5Z8J4M9yy7f40GN8vx6DeLInqROBxqZpP+xwoD9ICjn3dkW+zj9iPJ69Ae03sz5hiGsQmY4X6i6fUNsXPFv3I/V/IrnWwxXIiVc46o0nSgzz7LjY+obXp1RPy0slgNyrBDHPUjoGMXNnmhG0ai/brCTLV3EcKoMFnrKZtOA7C86uKchoa5xoaRah9U5KWNKU04dY1jnVl1e1likzHeSQObF58mssYVGt9fFA934ojkML0nZdkpkfnN2CK/8dqfqmzCWKw/wX45A7dQtUYrqU3KR1fBs72Fbqw4BDgKBdtUrLp5gaoMrvwVqGHMovxRGEZq/ty/xtYQoqpJHk2x5Bh/LUWmvaZ32/HXBfCQL2aNbdTvtk1t0+duZaSFIu5zcnVzv877ScUElCfDQCm+5WTV4wzQYfeaoPbqEYG9DjJ0BhCZFD5nlCnMzesXfff3L6qgEQFvRs8a0BZQmjHuzlg90v+sywM/IDlWqw1YM/UTNq/tDRnkEjXyv5v39chtFTnnQFMi3CJavPmMMqAdtDrxZ6+BM35GcY01ulhdiAFOFKtg9VKDpcnwsdTxk15rZOJS1gSTOCCPkbkHoA9oPjVSf/JKQOuscveeGZc+x2wIZXtdppP+Y0aWYKAIhCSgewmL2P82FypPZrEsvxxSdnPPRS6eKATF+oIfPedexGiGEupBI3tm24le1U6uB95Nryz/N8LFD0dcQts8/1NuRBRLT+QfiWRlLqUCoJnkkR71lWMuDRdVf5TzhiZ6hF+XUIgbLc+hO0GBe/erZ8aM/8mk7LFrU8Z3F6ryQL8TQvilJ9EeMfv0kt3FHvpgY92r690MXAvTfk13R1ZGTMgQ5yW6s0VsK4alwLKtY5TjF95GknbeG6/PaBX2xCp6uTSZVi9ObtpCENsY0SlCmD25yX1iRDLjXz8rzoYO7h6bBHDxH4hu6uFpaQMoiI05TxV1w+qMVDRJLhzKJqg3awUQIN8TDrYFYA07TVaqiF8gLPg2+YOpkUtrDQttZRfIOyghb+F7iy5Q8Uz3voHdvzMyspo4+sq46Xx4Sz/VWsopDZjOvrm6xAU4NHcUVrwrzx4LYpDaoZGnq+WzXNnxQYQvbB8F51LoI0jlzzR9yiGtAdwl35R960bwxviaeULIsm9PVjrSbDTcxnmCZJf/eM6pjFLh2bxQdgMmZ2eVX42nPhPN3MKj0zGD7FK7zF/Zr9e8uH/CaUGT/8mG/7QTYRHZ6RToqN2Q9p9veXHJjxoWHYsHNDxQDt8C0Q8nJtQSM+y1+gtc2n3eKIMJ+XFLfTZ1sbInXXxwjzPpcY4SxXV9c3LPKuieZc5DCgwzv9QPpiG2DGc0SF4/4DNppjC8D1CBivg/maNqz6yocSuFhT4VA+3HybvJdUfXKl97uVEznI6AQXg4Jm4BcORc8iu5izzgRbCHCD5g6tPVR6imPqW73fhuWzNXo5M+EpppoZjeUnQyis/XNyI5ZM/V0R78pRdTnP4T4rn/NccNnk//EstPjzTRzzNK1kRkjh0Ag+njVIaTVJ5/JOH+MtFQnWfQDOWljr4dmLjLQrMUpsdUNQFBuTGZ4PAfh30w888hGPtPAnkPDyCwyZiuNx4WL7v6+WP5klmU87SAnsmbAYcXcoPSSb5TY+ZcwJUAjq1wyPKJleNfTVg2LNlPbeNk+cPbicIwzDWWlw9cwbybS18CXsMUtllsSMHHMcMzqPjqx9VPgM1GHpnFzGzA4IIBMO2NqVC63ZbZnvHtlfiXW8CbtUXn+ktC74OIacdz2KfnIr4ivcUiCb+ZCCduOmbznsZw5CoRW3RZHWMBu0AJkLfdWALTVejLCiGVeO11AT1oIbBue0K2EqdUo6PSJPp1eZjMVhUFFNAEcrWr01QTnJAAP+FpHg8IYYxvyxvuIDIOwCXHY2mSXRhJ3/dELaGFUKjSch7bC08sdngEfxNYtMb77XdVzceUe5zVqxJKELvsrZ+d/Ha8ix3I7WqxzsK36ybJcE7YSnSR0uBdwZuZlLHepiIGWaEnbHrNbUIB6bbsjA8nI0aMZpooHYqpNukar951qMRZ2/23ybSPOBntoZ7ktzVlOD8L/266FnYuYtzhbWTTFnXdERZ0c2lvKYHgy6/77xDNuHTjHg2LMKsqSnH+U27WHEUgToF6ZgPFnGx5HqLSazbnJsRoxFYnp5TcwXLwqyep03MOBLyPaojm0s7uKAhGEkKR8EDQsURycAbEqcetT4SbNQbzrQKZ2BUOmCYEMdjCimIW037+dJSYwVL+k8tSXk3t7NV4Ty4CLg8514VVrdPaIE7wY+a10oswYNTET/GLkhvYtUkji35HmRL6LX2aTOIx/u7KSHueef8lTq3hLzZx30m/eLw0HeexA0xDrMKkW87H2Z/KgpMuZU8ICE3Q8YbHwXXOMwLSU4vt3cNHr2wtfTgtOdz0uQTtxW1xh8n4algK51DzqtFJAzYMXQbvzdj9zDoMOXw0xXxWOygyZUTvzXokDyD1K+G3v7mFxrEcrFi5mRPzXzy/+ztqdKYvGKkkVwZfYeWYiE7OyKsk7b0FHyi1FtH8RLuH1dmVYD8AjkxMHbO75EqLmFKUhafrZnfBVho3IKIypAe3Bwt/RmDMqTYBeEnYzeZlykVGkwzZv++NqG2zxpwyI5rxw2rSr3CX1AQQRtEn804K/IZ05s13n7I6EdUjw9iixzYnx3gQ8zc/dFDMpN/jBCmh4rgxQgS9MU9Pibq5Ff5pfejwuvRd14HmFWRPO8cOIKJ2dcVoULSzGjhCwpC6CDqMVw0/lp3XUUvM94PPE7VPBc5gPF0BCsk1q+ch0qXvJYCxCe2njvSZLSmuwqH0DUd+pG3CDkhBFu2YWpUjp915HbbE/h92OSCweSbT/n9Xot+nSWpZryRjjlwmH4RRpbQEitesPtrB7aQG5bGDA5ZPTyYrdbJwnc2I+CDdZlUQxXZLFw07ORnQHPc+8vDnGHm1XCLE1+lJS4rgQSDwziolzsn/314cK71UEm7dj3Z90XkQ2VtEtd8kmz7yC7dsSe5kT8a/lbkolaifXvU4JBuV4EwjT36KYaMIjoeW4lhJjK8PLpRoZsDPuSNu82ms6gFzmSqb+IRyYU5WewNCFLyAjwL57brkLbqwyw5FaSoapGiqVVmT+OSvZ4JrS/zJ2uEXA8/oi2f8ddH9dTBnv0PeXyYV2jdJLS++mgj3VJ2aCwAkkRVH4SxhwjwpAKz9rPYRI0VPSqE6ssNrdwTsqx3cnUL69qzwtUsRVHR60xnPUr0AZK/LCUe+n9FWLuOVsUDye8HinkecG3V5HQdS8FD3Uy29BhSNFQgK3XjylSw3XaoSRFVR53VLbjRuo2v1DFeEgbQKUX9iZr/L4HeoG6N2FZ7xQvzfI8tOR9avYMBV07aznrXDvtLda+2JeIZEj3P9j3E/5K32R6RISQdM/2KYJI6TSK+XeN0KnPxpjo6iNet8djFMLgDuhoxNXtHScWXiFTHqQXIFhsPLxLkQtk9YPTc1fQeNp74c1hVEJvU31fZ2hpT+p98tyw4WG7IIApnq644qbsJ/DL42cKy90LrtdtmnvFDj2ox1OFP0bCF4jx+TdSaD4oMrvoVDSTkUB9ByaRE7ZUP9CVxVtCDEuw9KiCQiOYb72AtF0gC62KXdxc907aw5VCPMoAJQio8bV4sQuotnKWDOa5IE4FnTwYtiGHcDrQqE776cur/IoNnm6AXlhQUWJUwPmYSZKXtODQ6ooXM62TBGbiHw78nXs5HVOmTamrsFqBU7jeNfkTP3SCAbiCXxHGgqWpzf/M1QxxPZ5jfjpbNiFTO2Dxx0O5amTSw+OaYUZLjY+LZl5O+o+KGimtsWB8/oR4BFIT+MyCgjtwnLIE38OTtFtSU7uA3MxqxTomoXk2t/lQyZOVCFcBREr8QF+lI7Ltpa0dD6teW+iZ3r2AF63b1QWS+8d8SzcA4KgeVkljPzL2zAmkJFt0Y0+3QjATagmoI99hDX5ZX6FilYkrKZtIjbCBEqTWqKavEApEiEpUObygxKLh5mqmr2la/MHqE5HJQrIcusc9AYorWrbilftFIihjxNVncv3xPsRmyELSLUMlK2FDnU37ZwD0Ptyccupadu2V5rvlhqMWSQ8sW67i353HgR94hkswiUmEXyrCRZPBlaMx5+/8JIp6U7l0OzrY7PdRLWBhfe1NbWKPQ/E+hI/ima7cgF+PxdhF761p+7uGLNvk8b5ksSpRpT1Xqp0hZJ3oKbXfxOW1vd6dritjYUXl1JPtPvw3C3hCKz7K9Th27R73/IKJLMFQWMpzls3PM5GCg+KgT/VIxwlQZtHrpaw7JFCNYt6M3gkIKOU1HFGsF2UqV4eklNW0Bfah1aAXkAjBNdq/0U0kPaJj1CXr4ll677D5ocwk75eZBRSpIqCVWcG+03esb+25Q5E6EwXTct6cbl4bQgOIJG+doj3EfIjokhTopqD4K48o/7bDgCjn5QRv3qH+EfPDbef9tZWi34bvqksX/JwJRTBRNPrV4ON0aj93nQDsxkehCrKKWeHk494N5A3zqmtisjEMPJRXS4AunX8M1tb3x3pK54v69Yez8UYWJzjcStQqdVCW8Hb2tNIqvFULZZ2shrUozfR0T9RwbmxorA6nSoTg7KER7Hc8xbfWSdidyY/aMbrx3Qpphp50RQ21laaQYTh2LHWlEjahb7yJeEcjaXISdZVDKKKHbQW2mgXwHm9Q1qCRIiwUZmT2xGxOCtryG4BWPwc+vSOdqtlmlJ5aVpWoxfdk5E0DDCvy8xv+2e1DEid6ZivKKGDfMZuwWZebOLRryQciKYu8EUI3Ffytexau02/axIfikV27Muw3nmCvthL2G67chSGCog0OsPsVvm8OEL6MswKFHXVwwUk049GToAX1hhfrwYXx8h+Rh46cLyze3T66Nk5NGppVrOGmmQB8OiOYUjLnJq0+WcXvZ7ZBE6j/gf2verQrCocPML5Yc47o+nEqLBGsJ4+yOXWM6OcIyPVZ1+UWcUuwadWtLbXk8hNvMAHHaXIldtNHiJ3khvpO7VV2OVUU39KDysYAwbWsPa7ipj07FnSRlgq4gkvfJwTrONldr7siEIlWHFGTEyJ/ThvrMsV3QZyOiVewL6hoGCHe0FqEg8MceMqMVg4spmZlmkho+ZeMLbQfO5Yn/vKeAJ0KGoPGuXIcNCeVg8g/rt91QUdQqU9Yu2WBNSaapv+attPVTyoIAP3+Wq1M2KLPBYOZdu0M7P+1qIX2/nlZlZ5krk0b5vrq4Icg1Fzyjcsd+AkndHrpuKBNtLO/d21OZyYCa5C2ZY+Bs+Br9RHasVejhwaDQuLAW/hJeiUdPmX5zWDH+DjaYbVNlVcWBeox96U3/J6OrbsBahbz/Tj4l0+Q1OH3+KMo5EtzdixQnPOqiD6fdUxSGF4PP4s25Urty1QRRURrDwJgE3qVQussbh6rW8hkDQCQkcTso7mZA7SbL6JJTON8N2j57tGAROIF0KeIHCJIAii88H0hbBOpZx90A1/7Ds7t6xdXY8Qp/GTJiyTxJusP23iRMbcbfZoyq4uexT/x7T9RdIW+1OT8nz6hSqjpvebq49ZOYgIgyo6bXhgCxtbZ3NCBv/gC/Ya4Og1bsfTZVLM8Ot45PHdqE6+crquGBIHJiTacE3v60B3OzG8izOG3WjiaZdpu7K2b6cpM9quJ7Xyx4z58HXKvCoFd73iNtekoz9mBXRmrJYLAFhv4R5sexYXfv6tI2pkXhkphtU5i/vCqUjl5W0SzUp6k3ibJ+tlO3y/PpnQp0Srfjlzu/r/Ni4PrQuQnDZzAoe01p0i4rK1+BfOd13jnACoEj/w0SOiZZgLJvqT6+nvsnMYruvDzIs4piiT8H+mt1YasDkPK2GTVY6sGrPY5mxJT1CQZZVI0/LYEBiWUZST7BET4NFrnz7+RDz1RBx5vlEK0O2sJ/JSsFjh96hjY2s/8tqV6B0Suocwq1jXUFDAccM2I2g6IWTRfRWWzYtR3sy4BvzyBZ74FOuTV228jlqKAF2DdSMkmyVpoULcydobrnMYVt8L8z+Kjy7zIHdshfqV9Df4vjUSQ73pPSkdzhybxOVB3bMt7KJkIhPeRNYHLuXmf6ojmijm5MDiL5rd59k+ItCykQxiIHbxlkqhZ0obGAaMuWinFnt8YOIzBdHaRYW9tjvuhlcXEXIJow3B60TnTJmj4zR8dOhP4/OQ+Xchm845zzObYosgxrljh4Izkw1dxCcgl3JIXeor9xS5Fan1Z4ZwmPVYCMh6zNB/2gyItRwXgqb3LbzzlyuYw7eEHgXHrVWAoPZ7QIrYoDhBQYQVM+jGGto7jkOrXzb+H/d8QIx86Bq0ssg83pDcj/jnIxrtc7XALTyu8L1oYQy2hQD3rL+Xzflb7MiJD0lSYnsZ5OLa2FNsCI4+X7ReorcBrZYDygKKT/+t1yNhWB3RXB0pknF7QASPsHQR7PUxH18n10GbDC5lufPAFfw0YlVvg0NP0ULk/AYGhVfqV2Js2P8cmv8BAiphWdD8DXAdL3VN5xoKkYh9SmbW0/8WBDD7xVWGl1lPrlzyFj4igDh7YkEJsmM2+vtW+gyE55LMu6naF17CTO6GymwDj9VvUjwVn1tjoLoxBE+hbCoxPqvIPYfgutc6zz8SMKADavxkdQu1WYq5jk5o6TF99zUWJS+4CsdiU3/V8yqmPbShELAc8aX5J++asLFMshMWofPvh7ahiJ6GzzekkeK2V7IcHxlmyBp8yk9G8jpFaRX4BdCNkxhJxE3iQe3Gl8DH7W+qs/psqwDdk8F5c18wrE4bhR5BZYbemG2zm11qufXye7GKXW6CPpDl6TOdvzHOB8npLvR7F4sIzBhuwmzand5FAeA7hOFP18QMWa38Kii+wchQtLw+1c7uE9ceWa17nHr9XaJAjdeo71nuEdibuq3EyUnCcSh+WyXjFesUDPMLgINlxBSHefGhN+zZ34AzrNAHSB6464sdUmScf6XeYpSnzEq2L5lVPKOUxIKG63RDsN/KMFh/CW86HRxtQpvCkLj2hdef9Uf72+MSbPpjK+3AApkmEoxUIrmO3l3W80E5N7/B39LwcNGrEz8jkXyCeNI/IcWZ7CZTaMcpdQ1LQEKrQLDkEWu0iWug1rHbk/PQ/G+iHJlcPrKb+K3Yj+3GO2GSxWcg+mUOBU73jim49C9VlUVwXseskOLo3aeZc0v1eU9Pl4GbEiT9/eEZ+IOOyiIeu4nYIK/dOM265rNntASjaV5wGyviQaLXKQaMzfAUJlBqe2SzIARAyzrn0HFbGcQDuKhP9zk8M9AXFu2+/SeriIvLog6Otfn1EKNhw0BVbrslFXvrLL9aalVUsNRGaSn8vdqeRO1hwzZcfwQ662fy771myAitMvhzjqsBontbSwmYrTwpr+YuU2MFh0x6Ti3b1pXDOZGCX/Gz4tUvEcFeoZa7E2YwROHlAEG/nhnri9iTuKrW19WAeDNCvu9PZvi8L3hQ01H7rn4H501L1BIwc+Zw/8gcxlfnml1MEXz3+OxYxQLKh7Tl/Ao+Y4+CB0tVzwAU/BkiFRp8OaF6KjLDD+76YBJAzUJyXKLz34lQa+X/QA3gDHyAaNhxGkWFHB6amzhQOYFcR5LDWtwpmi9xcmBVI83NLYjWeDsm2HLh1+YPpJyshRwCb0jXWW/hEk3IusZgQHaV1CTMY2bziKcY6nDbFzSmgWic2c0XI8GqFqqeRh6ehgFOaT0TdJrnBsri6VfVLekegToT0lAdq5S2Sso7SKDPSRGqOrl8N01reoXDrAhLQw4B/tfAH7yAURmrGEPGkxX9Sm1G+l4x64HKjYO2+ZeWKM6pmaNOWDFvyYSVVhbHCvyTe1BOZtnH3AIkAyZvUq9RbUJnrzMvUNTHPbRmBPUKh/cbGpvha542zw3HpEu+nzhEOoIgDAYvPaEi8AZB7lMRlDP4tyhJcMEPTOxY1CTbM2e91P9wcdbK5LLSxwq+JUQZCfq9drQwOV9UOBgF7idPeva3mmbVbQzsTfP4VGzRqHjq6dVjANx/XX/4uAYaMOSnwmJDgbMxD4Y2k0QA48kuMjUF/2LkJVa4WDw9D3ewImcKtv+BMTwAizWC/oOfprtasXAcNWty4fEI/SlM5XbY/ERNKMMdYltlSIM/Vf81Zewvtsm9oabVVxQgtKGN5lQ9P2KJgc0MoKFeWV6lrRHiOFh2eowzwFZYfhXeBK8aoaxiNptGpMohYWoQwzBjxiGzHwWlvQc7fanB1zyvnPqrdnji2GHBLN2AYT+I3WUp0hvJUXWAK8dxwMWTiWMdZqnsUShtHabqeAX15XYZKXfc0do/SSeNMEtTe21Bf81diVc1DtJ7F/6gvRoP8y6ReZy0kP4giaPbW1jtCGpRZ4qoY+0kc6VYDMVGtzjU5uHuSXeEy+bBpCw5lOlg/e734tM1wJDTrREX02lj53ubZurKDho1FyJtZqd8zlrsehtQJIXWr5cuckhkNZWFZ+w770jhDJulCL8McoL2MomY92VoE09rhK4J0lFfa4FEBSFsbZ5+CzB/YNqo52XWQJRCZF1umnhhpTe1t7rkFdKTfsLHSZmwuhXCHw9bkiUKJ7m6GqWPB7eqhp5L5JVWxokfut/ATb0TAtCoOMfnL9ZTCAeHpUVbwF3oHBAL9kEFuqj5bRXHCC7S63TrvJV1HE+nkYlbrwxL6k3dknOYPTco6V1C5e8AOvjBtyNHht4J0xdUH12Ksb7lA7vfUX6jhe5RTq34/7TNQP/VAKTEMbPmuBK/j1/myhwNaFrc/HamR6TxypLL+YBBs7baHVBN7ykCX7YaZoi2rXi/+Eha2oPxw4ugMtk1gvx5aQwjpSDzZAUBe/pL4Z7nvkT+AKb6Eti4Ak/VH+62V5jkHRfMt5O/I79gwahcboNz1QS9FP4JNfud1OdlwBlim30zoysv5DCStWLSpbb7xSMZL6Y4njEEINfime+u7YEGNimRLFu4Gk6rBhxh5tA9dg4WjDIskj0RN7wtJH4/8bHgy42fenlbm+dIIdzJ2PLMceMp1cjRrGfQeLUSQr+tqOip8To12qxcjutguY1Hv/o9EN4VUJH1oL5bThlFoAC/byysut8h8LxPb1RROgQCwB7xW4h5meZgGmSKrrJtBTtS2/D5l8T0jllzcfMgF19P7TUsw+ph5q6EPvkpefL+l5ZKf9FPKfjWeuK5ANnJeYcAwjrnQFJBSDMraewylBE=hmKE8XE5FpEz1U52ppdCpgYXEl6ksnRXppZgh7Hp9l5SWNepCMUt/tri3yhjEQamF1v6eTcQrkm9834M4/pk74Tl6YKk5K2K2fV96YHOHXz+hYVwvypmECBHP9aEVbl2puDXrepSTahs2CqPzuoppW9YqWA2oDfkmW8MJxE+vFdBFCewxeI0xHtnmpl6d4KzROdQzDGSmIc0f8c3WoFzyHLRtJjSjic0KW9AvHldGSwYHME86hpQYtncbvORqmX6wAynMkbeF8CVD/NaDkeZIBp8VWUivIzkjDiyBypR6Lqq0x2EH6mk/QyE/XX8aWKomUpcabqwu6YFnvGEjokpVSCUbWcyq7MJmmlYtaOh6OsCMGrFyL5dZMsfhx7AG1W9xE73Tqn05wNbWP9Yklf8tprVDP7WuIF2KwfqwrzF49AkNV1KWCpMcz0ID2knu7gLOL0l9EXhkwCrfTn/lLKn3FqPVNIdYq5iijEPztlmKcXfnH0AqZtHSWUJvktXpBsJcbXPv7fgmcDz563Yl/APuKFJ6zdgtgPdtVaa9xJQHOmKNbZGujfva/NA2ew4BL8+wGv5NVTn5ohZ8G1ZQ/VbesY6oeuDIsOxULidZgLbmCNoHc7qYWwPoOt8/fTJjgwMZ1zlYPHcoUoU1L8uAM/pqNpg45BnmXQ48DpXEAIAIK0FFOJmu9SD6k4LUqQ3Ubbf1xPPFrnYTch8ITwmzwMzpWi+/LCB50zhos4/bOZVHxHZ53NOWvJiQWuXvh5JbMBwh1G1kXbUpMeKkUNxNF2kM3A5NXkLpOXz4B+6jNywTTvCWvIsfuSnT5Gki2s4g6jBKK8RI0DSWmq1CD1nuSwwg3bdFUGxH493OQjyH0SIGsBrymEGYcPh3FKubhwfw18IHy5GFH59qP7oxMLy608TRnazOKE9h+vMUOaXsiVLrzPEg05s8p7q1svnUsHbNOIHddDizU2CCk4djRQM4gH0qqR0tAqCOHUGrUeXllQS74U0LvEAdKqs2/Pv+xwZMsvOZ5xmLb1/adFdQYzqHQZdeblmr5zWhqcepXAd+0boGsX+Yjl3nCq/fxr22OdnLel3va83wIsV291WDhEy20mBVOdKWu/gquFhsPHT89hgDQTb0u0G+ncg1qNBGmdcMhzKDuqDI5B0lzt5Tpgq5Z/qHArzE4/Pei7bHsy0bkXfwR/RpEX8ocVlerhAa+84Eq1vDbQqMCiELE1eiroNnZ7O3p1612hh1U8neaso/CAIsThnJl09pqrD6DXW+vIoS8tjTwK0brMfXcGlsGzUm2ZeZpp4hiv2Ju0XKMBn5ZU8WP6GzvjuKicutL6dvpIfqYQjRyLHm1VjN4oqgSv39eb5Pd6GTZ/CuwC3zLGeo+HWfrl/sBm/MNvzY0w2Asj0cvlXL7UVOlYNtzAU44bv50+6p1+58bNni8pr1VnAHzbx6fDtKmymNuDyHn17kd/ISd45usCQYDt7aaqJkL+zcLMnpw6OZ4z0Ip+sl2dkYvytEThI0qopgJMfk+6KTJUuK7LKvU5Mg33hDa9JvjDAF6DY7d19vqS/a/dUHZPY+EkmX28FIURQUOZ0dT7mqAhwheDJKWY9WPPRIqKyxH1xL5tW26MfiJ28R2HN8b9Vd0B+tA0LQhQCZdfMYoqrl57ncLVNNF1FEVOImf5sduC/5kdCYoGE43LazPzhziJCLCywvzfrxZ10A49Pa0qVLD309+7k8uHV58B3uKDoZ8uvs+uGCZw8W9zN+TCQl0U+nGsuQH2wWq8rn/f/4WhQXrvukrRrlO7qky3D0k4SZGH98lWjXjLX4V+EbD6m7eJsUjQqcTnV64Dzb5PutbCzZhxjCEjmPX2irYEicsHeUMW4TFKHs31yolAS98rTKFE5gJReLZbn+XpFLN//f7mHsX249oG1lRyyMwAKlvgBsEP9vjnSAGYUlOUX6Hzx86t0GSWIMNVB9337ReIw3KvDMM2lt2rVIS8rwGU8XQ8hmBMW0uF8SslmaIYDeWDYeHkOuHB2iH2tRI+vneZetMvJyeqnHnQDKRlR7gkm9pSzZhszRTyrC4qZtpEdobyADmdK7nBCmxFL6DbeYKJ1y8uUQz/HDW5sBU9xyRp/eEUSI0/2RAFSBIaknXl4YoJDj0I5RUpOIde26aNBdpfowEmXD0BPbJ0WWEXJ8ADP4EPXnc6g810p10gjOcXtYIdP/zvfEO1ZGQRttKqrgG/Mz1ESmTYGU55V3ZeDKosy1qClitRqSnRgGD5CUPg4H+wLQ2GKnU7E3gmHKIQLMIeuAlIz/zrhReF0pjsNHNasn27fem8XWsNDMLQqzZZa+QGihycQwuk70Gnd7MBJk/iCb04uvinK4A0E3QKYAY4uJEDlkVyYyirYGArT30zrfozUgS8k3KqULVEMijxYfTHo0kXFnm9lF5cB4xMj/fA1nB+fbC5ODpDDN0Ck+kMSh+AhjJvUFMLbvorYC18hF6ponDzpmxtiYs3a59kzhq1rTWMdqghZoMkIWTxuthuOVc4TUOuX5tVsoK4c5Sg76L43j7VXdsbyfHwiuNMcQUEuIBGe6sSquYXRi+XKhxS1rqLAEyAaqUTTY93C7SjQgF1q52saadiPukZo+AhuwmSXgTu1AMSnJRnqZiyxscAGxhxFfUv+F9J+EGPMkKdbT7AKrXuSalVNFvLI8KW0d6ZqiWC5JsD9+0O1XTXXeQZnMBxDzpiu75i+r3wWiOxV+yESvlix/+Llo2Jm1jLdBOUQi9QjCZVQKkCmjvEk+mucgL+oRnZ/aFxodjsgameXL575ysyioYNALrVJP4RmRyxOffWRBrwvQ2kMSVI9C7eLVafuLSIN0t6UV7bxQtiB7NjQzOHK9U/x5X7Pep8sw+lVAixx+ZPZd8SSxqQDb2W8pFj9fFQaF0YmfiZ9oIK1EIxKv7BxWk6ee9WW3XVmVwH3PwsDyVx+u5R1vJ0a0jDTW+mLbCefmeJCFs7cb9LL/78UK+88IqzFhJP8ZkhKuvFty5LEj9XEjpEeP+DcXfxDK7BAPP93e1g23MMLNG0i3DU9NgqiV8GDhmvPE45K8rtuNDUc77r2AYdgK/8oWnfEU3xUSK/IEeaQ5pT91KPAFdsEeyvqVpWqGJmiCKMJ9rmcI8eQULU9uCPaCvC/NNI542paaqQgIUPehADpBxERpuHjQAhz1fcfwyPtEwn8Rp6JLMCCi2BEeROMUvSWEpUNns20UISFxXzRFtU5k83300jOZMReQnt+y9uGyN9jLym1LRr+1XXGH6ylLxFh736HdY3LcaxNBanIyH7o5FZagJnW1lnYhYSD18Q3j9b7oqpzacb+7SDlYfLsR5BeYwkEYx9goeqbxvSOYOMT2/7QM7yp/q9zMFLuNzWNuvpi5HmruR6ZBnLqIAe4We8tvOZvdsHjDU+W6L7ZUUhNM/lx1Tt1SXikmu/r99w+pep6Rq3dytlFYyP8Ob+8piMmU8awJZQoYjgjLhONy+U=:Kehk2mbFCyCe25RTQkNzeA==<";
// $KEY=getHashedString($SALT_KEY.getHashedString("hee"));
// $DECR=getDecryptedContent($KEY,$LONG);
// echo "<hr>";
// echo strlen($DECR);
// echo "<hr>";
// echo "$DECR";



// ------------ Hashing methods ------------
function getHashedString($str){
	// if (CRYPT_SHA256 == 1){
	// TRACE
	// error_log("WARN : Required hash algorithm not found, using a default one. That sucks.", 3, LOG_FILE_NAME);
	// SHA-256 (sucks less) :
	// return crypt(CRYPT_SHA_256);
	// }
	// // MD5 (sucks) :
	// return md5($str);
	$result=hash("sha256",$str);
	
	// //DBG
	// error_log("result");
	
	return $result;
}

// ------------ Miscellaneous methods ------------
function getDelimitedString($content,$startStr,$endStr){
	$i1=strpos($content,$startStr)+strlen($startStr);
	$i2=strpos($content,$endStr);
	if($i1===false||$i2===false)
		return NULL;
	$result=substr($content,$i1,($i2-$i1));
	return $result;
}

// function str2hex($string){
// $hex='';
// for ($i=0; $i < strlen($string); $i++)
// $hex .= dechex(ord($string[$i]));
// return $hex;
// }
// function hex2str($hex){
// $string='';
// for ($i=0; $i < strlen($hex)-1; $i+=2)
// $string .= chr(hexdec($hex[$i].$hex[$i+1]));
// return $string;
// }

?>

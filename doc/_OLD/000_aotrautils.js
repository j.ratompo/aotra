
/* ## Utility global methods
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its licence.
 * 
 * # Library name : «utilsaotracode» 
 * # Library license : GPL (v3.0)
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email :j.ratompo+aotracode@gmail.com or info@alqemia.com
 * # Organization name : Projet Alambic
 * # Organization email : projetalambic+aotracode@gmail.com or info@alqemia.com
 * # Organization website :  alqemia.com or alqemia.com
 * 
 */

// - console logging :
function log(errorMessage,trace) {
	if(typeof console !=="undefined"){
		if(console.log && !trace) {
			console.log(errorMessage);
			return;
		}
	}

//	alert(errorMessage+(trace?("\nTRACE : "+trace):""));
	
	setTimeout(function() {
		throw new Error(errorMessage);
//		throw new Exception(errorMessage);
	}, 0);
}


// HTML and DOM management :


// 	- nice HTML components :

//		* An Ajax wait indicator :
function startWaitIndicator(){
	
// DOES NOT WORK (YET)
/*
	var imageStringRepresentation="data:image/gif;base64,R0lGODlhQABAAOf/AAAAAAEBAQICAgM"+
	"DAwQEBAUFBQYGBgcHBwgICAkJCQoKCgsLCwwMDA0NDQ4ODg8PDxAQEBERERISEhMTExQUFBUVFRYWFhcXFxgY" +
	"GBkZGRoaGhsbGxwcHB0dHR4eHh8fHyAgICEhISIiIiMjIyQkJCUlJSYmJicnJygoKCkpKSoqKisrKywsLC0tL" +
	"S4uLi8vLzAwMDExMTIyMjMzMzQ0NDU1NTY2Njc3Nzg4ODk5OTo6Ojs7Ozw8PD09PT4+Pj8/P0BAQEFBQUJCQk" +
	"NDQ0REREVFRUZGRkdHR0hISElJSUpKSktLS0xMTE1NTU5OTk9PT1BQUFFRUVJSUlNTU1RUVFVVVVZWVldXV1h" +
	"YWFlZWVpaWltbW1xcXF1dXV5eXl9fX2BgYGFhYWJiYmNjY2RkZGVlZWZmZmdnZ2hoaGlpaWpqamtra2xsbG1t" +
	"bW5ubm9vb3BwcHFxcXJycnNzc3R0dHV1dXZ2dnd3d3h4eHl5eXp6ent7e3x8fH19fX5+fn9/f4CAgIGBgYKCg" +
	"oODg4SEhIWFhYaGhoeHh4iIiImJiYqKiouLi4yMjI2NjY6Ojo+Pj5CQkJGRkZKSkpOTk5SUlJWVlZaWlpeXl5" +
	"iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq6ysrK2" +
	"tra6urq+vr7CwsLGxsbKysrOzs7S0tLW1tba2tre3t7i4uLm5ubq6uru7u7y8vL29vb6+vr+/v8DAwMHBwcLC" +
	"wsPDw8TExMXFxcbGxsfHx8jIyMnJycrKysvLy8zMzM3Nzc7Ozs/Pz9DQ0NHR0dLS0tPT09TU1NXV1dbW1tfX1" +
	"9jY2NnZ2dra2tvb29zc3N3d3d7e3t/f3+Dg4OHh4eLi4uPj4+Tk5OXl5ebm5ufn5+jo6Onp6erq6uvr6+zs7O" +
	"3t7e7u7u/v7/Dw8PHx8fLy8vPz8/T09PX19fb29vf39/j4+Pn5+fr6+vv7+/z8/P39/f7+/v///yH/C05FVFN" +
	"DQVBFMi4wAwEAAAAh+QQJFAD7ACwAAAAAQABAAAAI/gD3CRxIsKBBg6aO2fvHkGE7YgcjSpxIsSCyhhgzZgxW" +
	"saPHgdD++dNIUuNIZB9TGixXsqVLcCpTknNJs+TIZzEr1txp01/OiOx4CiXpb9ZPgiOHKs1Y7+i+pVA1/oxKF" +
	"WNMAHOSVo2aEoBXOVq3Ks3n0avZN2HF8vS3raLZt23Sqt25iuJbuHLn0px4966bvHpLmovYt28cwIEz+jxYuC" +
	"/YxDsNNi4MueZigpPvVuaJObPZepstd/bsA3Hohs0GevZ6eufi1QAytY68D/bsnadqey5xu+Y93Znp9aapz/Z" +
	"wmsaPt1ydwfTx1WSUu1y9SPpy2NizT66pvbt3ANy/qYtfTdPf+PPbkaNfb3ZMTeDszz+j2Q9+fPE17dm/3z2F" +
	"c1r78ZfdTHsFKOBqCogm0IHdLUTTOaoxiN0Qzv0zmoSZCcDZhRhSthMfBXU4WVy0hShiYSS2dJmJJ/YFB2L8S" +
	"NRiYY+RZNeMd9XYkD8c8YXjXYdh5E5HP/ql1UdFvsXGSColaRYbOTkJwFFJOmWghFYu2GKWHArIJWNefikje2" +
	"ISOV6ZTWaXZUAAIfkECRQA/AAsAAAAAEAAQAAACP4A+QkcSLCgQYPA1P1byPBfs1YHI0qcSJGgt3/+GmrcOKy" +
	"ix48DuW0cSTKjMZAoDdojybJlt5Qo62VsSXNjRmAwKSarybOkv5wRZ/YcqtEfKKAEiSodCQ4pv6VQN+YEJTSq" +
	"VZhVrUL11w+l1q8L/bH7CLasP2QV+5VdSzHU2rcT374VFlGe3LU/D959e84gup6Gsu4tarCnAACKBA9maI7gs" +
	"5r+VgCYzEjx4n9Ja26bzNnR5ZGGBlpuyLm0o9F3u/ITVtNO6dKNUMsVyPP160Wy2T6l+cz260S5wZ6sicD37+" +
	"Ba7e1uadx24s/6YNVsbrvyZ2I0XVG37fny45ZZtrVzR74UHM0U4m3HHpy+/es/5MO6n98evlL6+LfbH5q/v/H" +
	"9tW3Hj3/1BaeLeAMSmN4fPOWAoIIF0pReghCKB+BG2SBIYYX6WYaBhhty2NyFC7UzoUAiLphVewOlqGJYlJzY" +
	"oosd/oMPiwTRaOE/OOaoI4cH/VhhkEISKFGR/VGEJH5KLumeR07KWFGU1KVEpXEwXfkaUFoC4FSISH45Y5hij" +
	"qljmUS6iOaRQK7ZZJJuojSfmAEBACH5BAkUAPsALAAAAABAAEAAAAj+APcJHEiwoEGDuJz5+8eQ4bVeByNKnE" +
	"ix4LV/Cxtq1LiQnayKIEMO3LaxpEmN6USqPGjvpMuTC1utVFkv48ubG/31m1mRJM6fJ+HxjAi0qMuhBY0qLen" +
	"PE9JWNpdK9YeM566oUrOKWwk1q9eGAACoxPpVadiwIb+W20Qn61m0FaVqU/AWgNS6cCVqK+rPXxm8YZcCznvQ" +
	"KLDBgY0iJkywXtEEi+0WjSz2INmTlCX/zFyZYMvNmYGO4mwQKGfNOBORHrgX52nUOC1kJojTH6fXZmcLrP0aN" +
	"s5Xuq3hXNFbMOXdrosrDXV832WNhnr7Bh15301/BaQv9XeC8q3kypX4gqLc7OY96dOpI37ecBB6t5Fxynh/N/" +
	"7NEfSXDrD/MkR+pdkthpMJ//FFGU5WFDhZZNXchIuCpkXGC3i4LXXOcRSeJtUUx7HHkATaGVedepzVV10614X" +
	"IVw/NxYJTN+EtWB1yN8X4kwe67YMOTvxU+JM/0OQokIcY8aIhUP6sNhBQbpQYoZD7rPIkfzUqSRA/GQr4kj89" +
	"nkbUSz6etMtrEr3jko0a0YNAbxNhhuY/1WwgXUVY/cfJC+h1NlEnNuXp51siFcPQn4TqWdE3hf45U6J5DsUom" +
	"0jt82hzkQo0KWKVFnQpXpketKmhnWrKaKggKUrqolYOFRAAIfkECRQA+gAsAAAAAEAAQAAACP4A9QkcSLCgwY" +
	"OwiDnjBqAhgIMQI0qcaPDXv4sYLzrc2JCix48Enf3zl7Ekx5MPQao0eK6kS40oUa5UGe+lzZg4Z070ZLMnzpw" +
	"6D/Lr6fMn0KACiRI1+hOpPqVLmR5V6Ykk1JtSp360evVl1qYgu0b9GvOj2IxWMzFJQRasRK5iGRlom3XiMbH+" +
	"/JWg21biWUN8+UKsJzZwYIhwe94zbNggYaiLGXMUU7dgV8kcL+KoLHDbVcwbM3J+qtQfYNAASvbjDNUf6tQl/" +
	"QWSKpAb1L2gixpNChU1UTq0E7tklFsp7dJzJbd+wBRYb+VX5TBN99yw2NVGoWZifPYfU6hNrPF3/66ULd/uMH" +
	"9CRXEevffsSn3QdZ8eJ9TZZOnXjwlVXX797/3ETHXk6YcNU61AJYBUAF7EBFOkERVEgQAeRyBKDWJkIVGu2Ze" +
	"hPz9AqM8wUGF3UoYaiqiPcC5xcCKK/pi3G28XonjRNpwZ01o5Dtm4n1ZX+SMLbDZ+VZA3LMbmY4AqErTkZ0Ya" +
	"BEySS/qDY5QHUQnjPzLS9taTGYXYl0SiPInNg4JRFE6GoIXVIGYrjaQfnCv1o+VYh+nEzp1YpYkUMu7l6RSNh" +
	"dE1aEHE8JkilocWlI6iZDUqEXXGNSmpRIr0o1tZl860jD37iCbToAEBACH5BAkUAPsALAAAAABAAEAAAAj+AP" +
	"cJHEiwoEGDABIqXAjgoMOHECMSZEixYkOJGDMKtMjRosaPCDuKrAjy48iTHktGRMmSokqHLWO6fDlQps2FNPf" +
	"d3JnwJc+fJf/Z+cnz479//uoQ3ZnR31Gkc5belNju6VF/caTajGjValatLSF2tepPDtiwB8mNJUvnLEqHa7sm" +
	"dXvSYLm4cumONIi3q966A6H1fZrg796Bg5ECMwx4n9O+/hgDTpa4jOTDif9drptY2+a9iRV8Fmkq8WiRx0yf5" +
	"mhvcLnVHBNvgm0xcVvaFDPr3t13JO/fwDWLDE4880h2xZPHHTlMufOnnEA/d/5C+vTknK8Xr5Zde/ANh4OYeQ" +
	"dOr/Hj8boRNEav29+uxvtSsx/M7y3B+YPtEwR3Hr9VN/rd599YWB1mkDP9DQiVSA8lqOBcKTWooFxKkQRRLBP" +
	"KdRtOEtGTIVlmKaTRh2TB0ZNRJD4Fh0opHkVTPg7il9M+2jzYzoyI+fcLjgSRE6Ny/vDD40FIaTekWD/ylseR" +
	"EimTJF7+mMMkSKXco89aj/Vjjyw4BgQAIfkECRQA+wAsAAAAAEAAQAAACP4A9wkcSLCgQYMAEipcCOCgw4cQI" +
	"xJkSLFiQ4kYMwq0yNGixo8IO4qsCPLjyJMeS0JEyZKkyoMtY1J8OVGmTYU0993cmVAlz58XTQLlKXQoUYxGh0" +
	"pMqnQl06YOnxqNKhVqwapTr6IEhLVjyJH8/P3pyrHmyGn+/okl63LjyBFp/6kdy3ah2Y5y88qlW7enzpGI9OZ" +
	"d2/fiScF6Cdd12/EF4sR82f7teO8x5L6TOVoWrLiwxc2cI3teCAV06NEUS5lGLHp0tdWnq6KDTXuwIpurr9Xe" +
	"7S+RzNXKdvP23XK1L+G8b7NcvQ85b0YoiTB3ztvRyWKm7zWnvtu6yNX2trdzp+2vUccScS37o7QP2/ja/hZxN" +
	"Mdc4Hv4xBlGSK9+4H38FMF2zUCz/QebP8olpAZ/lhXEoIHqQQeAAbQVBCF51tHWR0HBPHjheAd9aKA/lhwUiY" +
	"j/PdQPitz5E5GHLK6GTUSQwBhjfxK9dmOFGdm4Y14g/QhaICX54yOLiLwkz5Ef5rSPNELK5aR/N/ozzpQEfcM" +
	"kdViGuGVtunT5kFr37SKmRNh8OZhccZz5kS32wBjXPfBMGRAAIfkECRQA/AAsAAAAAEAAQAAACP4A+QkcSLCg" +
	"QYMAEipceLChw4cQCy6cSFFhxIsYCVbcyDGjx4McQ4b86FGkyY4kIZ5ciTIlQpYwKbqUGLPmxJk2c94kqbOnx" +
	"ZI+e/7bBjSozX///DnD2DMDmUU5kyL9lyqizRL0kPpDenTr1H8qY2b6+rUm2a8PYfo4WxYm26myGsKs5/Wt27" +
	"dT5a7E2/Yk36nfXvr9y/Vkl7p4/QkWSXgqSzuI35LTeDIyX5joGlMWKcYy3piNKQ0c3Lgwyyeev/YTeHJaace" +
	"gCbM2mfpzzMN8FfMziec17Nh83e0Wmc+3aZjX/t4j7dvmF8ImERg/fveySD7T/0X9a5JZ9u3WQ/hu+25zH3eR" +
	"3cjXLB6eIzb1wG2HTAW/unyORuqzhG5S/8kLtSE1XEj+mcSKbCaxM91Rf9kzYEgBssUgX+A8yNGCMQUj22whh" +
	"RAhWWZtaOFGxsUEjmYchiRAcyy98CFNIg3xIXWMoTjaSZHMuF9jvsDYX2N7lQbSj0CKtGJpweh14Wsx+uYQgS" +
	"xWpIA9Myb1SVoVYThRCuQYp45VE8E3xjNSGXdRmFVmx1dGCpWpppr+XFLSm3QipQtJ26RZ51T+3JmSM3saJ+d" +
	"M9AVq40wCGZrYl4gWJIuiZ13Z6EHf6ElYkpM+1OWePWZ6ESX9uPmapy65c0+DFTYaEAAh+QQJFAD7ACwAAAAA" +
	"QABAAAAI/gD3CRxIsKDBgwASAkhnTZmvgxAjSpxoUKHFhP8yasx4jaLHjwQvigSwsWRGdCBTIhwp0qRJf/9+q" +
	"UzJkqVLlzAfzZxYs+bNn/x2Quzp8+dNfzqFDiRa1ChOakr3MW3q1GXQmVN7VnWKNavNrT/90fT6FSzOeB/Jij" +
	"Qjq5rZsNEoqk0oB99bszzVaoN5961Esnj49n3bbajXf4IHmxW7kukBxZD/VZz6OHLJTomPTmZquWRCY2BDTu2" +
	"80eJWxgKnBib972KArdKWMs0MeaQf2iUZT9XGmurP1LNJ91xUNZZUonKEE61q73hPu5anvnPab3T0qYyqWq+d" +
	"NQDu0kTN8kRWq51oLO5ky/d0O3guSadMrbV3r74msL7uMcIn2udufv1GMUXBdy79B+BR29XnngkEZjQVP1sZm" +
	"JAsVTlXUy8K5lfVPRbW1KCEClXlTodlFQgiAL84pRtT3pl44layERWOSScmxB5XwHEG3okjNJiRaExpwFeNBx" +
	"pVUFZNtEYkWLscSeRhoW32pFZg5dLYlCON4JdhWF5UjY8aocZllym+NU5eT5pA4WAevRimYiCB2JlKElp2SFc" +
	"aQgaIUP9BFhWJWQ0mZlRz3eVPR38CGShYMCU65nJgUeLoXzo61cekafmWEUyHYrrTSCXdY48liQYEADs=";
*/
	var rootDiv=document.body;
//	var img=document.getElementById("imgWaitAnimation");
//	if(!img){
//		img=document.createElement("img");
//		img.id="imgWaitAnimation";
//		img.setStyle(
//			"border:solid 5px black;"+	
//			"position:fixed;width:10%;top:45%;left:45%;");
//		rootDiv.appendChild(img);
//	}else{
//		jQuery(img).show();
//	}
//	jQuery(img).parent().not(img).css("opacity","0.5");
	// Allows to not have to div with the max z-index
	var oldBackground=jQuery(rootDiv).css("background");
	var oldPointerEvents=jQuery(rootDiv).find(">*").css("pointer-events");
	jQuery(rootDiv).css("background","black")
		.find(">*").each(function(){
			var d=jQuery(this).get()[0];
			d.setStyle("opacity:0.3;pointer-events:none;");
	});

	var waitIndicator=new Object();
	waitIndicator.end=function(){
		//		jQuery(img).hide();
//		jQuery(img).parent().not(img).css("opacity","1");
		// Allows to not have to div with the max z-index
		jQuery(rootDiv).css("background",oldBackground)
			.find(">*").each(function(){
				var d=jQuery(this).get()[0];
				d.setStyle("opacity:1;pointer-events:"+oldPointerEvents+";");
		});

	};
	
	return waitIndicator;
	
}


//		* An in-content tutorial generator:
function executeTutorialMacro(macrosStrings,/*OPTIONAL*/maxZIndexHoverLay,/*OPTIONAL*/labels){
	
	var incompleteActionMessage="You have to do previous action to complete this tutorial. Please start all over again.";
	incompleteActionMessage=labels["incompleteActionMessage"]?labels["incompleteActionMessage"]:incompleteActionMessage;
	var closeLabel=labels["closeLabel"]?labels["closeLabel"]:"Close";
	
	// (Actually this method is just encapsulating the doWholeProcessMethodAfterMacrosStrings(...) method...)
//	Example :
//   ["designate:#target1",
//	  "advice:#target1;Here is a piece of advice",
//    "designate:#target2",
//	  "advice:#target2;Here is another piece of advice"]
	
	
	var designateElement=function(targetElement,/*OPTIONAL*/onEndMethod){
		
		var POINTER="&#x2196;";
		var SHADOW_THICKNESS=3;
		var DELAY=500;
		
		if(empty(targetElement) || !jQuery(targetElement).is(":visible")){
			// We break the chain :
			alert(incompleteActionMessage);
			return;
		}
		
		if(!window.helpPointer){
			window.helpPointer=document.createElement("span");
			window.helpPointer.innerHTML=POINTER;
			window.helpPointer.setStyle(
					"position:absolute;font-size:32px;font-weight:bold;"
					+"z-index:"+(maxZIndexHoverLay|1)+";"
					+"text-shadow: -"+SHADOW_THICKNESS+"px -"+SHADOW_THICKNESS+"px 0 #FFF,"
					+""+SHADOW_THICKNESS+"px -"+SHADOW_THICKNESS+"px 0 #FFF,"
					+"-"+SHADOW_THICKNESS+"px "+SHADOW_THICKNESS+"px 0 #FFF,"
					+SHADOW_THICKNESS+"px "+SHADOW_THICKNESS+"px 0 #FFF;");
			
			document.body.appendChild(window.helpPointer);
			
			window.helpPointer.srcX=(getWindowSize("width")/2);
			window.helpPointer.srcY=(getWindowSize("height")/2);
		}else{
			jQuery(window.helpPointer).fadeIn(1000);
			
		}
		var helpPointerSlct=jQuery(window.helpPointer);

		var srcX=window.helpPointer.srcX;
		var srcY=window.helpPointer.srcY;
		
		window.helpPointer.setStyle("left:"+srcX+"px");
		window.helpPointer.setStyle("top:"+srcY+"px");

		// Destination coordinates :
//		var destX=(getWindowSize("width")/2);
//		var destY=(getWindowSize("height")/2);
//		if(!empty(targetElement) 
//				&& jQuery(targetElement).is(":visible")
//				){
		var targetRect=targetElement.getBoundingClientRect();
		var destX=targetRect.left+targetRect.width/2;
		var destY=targetRect.top+targetRect.height/2;
//		}
		
		bringElementSelectedToPosition(helpPointerSlct,destX,destY,DELAY,function(){
			helpPointerSlct.fadeOut(1000,function(){
				
				window.helpPointer.srcX=destX;
				window.helpPointer.srcY=destY;
				
				// We go to the next step :
				if(onEndMethod)
					onEndMethod();

			});
		});
		
	};
	
	var displayAdviceBox=function(targetElement,text,/*OPTIONAL*/onEndMethod){
		
		var BACKGROUND="#FFE746";
		
		if(empty(targetElement) || !jQuery(targetElement).is(":visible")){
			// We break the chain :
			alert(incompleteActionMessage);
			return;
		}
		
		if(!window.helpAdviceBox){
			window.helpAdviceBox=document.createElement("span");
			window.helpAdviceBox.setStyle(
					"background-color:"+BACKGROUND+";"
					+"-moz-border-radius: 20px;border-radius: 20px;"
					+"cursor:pointer;"
					+"z-index:"+(maxZIndexHoverLay|1)+";");
			
			var p=document.createElement("p");
			p.innerHTML=text;
			p.setStyle("margin:10px;");
			
			var a=document.createElement("a");
			a.className="closeButton";
			a.innerHTML=closeLabel;
			a.onclick=function(){
				jQuery(window.helpAdviceBox).hide(onEndMethod);
			};
			a.setStyle("display:block;margin-top:20px;float:right;text-decoration:underline;");

			window.helpAdviceBox.appendChild(a);
			window.helpAdviceBox.appendChild(p);
			
			window.helpAdviceBox.setStyle("position:absolute;");
			
			document.body.appendChild(window.helpAdviceBox);
		}else{
			var helpAdviceBoxSlct0=jQuery(window.helpAdviceBox);

			helpAdviceBoxSlct0.show();
			var p=helpAdviceBoxSlct0.find("p");
			p.get(0).innerHTML=text;
			var a=helpAdviceBoxSlct0.find("a.closeButton");
			
			a.get(0).onclick=function(){
				helpAdviceBoxSlct0.hide(onEndMethod);
			};
			
		}
		var helpAdviceBoxSlct=jQuery(window.helpAdviceBox);

		// Destination coordinates :
		var destX=(getWindowSize("width")/2);
		var destY=(getWindowSize("height")/2);
		if(!empty(targetElement)
				&& jQuery(targetElement).is(":visible")
				){
			var targetRect=targetElement.getBoundingClientRect();
			destX=targetRect.left+targetRect.width/2;
			destY=targetRect.top+targetRect.height/2;
		}
		
		helpAdviceBoxSlct.css("left",destX);
		helpAdviceBoxSlct.css("top",destY);
		
	};
	
	
	var doWholeProcessMethodAfterMacrosStrings=function(macrosStringsArray){
		
		var SEPARATOR=";";
		
		var firstFunction=null;
		var oldFunction=null;
		for(var i=0;i<macrosStringsArray.length;i++){
			
			var macroString=macrosStringsArray[i];
			var split1=macroString.split(":");
			var methodName=split1[0];
			var target=split1[1];
			var text="";
			if(contains(target,SEPARATOR)){
				var split2=target.split(SEPARATOR);
				target=split2[0];
				text=split2[1];
			}
			
			if(methodName!=="designate" && methodName!=="advice") continue;
			if(empty(jQuery(target).get()))	continue;
			
			var f=new Object();
			f.target=jQuery(target).get(0);
			f.text=text;
			f.methodName=methodName;
			
			if(methodName==="designate") f.method=designateElement;
			else if(methodName==="advice") f.method=displayAdviceBox;
			
			
			if(oldFunction!==null)
				oldFunction.endMethod=f;

			if(firstFunction===null) firstFunction=f;

			oldFunction=f;

		}
		
		var execFunction=function(f2){
			if(empty(f2))	return;
			
			if(f2.methodName==="designate"){
				f2.method(f2.target
						,function(){
							execFunction(f2.endMethod);
						});
			}else if(f2.methodName==="advice") {
				f2.method(f2.target,f2.text
						,function(){
							execFunction(f2.endMethod);
						});
			}
		};
		
		if(firstFunction!==null){
			execFunction(firstFunction);
		}
		
	};

	doWholeProcessMethodAfterMacrosStrings(macrosStrings);
	
	
}


//		* A polyvalent popup :

// Non-blocking of execution :
function promptValue(label,/*NULLABLE*/type,/*NULLABLE*/defaultValue
		,/*OPTIONAL BUT REQUIRED AS NULLABLE IF NEXT ARGUMENTS ARE SET*/doOnOK
		,/*OPTIONAL*/doOnCancel){
	
	var MAX_Z_INDEX_OVERLAY_LOCAL=9999;// (Repeated because must be stand-alone constant...)
	var POSITION_X="10%";
	var POSITION_Y="5%";
	var SPECIAL_SEPARATOR="@@@";
	
	var div=document.createElement("div");
	div.id="divPrompt";
	
	// Must remain the root body as parent of the prompt window...:
	var parent=document.body;
	parent.appendChild(div);
	
	div.className="promptWindow";
	div.setStyle("position:absolute;top:"+POSITION_X+";left:"+POSITION_Y+";z-index:"+(MAX_Z_INDEX_OVERLAY_LOCAL)+";");
//	div.setStyle("width:50%;height:50%;");
	div.setStyle("border:#888888 8px dotted;padding:10px;");
	
	// Default background :
	div.setStyle("background: #B3DCED !important;" +
			"background: -moz-linear-gradient(-45deg,  #b3dced 0%, #29b8e5 50%, #bce0ee 100%) !important;" +
			"background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#b3dced), color-stop(50%,#29b8e5), color-stop(100%,#bce0ee)) !important;" +
			"background: -webkit-linear-gradient(-45deg,  #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" +
			"background: -o-linear-gradient(-45deg,  #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" +
			"background: -ms-linear-gradient(-45deg,  #b3dced 0%,#29b8e5 50%,#bce0ee 100%)!important;" +
			"background: linear-gradient(135deg,  #b3dced 0%,#29b8e5 50%,#bce0ee 100%) !important;" +
			"filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3dced', endColorstr='#bce0ee',GradientType=1 );");
	
	// The label :
	var labelElement=document.createElement("div");	
	labelElement.innerHTML=label;
	labelElement.setStyle("padding-top:10px;padding-bottom:10px;");
	
	// Eventual validation :
	var validationFunction=function (value){return true;/*DO NOTHING*/};
	validationFunction.errorMessage="";
	if(!empty(type) && contains(type,":")){
		var typeParams=type.split(":");
		if(typeParams.length>1){
			var validationType=typeParams[1];
			
			if(validationType==="email"){
				validationFunction=function (value){return isValidEmail(value);};
				validationFunction.errorMessage="Typed email is incorrect.";
			}
		}
	}
	
	
	// Each field rendering :
	/*private*/var getRenderedInputElement=function(inputType,defaultValueLocal,/*OPTIONAL*/labelLocal){
		
		
		var inputElement=null;
		
		if(contains(inputType,"password")){
			inputElement=document.createElement("input");
			if(defaultValueLocal)	inputElement.value=defaultValueLocal;
			inputElement.cols="80";
			inputElement.type="password";
			
		}else if(contains(inputType,"textbox")){
			inputElement=document.createElement("input");
			if(defaultValueLocal)	inputElement.value=defaultValueLocal;
			inputElement.cols="80";
			inputElement.type="text";
			
		}else if(contains(inputType,"intbox")){
			inputElement=document.createElement("input");
			if(defaultValueLocal)	inputElement.value=defaultValueLocal;
			inputElement.cols="80";
			inputElement.type="number";

		}else if(contains(inputType,"uniqueChoice") || contains(inputType,"multipleChoice")){
			
			var choicesLabelsStrs=defaultValueLocal;
			if(typeof defaultValueLocal === "string"){
				choicesLabelsStrs=parseJSON(defaultValueLocal);
			}
			inputElement=document.createElement("p");
			
			for(var i=0;i<choicesLabelsStrs.length;i++){
				
				var radioButtonContainer=document.createElement("div");
				inputElement.appendChild(radioButtonContainer);
				
				var radio=document.createElement("input");
				
				if(contains(inputType,"multipleChoice")){
					radio.type="checkbox";
					radio.id="checkboxMultipleChoice_"+i;
				}else{
					radio.type="radio";
					radio.id="radioButtonUniqueChoice_"+i;
					radio.name="radioButtonsUniqueChoice";
				}
				
				radio.indexValue=i;
				radioButtonContainer.appendChild(radio);
			
				var choiceLabelStr=choicesLabelsStrs[i];
				var label=document.createElement("label");
				label.innerHTML=choiceLabelStr.trim();
				label.htmlFor=radio.id;
				radioButtonContainer.appendChild(label);
			}
			
			// We redefine the function used to retrieve entered value :
			inputElement.getResultValue=function(){
				var result="";
				jQuery(inputElement).find("input:checked").each(function(){
					var radioSlct=jQuery(this);
					result+=(empty(result)?"":",")+radioSlct.get(0).indexValue;
				});
				
				return result;
			};
			
		}else{
			// DEFAULT IS TEXT AREA :
			inputElement=document.createElement("textarea");
			if(defaultValueLocal)	inputElement.value=defaultValueLocal;
			inputElement.cols="80";
			inputElement.rows="20";
			
		}
		
		inputElement.setStyle("padding:10px");
		
		// The default function used to retrieve entered value :
		if(!inputElement.getResultValue){
			inputElement.getResultValue=function(){
				var result=inputElement.value;
				return result;
			};
		}
		
		// The label :
		inputElement.label=labelLocal;
		
		return inputElement;
	};
	
	
	// We define the global function used to retrieve entered value :
	var getGlobalResultValue=null;
	var defVal="";
	var inputElements=new Array();
	if(!empty(type) && containsIgnoreCase(type,"multiple{")){
		var namesWithTypesAndDefaultValuesStr=type.replace("multiple{","{");
		try{
			
			var defVals=new Array();
			if(!empty(defaultValue))	defVals=defaultValue.split(SPECIAL_SEPARATOR);
			
			var namesWithTypesAndDefaultValues=parseJSON(namesWithTypesAndDefaultValuesStr);
			var j=0;
			for(key in namesWithTypesAndDefaultValues){
				if(!namesWithTypesAndDefaultValues.hasOwnProperty(key))	continue;
				
				var t=namesWithTypesAndDefaultValues[key];
				var label=key;
				if(contains(t,":")){	// Syntax : «<type>:<label>»
					var split=t.split(":");
					t=split[0];
					label=split[1];
				}
				
				if(j<defVals.length)	defVal=defVals[j];
				else					defVal=null;
				
				inputElements.push(getRenderedInputElement(t,defVal,label));
				j++;
			}
			
			// We redefine the global function used to retrieve entered value :
			getGlobalResultValue=function(){
				var result="";
				
				for(var k=0;k<inputElements.length;k++){
					result+=(empty(result)?"":SPECIAL_SEPARATOR)+inputElements[k].getResultValue();
				}
				
				return result;
			};
			
		}catch(e1){
			var errorDiv=document.createElement("div");
			errorDiv.innerHTML="Error parsing JSON «"+namesWithTypesAndDefaultValuesStr+"» : ERROR «:"+e1+"»";
			inputElements.push(errorDiv);
		}
	}else{
		
		var singleInputElement=getRenderedInputElement(type,defaultValue);
		inputElements.push(singleInputElement);
		
		// We redefine the global function used to retrieve entered value :
		getGlobalResultValue=function(){
			return singleInputElement.getResultValue();
		};
	}
	
	
	
	// TODO : FIXME : Focus on text field is not working.... :,-(
	inputElements[inputElements.length-1].focus();
	
	
	var buttonCancel=document.createElement("button");
	buttonCancel.innerHTML="Cancel";
	buttonCancel.setStyle("display:block;float:right;");
	buttonCancel.onclick=function(){
		var promptedValue=getGlobalResultValue();
		if(doOnCancel){
			doOnCancel(promptedValue);
		}
		parent.removeChild(document.getElementById(div.id));
	};
	
	
	var buttonOK = document.createElement("button");
	buttonOK.innerHTML="OK";
	buttonOK.setStyle("display:block;margin-bottom:10px;");
	buttonOK.onclick=function(){
		var promptedValue=getGlobalResultValue();
		
		if(!validationFunction(promptedValue)){
			alert(validationFunction.errorMessage);
			return;
		}
		
		if(doOnOK){
			doOnOK(promptedValue);
		}
		parent.removeChild(document.getElementById(div.id));
	};


	// Positionning :
	div.appendChild(labelElement);
	div.appendChild(buttonCancel);
	div.appendChild(buttonOK);
	
	for(var i=0;i<inputElements.length;i++){
		var br=document.createElement("br");
		div.appendChild(br);
		
		// The label :
		if(inputElements[i].label){
			var divLabel=document.createElement("div");
			divLabel.innerHTML=inputElements[i].label;
			div.appendChild(divLabel);
		}
		
		div.appendChild(inputElements[i]);
	}

}



//		* A carousel : DEPENDS ON PROMPTVALUE(...) FUNCTION !

function makeCarousel(divSlct,width,height,transition,autoScroll,transitionDurationMillis,autoScrollDurationMillis,buttonsCSS,isEditable,doAfterEdit,/*OPTIONAL*/labels){

	if(divSlct===null)	return;
	if(empty(divSlct.get()))	return;

	var TRANSITION_FADE="fade";
	var LEFT="&#9664;";
	var RIGHT="&#9654;";
	var PAUSE="&#9608;";
	var MAX_Z_INDEX_OVERLAY_LOCAL=99999;
	var SPECIAL_SEPARATOR="@@@";
	
	var closeLabel=empty(labels["closeLabel"])?"Close":labels["closeLabel"];
	var editLabel=empty(labels["editLabel"])?"Edit":labels["editLabel"];
	
	
	divSlct.each(function(){

		var thisSlct=jQuery(this);
		var div=thisSlct.get(0);
		
		if(isEditable){
			
			// Edit button displaying (if carousel is rendered only) : 
			divSlct.prepend("<button class='iconBtn editCarousel' title='"+editLabel+"'>&#9998;</button>")
			.find(".editCarousel").click(function(){
				
				promptValue("Please edit carousel attributes"
						,"multiple{'width':'textbox:"+i18n({"fr":"Largeur","en":"Width"})+"'"
						+",'height':'textbox:"+i18n({"fr":"Hauteur","en":"Height"})+"'}"
						,width+SPECIAL_SEPARATOR+height
						
					,function(returnValue){
						if(empty(returnValue))	return;
						
						var s=returnValue.split(SPECIAL_SEPARATOR);
						
						thisSlct.css("width", s[0]).css("height",s[1]);
						
						doAfterEdit(s[0],s[1]);
						
					}
				);
				
			});
		}
		
		
		
		thisSlct.css("display","block")
			.css("width",width)
			.css("height",height);

		
		
		div.isTransitionPending=false;

		
		div.index=0;
		div.itemsSlcts=new Array();

		
		var olSlct=jQuery(div).find("ol");
		olSlct.css("list-style-type","none").css("overflow","hide");
		
		
		var doGoOnNext=function(){
			if(div.isTransitionPending)	return;
			
			var oldIndex=div.index;
			div.index++;
			if(div.itemsSlcts.length-1<div.index)	div.index=0;

			doTransition(div.itemsSlcts[oldIndex],div.itemsSlcts[div.index]);
		};
		
		/*private*/div.startNextsTask=function(){
			if(div.isNextsTaskExisting())	return;

			div.peNexts=new PeriodicalExecuter(function() {
				doGoOnNext();
			},autoScrollDurationMillis/1000);
			
		};
		
		/*private*/div.stopNextsTask=function(){
			if(!div.isNextsTaskExisting())	return;
			div.peNexts.stop();
			delete div.peNexts;
		};
		
		/*private*/div.isNextsTaskExisting=function(){
			return (typeof div.peNexts!=="undefined") && div.peNexts;
		};
		
		// We construct the list of carousel items :
		olSlct.find("li").each(function(index2){
			var liSlct=jQuery(this);
			liSlct.css("text-align","center").css("cursor","pointer");
			
			// Display an overlay :
			liSlct.click(function(){
				var w=getWindowSize("width");
				var h=getWindowSize("height");
				
				var root=document.body;
				
				if(!div.overlayDiv){
					
					div.overlayDiv=document.createElement("div");
					div.overlayDiv.setStyle(""
							+"background:black;" // Cannot set opacity to less or equal to 1, because else it will also make the images and all children elements content transparent !
							+"width:100%;height:100%;"
							+"position:fixed;"
							+"top:0;left:0;z-index:"+MAX_Z_INDEX_OVERLAY_LOCAL+";"
							+"text-align:center;"
							+"vertical-align:middle;"
							),
					root.appendChild(div.overlayDiv);

					
						div.overlayDisplay=document.createElement("div");
						div.overlayDisplay.setStyle(""
								+"width:80%;height:80%;"						
//								+"position:absolute;"
								+"display:inline-block;"
								+"margin:auto;"
								);
						div.overlayDiv.appendChild(div.overlayDisplay);
						
						
						div.overlayCloseButton=document.createElement("button");
						div.overlayCloseButton.setStyle(""
								+"opacity:inherit;margin:auto;");
						div.overlayCloseButton.innerHTML=closeLabel;
						div.overlayDiv.appendChild(div.overlayCloseButton);
						
						jQuery(div.overlayCloseButton).click(function(){
							jQuery(div.overlayDiv).fadeOut(1000);
						});
						
						
				}
				
				jQuery(div.overlayDiv).fadeIn(1000);
				
				div.overlayDisplay.innerHTML=liSlct.html();
				
			});
			
			div.itemsSlcts.push(liSlct);
		});
		
		var hideAllNonSelected=function(){
			olSlct.find("li").each(function(index2){
				var liSlct=jQuery(this);
				if(index2!=div.index)	liSlct.hide();
			});
		};
		hideAllNonSelected();
		
		var doTransition=function(oldElementSlct,newElementSlct){

			
			div.isTransitionPending=true;
			
			if(transition===TRANSITION_FADE){
			
				oldElementSlct.fadeOut(transitionDurationMillis/2,function(){
					newElementSlct.fadeIn(transitionDurationMillis/2,function(){
						hideAllNonSelected();
						div.isTransitionPending=false;
					});
				});
			}else{ // Default transition is no transition at all...:
				
				oldElementSlct.hide({complete:function(){
					newElementSlct.show();
					div.isTransitionPending=false;
				}});
			}
			
		};
		
		// Navigation buttons :
		
		var navigationButtonsCSS=buttonsCSS+";display:inline-block;cursor:pointer;";
		
		olSlct.prepend("<span class='pause' style='"+navigationButtonsCSS+";margin-left:5px;'>"+PAUSE+"</span>");
		olSlct.find(".pause").click(function(event){
			if(div.isNextsTaskExisting())
				div.stopNextsTask();
			else	div.startNextsTask(olSlct);
		});
		
		olSlct.prepend("<span class='prev' style='"+navigationButtonsCSS+";'>"+LEFT+"</span>");
		olSlct.find(".prev").click(function(event){
			if(div.isTransitionPending)	return;
			
			var oldIndex=div.index;
			div.index--;
			if(div.index<0)	div.index=div.itemsSlcts.length-1;
			
			doTransition(div.itemsSlcts[oldIndex],div.itemsSlcts[div.index]);
			
		});
		
		olSlct.prepend("<span class='next' style='"+navigationButtonsCSS+";float:right;'>"+RIGHT+"</span>");
		olSlct.find(".next").click(function(event){
			doGoOnNext();
		});
		
		
		if(autoScroll){
			div.startNextsTask();
		}

		
	});

}



//		- Fonction needed for the components above :


function getWindowSize(coordName){
	var ADJUSTMENT=0;
//	var ADJUSTMENT=20;

	if(coordName==="x" || coordName==="width")
		return window.innerWidth-ADJUSTMENT;
	if(coordName==="y" || coordName==="height")
		return window.innerHeight-ADJUSTMENT;
	return 0;
}


function bringElementSelectedToPosition(elementSlct,x,y,/*OPTIONAL*/delay,/*OPTIONAL*/onEndMethod){
	if(empty(elementSlct.get()))	return;
	elementSlct.animate({left:x+"px",top:y+"px"},(delay|1000),onEndMethod);
}



// UNUSED:
// CAUTION : this is a costly method...
function gotoValueLinearly(source,destination,doOnRefreshTreatment
		,/*OPTIONAL*/onEndMethod,/*OPTIONAL*/refreshingRateMillisParam,/*OPTIONAL*/numberOfStepsParam){
	
	var numberOfSteps=numberOfStepsParam?numberOfStepsParam:10;
	var refreshingRateMillis=refreshingRateMillisParam?refreshingRateMillisParam:500;
		
	var delta = destination - source;
	var sign = delta==0?1: delta/Math.abs(delta);
	var step = (Math.abs(delta * (1/numberOfSteps)));
	
	var cpt=0;
	var pe= new PeriodicalExecuter(function() {

		if (cpt>=numberOfSteps) {
			pe.stop();
			if(onEndMethod)	onEndMethod();
			return;
		}
		
		if(doOnRefreshTreatment)
			doOnRefreshTreatment(source + sign*step, cpt/numberOfSteps);// Advancement ratio
		
		cpt++;
		
	}, refreshingRateMillis/1000);

}


// TODO :redefine window.alert();
// TODO :redefine window.confirm();

// DOES NOT WORK WELL :
// Here is the position in non-rendered text : ie. «<p><b>Hello</b> |world</p>» = 16
function getHTMLCaretPositionInRichEditor(richTextComponent){
	
	if(!richTextComponent)	return -1;
	var editorSlct=jQuery(richTextComponent);
	if(empty(editorSlct.get()))	return -1;
	
	var editableDiv=editorSlct.get(0);

	
//	Utiliser deux compteurs...
	// We iterate over the longest (non-rendered content) string :
	var contentNonRendered=richTextComponent.innerHTML;
	contentNonRendered=toOneSimplifiedLine(contentNonRendered);
	
	// Uses jquery.caret plugin to retrieve the brute caret position :
	// Here is the position in rendered text : ie. «Hello |world» = 7
	var caretPositionRendered=
		(empty(editableDiv.lastCaretPosition) || editableDiv.lastCaretPosition<0)?
				editorSlct.caret():editableDiv.lastCaretPosition;
	
				
	var cptNonRendered=0;
	var cptRendered=0;
	var isIgnorableChunk=false;
	for(var i=0;i<contentNonRendered.length;i++){
		
		var c=contentNonRendered.charAt(i);
		
		if(caretPositionRendered<cptRendered)
			return cptNonRendered;
		
		// We have to escape html entities, too:
		if(c==="<" || c==="&")	isIgnorableChunk=true;
		else if(c===">" || c===";")	isIgnorableChunk=false;
		
		if(!isIgnorableChunk)	cptRendered++;
		cptNonRendered++;
		
	}
	
	return -1;
	

}



function getSelectedText(textComponent){
	
	var selectedText="";
	if(textComponent.tagName.toLowerCase()=="textarea"){
		
		// IE version
		if (document.selection != undefined){
			textComponent.focus();
			var sel = document.selection.createRange();
			selectedText = sel.text;
		}else if (textComponent.selectionStart != undefined){
			// Mozilla version
			var startPos = textComponent.selectionStart;
			var endPos = textComponent.selectionEnd;
			selectedText = textComponent.value.substring(startPos, endPos)
		}
	}else{
		// Mozilla version
	    if (typeof window.getSelection != "undefined") {
	        var sel = window.getSelection();
	        if (sel.rangeCount) {
	            var container = document.createElement("div");
	            for (var i = 0, len = sel.rangeCount; i < len; ++i) {
	                container.appendChild(sel.getRangeAt(i).cloneContents());
	            }
	            selectedText = container.innerHTML;
	        }
	        
	    } else if (typeof document.selection != "undefined") {
			// IE version
	        if (document.selection.type == "Text") {
	        	selectedText = document.selection.createRange().htmlText;
	        }
	    }
	}
	
	return selectedText;
}


function getSelectedValueInSelect(selectElementParam) {
	var selectElement=selectElementParam;
	if(typeof selectElementParam ==="string")
		selectElement=document.getElementById(selectElementParam);
	if(selectElement===null)	return null;
	if(!selectElement.options)	return null;
	var selectedOption=selectElement.options[selectElement.selectedIndex];
	if(!selectedOption)	return "";
	return selectedOption.value?selectedOption.value:"";
}


function renderSimpleSelect(selectId,listArray,style,/*OPTIONAL*/defaultSelectedIndex){
	var html="";
		
	html+="<select id=\""+selectId+"\" style=\""+style+"\">";
	
	for(var i=0;i<listArray.length;i++){
		var item=listArray[i];
		
		html+="<option value='"+item+"' ";
		if(i===defaultSelectedIndex) html+=" selected ";
		html+=">";
		html+=item;
		html+="</option>";
	}
	
	html+="</select>";
	
	return html;
}

function setSelectedValueInSelect(selectId,selectedValue){
	var selectElement=document.getElementById(selectId);
	if(!selectElement)	return;
	
	for(var i=0;i<selectElement.options.length;i++){
		var option=selectElement.options[i];
		if(option.value!==selectedValue)	delete option.selected;
		else option.selected="true";
	}
	
}

function getAsString(DOMElement,/*OPTIONAL*/docParam){
	var doc=null;
	if(docParam)	doc=docParam;
	else doc=document;
	var container = doc.createElement("div");
	container.appendChild(DOMElement.cloneNode(true));
	var result=container.innerHTML;
	return result;
}

function setElementVisible(element,visible){
	if(typeof element==="string")
		element=document.getElementById(element);
	if(element===null)	return;
	if(typeof element.setStyle ==="undefined"){
		if(visible)	element.style.display="block";
		else element.style.display="none";
	}else{
		if(visible)	element.setStyle("display:block");
		else element.setStyle("display:none");
	}
}

function appendAsFirstChildOf(element,parent){
	if(parent.firstChild)
		parent.insertBefore(element,parent.firstChild);
	else
		parent.appendChild(element);
}


//HTML elements management :


function asyncPreloadImage(src,callback){
	var img=new Image();
	img.src=src;
	img.onload=function(){
		callback(img);
	};
}

function getAllHTMLElementsWithClass(className,/*OPTIONAL*/elementWhereToSearchId){
	// PLEASE KEEP CODE :
//	var elements=new Array();
//
//	var els=document.getElementsByTagName("*");
//	for (var i=0;i<els.length;i++){
//		var e=els[i];
//		if (containsIgnoreCase(e.className,className))
//			elements.push(e);
//	}
//	return elements;
	return jQuery((elementWhereToSearchId?("#"+elementWhereToSearchId):"")+" ."+className).get();
}

function getAncestorsOrItselfContainsClassName(element,className){
	if(element.className && !empty(element.className) && containsIgnoreCase(element.className,className))
		return element;
	var parent=element.parentNode;
	if(parent===null)	return null;
	return getAncestorsOrItselfContainsClassName(parent,className);
} 


//UNUSED :
function getAllHTMLElementsStartingWithId(startStr){
	var elements=new Array();

	var r = new RegExp(startStr+"_.*");
	var els=document.getElementsByTagName("*");
	for (var i=0;i<els.length;i++){
		var e=els[i];
		if (r.test(e.id))
			elements.push(e);
	}
	return elements;
}




// Compatibility management :

function isIE () {
	if(typeof window.isIEVar === "undefined" || window.isIEVar===null){
		var myNav = navigator.userAgent.toLowerCase();
		window.isIEVar=(myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
	}
	return window.isIEVar;
}
function isMobile(){
	if(typeof window.isMobileVar=== "undefined" || window.isMobileVar===null)
		// Uses detectmobilebrowser.js external library (Public domain : Unlicence)
		window.isMobileVar=isBrowserMobile();
	return window.isMobileVar;
}



// XML parsing management :
function parseXMLString(txt){
	if (window.DOMParser){
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt,"text/xml");
	}else {
		// Internet Explorer
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt);
	}
	return xmlDoc;
}

// JSON parsing management :
function parseJSON(strParam){
	if(empty(strParam))	return null;
	var str=toOneSimplifiedLine(strParam.trim()).replace(/'/gim,"\"");
	try{
		if(JSON)	return JSON.parse(str);
		return 		jQuery.parseJSON(str);
	}catch(error){
		log("ERROR : Error parsing string «"+strParam+"» as JSON.");
	}
	return null;
}


// String management :

function isValidEmail(value){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function getSoundexFrSimple(strParam) {
	// CODE FOUND AT SOURCE : http://phpjs.org/functions/soundex/
	// http://kevin.vanzonneveld.net
	// + original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// + tweaked by: Jack
	// + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// + bugfixed by: Onno Marsman
	// + input by: Brett Zamir (http://brett-zamir.me)
	// + bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// + original by: Arnout Kazemier (http://www.3rd-Eden.com)
	// + revised by: Rafał Kukawski (http://blog.kukawski.pl)
	
	var str = (strParam + "").toUpperCase().trim();
	if (!str) return "";

	//	French version : SOURCE : http://fr.wikipedia.org/wiki/Soundex
	var sdx = [ 0, 0, 0, 0 ], m = {
		B : 1, P : 1,
		C : 2, K : 2, Q : 2,
		D : 3, T : 3,
		L : 4,
		M : 5, N : 5,
		R : 6,
		G : 7, J: 7,
		X : 8, Z : 8, S : 8,
		F : 9, V : 9
		};
	var i = 0;
	var j;
	var s = 0;
	var c;
	var p;
	
	while ((c = str.charAt(i++)) && s < 4) {
		if (j = m[c]) {
			if (j !== p) sdx[s++] = p = j;
		} else {
			s += i === 1;
			p = 0;
		}
	}
	sdx[0] = str.charAt(0);
	return sdx.join("");
	
}

function getSoundexEn(strParam) {
	// CODE FOUND AT SOURCE : http://phpjs.org/functions/soundex/
	// http://kevin.vanzonneveld.net
	// + original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// + tweaked by: Jack
	// + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// + bugfixed by: Onno Marsman
	// + input by: Brett Zamir (http://brett-zamir.me)
	// + bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// + original by: Arnout Kazemier (http://www.3rd-Eden.com)
	// + revised by: Rafał Kukawski (http://blog.kukawski.pl)
	// * example 1: soundex('Kevin');
	// * returns 1: 'K150'
	// * example 2: soundex('Ellery');
	// * returns 2: 'E460'
	// * example 3: soundex('Euler');
	// * returns 3: 'E460'
	var str = (strParam + "").toUpperCase().trim();
	if (!str) return "";

	var sdx = [ 0, 0, 0, 0 ], m = {
		B : 1, F : 1, P : 1, V : 1,
		C : 2, G : 2, J : 2, K : 2, Q : 2, S : 2, X : 2, Z : 2,
		D : 3, T : 3,
		L : 4,
		M : 5, N : 5,
		R : 6	},	i = 0, j, s = 0, c, p;
	
	while ((c = str.charAt(i++)) && s < 4) {
		if (j = m[c]) {
			if (j !== p) sdx[s++] = p = j;
		} else {
			s += i === 1;
			p = 0;
		}
	}
	sdx[0] = str.charAt(0);
	return sdx.join("");
}

function getHashedString(str){
	return hex_md5(str);
}

function getUniqueIdWithDate(){
	return getHashedString(new Date().getTime()+"");
}


function clearAccentuatedCharacters(str){
	return str
	.replace(/[àâä]/gim,"a")
	.replace(/[éèêë]/gim,"e")
	.replace(/[ìîï]/gim,"i")
	.replace(/[òôö]/gim,"o")
	.replace(/[ùûü]/gim,"u")
	.replace(/[ç]/gim,"c")
	;
}

function toOneSimplifiedLine(str,/*OPTIONAL*/mode){
	
	// USE WITH CAUTION, because it will remove all your «&amp;», «&infin;» and so on HTML characters :
	if(mode && mode=="hard")
		return str.replace(new RegExp("[^\\wàâäéèêëìîïòôöùûüç]+","igm")," ").trim();

	if(mode && mode=="preserveWhitespaces")
		return str.replace(/[\s\t\n\r\v\f]/igm," ").trim();

	if(mode && mode=="compactHTML")
		return str.replace(/[\s\t\n\r]+/igm," ").replace(/>[\s\t\n\r]+</igm,"><")
		.replace(/<br>/gim,"<br />")
		// dangerous substitution, but necessary, unfortunately...
		// (so we have to compensate it somewhere else, with the «´» character for apostrophe in text.)
		.replace("'",'"')
		.trim();
	
	// «Soft» mode is default mode :
	return str.replace(/[\s\t\n\r]+/igm," ").trim();
}

function getXMLElementAsString(element){
	var tmp = document.createElement("div");
	tmp.appendChild(element.cloneNode());
	return tmp.innerHTML;
}


function dec2hex(d) {return d.toString(16);}
function hex2dec(h) {return parseInt(h,16);}


function empty(sizable,/*OPTIONAL*/insist){
	if( (typeof sizable==="undefined") || sizable===null || sizable===false)	return true;
	if( typeof sizable==="string" && insist && (sizable+"").trim()==="")	return true;
	
	if( typeof sizable==="object" ){ // for classical and associative arrays
		
		if(sizable instanceof Array) return sizable.length<=0;
		
		// Actually an unsafe method to determine that variable «sizable» is not a classical (ie. non-associative) array...:

		if(typeof sizable.length==="undefined"){
			for(key in sizable){
				
				if(!sizable.hasOwnProperty(key))	continue;
				return false;
			}
			return true;
		}
		
	}
	
	// Object «sizable» is not really sizable, at this point :
	return sizable.length<=0;
}

function containsIgnoreCase(str,chunk,useRegexp){
	if(empty(str)){
//		//TRACE
//		log("WARN : Container is empty ! Cannot search ignoring case for «"+chunk+"».");
		return false;
	}
	if(!useRegexp)
		return str.toLowerCase().indexOf(chunk.toLowerCase())!==-1;
	return new RegExp(chunk,"gim").test(str);
}


function contains(container,objectToFind,useRegexp){
	if(empty(container)){
//		//TRACE
//		log("WARN : Container is empty ! Cannot search for «"+objectToFind+"».");
		return false;
	}
	if(container instanceof Array){
//		for (var i = 0; i < container.length; i++) {
//			if (container[i] === objectToFind)	return true;
//		}
//		return false;
		return container.indexOf(objectToFind)>=0; 
	}else if(container instanceof Object){
		for(key in container){
			if(!container.hasOwnProperty(key))	continue;
			if (container[key] === objectToFind)	return true;
		}
		return false;
	}
	
	var str=container;
	var chunk=objectToFind;
	if(!useRegexp)
		return str.indexOf(chunk)!==-1;
	return new RegExp(chunk,"gm").test(str);
}

function merge(array1,array2){
	var list=new Array(); 
	for(var i=0;i<array1.length;i++){
		var e=array1[i];
		if(!contains(list,e))	list.push(e);
	}
	for(var i=0;i<array2.length;i++){
		var e=array2[i];
		if(!contains(list,e))	list.push(e);
	}
	return list;
}


function getArrayAsJSONString(sizable){
	
	if( typeof sizable==="object" ){ // for classical and associative arrays
		
		if(sizable instanceof Array){
			var result="[";
			for(var i=0;i<sizable.length;i++){
				result+=(result.length<=1?"":",")+i+":"+'"'+sizable[i]+'"';
			}
			return result+"]";
		}
		
		// Actually an unsafe method to determine that variable «sizable» is not a classical (ie. non-associative) array...:
		if(typeof sizable.length==="undefined"){
			var result="{";
			for(key in sizable){
				if(!sizable.hasOwnProperty(key))	continue;
				result+=(result.length<=1?"":",")+'"'+key+'"'+":"+'"'+sizable[key]+'"';
			}
			return result+"}";
		}
	}
	
	return "";
}

function getArrayFromJSONString(sizableStr){
	if(empty(sizableStr))	return new Array();
	
	try{
		return parseJSON(sizableStr);
	}catch(e){
		//TRACE
		log("ERROR : Error parsing JSON. Details...:");
		log(e);
	}
	return new Array();
}


function removeHTML(strParam,/*OPTIONAL-NULLABLE*/replaceBreakLines,/*OPTIONAL*/replacementForWhatIsRemoved){
	var str=strParam;
	
	
	if(replaceBreakLines)	str=str
								   .replace(/<\/h[1-9]\s*>/gim,"\n")
								   .replace(/<\/div\s*>/gim,"\n")
								   .replace(/<\/p\s*>/gim,"\n")
								   .replace(/<br\s*(\/)*\s*>/gim,"\n")
								   .replace(/\r\n/gim,"")
								   .replace(/\r/gim,"\n");
	
	var r=/<(?:.|\n)*?>/igm;
	str=str.replace(r,replacementForWhatIsRemoved?replacementForWhatIsRemoved:"");

	str=decodeHTMLEntities(str);
	
	return str;
}

function decodeHTMLEntities(strParam){
	var str=strParam;

	// NOT WORKING :
//	str=jQuery("<textarea/>").text(str).html();
	
	str=str.replace(/&nbsp;/gim," ")
		   .replace(/&gt;/gim,">").replace(/&lt;/gim,"<")
		   .replace(/&amp;/gim,"&")
		   // TODO : Add all html entities...
		   ;
	
	return str;
}

// NOT WORKING :
//var decodeHTMLEntities = (function() {
//	
//	  // This prevents any overhead from creating the object each time
//	  var element = document.createElement('div');
//
//	  function decodeHTMLEntitiesLocal(strParam) {
//		 var str=strParam;
//	    if(str && typeof str === 'string') {
//	      // strip script/html tags
//	      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
//	      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
//	      element.innerHTML = str;
//	      str = element.textContent;
//	      element.textContent = '';
//	    }
//
//	    return str;
//	  }
//
//	  return decodeHTMLEntitiesLocal;
//})();


// NOT WORKING :
//function keepOnlyHTML(str,/*OPTIONAL*/replacementForWhatIsRemoved){
//	var r=/^(?!.*<(?:.|\n)*?>)/igm;
//	return str.replace(r,replacementForWhatIsRemoved?replacementForWhatIsRemoved:"");
//}


// UNUSED :
//function escapeHTML(str){
//	var pre = document.createElement("pre");
//	var text = document.createTextNode( str );
//	pre.appendChild(text);
//	var result=pre.innerHTML; 
//	return result;
//}


function getTextWordsExtract(textStrParam,wordsNumber,/*OPTIONAL*/wordPositionOffset){
	
	if(wordsNumber<=0)	return "";
	
	var textStr=toOneSimplifiedLine(removeHTML(textStrParam));
	
	var split=textStr.split(" ");
	
	var result="";
	
	var i=0;
	if(wordPositionOffset && isNumber(wordPositionOffset) && wordPositionOffset>0) i=wordPositionOffset;

	// One-character (not letter-like) sized words don't count :
	var split2=new Array();
	for(var j=0;j<split.length;j++){
		var s=split[j];
		if(s.length<1)	continue;
		if(s.length==1 && new RegExp("[^\\wàâäéèêëìîïòôöùûüç]","igm").test(s))	continue;
		split2.push(s);
	}
	
	for(;i<split2.length && i<wordsNumber;i++){
		var s=split2[i];
		result+=(empty(result)?"":" ")+s;
	}
	
	
	return result;
}

function getTextExtract(charactersNumberBefore,charactersNumberAfter,str,word){
	var DOTS_STRING="(...)";
	var HTML_HIGHLIGHT_TYPE="b";

	if(empty(str))	return "";
	
	var wordIndex=str.toLowerCase().indexOf(word.toLowerCase());
	if(wordIndex==-1 
		|| charactersNumberBefore < 0 || charactersNumberAfter < 0){
		return str.substring(0,Math.abs(charactersNumberBefore)+Math.abs(charactersNumberAfter));
	}

	var startIndex=Math.max(0,wordIndex-charactersNumberBefore);
	var endIndex=Math.min(str.length,wordIndex+word.length+charactersNumberAfter);
	
	var originalWordFromContent=str.substring(wordIndex,wordIndex+word.length);
	var r=new RegExp(word,"im");
	return (startIndex<=0?"":DOTS_STRING)
			 +str.substring(startIndex,endIndex)
			 .replace(r,"<"+HTML_HIGHLIGHT_TYPE+">"+originalWordFromContent+"</"+HTML_HIGHLIGHT_TYPE+">")
			 +(endIndex>=str.length?"":DOTS_STRING);
}

// Variables management :
function swapValues(a,b){
	var temp=a;
	a=b;
	b=temp;
}

// Arrays management :
function copy(array) {
	var result = null;
	
//	if(!isAssociative){
	if(array instanceof Array){

		result = new Array();
		for( var i = 0; i < array.length; i++)
			result.push(array[i]);
	}else{
		// Case of regular objects that are like associative arrays :

		result = new Object();
		for(key in array){
			if(!array.hasOwnProperty(key))	continue;
			result[key]=array[key];
		}
	}
	return result;
}

function remove(array, element) {
	
	if(array instanceof Array){
		
		if (empty(array))	return;
		var i = array.indexOf(element);
		if (i < 0)	return;
		array.splice(i, 1);
	}else{
		// Case of regular objects that are like associative arrays :
		for(key in array){
			if(!array.hasOwnProperty(key))	continue;
			if(array[key]===element){
				delete array[key];
				break;
			}
		}
	}
}

// UNUSED
//// CAUTION : ONLY WORKS ON Array ARRAYS, NOT Object ARRAYS !
//function swap(array, i1, i2) {
//	var temp = array[i1];
//	array[i1] = array[i2];
//	array[i2] = temp;
//}

// Types management :
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function isBoolean(n) {
	return typeof b === 'boolean';
}

// URL management :
function getDocumentFileName() {
	return getFileNameFromURL(document.location.href);
}

function getFileNameFromURL(url,/*OPTIONAL*/forceHTMLFileNameAppending) {
		var end = url.length;
		
		// From most outwards...
		if(url.indexOf("#") != -1)	end=url.indexOf("#");
		// ...to most inwards :
		if(url.indexOf("?") != -1)	end=url.indexOf("?");
		
		// Everything from last «/» to the end...:
		var result=url.substring(url.lastIndexOf("/")+1, end).trim();
		
		if(forceHTMLFileNameAppending && !contains(result,".html")){
			result=result
				+(empty(result) || result.charAt(result.length-1)==="/"
					?"index.html":".html");
		}
		
		return result;
}

function getCalculatedPageName(/*OPTIONAL*/pathParam){
	
	var path=pathParam;
	
	//TODO : Reinforce this check with a regular expression instead :
	if(empty(path,true)){
		
		var url=getWholeURLExceptQueryAndHash();
//		return empty(getFileNameFromURL(url))?url+"/index.html":url;
		path=getFileNameFromURL(url,true);
		
		if(empty(path,true)){
			alert("You must enter a non-empty and valid server URL.");
			return null;
		}
		
		return path;
		
	}
	
	//TODO : Reinforce this check with a regular expression instead :
	if(!contains(path,".html")){
		// TRACE
		log("WARN : Your URL does not point to a valid HTML file name, "
			+"then we force html extension to page name.");

//		var fileNameFromURL=getFileNameFromURL(path).trim();
//		return fileNameFromURL
//			+(fileNameFromURL.charAt(fileNameFromURL.length-1)==="/"
//				?"index.html":".html");
		
	}
	
	return getFileNameFromURL(path,true);
}


function getAbsoluteUrlForRelativeUrl(relativeURL){
	var serverURL=getCalculatedServerURLOnly();
	if(empty(relativeURL))	return serverURL;
	if(containsIgnoreCase(relativeURL,"(ht|f)tp[s]*://",true))	return relativeURL;
	return serverURL+relativeURL;
}

//Always has a final slash «/» appended on its end :
function getCalculatedServerURLOnly(){
	var url=getWholeURLExceptQueryAndHash();
	var filename=getFileNameFromURL(url);
	return addFinalSlashToURL(
			empty(filename)?url:url.replace("/"+filename,"")
		   );
}

// CAUTION : TO USE THIS FUNCTION, YOU MUST BE SURE THAT THERE *IS* ACTUALLY A SERVER PART, IT WILL NOT VERIFY BY ITSELF...
// OR ELSE IT WILL CAUSE UNPREDICTABLE BEHAVIOR :
function removeServerPartOfURL(urlWithServerPart){
//	return urlWithServerPart.replace(/^((ht|f)tp[s]*:\/\/[\W\.]\/)*/gim,"");
//	return urlWithServerPart.replace(getCalculatedServerURLOnly(),"");
	
	var result=urlWithServerPart;
	if(containsIgnoreCase(result,"(ht|f)tp[s]*://",true))
		result=result.replace(/(ht|f)tp[s]*:\/\//gim,"");

	return result
		.replace(result.substring(0,result.indexOf("/")),"")
		.replace(/^\//gim,"");
}

function getURLAnchor() {
	var url=document.location.href;
	if(!contains(url,"#"))	return "";
		return url.substring(url.lastIndexOf("#")+1, url.length);
}
	
function getURLParameter(nameParam) {
	var name=encodeURIComponent(nameParam);
	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(window.location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

function getWholeURLExceptQueryAndHash() {
	return window.location.href
	.replace(window.location.search,"")
	.replace(window.location.hash,"");
}

function addFinalSlashToURL(url){
	var result=url+(!empty(url) && url.charAt(url.length-1)==="/"?"":"/");
	return result;
}


// Persistence management :
function storeString(name,value,/*OPTIONAL*/forceUseCookie){
	if(!isHTML5StorageSupported("local") || forceUseCookie){
		//TRACE
		log("WARN : Storage is not supported by this browser, writing to cookie.");
		jQuery.cookie(name,value);
		return value;
	}
//	sessionStorage.setItem(name, value);
	localStorage.setItem(name, value);
	return value;
}
function getStringFromStorage(name,/*OPTIONAL*/forceUseCookie){
	if(!isHTML5StorageSupported("local") || forceUseCookie){
		//TRACE
		log("WARN : Storage is not supported by this browser, reading from cookie.");
		return jQuery.cookie(name);
	}
//	return sessionStorage.getItem(name);
	return localStorage.getItem(name);
}

function isHTML5StorageSupported(type) {
	if(!empty(type) && type==="local"){
		try {
		  return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
		  return false;
		}
	}
	try {
	  return 'sessionStorage' in window && window['sessionStorage'] !== null;
	} catch (e) {
	  return false;
	}
	return false;
}

// Files management :
function validateFilesExtensions(self,acceptedFilesTypesString){
	// UPLOADED FILES TYPES :
	var acceptedFilesTypes=acceptedFilesTypesString.split(",");
	var acceptedFilesExtensionsRegExpStr="";
	if(!empty(acceptedFilesTypes)){
		acceptedFilesExtensionsRegExpStr+="(";
		for(var i=0;i<acceptedFilesTypes.length;i++){
			acceptedFilesExtensionsRegExpStr+=(acceptedFilesExtensionsRegExpStr=="("?"":"|")+"\."+(acceptedFilesTypes[i].split("/")[1]);
		}
		acceptedFilesExtensionsRegExpStr+=")";
	}

	var files=self.files;
	var isCorrect=true;
	for(var i=0;i<files.length;i++){
		var file=files[i];
		 
		var name = file.name;
//		var size = file.size;
		var type = file.type;

		
		if(type){
			isCorrect=contains(acceptedFilesTypesString,type);
			if(!isCorrect)	break;
			
		}else{
			isCorrect=contains(name,acceptedFilesExtensionsRegExpStr,true);
			if(!isCorrect)	break;
		
		}
		// TODO : Add maximum size restriction :
		
		
		if(!isCorrect){
			alert("File type is incorrect (must be in : «"+acceptedFilesTypes+"»)");
			self.value="";
			return false;
		}
	}
	return true;
}

function getGenericFileType(fileParam){
	var OTHER_TYPE="other";
	
	var file = fileParam;
	if(!file){
		//TRACE
		log("ERROR : file is undefined !");
		return OTHER_TYPE;
	}
	
	if(file.type){
		if(containsIgnoreCase(file.type,"image"))	return "image";
		if(containsIgnoreCase(file.type,"audio"))	return "audio";
		if(containsIgnoreCase(file.type,"video"))	return "video";
		if(containsIgnoreCase(file.type,"text"))	return "text";
	}else if(file.name){
		// A less safe method, if no type could be retrieved from file :
		if(containsIgnoreCase(file.name,"(\.jpg|\.jpeg|\.png|\.gif)",true))	return "image";
		if(containsIgnoreCase(file.name,"(\.wav|\.mp3|\.ogg|\.mpeg)",true))	return "audio";
		if(containsIgnoreCase(file.name,"(\.mp4|\.avi)",true))				return "video";
		if(containsIgnoreCase(file.name,"(\.pdf|\.txt|\.doc)",true))		return "text";
	}else if(typeof file === "string"){
		// It is an unelegant manner to not duplicate code :
		var filename=file;
		file=new Object();
		file.name=filename;
		return getGenericFileType(file);
	}
	return OTHER_TYPE;
}

function getFileType(fileParam){
	var file = fileParam;
	if(file.type){
		return file.type;
	}else if(file.name){
		// A less safe method, if no type could be retrieved from file :
		if(containsIgnoreCase(file.name,"(\.jpg|\.jpeg)",true))	return "image/jpeg";
		if(containsIgnoreCase(file.name,"(\.png)",true))		return "image/png";
		if(containsIgnoreCase(file.name,"(\.gif)",true))		return "image/gif";
		if(containsIgnoreCase(file.name,"(\.mp3)",true))		return "audio/mp3";
		if(containsIgnoreCase(file.name,"(\.ogg)",true))		return "audio/ogg";
		if(containsIgnoreCase(file.name,"(\.wav)",true))		return "audio/wav";
		if(containsIgnoreCase(file.name,"(\.mpeg)",true))		return "audio/mpeg";
		if(containsIgnoreCase(file.name,"(\.mp4)",true))		return "video/mp4";
		if(containsIgnoreCase(file.name,"(\.avi)",true))		return "video/avi";
		if(containsIgnoreCase(file.name,"(\.pdf)",true))		return "text/pdf";
		if(containsIgnoreCase(file.name,"(\.txt)",true))		return "text/txt";
		if(containsIgnoreCase(file.name,"(\.doc)",true))		return "text/doc";
	}else if(typeof file === "string"){
		// It is an unelegant manner to not duplicate code :
		var filename=file;
		file=new Object();
		file.name=filename;
		return getFileType(file);
	}
	return "other";
}

// Functions management :
Function.prototype.clone = function() {
		var cloneObj = this;
		if(this.__isClone) {
			cloneObj = this.__clonedFrom;
		}

		var temp = function() { return cloneObj.apply(this, arguments); };
		for(var key in this) {
				temp[key] = this[key];
		}

		temp.__isClone = true;
		temp.__clonedFrom = cloneObj;

		return temp;
};




// Geometry mathematics 2D :

// UNUSED
Math.getRandomIntAroundValue=function(target,area,/*OPTIONAL*/exclusionParam){
	var exclusion=exclusionParam;
	if(!exclusionParam)	exclusion=0;
//	return Math.floor((Math.random()-0.5)*(area-exclusion)+target+exclusion);
	// TODO : FIXME : On today, exclusion param is always ignored...: 
	return Math.floor((Math.random()-0.5)*area+target);
};

Math.toRadians = function(degrees) {
	//Converts from degrees to radians.
	return degrees * Math.PI / 180;
};

Math.translate=function(point,xDelta,yDelta){
	point.x+=xDelta;
	point.y+=yDelta;
	return point;
};

Math.toDegrees = function(radians) {
	//Converts from radians to degrees.
	return radians * 180 / Math.PI;
};

function polarPositionDegreesToCartesianPosition(angleDegrees, distance){
	var result=new Array();
	result["x"] = distance * Math.cos(Math.toRadians(angleDegrees));
	result["y"] = distance * Math.sin(Math.toRadians(angleDegrees));
	return result;
}


function isOriented(orientation,width,height){
	if(orientation=="portrait"){
		return width<=height;
	}
	if(orientation=="landscape"){
		return width>height;
	}
	throw new Error("Please provide \"portrait\" or \"landscape\" as orientation.");
 
}

// Media management :

function playMedia(audioOrVideoElement) {
    if (!audioOrVideoElement)	return;
    if(audioOrVideoElement.paused) audioOrVideoElement.play();
    else audioOrVideoElement.pause();
}

// Webcam/microphone management :
function hasGetUserMedia() {
  // Note: Opera is unprefixed.
  return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
}


function getWebcamHandler(videoTagId){
	
	var b=hasGetUserMedia();
	
	if (b) {

		var webcamHandler=new Object();
		
		window.URL = window.URL || window.webkitURL;
		navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia ||
		                          navigator.mozGetUserMedia || navigator.msGetUserMedia;
		
		var videoTag=document.getElementById(videoTagId);
		if(videoTag==null){
			var backgroundContainerDiv=document.getElementById(AOTRA_SCREEN_DIV_ID)?document.getElementById(AOTRA_SCREEN_DIV_ID):document.body;
			
			videoTag=document.createElement("video");
			videoTag.id=videoTagId;
			videoTag.autoplay=true;
			videoTag.setStyle("display:none");
			appendAsFirstChildOf(videoTag,backgroundContainerDiv);
		}
		
		webcamHandler.video = videoTag;
		
		webcamHandler.localMediaStream = null;
		
		if (navigator.getUserMedia) {
			var self=webcamHandler;

			// Not showing vendor prefixes or code that works cross-browser.
			var initStream=function(stream){
				self.video.src = window.URL.createObjectURL(stream);
				self.localMediaStream = stream;
			};
			navigator.getUserMedia({video: true}, initStream, function(){
				// In case of not working, or user denied access to her/his webcam :
				// TRACE
				log("ERROR: Could not acquire webcam image.");
			});
			
		} else {
			// TRACE
			log("Your browser doesn't have a getUserMedia() usable method.");
			video.src = "";
		}
		
		webcamHandler.isReady=function(){
			return webcamHandler.video!==null;	
		};
		
		return webcamHandler;
	}
	
	// TRACE
	log("Method getUserMedia() is not supported in your browser");
	return null;
	
}

function getMicHandler(){
	
	var b=hasGetUserMedia();
	
	if (b) {

		var micHandler=new Object();

		// TODO : Develop...
	
		
		return micHandler;
	}
	
	// TRACE
	log("Method getUserMedia() is not supported in your browser");
	return null;
	
}


//Drawing and graphics management :

function getBeamLikeGradient(ctx,displayX1,displayY1,displayX2,displayY2,size,color,alpha,/*OPTIONAL*/zoomFactor){
	
	var lx = displayX2 - displayX1;
	var ly = displayY2 - displayY1;
	var lineLength = Math.sqrt(lx*lx + ly*ly);
	var wy = lx / lineLength * size*(!zoomFactor?1:zoomFactor);
	var wx = ly / lineLength * size*(!zoomFactor?1:zoomFactor);
	var grd = ctx.createLinearGradient(displayX1-wx/2, displayY1+wy/2, displayX1+wx/2, displayY1-wy/2);
	
	var colorComps=htmlColorCodeToDecimalArray(color);
	grd.addColorStop(0,"rgba(0,0,0,0)");
	grd.addColorStop(0.25,"rgba("+colorComps[0]+","+colorComps[1]+","+colorComps[2]+","+alpha+")");
	grd.addColorStop(0.50,"rgba(255,255,255,"+alpha+")");
	grd.addColorStop(.75,"rgba("+colorComps[0]+","+colorComps[1]+","+colorComps[2]+","+alpha+")");
	grd.addColorStop(1,"rgba(0,0,0,0)");
	return grd;
}

function htmlColorCodeToDecimalArray(hexColorCodeParam){
	var hexColorCode=hexColorCodeParam.replace("#","");
	
	if(hexColorCode.length<6)
		return [hex2dec(hexColorCode.charAt(1))*16
						,hex2dec(hexColorCode.charAt(2))*16
						,hex2dec(hexColorCode.charAt(3))*16];
	
	return [hex2dec(hexColorCode.substring(0,2))
					,hex2dec(hexColorCode.substring(2,4))
					,hex2dec(hexColorCode.substring(4,6))];
}

// Geocoding and maps management :

function getMapPositionFromLatLngStr(positionStrParam){
	var positionStr=positionStrParam.replace(";"," ");
	var split=positionStr.split(" ");
	if(split.length!==2 || !isNumber(split[0]) || !isNumber(split[1]))	return null;
	return new google.maps.LatLng(parseFloat(split[0]),parseFloat(split[1]));
}

//Google Analytics functions :
function initGoogleAnalytics(key,domain){
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', key, domain);
		ga('send', 'pageview');

}


//
//// FUNCTIONS ONLY TO COMPENSATE A GOOGLE MAPS API V3 LACK : cf. http://qfox.nl/notes/116 ------------------------------


////NOT WORKING :
//function latlngToPoint(map, latlngPosition){
//	// You can do this with a dummy overlay as well:
//	var overlay = new google.maps.OverlayView();
//	overlay.draw = function() {//DO NOTHING
//	};
//	overlay.setMap(map);
////	var ADJUSTMENT=80;
//	if(!overlay || !overlay.getProjection())	return null;
//	var result=overlay.getProjection().fromLatLngToContainerPixel(latlngPosition);
////	var result=overlay.getProjection().fromLatLngToDivPixel(latlngPosition);
////	var result=Math.translate(overlay.getProjection().fromLatLngToContainerPixel(latlngPosition),ADJUSTMENT,0);
//	return result;
//}

//// NOT WORKING :
//function pointToLatlng(map,x,y){
//	// You can do this with a dummy overlay as well:
//	var overlay = new google.maps.OverlayView();
//	overlay.draw = function() {//DO NOTHING
//	};
//	overlay.setMap(map);
//	if(!overlay || !overlay.getProjection())	return null;
//	var result=overlay.getProjection().fromDivPixelToLatLng(new google.maps.Point(x, y));
//	return result;
//}

// END OF FUNCTIONS ONLY TO COPENSATE A GOOGLE MAPS API V3 LACK. ------------------------------


// HTML5 CONTEXT ENHANCEMENTS, ONLY TO COMPENSATE A HTML5 CANVAS LACK : cf. http://stackoverflow.com/questions/4576724/dotted-stroke-in-canvas ------------------------------
function drawDashedLine(ctx,x, y, x2, y2, da){
    if (!da) da = [10,5];
    var dx = (x2-x);
    var dy = (y2-y);
    var len = Math.sqrt(dx*dx + dy*dy);
    var rot = Math.atan2(dy, dx);
    ctx.translate(x, y);
    ctx.moveTo(0, 0);
    ctx.rotate(rot);       
    var dc = da.length;
    var di = 0;
    var draw = true;
    x = 0;
    while (len > x) {
        x += da[di++ % dc];
        if (x > len) x = len;
        if(draw) ctx.lineTo(x, 0);
        else ctx.moveTo(x, 0);
        draw = !draw;
    }
}

// END OF HTML5 CONTEXT ENHANCEMENTS, ONLY TO COMPENSATE A HTML5 CANVAS LACK. ------------------------------


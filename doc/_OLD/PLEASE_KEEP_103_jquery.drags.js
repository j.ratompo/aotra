// This code was found at http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
// And appears to be public.

// DOESN'T WORK (YET):
//(function(jQuery) {
//jQuery(document).ready(function(jQuery){
function initDrags(jQuery){
	
    jQuery.fn.drags = function(optParam) {
    	
        var opt = jQuery.extend({handle:"",cursor:"move"}, optParam);

        var element = this;
        if(opt.handle !== "") {
            element = this.find(opt.handle);
        }

        var beginFunction=function(e,thisSlct){

        	var dragElement = thisSlct;
            if(opt.handle === "")   dragElement.addClass('draggable');
            else		           	dragElement = thisSlct.addClass('active-handle').parent().addClass('draggable');
            
            var z_idx = dragElement.css('z-index'),
                drg_h = dragElement.outerHeight(),
                drg_w = dragElement.outerWidth(),
                pos_y = dragElement.offset().top + drg_h - e.pageY,
                pos_x = dragElement.offset().left + drg_w - e.pageX;
            
            dragElement.css('z-index', 1000).parents().on("mousemove", function(e) {
            	
                jQuery('.draggable').offset({
                    top:e.pageY + pos_y - drg_h,
                    left:e.pageX + pos_x - drg_w
                }).on("mouseup", function() {
                    jQuery(this).removeClass('draggable').css('z-index', z_idx);
                });
            });
            e.preventDefault(); // disable selection
        };
        
        var endFunction=function(thisSlct){
        	
            if(opt.handle === "")	thisSlct.removeClass('draggable');
            else					thisSlct.removeClass('active-handle').parent().removeClass('draggable');
        };
        
//        var onClickFunction=function(e){
//        	var thisSlct=element;
//        	
//        	var el=thisSlct.get(0);
//        	if(!el.toggled){
//        		el.toggled=true;
//        		beginFunction(e,thisSlct);
//        	}else{
//        		endFunction(thisSlct);
//        		el.toggled=false;
//        	}
//        };
        
        element
        .css('cursor', opt.cursor)
        .on("mousedown",function(e){beginFunction(e,element);})
        .on("mouseup",function(){endFunction(element);})
//      .on("click", onClickFunction)
        // Additional free functions :
//        .parents().click(function(ev){
//        	var thisSlct=element;
//        	var el=thisSlct.get(0);
//        	if(el.toggled)	endFunction(element);
//        })
        ;
        
        return element;
    	
    };
    
    
    jQuery(".drags").drags();
    

//})(jQuery);
//});
}    

<?php

// PHP ERRORS DISPLAY : DEBUG ONLY (DEACTIVATED FOR SECURITY REASONS) :
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

// How to generate a password hash ?
// On the server run the following bash commands :
//  * To generate a simple hash : KEY=$(sudo cat ../private/saltkey.txt); echo $(curl -s "http://localhost/aotra.php?actionType=GENERATEPASSWORDHASH&saltKey=$KEY&clearTextPassword=ZZZZ");KEY="";
//  * To generate a fancy hash :  KEY=$(sudo cat ../private/saltkey.txt); echo '<!--writePasswordsHashesSalted:'$(curl -s "http://localhost/aotra.php?actionType=GENERATEPASSWORDHASH&saltKey=$KEY&clearTextPassword=ZZZZ")'-->';KEY="";
//  (Optional :  history -c; bash; )
// Alternatively, you can use «lynx» or any headless browser to perform the same request as with this curl command. Simply enter the same url passed as the curl command argument.
// If you have a graphical user interface available on your server, you can also use a headfull browser.


// Configuration :

// aotra server-side main configuration :

// Configuration for this script. You should'nt have to modify these values...
// This amount of seconds is the time user has to wait between each write/view password validation requests.
// (It only exists to make brute force attackers a little more annoyed...)
// It also can be a little annoying for user, because he won't be able to save during this freeze time,
// but you must understand that if we reset freeze time to zero when password is correct, then
// an attacker only has to brute force until the freeze time is reset instantly (meaning he/she found the right password),
// thus making this freeze time totally ineffective. So please set this value to a non-zero seconds amount at your discretion.
// (10 or 5 seconds are good values)
$FREEZE_SECONDS=10;

// The log filename :
$LOG_FILE_NAME="../logs/aotra.log";

// Server salt key :
// This key must be the same that was used to generate the hashes to stamp in the html file.
// IT MUST REMAIN PRIVATE because aotra uses a symetric encryption for writing and encrypting passwoord protection.
// The salt key file configuration :
$SALTKEY_LENGTH=512;
$SALT_KEY_FILE_NAME="../private/saltkey.txt"; //(only if file does not exists we generate the key file)
// IF SALTKEY FILE DOES NOT EXIST, then it is automatically generated at this location,
// by simply running this script, accessing thispage with no parameters.

// Information :

/*
 * # Description
 * A simple back-end file to achieve persistence on an aotra providing server.
 *
 * # 3 server configuration Prerequisites :
 * ( 0 : We assume that a simple php5+ server is properly installed, configured and running.)
 * (Note that any PHP server will do, but we provide you the configuration above for an Apache2 PHP server. Please adapt your server configuration in consequence.)
 * 1) File system access rights : Be sure that your PHP web server whatever it is has the right to write to aotra files location.
 * 2) Apache configuration :
 * For a proper functioning of this PHP server component, please set the following parameters in your
 * «/etc/php/<PHP VERSION>/apache2/php.ini» file :
 * file_uploads = On
 * upload_max_filesize = 100M
 * post_max_size = 200M
 * allow_url_fopen On
 * (disclaimer : Although it should not be critical, we do not guarantee the results of this script running on an environment NOT configured like above will be consistent with what expected.)
 * 3) Apache configuration for SECURITY : (it is of UTMOST IMPORTANCE to make your server understand that it MUST NOT EXECUTE php files under *_html/ directories !)
 * «/etc/apache2/apache2.conf» (+ if not already, activate the apache «alias» module with following command : 
 * «sudo a2enmod alias;sudo service apache2 restart;» )
 * RedirectMatch 403 "^(.*)_html/(.*)\.php"
 * 4) Create a .htaccess file in the directory where this file is, to prevent access to the saltkey file :
 * <Files "saltkey.txt">
 * require all denied
 * </Files>
 * 5) Install stupid functionnality mbstring :  sudo apt-get install php-mbstring
 *
 * NO : DOES NOT WORK : 4) Install MCRYPT : (openssl DOES NOT WORK.) sudo apt-get install php5-mcrypt ; sudo php5enmod mcrypt
 *
 * # About
 * -Project name : «aotra»
 * -Project license : LHGPL(Help Burma-Humanitary LGPL) (see aotra README information for details : https://alqemia.com/aotra.js )
 * -Author name : Jérémie Ratomposon massively helped by its programmating egos legions
 * -Author email : info@alqemia.com
 * -Organization name : Projet Alambic
 * -Organization email : admin@alqemia.com
 * -Organization website : https://alqemia.com
 *
 * #POSSIBLE INPUT PARAMETERS :
 * actionType (possible values :
 * "WRITE","UPLOAD",
 * "GENERATEPASSWORDHASH","DECRYPT","WRITEENCRYPTED",
 * "MAKEMEASANDWICH", "SUDOMAKEMEASANDWICH",
 * "PING"
 * ; all other values will be ignored and throw a 0104 error)
 * pageName
 * writePasswordHash
 * encryptPasswordHash
 * passwordClue
 * decryptPasswordHash
 * viewPasswordHash
 * data
 * otherServerURL
 * saltKey
 * clearTextPassword
 */

// aotra server-side advanced configuration :

// If needed, we make sure the hashing will take a long time, so that
// if the private salt key is leaked, it will be fairly difficult to crack
// password write protection and content encryption :
// Adjust this value for performance versus security :
// A greater value will make it more secure because more difficult to brute-force password guessing
// but also will make save/encrypting time bigger,
// A lesser value will make it more quick to save / encrypt, but also more easy to brute force password guessing.
// CAUTION : YOU MUST *NOT* CHANGE THIS VALUE, UNLESS THE HASH HEAVY TREATMENT VALUE IN YOUR AOTRA CLIENT-SIDE CODEIS THE SAME !
$HEAVY_TREATMENT_LOOP_COUNT=100000;

// DOES NOT WORK :
// // $CIPHER_NAME="AES-256-CBC";
// $CIPHER_NAME=MCRYPT_RIJNDAEL_128;

// These directories are the name of the directories where to store uploaded files according to their types :
// (MUST MATCH CLIENT-SIDE UPLOADING FILES STORING CONTRACT)
$IMAGES_TYPE_DIRECTORY="images";
$VIDEOS_TYPE_DIRECTORY="videos";
$AUDIOS_TYPE_DIRECTORY="audios";
$OTHERS_TYPE_DIRECTORY="";

// These are the characters separating password hashes :
$ACCOUNTS_SEPARATOR=",";
$ACCOUNT_DEFAULT_NAME="username";
$PASSWORDS_SEPARATOR="=";

// These are all the message coming from the server in response of requests :
$MESSAGES=array();
// #POSSIBLE AJAX OUTPUT CODES :

// - SPECIAL CODE :
$MESSAGES["0000"]="0000 DO NOTHING:Nothing was done.";

// - SUCCESS codes :
$MESSAGES["0001"]="0001 SUCCESS:Data were correctly written to file.";
$MESSAGES["0002"]="0002 SUCCESS:Files were correctly uploaded.";
// Easter egg #1 :
$MESSAGES["0003"]="0003 SUCCESS:Okayyy.";
// Presence check
$MESSAGES["0004"]="0004 SUCCESS:Back-end file here.";

// - 01XX Errors are errors of missing or incorrect arguments :
$MESSAGES["0100"]="0100 ERROR:No parameters received.";
$MESSAGES["0101"]="0101 ERROR:Server expected HTML file page name but none received.";
$MESSAGES["0102"]="0102 ERROR:Server expected data content name but none received.";
$MESSAGES["0103"]="0103 ERROR:Server expected writePasswordHash but none received.";
$MESSAGES["0104"]="0104 ERROR:Server did not understand action type.";
$MESSAGES["0105"]="0105 ERROR:Server expected an action type but none received.";
$MESSAGES["0106"]="0106 ERROR:Server expected a salt key but none received.";
$MESSAGES["0107"]="0107 ERROR:Server expected a clear-text password but none received.";
$MESSAGES["0108"]="0108 ERROR:Server expected an encrypting/decrypting password but none received.";
$MESSAGES["0109"]="0109 ERROR:Server could not find encrypted aotraScreen content.";

// - 02XX Errors are errors of normal treatment failure :
$MESSAGES["0201"]="0201 ERROR:Write password is incorrect.";
$MESSAGES["0202"]="0202 ERROR:I am a coffee pot."; // UNUSED ERROR SLO, NEVER THROWN.
$MESSAGES["0203"]="0203 ERROR:Referer URL is not coming from localhost.";
$MESSAGES["0204"]="0204 ERROR:Write password freeze time hasn't passed yet (".$FREEZE_SECONDS." seconds).";
$MESSAGES["0205"]="0205 ERROR:Decrypt password freeze time hasn't passed yet (".$FREEZE_SECONDS." seconds).";
$MESSAGES["0206"]="0206 ERROR:Decrypt password is incorrect.";

// - 03XX Errors are errors of system failure :
$MESSAGES["0301"]="0301 ERROR:SYSTEM CANNOT OPEN FILE.";
$MESSAGES["0302"]="0302 ERROR:System could not write to file.";
$MESSAGES["0303"]="0303 ERROR:System could not create directory.";
// Easter egg #1 :
$MESSAGES["0304"]="0304 ERROR:Just no.";

// Additional message
$ADDITIONAL_MESSAGE="\n This file is not available for free download, and is released"."\n under the terms of the Humanitary LGPL Licence. Permissions to copy and use it are granted"."\n by author Jeremie Ratomposon on a per-case basis on today (11/2015)."."\n Please contact for commercial information at info@alqemia.com";


// Salt key retrieving :
$SALT_KEY=readOrWriteSaltKeyLineFromFile($SALT_KEY_FILE_NAME,true);


// POSSIBLE DEBUG
// phpinfo();

$isDefaultFunctionningOverrided=false;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@ CORE MODULES SECTION @@@@@@@@@@@@@@@@@@@@@@@@@@@
// A module overrides default functionning at two conditions :
// 1- If core module is enabled
// 2- If the GET/POST call to this endpoint file has at least one of concerned core module parameters specified
// Else it simply will pass the hand to the next module

// ----------------------------------------------------------------------
// aotrapetra server-side core module :
// The words locution «aotra petra» means «what has zero limits» in malagasy, that is to say «infinite».
// I found it appropriate, since it is the server-side counterpart of the aotra, with «aotra» meaning «zero» in malagasy.
// It formerly used to stand for «Tsypetra Superior Yield Programming Environment for Total Representation Aotra»
// or «Technologie Supérieure tsYpetra de Programmation pour un Environnement Total de Représentation Aotra» in French.
// This module provides server-side treatments with a strong all taken care of client-server ajax exchanges,
// in the style of modern J2EE frameworks like ZK or GWT.
// (The choice has been made to put this module into aotra.php default core
// to have this functionnality without having to create a file dependency to another library,
// what would be against the aotra philosophy for minimalistic infrastructure to operate.)

// TODO ...

// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// aotrasome server-side core module :
// The words locution «aotra some» can be reduced to «aosome», what means «what is usually inspiring awe» in english.
// This word is really over-used on the Internet, and when designating a product aonly shows that its author is completly lacking humility
// (Of course, it is not the case for the author of this module).
// It stands for «aotra SOME ORM made easy», because recursive acronyms are mandatory to be cool in software industry, at some point (Some things just are the way they are as the song says).
// This module provides server-side ORM mapping infrastructure with a strong emphasis on simplicity and out-of-the-box developper usability.
// in the style of modern J2EE ORM frameworks like Hibernate.
// (The choice has been made to put this module into aotra.php default core
// to have this functionnality without having to create a file dependency to another library,
// what would be against the aotra philosophy for minimalistic infrastructure to operate.)

// Implementation details :
// Persistence :
// Aotrasome is intended to support several types of low-level persistence solutions (as long as they support UTF-8 characters encoding) : single PHP file (default), single CSV file, SQL database, SQLite, etc.
// For this, aotrasome uses a single table with the following definition : (5 columns)
// aotrasome_obj (fields order matter and should not be changed)
// * uid (positive integer) | module (-255 chars string) | type (-255 chars string) | attributes (+255 chars string) | version (positive or negative integer)
// -uid (Primary key) : contains an identifier (globaly unique) among the whole ORM table (this is to allow different modules to interact with each other following certain rules such as exposed objects rules)
// -module : contains the name (globaly unique) of the module where is this represented object
// -type : contains the class name (unique for a given module name) of this represented object
// -attributes :
// °For non-primitive objects : contains a string of «,» separated couples of attribute name (as named in the class) and attribute uid (Foreign key). The separator in the couple is «:»,
// and the «¢» flag at the beginning of the couples means «cascade delete» of this attribute, that is to say is deleted when parent object is deleted. This is marked with the «@Cascade» annotion in the PHP classs.
// example : «¢wheels:123,gpsDevice:234,coffeeMug:235,¢doors:124»
// There are primitive types defined, such as Array (corresponding to a PHP array), Integer, String, etc.
// °For primitive objects (String, Integer, etc.) : contains the value of the object, if necessary, for example if object is an Integer, a String, etc.
// -version: contains the version of the object. If an object is «deactivated», then its last representing row will have its version value turned into negative (this operation is reversible).
// Mysql databases initializaion commands :
// # create database aotra set utf8 ; use aotra; create user 'aotra' identified by 'aotra'; grant all privileges on aotra.* to 'aotra'; update user set password=password('aotrapass') where user='aotra'; flush privileges;
// # use aotra; create table aotrasome_obj (uid int primary key auto_increment, module varchar(255), type varchar(255), attributes text, version int);
// API details:
// Aotrasome provides an hibernate or jquery-like API, for CRUD operations to allow a code that keeps being minimal while understandable :
// examples :
// $SOME=Aotrasome::initPersistanceMysql("localhost:3306","aotra","aotrapass"); // uses an interface for object «Some» that is implemented by «Aotrasome»
// $newObj=$SOME->create("MyPHPClass")->fromExisting($clonedPHPObject);
// $objectsList=$SOME->select("MyPHPClass")->where()->and($SOME->eq("myAttr1",$value1),$SOME->or($SOME->eq("myAttr2",$value2),$SOME->gt("myAttr3",$value3)))->sortBy("attr1","asc")->limit(10,20);
// $object=$SOME->select("MyPHPClass")->where()->and($SOME->eq("myAttr1",$value1),$SOME->eq("myAttr2",$value2))->sortBy("attr1","asc")->first();
// $object=$SOME->select()->where()->uid($foundUid);
// $updatedObject=$SOME->update($updatedObject);
// $SOME->delete($object);

// TODO ...
/*
 * interface SOME{
 * // TODO : public initPersistanceMysql($serverURL,$username,$clearPassword);
 * public initPersistancePHPFile($filePath);
 * }
 *
 * class AOTRASOME implements SOME{
 *
 * // TODO : public initPersistanceMysql($serverURL,$username,$clearPassword){}
 * public initPersistancePHPFile($filePath){
 *
 * }
 * }
 */

// ----------------------------------------------------------------------

// @@@@@@@@@@@@@@@@@@@@@@@ END OF CORE MODULES SECTION @@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// Default functionning :
if(!$isDefaultFunctionningOverrided)
	echo main();

// FUNCTIONS
function main(){
	global $SALT_KEY, $MESSAGES, $FREEZE_SECONDS, $ACCOUNTS_SEPARATOR, $ACCOUNT_DEFAULT_NAME, $PASSWORDS_SEPARATOR, $ADDITIONAL_MESSAGE, $LOG_FILE_NAME;
	
	// Possible redirection :
	$otherServerURL=isset($_REQUEST["otherServerURL"])?$_REQUEST["otherServerURL"]:null;
	if(!empty($otherServerURL)&&$otherServerURL!==getServerURL()){
		return redirectRequestToOtherServer($otherServerURL);
	}
	
	// Case no parameters at all : (simple GET request)
	if(count($_REQUEST)===0){
		return $MESSAGES["0100"].$ADDITIONAL_MESSAGE;
	}
	
	// Required parameters check :
	$actionType=$_REQUEST["actionType"];
	if(empty($actionType)){
		return $MESSAGES["0105"];
	}
	
	// Presence check
	if($actionType==="PING")
		return $MESSAGES["0004"];
	
	// Easter egg #1
	if($actionType==="MANMAKEMEASANDWICH")
		return $MESSAGES["0304"];
	if($actionType==="SUDOMANMAKEMEASANDWICH")
		return $MESSAGES["0003"];
	
	if($actionType==="GENERATEPASSWORDHASH"){
		
		// localhost check :
		$refererURL=$_SERVER["HTTP_HOST"];
		if(trim($refererURL)!=="localhost"&&trim($refererURL)!=="127.0.0.1"){
			return $MESSAGES["0203"];
		}
		
		// We don't use the saltkey installed here on this server, to avoid
		// hackers that would be accesing this file to use this API to forge a password hash :
		// So we have to provide it in the API call parameters :
		$saltKeyParam=$_REQUEST["saltKey"];
		if(empty($saltKeyParam)){
			return $MESSAGES["0106"];
		}
		
		$clearTextPassword=$_REQUEST["clearTextPassword"];
		if(empty($clearTextPassword)){
			return $MESSAGES["0107"];
		}
		
		$passwordHash=getHashedString($clearTextPassword,true);
		
		$passwordHashSalted=getHashedString($saltKeyParam.$passwordHash,true);
		
		return $passwordHashSalted;
	}
	
	if($actionType==="WRITE"||$actionType==="WRITEENCRYPTED"||$actionType==="DECRYPT"||$actionType==="UPLOAD"){
		
		$pageName=$_REQUEST["pageName"];
		if(empty($pageName)){
			return $MESSAGES["0101"];
		}
	}
	
	if($actionType==="WRITE"||$actionType==="WRITEENCRYPTED"){
		$data=$_REQUEST["data"];
		if(empty($data)){
			return $MESSAGES["0102"];
		}
	}
	
	// Write password check :
	$writePasswordHashSalted="";
	$writePasswordHashParam="";
	if($actionType==="WRITE"||$actionType==="WRITEENCRYPTED"||$actionType==="UPLOAD"){
		
		// First we check if the asked page is writePassword-protected on server :
		$wholeWritePasswordHashSaltedStringFromFile=getWholePasswordHashSaltedStringFromFile("WRITE",$pageName);
		
		//DBG
		//error_log(">>>wholeWritePasswordHashSaltedStringFromFile:$wholeWritePasswordHashSaltedStringFromFile");
		
		
		if(!empty($wholeWritePasswordHashSaltedStringFromFile)){
			
			// If it is : then we check against received writePasswordHash :
			$writePasswordHashParam=$_REQUEST["writePasswordHash"];
			
			
			//DBG
			//error_log("]]]]writePasswordHashParam:$writePasswordHashParam");
			
			
			if(empty($writePasswordHashParam)){
				return $MESSAGES["0103"];
			}
			
			// In all cases of write password correctness, if freeze time is not complete, we throw a «freeze time is not completed» error :
			if(abs(time()-getFileDate($pageName,"modify"))<$FREEZE_SECONDS){
				return $MESSAGES["0204"];
			}
			
			$writePasswordHashSalted=getHashedString($SALT_KEY.$writePasswordHashParam,true);
			
			if(!validatePassword($writePasswordHashSalted,$wholeWritePasswordHashSaltedStringFromFile)){
				
				// If write password is incorrect, but freeze time has passed, we touch «modify» the file :
				touchFile($pageName,"modify");
				
				return $MESSAGES["0201"];
			}
		}else{
			// (If not, then no password was ever required, and that's OK...)
			// TRACE
			error_log("INFO : No write password was required for this file to be written on."."\n",3,$LOG_FILE_NAME);
		}
	}
	
	// -----------------------------------------------------------------------------------------------------------------------
	// Actions treatments :
	
	if($actionType==="WRITE"){
		
		//DBG
		//error_log(">>>>wholeWritePasswordHashSaltedStringFromFile:$wholeWritePasswordHashSaltedStringFromFile");
		
		
		// Then (or if page is not writePassword-protected), we write down received data to file :
		return writeDataToFile($pageName,$data,$wholeWritePasswordHashSaltedStringFromFile,
				// TODO : write password clue ?
				"");
	}else if($actionType==="WRITEENCRYPTED"){
		// Then (or if page is not writePassword-protected), we write down received data to file :
		
		$encryptingPasswordHashParam=$_REQUEST["encryptPasswordHash"];
		if(empty($encryptingPasswordHashParam)){
			return $MESSAGES["0108"];
		}
		
		$passwordClue=$_REQUEST["passwordClue"];
		
		$encryptingPasswordHashSalted=getHashedString($SALT_KEY.$encryptingPasswordHashParam,true);
		return writeDataToFile($pageName,$data,$wholeWritePasswordHashSaltedStringFromFile,$passwordClue,$encryptingPasswordHashSalted,true);
	}else if($actionType=="DECRYPT"){
		
		$decryptingPasswordHashParam=$_REQUEST["decryptPasswordHash"];
		if(empty($decryptingPasswordHashParam)){
			return $MESSAGES["0108"];
		}
		
		// In all cases of view password correctness, if freeze time is not complete, we throw a «freeze time is not completed» error :
		if(abs(time()-getFileDate($pageName,"access"))<$FREEZE_SECONDS){
			return $MESSAGES["0205"];
		}
		
		$encryptedAotraScreenContentFromFile=extractEncryptedContentFromFile($pageName);
		
		$seedKey=getHashedString($SALT_KEY.$decryptingPasswordHashParam,true);
		$decryptedAotraScreenContentFromFile=getDecryptedContent($seedKey,$encryptedAotraScreenContentFromFile);
		
		if(strpos($decryptedAotraScreenContentFromFile,"<aotraScreen")===false){
			// If decrypt password is incorrect, but freeze time has passed, we touch «access» the file :
			touchFile($pageName,"access");
			
			return $MESSAGES["0206"];
		}
		
		return $decryptedAotraScreenContentFromFile;
	}else if($actionType==="UPLOAD"){
		
		for($i=0;$i<count($_FILES["uploadedFiles"]["name"]);$i++){
			$fileName=$_FILES["uploadedFiles"]["name"][$i];
			$fileTmpName=$_FILES["uploadedFiles"]["tmp_name"][$i];
			$fileType=$_FILES["uploadedFiles"]["type"][$i];
			
			if(!receiveUploadedFile($pageName,$fileName,$fileTmpName,$fileType)){
				return $MESSAGES["0303"];
			}
		}
		return $MESSAGES["0002"];
	}
	
	return $MESSAGES["0104"];
}

// ==================================== UTILITY METHODS ====================================

// ------------ Passwords protection ------------
function validatePassword($passwordHashSalted,$wholePasswordHashSaltedStringFromFile){
	global $ACCOUNTS_SEPARATOR, $PASSWORDS_SEPARATOR;
	
	$passwordNamesAndHashes=explode($ACCOUNTS_SEPARATOR,trim($wholePasswordHashSaltedStringFromFile));
	
	if(count($passwordNamesAndHashes)<2){
		if($passwordHashSalted===$wholePasswordHashSaltedStringFromFile){
			return true;
		}
	}
	
	foreach($passwordNamesAndHashes as $passwordNameAndHash){
		if(empty($passwordNamesAndHashes))
			continue;
		$s=explode($PASSWORDS_SEPARATOR,trim($passwordNameAndHash));
		if(count($s)<2)
			continue;
		if(empty($s[1]))
			continue;
		
		if(trim($s[1])===$passwordHashSalted)
			return true;
	}
	return false;
}
function getWholePasswordHashSaltedStringFromFile($passwordType,$fileHTMLName){
	global $MESSAGES;
	
	// Format is : <!--(write/view)PasswordsHashesSalted:user1=hashhashhash1,user2=hashhashhash2,etc...-->\n
	
	$wholePasswordHashSaltedStringFromFile="";
	if($f=fopen($fileHTMLName,"rb") or die($MESSAGES["0301"])){
		
		fgets($f);
		
		// View password hash is on third line :
		if($passwordType==="CLUE")
			fgets($f);
		
		// Write password hash is on second line :
		$line=trim(fgets($f));
		
		$s=explode(":",str_replace("<!--","",str_replace("-->","",$line)));
		if(!empty($line)&&count($s)>0&&($s[0]==="writePasswordsHashesSalted"||$s[0]==="passwordClue")&&!empty($s[1])){
			$wholePasswordHashSaltedStringFromFile=$s[1];
		}
		
		fclose($f);
	}
	
	return trim($wholePasswordHashSaltedStringFromFile);
}
function readOrWriteSaltKeyLineFromFile($fileName,$generateIfNotFound=false){
	global $MESSAGES, $SALTKEY_LENGTH;
	$line="";
	
	if($generateIfNotFound){
		if(!file_exists($fileName)){
			
			if($f=fopen($fileName,"w") or die($MESSAGES["0301"])){
				
				for($i=0;$i<$SALTKEY_LENGTH;$i++){
					$line.=chr(rand(0,255));
				}
				
				// We have to sanitize URL offending characters in the saltkey string, because to generate password hash, we have to pass it through a GET parameter :
				$line=removeURLHostileChars(base64_encode(mb_convert_encoding($line,"UTF-8")));
				
				fwrite($f,$line);
				fclose($f);
				
				return $line;
			}
		}
	}
	
	if($f=fopen($fileName,"r") or die($MESSAGES["0301"])){
		$line=fgets($f);
		fclose($f);
		// We have to sanitize URL offending characters in the saltkey string, because to generate password hash, we have to pass it through a GET parameter :
		$line=removeURLHostileChars(mb_convert_encoding($line,"UTF-8"));
		return $line;
	}
	
	die($MESSAGES["0301"]);
}


// ------------ Password view protection methods ------------

// - Encrypt:
// We get the <aotraScreen> (tags included) tag content :
function extractClearContentFromDataStringForEncrypting($content){
	global $MESSAGES, $SALTKEY_LENGTH, $saltKey;
	
	$result=getDelimitedString($content,"<aotraScreen","</aotraScreen>");
	if($result===NULL)
		return $MESSAGES["0109"];
	
	$result="<aotraScreen".$result."</aotraScreen>";
	
	// We add a random salted time stamp, to prevent clear-text attacking the first (sensitive) chunk of the encrypting key :
	$obfuscatingStamp="";
	for($i=0;$i<$SALTKEY_LENGTH;$i++){
		$obfuscatingStamp.=chr(rand(0,255));
	}
	
	$stampPart="<!--".getHashedString($saltKey.$obfuscatingStamp.time())."-->";
	
	$result=$stampPart.$result;
	
	return $result;
}
function getEncryptedContent($saltedKeyHash,$decryptedAotraScreenContentFromFile){
	$result=encodeXORNoPattern($saltedKeyHash,$decryptedAotraScreenContentFromFile);
	// encrypt :
	$result=base64_encode($result); // normal text string to base 64 string, to «prettify» the output
	return $result;
}

// - Decrypt:
function extractEncryptedContentFromFile($fileHTMLName){
	global $MESSAGES;
	
	$content=file_get_contents($fileHTMLName);
	if($content===false)
		return $MESSAGES["0301"];
	
	$result=getDelimitedString($content,"<encryptedAotraScreenContent>","</encryptedAotraScreenContent>");
	if($result===NULL)
		return $MESSAGES["0109"];
	
	return $result;
}
function getDecryptedContent($saltedKeyHash,$encryptedAotraScreenContentFromFile){
	//decrypt :
	$encryptedAotraScreenContentFromFile=base64_decode($encryptedAotraScreenContentFromFile); // base 64 string to normal text string
	$result=encodeXORNoPattern($saltedKeyHash,$encryptedAotraScreenContentFromFile);
	
	// We remove the starting stamp :
	$result=preg_replace("/.*<aotraScreen/","<aotraScreen",$result);

	// Just to be sure that we return the string in the UTF-8 encoding :
	$result=mb_convert_encoding($result,"UTF-8");
	
	return $result;
}

// ------------ File writing methods ------------
function writeDataToFile($fileHTMLName,$originalData,$writePasswordHashSalted
		,/*OPTIONAL*/$passwordClue=""
		,/*OPTIONAL*/$encryptingPasswordHashSalted=""
		,/*OPTIONAL*/$encryptData=false){
	global $SALT_KEY, $MESSAGES, $LOG_FILE_NAME;
	
	if($f=fopen($fileHTMLName,"w") or die($MESSAGES["0301"])){
		
		$data="";
		
		$data.="<!DOCTYPE html>\n";
		
		// Passwords writing to file :
		// Write password hash is on second line :
		$data.="<!--writePasswordsHashesSalted:";
		if(!empty($writePasswordHashSalted)){
			$data.=$writePasswordHashSalted;
		}
		$data.="-->\n";
		
		// View password hash is on third line :
		$data.="<!--passwordClue:";
		if(!empty($passwordClue)){
			$data.=$passwordClue;
		}
		$data.="-->\n";
		
		// Data writing to file :
		
		if($encryptData===true){
			$clearContent=extractClearContentFromDataStringForEncrypting($originalData);
			
			$newEncryptedContent="<encryptedAotraScreenContent>".getEncryptedContent($encryptingPasswordHashSalted,$clearContent)."</encryptedAotraScreenContent>";
			
			$s="<script id=\"xmlModel\"";
			$index=strpos($originalData,$s);
			
			$originalData=substr($originalData,0,$index)."<script id=\"xmlModel\" type=\"application/xml\" data-keep=\"true\">".$newEncryptedContent."</script></body></html>";
		}
		
		$data.=$originalData;
		
		fwrite($f,$data);
		fclose($f);
		
		return $MESSAGES["0001"];
	}
	
	return $MESSAGES["0302"];
}

// ------------ Files uploading methods ------------
function receiveUploadedFile($pageName,$fileName,$fileTmpName,$fileType){
	global $IMAGES_TYPE_DIRECTORY, $VIDEOS_TYPE_DIRECTORY, $AUDIOS_TYPE_DIRECTORY, $OTHERS_TYPE_DIRECTORY;
	
	$typeDir="";
	if(strpos($fileType,"image")!==false){
		$typeDir=$IMAGES_TYPE_DIRECTORY."/";
	}else if(strpos($fileType,"video")!==false){
		$typeDir=$VIDEOS_TYPE_DIRECTORY."/";
	}else if(strpos($fileType,"audio")!==false){
		$typeDir=$AUDIOS_TYPE_DIRECTORY."/";
	}else{
		$typeDir=$OTHERS_TYPE_DIRECTORY."/";
	}
	
	// If dir does not exists, we create it :
	$dirPath=str_replace(".","_",$pageName)."/".$typeDir;
	if(!file_exists($dirPath)){
		if(!mkdir($dirPath,0700,true)||!file_exists($dirPath))
			return false;
	}
	
	move_uploaded_file($fileTmpName,$dirPath."/".$fileName);
	// DOCUMENTATION (PLEASE KEEP CODE) :
	// $fileName = $_FILES["uploadedFiles"]["name"][0];
	// $fileHandle = fopen($fileName, 'w') or die("SYSTEM CANNOT OPEN FILE.");
	// fclose($fileHandle);
	
	return true;
}

// ------------ Request redirection (proxying) methods ------------
function redirectRequestToOtherServer($destinationURL){
	$url=$destinationURL;
	$data=array(
			"actionType"=>$_REQUEST["actionType"],
			"pageName"=>$_REQUEST["pageName"],
			"writePasswordHash"=>$_REQUEST["writePasswordHash"],
			"viewPasswordHash"=>$_REQUEST["viewPasswordHash"],
			"data"=>$_REQUEST["data"],
					/*AND NOT otherServerURL !*/
				"saltKey"=>$_REQUEST["saltKey"],
			"clearTextPassword"=>$_REQUEST["clearTextPassword"]
	);
	
	// TODO : FIXME : For now, it's just too bad for https... :-/
	// use key 'http' even if you send the request to https://...
	$options=array(
			'http'=>array(
					'header'=>"Content-type: application/x-www-form-urlencoded\r\n",
					'method'=>'POST',
					'content'=>http_build_query($data)
			)
	);
	
	$context=stream_context_create($options);
	$result=file_get_contents($url,false,$context);
	
	return $result;
}
function getServerURL(){
	$serverURL="http";
	if($_SERVER["HTTPS"]=="on")
		$serverURL.="s";
	$serverURL.="://";
	if($_SERVER["SERVER_PORT"]!="80")
		$serverURL.=$_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	else
		$serverURL.=$_SERVER["SERVER_NAME"];
	return $serverURL;
}

// ------------ File methods ------------
function touchFile($fileName,$mode="access"){
	if($mode==="modify"){
		// DOESN'T WORK :
		// fclose(fopen($fileName, 'a')); // File will be created empty if it doesn't exist.
		touch($fileName,time(),getFileDate($fileName,"access")); // File will be created empty if it doesn't exist.
	}else{
		// DOESN'T WORK :
		// fclose(fopen($fileName, 'r'));
		touch($fileName,getFileDate($fileName,"modify"),time());
	}
}
function getFileDate($fileName,$mode="access"){
	if($mode==="modify"){
		return filemtime($fileName);
	}
	return fileatime($fileName);
}

// ------------ Cryptography methods ------------

// XOR implementation : (sucks)
// AES MCRYPT implementation : (does not work on decrypting)

// XOR implementation : (sucks a little less)
// We hash the encrypting / decrypting key several times on the previoous blocks and the salt key to not repeat it :
function encodeXORNoPattern($saltedKeyHash,$strToEncode){
	global $saltKey;
	
	$result="";
	$keyChunk=$saltedKeyHash;
	// Converts string to array :
	$splits=str_split($keyChunk);
	$i=0;
	$keyLength=strlen($keyChunk);
	$previousChars="";
	foreach(str_split($strToEncode) as $char){
		
		$keyByte=$splits[$i];
		// OTHER WAY (PLEASE KEEP CODE) : $result .= chr(ord($char) ^ ord($saltedKeyHash{$i++ % strlen($saltedKeyHash)}));
		$result.=chr(ord($char)^ord($keyByte));
		
		if($i%$keyLength===($keyLength-1)){
			$i=0;
			// NO : TOO LONG TREATMENT !: We hash with all the previous blocks too :
			// We hash with the private salt key and a character of the previous chunk (they add up at each new chunk):
			$keyChunk=getHashedString($previousChars.$saltKey.$keyChunk);
			$previousChars.=substr($keyChunk,-1);
			$splits=str_split($keyChunk);
		}else{
			$i++;
		}
	}
	return $result;
}

// ------------ Hashing methods ------------
function getHashedString($str,$makeTreatmentHeavy=false){
	global $HEAVY_TREATMENT_LOOP_COUNT;
	// if (CRYPT_SHA256 == 1){
	// TRACE
	// error_log("WARN : Required hash algorithm not found, using a default one. That sucks.", 3, LOG_FILE_NAME);
	// SHA-256 (sucks less) :
	// return crypt(CRYPT_SHA_256);
	// }
	// // MD5 (sucks) :
	// return md5($str);
	
	if(!$makeTreatmentHeavy){
		$result=hash("sha256",$str);
	}else{
		// If needed, we make sure the hashing will take a long time, so that
		// if the private salt key is leaked, it will be fairly difficult to crack
		// password write protection and content encryption :
		
		$result=$str;
		for($i=0;$i<$HEAVY_TREATMENT_LOOP_COUNT;$i++){
			$result=hash("sha256",$result);
		}
	}
	
	return $result;
}

// ------------ Miscellaneous methods ------------
function getDelimitedString($content,$startStr,$endStr){
	$i1=strpos($content,$startStr)+strlen($startStr);
	$i2=strpos($content,$endStr);
	if($i1===false||$i2===false)
		return NULL;
	$result=substr($content,$i1,($i2-$i1));
	return $result;
}
function removeURLHostileChars($str){
	return str_replace(array(
			" ",
			"=",
			"&",
			"%",
			"?",
			"/",
			"+"
	),"_",$str);
}
?>
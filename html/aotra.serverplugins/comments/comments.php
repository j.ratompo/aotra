<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body>
<?php

$SERVERPLUGINS_DIR="aotra.serverplugins/";

// *******************************
$PLUGIN="commentsPlugin.php"; // <= CHANGE THIS VALUE FOR THE FILE NAME OF YOUR PLUGIN
// *******************************

/* This is a server-side applet example, it will be called in a bubble's content iframe, this way :
	<iframe src="/aotra.serverplugins/commentsPlugin.php?parentBubbleName=YYYYYYYYY" frameBorder="0" seamless="" width="110%" height="70%"></iframe>
 */


// REQUIRED PARAMETERS PASSED TO IFRAME :
$parentBubbleName=$_REQUEST['parentBubbleName'];
if(empty($parentBubbleName)){
	echo "ERROR : Parameter «parentBubbleName» missing.";
	return;
}

$ACTION="/".$SERVERPLUGINS_DIR.$PLUGIN."?parentBubbleName=".$parentBubbleName;

// ============================= INITIALIZATION =============================
/*
--SQL :
CREATE DATABASE commentsDB;
USE commentsDB;

CREATE USER commentsUser;
SET PASSWORD FOR commentsUser = PASSWORD('commentsPassword');
GRANT ALL PRIVILEGES ON commentsDB.* TO commentsUser;

CREATE USER commentsUser@localhost;
SET PASSWORD FOR 'commentsUser'@'localhost' = PASSWORD('commentsPassword');
GRANT ALL PRIVILEGES ON commentsDB.* TO 'commentsUser'@'localhost';#IDENTIFIED BY 'commentsUser'


FLUSH PRIVILEGES;
*/

$db = mysqli_connect("localhost","commentsUser","commentsPassword","commentsDB") or die("ERROR CONNECTING TO DATABASE.");



// ============================= VIEWS ============================= 
 
if(empty($_REQUEST['view']) || $_REQUEST['view']==="defaultView"){
	// Default View :
	
	$comments=getAllComments();
	foreach($comments as $comment){
		?>
		<h2><?=$comment->author?> said :</h2>
		<p><?=$comment->text?></p><hr/>
		<?php
	}
	
	?>
	<br/><br/><a href='<?=$ACTION?>&view=addComment'>Add comment...</a>
	<?php
	
	
}else if($_REQUEST['view']==="addComment"){

	
	// First, we display a simple field :
	?>
	<form action='<?=$ACTION?>'>
	
	<input type='text' name='author'/><br/><br/>
	<textarea cols=20 rows=5 name='text'></textarea><br/><br/>
	
	<input type='submit' value='Publish your comment' 	onclick="javascript:document.getElementById('view').value='publish'" /><br/>
	<input type='submit' value='Cancel' 					onclick="javascript:document.getElementById('view').value='defaultView'" /><br/>
	
	<!-- System, mandatory input : -->
	<input type='hidden' name='view' id='view'/>
	<input type='hidden' name='parentBubbleName' value='<?=$parentBubbleName?>'/>
	
	</form>
	<?php
	

}else if($_REQUEST['view']==="publish"){

	$author=$_REQUEST['author'];
	$text=$_REQUEST['text'];

	if(empty($author) || empty($text)){
		?><h2>Please provide an author name and a text for your comment<h2><br/>
		<br/><br/><a href='<?=$ACTION?>&view=addComment'>Back</a>
		<?php

		return;
	}
	
	
	try{
	
		// To avoid commands injections attacks :
		$text=escapeshellcmd($text);
		
		$comment=createNewComment($author,$text);

	}catch(Exception $e){
		?><h2>An error occured and your comment could not be added.<h2><br/>
		<br/><br/><a href='<?=$ACTION?>&view=defaultView'>Continue</a>
		<?php
		return;
	}
	
	
	?><h2>Your comment has been successfully added, «<?=$author?>»<h2><br/>
	<a href='<?=$ACTION?>&view=defaultView'>Continue</a>
	<?php


}else{
	echo "ERROR : View named «".$_REQUEST['view']."» not found.";
}


// ============================= TREATMENTS (CONTROLLER) ============================= 

function getAllComments(){
	
	global $db;
	
	$results=array();

	$query = "SELECT author,text FROM comments" or die("DATABASE ERROR :".mysqli_error($db));
	$res = $db->query($query);
	
	while($row = mysqli_fetch_array($res)) {
	  $c=new CommentDAO();
	  $results[]=$c->extract($row);
	} 

	return $results;
}

function createNewComment($author,$text){
	global $db;
	
	$newComment=new Comment($author,$text);
	$c=new CommentDAO($newComment);
	$c->insert();

	return $newComment;
}




// ============================= MODEL ============================= 
// Transient :
class Comment{

	public $author="";
	public $text="";

	public function __construct($author,$text){
		$this->author=$author;
		$this->text=$text;
	}
	
}

// Persistent :(a very basic persistence implementation, it uses Mysqli :)
/*
--SQL :
USE commentsDB; CREATE TABLE comments (author VARCHAR(256),text TEXT(65535));
*/
class CommentDAO{
	public $comment;

	public function __construct($comment=NULL){
		$this->comment=$comment;
	}
	
	
	public function insert(){
		global $db;

		// SAFE :
		$query = $db->prepare("INSERT INTO comments VALUES (?,?)");
		$query->bind_param("ss",$this->comment->author,$this->comment->text);
		$query->execute();

		// UNSAFE :	
		//$query = "INSERT INTO comments VALUES ('".$this->comment->author."','".$this->comment->text."')" or die("DATABASE ERROR :".mysqli_error($db));
		//$db->query($query);
		
	}
	
	public function extract($row){
		$this->comment=new Comment($row["author"],$row["text"]);
		return $this->comment;
	}
	

}



?></body></html>
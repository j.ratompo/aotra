<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body>
<?php


$SERVERPLUGINS_DIR="aotra.serverplugins/";

// *******************************
$PLUGIN="examplePlugin.php"; // <= CHANGE THIS VALUE FOR THE FILE NAME OF YOUR PLUGIN
// *******************************


/* This is a server-side aotra plugin example. aotra plugins are facultative, and can be
	use within a bubble via this way :

	This is a server-side applet example, it will be called in a bubble's content iframe, this way :
	<iframe src="/aotra.serverplugins/examplePlugin.php" frameBorder="0" seamless="" width="110%" height="70%"></iframe>
 */

$ACTION="/".$SERVERPLUGINS_DIR.$PLUGIN;


// ============================= VIEWS ============================= 
 
if(empty($_REQUEST['view']) || $_REQUEST['view']==="defaultView"){
	// Default View :
	// First, we display a simple field :
	?>
	<form action='<?=$ACTION?>'>
	<input type='text' name='inputValue'/><br/><br/>
	<input type='submit' name='doTreatment1' value='Do treatment 1 (a simple addition)' 				onclick="javascript:document.getElementById('view').value='viewTreatment1'" /><br/>
	<input type='submit' name='doTreatment2' value='Do treatment 2 (display value in a new bubble)' onclick="javascript:document.getElementById('view').value='viewTreatment2'" /><br/>
	
	<!-- System, mandatory input : -->	
	<input type='hidden' name='view' id='view'/>
	
	</form>
	<?php
	
}else if($_REQUEST['view']==="viewTreatment1"){
	// Result view for treatment 1 :
	// Displays the result :
	?>
	Hello, I'm a string displaying a server side treatment : <strong><?=getServerSideTreatmentResult()?></strong>
	<br/><br/><a href='<?=$ACTION?>?view=defaultView'>Back...</a>
	<?php

}else if($_REQUEST['view']==="viewTreatment2"){
	// Result view for treatment 2 :
	// Displays the result in a bubble then go to it :

	?>
	Content has been put in a new bubble ! 
	<br/><br/><a href='<?=$ACTION?>?view=defaultView'>Back...</a>
	<?php
	
	$content="Hello, I'm a string displaying a server side treatment, in a bubble : <strong>".getServerSideTreatmentResult()."</strong>";
	?>
	<script type='text/javascript'>
	(function(){
	
	// REQUIRED PARENT MEMBERS ACCESS :
	var Bubble=parent.Bubble;var getAotraScreen=parent.getAotraScreen;var config=parent.config;

	var aotraScreen=getAotraScreen();
	var b=aotraScreen.getBubbleByName('newBubble');
	var plane=aotraScreen.getActivePlane();
	if(!b){
				b=new Bubble(
						'newBubble',80,80,200
						,'squared'
						,''
						,config.bubble.fontSize,config.bubble.isOpenableClosable,config.bubble.color
						,config.bubble.openCloseAnimation,config.bubble.borderStyle,config.bubble.openedOnCreate
						,config.bubble.fixedStrategy,false,false,null
						,null
						,""
						,""
						,""
						,aotraScreenLocal
				);
				plane.addBubble(b);
	}
	b.setContent("<?=$content?>");
	
	aotraScreen.doGotoBubble(b);
	
	})();
	</script>
	<?php


}

// ============================= TREATMENTS (CONTROLLER) ============================= 

function getServerSideTreatmentResult(){

	$inputVal=$_REQUEST['inputValue'];
	if(empty($inputVal) || !is_numeric($inputVal))	return "ERROR : Value «".$inputVal."» is not a number.";
	
	$result=intVal($inputVal);
	$result+=5;
	return $result;
}

// ============================= MODEL ============================= 

// None...

?></body></html>
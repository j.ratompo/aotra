<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body>
<?php

// *******************************
$PLUGIN="marketPlugin.php"; // <= CHANGE THIS VALUE FOR THE FILE NAME OF YOUR PLUGIN
// *******************************

$SERVERPLUGINS_DIR="aotra.serverplugins/";

/* This is a server-side applet example, it will be called in a bubble's content iframe, this way :
	<iframe src="/aotra.serverplugins/commentsPlugin.php?parentBubbleName=YYYYYYYYY" frameBorder="0" seamless="" width="110%" height="70%"></iframe>
 */


// REQUIRED PARAMETERS PASSED TO IFRAME :
$parentBubbleName=$_REQUEST['parentBubbleName'];
if(empty($parentBubbleName)){
	echo "ERROR : Parameter «parentBubbleName» missing.";
	return;
}

$ACTION="/".$SERVERPLUGINS_DIR.$PLUGIN."?parentBubbleName=".$parentBubbleName;



// ============================= INITIALIZATION =============================
/*
##--SQL :

CREATE DATABASE marketDB;	USE marketDB;

CREATE USER marketUser;
SET PASSWORD FOR marketUser = PASSWORD('marketPassword');
GRANT ALL PRIVILEGES ON marketDB.* TO marketUser;

CREATE USER marketUser@localhost;
SET PASSWORD FOR 'marketUser'@'localhost' = PASSWORD('marketPassword');
GRANT ALL PRIVILEGES ON marketDB.* TO 'marketUser'@'localhost';#IDENTIFIED BY 'marketUser';

FLUSH PRIVILEGES;

*/


$db = mysqli_connect("localhost","marketUser","marketPassword","marketDB") or die("ERROR CONNECTING TO DATABASE.");



// ============================= VIEWS ============================= 
 
if(empty($_REQUEST['view']) || $_REQUEST['view']==="defaultView"){

	// Default View :
	
	?>
	
	<form action='<?=$ACTION?>'>
		<input type='submit' value='View cart...'/>
		<input type='hidden' name='sessionId' id='inputSessionId'/>
		<input type='hidden' name='email' id='inputEmail'/>

		<input type='hidden' name='view' value='viewCart'/>
		<input type='hidden' name='parentBubbleName' value='<?=$parentBubbleName?>'/>		
	</form>
	
	
	<form action='<?=$ACTION?>'>
		<input type='submit' value='View all products...'/>

		<input type='hidden' name='view' value='viewAllProducts'/>		
		<input type='hidden' name='parentBubbleName' value='<?=$parentBubbleName?>'/>		
	</form>
	
	<script src="../aotra.min.js" type="text/javascript"></script>
	
	<script>

		function checkLocalStorage(){
		
			var sessionId=getStringFromStorage("marketCartSessionId",true);
			var email=getStringFromStorage("marketCartEmail",true);
			
			if(empty(sessionId) || empty(email) || email==="null"){
				sessionId=storeString("marketCartSessionId",getUniqueIdWithDate(),true);
				var promptedEmail=prompt("Please enter your email","");
				email=storeString("marketCartEmail",promptedEmail,true);
			}
			
			//TEST
			sessionId="111";
			email="gru@yopmail.com";
			
			document.getElementById("inputSessionId").value=sessionId;
			document.getElementById("inputEmail").value=email;
		}
		checkLocalStorage();
	</script>




	<?php


}else if($_REQUEST['view']==="viewCart"){
	
	$cart=retrieveCart();
	if(empty($cart)){
		echo "ERROR : No cart could be retrieved.";
		return;
	}

	?>
	<div>
		<h1><?=$cart->sessionId?></h1>;
		<h2><?=$cart->email?><h2>;
		<br/>
	
		<ul>
			<?php
			foreach($cart->commands as $command){
			?>
				<li>
					<h3><?=$command->product->name?></h3>
					(X <?=$command->quantity?>
					<strong><?=$command->product->price?></strong>
					<i><?=$command->getTotalPrice()?></i><br/>
					<small><?=$command->product->description?></small>				
				</li>
			<?php
			}
			?>
		</ul>	
	</div>
	<?php
	
	
}else if($_REQUEST['view']==="viewAllProducts"){


// TODO ...


}else if($_REQUEST['view']==="viewProduct"){

	$productId=$_REQUEST["productId"];
	if(empty($productId)){
		echo "ERROR : No product id received.";
		return;
	}
	
	$productDAO=new ProductDAO();

	$product=$productDAO->getProduct($productId);
	if(empty($product)){
		echo "ERROR : No product could with id «".$productId."» could be found.";
		return ;
	}

	?>
		<div>
			<h1><?=$product->name?></h1>
			<h2><?=$product->price?></h2><br/>
			<small><?=$product->description?></small>
		</div>
	<?php

}else{
	echo "ERROR : View named «".$_REQUEST['view']."» not found.";
}


// ============================= TREATMENTS (CONTROLLER) ============================= 

function retrieveCart(){
	$sessionId=$_REQUEST["sessionId"];
	if(empty($sessionId)){
		echo "ERROR : No session id received.";
		return;
	}
	
	$email=$_REQUEST["email"];

	$cartDAO=new CartDAO();
	$cart=$cartDAO->getCart($sessionId,$email);
	if(empty($cart)){
		echo "ERROR : Unable to create or retrieve cart.";
		return;
	}
	
	return $cart;
}




// ============================= MODEL ============================= 

/*
##--SQL :

USE marketDB;


CREATE TABLE carts (id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, sessionId VARCHAR(256) , email TEXT(65535));
#//TEST
INSERT INTO carts VALUES (1,"111", "test@somemail.com");


CREATE TABLE products (id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(256), description TEXT(65535), price FLOAT(11));
#//TEST
INSERT INTO products VALUES (1,"test product 1","blah blah blah",22.56);INSERT INTO products VALUES (2,"test product 2","blih blih blih",105.23);INSERT INTO products VALUES (3,"test product 3","bloh bloh bloh",5.41);


CREATE TABLE commands (id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, quantity INT(11) DEFAULT 0, cartId INT(11), productId INT(11));
ALTER TABLE commands ADD CONSTRAINT FOREIGN KEY (cartId) REFERENCES carts(id);
ALTER TABLE commands ADD CONSTRAINT FOREIGN KEY (productId) REFERENCES products(id);

#//TEST
INSERT INTO commands VALUES (1,2,1,1); INSERT INTO commands VALUES (2,5,1,2);




*/


// Transient :
class Cart{
	public $id=0;
	public $sessionId="";
	public $email="";
	
	public $commands=NULL;
	
	public function __construct($id,$sessionId,/*OPTIONAL*/$email=""){
		$this->id=$id;
		$this->sessionId=$sessionId;
		$this->email=$email;
		
		$this->commands=array();
	}
}



class Product{
	public $id=0;
	public $name="";
	public $description="";
	public $price=-1;

	public function __construct($id,$name,$description,$price){
		$this->id=$id;
		$this->name=$name;
		$this->description=$description;
		$this->price=$price;
	}
}

class Command{
	public $id=0;
	public $quantity=0;
	
	public $product=NULL;

	public function __construct($id,$quantity,$product){
		$this->id=$id;
		$this->quantity=$quantity;
		
		$this->product=$product;
	}
	
	public function getTotalPrice(){
		return $product->price * $this->quantity;
	}
}

// Persistent :(a very basic persistence implementation, it uses plain Mysqli :)
// It is better to use an ORM framework, such as PDO.

class CartDAO{
	
	public function __construct(){
	}
	
	public function insert($cart){
		global $db;

		$query = $db->prepare("INSERT INTO carts VALUES (DEFAULT,?,?)") or die("DATABASE ERROR :".mysqli_error($db));
		$query->bind_param("ss",$cart->sessionId,$cart->email);
		$query->execute();

		$res = $this->getCartRes($cart->sessionId);
		$row = mysqli_fetch_array($res);
		$cart=$this->extract($row);



		return $cart;
	}
	
	public function extract($row){
		if(empty($row))	return NULL;
	
		$cart=new Cart($row["id"],$row["sessionId"],$row["email"]);
		
		$commandDAO=new CommandDAO($cart);
		$cart->commands=$commandDAO->getCommandsForCart($cart);
		
		return $cart;
	}
	
	public function getCartRes($sessionId){
		global $db;
	
	
		$query = $db->prepare("SELECT id,sessionId,email FROM carts WHERE sessionId=? ") or die("DATABASE ERROR :".mysqli_error($db));
		$query->bind_param("s",$sessionId);
		$res = $db->query($query);
		
	
		return $res;
	}
	
	public function getCart($sessionId,/*OPTIONAL*/$email=NULL){
		global $db;
		
		// If a cart for this session doesn't exists, this view asks for an email for this session, and then, create a cart only usable for this session with it.
		// And if a cart exists for this session, then simply loads it. 
		
		//$cartDAO=new CartDAO();
		
		$res=$this->getCartRes($sessionId);
		$row = mysqli_fetch_array($res);
		
		
		
		$cart=NULL;
		if(!empty($row)){
			$cart=$this->extract($row);
		}else{
			$cart=$this->insert(new Cart(0,$sessionId,$email));
		}
	
		return $cart;
	}
}



class ProductDAO{
	
	public function __construct(){
	}
	
	public function insert($product){
		global $db;

		$query = $db->prepare("INSERT INTO products VALUES (DEFAULT,?,?,?)");
		$query->bind_param("ssd",$product->name,$product->description,$product->price);
		$query->execute();
		
		$res=$this->getProductRes($product->name,$product->description,$product->price);
	
		$row = mysqli_fetch_array($res);
		$product=$this->extract($row);
		
		return $product;
	}
	
	public function extract($row){
		if(empty($row))	return NULL;
	
		//$product=new Product($row["id"],$row["name"],$row["description"],floatval($row["price"]));
		$product=new Product($row["id"],$row["name"],$row["description"],$row["price"]);
		
		return $product;
	}

	public function getProductForCommand($command){
		global $db;
	
		$query = "SELECT p.id,p.name,p.description,p.price FROM products AS p JOIN commands AS c ON p.id=c.productId WHERE c.id=? " or die("DATABASE ERROR :".mysqli_error($db));
		$query->bind_param("i",$command->id);
		$res = $db->query($query);
		
		while($row = mysqli_fetch_array($res)) {
		  $results[]=$this->extract($row);
		} 
		
		return $results;
	}

	public function getProductRes($productName,$productDescription,$productPrice,/*OPTIONAL*/$productId=NULL){
		global $db;
		
		if(empty($productId)){
			$query = "SELECT id,name,description,price FROM products WHERE name=? AND description=? AND price=? " or die("DATABASE ERROR :".mysqli_error($db));
			$query->bind_param("ssd",$productName,$productDescription,$productPrice);
		}else{
			$query = "SELECT id,name,description,price FROM products WHERE id=? " or die("DATABASE ERROR :".mysqli_error($db));
			$query->bind_param("i",$productId);
		}
		
		$res = $db->query($query);
		return $res;
	}

	public function getProduct($productId){
		$res=getProductRes(NULL,NULL,NULL,$productId);
		$row=mysqli_fetch_array($res);
		return $this->extract($row);
	}
}



class CommandDAO{
	public $cart=NULL;
	
	public function __construct($cart=NULL){
		$this->cart=$cart;
	}
	
	public function insert($command){
		global $db;

		$query = $db->prepare("INSERT INTO commands VALUES (DEFAULT,?,?,?)");
		$query->bind_param("iii",$command->quantity,$this->cart->id,$command->product->id);
		$query->execute();
		
		$res=$this->getCommandRes($command->cartId,$command->productId);
		$row = mysqli_fetch_array($res);
		$command=$this->extract($row);
		
		return $command;
	}
	
	public function extract($row){
		if(empty($row))	return NULL;
	
		$command=new Command($row["id"],intval($row["quantity"]));
		
		$productDAO=new ProductDAO();
		$command->product=$productDAO->getProductForCommand($command);
		
		return $command;
	}

	public function getCommandsForCart($cart){
		$this->getCommandRes($cart->id);
		
		while($row = mysqli_fetch_array($res)) {
		  $results[]=$this->extract($row);
		} 
		return $results;
	}
	
	public function getCommandRes($cartId,/*OPTIONAL*/$productId=NULL){
		global $db;

		if(!empty($productId)){
			$query = "SELECT id,quantity,cartId,productId FROM commands WHERE cartId=? AND productId=? " or die("DATABASE ERROR :".mysqli_error($db));
			$query->bind_param("ii",$cartId,$productId);
		}else{
			$query = "SELECT id,quantity,cartId,productId FROM commands WHERE cartId=? " or die("DATABASE ERROR :".mysqli_error($db));
			$query->bind_param("i",$cartId);
		}
		
		$res = $db->query($query);
		return $res;	
	}

}

?></body></html>
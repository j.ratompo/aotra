cd build/

sh compile.sh

cd ..

echo "refreshing files to localhost (for local testing)..."

SRC_DIR="html"

########################################################################################

DEPLOY_DIR="/var/www/html/"

rsync -r --exclude '*.git*' --exclude '*_OLD*' $SRC_DIR/ $DEPLOY_DIR
rsync -r --exclude '*.git*' --exclude '*_OLD*' $SRC_DIR/*.html $DEPLOY_DIR 
#rsync -r   $SRC_DIR/* $DEPLOY_DIR
#rsync -r   $SRC_DIR/*.php $DEPLOY_DIR 
#rsync -r   $SRC_DIR/*.j*g $DEPLOY_DIR 
#rsync -r   $SRC_DIR/*.png $DEPLOY_DIR
#rsync -r   $SRC_DIR/*.gif $DEPLOY_DIR
#rsync -r   $SRC_DIR/*_html/ $DEPLOY_DIR

#rm -rf $DEPLOY_DIR/aotra*
rsync -r --exclude '*.git*' --exclude '*_OLD*' $SRC_DIR/aotra.serverplugins/ $DEPLOY_DIR/aotra.serverplugins/

cp -f    build/aotra*.js $DEPLOY_DIR
#rsync   	build/aotra.js $DEPLOY_DIR

chown www-data:www-data -R $DEPLOY_DIR ; chmod ugo+rwx -R /var/www;

#chmod 777 -R /var/www;

########################################################################################
echo "Done."
exit 0

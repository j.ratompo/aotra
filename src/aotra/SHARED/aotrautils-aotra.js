/* ## Utility global methods, specific for aotra
 * 
 * !!!!!!!!!!!! DEPENDENCIES !!!!!!!!!!!! 
 * 		- jQuery
 * 		- jQuery.caret
 * 		- jQuery.cookie
 * 		- jQuery.scrollTo
 * 		- jQuery.textEditor
 * 		- md5
 * 		- detectmobilebrowser
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * 
 *
 *
 * This set of methods gathers utility aotra-specific-purpose methods usable in any JS project containing aotra environment and dependencies.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 */


// DOES NOT WORK WELL :
// Here is the position in non-rendered text : ie. «<p><b>Hello</b> |world</p>»
// = 16
function getHTMLCaretPositionInRichEditor(richTextComponent) {

	if (!richTextComponent)
		return -1;
	var editorSlct = jQuery(richTextComponent);
	if (empty(editorSlct.get()))
		return -1;

	var editableDiv = editorSlct.get(0);

	// TODO : Use two counters...
	// We iterate over the longest (non-rendered content) string :
	var contentNonRendered = richTextComponent.innerHTML;
	contentNonRendered = toOneSimplifiedLine(contentNonRendered);

	// Uses jquery.caret plugin to retrieve the brute caret position :
	// Here is the position in rendered text : ie. «Hello |world» = 7
	var caretPositionRendered = (nothing(editableDiv.lastCaretPosition) || editableDiv.lastCaretPosition < 0) ? editorSlct.caret()
			: editableDiv.lastCaretPosition;

	var cntNonRendered = 0;
	var cntRendered = 0;
	var isIgnorableChunk = false;
	for (var i = 0; i < contentNonRendered.length; i++) {

		var c = contentNonRendered.charAt(i);

		if (caretPositionRendered < cntRendered)
			return cntNonRendered;

		// We have to escape html entities, too:
		if (c === "<" || c === "&")
			isIgnorableChunk = true;
		else if (c === ">" || c === ";")
			isIgnorableChunk = false;

		if (!isIgnorableChunk)
			cntRendered++;
		cntNonRendered++;

	}

	return -1;

}

// UNTESTED ! (DO NOT USE IN PRODUCTION)
// ANOTHER POSSIBILITY FOR CARET POSITIN :
// stackoverflow.com/questions/263743/how-to-get-caret-position-in-textarea
// Fix pour component.prop("selectionStart")
function getCaret(el) {
	if (el.selectionStart) {
		return el.selectionStart;
	} else if (document.selection) {
		el.focus();

		var r = document.selection.createRange();
		if (r == null)
			return 0;

		var re = el.createTextRange(), rc = re.duplicate();
		re.moveToBookmark(r.getBookmark());
		rc.setEndPoint('EndToStart', re);

		return rc.text.length;
	}
	return 0;
}



//Code from (blog.mobiscroll.com/working-with-touch-events/)
//DEPENDENY : NEEDS jQuery
function initTouchWheel(handle,/* OPTIONAL */filterElementId) {

var rootDivSlct = filterElementId ? jQuery("#" + filterElementId) : jQuery(document.body);

var startX, startY, tap;

function getCoord(e, c) {
	return /touch/.test(e.type) ? (e.originalEvent || e).changedTouches[0]['page' + c] : e['page' + c];
}

function setTap() {
	tap = true;
	setTimeout(function() {
		tap = false;
	}, 500);
}

rootDivSlct.on('touchstart', function(ev) {
	startX = getCoord(ev, 'X');
	startY = getCoord(ev, 'Y');
}).on('touchend', function(ev) {

	var curX = getCoord(ev, 'X');
	var curY = getCoord(ev, 'Y');

	var deltaX = Math.abs(curX - startX);
	var deltaY = Math.abs(curY - startY);

	// If movement is less than 20px, execute the handler
	if (deltaX < 20 && deltaY < 20) {
		// Prevent emulated mouse events
		ev.preventDefault();
		// handler.call(this, ev);
		handle(deltaX, deltaY);
	}
	setTap();
}).on('click', function(ev) {
	if (!tap) {
		// If handler was not called on touchend, call it on click;
		// handler.call(this, ev);
		handle(deltaX, deltaY);
	}
	ev.preventDefault();
});

}

//HTML and DOM management :

//- nice HTML components :

//* An Ajax wait indicator :
//(DEPENDENCY : Jquery library)
window.waitIndicator = {};
// TODO : develop :
// Progression initialization :
//window.waitIndicator.progressionMonitors=[];
//window.waitIndicator.progressionTotalAmount=0;
//window.waitIndicator.progressionAmount=0;

window.waitIndicator.init = function() {

	// UI initialization :
	var waitIndicatorElement = document.getElementById("waitIndicatorElement");
	if(!waitIndicatorElement) {
		var rootDiv = document.body;
		waitIndicatorElement = document.createElement("div");
		waitIndicatorElement.id = "waitIndicatorElement";
		waitIndicatorElement.style=
			 "display:none;"
	    +"font-size: 5em;color: white;font-weight: bold;font-family:sans-serif;"
			+"border:none;position:fixed;width:10%;top:0;left:0;"
			+"text-shadow: -3px -3px 0 #000, 3px -3px 0 #000, -3px 3px 0 #000, 3px 3px 0 #000;";
		waitIndicatorElement.innerHTML=i18n({"fr":"Chargement...","en":"Loading..."});
		rootDiv.appendChild(waitIndicatorElement);
	}

}

window.waitIndicator.start = function() {
	
	var waitIndicatorElement=document.getElementById("waitIndicatorElement");
	jQuery(waitIndicatorElement).show();
	
	// We don't create a modal screen, to allows to NOT have to div with the max z-index !
	var rootDiv = document.body;
	var $root=jQuery(rootDiv);
	window.waitIndicator.oldBackground = $root.css("background");
	// «pointer-events none» is dangerous, because if they are not reactivated, then, all will remain un-clickable.
	window.waitIndicator.oldPointerEvents = $root.find(">*").css("pointer-events");// potentially

	
	$root.css("background","black").find(">*")
		.css("opacity","0.3")
	// «pointer-events none» is dangerous, because if they are not reactivated, then, all will remain un-clickable.
		.css("pointer-events","none");
	
	
	return window.waitIndicator;
}

window.waitIndicator.end = function() {
	
	jQuery(document.getElementById("waitIndicatorElement")).hide();

	// jQuery(img).parent().not(img).css("opacity","1");
	// Allows to not have to div with the max z-index
	var rootDiv = document.body;
	var $root=jQuery(rootDiv);
	$root.css("background",window.waitIndicator.oldBackground).find(">*")
		.css("opacity","1")
	// «pointer-events none» is dangerous, because if they are not reactivated, then, all will remain un-clickable.
		.css("pointer-events",window.waitIndicator.oldPointerEvents);// potentially dangerous

	return window.waitIndicator;
};

//TODO : develop :
// Progression monitoring :
//window.waitIndicator.progress=function(amount){
//	window.waitIndicator.progressionAmount+=amount;
//	var waitIndicatorElement=document.getElementById("waitIndicatorElement");
//	waitIndicatorElement.innerHTML=Math.ceil(window.waitIndicator.progressionAmount/window.waitIndicator.progressionTotalAmount)+"%";
//};
//	
//window.waitIndicator.addProgressionMonitor=function(progressionTotalAmount){
//	
//	let result={
//		waitIndicatorElement:document.getElementById("waitIndicatorElement"),
//		progressionTotalAmount:progressionTotalAmount,
//		progress:function(amount){
//			window.waitIndicator.progress(amount);
//		}
//	};
//	
//	window.waitIndicator.progressionTotalAmount+=progressionTotalAmount;
//	window.waitIndicator.progressionMonitors.push(result);
//
//	return result;
//}
//
//function monitorProgression(progressionTotalAmount,monitoredFunction){
//	monitoredFunction.progressionMonitor=window.waitIndicator.addProgressionMonitor(progressionTotalAmount);
//	return monitoredFunction;
//};


//-------------------------------------------------------------------------------------------

//* An in-content tutorial generator:
//(DEPENDENCY : Jquery libray)
function executeTutorialMacro(macrosStrings,/* OPTIONAL */maxZIndexHoverLay,/* OPTIONAL */labels) {

var incompleteActionMessage = "You have to do previous action to complete this tutorial. Please start all over again.";
incompleteActionMessage = labels["incompleteActionMessage"] ? labels["incompleteActionMessage"] : incompleteActionMessage;
var closeLabel = labels["closeLabel"] ? labels["closeLabel"] : "Close";

// (Actually this method is just encapsulating the
// doWholeProcessMethodAfterMacrosStrings(...) method...)
// Example :
// ["designate:#target1",
// "advice:#target1;Here is a piece of advice",
// "designate:#target2",
// "advice:#target2;Here is another piece of advice"]

var designateElement = function(targetElement,/* OPTIONAL */onEndMethod) {

	var POINTER = "&#x2196;";
	var SHADOW_THICKNESS = 3;
	var DELAY = 500;

	if (nothing(targetElement) || !jQuery(targetElement).is(":visible")) {

		// We break the chain :
		// TRACE
		alert(incompleteActionMessage);
		return;
	}

	if (!window.helpPointer) {
		window.helpPointer = document.createElement("span");
		window.helpPointer.innerHTML = POINTER;
		window.helpPointer.setStyle("position:absolute;font-size:32px;font-weight:bold;" + "z-index:" + (maxZIndexHoverLay | 1) + ";" + "text-shadow: -"
				+ SHADOW_THICKNESS + "px -" + SHADOW_THICKNESS + "px 0 #FFF," + "" + SHADOW_THICKNESS + "px -" + SHADOW_THICKNESS + "px 0 #FFF," + "-"
				+ SHADOW_THICKNESS + "px " + SHADOW_THICKNESS + "px 0 #FFF," + SHADOW_THICKNESS + "px " + SHADOW_THICKNESS + "px 0 #FFF;");

		document.body.appendChild(window.helpPointer);

		window.helpPointer.srcX = (getWindowSize("width") / 2);
		window.helpPointer.srcY = (getWindowSize("height") / 2);
	} else {
		jQuery(window.helpPointer).fadeIn(1000);

	}
	var helpPointerSlct = jQuery(window.helpPointer);

	var srcX = window.helpPointer.srcX;
	var srcY = window.helpPointer.srcY;

	window.helpPointer.setStyle("left:" + srcX + "px");
	window.helpPointer.setStyle("top:" + srcY + "px");

	// Destination coordinates :
	// var destX=(getWindowSize("width")/2);
	// var destY=(getWindowSize("height")/2);
	// if(!nothing(targetElement)
	// && jQuery(targetElement).is(":visible")
	// ){
	var targetRect = targetElement.getBoundingClientRect();
	var destX = targetRect.left + targetRect.width / 2;
	var destY = targetRect.top + targetRect.height / 2;
	// }

	bringElementSelectedToPosition(helpPointerSlct, destX, destY, DELAY, function() {
		helpPointerSlct.fadeOut(1000, function() {

			window.helpPointer.srcX = destX;
			window.helpPointer.srcY = destY;

			// We go to the next step :
			if (onEndMethod)
				onEndMethod();

		});
	});

};

var displayAdviceBox = function(targetElement, text,/* OPTIONAL */onEndMethod) {

	var BACKGROUND = "#FFE746";

	if (nothing(targetElement) || !jQuery(targetElement).is(":visible")) {
		// We break the chain :
		// TRACE
		alert(incompleteActionMessage);
		return;
	}

	if (!window.helpAdviceBox) {
		window.helpAdviceBox = document.createElement("span");
		window.helpAdviceBox.setStyle("background-color:" + BACKGROUND + ";" + "-moz-border-radius: 20px;border-radius: 20px;" + "cursor:pointer;" + "z-index:"
				+ (maxZIndexHoverLay | 1) + ";");

		var p = document.createElement("p");
		p.innerHTML = text;
		p.setStyle("margin:10px;");

		var a = document.createElement("a");
		a.className = "closeButton";
		a.innerHTML = closeLabel;
		a.onclick = function() {
			jQuery(window.helpAdviceBox).hide(onEndMethod);
		};
		a.setStyle("display:block;margin-top:20px;float:right;text-decoration:underline;");

		window.helpAdviceBox.appendChild(a);
		window.helpAdviceBox.appendChild(p);

		window.helpAdviceBox.setStyle("position:absolute;");

		document.body.appendChild(window.helpAdviceBox);
	} else {
		var helpAdviceBoxSlct0 = jQuery(window.helpAdviceBox);

		helpAdviceBoxSlct0.show();
		var p = helpAdviceBoxSlct0.find("p");
		p.get(0).innerHTML = text;
		var a = helpAdviceBoxSlct0.find("a.closeButton");

		a.get(0).onclick = function() {
			helpAdviceBoxSlct0.hide(onEndMethod);
		};

	}
	var helpAdviceBoxSlct = jQuery(window.helpAdviceBox);

	// Destination coordinates :
	var destX = (getWindowSize("width") / 2);
	var destY = (getWindowSize("height") / 2);
	if (!nothing(targetElement) && jQuery(targetElement).is(":visible")) {
		var targetRect = targetElement.getBoundingClientRect();
		destX = targetRect.left + targetRect.width / 2;
		destY = targetRect.top + targetRect.height / 2;
	}

	helpAdviceBoxSlct.css("left", destX);
	helpAdviceBoxSlct.css("top", destY);

};

var doWholeProcessMethodAfterMacrosStrings = function(macrosStringsArray) {

	var SEPARATOR = ";";

	var firstFunction = null;
	var oldFunction = null;
	for (var i = 0; i < macrosStringsArray.length; i++) {

		var macroString = macrosStringsArray[i];
		var split1 = macroString.split(":");
		var methodName = split1[0];
		var target = split1[1];
		var text = "";
		if (contains(target, SEPARATOR)) {
			var split2 = target.split(SEPARATOR);
			target = split2[0];
			text = split2[1];
		}

		if (methodName !== "designate" && methodName !== "advice")
			continue;
		if (empty(jQuery(target).get()))
			continue;

		var f = new Object();
		f.target = jQuery(target).get(0);
		f.text = text;
		f.methodName = methodName;

		if (methodName === "designate")
			f.method = designateElement;
		else if (methodName === "advice")
			f.method = displayAdviceBox;

		if (oldFunction !== null)
			oldFunction.endMethod = f;

		if (firstFunction === null)
			firstFunction = f;

		oldFunction = f;

	}

	var execFunction = function(f2) {
		if (nothing(f2))
			return;

		if (f2.methodName === "designate") {
			f2.method(f2.target, function() {
				execFunction(f2.endMethod);
			});
		} else if (f2.methodName === "advice") {
			f2.method(f2.target, f2.text, function() {
				execFunction(f2.endMethod);
			});
		}
	};

	if (firstFunction !== null) {
		execFunction(firstFunction);
	}

};

doWholeProcessMethodAfterMacrosStrings(macrosStrings);

}



//-------------------------------------------------------------------------------------------

//* A carousel :
//(DEPENDENCY : promptvalue(...) function)
//(DEPENDENXY : Jquery library)
function makeCarousel(divSlct, height, transition, background, autoScroll, transitionDurationMillis, autoScrollDurationMillis, buttonsCSS, isEditable,
	doAfterEdit,/* OPTIONAL */labels) {

if (divSlct === null)
	return;
if (empty(divSlct.get()))
	return;

var TRANSITION_FADE = "fade";
var TRANSITION_SLIDE = "slide";
var LEFT = "&#9664;";
var RIGHT = "&#9654;";
var PAUSE = "&#9608;";
var MAX_Z_INDEX_OVERLAY_LOCAL = 99999;
var SPECIAL_SEPARATOR = "@@@";

var width = height;

var closeLabel = nothing(labels["closeLabel"]) ? "Close" : labels["closeLabel"];
var editLabel = nothing(labels["editLabel"]) ? "Edit" : labels["editLabel"];

divSlct.each(function() {

	var thisSlct = jQuery(this);
	var div = thisSlct.get(0);
	var editableButtonSlct = null;

	// Edit carousel possibility:
	if (isEditable) {

		// Edit button displaying (if carousel is rendered only) : &#9998;
		editableButtonSlct = jQuery("<button class='icon editBtn editCarousel' style='display:block' " + " title='" + editLabel + "'></button><br />");
		editableButtonSlct.click(function() {

			// A window to ask new values for this carousel :
			promptWindow("Please edit carousel attributes", "form{" + "'transition':'textbox:" + i18n({
				"fr" : "Type de transition",
				"en" : "Transistion type"
			}) + "'" + ",'height':'textbox:" + i18n({
				"fr" : "Hauteur",
				"en" : "Height"
			}) + "'" + ",'background':'textbox:" + i18n({
				"fr" : "Couleur de fonds",
				"en" : "Background color"
			}) + "'" + "}", transition + SPECIAL_SEPARATOR + height + SPECIAL_SEPARATOR + background, function(returnValue) {
				if (nothing(returnValue))
					return;
				var s = returnValue.split(SPECIAL_SEPARATOR);
				thisSlct.css("height", s[1]);
				thisSlct.css("background", s[2]);
				doAfterEdit(s[0], s[1], s[2]);
			});
		});
	}

	// Carousel initialization :
	div.isTransitionPending = false;
	div.index = 0;
	div.itemsSlcts = new Array();

	// Navigation methods :
	/* private */var doGoOnPrev = function() {
		if (div.isTransitionPending)
			return;
		var oldIndex = div.index;
		div.index--;
		if (div.index < 0)
			div.index = div.itemsSlcts.length - 1;
		doTransition(div.itemsSlcts[oldIndex], div.itemsSlcts[div.index], -1, div.index, div.itemsSlcts.length);
	};

	/* private */var doGoOnNext = function() {
		if (div.isTransitionPending)
			return;
		var oldIndex = div.index;
		div.index++;
		if (div.itemsSlcts.length - 1 < div.index)
			div.index = 0;
		doTransition(div.itemsSlcts[oldIndex], div.itemsSlcts[div.index], 1, div.index, div.itemsSlcts.length);
	};

	// UNUSED (for now)
	/* private */var pauseFunction = function(event) {
		if (div.isNextsTaskExisting())
			div.stopNextsTask();
		else
			div.startNextsTask(olSlct);
	};

	// Utility methods ;
	/* private */div.startNextsTask = function() {
		if (div.isNextsTaskExisting())
			return;
		div.peNexts = new PeriodicalExecuter(function() {
			doGoOnNext();
		}, autoScrollDurationMillis / 1000);
	};

	/* private */div.stopNextsTask = function() {
		if (!div.isNextsTaskExisting())
			return;
		div.peNexts.stop();
		delete div.peNexts;
	};

	/* private */div.isNextsTaskExisting = function() {
		return (typeof div.peNexts !== "undefined") && div.peNexts;
	};

	// We construct the list of carousel items :
	var olSlct = jQuery(div).find("ol");
	olSlct.css("list-style-type", "none").css("overflow", "hide");
	olSlct.find("li").each(
			function(index2) {
				var liSlct = jQuery(this);
				liSlct.css("text-align", "center").css("cursor", "pointer");

				liSlct.click(function() {

					// Display an overlayed image :
					var root = document.body;
					if (!div.overlayDiv) {

						// Overlay
						div.overlayDiv = document.createElement("div");
						div.overlayDiv.setStyle("" + "background:black;" // Cannot set
						// opacity to
								// less or equal
								// to 1, because
								// else it will
								// also make the
								// images and
								// all children
								// elements
								// content
								// transparent !
								+ "width:100%;height:100%;" + "position:fixed;" + "top:0;left:0;z-index:" + MAX_Z_INDEX_OVERLAY_LOCAL + ";" + "text-align:center;"
								+ "vertical-align:middle;" + "display:flex;align-items:center;align-content:center;flex-direction:column"), root.appendChild(div.overlayDiv);

						// Overlayed display :
						div.overlayDisplay = document.createElement("div");
						div.overlayDisplay.setStyle("margin:auto;");
						div.overlayDiv.appendChild(div.overlayDisplay);

						// Close button and close events
						div.overlayCloseButton = document.createElement("button");
						div.overlayCloseButton.setStyle("font-size:1em;opacity:inherit;margin:auto;");
						div.overlayCloseButton.innerHTML = closeLabel;
						div.overlayDiv.appendChild(div.overlayCloseButton);

						var closeFunction = function() {
							jQuery(div.overlayDiv).fadeOut(1000);
						};
						jQuery(div.overlayDiv).click(closeFunction);
						jQuery(div.overlayCloseButton).click(closeFunction);
						addFireOnAnyKey(closeFunction);

					}

					jQuery(div.overlayDiv).fadeIn(1000);
					div.overlayDisplay.innerHTML = liSlct.html();

				});

				div.itemsSlcts.push(liSlct);
			});

	/* private */var hideAllNonSelected = function() {
		olSlct.find("li").each(function(index2) {
			var liSlct = jQuery(this);
			if (index2 != div.index) {
				liSlct.hide();
			}
		});
	};
	hideAllNonSelected();

	// var onShow=function(elementSlct){
	// elementSlct.css("display","table");
	// };

	/* private */var doTransition = function(oldElementSlct, newElementSlct, directionFactor, index, numberOfItems) {

		div.isTransitionPending = true;
		if (transition === TRANSITION_FADE) {
			oldElementSlct.fadeOut(transitionDurationMillis / 2, function() {
				newElementSlct.fadeIn(transitionDurationMillis / 2, function() {
					hideAllNonSelected();
					div.isTransitionPending = false;
					// onShow(newElementSlct);
				});
			});
		} else if (transition === TRANSITION_SLIDE) {
			var SLIDE_DURATION = 1000;

			var MOVE_PROPERTY;

			// TODO : FIXME : This effect does not work pretty well...
			if (0 < directionFactor) {
				MOVE_PROPERTY = "left";
			} else {
				MOVE_PROPERTY = "right";
			}

			oldElementSlct.find(".items").css(MOVE_PROPERTY, "0");
			oldElementSlct.find(".items").stop().animate({
				MOVE_PROPERTY : "-100%"
			}, {
				duration : SLIDE_DURATION,
				complete : function() {
					oldElementSlct.hide(function() {
						oldElementSlct.find(".items").css(MOVE_PROPERTY, "0");
					});
				}
			});

			newElementSlct.find(".items").css(MOVE_PROPERTY, "100%");
			newElementSlct.show(function() {
				newElementSlct.find(".items").stop().animate({
					MOVE_PROPERTY : "0"
				}, {
					duration : SLIDE_DURATION,
					complete : function() {
						newElementSlct.find(".items").css(MOVE_PROPERTY, "0");
						div.isTransitionPending = false;
						// onShow(newElementSlct);
					}
				});
			});

		} else { // Default transition is no transition at all...:
			oldElementSlct.hide({
				complete : function() {
					newElementSlct.show(function() {
						div.isTransitionPending = false;
						// onShow(newElementSlct);
					});
				}
			});
		}

	};

	// CAUTION : CSS «flex» attribute is currently flawed and inconsistent and
	// does not work here !
	// Carousel div style :
	thisSlct.css("max-width", "100%").css("height", height).css("display", "flex").css("direction", "row").css("align-items", "stretch").css("background",
			background);

	olSlct.css("padding", "0 0 0 0")
	// .css("margin","0 0 0 0")
	.css("display", "inline-block").css("width", "100%").css("overflow", "hidden");

	var liSlct = olSlct.find("li");
	liSlct.css("height", "100%").css("width", "100%");

	liSlct.find(".items").css("position", "relative")
	// .css("display","table-cell")
	.css("vertical-align", "middle").find("img").css("width", "100%");

	// Editable button placing :
	if (editableButtonSlct)
		thisSlct.before(editableButtonSlct);

	// Navigation buttons :
	var navigationButtonsCSS = buttonsCSS + "opacity:0.5;cursor:pointer;" + "position:relative;" + "height:100%;"
			+ "display:flex;width:1.5em;align-items:center;";

	// thisSlct.prepend("<span class='pause'
	// style='"+navigationButtonsCSS+";margin-left:5px;'>"+PAUSE+"</span>");
	// thisSlct.find(".pause").click(pauseFunction);

	// Next button placing :
	thisSlct.prepend("<span class='prev' style='" + navigationButtonsCSS + ";'><span>" + LEFT + "</span></span>");
	thisSlct.find(".prev").click(function(event) {
		doGoOnPrev();
	});

	// Previous button placing :
	thisSlct.append("<span class='next' style='" + navigationButtonsCSS + ";flex-direction:row;justify-content:flex-end;'><span>" + RIGHT + "</span></span>");
	thisSlct.find(".next").click(function(event) {
		doGoOnNext();
	});

	// Auto scroll start (if necessary) :
	if (autoScroll)
		div.startNextsTask()

});

}



/* ## Utility global methods
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 * DEPENDENCY : JQUERY
 * 
 */


//================= aotra andrana (means «trial, try» in malagasy) sub-component =================
// unit and integration testing framework
//================================================================================================



// (Duplicated doc with aotrautils-server)
// ============================== COMPLETE AOTEST DOCUMENTATION BELOW ============================== 
//
// Unit tests framework embedded in aotra (called "aotest") : (inherits this utils library license)
// «aotest» is a short name used in code for «aotest», which is the real name.
// - LAUNCH INFORMATION :
// All tests are registered as javascript declaring function is executed.
// To execute registered tests, simply execute aotest.run(); after the tests registration code is executed.
// - DEPENDENCIES INFORMATION :
// (DEPENDENCY : the nothing(...) function in utils library)
// (DEPENDENCY : the log(...) function in utils library)
// (DEPENDENCY : the isArray(...) function in utils library)
// (DEPENDENCY : the getFunctionName(...) function in utils library)
// (DEPENDENCY : the getHashedString(...) function in utils library)
// (DEPENDENCY : the getUniqueIdWithDate(...) function in utils library)
// (DEPENDENCY : the getURLParameter(...) function in utils library)
// (DEPENDENCY : jquery methods and functions : if you want to run UI user tests, to simulate UI events and test the outcome... -examples below-)
// - MISCELLANEOUS :
// (Note to developer : functions / methods used in function tracking CANNOT be tested with aotest framework ! or else there will be calls overflow errors. Trust me. I tried.)
// - UNIT TESTS EXAMPLES :
////  Data and treatments testing :
////  Examples : 
//var addPoints2DCoordinates = aotest(
////  Tests definition
//{
//  //  Tested function/method name : 
//  name : "addPoints2DCoordinates"
//  //  Dummies :
//  ,
//  dummies : function() {
//      var result = new Object();
//      result.pDummy1 = new Object();
//      result.pDummy1.x = 20;
//      result.pDummy1.y = 10;
//      result.pDummy2 = new Object();
//      result.pDummy2.x = 60;
//      result.pDummy2.y = 40;
//
//      return result;
//  }
//  //  Normal scenarii :
//  , scenarioNormal1 : [ [ {
//      dummy : "pDummy1"
//  }, {
//      dummy : "pDummy2"
//  } ], function(result) {
//      return result.x === 80 && result.y === 50;
//  } ],
//  scenarioNormal2 : [ [ {
//      dummy : "pDummy1"
//  }, {
//      dummy : "pDummy2"
//  } ], {
//      asserts : "success"
//  } ]
//  //  Abnormal scenarii :
//  , scenarioParameter1IsNull : [ [ null, {
//      dummy : "pDummy2"
//  } ], {
//      asserts : "fail"
//  } ],
//  scenarioParameter2IsNull : [ [ {
//      dummy : "pDummy1"
//  }, null ], {
//      asserts : "fail"
//  } ]
//}
//// Function definition
//, function(p1, p2) {
//  var result = new Object();
//  aotest.assert(p1 === null, "fail");
//  aotest.assert(p2 === null, "fail");
//  result.x = p1.x + p2.x;
//  result.y = p1.y + p2.y;
//  return result;
//});
//
////  (Function can share the same dummies because dummies are global)
//var invertPoints2DCoordinates = aotest(
//
////  Tests definition 
//{
//  //  Tested function/method name :
//  name : "invertPoints2DCoordinates"
//
//  //  Dummies :
//  , dummies : null
//  //  Normal scenarii : 
//  , scenarioNormal1 : [ [ {
//      dummy : "pDummy1"
//  } ], function(result) {
//      return result.x === -20 && result.y === -10;
//  } ],
//  scenarioNormal2 : [ [ {
//      dummy : "pDummy2"
//  } ], function(result) {
//      return result.x === -60 && result.y === -40;
//  } ],
//  scenarioNormal3 : [ [ {
//      dummy : "pDummy1"
//  } ], {
//      asserts : "success"
//  } ]
//  //  Abnormal scenarii :
//  , scenarioParameterIsNull : [ [ null ], {
//      asserts : "fail"
//  } ]
//}
////  Function definition
//, function(p1) {
//  var result = new Object();
//  aotest.assert(p1 === null, "fail");
//  result.x = -p1.x;
//  result.y = -p1.y;
//  return result;
//});
////  Advanced unit testing :
//function less(a, b) {
//  if(!a || !b)
//      throw "Arguments must be non-null !";
//  return a - b;
//}
//function add(a, b) {
//  return a + b;
//}
//function multiply(a, b) {
//  var result = 0;
//  for(var i = 0; i < b; i++)
//      result += a;
//  return result;
//}
//
////  - Calls and errors throws monitored tests :
//
//aotest({
//  name : "multiply",
//  scenario1 : {
//      values : [ [ 2, 5 ], 10 ],
//      calls : {
//          multiply : 1,
//          add : 1,
//          less : 0
//      }
//  },
//  scenario2 : {
//      values : [ [ 2, null ], {
//          asserts : "fail"
//      } ],
//      calls : {
//          multiply : 1,
//          add : "throw",
//          less : 0
//      }
//  }
//}, multiply);
//aotest({
//  name : "less",
//  scenario1 : {
//      values : [ [ 2, 5 ], -3 ],
//      calls : {
//          multiply : 0,
//          add : 0,
//          less : 1
//      }
//  }
//}, less);
////  Another case, to illustrate that when null paramaters passed, instead of empty array, mean we don't want to execute the tested function / method as the main testing routine 
//less = aotest({}, less);
//multiply = aotest({}, multiply);
//add = aotest({
//  scenarAddTrack1 : {
//      values : [ null, function() {
//          return multiply(2, 10) === 20;
//      } ],
//      calls : {
//          multiply : 1,
//          add : 10,
//          less : 0
//      }
//  },
//  scenarAddTrack2 : {
//      values : [ null, function() {
//          return multiply(2, 10) === 20;
//      } ],
//      calls : {
//          multiply : 1,
//          add : 10,
//          less : 0
//      }
//  },
//  scenarAddTrack3 : [ null, function() {
//      return true;
//  } ]
//}, add);
//
//function less(a, b) {
//  if(!a || !b)
//      throw "Arguments must be non-null !";
//  return a - b;
//}
//function add(a, b) {
//  aotest.assert(a !== null && b !== null, "success");
//  return a + b;
//}
//function multiply(a, b) {
//  var result = 0;
//  for(var i = 0; i < b; i++)
//      result = add(result, a);
//  return result;
//}
////  CAUTION : If no assert is declared and your test expect asserts to fail, then you test will fail.  But, if you have no assert in your test and expect asserts to success, then your test will succeed.
////  It is because you can see asserts as obstacles to your test success : So if there is no obstacle to its success, then there is will be no reason for it not to succeed ! 
////  - Calls and errors throws monitored
//tests: less = aotest({}, less);
//add = aotest({}, add);
//aotest({
//  // ,scenario1:{values:[[2,20],40],calls:{multiply:1,add:20,less:0}}
//  // ,scenario2:{values:[[null,2],{asserts:"fail"}],calls:{multiply:1,add:"throw",less:0}}
//  scenario3 : {
//      values : [ [ 4, 3 ], {
//          asserts : "success"
//      } ],
//      calls : {
//          multiply : 1,
//          add : 3,
//          less : 0
//      }
//  }
//}, multiply);
////  Yet another example for calls and throws monitoring :
//function less(a, b) {
//  if(!a || !b)
//      throw "Arguments must be non-null !";
//  return a - b;
//}
//function add(a, b) {
//  aotest.assert(a !== null && b !== null, "success");
//  return a + b;
//}
//function multiply(a, b) {
//  var result = 0;
//  for(var i = 0; i < b; i++)
//      result = add(result, a);
//  return result;
//} //  - Calls and errors throws monitored
//tests: less = aotest({}, less);
//add = aotest({}, add);
//aotest({
//  // ,scenario1:{values:[[2,20],40],calls:{multiply:1,add:20,less:0}}
//  // ,scenario2:{values:[[null,2],{asserts:"fail"}],calls:{multiply:1,add:"throw",less:0}}
//  scenario3 : {
//      values : [ [ 4, 3 ], {
//          asserts : "success"
//      } ],
//      calls : {
//          multiply : 1,
//          add : 3,
//          less : 0
//      }
//  }
//}, multiply);
////  -Imbriqued tests :
//aotest({
//  name : "multiplyNested",
//  scenario1Nested : {
//      values : [ [ 2, 5 ], 10 ],
//      subs : {
//          scenario1Sub : [ [ 100, 5 ], 500 ],
//          scenario2Sub : {
//              values : [ [ 10, 10 ], 100 ],
//              subs : {
//                  scenario1SubSub : [ [ 1000, 1000 ], 1000000 ]
//              }
//          }
//      }
//  }
//}, multiply);
////  User UI testing :
//aotest({
//  name : "getAotraScreen",
//  scenario1 : [ [], function() {
//      return getAotraScreen() !== null;
//  } ]
//});
//aotest({
//  name : "editionProvider",
//  scenarioCreateBubble : [ [], function() {
//      var aotraScreen = getAotraScreen();
//
//      var activePlane = aotraScreen.getActivePlane();
//      aotest.assert(activePlane === null, "fail");
//
//      var oldBubblesNumber = activePlane.getBubbles().length;
//
//      aotest.assert(typeof jQuery === "undefined", "fail");
//
//      var createButtonS = jQuery(".icon.addBubbleBtn");
//      aotest.assert(empty(createButtonS.get()), "fail");
//      createButtonS.click();
//
//      aotest.assert(typeof aotraScreen.mainDiv === "undefined", "fail");
//
//      var mainDivS = jQuery(aotraScreen.mainDiv);
//      aotest.assert(empty(mainDivS.get()), "fail");
//      mainDivS.click();
//
//      var applyButtonS = jQuery("#buttonSaveAndCloseBubble");
//      aotest.assert(empty(applyButtonS.get()), "fail");
//      applyButtonS.click();
//
//      var newBubblesNumber = activePlane.getBubbles().length;
//
//      return newBubblesNumber === oldBubblesNumber + 1;
//  } ],
//  scenarioAsserts : [ [], {
//      asserts : "success"
//  } ]
//});




// AOTEST (headfull) User Simulation Tests :

window.aotestMethodsUI={
		pacingMillis:800, // Default value
		simulateKeysSend:function(element, str){

			// Prerequisiste : JQuery plugins adding :
			// jQuery plugin. Called on a jQuery object, not directly.
			if(nothing(jQuery.fn.simulateKeyPress )){
  			jQuery.fn.simulateKeyPress = function(character) {
  				// Internally calls jQuery.event.trigger with arguments (Event, data, elem).
  				// That last argument, 'elem', is very important!
  				jQuery(this).trigger({
  					type : 'keypress',
  					which : character.charCodeAt(0)
  				});
  			};
			}
			
			if(element.type !== "text") {
				return;
			}
			var elementSlct = element.slct;
			elementSlct.focus();
			// TODO : FIXME : Use a keypress plugin..
			//		for(var i = 0; i < str.length; i++) {
			//			setTimeout(function() {
			//				elementSlct.simulateKeyPress(str.charAt(i));
			//			}, window.aotestMethodsUI.pacingMillis * (i + 1));
			//		}
			// FOR DEBUG...:
			setTimeout(function() {
				elementSlct.val(str);
			}, window.aotestMethodsUI.pacingMillis);

			elementSlct.blur();
		},
		simulateKeysClick:function(element){
			// TODO
		},
};


//
//============================== COMPLETE RAJTEST DOCUMENTATION BELOW ============================== 
//
// Rajako monkey-testing automation module :
// rajtest is a short name used in code for rajakotest, which is the official name.
// - LAUNCH INFORMATION :
// Add script link or javascript code in each of the pages you want rajtest to generate aotests to.
// To execute tests generation, simply call your first page with the URL parameter «gentests=true».
// - DEPENDENCIES INFORMATION :
// (DEPENDENCY : aotest dependencies and methods and functions, in utils library)
//

window.aotest.rajtest = function() {
	if(getURLParameter("gentests") !== "true") {
		log("Parameter «gentests» is null, not launching tests generation.");
		return;
	}

	log("Starting tests generation...");

	var step = getURLParameter("step");
	if(nothing(step) || isNaN(step))
		step = 1;

	// Prerequisite : utility methods :
	var generateRandomText=function(){
		// TODO : DEVELOP....

	};

	// First we register all the inputs and interactive elements in page, in their DOM apparition order :
	var inputs = [];
	var queryInput = "a,button,input[type='text']";

	jQuery(queryInput).each(function() {
		var $this = jQuery(this);
		var tagName = $this.prop("tagName");
		var type = $this.attr("type");
		inputs.push({
			tagName : tagName,
			type : type,
			slct : $this
		});
	});

	// Then, we generate the monkey-testing scenarii :

	// TODO : DEVELOP....
	
};
jQuery(document).ready(window.aotest.rajtest);

// -------------------------------------------------------------------------------------------



function getAllHTMLElementsWithClass(className,/* OPTIONAL */elementWhereToSearchId) {
	// PLEASE KEEP CODE :
	// var elements=new Array();
	//
	// var els=document.getElementsByTagName("*");
	// for(var i=0;i<els.length;i++){
	// var e=els[i];
	// if(containsIgnoreCase(e.className,className))
	// elements.push(e);
	// }
	// return elements;
	return jQuery((elementWhereToSearchId ? ("#" + elementWhereToSearchId) : "") + " ." + className).get();
}



function makeGauge($display,title,value,total,percent,color,mode,/*OPTIONAL*/gaugeStyle){
	const DEFAULT_BACKGROUND_COLOR="#AAAAAA";

 	$display.css("font-size","20px");
 	$display.css("color","#FFFFFF");
 	$display.css("text-shadow","-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000");
 	$display.css("background-color",DEFAULT_BACKGROUND_COLOR);
 	if(mode==="gauge"){
	 	$display.css("background-image","linear-gradient(left, "+color+", "+color+" "+percent+"%, transparent "+percent+"%, transparent 100%)");
	 	$display.css("background-image","-webkit-linear-gradient(left,  "+color+", "+color+" "+percent+"%, transparent "+percent+"%, transparent 100%");
	 	$display.html(title+" "+value+"/"+total	+" ("+percent+"%)");
 	}else{
	 	$display.html(title+" "+value);
 	}

}
 	

//================= aotra lalao (means «game» in malagasy) sub-component =================
// basic gaming utilities
//========================================================================================



//Basic gaming management
//DEPENDENCY : REQUIRES JQUERY

function initBasicGaming(bubbleDiv){

 jQuery(bubbleDiv).find(".game.get").each(function(){
 	var $this=jQuery(this);					
 	var key=$this.attr("data-key");
 	if(nothing(key))	return;
 	var value=getStringFromStorage(key);
 	if(!nothing(value)) 	$this.html(value);
 });
 
 jQuery(bubbleDiv).find("input[type='text'].game.set").each(function(){
 	var $this=jQuery(this);
 	var key=$this.attr("data-key");
 	if(nothing(key))	return;
 	var oldValue=getStringFromStorage(key);
 	var defaultValue=$this.attr("data-default");
 	
 	// TODO : Develop : Ajouter randNumber et randText
 	
 	if(nothing(oldValue) && !nothing(defaultValue))	storeString(key,defaultValue);
 	$this.on("change",function(){
 		var value=jQuery(this).val();
 		if(!nothing(value)) 	storeString(key,value);
 	});
 });

 jQuery(bubbleDiv).find(".game.set.click").each(function(){
 	var $this=jQuery(this);
 	var key=$this.attr("data-key");
 	if(nothing(key))	return;
 	var oldValue=getStringFromStorage(key);
 	var defaultValue=$this.attr("data-default");
 	if(nothing(oldValue) && !nothing(defaultValue))	storeString(key,defaultValue);
 	var value=$this.attr("data-value");
 	if(nothing(value))		return;
 	$this.on("click",function(){
   		storeString(key,value);
 	});
 });
 
 
 // CAUTION : this method only uses the VISIBILITY and not the DISPLAY CSS attribute on elements it wants to show/hide :
 function ifTreatment($this,ifConditionFunction){
 	var key=$this.attr("data-key");
 	if(nothing(key))	return;

 	var value=$this.attr("data-value");
 	if(nothing(value))	value="true"; // A simplified boolean condition

 	var KEY_WORD="key:";
 	if(contains(value,KEY_WORD)) 	value=getStringFromStorage(value.replace(KEY_WORD),"");
 	if(nothing(value)) 	return;

 	var valueFromStorage=getStringFromStorage(key);
 	if(nothing(valueFromStorage))		valueFromStorage=null;
 	
 	var isConditionTrue=ifConditionFunction(valueFromStorage,value);
 	
 	if(isConditionTrue)   $this.css("visibility", "visible");
 	else									$this.css("visibility", "hidden");
 	
 };

 jQuery(bubbleDiv).find(".game.if.nothing").each(function(){ifTreatment(jQuery(this),function(a){return nothing(a) ;} ); });
 jQuery(bubbleDiv).find(".game.if.something").each(function(){ifTreatment(jQuery(this),function(a){return !nothing(a) ;} ); });
 jQuery(bubbleDiv).find(".game.if.true").each(function(){ifTreatment(jQuery(this),function(a){return !nothing(a) && a==="true" ;} ); });
 jQuery(bubbleDiv).find(".game.if.false").each(function(){ifTreatment(jQuery(this),function(a){return nothing(a) || a==="false" ;} ); });
 jQuery(bubbleDiv).find(".game.if.eq").each(function(){ifTreatment(jQuery(this),function(a,b){return a===b;} ); });
 jQuery(bubbleDiv).find(".game.if.ineq").each(function(){ifTreatment(jQuery(this),function(a,b){return a!==b;} ); });
 jQuery(bubbleDiv).find(".game.if.gt").each(function(){ifTreatment(jQuery(this),function(a,b){return parseInt(a)<parseInt(b);} ); });
 jQuery(bubbleDiv).find(".game.if.lt").each(function(){ifTreatment(jQuery(this),function(a,b){return parseInt(a)>parseInt(b);} ); });

 // TODO : FIXME : Only works for one single group per element ! Make other siblings work too !!
 jQuery(bubbleDiv).find(".game.group.rand").each(function(){
 	var $this=jQuery(this);
		var result=false;
 		
		var childrenSlct=$this.children();
		var intervalsUpperValues=[];

		var sum=0;
		childrenSlct.each(function(){
			var chance=parseInt(jQuery(this).attr("data-value"));
			
			if(nothing(chance))	return;
			sum+=chance;
			
			intervalsUpperValues.push({selector:jQuery(this),sum:sum});
			
			jQuery(this).css("visibility", "hidden");
		});

		var randomInt=Math.getRandomInt(sum);

		var intervalCurrentLowerValue=0;
		for(var i=0;i<intervalsUpperValues.length;i++){
			var obj=intervalsUpperValues[i];
			var intervalCurrentUpperValue=obj.sum;
			var slct=obj.selector;
			
			if(intervalCurrentLowerValue<randomInt && randomInt<=intervalCurrentUpperValue){
				slct.css("visibility", "visible");
				break;
			}
			intervalCurrentLowerValue=intervalCurrentUpperValue;
		}
		
		return result;
 });
 
}



function initRPGGaming($rpg){
	
	
	// Specifications displaying :
	initSpec=function($this){
		 
	 	var $params=$this.find(".params");
	 	if(!$params.length)	return;
	 	var params=$params.html();
// 		$params.html("");
 		$params.hide();

	 	
	 	var allValues={};
	 	
//	 	var dataAllValuesStr=$this.attr("data-allValues");
//	 	if(!dataAllValuesStr){

	 		var paramsArr=contains(params,":")?params.split(":"):[params];
	 	
		 	var titleStr=paramsArr[0].split(".");
		 	allValues.gameKey=titleStr[0];
		 	allValues.title=titleStr[1];
		 	allValues.key=allValues.gameKey+"."+allValues.title;
		 	$this.attr("id",allValues.key);

		 	var valuesArr=paramsArr[1].split("/");
		 	allValues.defaultValue=parseInt(valuesArr[0]);
		 	allValues.total=parseInt(valuesArr[1]);

		 	var modifiersStr=paramsArr[2];
		 	allValues.hasModifiers=false;
		 	allValues.modifiers=[];
	 		if(!empty(modifiersStr)){
			 	if(contains(modifiersStr,",")){
			 		var modifiersArr=modifiersStr.split(",");
			 		foreach(modifiersArr,(i)=>{
			 			allValues.modifiers.push(parseInt(i));
			 		});
			 	}else{
			 		allValues.modifiers=[parseInt(modifiersStr)];
			 	}
				allValues.hasModifiers=true;
	 		}
		 	
	 		allValues.isDice=paramsArr[3]==="dice";
	 		// UNUSEFUL : allValues.isAddableSubstractable=paramsArr[4]==="+-";
	 		allValues.color=paramsArr[4];
		 	
	 		var value=0;
		 	var storedValue=getStringFromStorage(allValues.key);
		 	if(nothing(storedValue)){
		 		value=parseInt(allValues.defaultValue);
	 			storeString(allValues.key,value);
		 	} else{
		 		value=parseInt(storedValue);
		 	}
		 	allValues.value=value;
		 	allValues.percent=Math.round((allValues.value/allValues.total)*100);
		 	
		 	allValues.modifierValue=0;
		 	
//	 	}else{
//		 	allValues=parseJSON(dataAllValuesStr);
//	 	}
	 	
	 	
	 	// UI elements :
	 	var $display=$this.find(".display");
	 	if(!$display.length){
	 		$display=jQuery("<span class='display'></span>");
	 		$this.append($display);

	 		if(allValues.isDice){
	 			
	 			if(allValues.hasModifiers){
	 
			 		var $modifiers=jQuery("<span class='modifiers'><select></select></span>");
			 		$this.append($modifiers);
//				 		$modifiers.parent().attr("data-hasModifiers",true);
			 		
			 		if(!empty(allValues.modifiers)){

			 			var $modifiersSelect=$modifiers.find("select");
				 		foreach(allValues.modifiers,(i)=>{
				 			var $option=jQuery("<option value='"+i+"'"+(i===0?" selected":"")+">"
				 														+(i<0?(i):(i===0?0:("+"+i)))
				 												+"</option>");
				 			$modifiersSelect.append($option);
				 		});

			 		}
			 		
	 			}
	 			
		 		var $dice=jQuery("<button class='dice'>Dice !</button>");
		 		$this.append($dice);
		 		$dice.click(function(event){
		 			var $diceButton=jQuery(this);
		 			
		 			var $diceParent=$diceButton.parent();
		 			
		 			if(allValues.hasModifiers && !empty(allValues.modifiers)){

		 				var $modifierSelect=$diceParent.find("select");
			 			
		 				if($modifierSelect.length){
			 				var valueForModifierStr=$modifierSelect.val();
				 			if(!nothing(valueForModifierStr)){
				 				allValues.modifierValue=parseInt(valueForModifierStr);
				 			}
		 				}
		 				
		 			}
		 			
		 			var randomValue=Math.getRandomInt(100,1);
		 			
		 			var success=false;
		 			if(randomValue<=allValues.percent+allValues.modifierValue){
		 				success=true;
		 			}
		 			
		 			
		 			var $diceLabel=$diceParent.find(".diceLabel");
		 			$diceLabel.attr("data-result",success?"success":"fail");
		 			$diceLabel.html(" "+randomValue).css("color",success?"green":"red");
		 			if(randomValue===1 || randomValue===100)	$diceLabel.css("font-weight","bold");
		 			else																			$diceLabel.css("font-weight","normal");
		 		

		 		});
		 		
		 		
		 		var $diceLabel=jQuery("<span class='diceLabel'></span>");
		 		$this.append($diceLabel);

		 		
	 		}
	 		
	 	}

	 	makeGauge($display,allValues.title,allValues.value,allValues.total,allValues.percent,allValues.color,"gauge");

	 	
	};
	$rpg.closest(".spec").each(function(){
		initSpec(jQuery(this));
	});
	
	
	// Amounts displaying :
	var initAmount=function($this){
		 
		 	var $params=$this.find(".params");
		 	if(!$params.length)	return;
		 	var params=$params.html();
//	 		$params.html("");
	 		$params.hide();
		 	
		 	var allValues={};
		 	
//		 	var dataAllValuesStr=$this.attr("data-allValues");
//		 	if(!dataAllValuesStr){

		 		var paramsArr=contains(params,":")?params.split(":"):[params];
		 	
			 	var titleStr=paramsArr[0].split(".");
			 	allValues.gameKey=titleStr[0];
			 	allValues.title=titleStr[1];
			 	allValues.key=allValues.gameKey+"."+allValues.title;
			 	$this.attr("id",allValues.key);
			 	
			 	var valueStr=paramsArr[1];
			 	if(contains(valueStr,"/")){
			 		// gauge, because there is a total value
			 		var valuesArr=valueStr.split("/");
			 		allValues.defaultValue=parseInt(valuesArr[0]);
			 		allValues.total=parseInt(valuesArr[1]);
			 		allValues.mode="gauge";
			 	}else{ // quantity, because there is no total value
			 		allValues.defaultValue=parseInt(valueStr);
			 		allValues.total=null;
			 		allValues.mode="quantity";
			 	}
			 	
			 	allValues.isAddableSubstractable=paramsArr[2]==="+-";
			 	allValues.color=paramsArr[3];
			 	
			 	
		 		var value=0;
			 	var storedValue=getStringFromStorage(allValues.key);
			 	if(nothing(storedValue)){
			 		value=parseInt(allValues.defaultValue);
		 			storeString(allValues.key,value);
			 	} else{
			 		value=parseInt(storedValue);
			 	}
			 	allValues.value=value;
			 	
			 	if(allValues.mode==="gauge"){
			 		allValues.percent=Math.round((allValues.value/allValues.total)*100);
			 	}else{
			 		allValues.percent=null;
			 	}
			 	
//		 	}else{
//			 	allValues=parseJSON(dataAllValuesStr);
//		 	}
			 	
			 	
		 	
		 	// UI elements :
		 	var $display=$this.find(".display");
		 	if(!$display.length){
		 		$display=jQuery("<span class='display'></span>");
		 		$this.append($display);

		 		
		 		if(allValues.isAddableSubstractable){
	 		
			 		var $addSubstract=jQuery("<span class='addSubstract'></span>");
			 		$this.append($addSubstract);
		 			
		 			var $addSubstractField=jQuery("<input type='number'></button>");
		 			$addSubstract.append($addSubstractField);
		 			$addSubstractField.css("width","60px");
		 			
			 		var $addSubstractButton=jQuery("<button>+-</button>");
			 		$addSubstract.append($addSubstractButton);
			 		$addSubstractButton.click(function(event){
			 			
			 			var $thisAddSubstractButton=jQuery(this);
			 			
			 			var $thisAddSubstractParent=$thisAddSubstractButton.parent();
			 			
			 			var addSubstractValue=$thisAddSubstractParent.find("input").val();
			 			if(!addSubstractValue)	return;
			 			
			 			var newVal=allValues.value+parseInt(addSubstractValue);
			 			if(allValues.total && (newVal<=0 || allValues.total<=newVal))	return;

			 			allValues.value=newVal;
//				 	allValues.percent=Math.round((allValues.value/allValues.total)*100);
			 			
			 			
			 			storeString(allValues.key,allValues.value);
			 			
			 			var $container=$thisAddSubstractParent.parent();
			 			$container.find(".display").remove();
			 			$container.find(".addSubstract").remove();

//			 		var newAllValuesStr=stringifyObject(allValues);
//			 		$container.attr("data-allValues",newAllValuesStr);

			 			initAmount($container);
			 			
			 			
			 		});
		 			
			 		
		 		}
		 		
		 		
		 	}

		 	makeGauge($display,allValues.title,allValues.value,allValues.total,allValues.percent,allValues.color,allValues.mode);
		 	
		 	
	};
	$rpg.closest(".amount").each(function(){
		initAmount(jQuery(this));
	});
	
	
	

	// Inventory displaying :
	var initInventory=function($this){
		 
		 	var $params=$this.find(".params");
		 	if(!$params.length)	return;
		 	var params=$params.html();
//	 		$params.html("");
	 		$params.hide();

		 	
		 	var allValues={};
		 	
//		 	var dataAllValuesStr=$this.attr("data-allValues");
//		 	if(!dataAllValuesStr){

		 		var paramsArr=contains(params,":")?params.split(":"):[params];
		 	
			 	var titleStr=paramsArr[0].split(".");
			 	allValues.gameKey=titleStr[0];
			 	allValues.title=titleStr[1];
			 	allValues.key=allValues.gameKey+"."+allValues.title;

			 	var configStr=$this.attr("data-config");
			 	if(configStr){
			 		allValues.config=parseJSON(configStr);
			 	}else{
			 		allValues.config={};
			 	}
			 	var displayModeStr=paramsArr[1];
			 	if(!allValues.config.displayMode){
			 		allValues.config.displayMode={};
			 		if(contains(displayModeStr,"x")){
				 		allValues.config.displayMode.type="grid";
				 		var displayModeSizes=displayModeStr.split("x");
				 		allValues.config.displayMode.w=parseInt(displayModeSizes[0]);
				 		allValues.config.displayMode.h=parseInt(displayModeSizes[1]);
				 		allValues.config.maxSize=allValues.config.displayMode.w*allValues.config.displayMode.h;
			 		}else{
					 	allValues.config.displayMode.type="list";
					 	allValues.config.maxSize=30;// default
			 		}
			 	}
			 	
			 	var contentStr=$this.attr("data-content");
			 	if(contentStr){
			 		var contentsArr=contains(contentStr,",")?contentStr.split(","):[contentStr];
			 		foreach(contentsArr,(i)=>{
			 			var chunks=i.split("=");
			 			var key=chunks[0];
			 			var value=parseInt(chunks[1]);
			 			var wholeKey=allValues.key+".inventory."+key;
			 			if(getStringFromStorage(wholeKey))	return "continue";
			 			storeString(wholeKey,value);
			 		});
			 	}
			 	
//		 	}else{
//			 	allValues=parseJSON(dataAllValuesStr);
//		 	}
			 	
		 	
		 	// UI elements :
		 	var $display=$this.find(".display");
		 	if(!$display.length){
		 		
		 		var $title=jQuery("<div class='title'></div>");
		 		$this.append($title);
		 		$title.html(allValues.title);
		 		
		 		$display=jQuery("<div class='display'></div>");
		 		$this.append($display);

		 		var cnt=0;
		 		foreach(localStorage,(i,wholeKey)=>{
		 			if(!contains(wholeKey,allValues.key) )	return "continue";
		 			
		 			var startIndex=wholeKey.indexOf("inventory.");
		 			var key=wholeKey.substr(startIndex).split(".")[1];
		 			var value=parseInt(i);
		 			
		 			var $cell;
		 			if(allValues.config.displayMode.type==="grid"){
		 				var $table=$display.find("table");
		 				if(!$table.length){
		 					$table=jQuery("<table></table>");
		 					$display.append($table);
		 				}
		 				if(cnt%allValues.config.displayMode.w===0){
		 					$tr=jQuery("<tr></tr>");
		 					$table.append($tr);
		 				}else{
		 					$tr=$table.find("tr").last();
		 				}
		 				$cell=jQuery("<span class='cell grid'></span>");
		 				$tr.append($cell);
		 				$cell.html(key.substr(0,1));
		 			}else{
		 				$cell=jQuery("<div class='cell list'></div>");
		 				$display.append($cell);
		 				$cell.html(key+" ("+value+")");
		 			}
		 			
		 			if(allValues.config.maxSize<=cnt)	return "return";
		 			cnt++
		 			
		 		});
		 		
		 		
		 	}

//		 	$display.css("font-size","20px");
//		 	$display.css("color","#FFFFFF");
//		 	$display.css("text-shadow","-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000");
//		 	$display.css("background-color",DEFAULT_BACKGROUND_COLOR);
		 	
	};
	$rpg.closest(".inventory").each(function(){
		initInventory(jQuery(this));
	});
	
	
	
	// Dice throw results conditional displaying :
	var initDiceTest=function($this){
		 
		 	var $params=$this.find(".params");
		 	if(!$params.length)	return;
		 	var params=$params.html();
//	 		$params.html("");
	 		$params.hide();

		 	
		 	var allValues={};
		 	
//		 	var dataAllValuesStr=$this.attr("data-allValues");
//		 	if(!dataAllValuesStr){

		 		var paramsArr=contains(params,":")?params.split(":"):[params];
		 		
			 	var titleStr=paramsArr[0].split(".");
			 	allValues.gameKey=titleStr[0];
			 	allValues.title=titleStr[1];
			 	allValues.key=allValues.gameKey+"."+allValues.title;
			 	
			 	var configStr=$this.attr("data-config");
			 	if(configStr){
			 		allValues.config=parseJSON(configStr);
			 	}else{
			 		allValues.config={};
			 	}

			 	
			 	allValues.outcomes={};
			 	var registerOutcomes=function(outcomeStr,type){
			 		
			 		allValues.outcomes[type]={};
				 	foreachSplit(outcomeStr,"," ,(str)=>{
				 		var key;
				 		var value;
				 		var split;
				 		if(contains(str,"+")){
				 			split=str.split("+");
				 			key=split[0];
				 			value=parseInt(split[1]);
				 		}else if(contains(str,"-")){
				 			split=str.split("-");
				 			key=split[0];
				 			value=parseInt(split[1])*-1;
				 		}else{
				 			return "continue";
				 		}
				 		
				 		allValues.outcomes[type][key]=value;
				 	});
			 	}
			 	
			 	var outcomeSuccessStr=paramsArr[1].split("=")[1];
			 	registerOutcomes(outcomeSuccessStr,"success");
			 	
			 	var outcomeFailStr=paramsArr[2].split("=")[1];
			 	registerOutcomes(outcomeFailStr,"fail");
			 	
//		 	}else{
//			 	allValues=parseJSON(dataAllValuesStr);
//		 	}

		 	var $fail=$this.find(".fail");
		 	var $success=$this.find(".success");
		 	var previousResult=getStringFromStorage(allValues.key+".diceTest.result");
		 	if(previousResult==="success"){
		 		$fail.hide();
		 	}else if(previousResult==="fail"){
		 		$success.hide();
		 	}else{
		 		$success.hide();
		 		$fail.hide();
		 	}
		 	
		 	var $commandDice=jQuery("[id='"+allValues.key+"'].spec");

		 	if(!$commandDice.length){
		 		return;
		 	}
		 	
		 	// UI elements :
		 	var $display=$this.find(".display");
		 	if(!$display.length){
		 		
		 		var $title=jQuery("<div class='title'></div>");
		 		$this.prepend($title);
		 		$title.html(allValues.title);

		 		$display=jQuery("<div class='display'></div>");
		 		$this.append($display);
		 		
		 		var $diceTestButton=jQuery("<button>Dice test!</button>");
		 		if(!previousResult){
		 			$display.append($diceTestButton);
		 		}

		 		$diceTestButton.click(function(){
		 			var $diceTestButtonLocal=jQuery(this);
		 			$diceTestButtonLocal.hide();
		 			
		 			var $container=$diceTestButtonLocal.parent().parent();
		 			
		 			var $commandDiceLocal=jQuery("[id='"+allValues.key+"'].spec");
		 			
		 			$commandDiceLocal.find("button.dice").click();
		 			var result=$commandDiceLocal.find(".diceLabel").attr("data-result");
		 			
		 			if(result==="success"){
		 				$container.find(".success").show();
		 			}else{
		 				$container.find(".fail").show();
		 			}
		 					
		 			foreach(allValues.outcomes[result],(buff,key)=>{
			 			
		 				var oldValueStr=getStringFromStorage(key);

		 				
		 				if(!oldValueStr)	return "continue";
		 				var newValue=parseInt(oldValueStr)+buff;
		 				
			 			
		 				// TODO : FIXME : On today, it only works for amount-type variables.

		 				storeString(key,newValue);
		 				var $container=jQuery("[id='"+key+"'].amount");
			 			$container.find(".display").remove();
			 			$container.find(".addSubstract").remove();
			 			initAmount($container);
		 				
		 			});
		 			
		 			// Finally, we store the dice result, to avoid that a simple page refresh
		 			// allows to re-roll the dice several times until desired result :
		 			storeString(allValues.key+".diceTest.result",result);
		 			
		 			
		 		});
		 		
		 	}
	};
	$rpg.closest(".diceTest").each(function(){
		initDiceTest(jQuery(this));
	});
	
	
	
	// Single action :
	var initAction=function($this){
		 
	 	var $params=$this.find(".params");
	 	if(!$params.length)	return;
	 	var params=$params.html();
 		$params.hide();
	 	
	 	var allValues={};
	 	
 		var paramsArr=contains(params,":")?params.split(":"):[params];
 		
	 	var titleStr=paramsArr[0].split(".");
	 	allValues.gameKey=titleStr[0];
	 	allValues.title=titleStr[1];
	 	allValues.key=allValues.gameKey+"."+allValues.title;
	 	
	 	var configStr=$this.attr("data-config");
	 	if(configStr){
	 		allValues.config=parseJSON(configStr);
	 	}else{
	 		allValues.config={};
	 	}
	 	
	 	allValues.outcomes={};
	 	var registerOutcomes=function(outcomeStr){
	 		
	 		allValues.outcomes={};
		 	foreachSplit(outcomeStr,"," ,(str)=>{
		 		var key;
		 		var value;
		 		var split;
		 		if(contains(str,"+")){
		 			split=str.split("+");
		 			key=split[0];
		 			value=parseInt(split[1]);
		 		}else if(contains(str,"-")){
		 			split=str.split("-");
		 			key=split[0];
		 			value=parseInt(split[1])*-1;
		 		}else{
		 			return "continue";
		 		}
		 		
		 		allValues.outcomes[key]=value;
		 	});
	 	}
		 
	 	var outcomesStr=paramsArr[1];
	 	registerOutcomes(outcomesStr);


	 	var $panel=$this.find(".panel");
	 	var previousResult=getStringFromStorage(allValues.key+".action.triggered");
	 	if(previousResult!=="true"){
	 		$panel.hide();
	 	}
	 	
	 	// UI elements :
	 	var $display=$this.find(".display");
	 	if(!$display.length){
	 		
	 		var $title=jQuery("<div class='title'></div>");
	 		$this.prepend($title);
	 		$title.html(allValues.title);

	 		$display=jQuery("<div class='display'></div>");
	 		$this.append($display);
	 		
	 		var $actionButton=jQuery("<button>Action!</button>");
	 		if(!previousResult){
	 			$display.append($actionButton);
	 		}


	 		$actionButton.click(function(){
	 			var $actionButtonLocal=jQuery(this);
	 			$actionButtonLocal.hide();
	 			
	 			var $container=$actionButtonLocal.parent().parent();
	 			
 				$container.find(".panel").show();
	 			
 				
	 			foreach(allValues.outcomes,(buff,key)=>{
		 			
	 				var oldValueStr=getStringFromStorage(key);

	 				
	 				if(!oldValueStr)	return "continue";
	 				var newValue=parseInt(oldValueStr)+buff;
	 				
		 			
	 				// TODO : FIXME : On today, it only works for amount-type variables.

	 				storeString(key,newValue);
	 				var $container=jQuery("[id='"+key+"'].amount");
		 			$container.find(".display").remove();
		 			$container.find(".addSubstract").remove();
		 			initAmount($container);
	 				
	 			});
	 			
	 			// Finally, we store the dice result, to avoid that a simple page refresh
	 			// allows to re-roll the dice several times until desired result :
	 			storeString(allValues.key+".action.triggered","true");
	 			
	 			
	 		});
	 		

	 	}

		 	
	};
	$rpg.closest(".action").each(function(){
		initAction(jQuery(this));
	});
	
	
	
}



//================= aotra lakile (means «key» in malagasy) sub-component =================
// encryption provider & management
//========================================================================================

const RANDOM_PREFIX_SEPARATOR="@@@RANDOM_PREFIX_SEPARATOR@@@";
function calculateEncryptedContentCouples(containerDiv,password){
	var encryptedContentsCouples=[];
	
	const $elementsToEncrypt=jQuery(containerDiv).find(">.cryptographied[data-encrypted=false]").each(function(index){
		var $this=jQuery(this);
		var thisDiv=this;
		var contentsCouple={};

		contentsCouple.oldContent=thisDiv.outerHTML;
		
		$this.attr("data-encrypted",true);
		
		
		var contentToEncrypt=thisDiv.innerHTML;
		
		var signature=getHashedString(contentToEncrypt);
		$this.attr("data-signature",signature);
		
		
		contentToEncrypt=	generateRandomString(key.length)+RANDOM_PREFIX_SEPARATOR+contentToEncrypt;
		
		var clearContent=contentToEncrypt;
		thisDiv.innerHTML=encryptContent(clearContent,password);
		
		contentsCouple.newContent=thisDiv.outerHTML;
		
		encryptedContentsCouples.push(contentsCouple);
	});
	
	return encryptedContentsCouples;
}


function encryptContent(str,password){
	return encrypt(str,password,"aes xor");
}

function decryptContent(str,password){
	var result=decrypt(str,password,"aes xor");
	if(contains(result,RANDOM_PREFIX_SEPARATOR)){
		return result.split(RANDOM_PREFIX_SEPARATOR)[1]; 
	}
	return result;

}




//================= aotra alphabet MDR alphabet (Monolinear Delta-Recognizable alphabet ) password =================
// calligraphic password component
//========================================================================================

function addPasswordVisibleButton($password){

	var $parent=$password.parent();
	var $visibleTogglerPassword=$parent.find(".visibleToggler-password");
	
//	if(!$password.attr("data-uuid")) $password.attr("data-uuid",getUniqueIdWithDate(12));
	
	if(!$visibleTogglerPassword.length){
		
		$visibleTogglerPassword=jQuery("<button>"+i18n({"fr":"Montrer","en":"Show"})+"</button>");
		$visibleTogglerPassword.insertAfter($password);
		$visibleTogglerPassword.addClass("visibleTogglerPassword");

		$visibleTogglerPassword.on("click",function(event){
			var $this=jQuery(this);
			
			var $parentLocal=$this.parent();
			var $visibleTogglerPasswordDisplayLocal=$parentLocal.find(".visibleTogglerPasswordDisplay");
			
			if(!$this.attr("data-show")){
				$this.attr("data-show",true);
				$this.html(i18n({"fr":"Cacher","en":"Hide"}));
				
				$visibleTogglerPasswordDisplayLocal.css("visibility","visible");
			}else{
				$this.attr("data-show",null);
				$this.html(i18n({"fr":"Montrer","en":"Show"}));
				
				$visibleTogglerPasswordDisplayLocal.css("visibility","hidden");
			}
		});

		$visibleTogglerPasswordDisplay=jQuery("<div></div>");
		$visibleTogglerPasswordDisplay.insertBefore($password);
		$visibleTogglerPasswordDisplay.css("visibility","hidden");
		$visibleTogglerPasswordDisplay.css("height","22px");
		$visibleTogglerPasswordDisplay.addClass("visibleTogglerPasswordDisplay");

		$password.on("change paste keyup",function(event){
			var $this=jQuery(this);
			
			var $parentLocal=$this.parent();
			var $visibleTogglerPasswordDisplayLocal=$parentLocal.find(".visibleTogglerPasswordDisplay");
			
			$visibleTogglerPasswordDisplayLocal.html($this.val());
		});
		
//		// DBG
//		console.log("$visibleTogglerPassword.onclick",$visibleTogglerPassword.onclick);
		
	}
	
}


function getRelativeMouseCoords(event,$element){
	var offset=$element.offset();
	var mouseX = Math.floor(event.pageX - offset.left);
	var mouseY = Math.floor(event.pageY - offset.top);
	return {x:mouseX, y:mouseY};
}


function deltaAnalysis($this,mouseCoords,tolerance,
	width,divisionsX,height,divisionsY,
	/*OPTIONAL*/onTileChange,/*OPTIONAL*/onInflexionChange,
	/*OPTIONAL*/oldValuesCoords={},/*OPTIONAL*/oldValuesSigns={},
	/*OPTIONAL*/sequenceCharacters={PLUS:"+",MINUS:"-",X:"x",Y:"y"}){
	
	const tileSizeW=Math.trunc(width/divisionsX);
	const tileSizeH=Math.trunc(height/divisionsY);
	
	
	var result="";
	
	// X :
	var isInflexionX=false;
	var mouseX = mouseCoords.x;
	if(oldValuesCoords.x){
		
		var oldX=oldValuesCoords.x;
		var deltaX=mouseX-oldX;
		var deltaXSign=deltaX==0 ? 0:(Math.abs(deltaX)<=tolerance ? 0:(deltaX<0?-1:1));
		
		// DELTAS DELTAS :
		var deltaDeltaXSign=null;
		if(!oldValuesSigns.x){
			oldValuesSigns.x=deltaXSign;
			deltaDeltaXSign=deltaXSign;
		}else{
			var oldSignX=oldValuesSigns.x;
			if(deltaXSign!==0 && oldSignX!==0 && deltaXSign!=oldSignX){
				deltaDeltaXSign=deltaXSign;
				oldValuesSigns.x=deltaXSign;
				isInflexionX=true;
			}
		}
		if(deltaDeltaXSign){
	//		// DBG
	//		console.log("deltaDeltaXSign:"+deltaDeltaXSign);
	
			result+=
				((deltaDeltaXSign<0?sequenceCharacters.MINUS:sequenceCharacters.PLUS)+sequenceCharacters.X);
	
		}
	
		
		// When we change of tile :
		if(onTileChange && Math.trunc(mouseX/tileSizeW)!==Math.trunc(oldValuesCoords.x/tileSizeW)){
			onTileChange();
		}
	}
	
	oldValuesCoords.x=mouseX;
	// End X.
	
	
	// Y :
	var isInflexionY=false;
	var mouseY = mouseCoords.y;
	if(oldValuesCoords.y){
		
		var oldY=oldValuesCoords.y;
		
		
		var deltaY=mouseY-oldY;
		var deltaYSign=deltaY==0 ? 0:(Math.abs(deltaY)<=tolerance ? 0:(deltaY<0?-1:1));
		
		// DELTAS DELTAS :
		var deltaDeltaYSign=null;
		if(!oldValuesSigns.y){
			oldValuesSigns.y=deltaYSign;
			deltaDeltaYSign=deltaYSign;
		}else{
			var oldSignY=oldValuesSigns.y;
			if(deltaYSign!==0 && oldSignY!==0 && deltaYSign!=oldSignY){
				deltaDeltaYSign=deltaYSign;
				oldValuesSigns.y=deltaYSign;
				isInflexionY=true;
			}
		}
		if(deltaDeltaYSign){
	//		// DBG
	//		console.log("deltaDeltaYSign:"+deltaDeltaYSign);
			
			result+=
				((deltaDeltaYSign<0?sequenceCharacters.MINUS:sequenceCharacters.PLUS)+sequenceCharacters.Y);
	
		}
	
		// When we change of tile :
		if(onTileChange && Math.trunc(mouseY/tileSizeH)!==Math.trunc(oldValuesCoords.y/tileSizeH)){
			onTileChange();
		}
	}
	oldValuesCoords.y=mouseY;
	// End Y.
	
	// If we have detected an inflexion :
	if(onInflexionChange && isInflexionX && isInflexionY){
		onInflexionChange(isInflexionX,isInflexionY);
	}
	
	
	return result;

}







function initCalligraphicPassword($password,
	/*OPTIONAL*/width=300,/*OPTIONAL*/height=300,
	/*OPTIONAL*/divisionsX=1,/*OPTIONAL*/divisionsY=1,
	/*OPTIONAL*/defaultTolerance=5){

	const SEQUENCE_CHARACTERS={PLUS:"+",MINUS:"-",X:"x",Y:"y"};
	
	var $parent=$password.parent();
	
//	if(!$password.attr("data-uuid")) $password.attr("data-uuid",getUniqueIdWithDate(12));
	
	var $calligraphicPassword=$parent.find(".calligraphicPassword");
	if(!$calligraphicPassword.length){

		$calligraphicPassword=jQuery("<canvas></canvas>");
		$calligraphicPassword.insertAfter($password);
		$calligraphicPassword.width(width);
		$calligraphicPassword.height(height);
		$calligraphicPassword.css("width",width+"px");
		$calligraphicPassword.css("height",height+"px");
		$calligraphicPassword.css("border","solid 1px");
		$calligraphicPassword.css("display","block");
		$calligraphicPassword.addClass("calligraphicPassword");
		$calligraphicPassword.hide();
		
		$calligraphicPasswordToggler=jQuery("<label><input type='checkbox'></input>"+i18n({"fr":"Calligraphique","en":"Calligraphic"})+"</label>");
		$calligraphicPasswordToggler.insertAfter($password);
		$calligraphicPasswordToggler.find("input[type='checkbox']")
			.on("change",function(event){
			var $this=jQuery(this);
			
			var isChecked=$this.prop("checked");
			if(isChecked)	$calligraphicPassword.show();
			else					$calligraphicPassword.hide();
		});

		
//		var ctx=$calligraphicPassword.get(0).getContext("2d");
//		ctx.save();
//		ctx.beginPath();
//		// To correct the most stupid canvas bug ever :
////		ctx.scale(.75,.375);
//		
//		for(var i=1;i<divisionsX;i++){
//			var xLocal=tileSizeW*i;
//			ctx.moveTo(xLocal, 0);
//			ctx.lineTo(xLocal, height);
//		}
//		for(var i=1;i<divisionsY;i++){
//			var yLocal=tileSizeH*i;
//			ctx.moveTo(0,yLocal);
//			ctx.lineTo(width,yLocal);
//		}
//		
//		ctx.restore();
//		ctx.stroke();
		
		
		
		var encodeSequence=function(sequenceStr){
			
			const VALUES=[0,1,2,3,4];
			
			var result=isNumber(VALUES[0])?0:"";
			
			for(var i=0;i<sequenceStr.length;i+=2){
				
				var str=sequenceStr.charAt(i)+sequenceStr.charAt(i+1);
				
				if(str===SEQUENCE_CHARACTERS.PLUS+SEQUENCE_CHARACTERS.X){
					result+=VALUES[1];
				}else if(str===SEQUENCE_CHARACTERS.MINUS+SEQUENCE_CHARACTERS.X){
					result+=VALUES[2];
				}else if(str===SEQUENCE_CHARACTERS.PLUS+SEQUENCE_CHARACTERS.Y){
					result+=VALUES[3];
				}else if(str===SEQUENCE_CHARACTERS.MINUS+SEQUENCE_CHARACTERS.Y){
					result+=VALUES[4];
				}else{
					result+=VALUES[0];
				}
				
			}
			
			return isNumber(VALUES[0]) ? result.toString(16).toLowerCase() : result;
		};

		
		
		window.calligraphicPasswordRecorder={
			isStarted:false,
			tolerance:defaultTolerance,
//			ctx:ctx,
			oldValues:{coords:{},signs:{}},
			
			start:function(){
				if(window.calligraphicPasswordRecorder.isStarted)	return;
				window.calligraphicPasswordRecorder.isStarted=true;
				
				window.calligraphicPasswordRecorder.oldValues.coords.x=null;
				window.calligraphicPasswordRecorder.oldValues.coords.y=null;
				
				window.calligraphicPasswordRecorder.oldValues.signs.x=null;
				window.calligraphicPasswordRecorder.oldValues.signs.y=null;
				window.calligraphicPasswordRecorder.sequence="";
			},
			
			stop:function(){
				if(!window.calligraphicPasswordRecorder.isStarted)	return;
				window.calligraphicPasswordRecorder.isStarted=false;

				if(empty(window.calligraphicPasswordRecorder.sequence))	return;

				
				var seq=encodeSequence(window.calligraphicPasswordRecorder.sequence);
				
//				// DBG
//				console.log("encodedSequence",seq);
				
				$password.val($password.val()+seq);
				
				$password.trigger("change");
				
			},
			
		};
		

		var doRecordStep=function(event){
			
			if(!window.calligraphicPasswordRecorder.isStarted)	return;

			var $this=jQuery(this);
			
			var mouseCoords=getRelativeMouseCoords(event,$this);
			var tolerance=window.calligraphicPasswordRecorder.tolerance;
			
			var oldValues=window.calligraphicPasswordRecorder.oldValues;
			
			var onTileChange=function(){
				window.calligraphicPasswordRecorder.stop();
				window.calligraphicPasswordRecorder.start();
			}

			var partialSequence=deltaAnalysis($this,
				mouseCoords,tolerance,
				width,divisionsX,height,divisionsY,
				onTileChange, null,
				oldValues.coords,oldValues.signs,
				SEQUENCE_CHARACTERS);
		
			window.calligraphicPasswordRecorder.sequence+=partialSequence;

			
//			ctx.save();
//			// The stupidity of this canvas bug is overwhelming :
////			window.calligraphicPasswordRecorder.ctx.scale(.75,.375);
//			window.calligraphicPasswordRecorder.ctx.fillRect(mouseX,mouseY,tolerance,tolerance);
//			ctx.restore();

		};
		
		
		
		
		var startRecording=window.calligraphicPasswordRecorder.start;
		var stopRecording=window.calligraphicPasswordRecorder.stop;
		
		$calligraphicPassword.mousedown(startRecording);
		$calligraphicPassword.mouseup(stopRecording);
		$calligraphicPassword.mouseleave(stopRecording);

		$calligraphicPassword.mousemove(doRecordStep);
		
		
	}
	
	
	
	
	
}






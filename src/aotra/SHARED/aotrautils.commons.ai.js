

/* ## Utility AI methods in a javascript, console (nodejs) server, or vanilla javascript with no browser environment.
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 *  
 */



// COMPATIBILITY browser javascript / nodejs environment :
if(typeof(window)==="undefined")	window=global;	



//=========================================================================
// GLOBAL CONSTANTS :

//=========================================================================




// AI management :



class OpenAIAPIClient{
	
	constructor(modelName, apiURL, agentRole, defaultPrompt){
		
  	//DBG
  	lognow(">>>>>>>>>>>>>>>>!!!!!!apiURL:",apiURL);
		
		// this.apiKey=apiKey;
		this.modelName=modelName;
		this.apiURL=apiURL;
		this.agentRole=agentRole;
		this.defaultPrompt=defaultPrompt;
	}

	async getAnswer(AIAPIKey, additionalPrompt, defaultPromptParameters={}){
		
		const PARAMETERS_DELIMITERS=["<",">"];
		
		let newDefaultPrompt=this.defaultPrompt;
		foreach(defaultPromptParameters, (value,key)=>{
			const regexp=new RegExp(PARAMETERS_DELIMITERS[0]+key+newDefaultPrompt[1] ,"gim");
			newDefaultPrompt=newDefaultPrompt.replace(regexp,value);
		});
		
		const self=this;
	  const messages = [
	   { role: "system", content: this.agentRole },
	   { role: "user", content: (newDefaultPrompt+ "\n" + additionalPrompt) },
	  ];
	  
  	//DBG
  	lognow("------------self.apiURL:",self.apiURL);
		
		const result=await this.launchRequest(AIAPIKey, messages);
		
		// DBG
		console.log("! RESULT text :",result);
		
		return result;
	}
	
	
	/*private*/launchRequest(AIAPIKey, messages){
				
		// DBG
		lognow("! launchRequest messages :",messages);
		
		
		const headers={
			"Authorization": "Bearer "+AIAPIKey,
			"Content-Type": "application/json",
		};
		const parameters={
			model:this.modelName,
			messages:messages,
		};

		return new Promise((resolve,reject)=>{
			performHTTPRequest(this.apiURL,"POST",headers,parameters,true).then((responseObj)=>{
				const responseData=responseObj.responseData;
				const data=responseData;
				
		 	 	// DBG
		 	  console.log("~~~~~~~~~~~data :",data);
		 	  
				if(data.error){
					const error=data.error;
					// TRACE
					console.error("Error:", error);
					reject(new Error(`${error}`));
				}
		
				const assistantReply = data.choices[0].message.content;
			 	// DBG
			  console.log("~~~~~~~~~~~assistantReply :",assistantReply);
			  
				resolve(assistantReply);
			}).catch(error => {
				// TRACE
				console.error("Error:", error);
				reject(error);
			});
		});
		
		
		
		
	}
	
	

	
}


// Nodejs compatibility :
getOpenAIAPIClient=(modelName, apiURL, agentRole, defaultPrompt)=>{
	return new OpenAIAPIClient(modelName, apiURL, agentRole, defaultPrompt);
};





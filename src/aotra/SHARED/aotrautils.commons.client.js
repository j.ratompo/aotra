/* ## Utility global methods in a browser (htmljs) client environment.
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 *  
 */


// Node dependencies :
if(typeof(require)!="undefined" && typeof(sjcl)=="undefined" )	sjcl = require("sjcl");


// COMPATIBILITY browser javascript / nodejs environment :
if(typeof(window)==="undefined")	window=global;	




// GLOBAL CONSTANTS :

const APOSTROPHE="’";
PERFORM_TESTS_ON_LIBRARY=false;

// CAUTION ! Apparently nodejs ECMAscript does not do lazy evaluation when it's for throwing declaraion exceptions (it's logical, when you think a second about it..)
function isUserMediaAvailable(){
	if(typeof(navigator)===undefined)	return false;
	return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia && navigator.mediaDevices.enumerateDevices);
}


// Dependances-optionalizing mechanisms :
if(typeof aotest === "undefined"){
	// If aotest library is not available, then we replace aotest calls with simple tested function calls :
	aotest=function(testcase,testedFunction){ return testedFunction; };
	// TRACE IF NEEDED
	if(PERFORM_TESTS_ON_LIBRARY)	console.log("WARN : aotest library not found, deactivating aotest unit test abilities.");
}
if(typeof monitorProgression === "undefined"){
	monitorProgression=function(progressionTotalAmount,monitoredFunction){ return monitoredFunction; };
}



// -------------------------------------------------------------------------------------------






// NOT AOTESTABLE !

//Works, but doesn't compile under PhoneGap...:
//function getURLParameter(nameParam){
//var name=encodeURIComponent(nameParam);
//return decodeURIComponent(
//(
//new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)' )
//.exec(window.location.search)
//|| [,""]
//)[1]
//.replace(/\+/g, '%20')
//)||null;
//}

//...so we replace it with another function that is supposed to do the same
//thing :
function getURLParameter(nameParam){
	var name=nameParam;
	if (name=new RegExp("[?&]" + encodeURIComponent(name) + "=([^&]*)").exec(location.search))
		return decodeURIComponent(name[1]);
	return null;
}




// A little method to help i18ning aotra user interface :
const I18N_REPOSITORY_VARIABLE_NAME="I18N_KEYS";
function i18n(languagesAndStrings, langParam=null){
	
	// ******************* CONFIG ******************* 
	const PREFERED_LANGUAGE="fr"; // My Quebecer wife (love on her for ever and beyond) forced me to set this...(I'm French.)
	
	const NO_TRANSLATION_LABEL="NO TRANSLATION FOUND";
	
	const ADD_SYSTEMATICALLY_TO_REPOSITORY=true;
	const MAX_CHARACTERS_FOR_TRANSLATION_KEY=64; // Really if you have more than 64 characters in commons, then it really should be the same string !

	// ***************** END CONFIG ***************** 

	
	if(!languagesAndStrings){
		// TRACE
		console.log("ERROR : No parameter for i18n provided!",languagesAndStrings);
		return NO_TRANSLATION_LABEL;
	}
	
	let lang=null;
	if(langParam){
		lang=langParam;
	}else{
		// Parameters priority :
		if(window){
			if(window.getURLParameter){
				lang=nonull(window.getURLParameter("language"), window.getURLParameter("lang"));
			}
			if(!lang && (window.language || window.lang)){
				lang=nonull(window.language, window.lang);
			}
		}else if(global){	// (NodeJS compatibility)
			if(global.getURLParameter){
				lang=nonull(global.getURLParameter("language"), global.getURLParameter("lang"));
			}
			if(!lang && (global.language || global.lang)){
				lang=nonull(global.language, global.lang);
			}
		}
		lang=nonull(lang, PREFERED_LANGUAGE);
	}
	
	if(contains(["capitalize","append"],lang)){
		// TRACE
		console.log("WARN : Language parameter has for value a reserved attribute keyword, using default language «"+PREFERED_LANGUAGE+"»");
		lang=PREFERED_LANGUAGE;
	}
	
	
	let result=null;
	
	
	if(!isString(languagesAndStrings)){ // Case direct i18n translation, with no translation keys :
		
		result=languagesAndStrings[lang];
	
		if (!result){
			// TRACE
			console.log("ERROR : No label found for language «"+lang+"»!",languagesAndStrings);
		}
	
		if (!result){
			for(key in languagesAndStrings){
				if (!languagesAndStrings.hasOwnProperty(key))
					continue;
				// We take the first that we find, in the other languages:
				result=languagesAndStrings[key];
				break;
			}
		}

		if (!result)	return NO_TRANSLATION_LABEL;
		
		
		// We add this «rogue» usage of i18n to the whole repository at all useful ends :
		if(ADD_SYSTEMATICALLY_TO_REPOSITORY){
//		let declarable={};
			let maxLength=Math.min(result.length, MAX_CHARACTERS_FOR_TRANSLATION_KEY);
			let key=toConvention("camel", result.substring(0,maxLength).toLowerCase() );
//		let calculatedKey=radicalKey+"_"+getUUID("short");
//		declarable[calculatedKey]=languagesAndStrings;
			i18n.declareSingleEntry(key, languagesAndStrings);
		}

		// Eventual post-treatments :
		if(languagesAndStrings.capitalize)	result=capitalize(result);
		if(languagesAndStrings.append)			result+=languagesAndStrings.append;

		return result;
	}
	// else // Case we use translation keys : (here, languagesAndStrings parameter holds just the wished translation key)

	
	let repository=(window?window:global)[I18N_REPOSITORY_VARIABLE_NAME];
	if(!repository){
		// TRACE
		console.log("ERROR : No repository variable found for repository «"+I18N_REPOSITORY_VARIABLE_NAME+"»!");
		return NO_TRANSLATION_LABEL;
	}
	
	// OLD (ie. splitted format :)
//		let repositoryForLang=repository[lang];
//		if (!repositoryForLang){
//			// TRACE
//			console.log("ERROR : No translation found for language «"+lang+"»!",languagesAndStrings);
//			return NO_TRANSLATION_LABEL;
//		}
//		result=repositoryForLang[languagesAndStrings]; // Here, languagesAndStrings is just a bundle key.
//		if (!result)	return NO_TRANSLATION_LABEL+" (for key «"+languagesAndStrings+"»)";
	
	let languagesAndStringsForTranslationKey=repository[languagesAndStrings];
	if (!languagesAndStringsForTranslationKey){
		// SILENT ERROR :
//		// TRACE
//		console.log("ERROR : No translation found for key «"+languagesAndStrings+"» for language «"+lang+"»!");
//	NO, OLD : return NO_TRANSLATION_LABEL;
		return languagesAndStrings;
	}
	result=i18n(languagesAndStringsForTranslationKey, lang); // Since we use the same format !
	
	return result;
};


i18n.declareSingleEntry=function(key, languagesAndStringsEntry){
	let newRepository;
	let foundRepository=(window?window:global)[I18N_REPOSITORY_VARIABLE_NAME];
	if(!foundRepository){
		// TRACE
		console.log("ERROR : No repository variable found for repository «"+I18N_REPOSITORY_VARIABLE_NAME+"»!");
		return;
	}
	
	let foundEntryForKey=foundRepository[key];
	if(!foundEntryForKey){
		foundRepository[key]=languagesAndStringsEntry;
//	}else{
//		// TRACE
//		console.log("WARN : Duplicate entry for translation key «"+key+"», keeping only the first declared :",foundEntryForKey);
	}
	
};


//USAGE :
//Example :
//i18n.declare({
//	"commencer":{"fr":"Commencer","en":"Start"}
//});
i18n.declare=function(repositoryInfos){
	let newRepository;
	let foundRepository=(window?window:global)[I18N_REPOSITORY_VARIABLE_NAME];
	if(foundRepository){
//		// TRACE
//		console.log("WARN : A repository has already been declared, merging.",foundRepository);
		newRepository=merge(foundRepository, repositoryInfos);
	}else{
		newRepository=repositoryInfos;
	}
	(window?window:global)[I18N_REPOSITORY_VARIABLE_NAME]=newRepository;
};

// USAGE :
// Example :
//i18n.declareSplitted({
//	"fr":{
//		"commencer":"Commencer",
//	},
//	"en":{
//		"commencer":"Start",
//	},
//});
i18n.declareSplitted=function(repositoryInfos){
	let nonSplittedFormat={};
	
	foreach(repositoryInfos,(allKeys, lang)=>{
		foreach(allKeys,(text, key)=>{
			
			let foundKeyInfos=nonSplittedFormat[key];
			if(!foundKeyInfos){
				nonSplittedFormat[key]={};
				foundKeyInfos=nonSplittedFormat[key];
			}
			
			let foundTextForLanguage=foundKeyInfos[lang];
			if(foundTextForLanguage){
				// TRACE
				console.log("WARN : Duplicate entry for translation key «"+key+"» for language «"+lang+"», keeping only the first declared :",foundTextForLanguage);
			}else{
				foundKeyInfos[lang]=text;
			}
			
		});
	});

	// Then we use the standard declaration method :	
	i18n.declare(nonSplittedFormat);
};

// For generating languages files helping purposes :
i18n.displayAll=function(displaySingleEntriesCalls=false){
	let foundRepository=(window?window:global)[I18N_REPOSITORY_VARIABLE_NAME];
	if(!foundRepository){
		// TRACE
		console.log("ERROR : No repository found under the variable name «"+I18N_REPOSITORY_VARIABLE_NAME+"»");
	}else{
		console.log("TRACE : Variable under the name of «"+I18N_REPOSITORY_VARIABLE_NAME+"»:", foundRepository);
		console.log("\n\n");

		let message="";
		foreach(foundRepository, (entry, entryKey)=>{
			let normalizedEntry={};
			if(entry.capitalize)	normalizedEntry.capitalize=entry.capitalize;
			if(entry.append)			normalizedEntry.append=entry.append;
			foreach(entry,(e,k)=>{
				normalizedEntry[k]=e;
			}
			,(i,key)=>{ return (!contains(["append","capitalize"],key));	}
			,(i1,i2)=>{ return (i1.key==i2.key?0:(i1.key=="fr"?-1:1));/*(Sorry, French always comes first !)*/}
			);
			let entryStr=stringifyObject(normalizedEntry);
			message+="\""+entryKey+"\":"+entryStr+",\n";
			if(displaySingleEntriesCalls)	message+="// i18n("+entryStr+");\n";
		});
		
		console.log("\n"+message);
		
	}
}





// -------------------------------------------------------------------------------------------

// Mouse management :

// Code from (www.adomas.org/javascript-mouse-wheel)
function initMouseWheel(handle,/* OPTIONAL */filterFunction){

	var wheel=function(event){

		if (filterFunction && !filterFunction(event.target))
			return;

		var delta=0;
		if (!event)
			event=window.event;
		if (event.wheelDelta){
			delta=event.wheelDelta / 120;
		} else if (event.detail){
			delta=-event.detail / 3;
		}
		if (delta)
			handle(delta);
		if (event.preventDefault)
			event.preventDefault();
		event.returnValue=false;
	};

	/* Initialization code. */
	if (window.addEventListener){
		window.addEventListener('DOMMouseScroll', wheel, { passive: false });
		window.addEventListener('wheel', wheel, { passive: false });
	}else{
		window.onmousewheel=document.onmousewheel=wheel;
	}

}


// (DOES NOT SEEM TO WORK)
//function getPressedButton(eventParam){
//  let event=eventParam || window.event;
//  if ("buttons" in event){
//      return event.buttons;
//  }
//  let button=event.which || event.button;
//  return button;
//}


function getPressedKey(eventParam){
  let event=nonull(eventParam, window.event);
  let keynum;
  if(window.event){				// IE                    
    keynum=event.keyCode;
  } else if(event.which){ 	// Netscape/Firefox/Opera                   
    keynum=event.which;
  }
  if(!keynum)	return null;
  switch (keynum){
		case 37: return "left"; break;
		case 38: return "up"; break;
		case 39: return "right"; break;
		case 40: return "down"; break;
		default: return String.fromCharCode(keynum).toLowerCase(); break;
  }
  return null;
}


// -------------------------------------------------------------------------------------------

// - Fonction needed for the components above :
function getWindowSize(coordName){

	const d=document;
	const e=d.documentElement;
	const g=d.getElementsByTagName("body")[0];
  
	
	if (coordName === "x" || coordName === "width")
		return window.innerWidth || e.clientWidth || g.clientWidth;
	if (coordName === "y" || coordName === "height")
		return window.innerHeight|| e.clientHeight|| g.clientHeight;
		
	if (coordName === "sx" || coordName === "screenWidth")
		return window.screen?window.screen.width:getWindowSize("x");
	if (coordName === "sy" || coordName === "screenHeight")
		return window.screen?window.screen.height:getWindowSize("y");
		
	return 0;
}

function bringElementSelectedToPosition(elementSlct, x, y,/* OPTIONAL */delay,/* OPTIONAL */onEndMethod){
	if (empty(elementSlct.get()))
		return;
	elementSlct.animate({
		left : x + "px",
		top : y + "px"
	}, (delay | 1000), onEndMethod);
}

function getSelectedText(textComponent){

	var selectedText="";
	if (textComponent.tagName.toLowerCase()=="textarea"){

		// IE version
		if (document.selection != undefined){
			textComponent.focus();
			var sel=document.selection.createRange();
			selectedText=sel.text;
		} else if (textComponent.selectionStart != undefined){
			// Mozilla version
			var startPos=textComponent.selectionStart;
			var endPos=textComponent.selectionEnd;
			selectedText=textComponent.value.substring(startPos, endPos);
		}
	} else {
		// Mozilla version
		if (typeof window.getSelection != "undefined"){
			var sel=window.getSelection();
			if (sel.rangeCount){
				var container=document.createElement("div");
				for(var i=0, len=sel.rangeCount; i<len; ++i){
					container.appendChild(sel.getRangeAt(i).cloneContents());
				}
				selectedText=container.innerHTML;
			}

		} else if (typeof document.selection != "undefined"){
			// IE version
			if (document.selection.type=="Text"){
				selectedText=document.selection.createRange().htmlText;
			}
		}
	}

	return selectedText;
}

function renderSimpleSelect(selectId, listArray, style,/* OPTIONAL */defaultSelectedIndex){
	var html="";

	html+="<select id=\"" + selectId + "\" style=\"" + style + "\">";

	for(var i=0; i<listArray.length; i++){
		var item=listArray[i];

		html+="<option value='" + item + "' ";
		if (i === defaultSelectedIndex)
			html+=" selected ";
		html+=">";
		html+=item;
		html+="</option>";
	}

	html+="</select>";

	return html;
}

function setSelectedValueInSelect(selectId, selectedValue){
	var selectElement=document.getElementById(selectId);
	if (!selectElement)
		return;

	for(var i=0; i<selectElement.options.length; i++){
		var option=selectElement.options[i];
		// DOESN'T WORK :
		// if(option.value!==selectedValue) delete option.selected;
		// else option.selected="true";
		if (option.value === selectedValue){
			selectElement.selectedIndex=i;
			break;
		}
	}

}

function getSelectedValueInSelect(selectElementParam){
	var selectElement=selectElementParam;
	if (typeof selectElementParam === "string")
		selectElement=document.getElementById(selectElementParam);
	if (selectElement === null)
		return null;
	if (!selectElement.options)
		return null;
	var selectedOption=selectElement.options[selectElement.selectedIndex];
	if (!selectedOption)
		return "";
	return selectedOption.value ? selectedOption.value : "";
}

function getAsString(DOMElement
//	,/* OPTIONAL */docParam
	){
//	var doc=null;
//	if (docParam){
//		doc=docParam;
//	}	else{
//		doc=document;
//	}
//	

	var container=document.createElement("div");
	container.appendChild(DOMElement.cloneNode(true));
	var result=container.innerHTML;
	
	return result;
}

// CSS management :
cssStringToJQueryObject=aotest({
	name : "cssStringToJQueryObject",
	scenario1 : [ [ "test: property property2;test2:thing" ], function(result){
		return result.test && result.test === "property property2" && result.test2 && result.test2 === "thing";
	} ]
// This test is valid an all, but my js editor can't stand it for it contains
// comments-like string in it :
// ,scenario2:[["/*test: property property2;*/ test2:thing; /*test3:thing3*/;
// test4:thing4 thing44; "],function(result){
// return result.test2 && result.test2==="thing"
// && result.test4 && result.test4==="thing4 thing44"; }]

}, function(cssStr){
	var result=new Object();
	var split1=cssStr.split(";");
	for(var i=0; i<split1.length; i++){
		var split2=split1[i].split(":");
		if (split2.length != 2)
			continue;
		var name=split2[0].trim();
		var value=split2[1].trim();
		if (contains(name, "/*") || contains(value, "/*") || contains(value, "*/"))
			continue;
		result[name.replace("*/", "").trim()]=value;
	}
	return result;
}, !PERFORM_TESTS_ON_LIBRARY);

// HTML elements management :

// A function only used here, in the following method. :
function getElementById(idOrElement){
	var element=idOrElement;
	if (typeof element === "string")
		element=document.getElementById(element);
	if (element === null)
		return null;
	return element;
}

function setElementVisible(elementIdParam, visible){
	var element=getElementById(elementIdParam);
	if (element === null)	return null;
	if(visible)	element.style.display="unset";
	else				element.style.display="none";
	return element;
}

function setElementHidden(elementIdParam, hidden){
	var element=getElementById(elementIdParam);
	if (element === null)	return null;
	if(hidden)	element.style.opacity=0;
	else				element.style.opacity="unset";
	return element;
}



function setOnlyVisible(parentElement,element,visible){
	foreach(parentElement.children,(child)=>{
		if(child.id===element.id)	setElementVisible(child,visible);
		else											setElementVisible(child,!visible);
	});
};


function isElementVisible(elementIdParam){
	var element=getElementById(elementIdParam);
	if (element === null)
		return false;
	if (element.style){
		if (element.style.display && element.style.display === "none")
			return false;
		if (element.style.visibility && element.style.visibility === "hidden")
			return false;
		return true;
	}
	return true;
}

function appendAsFirstChildOf(element, parent){
	if (parent.firstChild)
		parent.insertBefore(element, parent.firstChild);
	else
		parent.appendChild(element);
}

function removeElementFromParent(element){
	if (!element)
		return;
	if (!element.parentNode)
		return;
	element.parentNode.removeChild(element);
}

function asyncPreloadImage(src, callback){
	var img=new Image();
	img.src=src;
	img.onload=function(){
		callback(img);
	};
}


function getAncestorsOrItselfContainsClassName(element, className){
	if (element.className && !nothing(element.className) && containsIgnoreCase(element.className, className))
		return element;
	var parent=element.parentNode;
	if (parent === null)
		return null;
	return getAncestorsOrItselfContainsClassName(parent, className);
}

// UNUSED (BUT PLEASE KEEP CODE, just in case jQuery animate could not be used)
function startAnimation(sourceValue, destinationValue, animationDurationMillis, doOnEachStepMethod,/* OPTIONAL */doOnComplete,/* OPTIONAL */stepsNumber){

	if (!stepsNumber || stepsNumber<0)
		stepsNumber=10;
	var deltaValues=destinationValue - sourceValue;

	// TODO : Add several different easing modes, actually only «linear» is
	// offered by default :
	var stepAmount=deltaValues / stepsNumber;
	var currentValue=sourceValue;
	var cntSteps=0;
	var pe=new PeriodicalExecuter(function(){

		if (stepsNumber <= cntSteps || (destinationValue<currentValue && 0<deltaValues) || (currentValue<destinationValue && deltaValues<0)){
			pe.stop();
			if (doOnComplete)
				doOnComplete();
			return;
		}

		currentValue+=stepAmount;
		doOnEachStepMethod(currentValue);
		cntSteps++;

	}, animationDurationMillis / (1000 * stepsNumber));

}

// UNUSED :
//function getAllHTMLElementsStartingWithId(startStr){
//	var elements=new Array();
//
//	var r=new RegExp(startStr + "_.*");
//	var els=document.getElementsByTagName("*");
//	for(var i=0; i<els.length; i++){
//		var e=els[i];
//		if (r.test(e.id))
//			elements.push(e);
//	}
//	return elements;
//}

// Compatibility management :

function isIE(){
	if(typeof(navigator)===undefined)	return false;
	if (typeof window.isIEVar === "undefined" || window.isIEVar === null){
		const myNav=navigator.userAgent.toLowerCase();
		window.isIEVar=(myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
	}
	return window.isIEVar;
}
function isChrome(){
	return typeof(navigator)!==undefined && containsIgnoreCase(navigator.userAgent, "chrome");
}
function isFirefox(){
	return typeof(navigator)!==undefined && containsIgnoreCase(navigator.userAgent, "mozilla");
}

function isDeviceMobile(){
// TODO : FIXME : DOES NOT WORK !
//	if (typeof window.isMobileVar === "undefined" || window.isMobileVar === null)
//		// Uses detectmobilebrowser.js external library (licensed under the
//		// conditions of the «Public domain : Unlicense»)
//		window.isMobileVar=isBrowserMobile();
//	return window.isMobileVar;
	if(typeof(navigator)===undefined)	return false;
  let result=false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) result=true;})(navigator.userAgent||navigator.vendor||window.opera);
  return result;
}


function addFireOnAnyKey(onPress,/* OPTIONAL */clickableElementParam){
	var clickableElement=document;
	if (clickableElementParam)
		clickableElement=clickableElementParam;
	clickableElement.addEventListener("keypress", function(eventParam){
		var event=eventParam || window.event;
		// NOT WORKING : it is supposed to be ESCAPE...:
		// if(event.keyCode!==27) return;
		if (!event.keyCode)
			return;
		onPress();
	});
}

// XML parsing management :
function parseXMLString(txt){
	if (window.DOMParser){
		parser=new DOMParser();
		xmlDoc=parser.parseFromString(txt, "text/xml");
	} else {
		// Internet Explorer
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async=false;
		xmlDoc.loadXML(txt);
	}
	return xmlDoc;
}





//// Converts a string like :
//function javascriptStringToObject(str){
//	var result=new Object();
//	// TODO : Develop...
//	return result;
//}

// String management :

// Strings


function getXMLElementAsString(element){
	var tmp=document.createElement("div");
	tmp.appendChild(element.cloneNode());
	return tmp.innerHTML;
}

function getArrayAsJSONString(sizable){

	if (typeof sizable === "object"){ // for classical and associative arrays

		if (sizable instanceof Array){
			var result="[";
			for(var i=0; i<sizable.length; i++){
				result+=(result.length <= 1 ? "" : ",") + i + ":" + '"' + sizable[i] + '"';
			}
			return result + "]";
		}

		// Actually an unsafe method to determine that variable «sizable» is not a
		// classical (ie. non-associative) array...:
		if (!isArray(sizable)){
			var result="{";
			for(key in sizable){
				if (!sizable.hasOwnProperty(key))
					continue;
				result+=(result.length <= 1 ? "" : ",") + '"' + key + '"' + ":" + '"' + sizable[key] + '"';
			}
			return result + "}";
		}
	}

	return "";
}

//Arrays management :

function getArrayFromJSONString(sizableStr){
	if (nothing(sizableStr))
		return new Array();

	try {
		return parseJSON(sizableStr);
	} catch (ex){
		// TRACE
		log("ERROR : Error parsing JSON. Details...:");
		log(ex);
	}
	return new Array();
}

// HTML strings management

function removeHTML(strParam,/* OPTIONAL-NULLABLE */replaceBreakLines,/* OPTIONAL */replacementForWhatIsRemoved){
	var str=strParam;

	if (replaceBreakLines)
		str=str.replace(/<\/h[1-9]\s*>/gim, "\n").replace(/<\/div\s*>/gim, "\n").replace(/<\/p\s*>/gim, "\n").replace(/<br\s*(\/)*\s*>/gim, "\n").replace(
				/\r\n/gim, "").replace(/\r/gim, "\n");

	var r=/<(?:.|\n)*?>/igm;
	str=str.replace(r, replacementForWhatIsRemoved ? replacementForWhatIsRemoved : "");

	str=decodeHTMLEntities(str);

	return str;
}

function decodeHTMLEntities(strParam){
	var str=strParam;

	// NOT WORKING :
	// str=jQuery("<textarea/>").text(str).html();

	str=str.replace(/&nbsp;/gim, " ").replace(/&gt;/gim, ">").replace(/&lt;/gim, "<").replace(/&amp;/gim, "&")
	// TODO : Add all html entities...
	;

	return str;
}

// NOT WORKING :
// var decodeHTMLEntities=(function(){
//	
// // This prevents any overhead from creating the object each time
// var element=document.createElement('div');
//
// function decodeHTMLEntitiesLocal(strParam){
// var str=strParam;
// if(str && typeof str === 'string'){
// // strip script/html tags
// str=str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
// str=str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
// element.innerHTML=str;
// str=element.textContent;
// element.textContent='';
// }
//
// return str;
// }
//
// return decodeHTMLEntitiesLocal;
// })();

// NOT WORKING :
// function keepOnlyHTML(str,/*OPTIONAL*/replacementForWhatIsRemoved){
// var r=/^(?!.*<(?:.|\n)*?>)/igm;
// return
// str.replace(r,replacementForWhatIsRemoved?replacementForWhatIsRemoved:"");
// }

function escapeHTML(str){
	var pre=document.createElement("pre");
	var text=document.createTextNode(str);
	pre.appendChild(text);
	var result=pre.innerHTML;
	return result;
}

function capitalize(str){
	if(nothing(str))	return "";
	return str.replace(/(?:^|\s)\S/m, firstChar => firstChar.toUpperCase()); // We replace only the first character.
}

function startsWithUpperCase(str){
	if(nothing(str))	return false;
	return str[0].toUpperCase()===str[0];
}

function getTextWordsExtract(textStrParam, wordsNumber,/* OPTIONAL */wordPositionOffset){

	if (wordsNumber <= 0)
		return "";

	var textStr=toOneSimplifiedLine(removeHTML(textStrParam));

	var split=textStr.split(" ");

	var result="";

	var i=0;
	if (wordPositionOffset && isNumber(wordPositionOffset) && wordPositionOffset > 0)
		i=wordPositionOffset;

	// One-character (not letter-like) sized words don't count :
	var split2=new Array();
	for(var j=0; j<split.length; j++){
		var s=split[j];
		if (s.length<1)
			continue;
		if (s.length==1 && new RegExp("[^\\wàâäéèêëìîïòôöùûüç]", "igm").test(s))
			continue;
		split2.push(s);
	}

	for(; i<split2.length && i<wordsNumber; i++){
		var s=split2[i];
		result+=(nothing(result) ? "" : " ") + s;
	}

	return result;
}


//==================== CLIENT Geometry ====================

Math.isInScreen=function(coords,size=null,center=null,totalWidth,totalHeight,camera=null){
	
	if(center==null)	center={x:"center",y:"center"};
	
	let x=coords.x;
	let y=coords.y;
	
	
	let cameraPositionX;
	let cameraPositionY;
	
	if(camera && camera.position){
	
		let cameraPosition=camera.position.getParentPositionOffsetted();
		cameraPositionX=cameraPosition.x;
		cameraPositionY=cameraPosition.y;
	
	}else{
		cameraPositionX=totalWidth*.5;
		cameraPositionY=totalHeight*.5;

	}
	
	
	
	
	let isInScreenX=false;
	{
		let min;
		let max;
		let minTotal;
		let maxTotal;
		
		let demiTotalWidth=Math.floor(totalWidth*.5);
		
		
		minTotal=cameraPositionX-demiTotalWidth;
		maxTotal=cameraPositionX+demiTotalWidth;
		
		let width=(!size?0:size.w);
		
		
		if(center.x==="center"){
			let demiWidth=Math.floor(width*.5);
			min=x-demiWidth;
			max=x+demiWidth;
		}else if(center.x==="right"){
			min=x-width;
			max=x;
		}else{
			// Case "left"
			min=x;
			max=x+width;
		}
		
//		//DBG
//		lognow("minTotal:"+minTotal+"[min:"+min+",max:"+max+"]maxTotal:"+maxTotal);
		
		// segment is *out* of screen if min and max are both less than MIN or min and max more than MAX
//OLD:	isInScreenX= !((min<minTotal && max<minTotal) || (maxTotal<min && maxTotal<max));
		isInScreenX= (min<maxTotal && minTotal<max);

	}
	
	
	
	let isInScreenY=false;
	{
		let min;
		let max;
		let minTotal;
		let maxTotal;
		
		let demiTotalHeight=Math.floor(totalHeight*.5);
		minTotal=cameraPositionY-demiTotalHeight;
		maxTotal=cameraPositionY+demiTotalHeight;
		
		let height=(!size?0:size.h);
		
		
		if(center.y==="center"){
			let demiHeight=Math.floor(height*.5);
			min=y-demiHeight;
			max=y+demiHeight;
		}else if(center.y==="bottom"){
			min=y-height;
			max=y;
		}else{
			// Case "top"
			min=y;
			max=y+height;
		}
		
		// segment is *out* of screen if min and max are both less than MIN or min and max more than MAX
		// OLD : isInScreenY= !((min<minTotal && max<minTotal) || (maxTotal<min && maxTotal<max));
		isInScreenY=(min<maxTotal && minTotal<max);

	}

	return isInScreenX && isInScreenY;
}

Math.getVariance=function(values){
	let min=Math.minInArray(values);
	let max=Math.maxInArray(values);
	return Math.abs(max - min);
};

// UNUSED
Math.getVariancePercentage=function(values, canonValue){
	var min=Math.minInArray(values);
	var max=Math.maxInArray(values);
	if (min === 0 || max === 0 || canonValue === 0 || (min === max))
		return 0;
	return Math.abs(max - min) / canonValue;
};

Math.getHashValueFromArray=function(values){
	var result=0;
	var oldVal=null;
	for(var i=0; i<values.length; i++){
		var val=values[i];
		if (oldVal)
			result+=oldVal - val;
		oldVal=val;
	}
	return result;
};

Math.getVariationsSchemeFromArray=function(values){
	var result="";
	var oldVal=null;
	for(var i=0; i<values.length; i++){
		var val=values[i];
		if (oldVal){
			if (oldVal === val)
				result+="0";
			if (oldVal<val)
				result+="+";
			else
				result+="-";
		}
		oldVal=val;
	}
	return result;
};

Math.getSimplifiedVariationsSchemeFromArray=function(values,/* OPTIONAL */thresholdParam){
	var result="";
	var threshold=1;
	if (thresholdParam)
		threshold=thresholdParam;
	// RLE-style compression :
	var bruteScheme=Math.getVariationsSchemeFromArray(values);
	var cnt=1;
	var oldVal=null;
	for(var i=0; i<bruteScheme.length; i++){
		var val=bruteScheme.charAt(i);
		if (oldVal){
			if (oldVal === val){
				cnt++;
			} else {
				if (threshold <= cnt){
					result+=cnt + oldVal;
				}
				cnt=1;
			}
		}
		oldVal=val;
	}
	return result;
};





// ==================== Strings management ====================

// URLs management :
function getDocumentFileName(){
	return getFileNameFromURL(document.location.href);
}

var isURLHttps=aotest(
// Tests definition
{
	// Tested function/method name :
	name : "isURLHttps"
	// Normal scenarii :
	,
	scenario1ok : [ [ "https://example.com" ], true ],
	scenario2ok : [ [ "http://example.com" ], false ],
	scenario3ok : [ [ "example.com" ], false ]
	// Abnormal scenarii :
	,
	scenario1nok : [ [ "http s://example.com" ], false ],
	scenario2nok : [ [ " https://example.com" ], false ],
	scenario3nok : [ [ "http://https://example.com" ], false ]
}
// Function definition
, function(url){
	return startsWith(url.toLowerCase(), "https://");
}, !PERFORM_TESTS_ON_LIBRARY);


function isCharacterToSkip(c){
	return RegExp("\\s").test(c) || contains([",","\'","´","\"",":","-","(",")","[","]","<",">","!","?","%",".","/","=",],c);
}

function snakize(strParam, separator="_"){
	let result="";
	for(let i=0;i<strParam.length;i++){
		let c=strParam[i];
		if(isCharacterToSkip(c)){
			result+=separator;
		}else{
			result+=c.toLowerCase();
		}
		if(separator===".")	result=result.replace(/\\.+/,separator);
		else								result=result.replace(RegExp(separator+"+"),separator);
	}
	return result;
}

function toConvention(type="snake",str){
	if(empty(str))	return str;
	str=str.trim();
	let result="";
	if(type==="camel"){
		for(let i=0;i<str.length;i++){
			let c=str[i];
			
			
			if(isCharacterToSkip(c)){
				let nextChar="";
				
				if(i<str.length-1){// If we are not at the very last character :
					do{
						nextChar=str[i+1];
						i++;
					}while(isCharacterToSkip(nextChar) && i<str.length-1);
					if(isCharacterToSkip(nextChar))	nextChar="";
				}
				result+=nextChar.toUpperCase();
			}else{
				
				result+=c;
			}
			
			
		}
	}else{
		if(type==="bundle"){
			result=snakize(str,".");
		}else{
			result=snakize(str);
		}
	}
	
	return result;
}

/*DEPRECATED, use new URL("...") instead !*/
function getFileNameFromURL(url,/* OPTIONAL */forceHTMLFileNameAppending){
	var end=url.length;
	// From most outwards...
	if (url.indexOf("#") != -1)
		end=url.indexOf("#");
	// ...to most inwards :
	if (url.indexOf("?") != -1)
		end=url.indexOf("?");

	// Everything from last «/» to the end...:
	var result=url.substring(url.lastIndexOf("/") + 1, end).trim();

	if (forceHTMLFileNameAppending && !contains(result, ".html")){
		result=result + (nothing(result) || result.charAt(result.length - 1) === "/" ? "index.html" : ".html");
	}
	return result;
}

/*DEPRECATED, use new URL("...") instead !*/
function getCalculatedPageName(/* OPTIONAL */pathParam){

	var path=pathParam;

	// TODO : Reinforce this check with a regular expression instead :
	if (nothing(path, true)){

		var url=getWholeURLExceptQueryAndHash();
		// return empty(getFileNameFromURL(url))?url+"/index.html":url;
		path=getFileNameFromURL(url, true);

		if (nothing(path, true)){
			// TRACE
			alert("You must enter a non-empty and valid server URL.");
			return null;
		}

		return path;

	}

	// TODO : Reinforce this check with a regular expression instead :
	if (!contains(path, ".html")){
		// TRACE
		log("WARN : Your URL does not point to a valid HTML file name, " + "then we force html extension to page name.");

		// var fileNameFromURL=getFileNameFromURL(path).trim();
		// return fileNameFromURL
		// +(fileNameFromURL.charAt(fileNameFromURL.length-1)==="/"
		// ?"index.html":".html");

	}

	return getFileNameFromURL(path, true);
}

/*DEPRECATED, use new URL("...") instead !*/
function getAbsoluteUrlForRelativeUrl(relativeURL){
	var serverURL=getCalculatedServerURLOnly();
	if (nothing(relativeURL))
		return serverURL;
	if (containsIgnoreCase(relativeURL, "(ht|f)tp[s]*://", true))
		return relativeURL;
	return serverURL + relativeURL;
}

/*DEPRECATED, use new URL("...") instead !*/
// Always has a final slash «/» appended on its end :
function getCalculatedServerURLOnly(){
	var url=getWholeURLExceptQueryAndHash();
	var filename=getFileNameFromURL(url);
	return addFinalSlashToURL(nothing(filename) ? url : url.replace("/" + filename, ""));
}

/*DEPRECATED, use new URL("...") instead !*/
// CAUTION : TO USE THIS FUNCTION, YOU MUST BE SURE THAT THERE *IS* ACTUALLY A
// SERVER PART, IT WILL NOT VERIFY BY ITSELF...
// OR ELSE IT WILL CAUSE UNPREDICTABLE BEHAVIOR :
function removeServerPartOfURL(urlWithServerPart){
	// return urlWithServerPart.replace(/^((ht|f)tp[s]*:\/\/[\W\.]\/)*/gim,"");
	// return urlWithServerPart.replace(getCalculatedServerURLOnly(),"");

	var result=urlWithServerPart;
	if (containsIgnoreCase(result, "(ht|f)tp[s]*://", true))
		result=result.replace(/(ht|f)tp[s]*:\/\//gim, "");

	return result.replace(result.substring(0, result.indexOf("/")), "").replace(/^\//gim, "");
}

/*DEPRECATED, use new URL("...") instead !*/
/*UNUSED*/
function getHostnameOfURL(url){
	const result=url;
	if (containsIgnoreCase(result, "(ht|f)tp[s]*://", true))
		result=result.replace(/(ht|f)tp[s]*:\/\//gim, "");

	return result.substring(0, result.indexOf("/")).replace(/^\//gim, "");
}

/*DEPRECATED, use new URL("...") instead !*/
function getURLAnchor(){
	var url=document.location.href;
	if (!contains(url, "#"))
		return "";
	return url.substring(url.lastIndexOf("#") + 1, url.length);
}

/*DEPRECATED, use new URL("...") instead ! (see : https://developer.mozilla.org/en-US/docs/Web/API/URL)*/
function getWholeURLExceptQueryAndHash(){
	return window.location.href.replace(window.location.search, "").replace(window.location.hash, "");
}

function addFinalSlashToURL(url){
	var result=url + (!nothing(url) && url.charAt(url.length - 1) === "/" ? "" : "/");
	return result;
}


// Persistence management :
function createCookie(name, value, days=null){
  let expires;
  if (days){
      let date=new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires="; expires=" + date.toGMTString();
  }
  else {
      expires="";
  }
  document.cookie=name + "=" + value + expires + "; path=/";
}

/*public*/function storeString(name, value, forceUseCookie=false, chunksSize=null){
	const strLength=value.length;
	if(!chunksSize || strLength<=chunksSize)	return storeSingleString(name, value, forceUseCookie);
	const maxNumberOfChunks=Math.ceil(strLength/chunksSize);
	for(let nameIndex=1,chunksIndex=0,chunk;chunksIndex<maxNumberOfChunks;){
		chunk=value.substr(chunksIndex*chunksSize,Math.min(strLength,(chunksIndex+1)*chunksSize)-chunksIndex*chunksSize);
		const nameStr=name+nameIndex;
		storeSingleString(nameStr, chunk, forceUseCookie);
		nameIndex++;
		chunksIndex++;
	}
	return value;
}
/*private*/function storeSingleString(name, value, forceUseCookie=false){
	
	if((!isHTML5StorageSupported("local") || forceUseCookie ) && (typeof jQuery === "undefined"  || !jQuery )){
		// TRACE
		log("WARN : Storage is not supported by this browser, writing to cookie.");
		// OLD (not maintained anymore) : jQuery.cookie(name, value);
		createCookie(name, value);
		return value;
	}
	// sessionStorage.setItem(name, value);
	try{
		localStorage.setItem(name, value);
	}catch(e){
		console.log("ERROR : An error occured while trying to write to localStorage",e);
	}
	
	return value;
}


function getCookie(cookieName){
  if (document.cookie.length > 0){
      let cookieStart=document.cookie.indexOf(cookieName + "=");
      if (cookieStart != -1){
          cookieStart=cookieStart + cookieName.length + 1;
          let cookieEnd=document.cookie.indexOf(";", cookieStart);
          if (cookieEnd==-1){
              cookieEnd=document.cookie.length;
          }
          return unescape(document.cookie.substring(cookieStart, cookieEnd));
      }
  }
  return "";
}

/*public*/function getStringFromStorage(name, forceUseCookie=false, readChunked=false){
	const foundString=getSingleStringFromStorage(name, forceUseCookie);
	if(!readChunked || foundString)	return foundString;
	let result="";
	for(let nameIndex=1,foundValue=getSingleStringFromStorage(name+nameIndex, forceUseCookie);
			foundValue!==null;){
		result+=foundValue;
		nameIndex++;
		foundValue=getSingleStringFromStorage(name+nameIndex, forceUseCookie);
	}
	return result;
}
/*public*/function getSingleStringFromStorage(name, forceUseCookie=false){
	if((!isHTML5StorageSupported("local") || forceUseCookie  ) && (typeof jQuery === "undefined" || !jQuery ) ){
		// TRACE
		log("WARN : Storage is not supported by this browser, reading from cookie.");
		// OLD (not maintained anymore) : return jQuery.cookie(name);
		return getCookie(name);
	}
	// return sessionStorage.getItem(name);
	
	var result=null;
	try{
		result=localStorage.getItem(name);
	}catch(e){
		console.log("ERROR : An error occured while trying to read from localStorage",e);
	}

	return result;
}



function isHTML5StorageSupported(type){
	if (!nothing(type) && type === "local"){
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e){
			return false;
		}
	}
	try {
		return 'sessionStorage' in window && window['sessionStorage'] !== null;
	} catch (e){
		return false;
	}
	return false;
}

// ==================== Files management ====================

function validateFilesExtensions(self, acceptedFilesTypesString){
	
	const PATH_SEPARATOR="/";

	
	// UPLOADED FILES TYPES :
	var acceptedFilesTypes=acceptedFilesTypesString.split(",");
	var acceptedFilesExtensionsRegExpStr="";
	if (!empty(acceptedFilesTypes)){
		acceptedFilesExtensionsRegExpStr+="(";
		for(var i=0; i<acceptedFilesTypes.length; i++){
			acceptedFilesExtensionsRegExpStr+=(acceptedFilesExtensionsRegExpStr=="(" ? "" : "|") + "\." + (acceptedFilesTypes[i].split(PATH_SEPARATOR)[1]);
		}
		acceptedFilesExtensionsRegExpStr+=")";
	}

	var files=self.files;
	var isCorrect=true;
	for(var i=0; i<files.length; i++){
		var file=files[i];

		var name=file.name;
		// var size=file.size;
		var type=file.type;

		if (type){
			isCorrect=contains(acceptedFilesTypesString, type);
			if (!isCorrect)
				break;

		} else {
			isCorrect=contains(name, acceptedFilesExtensionsRegExpStr, true);
			if (!isCorrect)
				break;

		}
		// TODO : Add maximum size restriction :

		if (!isCorrect){
			// TRACE
			alert("File type is incorrect (must be in : «" + acceptedFilesTypes + "»)");
			self.value="";
			return false;
		}
	}
	return true;
}

function getGenericFileType(fileParam){
	var OTHER_TYPE="other";

	var file=fileParam;
	if (!file){
		// TRACE
		log("ERROR : file is undefined !");
		return OTHER_TYPE;
	}

	if (file.type){
		if (containsIgnoreCase(file.type, "image"))
			return "image";
		if (containsIgnoreCase(file.type, "audio"))
			return "audio";
		if (containsIgnoreCase(file.type, "video"))
			return "video";
		if (containsIgnoreCase(file.type, "text"))
			return "text";
	} else if (file.name){
		// A less safe method, if no type could be retrieved from file :
		if (containsIgnoreCase(file.name, "(\.jpg|\.jpeg|\.png|\.gif)", true))
			return "image";
		if (containsIgnoreCase(file.name, "(\.wav|\.mp3|\.ogg|\.mpeg)", true))
			return "audio";
		if (containsIgnoreCase(file.name, "(\.mp4|\.avi)", true))
			return "video";
		if (containsIgnoreCase(file.name, "(\.pdf|\.txt|\.doc)", true))
			return "text";
	} else if (typeof file === "string"){
		// It is an unelegant manner to not duplicate code :
		var filename=file;
		file=new Object();
		file.name=filename;
		return getGenericFileType(file);
	}
	return OTHER_TYPE;
}

function getFileType(fileParam){
	var file=fileParam;
	if (file.type){
		return file.type;
	} else if (file.name){
		// A less safe method, if no type could be retrieved from file :
		if (containsIgnoreCase(file.name, "(\.jpg|\.jpeg)", true))
			return "image/jpeg";
		if (containsIgnoreCase(file.name, "(\.png)", true))
			return "image/png";
		if (containsIgnoreCase(file.name, "(\.gif)", true))
			return "image/gif";
		if (containsIgnoreCase(file.name, "(\.mp3)", true))
			return "audio/mp3";
		if (containsIgnoreCase(file.name, "(\.ogg)", true))
			return "audio/ogg";
		if (containsIgnoreCase(file.name, "(\.wav)", true))
			return "audio/wav";
		if (containsIgnoreCase(file.name, "(\.mpeg)", true))
			return "audio/mpeg";
		if (containsIgnoreCase(file.name, "(\.mp4)", true))
			return "video/mp4";
		if (containsIgnoreCase(file.name, "(\.avi)", true))
			return "video/avi";
		if (containsIgnoreCase(file.name, "(\.pdf)", true))
			return "text/pdf";
		if (containsIgnoreCase(file.name, "(\.txt)", true))
			return "text/txt";
		if (containsIgnoreCase(file.name, "(\.doc)", true))
			return "text/doc";
	} else if (typeof file === "string"){
		// It is an unelegant manner to not duplicate code :
		var filename=file;
		file=new Object();
		file.name=filename;
		return getFileType(file);
	}
	return "other";
}

// Functions management :
Function.prototype.clone=function(){
	var cloneObj=this;
	if (this.__isClone){
		cloneObj=this.__clonedFrom;
	}

	var temp=function(){
		return cloneObj.apply(this, arguments);
	};
	for( var key in this){
		temp[key]=this[key];
	}

	temp.__isClone=true;
	temp.__clonedFrom=cloneObj;

	return temp;
};


/*public*/function downloadFile(name, content){
	const tempElement=document.createElement("a");
	tempElement.setAttribute("href","data:text/plain;charset=utf-8,"+encodeURIComponent(content));
	tempElement.setAttribute("download",name);
	tempElement.style.display="none";
	
	document.body.appendChild(tempElement);
	tempElement.click();
	document.body.removeChild(tempElement);
}

/*public*/function readFileContent(element){ // Needs an <input type="file" /> element
	return new Promise((onComplete,onError)=>{
		if(!element){
			onError(new Error("File input element is null, cannot read file."));
			return;
		}
		
		try{
			
			let file=element.files[0];
			if(!file)	return null;
			const reader=new FileReader();
			reader.onload=(e)=>{
				const content=e.target.result;
				onComplete(content);
			};
	
			reader.readAsText(file);
		}catch(error){
			onError(error);
		}
	});
}


// Drawing and graphics management :

function getBeamLikeGradient(ctx, displayX1, displayY1, displayX2, displayY2, size, color, alpha,/* OPTIONAL */zoomFactor){

	var lx=displayX2 - displayX1;
	var ly=displayY2 - displayY1;
	var lineLength=Math.sqrt(lx * lx + ly * ly);
	var wy=lx / lineLength * size * (!zoomFactor ? 1 : zoomFactor);
	var wx=ly / lineLength * size * (!zoomFactor ? 1 : zoomFactor);
	var grd=ctx.createLinearGradient(displayX1 - wx *.5, displayY1 + wy *.5, displayX1 + wx *.5, displayY1 - wy *.5);

	var colorComps=htmlColorCodeToDecimalArray(color ? color : "#ffffff");
	grd.addColorStop(0, "rgba(0,0,0,0)");
	grd.addColorStop(0.25, "rgba(" + colorComps[0] + "," + colorComps[1] + "," + colorComps[2] + "," + alpha + ")");
	grd.addColorStop(0.50, "rgba(255,255,255," + alpha + ")");
	grd.addColorStop(.75, "rgba(" + colorComps[0] + "," + colorComps[1] + "," + colorComps[2] + "," + alpha + ")");
	grd.addColorStop(1, "rgba(0,0,0,0)");
	return grd;
}

function getCanvasGradient(type="linear",colorStops=[]){
	// TODO : DEVELOP...!
	let result=ctx.createLinearGradient(0, 0, 100, 100);
	return result;
}

// hex2rgb
function hex2rgb(hexColorCodeParam){
	const colors=htmlColorCodeToDecimalArray(hexColorCodeParam);
	return {r:colors[0],g:colors[1],b:colors[2]};
}
function htmlColorCodeToDecimalArray(hexColorCodeParam){
	var hexColorCode=hexColorCodeParam.replace("#", "");

	if (contains(hexColorCode, ":"))
		hexColorCode=hexColorCode.split(":")[0];

	if (hexColorCode.length<6){
		return [ hex2dec(hexColorCode.charAt(0)) * 16, hex2dec(hexColorCode.charAt(1)) * 16, hex2dec(hexColorCode.charAt(2)) * 16 ];
	}

	return [ hex2dec(hexColorCode.substring(0, 2)), hex2dec(hexColorCode.substring(2, 4)), hex2dec(hexColorCode.substring(4, 6)) ];
}


/*private*/function componentToHex(c){
  var hex=c.toString(16);
  return hex.length==1 ? "0" + hex : hex;
}
/*public*/function rgb2Hex(r, g, b){
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}


changeLuminosity=function(htmlHexStr,amount){
	let rgb=htmlColorCodeToDecimalArray(htmlHexStr);
	let r=Math.coerceInRange(rgb[0]+amount,0,255);
	let g=Math.coerceInRange(rgb[1]+amount,0,255);
	let b=Math.coerceInRange(rgb[2]+amount,0,255);
	return rgb2Hex(r,g,b);
}


// HTML5 CONTEXT ENHANCEMENTS, ONLY TO COMPENSATE A HTML5 CANVAS LACK : cf.
// stackoverflow.com/questions/4576724/dotted-stroke-in-canvas
// ------------------------------
// UNUSED AND DEPRECATED :
//function drawDashedLine(ctx, x, y, x2, y2, da){
//	if (!da)
//		da=[ 10, 5 ];
//	var dx=(x2 - x);
//	var dy=(y2 - y);
//	var len=Math.sqrt(dx * dx + dy * dy);
//	var rot=Math.atan2(dy, dx);
//	
//	ctx.translate(x, y);
//	ctx.moveTo(0, 0);
//
//	ctx.rotate(rot);
//	var dc=da.length;
//	var di=0;
//	var draw=true;
//	x=0;
//	while (len > x){
//		x+=da[di++ % dc];
//		if (x > len)
//			x=len;
//		if (draw)
//			ctx.lineTo(x, 0);
//		else
//			ctx.moveTo(x, 0);
//		draw=!draw;
//	}
//}
// END OF HTML5 CONTEXT ENHANCEMENTS, ONLY TO COMPENSATE A HTML5 CANVAS LACK.
// ------------------------------





//===============================================================


// ==================== Media management ====================


// Webcam/microphone management :

// Compatibility we-will-be-really-ashamed-of-in-not-so-long-in-the-future-with-reason-and-awfully-stupid-but-must-exist-methods :
if(typeof(window)!=="undefined")	window.URL=window.URL || window.webkitURL;
// OLD: (DEPRECATED)
//navigator.getUserMedia=navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
//window.AudioContext=window.AudioContext || window.webkitAudioContext;

//function hasGetUserMedia(){
//	// OLD : (DEPRECATED)
////	return !!/*<-=to force a boolean result*/(navigator.getUserMedia);
//	return !!/*<-=to force a boolean result*/(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
//}



function doWithChunks(chunksNumber,dataArray,doFunction,/*OPTIONAL*/refreshingRateMillis,/*OPTIONAL*/modeParam){
	
	if(chunksNumber<1){
		log("ERROR : Chunks number is too small. chunksNumber:"+chunksNumber);
  	return;
	}
	
	var mode=modeParam?modeParam:"string";
	
  var remaining=0;
  var chunkBaseLength=1;
  var arrayLength=dataArray.length;
  
  chunkBaseLength=Math.round(arrayLength/chunksNumber);
  if(1<chunkBaseLength)		remaining=arrayLength%chunkBaseLength;
  if(chunkBaseLength<1){
  	log("ERROR : Chunks are too small. CHUNK_NUMBER:"+chunksNumber+",chunkBaseLength:"+chunkBaseLength);
  	return;
  }
  
  var chunkLength=chunkBaseLength;
  
  var iterate=function(i){
  		
		if(0<remaining && arrayLength-i<chunkBaseLength){
			chunkLength=remaining;
		}

		var chunk;
		if(mode==="string"){
			var index=i*chunkBaseLength;
			chunk=dataArray.substring(index,index+chunkLength);
		}

  	doFunction(chunk,i);
  
  };
  
  if(!refreshingRateMillis){
  	for(var i=0;i<chunksNumber;i++){
  		iterate(i);
  	}
  }else{
  	
  	var i=0;
  	var pe=setInterval(function(){
  		if(chunksNumber<=i){
  			clearInterval(pe);
  			return;
  		}
  		iterate(i);
  		i++;
  	},refreshingRateMillis);
  	
  }
  
}


removeAlpha=function(videoRawData){
	var newVideoRawData=[];
	for(var i=0;i<videoRawData.length;i++){
		var val=videoRawData[i];
		if((i+1)%4==0)	continue;
		newVideoRawData.push(val);
	}
	return newVideoRawData;
}

//convertDataURIToBinary=function(dataURI) {
base64StringToIntArray=function(dataURI){
	const  BASE64_MARKER=';base64,';

	const base64Index=dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
	let base64=dataURI.substring(base64Index);
	if(typeof(atob)==="undefined" && typeof(require)!=="undefined")		atob=require('atob');
	let raw=atob(base64);
	
	let rawLength=raw.length;
//	let array=new Uint8Array(new ArrayBuffer(rawLength));
	let array=[];
	for(let i=0; i<rawLength; i++) {
//		array[i]=raw.charCodeAt(i);
		array.push(raw.charCodeAt(i));
	}
//	return new Uint8Array(array);
	return array;
};

// DOES NOT SEEM TO WORK (for images src for instance)
intArrayToBase64String=function(intArray, addAlphaCharacter=false){
	let result="";
	if(!intArray || !isArray(intArray)){
		// TRACE
		lognow("ERROR : Int array is not an array. Aborting. Array parameter :",intArray);
		return result;
	}
	foreach(intArray,(i, index)=>{
		result+=String.fromCharCode(i);
		if(addAlphaCharacter && (index%3)==0)
			result+=String.fromCharCode(255);
	});
	result=btoa(result);
	return result;
};

// UNUSED :
//intArrayToBlob=function(intArray, type="image/bmp" /*other values : "image/jpeg", "image/png"*/){
//	if(!intArray || !isArray(intArray)){
//		// TRACE
//		lognow("ERROR : Int array is not an array. Aborting. Array parameter :",intArray);
//		return URL.createObjectURL(new Blob());
//	}
//
////	let str="";
////	foreach(intArray,(i)=>{
////		str+=String.fromCharCode(i);
////	});
////	let blob=new Blob([str], {type: type});
//
//	let arr=new Uint8Array(intArray);
//	let blob=new Blob([arr.buffer], {type: type});
//	
//	const result = URL.createObjectURL(blob);
//	return result;
//};






function addAlphaCanalIfNecessary(videoRawData, /*OPTIONAL*/isMessWithAlpha=false){
	let newVideoRawData=[];

	if(isMessWithAlpha){
		for(var i=0;i<videoRawData.length;i++){
	  		var val=videoRawData[i];
	  		newVideoRawData.push(val);
	  		if((i+1)%3==0){
	  			newVideoRawData.push(255);
	  		}
	  	}
	}else{
		newVideoRawData=videoRawData;
	}
	
	return newVideoRawData;
}

function normalizeData(videoRawData, destImageData, /*OPTIONAL*/isMessWithAlpha){

	let newVideoRawData=addAlphaCanalIfNecessary(videoRawData, isMessWithAlpha);

	// The only stupid way to pass new data to destination array ! Did I mention it is stupid ?
	for(var i=0;i<newVideoRawData.length;i++){
//  	destImageData[i]=String.fromCharCode(newVideoRawData[i]).charCodeAt();
//		destImageData[i]=parseInt(newVideoRawData[i],10);
		
		// MAIN :
//		destImageData[i]=newVideoRawData[i];

		if((i+1)%4!==0){
			// WORKAROUND :
			let workaroundValue=""+newVideoRawData[i];
			destImageData[i]=parseInt(""+workaroundValue,10);
		}else{
			destImageData[i]=255;
		}
	}
	
}


// (UNUSED)
function getBlackAndWhitePixels(rawData,width){
	
	let pixels=[];


	const MIN_THRESHOLD=20;
	const MAX_THRESHOLD=230;

	const d=rawData;
	
//	let squaredDistance=0;
	const STEP=3;
	for(let i=0;i<d.length;i+=STEP){
		
		let r=d[i];
		let g=d[i+1];
		let b=d[i+2];
//		let a=d[i+3];
			

		let pixel={ type:null }; // type=(0=black;null=gray;1=white)
		let avg=Math.averageInArray([r,g,b]);
		
		if(MAX_THRESHOLD<=avg){
			pixel.type=1;
		}else if(avg<=MIN_THRESHOLD){
			pixel.type=0;
		}
		
		if(pixel.type!==null){
			let coords=getCoordinatesForLinearIndex(i,width,STEP);
//			pixel.distanceToPreviousChange=squaredDistance;
			pixel.x=coords.x;
			pixel.y=coords.y;
			pixels.push(pixel);
//			squaredDistance=0;
//		}else{
//			squaredDistance++;
		}
	}
	
	return pixels;
}








// (OLD)
function populateContigousZonePixelsCoords(visitedPixelsIndexes=[],singleContigousZonePixelsCoords=[],d,i,width,height,step,filterFunction){

	if(contains(visitedPixelsIndexes,i))	return;

	// We have to call this now, to avoid being counted as a neighbor of its own neighbors later on !
	visitedPixelsIndexes.push(i);


	let r=d[i];
	let g=d[i+1];
	let b=d[i+2];
	
	
	if(filterFunction && !filterFunction(r,g,b)){

//		// DBG
//		d[i]=255;
//		d[i+1]=255;
//		d[i+2]=255;
	
		return;
	}



	let coords=getCoordinatesForLinearIndex(i,width,step);
	let x=coords.x;
	let y=coords.y;
	
	singleContigousZonePixelsCoords.push({x:x, y:y, color:{r:r,g:g,b:b}});
	
	
	let neighborsPixelsIndexes=getNeighborsPixelsIndexes(x,y,width,height,step);
	foreach(neighborsPixelsIndexes,(neighborPixelIndex)=>{
		
		// To reduce the number of calls :
		if(contains(visitedPixelsIndexes,neighborPixelIndex))	return "continue";
		let nr=d[neighborPixelIndex];
		let ng=d[neighborPixelIndex+1];
		let nb=d[neighborPixelIndex+2];
		if(filterFunction && !filterFunction(nr,ng,nb))	return "continue";

		// We call this method again for each unvisited neighbor pixel :
		if(contains(visitedPixelsIndexes,neighborPixelIndex))	return "continue"; // to avoid some recursive calls : 
		populateContigousZonePixelsCoords(visitedPixelsIndexes,singleContigousZonePixelsCoords,d,neighborPixelIndex,width,height,step,filterFunction);
		
	});

}

// (OLD)
function getContigousPixelsGroupsBarycentersByRecursiveNeighboring(rawData,width,step,filterFunction=null){

	let barycenters=[];
	
	let zones=[];
	
	const d=rawData;
	
	const visitedPixelsIndexes=[];
	let length=d.length;
	let height=Math.floor(length/(width*step));
	for(let i=0;i<length;i+=step){
	
		// To reduce the number of calls :
		if(contains(visitedPixelsIndexes,i))	continue;
		let r=d[i];
		let g=d[i+1];
		let b=d[i+2];
		if(filterFunction && !filterFunction(r,g,b))	continue;
		
	
		// Case pixel in zone has no parent :
		let singleContigousZonePixelsCoords=[];
		populateContigousZonePixelsCoords(visitedPixelsIndexes,singleContigousZonePixelsCoords,d,i,width,height,step,filterFunction);

		let zoneLength=singleContigousZonePixelsCoords.length;
		if(zoneLength<=1)	continue;
		
		
		zones.push(singleContigousZonePixelsCoords);

		// Barycenters calculation :
		let barycenter=Math.getBarycenter(singleContigousZonePixelsCoords);
		barycenters.push(barycenter);
	
		// barycenter color :
		let avgR=0;
		let avgG=0;
		let avgB=0;
		foreach(singleContigousZonePixelsCoords,(coords)=>{
			let color=coords.color;
			avgR+=color.r;
			avgG+=color.g;
			avgB+=color.b;
		});
		barycenter.color={r:avgR/zoneLength, g:avgG/zoneLength, b:avgB/zoneLength};
	
	}


	return barycenters;
}









function getNeighborsPixelsCoordinates(x, y, totalWidth, totalHeight){
	let results=[];
	
	// Line 1
	if(0<y){
		if(0<x && x<=totalWidth-1){
			results.push({x:x-1, y:y-1});
		}else results.push(null);
		if(0<=x && x<=totalWidth-1){
			results.push({x:x, y:y-1});
		} else results.push(null);
		if(0<=x && x<totalWidth-1){
			results.push({x:x+1, y:y-1});
		} else results.push(null);
	}
	// Line 2
	{
		if(0<x && x<=totalWidth-1){
			results.push({x:x-1, y:y});
		} else results.push(null);
		//
		// (We skip the central pixel...)
		//
		if(0<=x && x<totalWidth-1){
			results.push({x:x+1, y:y});
		} else results.push(null);
	}
	// Line 3
	if(y<totalHeight){
		if(0<x && x<=totalWidth-1){
			results.push({x:x-1, y:y+1});
		} else results.push(null);
		if(0<=x && x<=totalWidth-1){
			results.push({x:x, y:y+1});
		} else results.push(null);
		if(0<=x && x<totalWidth-1){
			results.push({x:x+1, y:y+1});
		} else results.push(null);
	}

	return results;
}



function getNeighborsPixelsIndexes(x, y, totalWidth, totalHeight, step){
	let results=[];
	
	const coords=getNeighborsPixelsCoordinates(x, y, totalWidth, totalHeight);

//	// Line 1
//	if(0<y){
//		if(0<x && x<=totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[0].x, coords[0].y, totalWidth, step));
//		}
//		if(0<=x && x<=totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[1].x, coords[1].y, totalWidth, step));
//		}
//		if(0<=x && x<totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[2].x, coords[2].y, totalWidth, step));
//		}
//	}
//	// Line 2
//	{
//		if(0<x && x<=totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[3].x, coords[3].y, totalWidth, step));
//		}
//		//
//		// (We skip the central pixel...)
//		//
//		if(0<=x && x<totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[4].x, coords[4].y, totalWidth, step));
//		}
//	}
//	// Line 3
//	if(y<totalHeight){
//		if(0<x && x<=totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[5].x, coords[5].y, totalWidth, step));
//		}
//		if(0<=x && x<=totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[6].x, coords[6].y, totalWidth, step));
//		}
//		if(0<=x && x<totalWidth-1){
//			results.push(getLinearIndexForCoordinates(coords[7].x, coords[7].y, totalWidth, step));
//		}
//	}

	foreach(coords,(coord)=>{
		if(!coord)	return "continue";
		results.push(getLinearIndexForCoordinates(coord.x, coord.y, totalWidth, step));
	});

	return results;
}


function getContigousPixelsGroupsBarycentersInfo(rawData,width,step,filterFunction=null){

	// OLD, BUT ALMOST WORKS :
//return getContigousPixelsGroupsBarycentersByRecursiveNeighboring(rawData,width,step,filterFunction);
	return getContigousPixelsGroupsBarycentersByLinearNeighboring(rawData,width,step,filterFunction);
	// DOES NOT WORK :
//	return getContigousPixelsGroupsBarycentersByMinesweeping(rawData,width,step,filterFunction);

}

// TODO :
function getContigousPixelsGroupsBarycentersByLinearNeighboring(rawData,width,step,filterFunction=null){

	const pixelsLinearIndexesZonesIndexes={};// zoned pixels are stocked in the form : { <pixel linear index> : <zone index> } 
	
	
	const zones=[];
	
	
	zoneIndex=0;
	const d=rawData;
	const length=d.length;
	const height=Math.floor(length/(width*step));
	for(let i=0;i<length;i+=step){
	
		let isPixelIndexInAZone=contains(pixelsLinearIndexesZonesIndexes, i, false, true); // We search in keys of the associative array only
		// If pixel has already been added toa zone, then we skip it :
		if(isPixelIndexInAZone) continue;
	
		// To reduce the number of calls :
		let r=d[i];
		let g=d[i+1];
		let b=d[i+2];
		if(filterFunction && !filterFunction(r,g,b))	continue;
		
		
		let coords=getCoordinatesForLinearIndex(i,width,step);
		let x=coords.x;
		let y=coords.y;

		// We filter the neighbor pixels :
		let neighborsPixelsIndexesRaw=getNeighborsPixelsIndexes(x, y, width, height, step);
		let neighborsPixelsIndexes=[];
		if(filterFunction){
			foreach(neighborsPixelsIndexesRaw,(neighborPixelIndex)=>{
	
				let nr=d[neighborPixelIndex];
				let ng=d[neighborPixelIndex+1];
				let nb=d[neighborPixelIndex+2];
	
				if(!filterFunction(nr,ng,nb))	return "continue";
	
				neighborsPixelsIndexes.push(neighborPixelIndex);
	
			});
		}else{
			let neighborsPixelsIndexes=neighborsPixelsIndexesRaw;
		}
		

		// We ignore single-pixel zones :
		if(empty(neighborsPixelsIndexes))	continue;


		let foundZoneIndex=null;

//		let isPixelIndexInAZone=contains(pixelsLinearIndexesZonesIndexes, i, false, true); // We search in keys of the associative array only
//		if(isPixelIndexInAZone){
//			foundZoneIndex=pixelsLinearIndexesZonesIndexes[i];
//		}else{
			// At this point, the currently examined pixel is not in any zone :
			foreach(neighborsPixelsIndexes,(neighborPixelIndex)=>{
			
				let isNeighborPixelIndexInAZone=contains(pixelsLinearIndexesZonesIndexes, neighborPixelIndex, false, true); // We search in keys of the associative array only
				if(isNeighborPixelIndexInAZone){
					foundZoneIndex=pixelsLinearIndexesZonesIndexes[neighborPixelIndex];
					return "break";
				}
			
			},(neighborPixelIndex)=>{
				// We want only already in a zone neighbors pixels :	
				return contains(pixelsLinearIndexesZonesIndexes, neighborPixelIndex, false, true); // We search in keys of the associative array only
			});
//		}

		let zone;
		// If we had neighbors, but we found no zone, then we create a new zone !
		if(foundZoneIndex==null){
		
			zone=[];
			zones.push(zone);
			
			
			// DBG
			zone.debugColor=(zoneIndex*16)%255;
			
			// We add the current pixel to the new zone :
			pixelsLinearIndexesZonesIndexes[i]=zoneIndex;

			// And we do the same for all its neighbors :
			foreach(neighborsPixelsIndexes,(neighborPixelIndex)=>{
				pixelsLinearIndexesZonesIndexes[neighborPixelIndex]=zoneIndex;

				let ncoords=getCoordinatesForLinearIndex(neighborPixelIndex,width,step);
				let nx=ncoords.x;
				let ny=ncoords.y;
				
				let nr=d[neighborPixelIndex];
				let ng=d[neighborPixelIndex+1];
				let nb=d[neighborPixelIndex+2];
				
				zone.push({x:nx,y:ny, color:{r:nr,g:ng,b:nb}});
				
				// DBG
				d[neighborPixelIndex]=255;
				d[neighborPixelIndex+1]=0;
				d[neighborPixelIndex+2]=0;
				
				
			},(neighborPixelIndex)=>{	
				return !contains(pixelsLinearIndexesZonesIndexes, neighborPixelIndex, false, true); // We search in keys of the associative array only
			});

		
			zoneIndex++;
			
			

			
			// DBG
			d[i]=0;
			d[i+1]=0;
			d[i+2]=255;

			
		}else{ // If we had neighbors, but we found a zone, then we add the current pixel to this found zone :
			
			zone=zones[foundZoneIndex];
			
			// If necessary, we add the current pixel to the found zone :
			pixelsLinearIndexesZonesIndexes[i]=foundZoneIndex;

			// And we do the same for all its neighbors :
			foreach(neighborsPixelsIndexes,(neighborPixelIndex)=>{
				pixelsLinearIndexesZonesIndexes[neighborPixelIndex]=foundZoneIndex;
				
				let ncoords=getCoordinatesForLinearIndex(neighborPixelIndex,width,step);
				let nx=ncoords.x;
				let ny=ncoords.y;
				
				let nr=d[neighborPixelIndex];
				let ng=d[neighborPixelIndex+1];
				let nb=d[neighborPixelIndex+2];
				
				zone.push({x:nx,y:ny, color:{r:nr,g:ng,b:nb}});
				
				
				// DBG
//				d[neighborPixelIndex]=Math.min(255,d[neighborPixelIndex]+zoneIndex*16);
//				d[neighborPixelIndex+1]=0;
//				d[neighborPixelIndex+2]=0;
				
				// DBG
				d[neighborPixelIndex]=zone.debugColor;
				d[neighborPixelIndex+1]=zone.debugColor;
				d[neighborPixelIndex+2]=zone.debugColor;
			
			},(neighborPixelIndex)=>{	
				return !contains(pixelsLinearIndexesZonesIndexes, neighborPixelIndex, false, true); // We search in keys of the associative array only
			});	
			
			
					
			// DBG
			d[i]=255-zone.debugColor;
			d[i+1]=255-zone.debugColor;
			d[i+2]=255-zone.debugColor;
			
			
		}
		zone.push({x:x,y:y, color:{r:r,g:g,b:b}});
		
	}


	// Barycenters calculation :
	let barycenters=Math.getBarycentersFromZonesWithColors(zones);

	return barycenters;
}


function getTargetPoints(rawData, width, height
//										, color="black"
										,ctx/*DBG*/){
	const DISPLAY_DEBUG_PIXELS=true;
	const DEBUG_DISPLAY_ACTIVE=true;
	
	const INCLUDE_BLACK_ZONES=false;

	if(empty(rawData))	return points;

	const STEP=3;


	const MIN_THRESHOLD=32;
	const MAX_THRESHOLD=224;
	const MEDIUM_THRESHOLD=128;
	
	

	const filterFunction=(r,g,b,a=null)=>{
	
////		if(color==="red"){
//////			if(MAX_THRESHOLD<=r && g<=MIN_THRESHOLD && b<=MIN_THRESHOLD)	return true;
////			if(MEDIUM_THRESHOLD<=r && g<=MEDIUM_THRESHOLD && b<=MEDIUM_THRESHOLD)	return true;
////		}else if(color==="green"){
//////			if(r<=MIN_THRESHOLD && MAX_THRESHOLD<=g && b<=MIN_THRESHOLD)	return true;
////			if(r<=MEDIUM_THRESHOLD && MEDIUM_THRESHOLD<=g && b<=MEDIUM_THRESHOLD)	return true;
////		}else if(color==="blue"){
//////			if(r<=MIN_THRESHOLD && g<=MIN_THRESHOLD && MAX_THRESHOLD<=b)	return true;
////			if(r<=MEDIUM_THRESHOLD && g<=MEDIUM_THRESHOLD && MEDIUM_THRESHOLD<=b)	return true;
////		}else{
//
		// Case «black»
		// (AVERAGE) 
		let avg=Math.averageInArray([r,g,b]);
		if(INCLUDE_BLACK_ZONES){
			if(avg<=MIN_THRESHOLD)	return true;
		}
		
//		}

//		if(MEDIUM_THRESHOLD<r && g<=MAX_THRESHOLD && b<=MAX_THRESHOLD)	return true;
//		if(MEDIUM_THRESHOLD<g && r<=MAX_THRESHOLD && b<=MAX_THRESHOLD)	return true;
//		if(MEDIUM_THRESHOLD<b && r<=MAX_THRESHOLD && g<=MAX_THRESHOLD)	return true;
		
//		if(MAX_THRESHOLD<r && MAX_THRESHOLD<g && MAX_THRESHOLD<b)	return false;

		// Case «bright colors
//		if(MEDIUM_THRESHOLD<avg){
			let variance=Math.getVariance([r,g,b]);
			if(MIN_THRESHOLD<variance){
				if(MAX_THRESHOLD<r) return true;
				if(MAX_THRESHOLD<g) return true;
				if(MAX_THRESHOLD<b) return true;
			}
//		}


		
		
		return false;
	};

	
	
	// 1) We only want the black pixels
	let allBarycenters=getContigousPixelsGroupsBarycentersInfo(rawData,width,STEP,filterFunction);
	
	
	
	// We only keep the marker points :
//	let filter=filterPoints(allBarycenters ,ctx/*DBG*/);


	// DBG
	let filter={points:allBarycenters};



	// DEBUG
//	if(DEBUG_DISPLAY_ACTIVE){
//
//		const d=rawData;
//		
//		for(let i=0;i<rawData.length;i+=STEP){
//	
//			let r=d[i];
//			let g=d[i+1];
//			let b=d[i+2];
//	
//			// DEBUG
//			if(!filterFunction(d[i],d[i+1],d[i+2])){
//				d[i]=255;
//				d[i+1]=255;
//				d[i+2]=255;
//			}else{
//				d[i]=0;
//				d[i+1]=0;
//				d[i+2]=0;
//			}
//		
//		}
//
//	}



	if(DISPLAY_DEBUG_PIXELS){

		const points=filter.points;
	
		const d=rawData;
	
		foreach(points,(p)=>{
			let i=getLinearIndexForCoordinates(p.x, p.y, width, STEP);
			
			
			let r=d[i];
			let g=d[i+1];
			let b=d[i+2];
			
			
//			if(g>r && b<r){
//				d[i]=255;
//				d[i+1]=128;
//				d[i+2]=128;
//				return "continue";
//			}
//			if(r<g && b<g){
//				d[i]=128;
//				d[i+1]=255;
//				d[i+2]=128;
//				return "continue";			
//			}
//			if(r<b && g<b){
//				d[i]=128;
//				d[i+1]=128;
//				d[i+2]=255;
//				return "continue";
//			}
			
			
//			if(color==="red"){
//				d[i]=255;
//				d[i+1]=128;
//				d[i+2]=128;
//			}else if(color==="green"){
//				d[i]=128;
//				d[i+1]=255;
//				d[i+2]=128;
//			}else if(color==="blue"){
//				d[i]=128;
//				d[i+1]=128;
//				d[i+2]=255;
//			}else{ // Case «black»
//				d[i]=128;
//				d[i+1]=128;
//				d[i+2]=128;
//			}
			
			let color=p.color;
			
			// Black color :
			let avg=Math.averageInArray([color.r,color.g,color.b]);
			if(avg<=MIN_THRESHOLD){
				d[i]=255;
				d[i+1]=255;
				d[i+2]=255;
				return "continue";
			}
			
			// Bright colors :
			if(color.g>color.r && color.b<color.r){
				d[i]=255;
				d[i+1]=128;
				d[i+2]=128;
				return "continue";
			}
			if(color.r<color.g && color.b<color.g){
				d[i]=128;
				d[i+1]=255;
				d[i+2]=128;
				return "continue";			
			}
			if(color.r<color.b && color.g<color.b){
				d[i]=128;
				d[i+1]=128;
				d[i+2]=255;
				return "continue";
			}
			
//			d[i]=255-color.r;
//			d[i+1]=255-color.g;
//			d[i+2]=255-color.b;
			
			d[i]=128;
			d[i+1]=128;
			d[i+2]=128;

	
		});
	}
	

	return filter;
}






// (DOES NOT WORK)
//function getContigousPixelsGroupsBarycentersByMinesweeping(rawData,width,step,filterFunction=null){
//
////	const visitedIndexes=[];
//
//	
//	let zones=[];
//	
//	let zonedPixelsLinearIndexes={};
//	zoneIndex=0;
//	
//	const d=rawData;
//	
//	let length=d.length;
//	let height=Math.floor(length/(width*step));
//	for(let i=0;i<length;i+=step){
//	
////		// DBG
////		if(contains(visitedIndexes, i)) continue;
//	
//	
//		// To reduce the number of calls :
//		let r=d[i];
//		let g=d[i+1];
//		let b=d[i+2];
//		if(filterFunction && !filterFunction(r,g,b))	continue;
//		
//		
//		let coords=getCoordinatesForLinearIndex(i,width,step);
//		let x=coords.x;
//		let y=coords.y;
//
//
//		// It is exactly like minesweeper :
//		let neighborsPixelsIndexes=getNeighborsPixelsIndexes(x, y, width, height, step);
//		
//		// We ignore single-pixel zones :
//		if(empty(neighborsPixelsIndexes))	continue;
//		
//		let zone=null;
//		
//		let hasAllItsNeighborsUnzoned=true;
//		foreach(neighborsPixelsIndexes,(neighborPixelIndex)=>{
//
//			// Is this neighbor pixel already zoned ?
//			const zonedPixelZoneIndex=zonedPixelsLinearIndexes[neighborPixelIndex];
//			if(zonedPixelZoneIndex!=null){
//				// If so, then we set the currently examined pixel to its zone.
//				zonedPixelsLinearIndexes[i]=zonedPixelZoneIndex;
//				
//				// And we don't need to see more neighbors pixels :
//				hasAllItsNeighborsUnzoned=false;
//				
//				zone=zones[zonedPixelZoneIndex];
//
//
//				// DBG
//				d[i]=Math.min(255,d[i]+zonedPixelZoneIndex*32)+16;
//				
//				return "break";
//			}
//			// If this neighbor pixel is not zoned, then potentially the currently examined pixel is the tip of a brand new zone :
//
//			// DBG
//			d[i]=Math.min(255,d[i]+zonedPixelZoneIndex*20);
//
//
////			// DBG
////			if(!contains(visitedIndexes, neighborPixelIndex)) visitedIndexes.push(neighborPixelIndex);
//
//
//		});
//	
//		// If this pixel has all its neighbors without a zone, then it is the tip of a new zone :
//		if(hasAllItsNeighborsUnzoned){
//			// Case new zone :
//			zonedPixelsLinearIndexes[i]=zoneIndex;
//			zone=[];
//			zones.push(zone);
//
//			zoneIndex++;
//			
//			// DBG
////			d[i+2]=Math.min(255,d[i+2]+zoneIndex*20);
//			d[i+2]=255;
//
//		}
//
//
////		// DBG
////		let neighborsPixelsCoords=getNeighborsPixelsCoordinates(x, y, width, height, step);
////		foreach(neighborsPixelsCoords,(coords)=>{
////			if(!coords)	return "continue";
////			
////			zone.push({x:coords.x,y:coords.y});
////		});
//
//
//
//			
//		zone.push({x:x,y:y, color:{r:r,g:g,b:b}});
//		
//		
////		visitedIndexes.push(i);
//		
//	}
//
//
//	// DBG
//	console.log("zonedPixelsLinearIndexes:",zonedPixelsLinearIndexes);
//	console.log("zones:",zones);
//
//
//	// Barycenters calculation :
//	let barycenters=Math.getBarycentersFromZonesWithColors(zones);
//
//
//	return barycenters;
//}




// (UNUSED)
function isPointKeptByDistanceDiff(allPoints,point){

	let keepPoint=false; 


	let squaredDistances=[];
	foreach(allPoints,(p2)=>{
		let squaredDistance=Math.getSquaredDistance(point,p2);
		squaredDistances.push(squaredDistance);
	},(p2)=>{ return point!==p2; });
	
	

	const NUMBER_OF_MAX_DISTANCES=2;
	const DISTANCES_DIFF_THRESHOLD=8;
	
	
	
//		// DBG
//		let keepPoint=true; 
	
	
	// We only keep the points where its 2 lowest distances to others are close enough : 
	// 2 = «equilateral triangle», or «any same-sided polygon»
	// 3 = «central pointed hexagon»
	// 4 = «simple snowflake»
	// 5 = «david star»
	let distancesArrayLength=squaredDistances.length;
	if(distancesArrayLength<=0)	return keepPoint;
	
	// At this point, we know all the distances from this point to all the other points :
	
	
	let oldDistance=null;
	foreach(squaredDistances,(d, i)=>{
	
		if(NUMBER_OF_MAX_DISTANCES<=i){
			return "break";
		}
		
		
		// Differences between smallest distances must mot be to great :
		if(oldDistance!==null){
			let diff=d -oldDistance;
			if(Math.abs(diff)<DISTANCES_DIFF_THRESHOLD){
				keepPoint=true;
			}else{
				return "break";
			}
		}
		
		oldDistance=d;
	
	
		
	},null,(d1,d2)=>{
		return (d1===d2?0:(d1<d2?-1:1));
	});
	


	return keepPoint;
}




function isPointKeptByClosedLoop(allPoints, point, parentPoint=null, originPoint=null,visitedPoints=[] ,ctx/*DBG*/){

	if(point===originPoint)	return true;
	
	if(contains(visitedPoints,point))	return false;
	
	if(!parentPoint)	originPoint=point;
	else				visitedPoints.push(point);


	let squaredDistances=[];
	foreach(allPoints,(p2)=>{
		let squaredDistance=Math.getSquaredDistance(point,p2);
		
		squaredDistances.push({squaredDistance:squaredDistance, destination:p2});
	},(p2)=>{ return point!==p2; });
	

	let keepPoint=false; 
	
//		// DBG
//		let keepPoint=true; 
	
	
	let distancesArrayLength=squaredDistances.length;
	if(distancesArrayLength<=0)	return keepPoint;
	
	
	// At this point, we know all the distances from this point to all the other points :
	
	// DBG 
	console.log("«««««««««««««««««");

	foreach(squaredDistances,(dInfo, i)=>{
	
		let childPoint=dInfo.destination;
		
		
//		//DBG
//		ctx.save();
//		ctx.moveTo(point.x,point.y);
//		ctx.lineTo(childPoint.x,childPoint.y);
//		ctx.strokeStyle="#FF8888";
//		ctx.stroke();
//		ctx.restore();
		
		
		
		// TODO : FIXME : DOES NOT WORK !
		
		
		// DBG
		console.log("squaredDistance:",dInfo.squaredDistance+" "+childPoint.x+":"+childPoint.y);
		

//		// We try to find a closed equilateral loop figure :
//		let isPointKept=isPointKeptByClosedLoop(allPoints, childPoint, point, originPoint, visitedPoints ,ctx/*DBG*/);
//		if(isPointKept){
//			keepPoint=true;
//			// We only look at the smallest calculated distance that is in a loop :
//			return "break";
//		}
		
		
	},(dInfo)=>{	return !contains(visitedPoints,dInfo.destination);	}
	 ,(dInfo1,dInfo2)=>{
		let d1=dInfo1.squaredDistance, d2=dInfo2.squaredDistance;
		return (d1===d2?0:(d1<d2?-1:1));
	});

	// DBG 
	console.log("»»»»»»»»»»»»»»»»»»»");


	return keepPoint;
}



function filterPoints(allPoints ,ctx/*DBG*/){

	const points=[];
	
	
	// We only keep the marker points :

	
	let numberOfKeptPoints=0;
	foreach(allPoints,(p)=>{
		
		
		// TODO : Develop... : FINISH THIS :
		// DBG
		let keepPoint=isPointKeptByDistanceDiff(allPoints,p); 
		
		// TODO : FIXME : DOES NOT WORK !
//		let keepPoint=isPointKeptByClosedLoop(allPoints,p, null, null, [] ,ctx/*DBG*/); 
		
		
		if(keepPoint){
			points.push(p);
			numberOfKeptPoints++;
		}
		
	});
	
	
	const drawCallBack=(ctx,x,y,ratioW=1,ratioH=1)=>{

//		// TODO.
//		foreach(allPoints,(p, i)=>{
//			ctx.save();
//			ctx.strokeStyle="#FF8888";
//			ctx.strokeText(i, (x*ratioW+p.x), (y*ratioH+p.y));
//			ctx.restore();
//		});

	};
	
	
	return {points:points, drawCallBack:drawCallBack};
}




//Deprecated : use getMediaHandler directly
/*public*/getWebcamHandler=function(
//	videoTagId
	){
	return getMediaHandlerIMPL({video:1,audio:1}
	//,null,videoTagId?("canvasFor_"+videoTagId):null,videoTagId
			);
};

// CAUTION : Returns nothing if we use a webcamIndex in the videoConfigParam config parameter object !
/*public*/getMediaHandler=function(videoConfigParam=null,audioConfigParam=null,
//		sourcesIndexes, 
		/*OPTIONAL*/size=null,
		/*OPTIONAL*/tagConfig={},
		/*OPTIONAL*/audioBufferSize=null,/*OPTIONAL*/numberOfAudioChannels=null,
		/*OPTIONAL*/doOnStart=null,
		/*OPTIONAL*/doFinallyWhenMultipleDevices=null
){
	
	if(!isUserMediaAvailable()){
		// TRACE
		log("ERROR : User medias is not supported in your browser (or are you running in a non-https context ?). Aborting");
		return null;
	}
	
	const cameras=[];
	const microphones=[];
	navigator.mediaDevices.enumerateDevices().then(function(devices){

		devices.forEach(function(d){
			if(d.kind==="videoinput")				cameras.push(d);
			else if(d.kind==="audioinput")	microphones.push(d);
	  });
			
		// CAUTION : On today this method only handles media handlers with a single track :

		let constraints={video:(videoConfigParam!=null), audio:(audioConfigParam!=null)}; // Default constraints
		if(!empty(cameras) && videoConfigParam){
			let webcamIndex=videoConfigParam.webcamIndex;
			if(webcamIndex!=null){
				const deviceIndex=Math.min(cameras.length-1, webcamIndex);
				constraints={ 
						video:{
							deviceId:{ exact: cameras[deviceIndex].deviceId }
						}
					};
			}
		}else if(!empty(microphones) && audioConfigParam){
			let microphoneIndex=audioConfigParam.microphoneIndex;
			if(microphoneIndex!=null){
				const deviceIndex=Math.min(microphones.length-1, microphoneIndex);
				constraints={ 
						audio:{
							deviceId: { exact: microphones[deviceIndex].deviceId }
						}
					};
			}
		}
		
		
		let mediaHandler=getMediaHandlerIMPL(constraints, size, tagConfig, audioBufferSize, numberOfAudioChannels, doOnStart, {cameras:cameras, microphones:microphones});
		if(doFinallyWhenMultipleDevices)	doFinallyWhenMultipleDevices(mediaHandler);
			
	});
	
	
};


// It is always a first a video (webcam handler), with some added audio if needed,
// or no video (sound only, just like in the Evangelion virtual conference room):
/*private*/function getMediaHandlerIMPL(mediaConstraints,
//		sourcesIndexes/*(describes which index is video and which index is audio, among possibilities)*/, 
		/*OPTIONAL*/size=null,
		/*OPTIONAL*/tagConfig={},
//	/*OPTIONAL*/canvasTagId,/*OPTIONAL*/videoTagId,
		/*OPTIONAL*/audioBufferSize=null,/*OPTIONAL*/numberOfAudioChannels=null,
		/*OPTIONAL*/doOnStart=null,
		devicesInfo=null){
	
//	const CANVASTAG_ID="canvasElement";
//	const VIDEOTAG_ID="videoElement";
	
	const DEFAULT_AUDIO_BUFFER_SIZE=4096;
	// const DEFAULT_WIDTH=1920;
 	// const DEFAULT_HEIGHT=1080;
	const DEFAULT_WIDTH=1280;
 	const DEFAULT_HEIGHT=720;
	
	let width=size && size.width?size.width:null;
	let height=size && size.height?size.height:null;
	
	
	audioBufferSize=nonull(audioBufferSize,DEFAULT_AUDIO_BUFFER_SIZE);
	numberOfAudioChannels=nonull(numberOfAudioChannels,1);//default is «mono»

  //canvasTagId=canvasTagId?canvasTagId:CANVASTAG_ID;
  //videoTagId=videoTagId?videoTagId:VIDEOTAG_ID;
//  var canvasTagId=CANVASTAG_ID;
//  var videoTagId=VIDEOTAG_ID;

	// 0- Basic validation :
	if(!mediaConstraints){
		// TRACE
		log("ERROR : Are you kidding me ? no media constraint has been set for this mediaHandler !");
		return null;
	}
	const isVideo=!!(mediaConstraints.video);
	const isAudio=!!(mediaConstraints.audio);
	if(!isVideo && !isAudio){
		// TRACE
		log("ERROR : Are you kidding me ? nor audio nor video has been selected for this mediaHandler !");
		return null;
	}
	
	
	if(!isUserMediaAvailable()){
		// TRACE
		log("ERROR : User medias is not supported in your browser (or are you running in a non-https context ?). Aborting");
		return null;
	}

//	if(!navigator.getUserMedia){
//		// TRACE
//		log("ERROR : Your browser doesn't have a getUserMedia() usable method.");
//		return null;
//	}

	// 1- Init the media handler :
	let mediaHandler={
		devicesInfo:devicesInfo,
//		UNUSED : mediaConstraints:mediaConstraints,
		// technical attributes :
		video:null, // This is where you will read the video image data !
		canvasTag:null,
		canvasCtx:null,
		audioData:[],// Where to store sound data
		audioCtx:null,
		
		videoDeviceId:(mediaConstraints.video?mediaConstraints.video.deviceId.exact:null),
		audioDeviceId:(mediaConstraints.audio?mediaConstraints.audio.deviceId.exact:null),
		
		imageInterceptors:[],
		appendImageInterceptor:(method)=>mediaHandler.imageInterceptors.push(method),
		
		// "public" mandatory interface methods :
		getVideoData:function(){	return null; },
		getAudioData:function(){	return null; },
		getAudioSampleRate:function(){
			if(!mediaHandler.audioCtx)	return null;
			return mediaHandler.audioCtx.sampleRate;
		},
				    					
		// UNUSED
//		,isReady:function(){return false;}
										  
	}; 


// UNUSED :	const backgroundContainerDiv=document.body;

	
	// This temporary, off-screen canvas is always used to get the image data for further use :
	let canvasTag=document.createElement("canvas");
	if(width)		canvasTag.width=width;
	if(height)	canvasTag.height=height;
//canvasTag.style.display="none";


	// This temporary, off-screen video is always needed to get the image data for further use :
	let videoTag;
	if(!tagConfig || !tagConfig.videoToUse){
		videoTag=document.createElement("video");
		videoTag.id="tmpVideoElement";
		videoTag.autoplay=true;
		videoTag.width=canvasTag.width;
		videoTag.height=canvasTag.height;
		videoTag.style.width=canvasTag.width+"px";
		videoTag.style.height=canvasTag.height+"px";

//		// UNUSEFUL (SINCE VIDEO ELEMENT IS NEVER APPENDED TO PAGE !) :
//		videoTag.style.position="absolute";
//		videoTag.style["pointer-events"]="none";
//		videoTag.style.display="none";
//		videoTag.style.display="none";
//		document.body.appendChild(videoTag);

	}else{
		videoTag=tagConfig.videoToUse;
	}
	
	
		
	mediaHandler.video=videoTag;


	// UNUSED
//	mediaHandler.isReady=function(){
////	return mediaHandler.video !== null;
//		return !!(mediaHandler.stream);
//	};
	
	// 2- Method to start the media stream :
	const startStream=function(constraints){
		
		if(!constraints){
			//constraints={video: (isVideo?{zoom:true,width:{ideal:nonull(width,DEFAULT_WIDTH)},height:{ideal:nonull(height,DEFAULT_HEIGHT)}}:false), audio: isAudio};
			// DBG
			constraints={video: (isVideo?
								{width:nonull(width,DEFAULT_WIDTH),height:nonull(height,DEFAULT_HEIGHT)}
//// ALTERNATIVELY, AND MORE FINE_TUNED  :
//							{zoom:true,mandatory: {
//                            minWidth: 1280,
//                            maxWidth: 1280,
//                            minHeight: 720,
//                            maxHeight: 720,
//                            }
//							}
							:false), audio: isAudio};
		}
		
		// TRACE
		console.log("INFO : Trying to start mediaHandler stream with constraints :",stringifyObject(constraints));

		navigator.mediaDevices.getUserMedia(constraints).then(
				/* use the stream */
	    	function(stream){
	        
	    		// UNUSED
	//    	mediaHandler.stream=stream;
	
					const streamSettings = stream.getVideoTracks()[0].getSettings();
	
	
					// DBG
					lognow("(DEBUG) streamSettings:",streamSettings);
					lognow("(DEBUG) stream:",stream);
					lognow("(DEBUG) stream.getVideoTracks():",stream.getVideoTracks());
					
	
					if(streamSettings){
						if(!canvasTag.width &&  streamSettings.width)	width=streamSettings.width;
						if(!canvasTag.height && streamSettings.height)	height=streamSettings.height;
						if(!canvasTag.width &&  width)		canvasTag.width=width;
						if(!canvasTag.height && height)		canvasTag.height=height;
					}

	
	      	// Video part :
	    		if(isVideo){
	    			
	    			// TRACE
	    			lognow("INFO : MEDIA HANDLER : Starting video...");
	    			
	    			// We always have a video tag (to grab the video data)
	  				if(!isArray(mediaHandler.video)){
	  					
	  					mediaHandler.video.srcObject=stream;
//	  					DOES NOT WORK : mediaHandler.video.src=stream;
	  					
	  					// NO : DOES NOT WORK (CREATES A BLANK IMAGE !) mediaHandler.video.muted=true;
	  					// STUPID WORKAROUND : Because apparently, if volume==0 or video is muted, then the image data is all black !!!
	  					mediaHandler.video.volume=0.0000000000001;
	//  				mediaHandler.video.autoplay=true;
	
							if(tagConfig && tagConfig.videoToUse && tagConfig.videoScale){
								if(isNumber(tagConfig.videoScale)){
									mediaHandler.video.style.scale=tagConfig.videoScale;
								}
							}
							
	  				}else{	// Audio part :
		
	  					foreach(mediaHandler.video,(v)=>{
	  						
	  						v.srcObject=stream;
	  						
	  						// NO : DOES NOT WORK (CREATES A BLANK IMAGE !) v.muted=true;
	    					// STUPID WORKAROUND : Because apparently, if volume==0 or video is muted, then the image data is all black !!!
	  						v.volume=0.0000000000001;
	//  					v.autoplay=true;
	
	  					});
	  				}
	
	      		
	      		mediaHandler.canvasTag=canvasTag;
	      		mediaHandler.canvasCtx=mediaHandler.canvasTag.getContext("2d");
	
	      		let x=0;
	      		let y=0;
	
	      		let width=mediaHandler.canvasTag.width;
	      		let height=mediaHandler.canvasTag.height;
	
						mediaHandler.getVideoData=function(){

		        		// To read the image data : (code from «PlaneWebcamProvider.js»)
		        		// (in a canvas drawing function with canvasContext ctx :)
		        		let ctx=mediaHandler.canvasCtx;

		        		ctx.save();
		        		let imageObj= (isArray(mediaHandler.video)?mediaHandler.video[0]:mediaHandler.video);
		        		ctx.drawImage(imageObj,x,y,width,height);
		        		let imageData=ctx.getImageData(x, y, width, height);
								foreach(mediaHandler.imageInterceptors,(method)=>{method(imageData.data);});
			    			ctx.restore();
		
								imageData.messWithAlpha=true;
							
			    			return imageData;// returns an object containing data in this form : {data:<image data array>, width:<image width>, height:<image height>, messWithAlpha:true}						  
						};
			      		
	      		// If we have feed actual video visible tags, then we play them :
	      		if(tagConfig && tagConfig.videoToUse && !tagConfig.invisibleVideoTag){
	      			
	      			
	    			if(!isArray(mediaHandler.video)){
			      		mediaHandler.video.onloadedmetadata=function(event){
							let video=event.target;
			      			video.play();
				        	if(tagConfig.videoMuted)	video.muted=true;
				        	else											delete video.muted;
			        	};
	    			}else{
	    				foreach(mediaHandler.video,(v)=>{	
	  		      			v.onloadedmetadata=function(event){
	  		      				let video=event.target;
	  		      				video.play();
						        	if(tagConfig.videoMuted)	video.muted=true;
						        	else							 				delete video.muted;
	  		          	};
	    				});
	    			}
	      			
	      		}

	    	}  // CAUTION : ON TODAY, ONE MEDIA HANDLER ONLY MANAGES EITHER VIDEO OR AUDIO BUT NOT BOTH SIMULTANEOUSLY ! 		        
	    		// NO : (Simultaneously :)
	    		// TO AVOID «could not start audio» ERRORS :
	    	else
	        // Audio part :
	    	if(isAudio){
	    			
	    			let micStream=stream;
	    			let numberOfAudioChannelsIN=numberOfAudioChannels;
	    			let numberOfAudioChannelsOUT=numberOfAudioChannels;
	    			
	    			// TODO : FIXME : DUPLICATED CODE :

	    			// TRACE
	    			lognow("INFO : MEDIA HANDLER : Starting sound...");
	    			
	    			let audioCtx=new AudioContext();
	    			mediaHandler.audioCtx=audioCtx;
	    			let microphone=audioCtx.createMediaStreamSource(micStream);
	    			
	    			// We create the bufferization :
	    			let scriptNode=audioCtx.createScriptProcessor(audioBufferSize,/*INPUT*/numberOfAudioChannelsIN,/*OUTPUT*/numberOfAudioChannelsOUT);
	    			scriptNode.onaudioprocess=function(audioProcessingEvent){
	    			  // The input buffer is the song we loaded earlier
	    			  let inputBuffer=audioProcessingEvent.inputBuffer;
	    			
	    			  // Loop through the output channels (in this case there is only one : we are in «mono» mode)
	    			  for(let channel=0; channel<inputBuffer.numberOfChannels; channel++){
	    			    var inputData=inputBuffer.getChannelData(channel);
	    			    mediaHandler.audioData[channel]=inputData;
	    			  }
	    			  
	    			}
	    			microphone.connect(scriptNode);
	    			scriptNode.connect(audioCtx.destination);
	    			
	    			
	    			// TODO : FIXME : DUPLICATED CODE :
	    			mediaHandler.getAudioData=function(/*OPTIONAL*/channel){
		    				if(!channel)	channel=0;
		    				let audioData=mediaHandler.audioData[channel];
		    				return { data:audioData };
						};
	    		}
	    		
	    		
	    		if((isVideo || isAudio) && doOnStart)		doOnStart(mediaHandler);
	    		
	             
	    	}
    	
			).catch(
			  /* handle the error */
				function(error){
					// TRACE
					console.log("ERROR : (Earlier process) Media handler could not start :",stringifyObject(error));

		
				}
			);
//    );
		
	}
	
	
	// 3- Sources selection :
	// DEPRECATED, use enumerateDevices
	
		
	// We start the stream :
	try{
		startStream(mediaConstraints);
	}catch(error){
		// TRACE
		console.log("ERROR : Media handler could not start because of an unhandled error :",stringifyObject(error));
		return null;
	}
				

	// TRACE
	console.log("INFO : Media handler initialization done.",mediaHandler);
	
	
	return mediaHandler;

};


// cf. https://developer.mozilla.org/fr/docs/Web/API/CanvasRenderingContext2D/putImageData
//function putImageData(ctx, imageData, dx, dy, dirtyX, dirtyY, dirtyWidth, dirtyHeight){
//// THIS CODE ONLY MANAGES SCALE DOWN ! IT CAN BE ENHANCED !
//  var data=imageData.data;
//  var height=imageData.height;
//  var width=imageData.width;
//  dirtyX=dirtyX || 0;
//  dirtyY=dirtyY || 0;
//  dirtyWidth=dirtyWidth !== undefined? dirtyWidth: width;
//  dirtyHeight=dirtyHeight !== undefined? dirtyHeight: height;
//  
//  var limitBottom=Math.min(dirtyHeight, height);
//  var limitRight=Math.min(dirtyWidth, width);
//  for(var y=dirtyY; y<limitBottom; y++){
//    for(var x=dirtyX; x<limitRight; x++){
//      var pos=y * width + x;
//      ctx.fillStyle='rgba(' + data[pos*4+0]
//                        + ',' + data[pos*4+1]
//                        + ',' + data[pos*4+2]
//                        + ',' + (data[pos*4+3]/255) + ')';
//      ctx.fillRect(x + dx, y + dy, 1, 1);
//    }
//  }
//  
//	ctx.putImageData(imageData, dx, dy, dirtyX, dirtyY, dirtyWidth, dirtyHeight);
//
//}


// CAUTION : uses a temporary canvas !
drawVideoImage=function(canvas,videoImage,
		/*OPTIONAL*/xParam,/*OPTIONAL*/yParam,/*OPTIONAL*/zoomsParam,
		/*OPTIONAL*/newWidthParam,/*OPTIONAL*/newHeightParam,
		/*OPTIONAL*/isMessWithAlpha){
	
	var videoRawImageData=videoImage.data;
	var oldWidth=videoImage.width;
	var oldHeight=videoImage.height;
	
	var x=nonull(xParam,0);
	var y=nonull(yParam,0);
	const zooms=nonull(zoomsParam,{zx:1,zy:1});
	
	var newWidth=newWidthParam?newWidthParam:canvas.width;
	var newHeight=newHeightParam?newHeightParam:canvas.height;

	var ctx=canvas.getContext("2d");
	
	let imageData=null;
	let tmpImage=null;
	if(videoImage.format==="base64"){

		tmpImage=new Image();
		tmpImage.src="data:image/jpeg;base64,"+videoRawImageData;
	
	}else{ // «Normal» case :

		// We copy the data in canvas :
		imageData=ctx.getImageData(x, y, oldWidth, oldHeight);
		// CAUTION : DOES NOT WORK :
	//	var imageData=ctx.createImageData(oldWidth, oldHeight);
		normalizeData(videoRawImageData, imageData.data, isMessWithAlpha);

	}

	let clipX=0; // UNUSED
	let clipY=0; // UNUSED

	var scaleX=null;
	var scaleY=null;
	if(!nothing(newWidthParam) || !nothing(newHeightParam)){
		var scaleXCalculated=null;
		var scaleYCalculated=null;
		if(!nothing(newWidthParam) && 0<oldWidth)		scaleXCalculated=newWidthParam/oldWidth;
		if(!nothing(newHeightParam) && 0<oldHeight)		scaleYCalculated=newHeightParam/oldHeight;
  		scaleX=!nothing(scaleXCalculated)?scaleXCalculated:scaleYCalculated; // default values are replaceable (to make sure we actually choose one) !
  		scaleY=!nothing(scaleYCalculated)?scaleYCalculated:scaleXCalculated;
	}
	
	if((nothing(scaleX) && nothing(scaleY))	|| (scaleX==1 && scaleY==1) ){
		ctx.clearRect(x, y, oldWidth, oldHeight);
		
		if(videoImage.format==="base64"){
			tmpImage.onload=function() {
				ctx.save();
			 	ctx.drawImage(tmpImage, x, y);
				ctx.restore();
			};
		}else{

			ctx.save();
			ctx.putImageData(imageData, x, y, clipX, clipY);
			ctx.restore();
		
		}
		
	}else{
	
		if(videoImage.format==="base64"){
			tmpImage.onload=function() {
				ctx.save();
			 	ctx.drawImage(tmpImage, clipX, clipY, tmpImage.width, tmpImage.height, x, y, newWidth, newHeight);
				ctx.restore();
			};
			
		}else{

			ctx.save();
		
			let tempCanvas=document.getElementById("tempCanvas");
			if(nothing(tempCanvas)){
				tempCanvas=document.createElement("canvas");
				tempCanvas.id="tempCanvas";
				tempCanvas.width=newWidth;
				tempCanvas.height=newHeight;
			}
			
			var tempCtx=tempCanvas.getContext("2d");
			
			tempCtx.save();
	
			tempCtx.putImageData(imageData, x, y, clipX, clipY, newWidth, newHeight);
			
			tempCtx.restore();
	
			ctx.scale(nonull(scaleX,scaleY)*zooms.zx, nonull(scaleY,scaleX)*zooms.zy); // default values are replaceable (to make sure we actually choose one) !
	
	//	NO : PROVOKES BLINKING:	ctx.clearRect(0, 0, newWidth, newHeight);
			ctx.drawImage(tempCanvas,x,y);
			
			
			ctx.restore();
			
		}
		
	}
	
}

const MEDIUM_THRESHOLD=128;
//const THRESHOLD_DIFF=128;
getDominantColor=(r,g,b)=>{

//	// To get the dominant color, one component has to be significantly superior to other components :
//	if(THRESHOLD_DIFF<r-g && THRESHOLD_DIFF<r-b)	return "r";
//	if(THRESHOLD_DIFF<g-r && THRESHOLD_DIFF<g-b)	return "g";
//	if(THRESHOLD_DIFF<b-r && THRESHOLD_DIFF<b-g)	return "b";

	if(MEDIUM_THRESHOLD<=r && g<=MEDIUM_THRESHOLD && b<=MEDIUM_THRESHOLD)	return "r";
	if(MEDIUM_THRESHOLD<=g && r<=MEDIUM_THRESHOLD && b<=MEDIUM_THRESHOLD)	return "g";
	if(MEDIUM_THRESHOLD<=b && r<=MEDIUM_THRESHOLD && g<=MEDIUM_THRESHOLD)	return "b";

	return null;
};

// TODO : FIXME : DEVELOP...
getColorFingersPointer=(mediaHandler,colorsPattern="rgb")=>{
	
	const STEP=3;// (webcam images have no alpha canal)
	const DELAY_MILLIS=500; // (to avoid degrading performances too much)
	
	const colorsPatternInfo={};
	for(let i=0;i<colorsPattern.length;i++){
		let prevChar,currentChar,nextChar;
		if(0<i)	prevChar=colorsPattern.charAt(i-1);
		currentChar=colorsPattern.charAt(i);
		if(i<colorsPattern.length-1)	nextChar=colorsPattern.charAt(i+1);
		const info=[];
		if(prevChar)	info.push(prevChar);
		if(nextChar)	info.push(nextChar);
		colorsPatternInfo[currentChar]=info;
	}
	
		
	const self={
		mediaHandler:mediaHandler,
//	debugPixelsIndexes:[],
		time:null,
		update:()=>{

			// To avoid degrading performances too much :
			if(!hasDelayPassed(self.time,DELAY_MILLIS))	return;
			self.time=getNow();
			
			const img=self.mediaHandler.getVideoData();
			const width=img.width;
			const height=img.height;
			const pixels=img.data;
			
			
			
			const coloredPixels=[];
			for(let i=0;i<pixels.length;i+=STEP){

				const r=pixels[i];
				const g=pixels[i+1];
				const b=pixels[i+2];
				const color=getDominantColor(r,g,b);
				if(!color)	continue;
				
				coloredPixels[i]=color;
			}
			
			// DBG
			lognow("coloredPixels.length:"+coloredPixels.length);
		
			
			const matchingPixelsIndexes=[];
			foreach(coloredPixels,(pixelColor,iStr)=>{
				
				const i=parseInt(iStr);
				
				// We look for pixels which neighbors are according to the colors pattern :

				const coords=getCoordinatesForLinearIndex(i,width,STEP);
				const x=coords.x;
				const y=coords.y;
				const neighborsIndexes=getNeighborsPixelsIndexes(x,y,width,height,STEP);
				
				let hasNeighborsOfTheRightColors=false;
				const expectedColors=colorsPatternInfo[pixelColor];
				foreach(neighborsIndexes,(neighborIndex)=>{
					const neighborColor=coloredPixels[neighborIndex];
					if(contains(expectedColors,neighborColor)){
						hasNeighborsOfTheRightColors=true;
						return "break";
					}
				});

				if(hasNeighborsOfTheRightColors)	matchingPixelsIndexes.push(i);
				
			});

			// TODO : FIXME :
			
			// DBG
			lognow("matchingPixelsIndexes.length:"+matchingPixelsIndexes.length);
			
//		self.debugPixelsIndexes=matchingPixelsIndexes;
				
//			foreach(matchingPixelsIndexes,(i)=>{
//			});


			// ---------------------------------------------------------					
//			// NO : FREEZES THE BROWSER : 
//			const MEDIUM_THRESHOLD=128;
//			const barycenters={};
//			barycenters["r"]=getContigousPixelsGroupsBarycentersInfo(pixels,width,STEP,(r,g,b,a=null)=>{
//				if(MEDIUM_THRESHOLD<=r && g<=MEDIUM_THRESHOLD && b<=MEDIUM_THRESHOLD)	return true;
//				return false;
//			});
//			barycenters["g"]=getContigousPixelsGroupsBarycentersInfo(pixels,width,STEP,(r,g,b,a=null)=>{
//				if(MEDIUM_THRESHOLD<=g && r<=MEDIUM_THRESHOLD && b<=MEDIUM_THRESHOLD)	return true;
//				return false;
//			});
//			barycenters["b"]=getContigousPixelsGroupsBarycentersInfo(pixels,width,STEP,(r,g,b,a=null)=>{
//				if(MEDIUM_THRESHOLD<=b && r<=MEDIUM_THRESHOLD && g<=MEDIUM_THRESHOLD)	return true;
//				return false;
//			});
//			// DBG
//			lognow("barycenters:",barycenters);
			// ---------------------------------------------------------
			
	
			
		},
	};
	
	
//	self.mediaHandler.appendImageInterceptor((pixels)=>{
//	
//		foreach(self.debugPixelsIndexes,(i)=>{
//			pixels[i]=128;
//			pixels[i+1]=128;
//			pixels[i+2]=128;
//		});
//	});
	
	return self;
};













// ====================================================== Sound handling ======================================================


function playAudioData(audioData,audioCtxParam=null,audioBufferSizeParam=null,numberOfAudioChannelsParam=null,sampleRateParam=null,
//sourceParam=null, bufferParam=null, 
	doOnEndPlayingSample=null,
	playbackRateParam=2
){
	
	const DEFAULT_AUDIO_BUFFER_SIZE=4096;
	
//	 To read the audio data :
//   simply perform a reading of mediaHandler.audioData
//   To play the sound :
	
	let audioCtx=nonull(audioCtxParam,new AudioContext());
	let audioBufferSize=nonull(audioBufferSizeParam,DEFAULT_AUDIO_BUFFER_SIZE);
	let numberOfAudioChannels=nonull(numberOfAudioChannelsParam,1);//default is «mono»
	let sampleRate=nonull(sampleRateParam,audioCtx.sampleRate);
  
  // We have to recreate them each time ! :
  let source=audioCtx.createBufferSource();
  let buffer=audioCtx.createBuffer(numberOfAudioChannels, audioBufferSize, sampleRate); // Only one channel for «mono»

  let data=buffer.getChannelData(0);
	for(let i=0; i<audioBufferSize; i++){
	 data[i]=audioData[i];
	}
	if(!source.buffer){
		source.playbackRate.value=playbackRateParam;
		source.buffer=buffer;
		source.connect(audioCtx.destination);
	// TODO : FIXME : DOES NOT CURRENTLY WORK :  	
	// BECAUSE OF INTERACTION-FIRST RESTRICTIONS WILL MAKE IT NOT WORK !
	  // OLD : source.start(0);
	  source.start();
	  
//	  // DBG
//	  lognow("audioSource STARTED : ",source);
	}
  	
  
  if(doOnEndPlayingSample){
	  const playingDurationMillis=(audioBufferSize/sampleRate);
	  // DBG
//	setTimeout(doOnEndPlayingSample,playingDurationMillis*1000); (because Hertz)
		doOnEndPlayingSample();
	}
  
  // OLD :
  //	// Fill the buffer with data from array;
  //	// Loop through the output channels (in this case there is only one : we are in «mono» mode)
  //	for(var channel=0; channel<buffer.numberOfChannels; channel++){
  //	  // This gives us the actual ArrayBuffer that contains the data
  //	  var nowBuffering=myArrayBuffer.getChannelData(channel);
  //	  for(var i=0; i<myArrayBuffer.length; i++){
  //	    nowBuffering[i]=audioData[i];
  //	  }
  //	}
  
  // OLD :  
  // cf. https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/createBuffer
  //  https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/createBuffer
  // // Get an AudioBufferSourceNode.
  // // This is the AudioNode to use when we want to play an AudioBuffer
  // var source=audioCtx.createBufferSource();
  // // set the buffer in the AudioBufferSourceNode
  // source.buffer=audioData;// The buffer array with the audio data
  // // connect the AudioBufferSourceNode to the
  // // destination so we can hear the sound
  // source.connect(audioCtx.destination);
  // // start the source playing
  // source.start();
	
  	
  	return audioCtx;
}


class SoundsLoop{
	constructor(doOnEnded=null){

		this.doOnEnded=doOnEnded;
		
		this.audios=null;
		this.currentAudio=null;
		this.hasAbortedLooping=false;
		this.hasLoopStarted=false;
		this.volume=1;
		this.mainLoopSound=null;
		this.started=false;
	
	}

	setVolume(newVolume=null){
		if(newVolume)	this.volume=newVolume;
		return this;
	}
	loop(pauseMillis=null,fadeInMillis=null,fadeOutMillis=null){
		
		this.currentAudio=Math.getRandomInArray(this.audios);
		this.currentAudio.setVolume(this.volume);

		this.pauseMillis=pauseMillis;
		this.fadeInMillis=fadeInMillis;
		this.fadeOutMillis=fadeOutMillis;
		
		// If it is the first time of the loop then there is no delay at the start:
		// or if we have no pause between loopings :
		this.started=true;
		if(!this.hasLoopStarted || !this.pauseMillis){
			this.startChainedLoop();
		}else{
			this.mainLoopSound=getTimer(this.startChainedLoop,this.pauseMillis);
		}
		
		return this;
	}
	
	/*private*/startChainedLoop(){
		if(!this.started)	return;
		
		const self=this;
		if(this.fadeInMillis && this.fadeOutMillis){
			if(this.fadeInMillis){
				this.currentAudio.fadeIn(this.fadeInMillis);
			}
			if(this.fadeOutMillis){
				this.currentAudio.fadeOut(this.fadeOutMillis,()=>{
					// The next one to loop :
					if(self.started)		self.loop(self.pauseMillis,self.fadeInMillis,self.fadeOutMillis);
					if(self.doOnEnded)	self.doOnEnded(self);
				});
			}
			
		}else{
			if(!this.fadeInMillis){
				this.currentAudio.jumpIn();
			}
			if(!this.fadeOutMillis){
				this.currentAudio.jumpOut(()=>{
						// The next one to loop : (the same, loop on itself)
						if(self.started)		self.loop(self.pauseMillis,self.fadeInMillis,self.fadeOutMillis);
						if(self.doOnEnded)	self.doOnEnded(self);
					}
				);
			}
		}

		this.hasLoopStarted=true;
	}
	
	
	stopPlaying(){
		// To prevent starting treatments after the loading has occurred after the loop has been stopped !
		// (rare case when the loading is so long that the user has aborted the loop before all loop files has been loaded !)
		if(!this.audios)	this.hasAbortedLooping=true;
		// (It is necessarily a loop !)
		this.started=false;
		if(this.mainLoopSound)	this.mainLoopSound.clear();
		if(this.currentAudio)	this.currentAudio.stopPlaying();
		return this;
	}
	
}

function loadSoundsLoop(filesPaths,doOnLoaded=null,doOnEnded=null){
	
	const selfSoundsLoop=new SoundsLoop(doOnEnded);
	
	let audios=[];
	let numberLoaded=0;
	foreach(filesPaths,(filePath)=>{
		audios.push( loadSound(filePath,function(selfParam/*UNUSED*/){
			
			numberLoaded++
			if(doOnLoaded && numberLoaded==filesPaths.length){
				
				// Once all have been loaded :
				selfSoundsLoop.audios=audios;

				// To prevent starting treatments after the loading has occurred after the loop has been stopped !
				// (rare case when the loading is so long that the user has aborted the loop before all loop files has been loaded !)
				if(!selfSoundsLoop.hasAbortedLooping)		doOnLoaded(selfSoundsLoop);
				
			}
			
		}) );
	});
	
	return selfSoundsLoop;
}



const FADE_REFRESH_MILLIS=500;

class Sound{
	constructor(filePath,doOnEnded=null){

		this.doOnEnded=doOnEnded;
	
		this.nativeAudioElement=new Audio(filePath);
		this.isLoop=false;
		this.hasLoopStarted=false;
		this.isReady=false;
		this.speed=1;
		this.volume=1;
		this.isPlaying=false;
		this.mainLoopSound=null;
		
	}
	
	
	setVolume(newVolume=null){
		if(newVolume)	this.volume=newVolume;
		this.nativeAudioElement.volume=this.volume;
		return this;
	}
	setSpeed(newSpeed=null){
		if(newSpeed)	this.speed=newSpeed;
		this.nativeAudioElement.playbackRate=this.speed;
		return this;
	}
	play(){
		if(!this.nativeAudioElement.paused)	return this;
		this.nativeAudioElement.play().then(()=>{
			this.isPlaying=true;
		});
		return this;
	}
	pause(){
		if(this.nativeAudioElement.paused || !this.isPlaying)	return this;
		this.nativeAudioElement.pause();
		this.isPlaying=false;
		return this;
	}
	resumeLoop(){
		if(!this.isLoop || !this.mainLoopSound)	return this;
		this.mainLoopSound.resume();
		this.play();
		return this;
	}
	pauseLoop(){
		if(!this.isLoop || !this.mainLoopSound || !this.isPlaying)	return this;
		this.mainLoopSound.pause();
		this.pause();
		return this;
	}
	
	startPlaying(){
		if(this.isLoop){
			this.loop();
		}else{
			this.play();
		}
	}
	
	setIsLoop(isLoop){
		this.isLoop=isLoop;
		return this;
	}
	
	stopPlaying(){
		// NO : We have to stop the audio file even if it's looping :
		// TODO : FIXME : FIND A BETTER WAY !!
		this.pause();
		this.nativeAudioElement.currentTime=0;
		if(this.isLoop && this.mainLoopSound){
			this.hasLoopStarted=false;
			this.mainLoopSound.clear();
		}
		return this;
	}
	
	loop(pauseMillis=null,fadeInMillis=null,fadeOutMillis=null){

		if(!this.isLoop)	return this;
		
		// At this point, this.isLoop is necessarily true

		// Some of the more commonly used properties of the audio element include : 
		// src, currentTime, duration, paused, muted, and volume
		this.pauseMillis=pauseMillis;
		this.fadeInMillis=fadeInMillis;
		this.fadeOutMillis=fadeOutMillis;
		
		// If it is the first time of the loop then there is no delay at the start:
		if(!this.hasLoopStarted || !this.pauseMillis){
			this.startChainedLoop();
		}else{
			this.mainLoopSound=getTimer(this.startChainedLoop, this.pauseMillis);
		}
		
		return this;
	}
	
	/*private*/startChainedLoop(){
		
		const self=this;
		if(this.fadeInMillis && this.fadeOutMillis){
			if(this.fadeInMillis){
				this.fadeIn(this.fadeInMillis);
			}
			if(this.fadeOutMillis){
				this.fadeOut(this.fadeOutMillis,()=>{
					// The next one to loop : (the same, loop on itself)
					self.loop(self.pauseMillis,self.fadeInMillis,self.fadeOutMillis);
					if(self.doOnEnded)	self.doOnEnded(self);
				});
			}
		}else{
			if(!this.fadeInMillis){
				this.jumpIn();
			}
			if(!this.fadeOutMillis){
				this.jumpOut(()=>{
						// The next one to loop : (the same, loop on itself)
						self.loop(self.pauseMillis,self.fadeInMillis,self.fadeOutMillis);
						if(self.doOnEnded)	self.doOnEnded(self);
					}
				);
			}
		}

		this.hasLoopStarted=true;
	}
	
	jumpIn(){
		this.nativeAudioElement.currentTime=0;
		this.play();
	}
	fadeIn(durationMillis){

		this.nativeAudioElement.currentTime=0;
		// We have to bypass the setVolume method here !
		this.nativeAudioElement.volume=0;
		
		let timeRatio=FADE_REFRESH_MILLIS/durationMillis;
		let volumeStep=timeRatio;
		
		const self=this;
		this.fadeRoutine=setInterval(()=>{
			// We have to bypass the setVolume method here !
			self.nativeAudioElement.volume=Math.min(self.volume,
					// and here too :
					self.nativeAudioElement.volume+volumeStep);
		},FADE_REFRESH_MILLIS);
		
		setTimeout(()=>{ clearInterval(self.fadeRoutine); },durationMillis);
		
		this.play();
		
		return this;
	}
	
	jumpOut(doOnEndJump=null){

		const self=this;		
		this.mainLoopSound=getTimer(()=>{
			self.pause();
			if(doOnEndJump)	doOnEndJump(self);
		},this.nativeAudioElement.duration*1000);
		
		return this;
	}
	
	fadeOut(durationMillis,doOnEndFade=null){

		const self=this;
		setTimeout(()=>{
			
			// We have to bypass the setVolume method here !
			self.nativeAudioElement.volume=self.volume;
			
			let timeRatio=FADE_REFRESH_MILLIS/durationMillis;
			let volumeStep=timeRatio;
			
			self.fadeRoutine=setInterval(()=>{
				// We have to bypass the setVolume method here !
				self.nativeAudioElement.volume=Math.max(0,
						// and here too :
						self.nativeAudioElement.volume-volumeStep);
			},FADE_REFRESH_MILLIS);
			
		},(this.nativeAudioElement.duration*1000-durationMillis));
		
		this.mainLoopSound=getTimer(()=>{
			clearInterval(self.fadeRoutine);
			self.pause();
			if(doOnEndFade)	doOnEndFade(self);
		},this.nativeAudioElement.duration*1000);
		
		return this;
	}
	
	isFinished(){
		return (this.nativeAudioElement.currentTime===this.nativeAudioElement.duration);
	}
	
}


function loadSound(filePath,doOnLoaded=null,doOnEnded=null){
	
	let self=new Sound(filePath,doOnEnded);

	self.nativeAudioElement.addEventListener("loadeddata", () => {
		// Some of the more commonly used properties of the audio element include : 
		// src, currentTime, duration, paused, muted, and volume
		self.isReady=true;
		if(doOnLoaded)		doOnLoaded(self);
	});
	
	if(!self.isLoop && doOnEnded){
		self.nativeAudioElement.addEventListener("onended", () => {
			// Some of the more commonly used properties of the audio element include : 
			// src, currentTime, duration, paused, muted, and volume
			doOnEnded(self);
		});
	}
	
	return self;
}





function getZoomedCenteredZoneCoords(xParam,yParam,width,height,center={x:"left",y:"top"}, zooms={zx:1,zy:1}){

	if(center===null)	center={x:"left",y:"top"};
	
	let x=xParam; // case "left"
	if(center.x==="center"){
		x=(xParam-Math.floor(width*.5));
	}else if(center.x==="right"){
		x=(xParam-width);
	}

	let y=yParam; // case "top"
	if(center.y==="center"){
		y=(yParam-Math.floor(height*.5));
	}else if(center.y==="bottom"){
		y=(yParam-height);
	}
	
	return {x:x*zooms.zx, y:y*zooms.zy, w:width*zooms.zx, h:height*zooms.zy, center:center};
}


function drawImageAndCenterWithZooms(ctx,image,xParam=0,yParam=0,zooms={zx:1,zy:1},center={x:"left",y:"top"},
			scaleFactorW=1,scaleFactorH=1,
			sizeW=null,sizeH=null,
			opacity=1,
			preserveContextState=false,
			clip=null,
			alphaAngleRadians=null){

	
	let clipPos=(clip?{x:clip.x, y:clip.y}:{x:0,y:0});
	let clipSize=(clip?{w:clip.w, h:clip.h}:{w:image.width,h:image.height});

	const drawingWidth=nonull(sizeW, Math.trunc(clipSize.w*scaleFactorW));
	const drawingHeight=nonull(sizeH, Math.trunc(clipSize.h*scaleFactorH));
	zooms=nonull(zooms,{zx:1,zy:1});
	const zoomedDrawingWidth=drawingWidth*zooms.zx;
	const zoomedDrawingHeight=drawingHeight*zooms.zy;
	
	const zoomedCenteredCoords=getZoomedCenteredZoneCoords(xParam,yParam,drawingWidth,drawingHeight,center,zooms);
	
	if((opacity!==1 && opacity!==0 ) || alphaAngleRadians!=null){
		if(!preserveContextState || alphaAngleRadians!=null)	ctx.save();
//	TO TRY : used  to cause problems because of .toFixed(...) that converts number into string !	ctx.globalAlpha=Math.roundTo(opacity,2);
		ctx.globalAlpha=opacity;
	}
	
	// TODO : FIXME : use zooms parameter !
	
	if(opacity!==0){

		const x=zoomedCenteredCoords.x;
		const y=zoomedCenteredCoords.y;
			

		if(alphaAngleRadians==null){
		
			ctx.drawImage(image,
					// Clip source :
					clipPos.x, clipPos.y,
					clipSize.w, clipSize.h,
					// Drawing destination (including scaling) :
					x, y,
					zoomedDrawingWidth, zoomedDrawingHeight);
		}else{
		
			let angleRadians=alphaAngleRadians;

			let demiW=zoomedDrawingWidth*.5;
			let demiH=zoomedDrawingHeight*.5;

			// Default rotation center is the center of the image :			
			let rotationCenterOffsetX=demiW;
			let rotationCenterOffsetY=demiH;
			if(center){
				if(center.x=="left")				rotationCenterOffsetX=0;
				else if(center.x=="right")	rotationCenterOffsetX=zoomedDrawingWidth;
				// Default rotation center is the center of the image.
				if(center.y=="top")					rotationCenterOffsetY=0;	 			// CAUTION : Y axis is inverted here !
				else if(center.y=="bottom")	rotationCenterOffsetY=zoomedDrawingHeight;	// CAUTION : Y axis is inverted here !
				// Default rotation center is the center of the image.
			}


			const a=Math.coerceAngle(-angleRadians,true,true); // BECAUSE Y AXIS IS INVERTED !
			const t=Math.rotateAround(a, rotationCenterOffsetX, rotationCenterOffsetY);
			ctx.translate((x+rotationCenterOffsetX-t.x), (y-t.y+rotationCenterOffsetY));
			ctx.rotate(a);

			ctx.drawImage(image,
					// Clip source :
					clipPos.x, clipPos.y,
					clipSize.w, clipSize.h,
					// Drawing destination (including scaling) :
					0,0,
					zoomedDrawingWidth, zoomedDrawingHeight);
		
		}
				
		
	}
	
	if((opacity!==1 && !preserveContextState && opacity!==0) || alphaAngleRadians!=null){
		ctx.restore();
	}
	
	return zoomedCenteredCoords;
}



// ====================================================== SPRITES ======================================================


class SpriteMonoThreaded{
	
	constructor(imagesPool,/*OPTIONAL*/refreshMillis=null,
		clipSize={w:100,h:100},scaleFactorW=1,scaleFactorH=1,
		orientation="horizontal",xOffset=0,yOffset=0,isLoop=true,isRandom=false,
		center={x:"left",y:"top"},
		zoomsParam={zx:1,zy:1}){
		
		this.center=center;
		this.imagesPool=imagesPool;
		this.scaleFactorW=scaleFactorW;
		this.scaleFactorH=scaleFactorH;
		this.xOffset=xOffset;
		this.yOffset=yOffset;
		this.isLoop=isLoop;
		this.zooms=nonull(zoomsParam,{zx:1,zy:1});
		
		// Clip only attributes:
		this.isClip=(!!clipSize);
		this.clipSize=(this.isClip?clipSize:null);
//		width:imagesPool.width,
//		height:imagesPool.height,
		this.index=0;
		this.orientation=orientation;
		this.framesNumber=( this.isClip?(this.orientation==="horizontal" ? Math.trunc(imagesPool.width/clipSize.w) : Math.trunc(imagesPool.height/clipSize.h)) : null );
		this.isRandom=isRandom;
		
		// Time measurement :
		this.time=getNow();
		this.refreshMillis=refreshMillis;
		this.durationTimeFactorHolder={durationTimeFactor:1};
		
		// Two-states : started, stopped.
		
	}
	
	setDurationTimeFactorHolder(durationTimeFactorHolder){
		this.durationTimeFactorHolder=durationTimeFactorHolder;
		return this;
	}
	
	// Draw :
	drawOnEachStepZoomedIfNeeded(ctx,xParam,yParam,angle=null,opacity=1){
		
		let drawingWidth, drawingHeight;
		let img=null;
		let clipX=0;
		let clipY=0;
		if(this.isClip){
			// We only draw a frame : (to avoid blinking)
			drawingWidth=Math.trunc(this.clipSize.w*this.scaleFactorW);
			drawingHeight=Math.trunc(this.clipSize.h*this.scaleFactorH);

			if(this.orientation==="horizontal"){
				clipX=this.index*this.clipSize.w;
				clipY=this.yOffset;
			}else{
				clipX=this.xOffset;
				clipY=this.index*this.clipSize.h;
			}
			
		}else{
			img=Math.getRandomInArray(this.imagesPool);
			this.clipSize={w:img.width,h:img.height};
			
			drawingWidth=Math.trunc(img.width*this.scaleFactorW);
			drawingHeight=Math.trunc(img.height*this.scaleFactorH);
		}

		const xImage=xParam+this.xOffset;
		const yImage=yParam+this.yOffset;
		const zooms=this.zooms;
		
		drawImageAndCenterWithZooms(ctx, nonull(img,this.imagesPool),
			xImage, yImage, 
			zooms,
			this.center,
			(this.scaleFactorW),
			(this.scaleFactorH),
			(!this.isClip?(drawingWidth):null),
			(!this.isClip?(drawingHeight):null),
			opacity,
			false,
			(this.isClip?{x:clipX,y:clipY, w:this.clipSize.w, h:this.clipSize.h}:{x:0,y:0, w:this.clipSize.w, h:this.clipSize.h}),
			angle);

		// Looping index with a delay :
		if(!this.refreshMillis || this.hasDelayPassed()){
			if(this.refreshMillis && this.hasDelayPassed()){
				this.time=getNow();
			}
			
			if(this.isClip){
				// We perform the step forward (if the loop is not finished):
				if(this.isLoop || index!==this.framesNumber){
					if(this.isRandom){
						this.index=Math.getRandomInt(this.framesNumber-1, 0);
					}else{
						this.index=((this.index+1)%(this.framesNumber/*if max is reached, then index is looped to 0*/));
					}
				}
			}

		}
	}
	
	hasDelayPassed(){
		return (!this.time || hasDelayPassed(this.time, this.refreshMillis*this.durationTimeFactorHolder.getDurationTimeFactor()));
	}
	
}


function getSpriteMonoThreaded(imagesPool,/*OPTIONAL*/refreshMillis=null,
		clipSize={w:100,h:100},scaleFactorW=1,scaleFactorH=1,
		orientation="horizontal",xOffset=0,yOffset=0,isLoop=true,isRandom=false,
		center={x:"left",y:"top"},
		zoomsParam={zx:1,zy:1}){

	return new SpriteMonoThreaded(imagesPool,refreshMillis,
		clipSize,scaleFactorW,scaleFactorH,
		orientation,xOffset,yOffset,isLoop,isRandom,
		center,
		zoomsParam);
}

// ====================================================== ROUTINES ======================================================


// ======================== Timeout ========================

// CAUTION : This is not a routine, but a single-use timeout !
class MonoThreadedTimeout{
	constructor(actuator, delayMillis){
		this.actuator=actuator;
	}
	start(delayMillis=null){
		this.started=true;
		this.time=getNow();
		this.delayMillis=delayMillis;
	}
	doStep(args=null){
		if(!this.started)	return;
		if(!hasDelayPassed(this.time, this.delayMillis))	return;
		return this.stop(args);
	}
	stop(args=null){
		if(!this.started)	return;
		this.started=false;
		if(this.actuator.doOnStop)	this.actuator.doOnStop(args);
		return this;
	}
}

function getMonoThreadedTimeout(actuator=null){
	return new MonoThreadedTimeout(actuator);
}


// ======================== Routine ========================

class MonoThreadedRoutine{
	constructor(actuator,refreshMillis=null,startDependsOnParentOnly=false){

		this.actuator=actuator;
		this.startDependsOnParentOnly=startDependsOnParentOnly;
		this.started=false;
		this.paused=false;
		this.time=getNow();
		this.refreshMillis=refreshMillis;
		
		this.durationTimeFactorHolder={durationTimeFactor:1};
		// Three-states : started, paused, stopped.

	}
	
	setDurationTimeFactorHolder(durationTimeFactorHolder){
		this.durationTimeFactorHolder=durationTimeFactorHolder;
		return this;
	}
	isStarted(){
		return this.started || this.startDependsOnParentOnly;
	}
	start(args=null){
		// if(this.isStarted()) return;
		if(this.actuator.terminateFunction && this.actuator.terminateFunction(args)){
			// CAUTION : Even if the routine is «pre-terminated» before even starting 
			// (ie. its stop conditions are fullfilled even before it has started)
			// we all the same have to trigger its stop treatments :
			if(this.actuator.doOnStop)	this.actuator.doOnStop(args);
			return;
		}
		this.started=true;
		this.paused=false;
		return this;
	}
	stop(args=null){
		if(!this.isStarted())	return;
		this.started=false;
		if(this.actuator.doOnStop)	this.actuator.doOnStop(args);
	}
	pause(args=null){
		this.paused=true;
		if(this.actuator.doOnSuspend)	this.actuator.doOnSuspend(args);
	}
	resume(args=null){
		this.paused=false;
		if(this.actuator.doOnResume)	this.actuator.doOnResume(args);
	}
	isPaused(){
		return this.paused;
	}
	doStep(args=null){
		if(!this.isStarted() || this.paused)	return;
		// Looping index with a delay :
		
		const delayHasPassed=hasDelayPassed(this.time, this.refreshMillis*this.durationTimeFactorHolder.getDurationTimeFactor());
		if(!this.refreshMillis || delayHasPassed){
			if(this.refreshMillis && delayHasPassed){
				this.time=getNow();
			}
			// We perform the step :
			if(this.actuator.terminateFunction && this.actuator.terminateFunction(args)){
				this.stop(args);
				return;
			}
			if(this.actuator.doOnEachStep)	this.actuator.doOnEachStep(args);
		}
		
	}
	
}


function getMonoThreadedRoutine(actuator, refreshMillis=null, startDependsOnParentOnly=false){
	return new MonoThreadedRoutine(actuator, refreshMillis, startDependsOnParentOnly);
}


class MonoThreadedGoToGoal{
	constructor(actuator,actualValue,goalValue,refreshMillis=null,totalTimeMillis=1000,mode="linear"){
		
		this.actuator=actuator;
		
		this.actualValue=actualValue;
		this.mode=mode;
		this.totalTimeMillis=totalTimeMillis;
		this.stepValue=((refreshMillis*(goalValue-actualValue))/totalTimeMillis);
		this.value=actualValue;
		this.goalValue=goalValue;
		this.started=false;
		this.paused=false;
		this.time=getNow();
		this.refreshMillis=refreshMillis;
		
		this.durationTimeFactorHolder={durationTimeFactor:1};
	
		// Three-states : started, paused, stopped.
	}
	setDurationTimeFactorHolder(durationTimeFactorHolder){
		this.durationTimeFactorHolder=durationTimeFactorHolder;
		
		// We recalculate total time-dependent values :
		this.totalTimeMillis=this.totalTimeMillis*this.durationTimeFactorHolder.getDurationTimeFactor();
		this.stepValue=((this.refreshMillis*(this.goalValue-this.actualValue))/this.totalTimeMillis);

		return this;
	}
	isStarted(){
		return this.started || this.startDependsOnParentOnly;
	}
	resetValueAndGoalTo(resetValue){
		this.value=resetValue;
		this.goalValue=resetValue;
		return this;
	}
	setNewGoal(goalValue, refreshMillisParam=null, totalTimeMillis=null){
		if(Math.round(this.value)===Math.round(goalValue))	return;
		if(refreshMillisParam)	this.refreshMillis=refreshMillisParam;
		if(totalTimeMillis)			this.totalTimeMillis=totalTimeMillis*this.durationTimeFactorHolder.getDurationTimeFactor();

		this.goalValue=goalValue;
		this.stepValue=((this.refreshMillis*(this.goalValue-this.value))/this.totalTimeMillis);
		
		if(!this.isStarted())	this.start();
			
		return this;
	}
	start(args=null){
		
		if(    this.value===this.goalValue
		//  || this.isStarted();
				|| (this.actuator.terminateFunction && this.actuator.terminateFunction())
				|| this.hasReachedGoal()){

			// CAUTION : Even if the routine is «pre-terminated» before even starting 
			// (ie. its stop conditions are fullfilled even before it has started)
			// we all the same have to trigger its stop treatments :
			if(this.actuator.doOnStop)	this.actuator.doOnStop(args);

			return;
		}
		this.started=true;
		this.paused=false;
		
		return this;
	}
	stop(args=null){
		if(!this.isStarted())	return;
		this.started=false;
		if(this.actuator.doOnStop)	this.actuator.doOnStop(args);
	}
	pause(){
		this.paused=true;
	}
	resume(){
		this.paused=false;
	}
	hasReachedGoal(){
		if(this.stepValue<0){
			if(this.value<=this.goalValue)	return true;
		}else{
			if(this.goalValue<=this.value)	return true;
		}
		return false;
	}
	
	doStep(args=null){
		if(!this.isStarted() || this.paused)	return this.value;

		// Looping index with a delay :
		
		if(!this.refreshMillis || this.hasDelayPassed()){
			if(this.refreshMillis && this.hasDelayPassed()){
				this.time=getNow();
			}
			
			// We check if we must stop :
			if(this.actuator.terminateFunction){
				if(this.actuator.terminateFunction()){
					this.stop();
					this.value=this.goalValue;
					return this.value;
				}
			}else{
				if(this.hasReachedGoal()){
					this.stop();
					this.value=this.goalValue;
					return this.value;
				}
			}
			
			// We perform the step :
			if(this.mode==="exponential"){
				this.value+=Math.pow(this.stepValue,2);
			}else{ // default is "linear"
				this.value+=this.stepValue;
			}
			
			if(this.actuator.doOnEachStep)	this.actuator.doOnEachStep(args);
		}
		
		return this.value;
	}
	
	hasDelayPassed(){
		return (!this.time || hasDelayPassed(this.time, this.refreshMillis*this.durationTimeFactorHolder.getDurationTimeFactor()));
	}

	
}


function getMonoThreadedGoToGoal(actuator,actualValue,goalValue,refreshMillis=null,totalTimeMillis=1000,mode="linear"){
	return new MonoThreadedGoToGoal(actuator,actualValue,goalValue,refreshMillis,totalTimeMillis,mode);
}


// =====================================================================================================================


//if signed :
//when precision==1/10000, range is -3.2766 to 3.2766
//when precision==1, range is -32766 to 32766
//if not signed :
//when precision==1/10000, range is 0 to 6.5535
//when precision==1, range is 0 to 65535
arrayToString=function(arr,compressResult=true,precisionFraction=null,isSigned=false){
	
	const MAX=65535; // (65536-1)
	const MIN_PRECISION=0.00001;
	const MAX_PRECISION=1;
	if(precisionFraction)	precisionFraction=Math.coerceInRange(precisionFraction,MIN_PRECISION,MAX_PRECISION);

	let str="";
	for(let i=0;i<arr.length;i++){
		let val=arr[i];

		if(precisionFraction){
			if(isSigned){
				const OFFSET=32766; // ((65536-1)/2)
				val=(val/precisionFraction)+OFFSET;
			}else{
				val=val/precisionFraction;
			}
			val=Math.coerceInRange(val,0,MAX);
		}
		
		str+=String.fromCharCode(val);
	}

	// TODO : FIXME : DOES NOT WORK !
//	if(compressResult)	str=LZWString.compress(str);
	
	return str;
}

// if signed :
// when precision==1/10000, range is -3.2766 to 3.2766
// when precision==1, range is -32766 to 32766
// if not signed :
// when precision==1/10000, range is 0 to 6.5535
// when precision==1, range is 0 to 65535
function arrayFromString(strParam,compressResult=true,precisionFraction=null,isSigned=false){
	
	const MAX=65535;  // (65536-1)
	const MIN_PRECISION=0.00001;
	const MAX_PRECISION=1;
	if(precisionFraction)	precisionFraction=Math.coerceInRange(precisionFraction,MIN_PRECISION,MAX_PRECISION);

// TODO : FIXME : DOES NOT WORK !
//	let str=compressResult?LZWString.decompress(strParam):strParam;
	let str=strParam;
	
	let arr=[];
	for(let i=0;i<str.length;i++){

		let val=str.charCodeAt(i);

		if(precisionFraction){
			if(isSigned){
				const OFFSET=32766; // ((65536-1)/2)
				val=(val-OFFSET)*precisionFraction;
				val=Math.coerceInRange(val,-OFFSET*precisionFraction,OFFSET*precisionFraction);
			}else{
				val=val*precisionFraction;
				val=Math.coerceInRange(val,0,MAX);
			}
		}

		arr.push(val);
	}
	
	return arr;
}


//OLD CODE :
//function getWebcamHandler(videoTagId){
//
//var b=hasGetUserMedia();
//if (!b){
//	// TRACE
//	log("Method getUserMedia() is not supported in your browser");
//	return null;
//}
//
//var webcamHandler=new Object();
//
//var videoTag=document.getElementById(videoTagId);
//if (videoTag==null){
//	var backgroundContainerDiv=document.getElementById(AOTRA_SCREEN_DIV_ID) ? document.getElementById(AOTRA_SCREEN_DIV_ID) : document.body;
//
//	videoTag=document.createElement("video");
//	videoTag.id=videoTagId;
//	videoTag.autoplay=true;
//	videoTag.style="display:none";
//	appendAsFirstChildOf(videoTag, backgroundContainerDiv);
//}
//
//webcamHandler.video=videoTag;
//
//webcamHandler.localMediaStream=null;
//
//if (navigator.getUserMedia){
//	var self=webcamHandler;
//
//	// Not showing vendor prefixes or code that works cross-browser.
//	var initStream=function(stream){
//		DEPRECATED : 	self.video.src=window.URL.createObjectURL(stream);
//		self.video.srcObject=stream;
//		self.localMediaStream=stream;
//	};
//
//	// TODO ...
//
//	// Webcam selection (if several and if browser supports it :)
//	if (MediaStreamTrack.getSources){
//		MediaStreamTrack.getSources(function(sourceInfos){
//			// var audioSourceId=null;
//			var videoSourceId=null;
//
//			for(var i=0; i != sourceInfos.length; ++i){
//				var sourceInfo=sourceInfos[i];
//
//				// if (sourceInfo.kind === 'audio'){
//				// console.log(sourceInfo.id, sourceInfo.label || 'microphone');
//				//
//				// audioSourceId=sourceInfo.id;
//				// } else
//
//				if (sourceInfo.kind === 'video'){
//					videoSourceId=sourceInfo.id;
//				} else {
//					// TRACE
//					log('Some other kind of source: ' + sourceInfo);
//				}
//			}
//
//			// TODO : FIXME : Currently, we choose the last video source
//			// (probably front, but we are not sure....we should be sure !)...
//
//			var constraints={
//				// audio: {
//				// optional: [{sourceId: audioSourceId}]
//				// },
//				video : {
//					optional : [ {
//						sourceId : videoSourceId
//					} ]
//				}
//			};
//
//			navigator.getUserMedia(constraints, initStream, function(){
//				// In case of not working, or user denied access to her/his webcam :
//				// TRACE
//				log("ERROR: Could not acquire webcam image.");
//			});
//
//		});
//	} else {
//
//		// TRACE
//		log("WARN : Could not use video source selection, using default one.");
//
//		navigator.getUserMedia({
//			video : true
//		}, initStream, function(){
//			// In case of not working, or user denied access to her/his webcam :
//			// TRACE
//			log("ERROR: Could not acquire webcam image.");
//		});
//	}
//
//} else {
//	// TRACE
//	log("Your browser doesn't have a getUserMedia() usable method.");
//	video.src="";
//}
//
//webcamHandler.isReady=function(){
//	return webcamHandler.video !== null;
//};
//
//return webcamHandler;
//}



function playMedia(audioOrVideoElement){
	if (!audioOrVideoElement)
		return;
	if (audioOrVideoElement.paused)
		audioOrVideoElement.play();
	else
		audioOrVideoElement.pause();
}

// vanilla.js AJAX :
//var r=new XMLHttpRequest();
//r.open("POST", "path/to/api", true); 
//r.onreadystatechange=function (){ 
//if (r.readyState != 4 || r.status != 200) return; 
////TRACE
//console.log("Success: " + r.responseText);
//};
//r.send("banana=yellow");












// Image treatments management :
function getAverageOnArea(x1, y1, x2, y2, data, totalWidth){

	let STEP=4;// 3 channels + 1 for alpha channel !

	let result=new Object();
	result.r=0;
	result.g=0;
	result.b=0;

	let cnt=0;
	for(let i=getLinearIndexForCoordinates(x1, y1, totalWidth, STEP); i<getLinearIndexForCoordinates(x2, y2, totalWidth, STEP); i+=STEP){
		if (indexIsOutsideArea(i, x1, y1, x2, y2, data, totalWidth, STEP))
			continue;

		// red
		var red=data[i];
		result.r+=red;
		// green
		var green=data[i + 1];
		result.g+=green;
		// blue
		var blue=data[i + 2];
		result.b+=blue;

		cnt++;

	}

	if (0<cnt){
		result.r=result.r / cnt;
		result.g=result.g / cnt;
		result.b=result.b / cnt;
	}

	return result;
}

function setColorOnArea(color, x1, y1, x2, y2, data, totalWidth){

	var STEP=4;// last int is for alpha canal !
	for(var i=getLinearIndexForCoordinates(x1, y1, totalWidth, STEP); i<getLinearIndexForCoordinates(x2, y2, totalWidth, STEP); i+=STEP){
		// TODO : FIXME : NOT RELIABLE FUNCTION ! TRY NOT TO USE !
		if (indexIsOutsideArea(i, x1, y1, x2, y2, data, totalWidth, STEP))
			continue;

		// red
		data[i]=color.r;
		// green
		data[i + 1]=color.g;
		// blue
		data[i + 2]=color.b;

	}
	
	return color;
}

function offsetColorOnArea(correctionValue, x1, y1, x2, y2, data, totalWidth){

	var STEP=4;// last int is for alpha canal !
	for(var i=getLinearIndexForCoordinates(x1, y1, totalWidth, STEP); i<getLinearIndexForCoordinates(x2, y2, totalWidth, STEP); i+=STEP){
		// TODO : FIXME : NOT RELIABLE FUNCTION ! TRY NOT TO USE !
		if (indexIsOutsideArea(i, x1, y1, x2, y2, data, totalWidth, STEP))
			continue;

		var color=offsetColors(createColor(data[i], data[i + 1], data[i + 2]), correctionValue);

		// red
		data[i]=color.r;
		// green
		data[i + 1]=color.g;
		// blue
		data[i + 2]=color.b;
	}
}

function getLinearIndexForCoordinates(x, y, totalWidth, step){
	return Math.floor((x + y * totalWidth) * step);
}

function getCoordinatesForLinearIndex(index, totalWidth, step=1){
	let actualIndex=index/step;
	let actualWidth=totalWidth;
	let x=Math.floor(actualIndex%actualWidth);
	let y=Math.floor(actualIndex/actualWidth);
	return {x:x, y:y};
}

// TODO : FIXME : NOT RELIABLE FUNCTION ! TRY NOT TO USE !
function indexIsOutsideArea(indexParam, x1, y1, x2, y2, data, totalWidth, step){
	var index=indexParam;
	if (0<step)
		index=index / step;
	return (index % totalWidth<x1 || x2<index % totalWidth) || (index<x1 + y1 * totalWidth || x2 + y2 * totalWidth<index);
}

function createColor(r, g, b){
	var result=new Object();
	result.r=r;
	result.g=g;
	result.b=b;
	return result;
}

function blackAndWhite(colors,/* OPTIONAL */shadesOfGrayNumber){

	var MAX_VALUE=255;

	var result=new Object();
	result.r=0;
	result.g=0;
	result.b=0;

	var valueColor=Math.averageInArray([ colors.r, colors.g, colors.b ]);

	if (shadesOfGrayNumber && 1<shadesOfGrayNumber){

		var portion=Math.floor(MAX_VALUE / shadesOfGrayNumber);

		var minThreshold=0;
		var maxThreshold=portion;

		for(var i=0; i<shadesOfGrayNumber; i++){

			if (Math.max(0, minThreshold - Math.floor(portion *.5)) <= valueColor && valueColor <= maxThreshold - Math.floor(portion *.5))
				valueColor=i * portion;
			else if (minThreshold + Math.floor(portion *.5) <= valueColor && valueColor <= Math.min(MAX_VALUE, maxThreshold + Math.floor(portion *.5)))
				valueColor=(i + 1) * portion;

			minThreshold=maxThreshold;
			maxThreshold+=portion;
		}

	}

	result.r=valueColor;
	result.g=valueColor;
	result.b=valueColor;
	return result;
}

function exacerbateColor(colors){
	var result=new Object();
	result.r=0;
	result.g=0;
	result.b=0;

	var minColor=Math.minInArray([ colors.r, colors.g, colors.b ]);
	var maxColor=Math.maxInArray([ colors.r, colors.g, colors.b ]);
	if (!minColor || !maxColor)
		return result;

	var THRESHOLD_MIN=32;
	if (Math.abs(minColor - maxColor)<THRESHOLD_MIN){
		if ((result.r + result.g + result.b) / 3<THRESHOLD_MIN){
			result.r=0;
			result.g=0;
			result.b=0;
		} else if ((255 - THRESHOLD_MIN)<(result.r + result.g + result.b) / 3){
			result.r=255;
			result.g=255;
			result.b=255;
		}
		return result;
	}

	// if(colors.r===maxColor || colors.r===minColor ){
	// if(colors.r===maxColor) result.r=255;
	// else result.r=0;

	// if(colors.r<127) result.r=0;
	// else result.r=255;
	// }else{
	if (Math.abs(colors.r - minColor) > Math.abs(colors.r - maxColor))
		result.r=255;
	else
		result.r=0;
	// }

	// if(colors.g===maxColor || colors.g===minColor ){
	// if(colors.g===maxColor) result.g=255;
	// else result.g=0;

	// if(colors.g<127) result.g=0;
	// else result.g=255;
	// }else{
	if (Math.abs(colors.g - minColor) > Math.abs(colors.g - maxColor))
		result.g=255;
	else
		result.g=0;
	// }

	// if(colors.b===maxColor || colors.b===minColor ){
	// if(colors.b===maxColor) result.b=255;
	// else result.b=0;

	// if(colors.b<127) result.b=0;
	// else result.b=255;
	// }else{
	if (Math.abs(colors.b - minColor) > Math.abs(colors.b - maxColor))
		result.b=255;
	else
		result.b=0;
	// }

	return result;

}

function offsetColors(colors, offsetValue){
	var result=new Object();
	result.r=Math.max(0, Math.min(255, colors.r + offsetValue));
	result.g=Math.max(0, Math.min(255, colors.g + offsetValue));
	result.b=Math.max(0, Math.min(255, colors.b + offsetValue));
	return result;
}

// UNUSED
// NOT WORKING :
// TODO : FIXME
// function mirrorImage(imageDataParam,width,height){
// var STEP=4;
//	
// var imageData=imageDataParam;
// var data=imageData.data;
//    
// var x=0,y=0;
// for(var i=getLinearIndexForCoordinates(0,0,width,STEP);
// i<getLinearIndexForCoordinates(Math.floor(width*.5),height,width,STEP);
// i+=STEP){
//		
//		
// var i1=getLinearIndexForCoordinates(x,y,STEP);
// var i2=getLinearIndexForCoordinates(width+width%2-x,y,STEP);
// swapInArray(data, i1, i2);
// swapInArray(data, i1+1, i2+1);
// swapInArray(data, i1+2, i2+2);
//
//		
// x++;
// if(width<=x){
// x=0;
// y++;
// }
//		
// }
//	
// return imageData;
// }

// Filters applied to an image stream :

var Filters=new Object();

Filters.areaPixellationForAotraMarkFilter=new Object();
Filters.areaPixellationForAotraMarkFilter.filter=function(imageDataParam,/* OPTIONAL */width,/* OPTIONAL */height){

	var imageData=imageDataParam;
	var data=imageData.data;

	// Tuning all settings values :
	// -Limited amount of shades of gray to simplify image :
	// var NUMBER_OF_SHADES_OF_GRAY=16;
	var NUMBER_OF_SHADES_OF_GRAY=16;
	// var NUMBER_OF_SHADES_OF_GRAY=32;
	// var ALLOWED_DIFFERENCE=32;
	var ALLOWED_DIFFERENCE=Math.round(255 / NUMBER_OF_SHADES_OF_GRAY) * 2;

	// -Cells to simplify image :
	// WORKS :
	// var CELL_SIZE_PX=20;
	// var REDUCTION_FACTOR=0.5;
	var CELL_SIZE_PX=10;
	var REDUCTION_FACTOR=0.5;

	// *******************************************************************************************************
	// Step 1 : we normalize image :
	// To make reduced area square :
	var AREA_SIZE=Math.min(width, height);
	// CAUTION : Math.trunc(...) function does not exist on every browser, so we
	// replace it with its universal equivalent Math.floor(...).
	var REDUCED_AREA_X1=Math.floor(width *.5 - AREA_SIZE * (REDUCTION_FACTOR *.5));
	var REDUCED_AREA_Y1=Math.floor(height *.5 - AREA_SIZE * (REDUCTION_FACTOR *.5));
	var REDUCED_AREA_X2=Math.floor(width *.5 + AREA_SIZE * (REDUCTION_FACTOR *.5));
	var REDUCED_AREA_Y2=Math.floor(height *.5 + AREA_SIZE * (REDUCTION_FACTOR *.5));

	// We etalonnate and degrade the image :
	// ------------------------------------------------------------------
	// - We will look for the max color (actually N&B) value first to etalonnate
	// (ie. render image constant whatever is the luminosity)
	var maxValue=0;
	var pixs=new Object();

	// (A 2D-based restriction area calculation) :
	for(var x=REDUCED_AREA_X1; x <= REDUCED_AREA_X2; x+=CELL_SIZE_PX){ // X
		// goes
		// forward
		for(var y=REDUCED_AREA_Y1; y <= REDUCED_AREA_Y2; y+=CELL_SIZE_PX){ // Y
			// goes
			// forward

			var x1=x;
			var y1=y;
			var x2=x + CELL_SIZE_PX;
			var y2=y + CELL_SIZE_PX;

			var average=getAverageOnArea(x1, y1, x2, y2, data, width);
			average=blackAndWhite(average);

			pixs[x + "," + y]=average;

			if (maxValue<average.r)
				maxValue=average.r;

		}
	}

	// - We degradate image :
	var correctionValue=255 - maxValue;

	// (A 2D-based restriction area calculation) :
	for(var x=REDUCED_AREA_X1; x <= REDUCED_AREA_X2; x+=CELL_SIZE_PX){ // X
		// goes
		// forward
		for(var y=REDUCED_AREA_Y1; y <= REDUCED_AREA_Y2; y+=CELL_SIZE_PX){ // Y
			// goes
			// forward

			var average=pixs[x + "," + y];

			average=offsetColors(average, correctionValue);
			average=blackAndWhite(average, NUMBER_OF_SHADES_OF_GRAY);

			pixs[x + "," + y]=average;

			// DEBUG
			// if(!window.testtest){
			// // DEBUG
			// alert(")))) pixs["+x+","+y+"]:"+average.r);
			// window.testtest=true;
			// }

			// //TRACE (PLEASE KEEP CODE !)
			// var x1=x, y1=y;
			// var x2=x+CELL_SIZE_PX, y2=y+CELL_SIZE_PX;
			// setColorOnArea(average,x1,y1,x2,y2,data,width);

		}
	}

	// *******************************************************************************************************
	// Step 2 : We calculate image bounds (tracking of the bold black square
	// border): ------------------------------------------------------------------

	// We will look for the 4 minimal points defining the recognition
	// pseudo-rectangle :
	var detectChange=function(oldValColor, newValColor){
		var diff=newValColor.r - oldValColor.r;
		return ALLOWED_DIFFERENCE<Math.abs(diff) && diff<0; // ...and if color
		// change is from
		// white (hight
		// value) to black
		// (low value)
	};

	// left-top corner :
	var p=new Array();
	p[0]=new Object();// left-top corner
	p[0].x=REDUCED_AREA_X1 + CELL_SIZE_PX * 2;
	p[0].y=REDUCED_AREA_Y1 + CELL_SIZE_PX * 2;
	p[0].isDetermined=false;
	var oldVal=null;
	// (A 2D-based restriction area calculation) :
	for(var x=REDUCED_AREA_X1 + CELL_SIZE_PX * 2; x <= REDUCED_AREA_X2 - CELL_SIZE_PX; x+=CELL_SIZE_PX){ // X
		// goes
		// forward
		for(var y=REDUCED_AREA_Y1 + CELL_SIZE_PX * 2; y <= REDUCED_AREA_Y2 - CELL_SIZE_PX; y+=CELL_SIZE_PX){ // Y
			// goes
			// forward
			var average=pixs[x + "," + y];
			// If first encountered pixels cell with high delta value;
			if (!p[0].isDetermined && oldVal && detectChange(oldVal, average)){
				p[0].x=x;
				p[0].y=y;
				p[0].isDetermined=true;
				break;
			}
			oldVal=average;
		}
	}

	// right-top corner :
	p[1]=new Object();// right-top corner
	p[1].x=REDUCED_AREA_X2 - CELL_SIZE_PX * 2;
	p[1].y=REDUCED_AREA_Y1 + CELL_SIZE_PX * 2;
	p[1].isDetermined=false;
	oldVal=null;
	for(var x=REDUCED_AREA_X2 - CELL_SIZE_PX * 2; REDUCED_AREA_X1 + CELL_SIZE_PX <= x; x -= CELL_SIZE_PX){ // X
		// goes
		// backwards
		for(var y=REDUCED_AREA_Y1 + CELL_SIZE_PX * 2; y <= REDUCED_AREA_Y2 - CELL_SIZE_PX; y+=CELL_SIZE_PX){ // Y
			// goes
			// forward
			var average=pixs[x + "," + y];
			// If last encountered pixels cell with high delta value :
			if (!p[1].isDetermined && oldVal && detectChange(oldVal, average)){
				p[1].x=x;
				p[1].y=y;
				p[1].isDetermined=true;
				break;
			}
			oldVal=average;
		}
	}

	// right-bottom corner :
	p[2]=new Object();// right-bottom corner
	p[2].x=REDUCED_AREA_X2 - CELL_SIZE_PX * 2;
	p[2].y=REDUCED_AREA_Y2 - CELL_SIZE_PX * 2;
	p[2].isDetermined=false;
	oldVal=null;
	// (A 2D-based restriction area calculation) :
	for(var x=REDUCED_AREA_X2 - CELL_SIZE_PX * 2; REDUCED_AREA_X1 + CELL_SIZE_PX <= x; x -= CELL_SIZE_PX){ // X
		// goes
		// backwards
		for(var y=REDUCED_AREA_Y2 - CELL_SIZE_PX * 2; REDUCED_AREA_Y1 + CELL_SIZE_PX <= y; y -= CELL_SIZE_PX){ // Y
			// goes
			// backwards
			var average=pixs[x + "," + y];
			// If last encountered pixels cell with high delta value :
			if (!p[2].isDetermined && oldVal && detectChange(oldVal, average)){
				p[2].x=x;
				p[2].y=y;
				p[2].isDetermined=true;
				break;
			}
			oldVal=average;
		}
	}

	// left-bottom corner :
	p[3]=new Object();// left-bottom corner
	p[3].x=REDUCED_AREA_X1 + CELL_SIZE_PX * 2;
	p[3].y=REDUCED_AREA_Y2 - CELL_SIZE_PX * 2;
	p[3].isDetermined=false;
	oldVal=null;
	// (A 2D-based restriction area calculation) :
	for(var x=REDUCED_AREA_X1 + CELL_SIZE_PX * 2; x <= REDUCED_AREA_X2 - CELL_SIZE_PX; x+=CELL_SIZE_PX){ // X
		// goes
		// forward
		for(var y=REDUCED_AREA_Y2 - CELL_SIZE_PX * 2; REDUCED_AREA_Y1 + CELL_SIZE_PX <= y; y -= CELL_SIZE_PX){ // Y
			// goes
			// backwards
			var average=pixs[x + "," + y];
			// If last encountered pixels cell with high delta value :
			if (!p[3].isDetermined && oldVal && detectChange(oldVal, average)){
				p[3].x=x;
				p[3].y=y;
				p[3].isDetermined=true;
				break;
			}
			oldVal=average;
		}
	}

	// *******************************************************************************************************
	// Step 3 : We calculate the image «hash»:
	// ------------------------------------------------------------------

	// If all 4 four points seem to describe a square and are not in initial
	// position :
	// var SQUARE_TOLERANCE=Math.round(CELL_SIZE_PX/4);
	var SQUARE_TOLERANCE=Math.round(CELL_SIZE_PX * 1.5);

	var isSquareFound=function(){

		var noneIsDetermined=true;
		for(var i=0; i<p.length; i++)
			if (p[i].isDetermined)
				noneIsDetermined=false;
		if (noneIsDetermined)
			return false;

		// This filter also allows to check minimal square size :
		var diag1=Math.sqrt(Math.getSquaredDistance(p[0], p[2]));
		var diag2=Math.sqrt(Math.getSquaredDistance(p[1], p[3]));
		// if(SQUARE_TOLERANCE<Math.abs(diag1-diag2)) return false;
		var diagonalVariance=Math.getVariance([ diag1, diag2 ]);
		if (SQUARE_TOLERANCE<diagonalVariance)
			return false;

		var d0=Math.sqrt(Math.getSquaredDistance(p[0], p[1]));
		var d1=Math.sqrt(Math.getSquaredDistance(p[1], p[2]));
		var d2=Math.sqrt(Math.getSquaredDistance(p[2], p[3]));
		var d3=Math.sqrt(Math.getSquaredDistance(p[3], p[0]));

		// var VARIANCE_PERCENTAGE_MAX=.3;
		// var variance=Math.getVariancePercentage([d0,d1,d2,d3],AREA_SIZE);

		var variance=Math.getVariance([ d0, d1, d2, d3 ]);
		if (SQUARE_TOLERANCE<variance)
			return false;

		var c0=Math.abs(d0 - d1)<SQUARE_TOLERANCE;
		var c1=Math.abs(d1 - d2)<SQUARE_TOLERANCE;
		var c2=Math.abs(d2 - d3)<SQUARE_TOLERANCE;
		var c3=Math.abs(d3 - d0)<SQUARE_TOLERANCE;

		// Only 3 distances are enough to tell if it is a square:
		if (!((c0 && c1 && c2) || (c1 && c2 && c3) || (c2 && c3 && c0) || (c3 && c0 && c1)))
			return false;

		// // DEBUG
		// alert("SQUARE FOUND : diagonalVariance: "+diagonalVariance+"; variance
		// :"+variance);

		return true;
	};

	// TRACES :
	offsetColorOnArea(-64, REDUCED_AREA_X1 + CELL_SIZE_PX, REDUCED_AREA_Y1 + CELL_SIZE_PX, REDUCED_AREA_X2 - CELL_SIZE_PX, REDUCED_AREA_Y2 - CELL_SIZE_PX, data,
			width);

	// TRACES :
	var dotSize=Math.round(CELL_SIZE_PX *.5);
	// DEBUG
	for(var i=0; i<p.length; i++)
		offsetColorOnArea(127, p[i].x - dotSize, p[i].y - dotSize, p[i].x + dotSize, p[i].y + dotSize, data, width);

	if (isSquareFound()){

		var imageHash="";
		var hashValues=new Array();

		// TODO ...
		// FREEZE
		// We calculate the detected square hash print :

		// // Using only regularly chosen coordinates cells :
		for(var x=p[0].x; x <= p[2].x; x+=CELL_SIZE_PX){ // X goes forward
			for(var y=p[0].y; y <= p[2].y; y+=CELL_SIZE_PX){ // Y goes forward
				var pix=pixs[x + "," + y];
				if (!pix)
					continue;

				// // One out of 4 only is keeped :
				// if(x%4!==0 || y%4!==0) continue;

				hashValues.push(pix.r);
			}
		}

		// // We draw a cross to calculate the image hash :
		// var xCross=Math.round(Math.abs(p[0].x-p[1].x)*.5);
		// for(var y=p[0].y;y<=p[1].y;y+=CELL_SIZE_PX){ // Y goes forward
		// var pix=pixs[xCross+","+y];
		// if(!pix) continue;
		// hashValues.push(pix.r);
		// }
		// var yCross=(Math.abs(p[1].y-p[2].x)*.5);
		// for(var x=p[1].x;x<=p[2].x;x+=CELL_SIZE_PX){ // X goes forward
		// var pix=pixs[x+","+yCross];
		// if(!pix) continue;
		// hashValues.push(pix.r);
		// }

		// var bruteScheme=Math.getVariationsSchemeFromArray(hashValues);
		// imageHash=Math.getSimplifiedVariationsSchemeFromArray(hashValues);
		//
		// //TRACE
		// var str="";for(var i=0;i<10;i++) str+=hashValues[i]+",";
		// alert("HASHING 8:
		// ("+str+")->"+hashValues.length+";("+bruteScheme+")imageHash:"+imageHash);

		// var bruteScheme=Math.getVariationsSchemeFromArray(hashValues);
		// imageHash=Math.getSimplifiedVariationsSchemeFromArray(hashValues);

		var treatedHashValues=Math.getAverageValues(hashValues, 3, true);
		for(var i=0; i<10; i++)
			imageHash+=treatedHashValues[i] + ",";

		// TRACE
		var str="";
		for(var i=0; i<10; i++)	str+=hashValues[i] + ",";
		// TRACE
		log("HASHING 11: (" + str + ")->" + hashValues.length + ";imageHash:" + imageHash);

	} else { // If no square has been found :
		/* DO NOTHING */
	}

	return imageData;

};

Filters.areaPixellationFilter=new Object();
Filters.areaPixellationFilter.filter=function(imageDataParam,/* OPTIONAL */width,/* OPTIONAL */height,/* OPTIONAL */isExacerbateParam,/* OPTIONAL */
isBlackAndWhiteParam,/* OPTIONAL */grayShadesNumberParam){
	
	var scaledDownRawData=[];
	
	var isExacerbate=isExacerbateParam !== null ? isExacerbateParam : false;
	var isBlackAndWhite=isBlackAndWhiteParam !== null ? isBlackAndWhiteParam : false;
	var grayShadesNumber=grayShadesNumberParam !== null ? grayShadesNumberParam : null;

	var imageData=imageDataParam;
	var data=imageData.data;

	// 1) Make cells to simplify image :
	var CELL_SIZE_PX=20;

	var REDUCTION_FACTOR=0.5;
	var REDUCED_AREA_X1=width * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_Y1=height * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_X2=width - width * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_Y2=height - height * (REDUCTION_FACTOR *.5);

	// A 2D-based restriction area calculation :
	for(var x=REDUCED_AREA_X1; x<REDUCED_AREA_X2; x+=CELL_SIZE_PX){
		for(var y=REDUCED_AREA_Y1; y<REDUCED_AREA_Y2; y+=CELL_SIZE_PX){

			var x1=x;
			var y1=y;
			var x2=x + CELL_SIZE_PX;
			var y2=y + CELL_SIZE_PX;

			var average=getAverageOnArea(x1, y1, x2, y2, data, width);
			if (isExacerbate)
				average=exacerbateColor(average);
			if (isBlackAndWhite)
				average=blackAndWhite(average, grayShadesNumber);

			var c=setColorOnArea(average, x1, y1, x2, y2, data, width);
			
			newRawData.push(c.r);
			newRawData.push(c.g);
			newRawData.push(c.b);
			newRawData.push(1);// alpha canal
			
		}
	}

	return scaledDownRawData;

};

Filters.colorInversionFilter=new Object();
Filters.colorInversionFilter.filter=function(imageDataParam,/* OPTIONAL */width,/* OPTIONAL */height){

	var imageData=imageDataParam;

	var data=imageData.data;
	for(var i=0; i<data.length; i+=4){
		// red
		data[i]=255 - data[i];
		// green
		data[i + 1]=255 - data[i + 1];
		// blue
		data[i + 2]=255 - data[i + 2];
	}

	return imageData;
};

Filters.exacerbationFilter=new Object();
Filters.exacerbationFilter.filter=function(imageDataParam,/* OPTIONAL */width,/* OPTIONAL */height){

	var imageData=imageDataParam;

	var data=imageData.data;

	// light shift :
	var shift=64;

	for(var i=0; i<data.length; i+=4){

		// red
		var red=data[i];
		red=Math.min(255, red + shift);
		if (red <= 127)
			data[i]=0;
		else
			data[i]=255;

		// green
		var green=data[i + 1];
		green=Math.min(255, green + shift);
		if (green <= 127)
			data[i + 1]=0;
		else
			data[i + 1]=255;

		// blue
		var blue=data[i + 2];
		blue=Math.min(255, blue + shift);
		if (blue <= 127)
			data[i + 2]=0;
		else
			data[i + 2]=255;

	}

	return imageData;

};

Filters.areaBlackAndWhiteWithThresholdFilter=new Object();
Filters.areaBlackAndWhiteWithThresholdFilter.filter=function(imageDataParam,/* OPTIONAL */width,/* OPTIONAL */height){

	var imageData=imageDataParam;

	var data=imageData.data;

	var REDUCTION_FACTOR=0.5;

	var REDUCED_AREA_X1=width * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_Y1=height * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_X2=width - width * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_Y2=height - height * (REDUCTION_FACTOR *.5);

	// var
	// x1=REDUCED_AREA_X1,y1=REDUCED_AREA_Y1,x2=REDUCED_AREA_X2,y2=REDUCED_AREA_Y2;

	var x=0;
	var y=0;
	var STEP=4;

	// A 1D-ONLY-based restriction area calculation :
	for(var i=0; i<data.length; i+=4){// last int is for alpha canal !
		if (x<REDUCED_AREA_X1 || REDUCED_AREA_X2<x || y<REDUCED_AREA_Y1 || REDUCED_AREA_Y2<y){
			// for(var i=getLinearIndexForCoordinates(x1,y1,width,STEP); i <
			// getLinearIndexForCoordinates(x2,y2,width,STEP); i+=STEP){
			// if(indexIsOutsideArea(i,x1,y1,x2,y2,data,width,STEP)){

			x++;
			if (width <= x){
				x=0;
				y++;
			}

			continue;
		}

		// red
		var red=data[i];
		// green
		var green=data[i + 1];
		// blue
		var blue=data[i + 2];

		var color=new Object();
		color.r=red;
		color.g=green;
		color.b=blue;

		color=blackAndWhite(color, 2);// Only two shades of gray : pitch black, or
		// pure white... :-P

		data[i]=color.r;
		data[i + 1]=color.g;
		data[i + 2]=color.b;

		x++;
		if (width <= x){
			x=0;
			y++;
		}
	}

	return imageData;
};

Filters.areaExacerbationFilter=new Object();
Filters.areaExacerbationFilter.filter=function(imageDataParam,/* OPTIONAL */width,/* OPTIONAL */height){

	var imageData=imageDataParam;

	var data=imageData.data;

	var REDUCTION_FACTOR=0.5;

	var REDUCED_AREA_X1=width * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_Y1=height * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_X2=width - width * (REDUCTION_FACTOR *.5);
	var REDUCED_AREA_Y2=height - height * (REDUCTION_FACTOR *.5);

	// var
	// x1=REDUCED_AREA_X1,y1=REDUCED_AREA_Y1,x2=REDUCED_AREA_X2,y2=REDUCED_AREA_Y2;

	var x=0;
	var y=0;
	var STEP=4;

	// A 1D-ONLY-based restriction area calculation :
	for(var i=0; i<data.length; i+=4){// last int is for alpha canal !
		if (x<REDUCED_AREA_X1 || REDUCED_AREA_X2<x || y<REDUCED_AREA_Y1 || REDUCED_AREA_Y2<y){

			// for(var i=getLinearIndexForCoordinates(x1,y1,width,STEP); i <
			// getLinearIndexForCoordinates(x2,y2,width,STEP); i+=STEP){
			// if(indexIsOutsideArea(i,x1,y1,x2,y2,data,width,STEP)){

			x++;
			if (width <= x){
				x=0;
				y++;
			}

			continue;
		}

		// red
		var red=data[i];
		// green
		var green=data[i + 1];
		// blue
		var blue=data[i + 2];

		var color=new Object();
		color.r=red;
		color.g=green;
		color.b=blue;

		color=exacerbateColor(color);

		data[i]=color.r;
		data[i + 1]=color.g;
		data[i + 2]=color.b;

		x++;
		if (width <= x){
			x=0;
			y++;
		}
	}

	return imageData;
};







// Geocoding and maps management :

function getMapPositionFromLatLngStr(positionStrParam){
	var positionStr=positionStrParam.replace(";", " ");
	var split=positionStr.split(" ");
	if (split.length !== 2 || !isNumber(split[0]) || !isNumber(split[1]))
		return null;
	return new google.maps.LatLng(parseFloat(split[0]), parseFloat(split[1]));
}

// Google Analytics functions :
function initGoogleAnalytics(key, domain){
	(function(i, s, o, g, r, a, m){
		i['GoogleAnalyticsObject']=r;
		i[r]=i[r] || function(){
			(i[r].q=i[r].q || []).push(arguments);
		}, i[r].l=1 * new Date();
		a=s.createElement(o), m=s.getElementsByTagName(o)[0];
		a.async=1;
		a.src=g;
		m.parentNode.insertBefore(a, m);
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
	ga('create', key, domain);
	ga('send', 'pageview');

}

//
// // FUNCTIONS ONLY TO COMPENSATE A GOOGLE MAPS API V3 LACK : cf.
// qfox.nl/notes/116 ------------------------------

// //NOT WORKING :
// function latlngToPoint(map, latlngPosition){
// // You can do this with a dummy overlay as well:
// var overlay=new google.maps.OverlayView();
// overlay.draw=function(){//DO NOTHING
// };
// overlay.setMap(map);
// // var ADJUSTMENT=80;
// if(!overlay || !overlay.getProjection()) return null;
// var
// result=overlay.getProjection().fromLatLngToContainerPixel(latlngPosition);
// // var result=overlay.getProjection().fromLatLngToDivPixel(latlngPosition);
// // var
// result=Math.translate(overlay.getProjection().fromLatLngToContainerPixel(latlngPosition),ADJUSTMENT,0);
// return result;
// }

// // NOT WORKING :
// function pointToLatlng(map,x,y){
// // You can do this with a dummy overlay as well:
// var overlay=new google.maps.OverlayView();
// overlay.draw=function(){//DO NOTHING
// };
// overlay.setMap(map);
// if(!overlay || !overlay.getProjection()) return null;
// var result=overlay.getProjection().fromDivPixelToLatLng(new
// google.maps.Point(x, y));
// return result;
// }

// END OF FUNCTIONS ONLY TO COMPENSATE A GOOGLE MAPS API V3 LACK.
// ------------------------------


// UI, Non-Jquery utility methods :

function createHideableControlPanel(location){
	// TODO...
	
	
}


// View in fullscreen
function openFullscreen(element){
	if(element.requestFullscreen){
	    return element.requestFullscreen();
	}else if(element.mozRequestFullScreen){ /* Firefox */
	    return element.mozRequestFullScreen();
	}else if(element.webkitRequestFullscreen){ /* Chrome, Safari and Opera */
	    return element.webkitRequestFullscreen();
	}else if(element.msRequestFullscreen){ /* IE/Edge */
		return element.msRequestFullscreen();
	}
	
	// TRACE
	lognow("ERROR : open fullscreen not supported by this client !");
	
	return new Promise((resolve,reject)=>{
	  	reject("ERROR : NO FULLSCREEN METHOD FOUND : DO NOTHING !");
	});
}

// Close fullscreen
function closeFullscreen(){
  if (document.exitFullscreen){
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen){ /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen){ /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen){ /* IE/Edge */
    document.msExitFullscreen();
  }else{
  	// TRACE
  	lognow("ERROR : close fullscreen not supported by this client !");
  }
}

function createCrossControl(addZoom=false,doOnClick=function(axis,direction){},parentElement=window){
	
	let div=document.createElement("div");

	
	// ZOOM
	if(addZoom){

		let zoomRow=document.createElement("div");
		let zoomIn=document.createElement("button");
		zoomIn.innerHTML="+";
		zoomIn.onclick=function(event){
			doOnClick("zoom",1);
		};
		zoomRow.appendChild(zoomIn);
		let zoomOut=document.createElement("button");
		zoomOut.innerHTML="-";
		zoomOut.onclick=function(event){
			doOnClick("zoom",-1);
		};
		zoomRow.appendChild(zoomOut);
		div.appendChild(zoomRow);

	}

	// UP
	let upRow=document.createElement("div");
	let up=document.createElement("button");
	up.innerHTML="^";
	up.onclick=function(event){
		doOnClick("y",-1);
	};
	upRow.style="text-align:center;";
	upRow.appendChild(up);
	div.appendChild(upRow);
	
	let horizontalRow=document.createElement("div");
	// LEFT
	let left=document.createElement("button");
	left.innerHTML="<";
	left.onclick=function(event){
		doOnClick("x",-1);
	};
	horizontalRow.appendChild(left);
	// RIGHT
	let right=document.createElement("button");
	right.innerHTML=">";
	right.onclick=function(event){
		doOnClick("x",1);
	};
	horizontalRow.appendChild(right);
	div.appendChild(horizontalRow);

	// DOWN
	let downRow=document.createElement("div");
	let down=document.createElement("button");
	down.innerHTML="v";
	down.onclick=function(event){
		doOnClick("y",1);
	};
	downRow.style="text-align:center;";
	downRow.appendChild(down);
	div.appendChild(downRow);
	
	
	parentElement.appendChild(div);

	return div;
}


// UNUSED AND DOES NOT SEEM TO WORK !
//function getCalculatedStyle(element,styleProp){
//	if (window.getComputedStyle){
//		return document.defaultView.getComputedStyle(element).getPropertyValue(styleProp); 
//  } else if (element.currentStyle){
//  	return element.currentStyle[styleProp];
//  }                     
//	return null;
//}


function normalizeDisplayQuotesForJSONParsing(str){
	// We use a quote-looking but JSON / script insignificant quote character : ( https://fr.wikipedia.org/wiki/Apostrophe_(typographie) ) :
	return str.replace(/'/gim,"ʼ");
}


//-------------------------------------------------------------------------------------------



//A polyvalent popup :
//CAUTION : Non-blocking of execution ! (uses callbacks)
function promptWindow(label,type=null,defaultValue=null,
	doOnOK=null,doOnCancel=null,
	config={displayMode:"popup",backgroundColor1:"#B3DCED",font:"helvetica",width:"80%",height:"80%"}){
	
	// NO TYPE = simple alert
	
	const LABEL_AND_DEFAULT_VALUE_SEPARATOR=":";
	const MAX_Z_INDEX_OVERLAY_LOCAL=9999;// (Repeated because must be stand-alone constant...)
	const promptWindowWidth=(config && config.width?config.width:"80%");
	const promptWindowHeight=(config && config.height?config.height:"80%");
	// DOES NOT WORK : const promptWindowWidth="auto", promptWindowHeight="auto";


//	const POSITION_X="7.5%";
//	const POSITION_Y="5%";
	const SPECIAL_SEPARATOR="@@@";
	
	
	let resultPromiseOK=null;
	let resultPromiseCancel=null;
	
	
	let displayMode=config && config.displayMode ? config.displayMode:"popup";
	let backgroundColor1=config && config.backgroundColor1 ? config.backgroundColor1:"#B3DCED";
	let backgroundColor2=config && config.backgroundColor2 ? config.backgroundColor2:"#29B8E5";
//	let backgroundColor3=config && config.backgroundColor3 ? config.backgroundColor3:"#BCE0EE";
	let font=config && config.font ? config.font:"helvetica";
	let textColor=config && config.textColor ? config.textColor:"#000000";
	
	
	// ------------------
	let closeWindow=function(){
		if(contains(displayMode,"modal")) parent.removeChild(document.getElementById(divModal.id));
		else															parent.removeChild(document.getElementById(divWindow.id));
	};
	
	//Each field rendering :
	/* private */const getRenderedInputElement=function(inputType, defaultValueParam=null,/* OPTIONAL */labelParam=null){
	
		const DISABLED_OPACITY=0.6;
	
		let inputElement=null;
		if(contains(inputType, "password")){
			inputElement=document.createElement("input");
			if(defaultValueParam)		inputElement.value=defaultValueParam;
			inputElement.cols="80";
			inputElement.type="password";
		} else if(contains(inputType, "textbox")){
			inputElement=document.createElement("input");
			if(defaultValueParam)		inputElement.value=defaultValueParam;
			inputElement.cols="80";
			inputElement.type="text";
		} else if(contains(inputType, "intbox")){
			inputElement=document.createElement("input");
			if(defaultValueParam)		inputElement.value=defaultValueParam;
			inputElement.cols="80";
			inputElement.type="number";
	
			
		} else if(contains(inputType, "uniqueChoice") || contains(inputType, "multipleChoice")){ // If we have a group of inputs for (a) value(s) outcome :
	
			let choicesLabelsStrs=defaultValueParam;
			if(typeof(defaultValueParam) === "string"){
				choicesLabelsStrs=parseJSON(defaultValueParam);
			}
			
			inputElement=document.createElement("p");
			inputElement.style.display="flex";
			
	
			// Represents a list with a unique or a multiple outcome value(s) :

			resultPromiseOK=new Promise((resolve, reject)=>{
				
					foreach(choicesLabelsStrs,(choiceLabelStr,i)=>{
						
						// Radio image card :
						const radioButtonContainer=document.createElement("div");
						radioButtonContainer.style.width="200px";
						// radioButtonContainer.style.height="400px";
						radioButtonContainer.style["padding-right"]="10px";
						radioButtonContainer.style["text-align"]="center";
						radioButtonContainer.style["font-size"]="10px";
						radioButtonContainer.style["line-height"]="10px";
						inputElement.appendChild(radioButtonContainer);
						
						
		
						let radio=document.createElement("input");
						if(contains(inputType, "multipleChoice")){ // multipleChoice :
							radio.type="checkbox";
							radio.id="checkboxMultipleChoice_" + i;
							
						} else { // uniqueChoice :
							
							if(contains(inputType, "icons")){
			
								radio.type="image";
								if(typeof(choiceLabelStr) === "string"){
									radio.src=choiceLabelStr.trim();
								}else{
									
									if(choiceLabelStr.width)	radioButtonContainer.style.width=choiceLabelStr.width;
									if(choiceLabelStr.height)	radioButtonContainer.style.height=choiceLabelStr.height;
									
									radio.src=choiceLabelStr.src.trim();
									
									if(choiceLabelStr.inactive){
										radio.disabled=true;
										radio.style.opacity=DISABLED_OPACITY;
									}
								}
								
								// Icon substitutes itself to the OK button :
								
								radio.onclick=function(event){
									let promptedValue=event.target.indexValue;
								
									if(!validationFunction(promptedValue)){
										// TRACE
										alert(validationFunction.errorMessage);
										return;
									}
								
									if(doOnOK){
									  const doOnOKResult=doOnOK(promptedValue);
									  resolve({value:promptedValue,result:doOnOKResult});
									}
									closeWindow();
									
								};
								
								
							}else{
								radio.type="radio";
							}
							
							radio.id="radioButtonUniqueChoice_" + i;
							radio.name="radioButtonsUniqueChoice";
							
						}
						
						
						radio.indexValue=i;
						radioButtonContainer.appendChild(radio);
			
						let choiceLabel=document.createElement("label");
						if(typeof(choiceLabelStr)==="string"){
							choiceLabel.innerHTML=choiceLabelStr.trim();
						}else{
							choiceLabel.innerHTML=choiceLabelStr.html.trim();
						}
						choiceLabel.htmlFor=radio.id;
						radioButtonContainer.appendChild(choiceLabel);


						
					});
			
			});
			
			
	
			// We redefine the function used to retrieve entered value :
			if(contains(inputType, "icons")){
				
				inputElement.getResultValue=()=>{return null;};
				
			}else{
	
				inputElement.getResultValue=function(){
					let result="";
					
					// OLD :
//				jQuery(inputElement).find("input:checked").each(function(){
//					let radioSlct=jQuery(this);
//					result+=(nothing(result) ? "" : ",") + radioSlct.get(0).indexValue;
//				});
					
					foreach(inputElement.children,(child)=>{
						result+=(nothing(result) ? "" : ",") + child.indexValue;
					},(c)=>{	return child.checked; });
					
					
		
					return result;
				};
			}
	
			
		} else if(contains(inputType, "textarea")){
			
			inputElement=document.createElement("textarea");
			if(defaultValueParam)
				inputElement.value=defaultValueParam;
			inputElement.cols="80";
			inputElement.rows="20";
	
		}
		
		// DEFAULT TYPE IS a simple confirm or yesno popup

		inputElement.style.padding="4px";
		inputElement.style.margin="0";
	
		// The default function used to retrieve entered value :
		if(!inputElement.getResultValue){
			inputElement.getResultValue=function(){
				let result=inputElement.value;
				return result;
			};
		}
	
		// The label for the whole prompted value :
		inputElement.label=labelParam;
	
		return inputElement;
	};
	
	
	// ------------------
	
	
	let divWindow=document.createElement("div");
	divWindow.id="divPrompt";
	

	// Must remain the root body as parent of the prompt window...:
	let parent=document.body;

	
	
	if(contains(displayMode,"modal")){
		let divModal=document.createElement("div");
		divModal.id="divModal";
		divModal.style.width="100%";
		divModal.style.height="100%";
		divModal.style.background="#000000";
		divModal.style.position="fixed";
		divModal.style.zIndex=MAX_Z_INDEX_OVERLAY_LOCAL;
		if(contains(displayMode,":")){
			let s=displayMode.split(":");
			divModal.style.opacity=s[1];	
		}else{
			divModal.style.opacity="0.85";// Default modal transparency value
		}
		
		divModal.appendChild(divWindow);
//	parent.appendChild(divModal);
		
		appendAsFirstChildOf(divModal, parent);
	}else{
//		parent.appendChild(divWindow);
		
		appendAsFirstChildOf(divWindow, parent);
		
	}
	

	divWindow.className="promptWindowClass";
	
	let style="";
	style+="position:absolute;";
	style+="left:50%;top:20%;";
	style+="z-index:" + (MAX_Z_INDEX_OVERLAY_LOCAL) + ";";
	style+="z-index:" + (MAX_Z_INDEX_OVERLAY_LOCAL) + ";";
	style+="width:"+promptWindowWidth+";height:"+promptWindowHeight+"; ";
	style+="border:#888888 8px dotted;padding:10px;opacity:1;";
	style+="font-family:"+font+";";
	style+="color:"+textColor+";";

	// Default background :
	style+="background: "+backgroundColor1+" !important;" + "background: -moz-linear-gradient(-45deg,  "+backgroundColor1+" 0%, "+backgroundColor2+" 50%, "+backgroundColor1+" 100%) !important;"
			+ "background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,"+backgroundColor1+"), color-stop(50%,"+backgroundColor2+"), color-stop(100%,"+backgroundColor1+")) !important;"
			+ "background: -webkit-linear-gradient(-45deg,  "+backgroundColor1+" 0%,"+backgroundColor2+" 50%,"+backgroundColor1+" 100%) !important;"
			+ "background: -o-linear-gradient(-45deg,  "+backgroundColor1+" 0%,"+backgroundColor2+" 50%,"+backgroundColor1+" 100%) !important;"
			+ "background: -ms-linear-gradient(-45deg,  "+backgroundColor1+" 0%,"+backgroundColor2+" 50%,"+backgroundColor1+" 100%)!important;"
			+ "background: linear-gradient(135deg,  "+backgroundColor1+" 0%,"+backgroundColor2+" 50%,"+backgroundColor1+" 100%) !important;"
			+ "filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='"+backgroundColor1+"', endColorstr='"+backgroundColor1+"',GradientType=1 );";
	
	divWindow.style=style;
	
	divWindow.style["margin-left"]=(-divWindow.offsetWidth/2)+"px";
	divWindow.style["margin-top"]= (-divWindow.offsetHeight/2)+"px";

	
	// The label for the title of the window :
	let labelElement=document.createElement("div");
	labelElement.innerHTML=label;
	labelElement.style="padding-top:0;padding-bottom:10px;";
	// Positioning :
	divWindow.appendChild(labelElement);
	
	
	// The container containing all the elements :
	let divContainer=document.createElement("div");
	divContainer.id="divPromptContainer";
	divContainer.style="overflow-y:auto;width:100%;max-height:83%;";
	divWindow.appendChild(divContainer);
	
	

	
	// Eventual validation :
	let validationFunction=function(value){
		return true;/* DO NOTHING */
	};
	validationFunction.errorMessage="";
	if(!nothing(type) && contains(type, ":")){
		let typeParams=type.split(":");
		if(typeParams.length > 1){
			let validationType=typeParams[1];
	
			if(validationType === "email"){
				validationFunction=function(value){
					return isValidEmail(value);
				};
				validationFunction.errorMessage="Typed email is incorrect.";
			}
		}
	}
	
	
	// We define the global function used to retrieve entered value :
	let getGlobalResultValue=null;
	const inputElements=[];
	
	 // Case the window is actually a form of several fields :
	if(!nothing(type) && containsIgnoreCase(type, "form{")){
		
		let namesWithTypesAndDefaultValuesStr=type.replace("form{", "{");
		try {
	
			let defaultValues=new Array();
			if(!nothing(defaultValue))	defaultValues=defaultValue.split(SPECIAL_SEPARATOR);
	
			let namesWithTypesAndDefaultValues=parseJSON(namesWithTypesAndDefaultValuesStr);
			let j=0;
			foreach(namesWithTypesAndDefaultValues,(inputItemDescriptionString,key)=>{
	
				let typeInputItem=inputItemDescriptionString;
				let label=key;
				// Syntax : «<type>:<label>»
				if(contains(inputItemDescriptionString, LABEL_AND_DEFAULT_VALUE_SEPARATOR)){
					let split=inputItemDescriptionString.split(LABEL_AND_DEFAULT_VALUE_SEPARATOR);
					typeInputItem=split[0];
					label=split[1];
				}
	
				if(j<defaultValues.length)	defaultValue=defaultValues[j];
				else													defaultValue=null;
	
				let renderedInputItem=getRenderedInputElement(typeInputItem, defaultValue, label);
				inputElements.push(renderedInputItem);
				j++;
			});
	
			// We redefine the global function used to retrieve entered value :
			getGlobalResultValue=function(){
				let result="";
	
				for(let k=0; k<inputElements.length; k++){
					result+=(nothing(result) ? "" : SPECIAL_SEPARATOR) + inputElements[k].getResultValue();
				}
	
				return result;
			};
	
		} catch (e1){
			let errorDiv=document.createElement("div");
			errorDiv.innerHTML="Error parsing JSON «" + namesWithTypesAndDefaultValuesStr + "» : ERROR «:" + e1 + "»";
			inputElements.push(errorDiv);
		}
	
	
	} else if(!type || type==="yesno"){ // Case no type (meaning simple alert) or yes/no

		// We redefine the global function used to retrieve entered value :
		getGlobalResultValue=function(){
			return null;
		};

	} else { // Case of a single field :
	
		let singleInputElement=getRenderedInputElement(type, defaultValue);
		inputElements.push(singleInputElement);
	
		// Focus on password field :
		singleInputElement.focus();
	
		
		// We redefine the global function used to retrieve entered value :
		getGlobalResultValue=function(){
			return singleInputElement.getResultValue();
		};
	}
	
	
	// TODO : FIXME : Focus on text field is not working.... :,-(
//	inputElements[inputElements.length - 1].focus();
	
	// This button is displayed in all cases :
	// except if we have no type meaning simple alert :
	if(type){
		let buttonCancel=document.createElement("button");
		buttonCancel.innerHTML="Cancel";
		buttonCancel.style="display:block;float:right;";
		resultPromiseCancel=new Promise((resolve, reject)=>{ 
				buttonCancel.onclick=function(){
					let promptedValue=getGlobalResultValue();
					if(doOnCancel){
						const doOnCancelResult=doOnCancel(promptedValue);
						resolve({value:promptedValue,result:doOnCancelResult});
					}
					closeWindow();
				};
		});
		divWindow.appendChild(buttonCancel);
	}
	
	
	// We hide this OK button, when we are in a "radio-image" unique selection case,
	// since the doOnOK() is triggered when we click on the "radio-image" icon,
	// thus rendering this OK button unuseful ! :
	 if(nothing(type) || !contains(type, "uniqueChoice")){
	 
			let buttonOK=document.createElement("button");
			buttonOK.innerHTML="OK";
			buttonOK.style="display:block;margin-bottom:10px;";
			
			resultPromiseOK=new Promise((resolve, reject)=>{
				
					buttonOK.onclick=function(){
						let promptedValue=getGlobalResultValue();
					
						if(promptedValue!=null && !validationFunction(promptedValue)){
							// TRACE
							alert(validationFunction.errorMessage);
							return;
						}
					
						if(doOnOK){
							const doOnOKResult=doOnOK(promptedValue);
							resolve({value:promptedValue,result:doOnOKResult});
						}
						closeWindow();
					};

			});		
			divWindow.appendChild(buttonOK);
	}

	// Final integration of the generated input elements :
	for(let i=0; i<inputElements.length; i++){
		if(0<i){
			const br=document.createElement("br");
			divContainer.appendChild(br);
		}
	
		// The label :
		if(inputElements[i].label){
			const divLabel=document.createElement("div");
			divLabel.innerHTML=inputElements[i].label;
			divContainer.appendChild(divLabel);
		}
	
		divContainer.appendChild(inputElements[i]);
	}
	

	return {
			toPromise:(issue="OK")=>(issue=="OK"?resultPromiseOK:resultPromiseCancel),
			toPromises:()=>{ return {OK:resultPromiseOK,cancel:resultPromiseCancel}; }
		};
}




getInputCoords=function(event){
	return (((event.clientX && event.clientY) || !event.touches) ? event : event.touches.item(0) );
}









// Three.js utils :

/*private*/function isWebglAvailable(){
	try {
		let canvas=document.createElement("canvas");
		return !!(window.WebGLRenderingContext && (canvas.getContext("webgl") || canvas.getContext("experimental-webgl")));
	} catch (error){
		return false;
	}
}


function getThreeRenderer(conf, pixelRatio=window.devicePixelRatio){

	if(!THREE){
		lognow("ERROR : Three.js library missing ! Cannot create ThreeJS renderer.");
		return null;
	}
	
	let renderer;	
	if(conf.canvas){
		if(isWebglAvailable()){
			
			if(conf.background==="transparent"){
				conf.alpha=true;
			}
			
			renderer=new THREE.WebGLRenderer(conf);
			renderer.setPixelRatio(pixelRatio);
			
			if(conf.background==="transparent"){
				renderer.setClearColor(0x000000, 0);
			}
			
		}
	}else if(conf.type === "css"){ 
		renderer=new THREE.CSS3DRenderer();
	}

	if(!renderer){
		// TRACE
		lognow("ERROR : No renderer found found. Aborting.");
		return null;
	}

// REMOVED FROM THREEJS :
// let renderer=new THREE.CanvasRenderer({
//		canvas : conf.canvas
// });


	if(conf.canvas){
		renderer.setSize(conf.canvas.width, conf.canvas.height);
	}else if(conf.w && conf.h){
		renderer.setSize(conf.w, conf.h);
	}

	return renderer;
}


function getVRHandler(renderer,modeVR="immersive-vr",optionsVR=["local-floor", "bounded-floor", "hand-tracking", "layers"]){
//	"immersive-ar", "immersive-vr", "inline"

	// See : https://developer.mozilla.org/en-US/docs/Web/API/XRSystem/requestSession	
	
	if(typeof(navigator)===undefined || !renderer){
		// TRACE
		lognow("ERROR : No 3D renderer provided, cannot init the VR session.");
		return null;
	}
	

	if("xr" in navigator){

		const result={
			
			currentSession:null,

			/*private*/onSessionStarted:async function(session){
				session.addEventListener("end", result.onSessionEnded);
				
				await renderer.xr.setSession(session);

				result.currentSession=session;
			},

			/*private*/onSessionEnded:(event)=>{
				result.currentSession.removeEventListener("end", result.onSessionEnded);
				result.currentSession=null;
			},

			start:()=>{

				if(result.currentSession)	return;

				// WebXR's requestReferenceSpace only works if the corresponding feature
				// was requested at session creation time. For simplicity, just ask for
				// the interesting ones as optional features, but be aware that the
				// requestReferenceSpace call will fail if it turns out to be unavailable.
				// ('local' is always available for immersive sessions and doesn't need to
				// be requested separately.)

				const sessionInitConf={optionalFeatures:optionsVR};
				navigator.xr.requestSession(modeVR, sessionInitConf)
						.then(result.onSessionStarted)
						.catch(error=>lognow("ERROR : Error initializing XR context : ",error));

				return result;
			},
			
			end:()=>{
				if(!result.currentSession)	return;
				result.currentSession.end();
				return result;
			},

		};

		navigator.xr.isSessionSupported(modeVR).then((supported)=>{
			if(!supported){
				// TRACE
				lognow("ERROR : VR is not supported by your browser.");
			}else{
				result.isReady=true;				
			}
		});
		
		if(renderer.xr)	renderer.xr.enabled=true;

		return result;
	}
	
	if(window.isSecureContext === false) {
		// TRACE
		lognow("ERROR : WEBXR needs a HTTPS environemnt.");
		return null;
	}
	
	// TRACE
	lognow("ERROR : WEBXR not available.");
	
	return null;
};



// WEBGL Management :

function convertHTMLToImage(elementHTML,callBack,mode="png",quality=null,height,width){
	
	const doOnComplete=(dataUrl)=>{
    let img = new Image();
    img.src = dataUrl;
    callBack(img);
	};
	const doOnError=(error)=>{
 		// TRACE
		console.error("ERROR : Could not convert element to an image, something went wrong:", error);
  };

	// DOES NOT WORK :
	// https://html2canvas.hertzen.com/
	//
//		html2canvas(elementHTML,{scale:1}).then((canvas)=>{
//			const dataUrl=canvas.toDataURL("image/png");
//			let img = new Image();
//			img.src = dataUrl;
//			callBack(img);
//		}).catch((error)=>{
//        console.error("ERROR : Could not convert element to an image, something went wrong:", error);
//    });

		// https://github.com/tsayen/dom-to-image
		if(mode=="png")	domtoimage.toPng(elementHTML,{quality:nonull(quality,0.75),height:height,width:width}).then(doOnComplete).catch(doOnError);
		else if(mode=="svg")	domtoimage.toSvg(elementHTML, {filter:()=>true,height:height,width:width}).then(doOnComplete).catch(doOnError);
		else domtoimage.toJpeg(elementHTML,{quality:nonull(quality,0.2),height:height,width:width}).then(doOnComplete).catch(doOnError);

}





// SVG MANAGEMENT : 


function getPointsFromSVGDString(str){
    str = str.replace(/[0-9]+-/g, function(v)
        {
            return v.slice(0, -1) + " -";
        })
        .replace(/\.[0-9]+/g, function(v)
        {
            return v.match(/\s/g) ? v : v + " ";
        }).replace(/\,/g,"");
    
    let keys = str.match(/[MmLlHhVv]/g);
    let paths = str.split(/[MmLlHhVvZz]/g)
						    .filter(function(v){ return v.length > 0})
						    .map(function(v){return v.trim()});
    
    let x = 0, y = 0;
    const points = [];
    for(let i = 0, KL=keys.length ; i<KL ; i++) {
        switch(keys[i]) {
            case "M": case "L": case "l":
                let arr = paths[i].split(/\s/g).filter(function(v) { return v.length > 0 });
                for(let t = 0, AL=arr.length ; t<AL ; t+=2) {
                    x = (keys[i]=="l" ? x : 0) + parseFloat(arr[t]);
                    y = (keys[i]=="l" ? y : 0) + parseFloat(arr[t+1]);
                    points.push({x:x,y:y});
                }
                break;
            case "V":
                y = parseFloat(paths[i]);
                points.push({x:x,y:y});
                break;
            case "v":
                y+=parseFloat(paths[i]);
                points.push({x:x,y:y});
                break;
            case "H":
                x = parseFloat(paths[i]);
                points.push({x:x,y:y});
                break;
            case "h":
                x+=parseFloat(paths[i]);
                points.push({x:x,y:y});
                break;
        }
    }
    
    return points;
}

// Simple polygon collision detection : https://github.com/w8r/GreinerHormann
// DOC : https://milevski.co/GreinerHormann/

//var intersection = greinerHormann.intersection(source, clip);
//var union        = greinerHormann.union(source, clip);
//var diff         = greinerHormann.diff(source, clip);
//
//
//if(intersection){
//    if(typeof intersection[0][0] === 'number'){ // single linear ring
//        intersection = [intersection];
//    }
//    for(var i = 0, len = intersection.length; i<len; i++){
//        L.polygon(intersection[i], {...}).addTo(map);
//    }
//}




//
//
//// obj - your object (THREE.Object3D or derived)
//// pointOfRotation - the point of rotation (THREE.Vector3)
//// axis - the axis of rotation (normalized THREE.Vector3)
//// angle - radian value of rotation
//// pointIsWorld - boolean indicating the point is in world coordinates (default=false)
///*private*/function rotateAboutPoint(obj, axis, angle, pointOfRotation=new THREE.Vector3(0,0,0), pointIsWorld=false){
//
// if(pointIsWorld){
//     obj.parent.localToWorld(obj.position); // compensate for world coordinate
// }
//
// obj.position.sub(pointOfRotation); // remove the offset
// obj.position.applyAxisAngle(axis, angle); // rotate the POSITION
// obj.position.add(pointOfRotation); // re-add the offset
//
// if(pointIsWorld){
//     obj.parent.worldToLocal(obj.position); // undo world coordinates compensation
// }
//
// obj.rotateOnAxis(axis, angle); // rotate the OBJECT
//}









// -Client :

// TODO : Use everywhere it's applicable !
initClient=function(isNodeContext=true, useSocketIOImplementation=/*DEBUG*/false, doOnServerConnection=null, url, /*OPTIONAL*/port=25000, isSecure=true, /*OPTIONAL*/timeout=10000, /*OPTIONAL*/selfParam=null){
	let aotraClient={};
	
	aotraClient.client={};
	aotraClient.client.start=function(){
	
		// DBG
		lognow("INFO : Setting up client :...");
		
		let socketToServer=WebsocketImplementation.getStatic(isNodeContext, useSocketIOImplementation).connectToServer(url, port, isSecure, timeout);
		if(!socketToServer){
			// TRACE
			lognow("ERROR : CLIENT : Could not get socketToServer, aborting client connection.");
			return null;
		}
		
		// When client is connected, we execute the callback :
		// CAUTION : MUST BE CALLED ONLY ONCE !
		socketToServer.onConnectionToServer(()=>{
			if(doOnServerConnection){
				if(selfParam)	doOnServerConnection.apply(selfParam,[socketToServer]);
				else				  doOnServerConnection(socketToServer);
			}
		
			
		});

//		// Errors handling :
//		if(doOnError && useSocketIOImplementation /*TODO : FIXME: FOR NOW, ONLY WORKS FOR SOCKET.IO IMPLEMENTATION !'*/){
//			
////			// DBG
////			lognow(">>> serverSocket:",serverSocket);
//
//
//			const errorMethod=function(){
//				// TRACE
//				lognow("ERROR : CLIENT : Client encountered an error while trying to connect to server.");
//				
//				if(selfParam)	doOnError.apply(selfParam);
//				else					doOnError();
//			};
//			
//			socketToServer.clientSocket.on("connect_error", errorMethod);
//				
////			// DOES NOT SEEM TO WORK :
////			socketToServer.on("connect_failed", errorMethod);
////			// DOES NOT SEEM TO WORK :
////			socketToServer.on("error", errorMethod);
//			
//		}
//
//		// Data messages handling :
//		if(doOnMessage){
//			socketToServer.onIncomingMessage((message)=>{
//				if(selfParam)	doOnMessage.apply(selfParam,[message]);
//				else				  doOnMessage(message);
//			});
//		}
		
		// DBG
		lognow("aotraClient.client:",aotraClient.client);
		
		// DBG
//		aotraClient.clientSocket.addEventListener("close",()=>{
//			// TRACE
//			lognow("WARN : CONNECTION CLOSED !");		
//		});

		
		aotraClient.client.socketToServer=socketToServer;
		
		return aotraClient;
	};
	
	
	return aotraClient;
}




// ========================= FUSRODA CLIENT : =========================

class VNCFrame2D{
	
	constructor(url, port=6080, isSecure=true, htmlConfig={parentElement:null,imageMode:"CSSBackground"}, mouseEventsRefreshMillis=100){
		
		
		this.url=url;
		this.port=port;
		this.isSecure=isSecure;
		this.parentElement=nonull(htmlConfig.parentElement, document.body);
		this.imageMode=htmlConfig.imageMode;
		this.mouseEventsRefreshMillis=mouseEventsRefreshMillis;

		
		this.fusrodaClient=null;
		
		
		this.screenAndAudioConfig=null;
		this.image=null;
		self.audioCtx=null;
		
		if(this.imageMode=="canvas"){
			this.canvas=document.createElement("canvas");
			this.parentElement.appendChild(this.canvas);
		}
		
		this.lastPointerMoveTime=null;
		
	}
	
	
	start(){

		const self=this;
		this.fusrodaClient=createFusrodaClient(
			// doOnClientReady:
			(messageConfig)=>{
				
					self.screenAndAudioConfig=messageConfig;

					if(self.imageMode=="canvas"){
						self.image=new Image();
						self.image.addEventListener("load",(event)=>{
							
							// //DBG
							// lognow("Image finished loading.");
							
							const image=event.target;
							image.isImageLoading=false;
							
							if(!self.canvasSizeHasBeenSet){
								self.canvas.width=image.width;
								self.canvas.height=image.height;
								self.canvasSizeHasBeenSet=true;
							}

							self.canvas.getContext("2d").drawImage(image,0,0);

							self.fusrodaClient.client.socketToServer.send("screenFrameRequest", {});

						});
						
					}

					
					self.audioCtx=new AudioContext({sampleRate: self.screenAndAudioConfig.sampleRate});
					
				
			},// doOnDataReception:
				{
				"image":(imageDataStr)=>{
					
					
					const base64String="data:image/png;base64,"+imageDataStr;
					if(self.imageMode=="canvas"){
						if(self.image.complete){
							self.image.src=base64String;
							self.image.isImageLoading=true;
						}
					}else{
						self.parentElement.style["background-image"]="url('"+base64String+"');";
					}
					
					
				},
				"sound":(soundDataStr)=>{
					const soundData=getDecodedArrayFromSoundDataString(soundDataStr);
					playAudioData(soundData,self.audioCtx,self.screenAndAudioConfig.audioBufferSize,1,self.screenAndAudioConfig.sampleRate,()=>{
						self.fusrodaClient.client.socketToServer.send("soundSampleRequest", {});
					});
				}
		}, this.url, this.port, this.isSecure);


		// TODO : FIXME : DUPLICATED CODE !
		this.parentElement.addEventListener("pointermove",(event)=>{
			
			const config=self.screenAndAudioConfig;
			if(!config || !self.canvas.width)	return;
			
			
			if(!hasDelayPassed(self.lastPointerMoveTime,self.mouseEventsRefreshMillis))	return;
			self.lastPointerMoveTime=getNow();
			
		
			const width=config.width;
			const height=config.height;
			
			const x=Math.floor((event.clientX/self.canvas.width)*width);
			const y=Math.floor((event.clientY/self.canvas.height)*height);
			
			
			self.fusrodaClient.sendMouseMoveEvent({x:x,y:y});
			
			// DBG
			lognow("sendMouseMoveEvent:"+x+";"+y);
			
		});
		// TODO : FIXME : DUPLICATED CODE !
		this.parentElement.addEventListener("mousedown",(event)=>{
			const mouseButton=event.button;
			self.fusrodaClient.sendMouseClickEvent({mouseButton:mouseButton});
			
			// DBG
			lognow("mousedown:",event);
		});
		// TODO : FIXME : DUPLICATED CODE !
		this.parentElement.addEventListener("mouseup",(event)=>{
			const mouseButton=event.button;
			self.fusrodaClient.sendMouseReleasedEvent({mouseButton:mouseButton});
		});
		// TODO : FIXME : DUPLICATED CODE !
		const WHEEL_EVENT_FACTOR=.01;
		this.parentElement.addEventListener("wheel",(event)=>{
			const wheelAmount=event.deltaY*WHEEL_EVENT_FACTOR;
			self.fusrodaClient.sendWheelEvent({wheelAmount:wheelAmount});
			
			// DBG
			lognow("wheel:",event);
		});
		// TODO : FIXME : DUPLICATED CODE !
		// To remove the local contextual menu :
		this.parentElement.addEventListener("contextmenu",(event)=>{event.preventDefault();});
		
		// TODO : FIXME : DUPLICATED CODE !
		this.parentElement.addEventListener("keydown",(event)=>{
			let keyChar=event.key;
			const keyCode=event.keyCode;
			
			// To avoid server-side «A JSONObject text must end with '}'» errors :
			if(keyChar==="{")	keyChar="left_brace";
			if(keyChar==="}")	keyChar="right_brace";
			
			self.fusrodaClient.sendKeyboardPressedEvent({keyChar:keyChar, keyCodeFallback:keyCode});
		});
		// TODO : FIXME : DUPLICATED CODE !
		this.parentElement.addEventListener("keyup",(event)=>{
			let keyChar=event.key;
			const keyCode=event.keyCode;
			
			// DBG
			lognow("!!! keyup KEYUP event:[code:"+event.code+"][keyCode:"+keyCode+"][key:"+keyChar+"]",event);

			// To avoid server-side «A JSONObject text must end with '}'» errors :
			if(keyChar==="{")	keyChar="left_brace";
			if(keyChar==="}")	keyChar="right_brace";
			
			self.fusrodaClient.sendKeyboardReleasedEvent({keyChar:keyChar, keyCodeFallback:keyCode});
		});
		


		this.fusrodaClient.client.start();


		return this;
	}
	
}



/*private*/getDecodedArrayFromSoundDataString=function(soundDataStr){
	const decodedIntsArray=[];
					
	const decodedString=atob(soundDataStr);
					
//// DBG
//const decodedString=soundDataStr;

	for(let i=0;i<decodedString.length;i++){
		const dataInt=decodedString.charCodeAt(i)-127;
		
		// CAUTION : audio needs to be in [-1.0; 1.0]
		decodedIntsArray.push(dataInt/127);
	}

	return decodedIntsArray;
}





// ============================================== BROWSER CLIENTS ==============================================


// FUSRODA : 

createFusrodaClient=function(doOnClientReady, doOnDataReception, urlParam=null, portParam=6080){
 	// CAUTION : WORKS BETTER WHEN UNSECURE, BUT 'NEEDS CLIENT BROWSER TO ALLOW MIXED (SECURE/UNSECURE) CONTENT !
	
	const isSecure=(contains(urlParam.toLowerCase(),"https") || contains(urlParam.toLowerCase(),"wss"));
	
	
	const fusrodaClient=initClient(false,/*CAUTION : Fusroda Java server requires the SOcket IO websocket implementation !!*/true, /*doOnServerConnection*/(socketToServer)=>{
		
		// DBG
		lognow("FUSRODA CLIENT : SETTING UP...");
		

		//0)
		socketToServer.receive("protocolConfig", (messageConfig)=>{
			
			// DBG
			lognow("(CLIENT) (DEBUG) Received protocol config from server : messageConfig:",messageConfig);
			
			//2)			
			socketToServer.receive("imagePacket", (doOnDataReception && doOnDataReception["image"]?doOnDataReception["image"]:()=>{/*DO NOTHING*/}));
			
			// //DBG
			// lognow("Initial image request.");
			
			// Initial frame request sending :
			socketToServer.send("screenFrameRequest", {});

			//2)			
			socketToServer.receive("soundPacket", (doOnDataReception && doOnDataReception["sound"]?doOnDataReception["sound"]:()=>{/*DO NOTHING*/}));
			// Initial sound sample request sending :
			socketToServer.send("soundSampleRequest", {});


			doOnClientReady(messageConfig);
		});
		
		//1)
		socketToServer.send("protocol", { type:"hello" });

		
	}, urlParam, portParam, isSecure);
	
	fusrodaClient.sendMouseMoveEvent=(mouseEvent)=>{
		fusrodaClient.client.socketToServer.send("mouseMoveEvent", mouseEvent);
	};
		
	fusrodaClient.sendMouseClickEvent=(mouseEvent)=>{
		fusrodaClient.client.socketToServer.send("mouseClickEvent", mouseEvent);
	};
	fusrodaClient.sendMouseReleasedEvent=(mouseEvent)=>{
		fusrodaClient.client.socketToServer.send("mouseReleasedEvent", mouseEvent);
	};
	fusrodaClient.sendWheelEvent=(wheelEvent)=>{
		fusrodaClient.client.socketToServer.send("wheelEvent", wheelEvent);
	};

	fusrodaClient.sendKeyboardPressedEvent=(keyboardEvent)=>{
		fusrodaClient.client.socketToServer.send("keyboardPressedEvent", keyboardEvent);
	};
	fusrodaClient.sendKeyboardReleasedEvent=(keyboardEvent)=>{
		fusrodaClient.client.socketToServer.send("keyboardReleasedEvent", keyboardEvent);
	};

	

//	// DBG
//	fusrodaClient.clientSocket.addEventListener("close",()=>{
//		lognow("CONNECTION CLOSED !");		
//	});
	
	return fusrodaClient;
}









// ORITA :




// =======================================================================
// ========================== orita CONSTANTS ==========================
// =======================================================================

	
	
//Global constants :


window.ORITA_CONSTANTS={

	DEFAULT_PROTOCOL:"http",
	DEFAULT_PORT:25000,
		
	STRINGIFY_VIDEO_DATA:false,
		
		
	MOBILE_VIDEO_CONSTRAINTS: {},
	//OLD : CHANNELS_CONFIG:{video:2,audio:1},
	//	let CHANNELS_CONFIG:null,
	IS_MOBILE:null,
		
		
	AUDIO_BUFFER_SIZE:16384,
		
	DEFAULT_CAPTURE_WIDTH:64,
	DEFAULT_CAPTURE_HEIGHT:64,
		

};





// ------------------------------------------------------------------------------------------
// -------------------------------------- MAIN CLIENT --------------------------------------
// ------------------------------------------------------------------------------------------

window.createOritaMainClient=function(
 doOnRegistered,
 treatVideoData, treatAudioData,
 doOnInputsCreated, doOnInputsUpdated, doOnOutputsCreated, doOnOutputsUpdated,
 urlParam=null, portParam=25000, ignoreHashKey=false){

	const ORITA_HASH_STRING_NAME="oritaClientHash";

	const isSecure=(contains(urlParam.toLowerCase(),"https") || contains(urlParam.toLowerCase(),"wss"));



	const onCommunicationEventListeners=[];
	
	// ---------------------------------------------------------------------
	// I/Os setup
	
	
	// Inputs :
	
	// 2-
	onCommunicationEventListeners.push({
		condition:(message)=>message.type==="request.microClient.appendInputs",
		execute:(message)=>{
			let inputsGPIO=message.inputsGPIO;
			let microClientId=message.microClientId;
			
			let microClientInfos=mainClient.oritaMainClient.microClientsInfos[microClientId];
			if(!microClientInfos)	mainClient.oritaMainClient.microClientsInfos[microClientId]={};
			microClientInfos=mainClient.oritaMainClient.microClientsInfos[microClientId];
			if(microClientInfos.hasInputs)	return; // (to avoid registering several times)
			microClientInfos.hasInputs=true;
			
			microClientInfos.lastTime=getNow();
			mainClient.inputsGPIO=inputsGPIO;
			
			doOnInputsCreated(mainClient, microClientId, mainClient.inputsGPIO);
		},
	});
	
	// 4-
	onCommunicationEventListeners.push({
		condition:(message)=>message.type==="request.microClient.updateInputsValues",
		execute:(message)=>{

			let inputsGPIO=message.inputsGPIO;
					
		//				// DBG
		//				console.log("updateInputsValues UPDATE VALUE ! message:",message);
		//				console.log("updateInputsValues UPDATE VALUE ! inputsGPIO:",inputsGPIO);
					
					
			let microClientId=message.microClientId;
			
			let microClientInfos=mainClient.oritaMainClient.microClientsInfos[microClientId];
			if(!microClientInfos)	mainClient.oritaMainClient.microClientsInfos[microClientId]={};
			microClientInfos=mainClient.oritaMainClient.microClientsInfos[microClientId];
			microClientInfos.lastTime=getNow();
		
			mainClient.inputsGPIO=inputsGPIO;
			
			
			
			doOnInputsUpdated(mainClient, microClientId, mainClient.inputsGPIO);
		},
	});
				
	
	

	// Outputs :

	// 2-
	onCommunicationEventListeners.push({
		condition:(message)=>message.type==="request.microClient.appendOutputs",
		execute:(message)=>{
			let outputsGPIOLocal=message.outputsGPIO;
			let microClientId=message.microClientId;
			
			let microClientInfos=mainClient.oritaMainClient.microClientsInfos[microClientId];
			if(!microClientInfos)	mainClient.oritaMainClient.microClientsInfos[microClientId]={};
			microClientInfos=mainClient.oritaMainClient.microClientsInfos[microClientId];
			if(microClientInfos.hasOutputs)	return; // (to avoid registering several times)
			microClientInfos.hasOutputs=true;
			
			microClientInfos.lastTime=getNow();
			
			mainClient.outputsGPIO=outputsGPIOLocal;
			
			
			doOnOutputsCreated(mainClient, microClientId, mainClient.outputsGPIO);
			
		},
	});

	// 4-
	onCommunicationEventListeners.push({
		condition:(message)=>message.type==="response.microClient.outputChanged",
		execute:(message)=>{

			let outputsGPIOLocal=message.outputsGPIO;
			let microClientId=message.microClientId;

					
		//				// DBG
		//				console.log("outputChanged UPDATE VALUE ! message:",message);
		//				console.log("outputChanged UPDATE VALUE ! outputsGPIOLocal:",outputsGPIOLocal);
		
			mainClient.outputsGPIO=outputsGPIOLocal;
			
			doOnOutputsUpdated(mainClient, microClientId, mainClient.outputsGPIO);
		},
	});

	




	const oritaClient=initClient(false, false, /*doOnServerConnection*/(socketToServer)=>{

		// DBG
		lognow("ORITA MAIN CLIENT : SETTING UP");
	
		// TRACE
		console.log("INFO : MAIN CLIENT : Sending register request...");
		
		
		// TRACE
		console.log("INFO : MAIN CLIENT : Checking registered client key hash...");
		
		const storedHashKey=getStringFromStorage(ORITA_HASH_STRING_NAME);
		if(!storedHashKey && !ignoreHashKey){
			promptWindow("Please enter your client key :","password",null,(clearText)=>{
			
				if(empty(clearText)){
					// TRACE
					lognow("ERROR : MAIN CLIENT : Client key string is empty. Cannot proceed to registration request sending to server.");
					return;
				}
				
				const calculatedHash=getHashedString(clearText);// (we use the heavy treatment thing.)
			
				storeString(ORITA_HASH_STRING_NAME, calculatedHash);
			
				// TODO : FIXME : DUPLICATED CODE :
				socketToServer.send("protocol", "request.register:mainClient:"+calculatedHash);
								
			},()=>{
				// TRACE
				lognow("WARN : MAIN CLIENT : Cannot send registration request to server if no client key is provided.");
			});		        
		    
		}else{
			// TODO : FIXME : DUPLICATED CODE :
			socketToServer.send("protocol", "request.register:mainClient:"+storedHashKey);
		}






		// ------------------------------ CONNECTION EVENTS REGISTRATION ------------------------------
	
	
	
		// DBG
		lognow("oritaClient (MAIN):",oritaClient);
	
		
				
		oritaClient.client.socketToServer.receive("protocol", (message)=>{
					
					// TRACE
					lognow("Mainclient received message from server:",message);
					
					
					if(message.type==="response.mainClient.registered"){
					
						// TRACE
						log("Main client registered to server.");
		
						// TRACE
						log("Starting main receiving :");
						
						
						
						
						// Main client starts receiving :
						oritaClient.client.socketToServer.receive("server.send.data", (receivedData)=>{
		
							
							let microClientId=receivedData.microClientId;
							
	//						// DEBUG
	//						lognow("INFO : MAINCLIENT : MainClient received data from microClient «"+microClientId+"» :",receivedData);
							
							let compression=receivedData.compression;
							
		//					// DBG
		//					console.log("receivedData:",receivedData);
		//					console.log("receivedData.compression:",compression);
							
							if(receivedData.video){// -------- VIDEO :
		
								if(compression){
									// Actually, only handles "LZW" uncompression, for now...
									let uncompressedData;
									if(ORITA_CONSTANTS.STRINGIFY_VIDEO_DATA)	uncompressedData=arrayFromString(receivedData.video.data, compression.algorithm?true:false, compression.precision);
									else						uncompressedData=receivedData.video.data;
									
									receivedData.video.data=uncompressedData;
								}
								
		//						// DBG
		//						console.log("received receivedData.length",receivedData);
								
								
								let microClientInfos=oritaClient.microClientsInfos[microClientId];
								if(!microClientInfos)	oritaClient.microClientsInfos[microClientId]={};
								microClientInfos=oritaClient.microClientsInfos[microClientId];
								microClientInfos.hasVideo=true;
								
								oritaClient.microClientsInfos[microClientId].lastTime=getNow();
								
								
								
								treatVideoData(microClientId, receivedData);
								
								
							
							}else if(receivedData.audio){// -------- AUDIO :
								
								if(compression){
									// Actually, only handles "LZW" uncompression, for now...
									let uncompressedData;
									if(ORITA_CONSTANTS.STRINGIFY_VIDEO_DATA)	uncompressedData=arrayFromString(receivedData.audio.data, compression.algorithm?true:false, compression.precision);
									else																			uncompressedData=receivedData.audio.data;
									
									receivedData.audio.data=uncompressedData;
								}
	
								
								let microClientInfos=oritaClient.microClientsInfos[microClientId];
								if(!microClientInfos)	oritaClient.microClientsInfos[microClientId]={};
								microClientInfos=oritaClient.microClientsInfos[microClientId];
								microClientInfos.hasAudio=true;
								
								oritaClient.microClientsInfos[microClientId].lastTime=getNow();
								
								
								treatAudioData(microClientId, receivedData);
								
							
							}
							
						});
						
						
		
						
						
						doOnRegistered();
	
					}
					
					
								
		});
				
	
		
		oritaClient.client.socketToServer.receive("communication", (message)=>{
			
			// DBG
			lognow("MAIN CLIENT RECEIVED COMMUNICATION :",message);
	
			// We execute eventual on message events listeners :
			foreach(oritaClient.onCommunicationEventListeners, (e)=>{
				if(!e.condition || e.condition(message))	e.execute(message);
			});
		
		});
		






	
	
	}, urlParam, portParam, isSecure);

	
	oritaClient.microClientsInfos={};
	oritaClient.inputsGPIO={};
	oritaClient.outputsGPIO={};
	oritaClient.onCommunicationEventListeners=onCommunicationEventListeners;
	
	
	
	
	oritaClient.start=()=>{
		
		// DELEGATED START HERE !!:
		oritaClient.client.start();
		return oritaClient;
	};	
	
	
	

	return oritaClient;
};




// ------------------------------------------------------------------------------------------
// -------------------------------------- MICRO CLIENT --------------------------------------
// ------------------------------------------------------------------------------------------






// SEE http://codefoster.com/pi-basicgpio/

// INSTALL ON TARGET PLATFORM : https://ma2shita.medium.com/how-to-use-raspi-gpio-instead-of-gpio-of-wriring-pi-af2ab00eda57

// ON 10/2024 GPIO IS UTTERLY BROKEN ON THE "BOOKWORM" RASPBIAN DISTRIBUTION WITH NO SOLUTION !!! 

const GPIO_BASE_PATH = "/sys/class/gpio";
window.gpioUtils = {
	open: (gpioNumber, mode, doOnSuccess = null) => {

		if (!isNumber(gpioNumber) || !contains(["in", "out"], mode)) {
			// TRACE
			lognow("ERROR : Invalid parameters : " + gpioNumber + " ; " + mode + ". Aborting.");
			return;
		}

		const gpioFilePath = GPIO_BASE_PATH + "/gpio" + gpioNumber;
		const setDirection = () => {
			fs.writeFile(gpioFilePath + "/direction", mode + "", (error) => {
				if (error) {
					// TRACE
					lognow("ERROR : Could not set mode «" + mode + "» on pin «" + gpioNumber + "». Abording setup.", error);
					return;
				}
				if (doOnSuccess) doOnSuccess();
			});
		};

		fs.access(gpioFilePath, fs.constants.F_OK, (error) => {
			if (error) {
				// Case export file does not exists 
				// In this case we export the gpio pin :
				fs.writeFile(GPIO_BASE_PATH + "/export", gpioNumber + "", (error) => {
					if (error) {
						// TRACE
						lognow("ERROR : Could not export pin «" + gpioNumber + "». Abording setup.", error);
						return;
					}
					setDirection(gpioNumber, mode, doOnSuccess);
				});
				return;
			}
			// Case export file already exists 
			setDirection(gpioNumber, mode, doOnSuccess);
		});
	},

	close: (gpioNumber, doOnSuccess = null) => {
		if (!isNumber(gpioNumber)) {
			// TRACE
			lognow("ERROR : Invalid parameter : " + gpioNumber + ". Aborting.");
			return;
		}

		const gpioFilePath = GPIO_BASE_PATH + "/gpio" + gpioNumber;
		fs.access(gpioFilePath, fs.constants.F_OK, (error) => {
			if (error) {
				// Case export file does not exists
				// TRACE
				lognow("ERROR : Export file does not exist, could not unexport pin «" + gpioNumber + "».", error);
				return;
			}
			// Case export file already exists 
			// In this case we export the gpio pin :
			fs.writeFile(GPIO_BASE_PATH + "/unexport", gpioNumber + "", (error) => {
				if (error) {
					// TRACE
					lognow("ERROR : Could not unexport pin «" + gpioNumber + "».", error);
					return;
				}
				if (doOnSuccess) doOnSuccess();
			});
		});
	},

	write: (gpioNumber, stateNumber, doOnSuccess = null) => {

		if (!isNumber(gpioNumber) || !contains([0, 1], stateNumber)) {
			// TRACE
			lognow("ERROR : Invalid parameter : " + gpioNumber + " ; " + stateNumber + ". Aborting.");
			return;
		}


		const gpioFilePath = GPIO_BASE_PATH + "/gpio" + gpioNumber;
		fs.access(gpioFilePath, fs.constants.F_OK, (error) => {
			if (error) {
				// Case export file does not exists
				// TRACE
				lognow("ERROR : Export file does not exist, cannot write «" + stateNumber + "» on pin «" + gpioNumber + "».", error);
				return;
			}

			// DBG
			lognow("Attempting to write " + stateNumber + " on pin BCM " + gpioNumber);

			// Case export file already exists 
			// In this case we export the gpio pin :
			fs.writeFile(gpioFilePath + "/value", stateNumber + "", (error) => {
				if (error) {
					// TRACE
					lognow("ERROR : Could not write «" + stateNumber + "» on pin «" + gpioNumber + "».", error);
					return;
				}
				if (doOnSuccess) doOnSuccess();
			});
		});
	},

	read: (gpioNumber, doOnSuccess) => {

		if (!isNumber(gpioNumber)) {
			// TRACE
			lognow("ERROR : Invalid parameter : " + gpioNumber + ". Aborting.");
			return;
		}

		const gpioFilePath = GPIO_BASE_PATH + "/gpio" + gpioNumber;
		fs.access(gpioFilePath, fs.constants.F_OK, (error) => {
			if (error) {
				// Case export file does not exists
				// TRACE
				lognow("ERROR : Export file does not exist, cannot read value of pin «" + gpioNumber + "».", error);
				return;
			}

			// Case export file already exists 
			// In this case we export the gpio pin :
			fs.readFile(gpioFilePath + "/value", "utf-8", (error, data) => {

				//				// DBG
				//				lognow("DEBUG : read value:"+data);

				if (error) {
					// TRACE
					lognow("ERROR : Could not write «" + stateNumber + "» on pin «" + gpioNumber + "».", error);
					return;
				}
				const value = (data + "").trim() || "0";
				doOnSuccess((value == "1"));
			});
		});
	},

};






// const REFRESHING_RATE_MILLIS_AUDIO:100,


//	const MICRO_CLIENT_MESS_WITH_ALPHA=true;
//	const MICRO_CLIENT_MESS_WITH_ALPHA=false;


const REFRESHING_RATE_MILLIS_DEFAULT = 1000;


// Output constants :	
const STEPPER_DELAY_MILLIS = 1;
const DEFAULT_STEPPER_SEQUENCE = [
	[1, 0, 0, 0],
	[0, 1, 0, 0],
	[0, 0, 1, 0],
	[0, 0, 0, 1],
];
const HALF_STEP_STEPPER_SEQUENCE = [
	[0, 1, 0, 0],
	[0, 1, 0, 1],
	[0, 0, 0, 1],
	[1, 0, 0, 1],
	[1, 0, 0, 0],
	[1, 0, 1, 0],
	[0, 0, 1, 0],
	[0, 1, 1, 0],
];



createOritaMicroClient=function(url, port, isNode=false, staticMicroClientIdParam=null){


	let audioBufferSize = ORITA_CONSTANTS.AUDIO_BUFFER_SIZE;

	
	const staticMicroClientId=(!isNode?nonull(staticMicroClientIdParam,getURLParameter("mid")):null);

	
	const oritaClient={
	
		staticMicroClientId:staticMicroClientId,
	
		onRegistrationEventListeners:[],
		onCommunicationEventListeners:[],
		
		outputsGPIO:{},
		inputsGPIO:{},
		
		
	};
		
		

	oritaClient.start = function(mediasArg = null) {



		const startResultPromise = new Promise((resolve, reject) => {


			// Other parameters :
			let allMediaParametersArray;
			if(!isNode && !mediasArg){
				allMediaParametersArray = getMediaParametersArray(getURLParameter("medias"), getURLParameter("vr"));
			} else { // if we are in a node micro client context :
				allMediaParametersArray = getMediaParametersArray(mediasArg);
			}
			let mediaParametersArray = allMediaParametersArray.medias;




			let captureWidth = null;
			let captureHeight = null;
			let containsVideoOrAudio = false;
			foreach(mediaParametersArray, (mediasConfigElement) => {
				if (contains(["video", "audio"], mediasConfigElement.medias)) {
					containsVideoOrAudio = true;
					return "break";
				}
			});
			
			
			if (containsVideoOrAudio && oritaClient.captureConfig) {
				captureWidth = nonull(oritaClient.captureConfig.width, ORITA_CONSTANTS.DEFAULT_CAPTURE_WIDTH);
				captureHeight = nonull(oritaClient.captureConfig.height, ORITA_CONSTANTS.DEFAULT_CAPTURE_HEIGHT);
			}

			// DBG
			lognow("INFO : MICRO CLIENT : Starting micro client...", mediasArg);


			const doOnStartMediaHandlerMicroClient = (selfMediaHandler = null, medias = null, videoSide = null) => {


				// We have to wait for the medias handler to be ready, before doing anything :

				// TRACE
				if (selfMediaHandler) lognow("INFO : MICRO CLIENT : microClient started with mediahandler.");
				else lognow("INFO : MICRO CLIENT : microClient started without mediahandler.");




				// TODO : FIXME : Utiliser initClient(...) au lieu de directement getStatic(...) avec le paramètre isNode transmis dans l'appel)
				//oritaClient=initClient(isNode,false,doOnServerConnection=null, url, port);
				oritaClient.client={};
				oritaClient.client.socketToServer = WebsocketImplementation.getStatic(isNode).connectToServer(url, port);
				oritaClient.client.socketToServer.onConnectionToServer(() => {




					// TRACE
					console.log("INFO : MICRO CLIENT : Sending register request...");

					// Only micro client determines its own id ! (to avoid multi-registration protocol bug !)

					const microClientId = nonull(oritaClient.staticMicroClientId, getUUID("short"));

					oritaClient.microClientId = microClientId;

					// TRACE
					lognow("INFO : (CLIENT) Microclient will now register to server with microClientId «" + microClientId + "».");


					oritaClient.client.socketToServer.send("protocol", "request.register:microClient:" + microClientId);




	
	
	
					// ------------------------------ CONNECTION EVENTS REGISTRATION ------------------------------
					
					oritaClient.client.socketToServer.receive("protocol", (message) => {
	
						// TRACE
						lognow("INFO : MICRO CLIENT : Server sent a message on the protocol channel : message:", stringifyObject(message));
	
	
						if (contains(message.type, "response.microClient.registered")) {
	
							const microClientId = message.microClientId;
	
							// To be sure this registration response does not concerns another micro client :
							if (oritaClient.microClientId !== microClientId) return;
	
	
							// DBG
							lognow("		MICRO CLIENT REGISTERED : message:", message);
	
	
							// CAUTION : ON TODAY, MICRO CLIENTS ONLY SUPPORT ONE MEDIA TO SEND AT THE TIME !!!
							if (medias === "audio" || medias === "video") {
	
								// TRACE
								lognow("Starting micro client sending method for micro client microClientId «" + microClientId + "»:");
	
	
								// START SENDING AUDIO OR VIDEO :
								// Refreshing rate calculus :
								let refreshingRateMillis = REFRESHING_RATE_MILLIS_DEFAULT;
								if (medias === "audio") {
									// (We want millis, not seconds)
									refreshingRateMillis = Math.floor((audioBufferSize / selfMediaHandler.getAudioSampleRate()) * 1000);
								} else if (medias === "video" && oritaClient.captureConfig) {
									refreshingRateMillis = nonull(oritaClient.captureConfig.refreshMillis, ORITA_CONSTANTS.DEFAULT_CAPTURE_REFRESH_MILLIS);
								}
	
								// *************************
								// LOCAL SENDING LOOP :
								oritaClient.sending = setInterval(function() {
	
									let data = {};
									if (medias === "video") {
	
										let videoData = selfMediaHandler.getVideoData();
	
	
										//		// DBG
										//		console.log("INFO : MICRO CLIENT : videoData : ",videoData);
	
										if (videoData && videoData.data) {
	
											let videoRawData = videoData.data;
	
	
											if (videoData.format !== "base64" && videoData.messWithAlpha) {
												videoRawData = removeAlpha(videoRawData);
											}
	
											// We set the Side is «left» or «right»
											if (videoSide) data.videoSide = videoSide;
	
											//		// TRACE
											//		lognow("CLIENT : getting to send : videoRawData.length:"+videoRawData.length);
	
											// Actually, only handles "LZW" compression, for now...
	
											let stringToSend;
											if (ORITA_CONSTANTS.STRINGIFY_VIDEO_DATA) stringToSend = arrayToString(videoRawData, false, 1);
											else stringToSend = videoRawData;
	
	
	
											data.video = {
												width: videoData.width,
												height: videoData.height,
												data: stringToSend,
												messWithAlpha: videoData.messWithAlpha,
												format: videoData.format,
											};
											data.compression = {
												precision: 1,
												//algorithm:"LZW"
											};
	
										}
	
									} else if (medias === "audio") {
	
										let audioData = selfMediaHandler.getAudioData().data;
	
										//	    						// DBG
										//	      						log("CLIENT : getting to send : audioData:"+audioData.length);
	
										// Actually, only handles "LZW" compression, for now...
										// DBG
										//				    						let stringToSend=arrayToString(audioData,false,0.001);
										//				    						data.audio={ data: stringToSend };
										//				    						data.compression={precision:0.001,
										//	//			    	algorithm:"LZW"
										//				    						};
	
										// DBG
										data.audio = { data: audioData };
										data.compression = {};
	
									} else { // Case no medias selected
										/*DO NOTHING*/
										// // TRACE
										// lognow("WARN : MICRO CLIENT : Caution, micro client has no data to send to main clients !");
										return;
									}
	
	
									//	// TRACE
									//	lognow("CLIENT : SENDING DATA TO SERVER :");
									//	    					// DBG
									//	    					log("data:"+data);
	
	
									data.microClientId = oritaClient.microClientId;
	
									// Micro client starts sending :
									oritaClient.client.socketToServer.send("microClient.send.data", data);
	
	
	
								}
									, refreshingRateMillis); // SENDING LOOP
								// *************************
							}
	
	
	
							// We execute eventual on registration events listeners :
							foreach(oritaClient.onRegistrationEventListeners, (e) => {
								e.execute(microClientId, message);
							});
	
	
							//End of startResultPromise :
							resolve(oritaClient); // CAUTION : Media handler might be ready AFTER this point !
						}
	
					});
	
					oritaClient.client.socketToServer.receive("communication", (message) => {
						// We execute eventual on message events listeners :
						foreach(oritaClient.onCommunicationEventListeners, (e) => {
							if (!e.condition || e.condition(message)) e.execute(message);
						});
					});

				});


			};






			// TODO FIXME : Now, we launch one thread loop for each media handler of this medias array,
			// instead of launching only one thread for all media handlers :



			foreach(mediaParametersArray, (mediaParameters) => {

				let mediaHandlerLocal = null;


				let videoSide = mediaParameters.videoSide;
				let medias = mediaParameters.medias;
				let webcamIndex = mediaParameters.webcamIndex;
				let microphoneIndex = mediaParameters.microphoneIndex;



				// TRACE
				lognow("INFO : MICRO CLIENT : Microclient started :");

				// DBG
				lognow("INFO : MICRO CLIENT : Chosen webcamIndex : " + webcamIndex);
				lognow("INFO : MICRO CLIENT : Chosen microphoneIndex : " + microphoneIndex);


				try {



					if (!isNode) {

						//	OLD : if(webcamIndex)			videoConstraints={deviceId: { exact: cameras[webcamIndex].deviceId }};
						getMediaHandler(
							medias === "video" ? { webcamIndex: webcamIndex } : null,
							medias === "audio" ? { microphoneIndex: microphoneIndex } : null,
							//										CHANNELS_CONFIG,
							{ width: captureWidth, height: captureHeight },
							null, /*(no need for a direct video tag possibility for micro client)*/
							audioBufferSize, 1/*(means mono audio)*/,
							(mh) => {
								//DBG
								console.log("mh:", mh); console.log("medias:", medias);
								doOnStartMediaHandlerMicroClient(mh, medias, videoSide);
							},
							(mh) => { mediaHandlerLocal = mh; });

					} else { // Case node micro client :



						// DBG
						console.log("mediaParameters", mediaParameters);
						console.log("medias", medias);


						const videoConfigLocal = (medias === "video" ? (webcamIndex != null ? { webcamIndex: webcamIndex } : ORITA_CONSTANTS.MOBILE_VIDEO_CONSTRAINTS) : null);
						const audioConfigLocal = (medias === "audio" ? (microphoneIndex != null ? { microphoneIndex: microphoneIndex } : {}) : null);

						// DBG
						console.log("videoConfigLocal", videoConfigLocal);
						console.log("webcamIndex", webcamIndex);
						console.log("ORITA_CONSTANTS.MOBILE_VIDEO_CONSTRAINTS", ORITA_CONSTANTS.MOBILE_VIDEO_CONSTRAINTS);
						console.log("audioConfigLocal", audioConfigLocal);


						mediaHandlerLocal = getNodeMediaHandler(
							videoConfigLocal,
							audioConfigLocal,
							//									CHANNELS_CONFIG,
							{ width: captureWidth, height: captureHeight },
							audioBufferSize, 1/*(means mono audio)*/,
							(mh) => { doOnStartMediaHandlerMicroClient(mh, medias, videoSide); });


					}

				} catch (e) {
					// TRACE
					log("ERROR : Media handler error occured while trying to be initialized for microClient:");
					log(e);
					return;
				}

				//				if(!mediaHandlerLocal){
				//					// TRACE
				//					log("ERROR : Media handler is null for microClient.");
				//					return;
				//				}


			});


			if (empty(mediaParametersArray)) doOnStartMediaHandlerMicroClient();

		});


		return startResultPromise;
	};



	// ******************

	// List manager treatments :
	oritaClient.onRegistrationEventListeners.push({
		execute: (microClientId, message) => {

			const listPosition = message.listPosition;
			if (!nothing(listPosition)) {
				// TRACE
				lognow("INFO : Client has position «" + listPosition + "» in server list.");
				oritaClient.listPosition = listPosition;
			}
		}
	});


	// Inputs / outputs :
	oritaClient.onRegistrationEventListeners.push({
		execute: (microClientId, message) => {

			if (empty(oritaClient.inputsGPIO) && empty(oritaClient.outputsGPIO)) return;


			oritaClient.onCommunicationEventListeners.push({
				condition: (message) => message === "request.server.signalIOs",
				execute: (message) => {

					// DBG
					lognow("SIGNALING !!!");


					if (!empty(oritaClient.inputsGPIO)) {

						// DBG
						lognow("SIGNALING INPUTS");

						oritaClient.client.socketToServer.send("communication", {
							type: "request.microClient.appendInputs",
							inputsGPIO: oritaClient.inputsGPIO,
							microClientId: oritaClient.microClientId,
						});
					}

					if (!empty(oritaClient.outputsGPIO)) {

						// DBG
						lognow("SIGNALING OUTPUTS");

						oritaClient.client.socketToServer.send("communication", {
							type: "request.microClient.appendOutputs",
							outputsGPIO: oritaClient.outputsGPIO,
							microClientId: oritaClient.microClientId,
						});
					}

				},

			});



		},

	});


	// Inputs :
	oritaClient.registerInputsGPIO = (gpios, refreshRateMillis = 1000) => {

		// 0-
		foreach(gpios, (gpioNumber, gpioId) => {
			let input = {
				gpioId: gpioId,
				gpioNumber: gpioNumber,
				state: null,
				init: () => {
					gpioUtils.open(input.gpioNumber, "in");
					input.previousState = null;
				},
				read: () => {

					gpioUtils.read(input.gpioNumber, (state) => {
						input.previousState = input.state;
						input.state = state;

						//							// DBG
						//							console.log("~~~READ STATE : ",input.state);
					});

				},
			};
			input.init();
			oritaClient.inputsGPIO[gpioId] = input;
		});


		// 1-
		oritaClient.onRegistrationEventListeners.push({
			execute: (microClientId, message) => {
				oritaClient.client.socketToServer.send("communication", {
					type: "request.microClient.appendInputs",
					inputsGPIO: oritaClient.inputsGPIO,
					microClientId: oritaClient.microClientId,
				});
			},
		});

		// 3-
		setInterval(() => {

			let inputsGPIO = oritaClient.inputsGPIO;

			foreach(inputsGPIO, (input) => {
				input.read();
			});


			oritaClient.client.socketToServer.send("communication", {
				type: "request.microClient.updateInputsValues",
				inputsGPIO: inputsGPIO,
				microClientId: oritaClient.microClientId,
			});


		}, refreshRateMillis);


		return oritaClient;
	};




	// Outputs :
	oritaClient.stepperIntervalRoutines = {};
	oritaClient.registerOutputsGPIO = (gpios) => {

		// 0-
		foreach(gpios, (gpioInfos, gpioId) => {

			let output = {
				gpioId: gpioId,
				gpioInfos: gpioInfos,
				state: null,
				collisionningOutputsIds: null,
				//					lastPhaseIndex:null,
				//					phaseIndex:0,
				sequenceIndex: 0,
				init: () => {

					if (isNumber(output.gpioInfos)) {
						const gpioNumber = output.gpioInfos;
						gpioUtils.open(gpioNumber, "out");

					} else {
						foreach(output.gpioInfos.phases, (phase) => {
							const gpioNumber = phase;
							gpioUtils.open(gpioNumber, "out");

						});
					}

					output.previousState = null;
					output.state = false;
				},
				on: (doOnEndCommand = null/*UNUSED*/) => {

					// Other outputs exclusion :
					if (output.collisionningOutputsIds) {
						foreach(output.collisionningOutputsIds, (collisionningOutputId) => {
							const collisionningOutput = oritaClient.outputsGPIO[collisionningOutputId];
							if (!collisionningOutput) return "continue";
							collisionningOutput.off(doOnEndCommand);
						}, (collisionningOutputId) => {
							const collisionningOutput = oritaClient.outputsGPIO[collisionningOutputId];
							return collisionningOutput && collisionningOutput.state;
						});
					}


					if (isNumber(output.gpioInfos)) {
						// Case one single 0/1 output :
						const gpioNumber = output.gpioInfos;

						gpioUtils.write(gpioNumber, 1, doOnEndCommand);


					} else {

						// Case stepper motor output:
						const direction = nonull(output.gpioInfos.direction, 1);
						let sortedPhases;
						if (direction == -1) {
							sortedPhases = [];
							if (!empty(output.gpioInfos.phases)) {
								for (let i = output.gpioInfos.phases.length - 1; 0 <= i; i--) {
									sortedPhases.push(output.gpioInfos.phases[i]);
								}
							}
						} else {
							sortedPhases = output.gpioInfos.phases;
						}

						const sequence = nonull(output.gpioInfos.sequence, DEFAULT_STEPPER_SEQUENCE);
						const stepDelayMillis = output.gpioInfos.stepDelayMillis;
						const stepperMethodCallback = async () => {

							const outputSelf = stepperMethodCallback.output;
							const sortedPhasesSelf = stepperMethodCallback.sortedPhases;

							await new Promise((resolve, reject) => setTimeout(resolve, STEPPER_DELAY_MILLIS));
							const sequenceLine = sequence[outputSelf.sequenceIndex];
							if (sequenceLine && !empty(sequenceLine)) {
								foreach(sequenceLine, (phaseState, i) => {
									const gpioNumber = sortedPhasesSelf[i];
									gpioUtils.write(gpioNumber, phaseState, doOnEndCommand);
								});
							}

							if (outputSelf.sequenceIndex < sequence.length - 1) outputSelf.sequenceIndex++;
							else outputSelf.sequenceIndex = 0;
						};

						stepperMethodCallback.output = output;
						stepperMethodCallback.sortedPhases = sortedPhases;
						const stepperMethod = setInterval(stepperMethodCallback, stepDelayMillis);
						oritaClient.stepperIntervalRoutines[gpioId] = stepperMethod;



					}

					output.previousState = output.state;
					output.state = true;
				},
				off: (doOnEndCommand = null/*UNUSED*/) => {

					if (isNumber(output.gpioInfos)) {
						// Case one single 0/1 output :
						const gpioNumber = output.gpioInfos;
						gpioUtils.write(gpioNumber, 0, doOnEndCommand);

					} else {
						// Case stepper motor output:
						foreach(output.gpioInfos.phases, (phase) => {
							const gpioNumber = phase;
							gpioUtils.write(gpioNumber, 0, doOnEndCommand);
						});
						clearInterval(oritaClient.stepperIntervalRoutines[gpioId]);
						oritaClient.stepperIntervalRoutines[gpioId] = null;
					}

					output.previousState = output.state;
					output.state = false;
				},
			};
			output.init();


			// We detect the possible outputs collisions among all the already registered outputs :
			foreach(oritaClient.outputsGPIO, (otherOutput, otherOutputName) => {

				const collisionningOutputsIdsAndPhases = {};
				if (isNumber(otherOutput.gpioInfos)) {
					const phase = otherOutput.gpioInfos;
					if (!collisionningOutputsIdsAndPhases[otherOutputName]) {
						collisionningOutputsIdsAndPhases[otherOutputName] = { outputId: otherOutput.gpioId, phases: [phase] };
					} else {
						collisionningOutputsIdsAndPhases[otherOutputName].phases.push(phase);
					}
				} else {
					foreach(gpioInfos.phases, (phase) => {
						if (contains(otherOutput.gpioInfos.phases, phase)) {
							if (!collisionningOutputsIdsAndPhases[otherOutputName]) {
								collisionningOutputsIdsAndPhases[otherOutputName] = { outputId: otherOutput.gpioId, phases: [phase] };
							} else {
								collisionningOutputsIdsAndPhases[otherOutputName].phases.push(phase);
							}
						}
					});
				}
				if (!empty(collisionningOutputsIdsAndPhases)) {
					//						// DEBUG
					//						lognow("WARN : Found another outputsGPIO using this phase : ",collisionningOutputsIdsAndPhases);
					output.collisionningOutputsIds = [];
					foreach(collisionningOutputsIdsAndPhases, (collisionningOutputIdAndPhases) => {
						const collisionningOutput = oritaClient.outputsGPIO[collisionningOutputIdAndPhases.outputId];
						if (!collisionningOutput) return "continue";
						output.collisionningOutputsIds.push(collisionningOutputIdAndPhases.outputId);
						// Backlink :
						if (!collisionningOutput.collisionningOutputsIds) collisionningOutput.collisionningOutputsIds = [];
						collisionningOutput.collisionningOutputsIds.push(output.gpioId);
					});
				}
			});


			// We add this output to the micro client outputs :
			oritaClient.outputsGPIO[gpioId] = output;
		});

		// 1-
		oritaClient.onRegistrationEventListeners.push({
			execute: (microClientId, message) => {
				oritaClient.client.socketToServer.send("communication", {
					type: "request.microClient.appendOutputs",
					outputsGPIO: oritaClient.outputsGPIO,
					microClientId: oritaClient.microClientId,
				});
			},
		});

		// 3-
		oritaClient.onCommunicationEventListeners.push({
			condition: (message) => message.type === "request.mainClient.outputChanged",
			execute: (message) => {

				//					foreach(message.outputsChanged, (outputChanged)=>{

				const outputChanged = message.outputChanged;

				// DBG
				console.log("!!! CHANGE STATE :", outputChanged);

				const changeToState = (gpioIdParam, stateToPut, doOnEndCommand = null/*UNUSED*/) => {
					let output = oritaClient.outputsGPIO[gpioIdParam];
					if (!output) {
						// TRACE
						lognow("ERROR : Output for gpio id «" + gpioIdParam + "» does not exist for this micro client.");
						return;
					}
					if (stateToPut) output.on(doOnEndCommand);
					else output.off(doOnEndCommand);

					// DBG
					console.log("!!! CHANGE STATE :stateToPut", stateToPut);
				};

				let gpioId = outputChanged.gpioId;

				let changeStateVanilla = true;
				if (message.actionnersConfigs) {

					let actionnerConfig = foreach(message.actionnersConfigs, (ac) => {
						if (ac.gpioId === gpioId) return ac;
					});

					if (actionnerConfig) {

						if (actionnerConfig.gpioId && actionnerConfig.gpioId !== gpioId) {
							return; // DO NOTHING
						}

						if (actionnerConfig.type === "pushOnce") {

							let output = oritaClient.outputsGPIO[gpioId];
							if (!output) {
								// TRACE
								lognow("ERROR : Output for gpio id «" + gpioId + "» does not exist for this micro client.");
								return;
							}

							// ACTUAL STATE :
							if (output.state) return; // DO NOTHING
							// WISHED STATE :
							if (!outputChanged.state) return; // DO NOTHING

							let latencyMillis = nonull(actionnerConfig.latencyMillis, 1000);
							// We set the output to the 1 state :
							changeToState(gpioId, true, (err, stdout, stderr) => {
								if (err) {
									console.log(err);
									return;
								}
								if (stderr) {
									console.log(stderr);
									return;
								}

								// We return the output to the 0 state, after a while :
								setInterval(() => {
									changeToState(gpioId, false);
								}, latencyMillis);

							});

							changeStateVanilla = false;
						}
					}
				}
				if (changeStateVanilla) {
					changeToState(gpioId, outputChanged.state);
				}


				//				});

				// 4-
				oritaClient.client.socketToServer.send("communication", {
					type: "response.microClient.outputChanged",
					outputsGPIO: oritaClient.outputsGPIO,
				});

			},
		});


		return oritaClient;
	};



	// TODO : DEVELOP...

	//		// Mobile platforms:
	//		oritaClient.registerInputsTouch=()=>{
	//				return oritaClient;
	//		};
	//		oritaClient.registerInputsOrientation=()=>{
	//				return oritaClient;
	//		};
	//		// Desktop / potentially mobile platforms:
	//		oritaClient.registerInputsMouse=()=>{
	//				return oritaClient;
	//		};
	//		oritaClient.registerInputsKeyboard=()=>{
	//				return oritaClient;
	//		};


	// INPUTS SENDING :
	oritaClient.sendCommand = (commandName, commandParameters = null) => {
		const microClientId = oritaClient.microClientId;
		const command = { microClientId: microClientId, type: "microClient.send.command", name: commandName, parameters: commandParameters };
		oritaClient.client.socketToServer.send("communication", command);
		return oritaClient;
	};


	return oritaClient;
};




//*********************************** AUTO-ORGANIZING REAL-TIME AORTAC CLUSTERIZATION (AORTAC) *********************************** */




class AORTACClient{
	
		constructor(clientId, serverNodeOrigin, model, view, isNodeContext=true, sslConfig={/*OPTIONAL*/certPath:null,/*OPTIONAL*/keyPath:null}){
			this.clientId=clientId;
			this.serverNodeOrigin=serverNodeOrigin;
			this.isNodeContext=isNodeContext;
			this.sslConfig=sslConfig;
			
			this.model=model;						// must implement functions
			this.view=view; 						// Must implement functions
			this.view.setClient(this);
			
			this.clientInstanceToReferenceServerNode=null;
			this.currentServersNodes={};
			
			
			// TRACE
			lognow(`AORTAC client created with id ${this.clientId}.`);
			
		}
		
		start(){
		
			const self=this;
			
			const splits=splitURL(this.serverNodeOrigin);
			let protocol=nonull(splits.protocol,"ws"), host=nonull(splits.host,"localhost"), port=nonull(splits.port,"30000");
			
			
			// THIS IS ON THE REFERENCE SERER NODE ONLY :
			const clientInstanceToReferenceServerNode=initClient(this.isNodeContext, false, (socketToServer)=>{
				
				
				// First client needs to register to its main reference serer node : 
				// TRACE
				lognow(`		(client ${self.clientId}) Sending client registering request to server node...`);
				
				const helloClientRequest={
					clientId:self.clientId,
					type:"request.register.client",
					isReferenceNode:true,
				};
				socketToServer.send("protocol", helloClientRequest);
				
				// We place a listener from server on this client instance :
				socketToServer.receive("protocol",(message)=>{
					
					
					// Adding listeners :
					if(message.type==="response.register.client"){
						
						// TRACE
						lognow(`		(${self.clientId}) Receving registering response from reference server...`);
						
						// Case reference node:
							
						// Second, once client is registered at its refernce server, then it needs to know to which servers it needs to connect to : 
						// TRACE
						lognow("(client "+self.clientId+") Sending client interrogation request to server node...");
						
						const boundaries=self.view.getBoundaries();
						const interrogationRequest={
							clientId:self.clientId,
							type:"request.interrogation.client",
							boundaries:boundaries
						};
						socketToServer.send("protocol", interrogationRequest);
					

					}
				});
				
				// We place a listener from server on this client instance :
				socketToServer.receive("protocol",(message)=>{
					if(message.type==="response.unregister.client"){
						
						// TRACE
						lognow(`		(${self.clientId}) Receving unregistering response from reference server...`);
						
						// DO NOTHING
						
					}
				});
			 		
				
				// We place a listener from server on this client instance :
				socketToServer.receive("protocol",(message)=>{
					if(message.type==="response.interrogation.client"){
						
						// TRACE
						lognow("!!!!!! ("+self.clientId+") Receving interrogation response from requested server...",message);
						
						// Now the client needs to know to which other nodes it needs to connect in order to get all its model objects :
						const serversNodesIdsForModelObjectsForClient=message.serversNodesIdsForModelObjectsForClient;

						// Now the client needs to know to which other nodes it needs to connect in order to get all its model objects :
						
						
						// We connect (only) to the relevant servers directly :
						foreach(serversNodesIdsForModelObjectsForClient,(nodeInfoAndObjectsIds, serverNodeId)=>{
							
							const splitsServerInfo=splitURL(nodeInfoAndObjectsIds.nodeServerInfo);
							const clientInstanceToAuthorityServerNode=initClient(this.isNodeContext, false, (socketToServer)=>{
							
								// Then we register to them :
								// TRACE
								lognow(`			(client ${self.clientId}) Sending client registering request to authority server node...`);
								
								const helloToAuthorityClientRequest={
									clientId:self.clientId,
									type:"request.register.client",
									isReferenceNode:false,
								};
								socketToServer.send("protocol", helloToAuthorityClientRequest);
							
							
							}, splitsServerInfo.protocol+"://"+splitsServerInfo.host, splitsServerInfo.port, false);
							self.currentServersNodes[serverNodeId]={clientInstance:clientInstanceToAuthorityServerNode};
							clientInstanceToAuthorityServerNode.client.start();
							
							
							clientInstanceToAuthorityServerNode.client.socketToServer.receive("protocol", (message)=>{
								
								// Adding listeners :
								if(message.type==="response.register.client"){
									// We place a listener from server on this client instance :
									// Adding listeners :
									
									// Case non-reference authority node servers :
									
									// TRACE
									lognow(`		(${self.clientId}) Receving registering response from reference server...`);
									
									// We merge our client's model with the partial model sent from the server it just registered to :
									const partialModelString=message.partialModelString;
									const partialModel=JSON.parseRecycle(partialModelString);
									
									// Merging means that all objects in partialModel not being in model must be added to model :
									self.model.clientMergeWith(partialModel);
									
									// TRACE
									lognow("(client "+self.clientId+") Client model has been merged with a partial model from a server node...",self.model);
			
								}
								
								
							});
							
							
							
							clientInstanceToAuthorityServerNode.client.socketToServer.receive("inputs", (message)=>{
								
								// Adding listeners :
								if(message.type==="response.objectsModifiedOrAdded.client"){
									const modifiedOrAddedObjects=JSON.parseRecycle(message.objectsString);
									
									if(!message.isAddingObjects){
										self.model.clientUpdateObjects(modifiedOrAddedObjects);
									}else{
										self.model.clientAddObjects(modifiedOrAddedObjects);
									}
									
									// TRACE
									lognow(`			(client ${self.clientId}) Receving modified or added objects after the inputs...modifiedOrAddedObjects=`,modifiedOrAddedObjects);
									

								}
								
								
							});
											
							
						});
						
					}
				});
				
				
				//self.addListeners();


				
			}, protocol+"://"+host, port, false);
			clientInstanceToReferenceServerNode.client.start();
			this.clientInstanceToReferenceServerNode={clientInstance:clientInstanceToReferenceServerNode};
			
			
			return this;
		}
		

		/*public*/sendInputs(inputs, subBoundaries=null){
			
			const self=this;


			if(subBoundaries){
				// We send client's inputs to ALL its currently connected servers :
				const self=this;
				foreach(this.currentServersNodes,(serversNode,serverNodeId)=>{
					self.sendInputsToServer(serversNode, inputs, subBoundaries);
				});
			}else{
				// If we have no boundaries set for these inputs, then we only send them to one server node at random :
				const serversNode=Math.getRandomInArray(this.currentServersNodes);
				this.sendInputsToServer(serversNode, inputs, subBoundaries);
			}
			
			
		}

		/*private*/sendInputsToServer(serversNode, inputs, subBoundaries=null){
			const clientInstance=serversNode.clientInstance;
			
			const inputsMessage={
				clientId:this.clientId,
				type:"request.inputs.client",
				inputs:inputs,
				subBoundaries:subBoundaries,
			};
				
			clientInstance.client.socketToServer.send("inputs", inputsMessage);
		}





		
}


getAORTACClient=function(clientId=getUUID(), serverNodeOrigin="ws://127.0.0.1:40000", model, view, isNodeContext=true){
	return new AORTACClient(clientId, serverNodeOrigin, model, view, isNodeContext);
}






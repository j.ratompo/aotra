

/* ## Utility global methods in a javascript, at least console (nodejs) server, or vanilla javascript with no browser environment.
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 *  
 */


// COMPATIBILITY browser javascript / nodejs environment :
if(typeof(window)==="undefined")	window=global;	



//=========================================================================
// GLOBAL CONSTANTS :


Math.TAU=6.28318530718; // == Math.PI*2
Math.PSI=1.57079632679; // == Math.PI/2
Math.TAN_RATIOS_DEGREES={
	22.5:0.4142135624,
	45:1,
	67.5:2.4142135624,
	112.5:-2.4142135624,
	135:-1,
	157.5:-0.4142135624,
	202.5:0.4142135624,
	225:1,
	247.5:2.4142135624,
	292.5:-2.4142135624,
	315:-1,
	337.5:-0.4142135624
};
Math.LN_2_INVERTED=1.44269504089; // == 1/ln(2)

Math.INVERSE_OF_SQUARE_OF_TWO=0.707106781;  // == 1/sqrt(2)
Math.RATIO_TO_DEGREES=57.295779513;  		// == 180/PI
Math.RATIO_TO_RADIANS=0.017453293;  		// == PI/180



//=========================================================================


// ==================== COMMONS Geometry ====================





// Geometry mathematics 2D :
window.getPointsOnCircle=function(xParam, yParam, radius, number,resultWrappingMode="none"){
	const CSS_STRING = "cssString";
	var ANGULAR_OFFSET_RADIANS = -Math.PSI;
	var results;
	if(resultWrappingMode === CSS_STRING)
		results = "";
	else
		results = new Array();
	if(number < 0)
		return results;
	if(number === 1){
		var point = new Object();
		point.x = xParam;
		point.y = yParam;
		if(resultWrappingMode === CSS_STRING)
			results += ((empty(results) ? "" : ",") + (point.x + "px " + point.y + "px"));
		else
			results.push(point);
		return results;
	}
	var k = ANGULAR_OFFSET_RADIANS;
	var step = (Math.TAU / number);
	for (var i = 0; i < number; i++){
		var point = new Object();
		point = Math.rotate(k, radius, xParam, yParam);
		if(resultWrappingMode === CSS_STRING)
			results += ((empty(results) ? "" : ",") + (point.x + "px " + point.y + "px"));
		else
			results.push(point);
		k += step;
	}

	return results;
}


Math.onHalfCircleRadians=function(angleRadians){
	return angleRadians<Math.PI ? angleRadians % Math.PI : -angleRadians % Math.PI;
};

Math.onHalfCircleDegrees=function(angleRadians){
	return angleRadians<180 ? angleRadians % 180 : -angleRadians % 180;
};

Math.toRadians = function(degrees,positiveOnly=false){
	// Converts from degrees to radians.
	if(positiveOnly){
		if(degrees<0){
			degrees=degrees+360;
		}
	}
	return (degrees * Math.PI / 180) % (Math.TAU);
};

Math.toDegrees = function(radians,positiveOnly){
	// Converts from radians to degrees.
	if(positiveOnly){
		if(degrees<0){
			degrees=degrees+360;
		}
	}
	return (radians * 180 / Math.PI) % (360);
};


Math.rotateAround=function(angleRadians, xParam=0, yParam=0, xCenterParam=0, yCenterParam=0){
	let point = {};

	let cosAngle=Math.cos(angleRadians);
	let sinAngle=Math.sin(angleRadians);
	
	let deltaX = xParam - xCenterParam;
	let deltaY = yParam - yCenterParam;
	
	point.x = Math.round(cosAngle * deltaX - sinAngle * deltaY) + xCenterParam;
	point.y = Math.round(sinAngle * deltaX + cosAngle * deltaY) + yCenterParam;
	
	return point;
};

// UNUSED (AND UNUSEFUL) :
//Math.getGluedPosition=function(referencePosition, relativePosition){
//	const referenceAngleRadians=referencePosition.getAngle2D();
//	if(relativePosition.baseAngleBeforeGluing==null)	
//		relativePosition.baseAngleBeforeGluing=relativePosition.getAngle2D();
//	relativePosition.setAngle2D(Math.coerceAngle(relativePosition.baseAngleBeforeGluing+referenceAngleRadians,true,true));
//	return Math.rotateAround(referenceAngleRadians, 
//								relativePosition.x, relativePosition.y,
//								referencePosition.x, referencePosition.y);
//};


//// DBG
//let pAngle=0.31416;
//let p1=Math.rotateAround(pAngle,0,10);lognow(p1.x+";"+p1.y);
//let p2=Math.rotateAround(pAngle,p1.x,p1.y);lognow(p2.x+";"+p2.y);
//let p3=Math.rotateAround(pAngle,p2.x,p2.y);lognow(p3.x+";"+p3.y);
//let p4=Math.rotateAround(pAngle,p3.x,p3.y);lognow(p4.x+";"+p4.y);
//let p5=Math.rotateAround(pAngle,p4.x,p4.y);lognow(p5.x+";"+p5.y);
//let p6=Math.rotateAround(pAngle,p5.x,p5.y);lognow(p6.x+";"+p6.y);
//let p7=Math.rotateAround(pAngle,p6.x,p6.y);lognow(p7.x+";"+p7.y);
//let p8=Math.rotateAround(pAngle,p7.x,p7.y);lognow(p8.x+";"+p8.y);



Math.rotate = function(angleRadians, radius, xParam=0, yParam=0){
	let point	= {};
	point.x = radius * Math.round(Math.cos(angleRadians) + xParam);
	point.y = radius * Math.round(Math.sin(angleRadians) + yParam);
	return point;
}

Math.translate = function(point, xDelta, yDelta){
	point.x += xDelta;
	point.y += yDelta;
	return point;
};

window.polarPositionDegreesToCartesianPosition=function(angleDegrees, distance){
	let angleRadians=Math.toRadians(angleDegrees);
	return Math.rotate(angleRadians, distance);
}

window.polarToCartesianPosition=function(angle, distance){
	return Math.rotate(angle, distance);
}

Math.polarPositionRadiansToCartesianPosition3D=(distance,gamma,alpha,xOffset=0,yOffset=0)=>{
	const deltaGamma=(yOffset?(Math.atan(yOffset/distance)):0);
	gamma=Math.coerceAngle(gamma+deltaGamma,true,true);
	const deltaAlpha=(xOffset?(Math.atan(xOffset/distance)):0);
	alpha=Math.coerceAngle(alpha+deltaAlpha,true,true);
	const x=distance*Math.cos(alpha);
	const y=distance*Math.sin(gamma);
	const z=distance*Math.sin(alpha);
	return {x:x,y:y,z:z};
}

// DOES NOT SEEM TO WORK :
//Math.getArcCoordinatesOnSphereFromCartesianVector=function(vector3D,sphereRadius=1){
//	const zAxisAngle=(vector3D.x==0?Math.PI*.5:Math.atan(vector3D.y/vector3D.x));
//	const yAxisAngle=(vector3D.z==0?0:Math.atan(vector3D.x/vector3D.z));
//	const y=yAxisAngle*sphereRadius;
//	const x=zAxisAngle*sphereRadius;
//	return {x:x,y:y};
//}


Math.expApprox=function(x){
	return Math.pow(2,(x*Math.LN_2_INVERTED));
}
Math.atanApprox2=function(x){
	if(x===0)	return 0;
	const MAGIC_FACTOR=1.8;
	let xInverted=1/x;
	return (x<0? -Math.expApprox(xInverted)*MAGIC_FACTOR : Math.expApprox(-xInverted)*MAGIC_FACTOR );
}
Math.atanApprox1=function(x){
	if(x===0)	return 0;
	const MAGIC_OFFSET=1.4;
	const MAGIC_OFFSET_DENUMERATOR=0.714;
	return (x<0? ((1/(-x+MAGIC_OFFSET_DENUMERATOR))-MAGIC_OFFSET) : (-(1/(x+MAGIC_OFFSET_DENUMERATOR))+MAGIC_OFFSET) );
}


Math.atanApprox=function(x){
	if(x===0)	return 0;
	const MAGIC_OFFSET=1.4;
	const MAGIC_OFFSET_DENUMERATOR=0.714;
	const MAGIC_POW=1.4;
	return (x<0? ((1/(Math.pow(-x,MAGIC_POW)+MAGIC_OFFSET_DENUMERATOR))-MAGIC_OFFSET) : (-(1/(Math.pow(x,MAGIC_POW)+MAGIC_OFFSET_DENUMERATOR))+MAGIC_OFFSET) );
}



window.calculateAngleRadians2D=function(p1, p2, approximate=false){
	let x1=p1.x, y1=p1.y;
	let x2=p2.x, y2=p2.y;
	
	let deltaX=x2-x1, deltaY=y2-y1;
	if(deltaX == 0)
		// up case
		if(0 < deltaY)		return Math.PSI;
		// down case
		else if(deltaY < 0)	return Math.PI+Math.PSI;

	if(deltaY == 0)
		// right case
		if(0 < deltaX)		return 0;
		// left case
		else if(deltaX < 0)	return Math.PI;
	
	let angularOffset=0;
	
	if(0 < deltaY)
		// up-left case
		if(deltaX < 0)	angularOffset=-Math.PI; // (because math)
		
	if(deltaY < 0)
		// low-left case
		if(deltaX < 0)	angularOffset=Math.PI; // (because math)
	
	return Math.coerceAngle((approximate?Math.atanApprox(deltaY/deltaX):Math.atan(deltaY/deltaX))+angularOffset,true,true);
}






//// OLD : DOES NOT WORK
//window.calculateAngleRadians2D_OLD=function(p1, p2, approximate=false){
//	let x1=p1.x, y1=p1.y;
//	let x2=p2.x, y2=p2.y;
//	
//	let deltaX=(x2-x1), deltaY=(y2-y1);
//	
//	if(deltaX == 0)
//		// up case
//		if(0 < deltaY)
//			return Math.PSI;
//		else if(deltaY < 0)
//			// down case
//			return Math.PI+Math.PSI;
//
//	if(deltaY == 0)
//		// right case
//		if(0 < deltaX)
//			return 0;
//		else if(deltaX < 0)
//			// left case
//			return Math.PI;
//
//	// up-right case
//	if(0 < deltaY)
//		if(0 < deltaX)
//			return (!approximate?Math.atan(deltaY/deltaX):Math.atanApprox(deltaY/deltaX));
//		// up-left case
//		if(deltaX < 0)
//			return (!approximate?Math.atan(deltaX/-deltaY):Math.atanApprox(deltaX/-deltaY)) + Math.PSI;
//		
//	// low-left case
//	if(deltaY < 0)
//		if(deltaX < 0){
//			// DBG
//			lognow("LOW LEFT CASE");
//			return (!approximate?Math.atan(-deltaY/deltaX):Math.atanApprox(-deltaY/deltaX)) + Math.PI;
//			}
//		// low-right case
//		if(0 < deltaX)
//			return (!approximate?Math.atan(deltaY/deltaX):Math.atanApprox(deltaY/deltaX)) + Math.TAU;
//
//	// If the result is desired in degrees, then uncomment this line instead:
//	// return result*(180/Math.PI);
//	return 0; // By default, returned result is in radians from 0 to TAU (always positive)
//}

window.calculateAngleRadians3D=function(p1, p2, approximate=false){
	let a=calculateAngleRadians2D({x:p1.x, y:p1.y}, {x:p2.x, y:p2.y}, approximate); // alpha => angle [x,y]
	let b=calculateAngleRadians2D({x:p1.z, y:p1.y}, {x:p2.z, y:p2.y}, approximate); // beta  => angle [z,y]
	let g=calculateAngleRadians2D({x:p1.x, y:p1.z}, {x:p2.x, y:p2.z}, approximate); // gamma => angle [x,z]
	return {a:a, b:b, g:g};
}

window.calculateLinearlyMovedPoint2DPolar=function(currentPoint, angleRadians, linearMotion){
	let x = currentPoint.x + linearMotion * Math.cos(angleRadians);
	let y = currentPoint.y + linearMotion * Math.sin(angleRadians);
	return {x:x, y:y};
}


// NEW : WE ACTUALL ONLY NEED 2 ANGLES ! BETA angle in plan [Z,Y] AND GAMMA angle in plan [X,Z] !
window.calculateLinearlyMovedPoint3DPolar=function(currentPoint, anglesRadians, linearMotion){

// IN THIS NORM (MOBILE NORM:)
// alpha => angle [x,y]
// beta  => angle [z,y]
// gamma => angle [x,z]

	let translatedBeta= calculateLinearlyMovedPoint2DPolar({x:currentPoint.z, y:currentPoint.y}, anglesRadians.b, linearMotion);
	let translatedGamma=calculateLinearlyMovedPoint2DPolar({x:currentPoint.x, y:currentPoint.z}, anglesRadians.g, linearMotion);
	
	let x=translatedGamma.x;
	let y=translatedBeta.y;
	let z=translatedBeta.x;
	
	return {x:x, y:y, z:z};
}




//// TEST
//function test(){
//	
//	for(var i=0;i<1000;i++){
//		
//		let x1=Math.round(Math.random()*1000-500);
//		let y1=Math.round(Math.random()*1000-500);
//
//		let x2=Math.round(Math.random()*1000-500);
//		let y2=Math.round(Math.random()*1000-500);
//
//		let p1={x:x1,y:y1};
//		let p2={x:x2,y:y2};
//		
//		console.log(calculateAngleRadians2D(p1,p2)+" approx:"+calculateAngleRadians2D(p1,p2,true)); 
//		
//	}
//	
//}
//test();


Math.coerceAngle=function(angle,isRadians=false,isOnlyPositive=false){
	const WHOLE_ARC=(isRadians?Math.TAU:360);
	let result=0;
	if(angle <= 0)	result=(WHOLE_ARC - Math.abs(angle % WHOLE_ARC)) % WHOLE_ARC;
	else			result=(angle % WHOLE_ARC);
	if(isOnlyPositive && result<0){
		result+=WHOLE_ARC;
	}
	return result;
}


Math.getAnglesDiffOnDemiCircle=(angleSourceParam, angleDestinationParam, isDegrees=false)=>{
	
//	// DOES NOT WORK IN CERTAIN CASES :
//	let result=angleDestination-angleSource;
//	const DEMI_CIRCLE_ANGLE=(isDegrees?180:Math.PI);
//	const WHOLE_ARC=(isDegrees?360:Math.TAU);
//	result= (result + DEMI_CIRCLE_ANGLE) % WHOLE_ARC - DEMI_CIRCLE_ANGLE
//	return result;
//	// DOES NOT WORK IN CERTAIN CASES :
//	let angleDiff=(angleDestination-angleSource);
//	let result=((angleDiff+Math.PI)%Math.TAU)-Math.PI;


	// We do the treatment in radians only, and we convert back to degrees if necessary :
	let angleSource=(isDegrees?Math.toRadians(angleSourceParam):angleSourceParam);
	let angleDestination=(isDegrees?Math.toRadians(angleDestinationParam):angleDestinationParam);
	let angleDiff=Math.abs(angleDestination-angleSource);
	let direction;
	if(Math.PI<angleDiff){
		angleDiff=(Math.TAU-angleDiff);
		if(angleSourceParam<angleDestinationParam)	direction=-1;
		else																				direction=1;
	}else{
		if(angleSourceParam<angleDestinationParam)	direction=1;
		else																				direction=-1;
	}

	let result=direction*(isDegrees?Math.toDegrees(angleDiff):angleDiff);
	return result;
}






// Polygons 

// polygon objects are an array of vertices forming the polygon
//     var polygon1=[{x:100,y:100},{x:150,y:150},{x:50,y:150},...];
// The polygons can be both concave and convex
// return true if the 2 polygons are colliding 
Math.polygonsCollide=function(p1,p2){

    // turn vertices into line points
    var lines1=verticesToLinePoints(p1);
    var lines2=verticesToLinePoints(p2);
    // test each poly1 side vs each poly2 side for intersections
    for(i=0; i<lines1.length; i++){
    for(j=0; j<lines2.length; j++){
        // test if sides intersect
        var p0=lines1[i][0];
        var p1=lines1[i][1];
        var p2=lines2[j][0];
        var p3=lines2[j][1];
        // found an intersection -- polygons do collide
        if(lineSegmentsCollide(p0,p1,p2,p3)){return true;}
    }}
    // none of the sides intersect
    return false;
}
// helper: turn vertices into line points
function verticesToLinePoints(polygon, isLineKeptFunction=null){
    // make sure polys are self-closing
    const firstPoint=polygon[0];
    if(!(firstPoint.x==polygon[polygon.length-1].x && firstPoint.y==polygon[polygon.length-1].y)){
        polygon.push({x:firstPoint.x,y:firstPoint.y});
    }
    var lines=[];
    for(var i=1;i<polygon.length;i++){
        var p1=polygon[i-1];
        var p2=polygon[i];
        if(isLineKeptFunction && !isLineKeptFunction(p1,p2))
        	continue;
        lines.push([ 
            {x:p1.x, y:p1.y},
            {x:p2.x, y:p2.y}
        ]);
    }
    return lines;
}
// helper: test line intersections
// point object: {x:, y:}
// p0 & p1 form one segment, p2 & p3 form the second segment
// Get interseting point of 2 line segments (if any)
// Attribution: http://paulbourke.net/geometry/pointlineplane/
function lineSegmentsCollide(segment1Point1,segment1Point2,segment2Point1,segment2Point2) {

	// ORIGINAL :
	// var unknownA = (p3.x-p2.x) * (p0.y-p2.y) - (p3.y-p2.y) * (p0.x-p2.x);
    // var unknownB = (p1.x-p0.x) * (p0.y-p2.y) - (p1.y-p0.y) * (p0.x-p2.x);
    // var denominator  = (p3.y-p2.y) * (p1.x-p0.x) - (p3.x-p2.x) * (p1.y-p0.y);        
	

	let deltaXPoint1=segment1Point1.x-segment2Point1.x;
	let deltaYPoint1=segment1Point1.y-segment2Point1.y;
	let deltaYPoint2=segment2Point2.y-segment2Point1.y;

	let deltaXSegment1Point2_1=segment1Point2.x-segment1Point1.x;
	let deltaYSegment1Point2_1=segment1Point2.y-segment1Point1.y;
	let deltaXSegment2Point2_1=segment2Point2.x-segment2Point1.x;

    let numerator1 = (deltaXSegment2Point2_1 * deltaYPoint1) - (deltaYPoint2 * deltaXPoint1);
    let numerator2 = (deltaXSegment1Point2_1 * deltaYPoint1) - (deltaYSegment1Point2_1 * deltaXPoint1);
    let denominator = (deltaYPoint2 * deltaXSegment1Point2_1) - (deltaXSegment2Point2_1 * deltaYSegment1Point2_1);        

    // Test if Coincident
    // If the denominator and numerator for the ua and ub are 0
    //    then the two lines are coincident.    
    if(numerator1==0 && numerator2==0 && denominator==0)	return false;

    // Test if Parallel 
    // If the denominator for the equations for ua and ub is 0
    //     then the two lines are parallel. 
    if(denominator == 0) return false;

    // test if line segments are colliding
    numerator1 = numerator1/denominator;
    numerator2 = numerator2/denominator;
    return (numerator1>=0 && numerator1<=1 && numerator2>=0 && numerator2<=1);
}



// http://alienryderflex.com/polygon/
// 1) We draw a line parallel to the X axis, at the Y coordinate of the tested point,
// 2) And we collect all the intersection points (or nodes) with the polygon's sides.
// (We exclude nodes that have the same coordinates as polygon vertices !)
// 3) If there is an odd number (or 1) of nodes on each side (on the X axis) of the point, then the point is inside the polygon  

function getSegmentEquationParameters(p1,p2){
	const deltaX=p2.x-p1.x;
	const deltaY=p2.y-p1.y;
	if(deltaX==0)	return null;
	if(deltaY==0)	return {slope:0, yOffset:p2.y};
	const slope=(deltaX/deltaY);
	const yOffset=-(slope*p1.x-p1.y);
	return {slope:slope, yOffset:yOffset};
}

function getIntersectionPointOnSegmentAtY(p1,p2,yTarget){
	if((yTarget<p1.y && yTarget<p2.y) || (p1.y<yTarget && p2.y<yTarget))	return null;
	const equationParameters=getSegmentEquationParameters(p1,p2);
	if(!equationParameters)	return null;
	if(equationParameters.slope==0)	return null;
	const xCollision=(yTarget-equationParameters.yOffset)/equationParameters.slope;
	return {x:xCollision, y:yTarget};
}

Math.isPointInPolygon=function(point,polygon){

	const yTarget=point.y;
	const lines=verticesToLinePoints(polygon,(p1,p2)=>{
		return !((yTarget<p1.y && yTarget<p2.y) || (p1.y<yTarget && p2.y<yTarget));
	});
	
	if(empty(lines))	return false;
	
	let leftNodesCount=0;
	let rightNodesCount=0;
	foreach(lines,(line)=>{
		const p1=line[0];
		const p2=line[1];
		const nodePoint=getIntersectionPointOnSegmentAtY(p1,p2,yTarget);
		if(!nodePoint)	return "continue";
		if(nodePoint.x<point.x)	leftNodesCount++;
		else					rightNodesCount++;
	});
	
	if(leftNodesCount==0 || rightNodesCount==0)	return false;
	if(!Math.isEven(leftNodesCount) && !Math.isEven(rightNodesCount))	return true;
	
	return false;
};


getScaledPolygon=function(points, scaleFactorX, scaleFactorY){
	const results=[];
	foreach(points,(p)=>{
		if(p.x!=null && p.y!=null){
			results.push({x:p.x*scaleFactorX, y:p.y*scaleFactorY});
		}else if(p.length && p.length==2){
			results.push([p[0]*scaleFactorX, p[1]*scaleFactorY]);
		}
	});
	return results;
};

getTranslatedPolygon=function(points, deltaX, deltaY){
	const results=[];
	foreach(points,(p)=>{
		if(p.x!=null && p.y!=null){
			results.push({x:p.x+deltaX, y:p.y+deltaY});
		}else if(p.length && p.length==2){
			results.push([p[0]+deltaX, p[1]+deltaY]);
		}
	});
	return results;
};

getRotatedPolygon=function(points, angleRadians, rotationCenter={x:0,y:0}){
	const results=[];
	foreach(points,(p)=>{
		if(p.x!=null && p.y!=null){
			const rotatedPoint=Math.rotateAround(angleRadians, p.x, p.y, rotationCenter.x, rotationCenter.y);
			results.push(rotatedPoint);
		}else if(p.length && p.length==2){
			const rotatedPoint=Math.rotateAround(angleRadians, p[0], p[1], rotationCenter.x, rotationCenter.y);
			results.push([rotatedPoint.x, rotatedPoint.y]);
		}
	});
	return results;
};






window.isOriented=function(orientation, width, height){
	if(orientation == "portrait"){
		return width <= height;
	}
	if(orientation == "landscape"){
		return width > height;
	}
	throw new Error("Please provide \"portrait\" or \"landscape\" as orientation.");

}







// Shadowing :


window.getSunShadowProjectedPoint=function(originalPoint,angleDegreesParam,groundY,exagerationFactor=1,precision=2){
	
	// To correct a strange bug due to the y-axis inversion :
	let angleDegrees=Math.coerceAngle(90-Math.roundTo(angleDegreesParam,precision));
	
	let result={x:originalPoint.x,y:originalPoint.y};
	
	let deltaY=originalPoint.y-groundY;

	
	if(angleDegrees<90){
		// In this case, deltaY should be positive !
		if(deltaY<0)	deltaY=-deltaY;
		result.x=result.x-deltaY*Math.tan(Math.toRadians(angleDegrees))*exagerationFactor;
	}else if(angleDegrees<180){
		// In this case, deltaY should be positive !
		if(deltaY<0)	deltaY=-deltaY;
		result.x=result.x+deltaY*Math.tan(Math.toRadians( 180-angleDegrees ))*exagerationFactor;
	}else if(angleDegrees<270){
		// In this case, deltaY should be negative !
		if(0<deltaY)	deltaY=-deltaY;
		result.x=result.x+deltaY*Math.tan(Math.toRadians( angleDegrees-180 ))*exagerationFactor;
	}else{
		// In this case, deltaY should be negative !
		if(0<deltaY)	deltaY=-deltaY;
		result.x=result.x-deltaY*Math.tan(Math.toRadians( 360-angleDegrees))*exagerationFactor;
	}
	result.y=groundY;
	
	
	return result;
	
}


window.isInZone=function(point, zone, zoneOffsets=null, center={x:"center",y:"center",z:"center"}, invertYAxis=false, zooms=null){
	
	if(!center){
		center={x:"center",y:"center",z:"center"}
	}
	if(invertYAxis==null){
		invertYAxis=false;
	}
	if(zooms){
		point=getZoomedPosition(point, zooms);
		zone=getZoomedZone(zone, zooms);
	}
	
	let pointX=point.x;
	let pointY=point.y;
	let pointZ=point.z;
	
	let zoneX=zone.x;
	let zoneY=zone.y;
	let zoneZ=zone.z;
	if(zoneOffsets){
		if(zoneX!=null)	zoneX+=nonull(zoneOffsets.x,0);
		if(zoneY!=null)	zoneY+=nonull(zoneOffsets.y,0);
		if(zoneZ!=null)	zoneZ+=nonull(zoneOffsets.z,0);
	}
	
	// We ignore zone parameters if they are null or 0 : 
	let isXInZone=zone.w?false:true;
	let isYInZone=zone.h?false:true;
	let isZInZone=zone.d?false:true;
	
	
	if(zone.w || zone.h || zone.d){

		if(zone.w){
			if(pointX!=null && zoneX!=null){
				if(center.x==="center"){
					let demiSize=Math.floor(zone.w*.5);
					if((zoneX - demiSize) <= pointX && pointX <= (zoneX + demiSize) )
						isXInZone=true;
				}else if(center.x==="right"){
					if((zoneX - zone.w) <= pointX && pointX <= zoneX )
						isXInZone=true;
				}else if(center.x==="left"){
					if(zoneX <= pointX && pointX <= (zoneX + zone.w) )
						isXInZone=true;
				}
			}else{
				isXInZone=true;
			}
		}
		
		if(zone.h){
			if(pointY!=null && zoneY!=null){
				if(center.y==="center"){
					let demiSize=Math.floor(zone.h*.5);
					if((zoneY - demiSize) <= pointY && pointY <= (zoneY + demiSize) )
						isYInZone=true;
				}else if(center.y==="top"){
					if(!invertYAxis){
						if((zoneY - zone.h) <= pointY && pointY <= zoneY )
							isYInZone=true;
					}else{
						if(zoneY <= pointY && pointY <= (zoneY + zone.h) )
							isYInZone=true;
					}
				}else if(center.y==="bottom"){
					if(!invertYAxis){
						if(zoneY <= pointY && pointY <= (zoneY + zone.h) )
							isYInZone=true;
					}else{
						if((zoneY - zone.h) <= pointY && pointY <= zoneY )
							isYInZone=true;
					}
				}
			}else{
				isYInZone=true;
			}
		}
		
		if(zone.d){
			if(pointZ!=null && zoneZ!=null){
				if(center.z==="center"){
					let demiSize=Math.floor(zone.d*.5);
					if((zoneZ - demiSize) <= pointZ && pointZ <= (zoneZ + demiSize) )
						isZInZone=true;
				}else if(center.z==="front"){
					if((zoneZ - zone.d) <= pointZ && pointZ <= zoneZ )
						isZInZone=true;
				}else if(center.z==="back"){
					if(zoneZ <= pointZ && pointZ <= (zoneZ + zone.d) )
						isZInZone=true;
				}
			}else{
				isZInZone=true;
			}
		}
		
		
	}else{
		
		if(pointX!=null && zoneX!=null){
			if(center.x==="center"){
				let demiW=Math.floor(zone.w*.5);
				if((zoneX - demiW) <= pointX && pointX <= (zoneX + demiW)) 
						isXInZone=true;
			}else if(center.x==="right"){
				if((zoneX - zone.w) <= pointX && pointX <= zoneX )
						isXInZone=true;
			}else if(center.x==="left"){
				if( zoneX <= pointX && pointX <= (zoneX + zone.w ) )
					isXInZone=true;
			}
		}else{
			isXInZone=true;
		}

		if(pointY!=null && zoneY!=null){
			if(center.y==="center"){
				let demiH=Math.floor(zone.h*.5);
				if((zoneY - demiH) <= pointY && pointY <= (zoneY + demiH)) 
						isYInZone=true;
			}else if(center.y==="top"){
				if(!invertYAxis){
					if((zoneY - zone.h) <= pointY && pointY <= zoneY )
							isYInZone=true;
				}else{
					if( zoneY <= pointY && pointY <= (zoneY + zone.h ) )
						isYInZone=true;
				}
			}else if(center.y==="bottom"){
				if(!invertYAxis){
					if( zoneY <= pointY && pointY <= (zoneY + zone.h ) )
						isYInZone=true;
				}else{
					if((zoneY - zone.h) <= pointY && pointY <= zoneY )
						isYInZone=true;
				}
			}
		}else{
			isYInZone=true;
		}
		
		if(pointZ!=null && zoneZ!=null){
			if(center.z==="center"){
				let demiW=Math.floor(zone.d*.5);
				if((zoneZ - demiW) <= pointZ && pointZ <= (zoneZ + demiW)) 
						isZInZone=true;
			}else if(center.z==="front"){
				if((zoneZ - zone.d) <= pointZ && pointZ <= zoneZ )
						isZInZone=true;
			}else if(center.z==="back"){
				if( zoneZ <= pointZ && pointZ <= (zoneZ + zone.d ) )
					isZInZone=true;
			}
		}else{
			isZInZone=true;
		}
			

	}
	
	return isXInZone && isYInZone && isZInZone;
}


Math.getQuadrant=function(centerPoint,point){
	let deltaX=point.x-centerPoint.x;
	let deltaY=point.y-centerPoint.y;
	if(deltaY<=deltaX){
		if(deltaX>0){
			return "E";
		}else{
			return "W";
		}
	}else{ // deltaX<deltaY
		if(deltaY>0){
			return "N";
		}else{
			return "S";
		}
	}
}



Math.compareDistances=function(p1, p2, distance, comparison){
	let distanceSquare=Math.pow(distance, 2);
	let diffSquare=Math.getSquaredDistance(p1, p2);
	if(comparison === "gt")
		return distanceSquare < diffSquare;
	if(comparison === "ge")
		return distanceSquare <= diffSquare;
	if(comparison === "lt")
		return diffSquare < distanceSquare;
	if(comparison === "le")
		return diffSquare <= distanceSquare;
	// TRACE
	log("ERROR: Please provide a «comparison» parameter");
	return false;
};

Math.getSquaredDistance=function(p1, p2){
	return Math.pow(Math.abs(p2.x-p1.x), 2) + Math.pow(Math.abs(p2.y-p1.y), 2) + ((p1.z!=null && p2.z!=null)?Math.pow(Math.abs(p2.z-p1.z), 2):0);
};

Math.getDistance=function(p1, p2){
	return Math.sqrt(Math.getSquaredDistance(p1,p2));
};




Math.getBarycenter=function(coordsArray){
	
	let coordsX=[];
	let coordsY=[];
	foreach(coordsArray,(coords)=>{
		coordsX.push(coords.x);
		coordsY.push(coords.y);
	});

	// Barycenters calculation :
	let bx=Math.floor(Math.averageInArray(coordsX));
	let by=Math.floor(Math.averageInArray(coordsY));

	
	return {x:bx,y:by};
}


Math.getBarycentersFromZonesWithColors=function(zonesWithColors, withColors=true){
	let barycenters=[];
	
	foreach(zonesWithColors,(singleContigousZonePixelsCoords)=>{
		let barycenter=Math.getBarycenter(singleContigousZonePixelsCoords);
		barycenters.push(barycenter);
		
		let zoneLength=singleContigousZonePixelsCoords.length;
		
		if(withColors){
			// barycenter color :
			let avgR=0;
			let avgG=0;
			let avgB=0;
			foreach(singleContigousZonePixelsCoords,(coords)=>{
				let color=coords.color;
				avgR+=color.r;
				avgG+=color.g;
				avgB+=color.b;
			});
			barycenter.color={r:avgR/zoneLength, g:avgG/zoneLength, b:avgB/zoneLength};
		}else{
			barycenter.color={r:128, g:128, b:128};
		}
		
	});

	return barycenters;
}


Math.getBidimensional8Direction=(currentPosition,destination,invertYAxis=false)=>{

	let x=currentPosition.x;
	let y=currentPosition.y;
	
	let deltaX=(destination.x-x);
	let deltaY=(invertYAxis?-1:1)*(destination.y-y);

	if(deltaX===0 && deltaY===0)	return null;

	let directionXAxis=(deltaX<0?"W":"E");
	let directionYAxis=(deltaY<0?"S":"N");
	if(deltaX===0)	return directionYAxis;
	if(deltaY===0)	return directionXAxis;
	
	let tanRatio=(deltaY/deltaX);
	
	if(0<deltaX && 0<deltaY){ // 0 -> PI/2 quarter :
		if(tanRatio<=Math.TAN_RATIOS_DEGREES[22.5])	return "E";
		if(Math.TAN_RATIOS_DEGREES[22.5]<=tanRatio && tanRatio<Math.TAN_RATIOS_DEGREES[67.5])	return "NE";
		if(Math.TAN_RATIOS_DEGREES[67.5]<=tanRatio)	return "N";
	}else if(deltaX<0 && 0<deltaY){ // PI/2 -> PI quarter : 
		if(tanRatio<=Math.TAN_RATIOS_DEGREES[112.5])	return "N";
		if(Math.TAN_RATIOS_DEGREES[112.5]<=tanRatio && tanRatio<Math.TAN_RATIOS_DEGREES[157.5])	return "NW";
		if(Math.TAN_RATIOS_DEGREES[157.5]<=tanRatio)	return "W";
	}else if(deltaX<0 && deltaY<0){ // PI -> (3/2)*PI quarter : 
		if(tanRatio<=Math.TAN_RATIOS_DEGREES[202.5])	return "W";
		if(Math.TAN_RATIOS_DEGREES[202.5]<=tanRatio && tanRatio<Math.TAN_RATIOS_DEGREES[247.5])	return "SW";
		if(Math.TAN_RATIOS_DEGREES[247.5]<=tanRatio)	return "S";
	}else{ // (3/2)*PI -> 2*PI quarter :
		if(tanRatio<=Math.TAN_RATIOS_DEGREES[292.5])	return "S";
		if(Math.TAN_RATIOS_DEGREES[292.5]<=tanRatio && tanRatio<Math.TAN_RATIOS_DEGREES[337.5])	return "SE";
		if(Math.TAN_RATIOS_DEGREES[337.5]<=tanRatio)	return "E";
	}
	return null;
};



/*public static*/window.getRandomPositionInZone=function(positionZone, xOffset=0, yOffset=0, zOffset=0, avoidOverlap=null, size=null, randomizeAngles=false){
	
	let w=nonull(positionZone.w,0);
	let h=nonull(positionZone.h,0);
	let d=nonull(positionZone.d,0);

	let demiW=w*.5;
	let demiH=h*.5;
	let demiD=d*.5;
	
	let xOrZero=nonull(positionZone.x,0);
	let yOrZero=nonull(positionZone.y,0);
	let zOrZero=nonull(positionZone.z,0);
	
	// If respectively width or height is 0, then there is no need to use a randomization !
	let x= ( w<=0 ? xOrZero : (xOrZero + Math.getRandomInt(Math.floor(demiW), -Math.ceil(demiW)))) + xOffset;
	let y= ( h<=0 ? yOrZero : (yOrZero + Math.getRandomInt(Math.floor(demiH), -Math.ceil(demiH)))) + yOffset;
	let z= ( d<=0 ? zOrZero : (zOrZero + Math.getRandomInt(Math.floor(demiD), -Math.ceil(demiD)))) + zOffset;
	
	if(avoidOverlap && size){
		
		let itemW=nonull(size.w,0);
		let itemH=nonull(size.h,0);
		let itemD=nonull(size.d,0);
		
		if(xOrZero/*includes case==0*/ && 0<itemW && contains(avoidOverlap,"x")) x=Math.floor(x/itemW)*itemW;
		if(yOrZero/*includes case==0*/ && 0<itemH && contains(avoidOverlap,"y")) y=Math.floor(y/itemH)*itemH;
		if(zOrZero/*includes case==0*/ && 0<itemD && contains(avoidOverlap,"z")) z=Math.floor(z/itemD)*itemD;

	}

	let b=positionZone.b;
	let g=positionZone.g;
	let a=positionZone.a;
	if(randomizeAngles){
		b=Math.random()*Math.PI*2;
		g=Math.random()*Math.PI*2;
		a=Math.random()*Math.PI*2;
	}

	return {x:x, y:y, z:z, b:b, g:g, a:a, parallax:positionZone.parallax};
}


// CAUTION : ONLY HANDLES «center-positionned» ZONES !
/*public static*/window.isZoneCollidingWithZone=function(zone1, zone2){

	let demiWidth1=zone1.w*.5;
	let demiWidth2=zone2.w*.5;

	let xMin1=zone1.x-demiWidth1;
	let xMax1=zone1.x+demiWidth1;
	let xMin2=zone2.x-demiWidth2;
	let xMax2=zone2.x+demiWidth2;

//OLD :	let widthsOverlap=((xMin2<xMin1 && xMin1<xMax2) || (xMin2<xMax1 && xMax1<xMax2)
//						|| (xMin1<xMin2 && xMin2<xMax1) || (xMin1<xMax2 && xMax2<xMax1));

	let widthsOverlap=(xMin1<xMax2 && xMin2<xMax1);


	let demiHeight1=zone1.h*.5;
	let demiHeight2=zone2.h*.5;

	let yMin1=zone1.y-demiHeight1;
	let yMax1=zone1.y+demiHeight1;
	let yMin2=zone2.y-demiHeight2;
	let yMax2=zone2.y+demiHeight2;

//OLD : let heightsOverlap=((yMin1<yMin2 && yMin2<yMax1) || (yMin1<yMax2 && yMax2<yMax1)
//					 || (yMin2<yMin1 && yMin1<yMax2) || (yMin2<yMax1 && yMax1<yMax2));

	let heightsOverlap=(yMin1<yMax2 && yMin2<yMax1);
	
	return widthsOverlap && heightsOverlap;
}



// CAUTION : ONLY HANDLES «center-positionned» ZONES !
/*public static*/window.getIntersectionZone=function(zone1, zone2){
	if(!isZoneCollidingWithZone(zone1, zone2))	return null;
	
	let edges=[];
	
	
	// Horizontal axis :
	let hasXAxisWholeZone1=false;
	let hasXAxisWholeZone2=false;
	
	let demiWidth1=zone1.w*.5;
	let demiWidth2=zone2.w*.5;
	let xMin1=zone1.x-demiWidth1;
	let xMax1=zone1.x+demiWidth1;
	let xMin2=zone2.x-demiWidth2;
	let xMax2=zone2.x+demiWidth2;	

	let deltaXMins=xMin2-xMin1;
	let deltaXMaxs=xMax2-xMax1;
	
	let deltaXMinsSign=(deltaXMins<0?-1:1);
	let deltaXMaxsSign=(deltaXMaxs<0?-1:1);
	let intersectionXMin=0;
	let intersectionXMax=0;
	if(deltaXMinsSign==deltaXMaxsSign){ // case «parallelogram»
		if(deltaXMinsSign<0){ // case /=/ parallelogram
			intersectionXMin=xMin1;
			intersectionXMax=xMax2;
			// We calculate edges :
			edges.push("left");
		}else{	 // case \=\ parallelogram
			intersectionXMin=xMin2;
			intersectionXMax=xMax1;
			// We calculate edges :
			edges.push("right");
		}
	}else{  // case «trapeze»
		if(deltaXMinsSign<0){ // case /=\ trapeze
			intersectionXMin=xMin1;		// Basically zone1 x !
			intersectionXMax=xMax1;
			// We calculate edges :
			hasXAxisWholeZone1=true;
		}else{	 // case \=/ trapeze
			intersectionXMin=xMin2;		// Basically zone2 x !
			intersectionXMax=xMax2;
			// We calculate edges :
			hasXAxisWholeZone2=true;
		}
	}

	let w=intersectionXMax-intersectionXMin;
	let x=(intersectionXMax+intersectionXMin)*.5; // (The average)


	// Vertical axis :

	let hasYAxisWholeZone1=false;
	let hasYAxisWholeZone2=false;

	let demiHeight1=zone1.h*.5;
	let demiHeight2=zone2.h*.5;

	let yMin1=zone1.y-demiHeight1;
	let yMax1=zone1.y+demiHeight1;
	let yMin2=zone2.y-demiHeight2;
	let yMax2=zone2.y+demiHeight2;
	

	let deltaYMins=yMin2-yMin1;
	let deltaYMaxs=yMax2-yMax1;
	
	let deltaYMinsSign=(deltaYMins<0?-1:1);
	let deltaYMaxsSign=(deltaYMaxs<0?-1:1);
	let intersectionYMin=0;
	let intersectionYMax=0;
	if(deltaYMinsSign==deltaYMaxsSign){ // case «parallelogram»
		if(deltaYMinsSign<0){ // case /=/ parallelogram
			intersectionYMin=yMin1;
			intersectionYMax=yMax2;
			// We calculate edges :
			edges.push("bottom");		// Here the Y axis is NOT inverted !
		}else{	 // case \=\ parallelogram
			intersectionYMin=yMin2;
			intersectionYMax=yMax1;
			// We calculate edges :
			edges.push("top");	// Here the Y axis is NOT inverted !
		}
	}else{  // case «trapeze»
		if(deltaYMinsSign<0){ // case /=\ trapeze
			intersectionYMin=yMin1;		// Basically zone2 y !
			intersectionYMax=yMax1;
			// We calculate edges :
			hasYAxisWholeZone1=true;
		}else{	 // case \=/ trapeze
			intersectionYMin=yMin2;		// Basically zone2 y !
			intersectionYMax=yMax2;
			// We calculate edges :
			hasYAxisWholeZone2=true;
		}
	}

	let h=intersectionYMax-intersectionYMin;
	let y=(intersectionYMax+intersectionYMin)*.5; // (The average)
	
	// We calculate edges :
	if(hasXAxisWholeZone1 && hasYAxisWholeZone1)	edges.push("wholeZone1");
	if(hasXAxisWholeZone2 && hasYAxisWholeZone2)	edges.push("wholeZone2");
	
	return {x:x,y:y,w:w,h:h,edges:edges};
}

/*public static*/window.areZonesIdentical=function(zone1, zone2){
	return (zone1==zone2 || (zone1.x==zone2.x || zone1.y==zone2.y || zone1.w==zone2.w || zone1.h==zone2.h));
}



// 3D :


/*
// (needs the jsgl library file !)
// UNUSED
function screenToSpherePt(mouseX, mouseY) {
	var p = { x: camera_mat.e12, y: camera_mat.e13, z: camera_mat.e14 + 1 };
	// camera dir
	var r = { x: camera_mat.e0, y: camera_mat.e1, z: camera_mat.e2 };
	var up = { x: camera_mat.e4, y: camera_mat.e5, z: camera_mat.e6 };
	var right = { x: camera_mat.e8, y: camera_mat.e9, z: camera_mat.e10 };
	var tan_half = Math.tan(horizontal_fov_radians / 2);
	r = jsgl.vectorAdd(r, jsgl.vectorScale(right, mouseX * tan_half));
	r = jsgl.vectorAdd(r, jsgl.vectorScale(up, mouseY * tan_half));
	r = jsgl.vectorNormalize(r);

	return rayVsUnitSphereClosestPoint(p, r);
}
*/

/*
// (needs the jsgl library file !)
// UNUSED
// Return the first exterior hit or closest point between the unit
// sphere and the ray starting at p and going in the r direction.
function rayVsUnitSphereClosestPoint(p, r) {
	var p_len2 = jsgl.dotProduct(p, p);
	if (p_len2 < 1) {
		// Ray is inside sphere, no exterior hit.
		return null;
	}

	var along_ray = -jsgl.dotProduct(p, r);
	if (along_ray < 0) {
		// Behind ray start-point.
		return null;
	}

	var perp = jsgl.vectorAdd(p, jsgl.vectorScale(r, along_ray));
	var perp_len2 = jsgl.dotProduct(perp, perp);
	if (perp_len2 >= 0.999999) {
		// Return the closest point.
		return jsgl.vectorNormalize(perp);
	}

	// Compute intersection.
	var e = Math.sqrt(1 - jsgl.dotProduct(perp, perp));
	var hit = jsgl.vectorAdd(p, jsgl.vectorScale(r, (along_ray - e)));
	return jsgl.vectorNormalize(hit);
}
*/





















// MUST REMAIN AT THE END OF THIS LIBRARY FILE !

AOTRAUTILS_GEOMETRY_LIB_IS_LOADED=true;
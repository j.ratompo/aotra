

/* ## Utility global methods in a javascript, at least console (nodejs) server, or vanilla javascript with no browser environment.
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 *  
 */


// COMPATIBILITY browser javascript / nodejs environment :
if(typeof(window)==="undefined")	window=global;	



//=========================================================================
// GLOBAL CONSTANTS :
PERFORM_TESTS_ON_LIBRARY=false;

//FLATTEN / UNFLATTEN JSON CONSTANTS :

const ROOT_UUID="00000000-0000-0000-0000-000000000000";
const DEFAULT_UUID_ATTR_NAME="JSONid";/* Yet Another UUID */
const DEFAULT_CLASSNAME_ATTR_NAME="JSONtype";/* Yet Another Classname */
const DEFAULT_POINTER_TO_ATTR_NAME="JSONref";/* Yet Another PointerTo */

//=========================================================================


// - console logging :
// NOT AOTESTABLE !
// DEPRECATED :
window.log=function(errorMessage,/*OPTIONAL*/trace,/*OPTIONAL*/failToAlert){

	// To log message :
	if(typeof console !== "undefined"){
		if(console.log && !trace){
			console.log(errorMessage);
			return errorMessage;
		}
	} else if(failToAlert && typeof alert !== "undefined"){
		alert(errorMessage);
		return errorMessage;
	}

	// To add an error stack if we want to display it :
	// CAUTION : THE FUNCTION WILL NOT RETURN THE ERROR MESSAGE IN THIS CASE !
	setTimeout(function(){
		throw new Error(errorMessage);
		// throw new Exception(errorMessage);
	}, 1);

	return errorMessage;
}


window.lognow=function(errorMessage,object=null,/*OPTIONAL*/trace=false,/*OPTIONAL*/failToAlert=false){

	const LOG_SEPARATOR=":";
	
	// To log message :
	if(typeof console !== "undefined"){
		if(console.log && !trace){
			if(object)	console.log(getNow()+LOG_SEPARATOR+errorMessage,object);
			else				console.log(getNow()+LOG_SEPARATOR+errorMessage);
			return errorMessage;
		}
	} else if(failToAlert && typeof alert !== "undefined"){
		if(object)		alert(getNow()+LOG_SEPARATOR+errorMessage+LOG_SEPARATOR+object);
		else					alert(getNow()+LOG_SEPARATOR+errorMessage);
		return errorMessage;
	}

	// To add an error stack if we want to display it :
	// CAUTION : THE FUNCTION WILL NOT RETURN THE ERROR MESSAGE IN THIS CASE !
	setTimeout(function(){
		if(object)		throw new Error(getNow()+LOG_SEPARATOR+errorMessage+LOG_SEPARATOR+object);
		else					throw new Error(getNow()+LOG_SEPARATOR+errorMessage);
		// throw new Exception(errorMessage);
	}, 1);

	return errorMessage;
}

//================= aotra andrana (means «trial, try» in malagasy) sub-component =================
// unit and integration testing framework
//================================================================================================




// (Duplicated doc with aotrautils-jquery)
// ============================== COMPLETE AOTEST DOCUMENTATION BELOW ============================== 
//
// Unit tests framework embedded in aotra (called "aotest") : (inherits this utils library license)
// «aotest» is a short name used in code for «aotest», which is the real name.
// - LAUNCH INFORMATION :
// All tests are registered as javascript declaring function is executed.
// To execute registered tests, simply execute aotest.run(); after the tests registration code is executed.
// - DEPENDENCIES INFORMATION :
// (DEPENDENCY : the nothing(...) function in utils library)
// (DEPENDENCY : the log(...) function in utils library)
// (DEPENDENCY : the isArray(...) function in utils library)
// (DEPENDENCY : the getFunctionName(...) function in utils library)
// (DEPENDENCY : the getHashedString(...) function in utils library)
// (DEPENDENCY : the getUniqueIdWithDate(...) function in utils library)
// (DEPENDENCY : the getURLParameter(...) function in utils library)
// (DEPENDENCY : jquery methods and functions : if you want to run UI user tests, to simulate UI events and test the outcome... -examples below-)
// - MISCELLANEOUS :
// (Note to developer : functions / methods used in function tracking CANNOT be tested with aotest framework ! or else there will be calls overflow errors. Trust me. I tried.)
// - UNIT TESTS EXAMPLES :
////  Data and treatments testing :
////  Examples : 
//var addPoints2DCoordinates=window.aotest(
////  Tests definition
//{
//  //  Tested function/method name : 
//  name : "addPoints2DCoordinates"
//  //  Dummies :
//  ,
//  dummies : function(){
//      var result=new Object();
//      result.pDummy1=new Object();
//      result.pDummy1.x=20;
//      result.pDummy1.y=10;
//      result.pDummy2=new Object();
//      result.pDummy2.x=60;
//      result.pDummy2.y=40;
//
//      return result;
//  }
//  //  Normal scenarii :
//  , scenarioNormal1 : [ [ {
//      dummy : "pDummy1"
//  }, {
//      dummy : "pDummy2"
//  } ], function(result){
//      return result.x === 80 && result.y === 50;
//  } ],
//  scenarioNormal2 : [ [ {
//      dummy : "pDummy1"
//  }, {
//      dummy : "pDummy2"
//  } ], {
//      asserts : "success"
//  } ]
//  //  Abnormal scenarii :
//  , scenarioParameter1IsNull : [ [ null, {
//      dummy : "pDummy2"
//  } ], {
//      asserts : "fail"
//  } ],
//  scenarioParameter2IsNull : [ [ {
//      dummy : "pDummy1"
//  }, null ], {
//      asserts : "fail"
//  } ]
//}
//// Function definition
//, function(p1, p2){
//  var result=new Object();
//  aotest.assert(p1 === null, "fail");
//  aotest.assert(p2 === null, "fail");
//  result.x=p1.x + p2.x;
//  result.y=p1.y + p2.y;
//  return result;
//});
//
////  (Function can share the same dummies because dummies are global)
//var invertPoints2DCoordinates=window.aotest(
//
////  Tests definition 
//{
//  //  Tested function/method name :
//  name : "invertPoints2DCoordinates"
//
//  //  Dummies :
//  , dummies : null
//  //  Normal scenarii : 
//  , scenarioNormal1 : [ [ {
//      dummy : "pDummy1"
//  } ], function(result){
//      return result.x === -20 && result.y === -10;
//  } ],
//  scenarioNormal2 : [ [ {
//      dummy : "pDummy2"
//  } ], function(result){
//      return result.x === -60 && result.y === -40;
//  } ],
//  scenarioNormal3 : [ [ {
//      dummy : "pDummy1"
//  } ], {
//      asserts : "success"
//  } ]
//  //  Abnormal scenarii :
//  , scenarioParameterIsNull : [ [ null ], {
//      asserts : "fail"
//  } ]
//}
////  Function definition
//, function(p1){
//  var result=new Object();
//  aotest.assert(p1 === null, "fail");
//  result.x=-p1.x;
//  result.y=-p1.y;
//  return result;
//});
////  Advanced unit testing :
//function less(a, b){
//  if(!a || !b)
//      throw "Arguments must be non-null !";
//  return a - b;
//}
//function add(a, b){
//  return a + b;
//}
//function multiply(a, b){
//  var result=0;
//  for (var i=0; i<b; i++)
//      result += a;
//  return result;
//}
//
////  - Calls and errors throws monitored tests :
//
//window.aotest({
//  name : "multiply",
//  scenario1 : {
//      values : [ [ 2, 5 ], 10 ],
//      calls : {
//          multiply : 1,
//          add : 1,
//          less : 0
//      }
//  },
//  scenario2 : {
//      values : [ [ 2, null ], {
//          asserts : "fail"
//      } ],
//      calls : {
//          multiply : 1,
//          add : "throw",
//          less : 0
//      }
//  }
//}, multiply);
//window.aotest({
//  name : "less",
//  scenario1 : {
//      values : [ [ 2, 5 ], -3 ],
//      calls : {
//          multiply : 0,
//          add : 0,
//          less : 1
//      }
//  }
//}, less);
////  Another case, to illustrate that when null paramaters passed, instead of empty array, mean we don't want to execute the tested function / method as the main testing routine 
//less=window.aotest({}, less);
//multiply=window.aotest({}, multiply);
//add=window.aotest({
//  scenarAddTrack1 : {
//      values : [ null, function(){
//          return multiply(2, 10) === 20;
//      } ],
//      calls : {
//          multiply : 1,
//          add : 10,
//          less : 0
//      }
//  },
//  scenarAddTrack2 : {
//      values : [ null, function(){
//          return multiply(2, 10) === 20;
//      } ],
//      calls : {
//          multiply : 1,
//          add : 10,
//          less : 0
//      }
//  },
//  scenarAddTrack3 : [ null, function(){
//      return true;
//  } ]
//}, add);
//
//function less(a, b){
//  if(!a || !b)
//      throw "Arguments must be non-null !";
//  return a - b;
//}
//function add(a, b){
//  aotest.assert(a !== null && b !== null, "success");
//  return a + b;
//}
//function multiply(a, b){
//  var result=0;
//  for (var i=0; i<b; i++)
//      result=add(result, a);
//  return result;
//}
////  CAUTION : If no assert is declared and your test expect asserts to fail, then you test will fail.  But, if you have no assert in your test and expect asserts to success, then your test will succeed.
////  It is because you can see asserts as obstacles to your test success : So if there is no obstacle to its success, then there is will be no reason for it not to succeed ! 
////  - Calls and errors throws monitored
//tests: less=window.aotest({}, less);
//add=window.aotest({}, add);
//window.aotest({
//  // ,scenario1:{values:[[2,20],40],calls:{multiply:1,add:20,less:0}}
//  // ,scenario2:{values:[[null,2],{asserts:"fail"}],calls:{multiply:1,add:"throw",less:0}}
//  scenario3 : {
//      values : [ [ 4, 3 ], {
//          asserts : "success"
//      } ],
//      calls : {
//          multiply : 1,
//          add : 3,
//          less : 0
//      }
//  }
//}, multiply);
////  Yet another example for calls and throws monitoring :
//function less(a, b){
//  if(!a || !b)
//      throw "Arguments must be non-null !";
//  return a - b;
//}
//function add(a, b){
//  aotest.assert(a !== null && b !== null, "success");
//  return a + b;
//}
//function multiply(a, b){
//  var result=0;
//  for (var i=0; i<b; i++)
//      result=add(result, a);
//  return result;
//} //  - Calls and errors throws monitored
//tests: less=window.aotest({}, less);
//add=window.aotest({}, add);
//window.aotest({
//  // ,scenario1:{values:[[2,20],40],calls:{multiply:1,add:20,less:0}}
//  // ,scenario2:{values:[[null,2],{asserts:"fail"}],calls:{multiply:1,add:"throw",less:0}}
//  scenario3 : {
//      values : [ [ 4, 3 ], {
//          asserts : "success"
//      } ],
//      calls : {
//          multiply : 1,
//          add : 3,
//          less : 0
//      }
//  }
//}, multiply);
////  -Imbriqued tests :
//window.aotest({
//  name : "multiplyNested",
//  scenario1Nested : {
//      values : [ [ 2, 5 ], 10 ],
//      subs : {
//          scenario1Sub : [ [ 100, 5 ], 500 ],
//          scenario2Sub : {
//              values : [ [ 10, 10 ], 100 ],
//              subs : {
//                  scenario1SubSub : [ [ 1000, 1000 ], 1000000 ]
//              }
//          }
//      }
//  }
//}, multiply);
////  User UI testing :
//window.aotest({
//  name : "getAotraScreen",
//  scenario1 : [ [], function(){
//      return getAotraScreen() !== null;
//  } ]
//});
//window.aotest({
//  name : "editionProvider",
//  scenarioCreateBubble : [ [], function(){
//      var aotraScreen=getAotraScreen();
//
//      var activePlane=aotraScreen.getActivePlane();
//      aotest.assert(activePlane === null, "fail");
//
//      var oldBubblesNumber=activePlane.getBubbles().length;
//
//      aotest.assert(typeof jQuery === "undefined", "fail");
//
//      var createButtonS=jQuery(".icon.addBubbleBtn");
//      aotest.assert(empty(createButtonS.get()), "fail");
//      createButtonS.click();
//
//      aotest.assert(typeof aotraScreen.mainDiv === "undefined", "fail");
//
//      var mainDivS=jQuery(aotraScreen.mainDiv);
//      aotest.assert(empty(mainDivS.get()), "fail");
//      mainDivS.click();
//
//      var applyButtonS=jQuery("#buttonSaveAndCloseBubble");
//      aotest.assert(empty(applyButtonS.get()), "fail");
//      applyButtonS.click();
//
//      var newBubblesNumber=activePlane.getBubbles().length;
//
//      return newBubblesNumber === oldBubblesNumber + 1;
//  } ],
//  scenarioAsserts : [ [], {
//      asserts : "success"
//  } ]
//});



// AOTEST (headless) Unit Tests :

// Initialization :
if(!window.aotestMethods)
	window.aotestMethods=new Object();
if(!window.aotestAllTestsManager){
	window.aotestAllTestsManager={
			isEmpty:function(){
				let result=true;
				foreach(this,(test,testNameLocal)=>{
					result=false;
					return "break";
				},(test,testNameLocal)=>{	return !window.aotestMethods.aotratestKeyInPredefinedParameters(testNameLocal);	});
				return result;
			}
	};
	
	if(!window.aotestAllTestsManager.dummies)
		window.aotestAllTestsManager.dummies=new Object();
	window.aotestAllTestsManager.activateAsserts=false;
	window.aotestAllTestsManager.currentRunningScenario=null;
};

// Utility methods for aotest framework :
window.aotestMethods.getScenariiInTest=function(test){
	let results={};
	foreach(test,(scenario,scenarioName)=>{
		results[scenarioName]=scenario;
	},(scenario,scenarioName)=>{ return window.aotestMethods.isScenarioName(scenarioName); });
	return results;
};


/*private*/window.aotestMethods.doForAllTestsByType=function(allTestsByType, doOnIteration, filter=null, mustTerminate=null){
	
	const loopResultI=foreach(allTestsByType,(testsByFunction,methodName)=>{
		const loopResultJ=foreach(testsByFunction,(test,executionCoupleName)=>{ // (execution couple IS test)
		
		const scenarii=aotestMethods.getScenariiInTest(test);
	  	const loopResultK=foreach(scenarii,(scenario, scenarioName)=>{
		
				if(doOnIteration)		doOnIteration(scenario, test);
  			if(mustTerminate && mustTerminate(scenario))
  				return scenario;
  			
		  },(scenario, scenarioName)=>!filter || filter(scenario));
		
		  if(loopResultK)
		  	return loopResultK;
		  
  	});
  	
		if(loopResultJ)
			return loopResultJ;

	});
		
	return loopResultI;
}

/*public*/window.aotestMethods.iterateOverScenarii=function(allTests, doOnIteration, testsType=null, filter=null, mustTerminate=null){
	
	if(testsType){
		if(testsType==="*"){
				return foreach(allTests,(allTestsByType,t)=>{
				const loopResult=window.aotestMethods.doForAllTestsByType(allTestsByType, doOnIteration, filter, mustTerminate);
				if(loopResult)
					return loopResult;
			});
		}
		
		const allTestsByType=allTests[testsType];
		return window.aotestMethods.doForAllTestsByType(allTestsByType, doOnIteration, filter, mustTerminate);
	}

	const allTestsByType=allTests;
	return window.aotestMethods.doForAllTestsByType(allTestsByType, doOnIteration, filter, mustTerminate);
};

/*public*/window.aotestMethods.iterateOverValuesOnClonedObject=function(scenarioParam, doOnIterationForValue, visited=[], valuePath="", isValueFunction=null){

	// CAUTION : We ABSOLUTELY need to iterate over a deep copy, or else it will create weird incomprehensible bugs 
	// if we do another recursive browsing on the initial object !!!
	const scenario=parseJSON(stringifyObject(scenarioParam));


	const basePath=valuePath;
	foreach(scenario,(item, itemNameOrIndex)=>{
		
		if(contains(visited, item))	return "continue";
		visited.push(item);
		
		valuePath=basePath+"."+itemNameOrIndex;
		
		if(isValueFunction){
			if(isValueFunction(item)){
				scenario[itemNameOrIndex]=doOnIterationForValue(item, itemNameOrIndex, valuePath);
			}else{
				if(isArray(item)){
					window.aotestMethods.iterateOverValuesOnClonedObject(item, doOnIterationForValue, visited, valuePath, isValueFunction);
				}else if(isObject(item)){
					window.aotestMethods.iterateOverValuesOnClonedObject(item, doOnIterationForValue, visited, valuePath, isValueFunction);
				}else{
					scenario[itemNameOrIndex]=item;
				}
			}
		}else{
			if(isArray(item)){
				window.aotestMethods.iterateOverValuesOnClonedObject(item, doOnIterationForValue, visited, valuePath, isValueFunction);
			}else if(isObject(item)){
				window.aotestMethods.iterateOverValuesOnClonedObject(item, doOnIterationForValue, visited, valuePath, isValueFunction);
			}else{
				scenario[itemNameOrIndex]=doOnIterationForValue(item, itemNameOrIndex, valuePath);
			}
		}

	});

};
	





window.aotestMethods.isScenarioName=function(scenarioName){
	return contains(scenarioName,"_scenario");
};

// (a little utility function for the unitary tests framework aotest :)
window.aotestMethods.aotratestKeyInPredefinedParameters=function(key){
	return contains(
	         ["activateAsserts","currentRunningScenario","name","dummies","prerequisite","clean","methodName",
	         "mainInstance","nature","isEmpty"],
	         key);
};
window.aotestMethods.registerScenarioInfos=function(scenariiListParam, name, scenarioInfos){
	// Scenario attriburtes :
	scenarioInfos["name"]=name;
	scenariiListParam[name]=scenarioInfos;
	// Sub-scenarii registering :
	var subScenariiList=null;
	if(!isArray(scenarioInfos))
		subScenariiList=scenarioInfos["subs"];
	if(subScenariiList && typeof subScenariiList === "object"){
		
		foreach(subScenariiList, (subScenarioInfos,key)=>{
			subScenarioInfos["parentScenarioName"]=name;
			window.aotestMethods.registerScenarioInfos(subScenariiList, key, subScenarioInfos);
		});
		
	}
};

// UNUSED AND BOGUS :
// TODO : FIXME : On today, tracking is deactivated for aotest, because random unresolved bugs occur :
//
///*private*/window.aotestMethods.trackFunction=function(func, methodArgs){
//
//	// DBG
//	console.log("!!! func:",func);
//	console.log("!! func.prototype:",func.prototype);
//	console.log("!! func.prototype.constructor:",func.prototype.constructor);
//	
// DOES NOT WORK : func.prototype IS SOMETIMES UNDEFINED !!
//	
//	// TODO : FIXME : Not sure if this is the right objet to call the function upon....
//	var callerObject=func.prototype.constructor.caller;
//	
//	
//	var currentRunningScenario=window.aotestAllTestsManager.currentRunningScenario;
//	// If we are not in a running test scenario execution context, then we skip
//	// tracking :
//	if(!currentRunningScenario)
//		return func.apply(callerObject, argsAsArray);
//
//	var functionName=getFunctionName(func);
//	// UNUSED attribute :
//	currentRunningScenario.functionName=functionName;
//	var functionResult=null;
//	if(!currentRunningScenario.callsCounts)
//		currentRunningScenario.callsCounts=new Object();
//	var callsCounts=currentRunningScenario.callsCounts[functionName];
//	if(!callsCounts)
//		currentRunningScenario.callsCounts[functionName]=0;
//	if(!currentRunningScenario.throwsCounts)
//		currentRunningScenario.throwsCounts=new Object();
//	var throwsCounts=currentRunningScenario.throwsCounts[functionName];
//	if(!throwsCounts)
//		currentRunningScenario.throwsCounts[functionName]=0;
//	currentRunningScenario.callsCounts[functionName]++;
//	try {
//		functionResult=func.apply(callerObject, argsAsArray);
//	} catch (e){
//		currentRunningScenario.throwsCounts[functionName]++;
//		throw e;
//	}
//
//	return functionResult;
//};

// UNUSED AND BOGUS :
// TODO : FIXME : On today, tracking is deactivated for aotest, because random unresolved bugs occur :
//
//window.aotestMethods.getTrackedFunctionDefinition=function(/* NULLABLE */functionDefinition=null){
//	if(!functionDefinition)
//		return null;
//	
//	var trackedFunctionDefinition=function(){
//		// IMPORTANT : «arguments» here is a system variable containing all
//		// arguments for this function call...
//		return window.aotestMethods.trackFunction(functionDefinition, arguments);
//	};
//	
//	return trackedFunctionDefinition;
//};

// Main method to initialize (ie. register) a test on a function/method :
window.aotest=function(parameters,functionDefinition=null,ignoreTest=false,CLASSNAME_ATTR_NAME=DEFAULT_CLASSNAME_ATTR_NAME){
	// (If we skip test, then there is no need to add tracking on function /
	// method :)
	if(ignoreTest)
		return functionDefinition;

	const DEBUG_TRACE=true;

	// Parameters :
	// Tested function/method name :
	const methodNameParam=parameters.methodName;
	
	
	if(!functionDefinition){
		let mainInstance=parameters.mainInstance;
		if(!mainInstance)		functionDefinition=window[methodNameParam];
		else{
			
//		// DBG
//		console.log("parameters:",parameters);
			
			functionDefinition=mainInstance[methodNameParam];

			if(!functionDefinition && mainInstance[CLASSNAME_ATTR_NAME]){
				let instanceTypeName=mainInstance[CLASSNAME_ATTR_NAME];

				// DBG
				console.log("ERROR : Could not find method «"+methodNameParam+"» on instance of the following 'alleged type' : «"+instanceTypeName+"». Test registration will be skipped.");

//				// DBG
//				console.log("+++instanceTypeName:",instanceTypeName);
				
			}

//			// DBG
//			console.log("+++mainInstance:",mainInstance);
//			console.log("+++functionDefinition:",functionDefinition);

			
		}
	}
	if(!functionDefinition){
		// TRACE
		console.log("ERROR : No function definition found for function named «"+nonull(methodNameParam, "<unknown name function>")+"», aborting test registration.");
		return null;
	}
	let methodName=getFunctionName(functionDefinition);

	// Current method context initialization :
	// Test parameters population :
	var currentTest=new Object();

	var nameParam=parameters["name"] ? parameters["name"] : (methodName ? methodName : ("noNameTest_" + getUniqueIdWithDate()));
	
	// TRACE
	// if(DEBUG_TRACE) log("DEBUG : Registering test «"+nameParam+"»");
	// Required name :
	currentTest.name=nameParam;

	// (optional) Func (ie. function name) :
	let func=parameters["methodName"];
	if(!!func /*(forced to boolean)*/){
		currentTest["methodName"]=func;
	}
	
	// We add to the global context :
	window.aotestAllTestsManager[currentTest.name]=currentTest;
	// Dummies :
	var dummiesParam=parameters["dummies"];
	var dummiesLocal=new Object();
	if(typeof (dummiesParam) === "function"){
		dummiesLocal=dummiesParam(new Object());
	} else if(typeof (dummiesParam) === "object"){
		dummiesLocal=dummiesParam;
		// }else{
		// // TRACE
		// if(DEBUG_TRACE){
		// var displayTestName="Unit test for function «"+nameParam+"»";
		// log("DEBUG : WARN : No dummies for test «"+displayTestName+"»");
		// }
	}
	// We add to the global context :
	foreach(dummiesLocal, (dummy,key)=>{
		window.aotestAllTestsManager.dummies[key]=dummy;
	});
	
	// (optional) Prerequisite function :
	var prerequisite=parameters["prerequisite"];
	if(prerequisite && typeof prerequisite === "function"){
		currentTest["prerequisite"]=prerequisite;
	}
	// (optional) Clean function :
	var clean=parameters["clean"];
	if(clean && typeof clean === "function"){
		currentTest["clean"]=clean;
	}
	
	// (optional) main Instance present or not :
	var mainInstance=parameters["mainInstance"];
	if(!!mainInstance /*(forced to boolean)*/){
		currentTest["mainInstance"]=mainInstance;
	}

	// Scenarii (last parameters !) :
	var scenariiListLocal={};
	var scenariiCount=0;
	
	foreach(parameters, (scenarioInfos,key)=>{
		
		if(window.aotestMethods.aotratestKeyInPredefinedParameters(key))
		return "continue";

		// if(DEBUG_TRACE) log("DEBUG : Registering test «"+nameParam+"» : scenario
		// «"+key+"»");
		window.aotestMethods.registerScenarioInfos(scenariiListLocal, key, scenarioInfos);
		scenariiCount++;
	});
	
	if(DEBUG_TRACE)
		console.log(("DEBUG : (" + scenariiCount + ") test scenarii registered for test «" + nameParam + "»."));

	currentTest.scenariiList=scenariiListLocal;
	
	// TODO : FIXME : On today, tracking is deactivated for aotest, because random unresolved bugs occur :
	// We add the method to the global tracking context :
//	functionDefinition=window.aotestMethods.getTrackedFunctionDefinition(functionDefinition);
	
	window.aotestAllTestsManager[currentTest.name].functionDefinition=functionDefinition;
	return functionDefinition;
};
// Tests running :
// CAUTION : If no assert is declared and your test expect asserts to fail, then
// your test will fail.
// But, if you have no assert in your test and expect asserts to success, then
// your test will succeed.
// It is because you can see asserts as obstacles to your test success :
// So if there is no obstacle to its success, then there is will be no reason
// for it not to succeed !
aotest.assert=function(booleanCondition, behavior){
	if(!window.aotestAllTestsManager.activateAsserts)
		return;
	if(booleanCondition && behavior === "fail")
		throw "(Assert failed : evaluated condition succeeded instead of failed.)";
	if(!booleanCondition && behavior === "success")
		throw "(Assert failed : evaluated condition failed instead of succeeded.)";
};

/* STANDARD AOTEST RESULT :
 * This method returns a tests results report object of the following structure :
 * 
  [name]:
			-global 
				-type:string="testsSuiteResult"
				-childrenFailed : number
				-childrenRunned: number
				-isFailed: boolean
				-message : string
			-children
				[name] :				 
					...(IDEM)... with : -type="testResult"
					-children
						[name] :				 
						 ...(IDEM)... with : -type="scenarioResult"
						 		(-children    // (sub-scenarii :)
									[name] :				 
									 ...(IDEM)... with : -type="scenarioResult" )*
  
 * 
 * */

aotest.run=function(testName=null,scenarioName=null){
	// CONSTANTS :
	var FAIL_IF_CHILDREN_FAILED=true;
	
	// Result report variable:
	var testsResultsObj={};
	
	// DBG
	console.log("		window.aotestAllTestsManager",window.aotestAllTestsManager);
	
	
	if(window.aotestAllTestsManager.isEmpty()){
		// TRACE
		console.log("WARN : No registered javascript tests to run. Aborting.");
		
  	const emptyTestsSuiteResult={
			global:{
				type:"testsSuiteResult", childrenFailed:0, childrenRunned:0,
				isFailed:false,
				message:"No registered javascript tests to run.",
				expected:null,
				actual:null,
			},
			children:{},
		};
		testsResultsObj["emptyJavascriptTestsSuite"]=emptyTestsSuiteResult;
		
		return testsResultsObj;
	}
	
	aotest.isRunning=true;
	
	// Function 1, case run one test>one scenario
	const runOneTestOneScenario=function(chosenTestParam, chosenScenarioParam){
	
	
		let scenarioResultObjLocal={
				global:{
					type:"scenarioResult",
					childrenFailed:0,
					childrenRunned:0,
					isFailed:false,
					message:""
				},
				children:{}
		};

		
		var functionParameters=null;
		// Which scenario is currently running :
		window.aotestAllTestsManager.currentRunningScenario=chosenScenarioParam;
		
		// Functions calls counting :
		var isChosenScenarioParamArray=isArray(chosenScenarioParam);

		if(!isChosenScenarioParamArray) functionParameters=chosenScenarioParam.methodArgs;
		else 														functionParameters=chosenScenarioParam[0];
		
		// CAUTIOM : At this point, functionParameters is an associative array (parameterName : parameterValue)
		
		let mainInstance=nonull(chosenTestParam.mainInstance,null); // To ensure we have null and not undefined

		// CAUTION : functionParameters can be null, meaning we want no execution of
		// the function / method !
		// (a different case if we want to execute it with no parameters !)
		var functionParametersPopulated=null; // ...if this array is null, then we don't want to execute the function !!
		if(functionParameters){
			functionParametersPopulated=new Array();
			
			// We populate dummies here :
			foreach(functionParameters,(fp)=>{
				if(fp){
					if(fp["dummy"]){
						var dummyName=fp["dummy"];
						var dummy=window.aotestAllTestsManager.dummies[dummyName];
						functionParametersPopulated.push(dummy);
					} else {
						functionParametersPopulated.push(fp);
					}
				} else {
					functionParametersPopulated.push(null);
				}
			});
			
			
		}
		
		
		
		let currentExecutedTest=window.aotestAllTestsManager[chosenTestParam.name];
		var functionDefinition=currentExecutedTest.functionDefinition;

		var expectedResult=null;
		if(!isChosenScenarioParamArray)	expectedResult=chosenScenarioParam.result;
		else														expectedResult=chosenScenarioParam[1];
		
		// We copy as flat structures and store all the possible function context objects before its execution :
		currentExecutedTest.oldResultFlat=(expectedResult!=null?getAsFlatStructure(expectedResult,true):null);

		// NEW
		// (special, scenario instance has priority over the main, test instance, for the function application :)
		let actualInstance=nonull(chosenScenarioParam.specialInstance,mainInstance);
		
		
		// DBG
		console.log("!!! functionDefinition:",functionDefinition);
		console.log("!!! functionParametersPopulated:",functionParametersPopulated);
		
		
		// CAUTION : functionParameters can be null, meaning we want no execution of
		// the function / method !
		// (a different case if we want to execute it with no parameters ! in that case we will have an empty array of arguments)
		if(functionDefinition && functionParametersPopulated){
			
			
			// DBG
			console.log("²²²²²² expectedResult",expectedResult);

			
			var functionResult=null;
			if(expectedResult){

				
				if(typeof (expectedResult) === "function"){

					// If we test according to result check only : (Here we only verify the existence of a result or not)
					window.aotestAllTestsManager.activateAsserts=false;
					functionResult=functionDefinition.apply(actualInstance, functionParametersPopulated);
					if(!expectedResult(functionResult)){

						// TRACE
						scenarioResultObjLocal.global.message="FAIL: No result detected after function execution but one expected.";
						scenarioResultObjLocal.global.isFailed=true;
					
					}

				} else if(expectedResult.asserts){
					// If we test according to asserts only :
					window.aotestAllTestsManager.activateAsserts=true;
					try {
						functionResult=functionDefinition.apply(actualInstance, functionParametersPopulated);
					} catch (e){
						
						// TRACE
						scenarioResultObjLocal.global.message="FAIL: Scenario interrupted ! Assert result : «" + e + "».";
						scenarioResultObjLocal.global.isFailed=true;
						
					}
					// If we expect that asserts will fail (at least one of them), then
					// the test is passed though !
					// Normal logic if we expect, at the contrary, all asserts to succeed
					// :
					// (ie. «at least one assert must fail» or «all asserts must succeed»)
					// expected result.
					if(expectedResult.asserts === "fail"){
						
						// TRACE
						scenarioResultObjLocal.global.message="FAIL: Assert scenario was in «fail» expectation logic.";
						scenarioResultObjLocal.global.isFailed=!scenarioResultObjLocal.global.isFailed;
						
					}
				} else {
					// If we test according to result check only :
					window.aotestAllTestsManager.activateAsserts=false;
					functionResult=functionDefinition.apply(actualInstance, functionParametersPopulated);

					
					if(!expectedResult.notResult){
						if(expectedResult !== functionResult){
							
							// TRACE
							scenarioResultObjLocal.global.message="FAIL: Expected result does not match function execution result.";
							scenarioResultObjLocal.global.isFailed=true;
							
						}
					}else{
						if(expectedResult.notResult === functionResult){

							// TRACE
							scenarioResultObjLocal.global.message="FAIL: Expected result matches unwanted function execution result.";
							scenarioResultObjLocal.global.isFailed=true;

						}
					}
				}
				
				
			} else {
				
				// If we test according to asserts only, and not caring about the
				// function returned result :
				window.aotestAllTestsManager.activateAsserts=true;
				try {
					functionResult=functionDefinition.apply(actualInstance, functionParametersPopulated);
				} catch (e){
					
					// TRACE
					scenarioResultObjLocal.global.message="FAIL: Scenario interrupted ! Assert result : «" + e + "».";
					scenarioResultObjLocal.global.isFailed=true;
					
				}
			}

			
			

			// *****************************************
			// We check function/methods calls here :
			// (overrides all other previous test result calculations)

			// TRACE
			var chosenScenarioParamName=null;
			if(!isChosenScenarioParamArray)	chosenScenarioParamName=chosenScenarioParam.name;
			
			
			// UNUSED :
			// TODO : FIXME : On today, tracking is deactivated for aotest, because random unresolved bugs occur :
//			var callsJSONConfig=null;
//			if(!isChosenScenarioParamArray){
//				callsJSONConfig=chosenScenarioParam.calls;
//			}
//			
//			
//			
//			if( // (to avoid calculating more if test has already failed :)
//					!scenarioResultObjLocal.global.isFailed && callsJSONConfig){
//				var currentRunningScenarioLocal=window.aotestAllTestsManager.currentRunningScenario;
//				
//				// TRACE
//				console.log("LOG: Tracking results for scenario «" + chosenScenarioParam.name + "»");
//
//
//				foreach(currentRunningScenarioLocal.callsCounts, (calculatedCount,key)=>{
//					
//					// console.log("callsCounts «"+key+"»: "+calculatedCount+" ; EXPECTED :
//					// "+callsJSONConfig[key]);
//					if(callsJSONConfig[key] && isNumber(callsJSONConfig[key])){
//						if(callsJSONConfig[key] != calculatedCount){
//
//							// TRACE
//							scenarioResultObjLocal.global.message="FAIL: Scenario called function the wrong amount of times.";
//							scenarioResultObjLocal.global.isFailed=true;
//
//						}
//					}
//					// We reset count :
//					currentRunningScenarioLocal.callsCounts[key]=0;
//				});
//
//				foreach(currentRunningScenarioLocal.throwsCounts, (calculatedCount, key)=>{
//					
//					// console.log("throwsCounts «"+key+"»: "+calculatedCount+" ; EXPECTED :
//					// "+callsJSONConfig[key]);
//					if(callsJSONConfig[key] && callsJSONConfig[key] === "throw"){
//						if(calculatedCount === 0){
//
//							// TRACE
//							scenarioResultObjLocal.global.message="FAIL: Scenario expected an exception throw on function the wrong and recorded none.";
//							scenarioResultObjLocal.global.isFailed=true;
//							
//						}
//					}
//					// We reset count :
//					currentRunningScenarioLocal.throwsCounts[key]=0;
//				});
//				
//			}
			
		} else { // IF NOT (functionDefinition && functionParametersPopulated)
			// If no function is the actual object of the test, but if all is in the expected
			// result function (example : for UI tests !) :
			// (reminder : if functionParametersPopulated array is NULL, then we don't
			// want to execute the function !!)
			
			
			
			if(expectedResult){
				if(typeof (expectedResult) === "function"){
					// In this special case, asserts are activated within this function,
					// expectedResult() (UI tests):
					window.aotestAllTestsManager.activateAsserts=true;
					// But in this mode, we don't have the choice to set the asserts logic
					// (i.e. «at least one assert must fail» or «all asserts must
					// succeed») expected result.
					try {

						// If we test according to result check only : (Here we only verify the existence of a result or not)
						if(!expectedResult()){


							// TRACE
							scenarioResultObjLocal.global.message="FAIL: No result detected after function execution but one expected.";
							scenarioResultObjLocal.global.isFailed=true;
							
							
						}
					} catch (e){
						
						
						// TRACE
						scenarioResultObjLocal.global.message="FAIL: Scenario interrupted ! Assert result : «" + e + "».";
						scenarioResultObjLocal.global.isFailed=true;
						
						
					}
				}
			}
			
			
			// There can be no other case if we are in UI tests case.
		}
		
		
		
		// States comparison test handling :
		// (overrides all other previous test result calculations)
		if( // (to avoid calculating more if test has already failed :)
				!scenarioResultObjLocal.global.isFailed 
				&& !isChosenScenarioParamArray 
				&& chosenScenarioParam.after){

			
			let beforeResultFlat=currentExecutedTest.oldResultFlat; 		// expected result
			
			
			// At this point, we already have applied the method to the input object, in order to compare it with the output object, if necessary.
			let actualInstanceFlat=(actualInstance!=null?getAsFlatStructure(actualInstance,true):null);	// actual instance (after execution)
			let actualArgsFlat=(functionParametersPopulated!=null?getAsFlatStructure(functionParametersPopulated,true):null);	// actual arguments (after execution)
			

			let afterInstanceFlat=null;
			let afterArgsFlat=null;
			let afterResultFlat=null;
			if(chosenScenarioParam.after.alreadyFlat){
				afterInstanceFlat=chosenScenarioParam.after.afterInstance; 	// EXPECTED instance
				afterArgsFlat=chosenScenarioParam.after.methodArgs; 				// EXPECTED arguments
				afterResultFlat=functionResult;															// actual result
			}else{
				afterInstanceFlat=getAsFlatStructure(chosenScenarioParam.after.afterInstance,true); // EXPECTED instance
				afterArgsFlat=getAsFlatStructure(chosenScenarioParam.after.methodArgs,true);				// EXPECTED arguments
				afterResultFlat=getAsFlatStructure(functionResult,true);														// actual result (after execution)
			}

			let areEquivalentInstances=(blank(actualInstanceFlat) && blank(afterInstanceFlat)) || areEquivalentFlatMaps(actualInstanceFlat,afterInstanceFlat);
			
//			// DBG
//			console.log("---------------------------------------------");
//			console.log("		actualInstanceFlat:",actualInstanceFlat);
//			console.log("		afterInstanceFlat:",afterInstanceFlat);
//			console.log("-----");
//			console.log("		beforeResultFlat:",beforeResultFlat);
//			console.log("		afterResultFlat:",afterResultFlat);
//			console.log("-----");
//			console.log("		actualArgsFlat:",actualArgsFlat);
//			console.log("		afterArgsFlat:",afterArgsFlat);
//			console.log("---------------------------------------------");
			
			let areEquivalentArgs=(blank(actualArgsFlat) && blank(afterArgsFlat)) 
														|| areEquivalentFlatMaps(actualArgsFlat,afterArgsFlat);
			let areEquivalentResults=!!(blank(beforeResultFlat) && blank(afterResultFlat)
														|| areEquivalentFlatMaps(beforeResultFlat,afterResultFlat) ) /*(forced to boolean)*/;
			
			if(  !areEquivalentInstances
				|| !areEquivalentArgs
				|| !areEquivalentResults){
				
				scenarioResultObjLocal.global.message=
					"FAIL: Scenario failed !"
					+(areEquivalentInstances?"":"(instances are not equivalent) ")
					+(areEquivalentArgs?"":"(functions arguments are not equivalent) ")
					+(areEquivalentResults?"":"(results are not equivalent) ")
					;
				scenarioResultObjLocal.global.isFailed=true;
				
			}
			
			
			
			// DBG
			console.log("³³³³³³ areEquivalentInstances",areEquivalentInstances);
			console.log("³³³³³³ areEquivalentArgs",areEquivalentArgs);
			console.log("³³³³³³ areEquivalentResults",areEquivalentResults);
			
			
		}
		
		
		// We check sub-scenarii here :
		var subScenariiList=null;
		if(!isChosenScenarioParamArray)
			subScenariiList=chosenScenarioParam.subs;
		
		let oneChildFailed=false;
		if(subScenariiList && typeof subScenariiList === "object"){
			foreach(subScenariiList, (subScenario,subScenarioNameLocal)=>{
				
				// DBG
				console.log("WE RUN SUB SCENARIO !!!");
				
				// Recursive call :
				let childrenResultObj=runOneTestOneScenario(chosenTestParam, subScenario);
				if(childrenResultObj.global.isFailed){
					oneChildFailed=true;
					scenarioResultObjLocal.global.childrenFailed++;
				}
				scenarioResultObjLocal.global.childrenRunned++;
				// We add the sub-scenario result to the children array:
				scenarioResultObjLocal.children[subScenarioNameLocal]=childrenResultObj;
				
			});
			
		}
		if(FAIL_IF_CHILDREN_FAILED){
			if(oneChildFailed){
				
				// TRACE
				scenarioResultObjLocal.global.message="FAIL: This scenario failed only because some of its children failed.";
				scenarioResultObjLocal.global.isFailed=true;
			
			}
		}
		
		// DBG
		console.log("&&&&&& chosenScenarioParamName:",chosenScenarioParamName);
		
		
		if(!scenarioResultObjLocal.global.isFailed){
			// TRACE
			scenarioResultObjLocal.global.message="SUCCESS: Scenario «"+chosenScenarioParamName+"» passed successfully.";
			scenarioResultObjLocal.global.isFailed=false;
		}
		
		
		return scenarioResultObjLocal;
	};
	

	// Function 2, case run one test>all scenarii
	const runOneTestAllScenarii=function(chosenTestParam){
		
		
		// We init a new empty test result object :
		let testResultObjLocal={
				global:{
					type:"testResult",
					childrenFailed:0,
					childrenRunned:0,
					isFailed:false,
					message:""
				},
				children:{}
		};
		
		
		if(chosenTestParam["prerequisite"]){
			// TRACE
			console.log("LOG: Running prerequisite for «" + chosenTestParam.name + "»...");
			// Prerequisite function running :
			chosenTestParam.prerequisite();
		}
		
		// TRACE
		console.log("LOG: Running all test «" + chosenTestParam.name + "» scenarii...");
		
		
		const scenariiListLocal=chosenTestParam.scenariiList;
		
		// // DBG
		// console.log("chosenTestParam",chosenTestParam);
		
		foreach(aotestMethods.getScenariiInTest(scenariiListLocal), (scenario,scenarioNameLocal)=>{
			
			// DBG
			console.log("WE RUN ONE SCENARIO !!!");
			
			let scenarioResultObjLocal=runOneTestOneScenario(chosenTestParam, scenariiListLocal[scenarioNameLocal]);
			if(scenarioResultObjLocal.global.isFailed){
				
				// TRACE
				testResultObjLocal.global.message="FAIL: This test failed only because some of its children failed.";
				testResultObjLocal.global.isFailed=true;
				testResultObjLocal.global.childrenFailed++;
				
			}
			testResultObjLocal.global.childrenRunned++;
			// We add the scenario result to the children array:
			testResultObjLocal.children[scenarioNameLocal]=scenarioResultObjLocal;

			
		});
		
		if(chosenTestParam.clean){
			// TRACE
			console.log("LOG: Running clean for «" + chosenTestParam.name + "»...");
			// Clean function running :
			chosenTestParam.clean();
		}
		
		return testResultObjLocal;
	};
	
	
	// Function 3, case run all tests>all scenarii
	const runAllTestsAllScenarii=function(){
		
		// TRACE
		console.log("LOG: Running all tests...");
		
		
		// We init a new empty all tests result object :
		let allTestsResultObjLocal={
				global:{
					type:"testsSuiteResult",
					childrenFailed:0,
					childrenRunned:0,
					isFailed:false,
					message:""
				},
				children:{}
		};
		
		
		
		var allTestsLocal=window.aotestAllTestsManager;
		foreach(allTestsLocal,(test,testNameLocal)=>{

			let testResultObj=runOneTestAllScenarii(allTestsLocal[testNameLocal]);
			if(testResultObj.global.isFailed){
				
				// TRACE
				allTestsResultObjLocal.global.message="FAIL: This all-tests run suite failed only because some of its children failed.";
				allTestsResultObjLocal.global.isFailed=true;
				allTestsResultObjLocal.global.childrenFailed++;
				
			}
			allTestsResultObjLocal.global.childrenRunned++;
			// We add the scenario result to the children array:
			allTestsResultObjLocal.children[testNameLocal]=testResultObj;

			
		},(test,testNameLocal)=>{	return !window.aotestMethods.aotratestKeyInPredefinedParameters(testNameLocal);	});
		
		return allTestsResultObjLocal;
	};
	
	
	// ------
	// «MAIN»
	// ------

	
	// We init a new empty all tests result object :
	let allTestsResultObj={
			global:{
				type:"testsSuiteResult",
				childrenFailed:0,
				childrenRunned:0,
				isFailed:false,
				message:""
			},
			children:{}
	};
	
	
	if(testName && window.aotestAllTestsManager[testName]){
		var chosenTest=window.aotestAllTestsManager[testName];
		
		if(scenarioName && chosenTest.scenariiList[scenarioName]){
			var chosenScenario=chosenTest.scenariiList[scenarioName];
			
			// DBG
			console.log("Case run one test -> one scenario");
			
			// We create the single child of this tests suite in the children array:
			allTestsResultObj.children[testName]={
					global:{
						type:"testResult",
						childrenFailed:0,
						childrenRunned:0,
						isFailed:false,
						message:""
					},
					children:{}
			};

			let testResultObj=allTestsResultObj.children[testName];
			
			// Case run one test -> one scenario (1->1)
			let scenarioResultObj=runOneTestOneScenario(chosenTest, chosenScenario);
			if(scenarioResultObj.global.isFailed){

				// TRACE
				testResultObj.global.message="FAIL: This all-tests run suite failed only because some of its children (scenario) failed.";
				testResultObj.global.isFailed=true;
				testResultObj.global.childrenFailed++;
				
			}
			testResultObj.global.childrenRunned++;
			// We add the scenario result to the children array of the only child of this tests suite:
			testResultObj.children[scenarioName]=scenarioResultObj;
			

		} else {
			
			// DBG
			console.log("Case run one test -> all scenarii");
			
			// Case run one test -> all scenarii (1->*)
			let testResultObj=runOneTestAllScenarii(chosenTest);
			if(testResultObj.global.isFailed){
				
				// TRACE
				allTestsResultObj.global.message="FAIL: This all-tests run suite failed only because some of its children (test) failed.";
				allTestsResultObj.global.isFailed=true;
				allTestsResultObj.global.childrenFailed++;
				
			}
			allTestsResultObj.global.childrenRunned++;
			// We add the test result to the children array:
			allTestsResultObj.children[testName]=testResultObj;

		}
	} else {

		// DBG
		console.log("Case run all tests -> all scenarii");
		
		// Case run all tests -> all scenarii (*->*)
		let testsSuiteResultObj=runAllTestsAllScenarii();
		// The test suite is the executed test suite :
		allTestsResultObj=testsSuiteResultObj;
		
	}
	
	
	var buildHRReport=function(resultObj){

		let str="";
		
		let resultsFailed=getCumulatedInt(resultObj,"childrenFailed");
		let resultsRunned=getCumulatedInt(resultObj,"childrenRunned");
		
		
		// TRACE
		str+="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
		str+=" 									AOTEST REPORT								 \n";
		str+="REPORT: "+resultsFailed + "/" + resultsRunned + " scenarii failed.\n";
		str+="REPORT: "+(resultsRunned - resultsFailed) + "/" + resultsRunned + " scenarii succeeded.\n";
		str+="REPORT: "+resultsRunned + " scenarii runned.\n";
		str+="@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n";
		
		if(0<resultsRunned){
			if(!resultObj.global.isFailed){
				// TRACE
				str+="LOG: RESULT :>>>>>> All tests SUCCEEDED ! :D\n";
			} else {
				// TRACE
				str+="LOG: RESULT :>>>>>> Some tests FAILED... :(\n";
			}
		} else {
			// TRACE
			str+="LOG: RESULT :>>>>>> No tests runned... :|\n";
		}
		
		return str;
	};
	

	var getCumulatedInt=function(resultObj, attrName){

		let result=0;
		
		result+=resultObj.global[attrName];
		
		foreach(resultObj.children,(child)=>{
			result+=getCumulatedInt(child, attrName);
		});
		
		return result;
	};

	
	
	
	// TRACE
	console.log(	buildHRReport(allTestsResultObj));
	
	
	// DBG
	console.log("TOTAL RESULT :",allTestsResultObj);

	
	aotest.isRunning=false;
	
	return allTestsResultObj;
};



aotest.profile=function(rootObject,methodName,visited=[]){
	
	
//if(!rootObject || !isObject(rootObject) || contains(visited,rootObject))	return;
	if(			!rootObject || isPrimitive(rootObject)	|| isFunction(rootObject) 
			||  contains(visited,rootObject))	return;

	const THRESHOLD_MILLIS=10;

	visited.push(rootObject);
	
	let method=rootObject[methodName];
	if(method && isFunction(method)){
		rootObject[methodName]=function(){
			
			let time=getNow();
			
			method.apply(rootObject, arguments);
			
			let deltaMillis=(getNow()-time);
			
			// TRACE
			if(THRESHOLD_MILLIS<=deltaMillis)	lognow("Duration ["+deltaMillis+"ms] of «"+methodName+"» on object:"+getClassName(rootObject),rootObject);
		};
	}
	
	foreach(rootObject,(attr,attrName)=>{
		
		// DBG
		if(attrName==="currentContainer")	lognow("currentContainer:",attr);
		
		aotest.profile(attr,methodName,visited);
		
	});
	
	
};



//================================================================
//================= Tjread control utility methods =================
//================================================================


window.sleep = (millis) => new Promise( resolve => setTimeout(resolve, millis) );



//================================================================
//================= Cryptography utility methods =================
//================================================================


// NOT AOTESTABLE !
//TODO : develop :
//getHashedString=monitorProgression(100000,
window.getHashedString=function(str,algorithmName="SHA-256",/*OPTIONAL*/isHeavyTreatment=false){
	
	//CAUTION : YOU MUST *NOT* CHANGE THIS VALUE, UNLESS THE HASH HEAVY TREATMENT VALUE IN YOUR AOTRA SERVER-SIDE CODEIS THE SAME !
	const HEAVY_TREATMENT_LOOP_COUNT=100000;
	
	if(isHeavyTreatment){

		// If needed, we make sure the hashing will take a long time, so that
		// if the private salt key is leaked, it will be fairly difficult to crack
		// password write protection and content encryption :
		
		let result=str;
		for(var i=0;i<HEAVY_TREATMENT_LOOP_COUNT;i++){
			result=getHashedString(result,algorithmName,false);
		}

		
		return result;
		
	}else{
	
		
		// SHA-256 hash (sucks less) :
		// DEPENDENCY : SJCL JS libray (Stanford Javascript Crypto Library)
		if(algorithmName=="SHA-256" && typeof(sjcl)!="undefined"){
			let result=sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(str));
			
			// TODO : develop :
//			hash.progressionMonitor.progress(1);
	
			
			return result;
		}
		
		// if not found : MD5 hash (sucks) :
		//TRACE
		log("WARN : Could not find SHA hashing, using MD5. This sucks, and this very likely will prevent your aotra client to be able to write data to aotra server.");
		
		return hex_md5(str);
	}
}
//);

function encrypt(strToEncrypt,key,/*OPTIONAL*/mode){
	if(!mode || typeof(sjcl)=="undefined")	mode="xor";
	mode=mode.trim();

	if(contains(mode,"aes") && contains(mode,"xor")){

		// XOR :
		var xorStr=btoa(encodeXORNoPattern(strToEncrypt,key));	
		
		var aesStr=
			// AES
			sjcl.encrypt(key,xorStr);

		return aesStr;
	}
	
	//	DEPENDENCY : SJCL JS libray (Stanford Javascript Crypto Library)
	if(mode==="aes")
		return sjcl.encrypt(key,strToEncrypt);
	return btoa(encodeXORNoPattern(strToEncrypt,key));

}

function decrypt(strToDecrypt,key,/*OPTIONAL*/mode){
	if(!mode || typeof(sjcl)=="undefined")	mode="xor";
	mode=mode.trim();

	if(contains(mode,"aes") && contains(mode,"xor")){
		
		// AES
		var aesStr=sjcl.decrypt(key,strToDecrypt);
		
		var xorStr=
			// XOR :
			encodeXORNoPattern(atob(aesStr),key);

		return xorStr;
		
	}
	
	//	DEPENDENCY : SJCL JS libray (Stanford Javascript Crypto Library)
	if(mode==="aes")
		return sjcl.decrypt(key,strToDecrypt);
	return encodeXORNoPattern(atob(strToDecrypt),key);
	
	
}

function encodeXORNoPattern(strToEncode,key){
	let result="";
	let keyChunk=key;
	let keyLength=keyChunk.length;
	let previousChars="";
	let cnt=0;
	for(let i=0;i<strToEncode.length;i++){
		let char=strToEncode.charAt(i);
		let keyByte=keyChunk.charAt(cnt);
		
		result+=String.fromCharCode(char.charCodeAt(0)^keyByte.charCodeAt(0));
		
		if(cnt%keyLength===(keyLength-1)){
			cnt=0;
			// NO : TOO LONG TREATMENT !: We hash with all the previous blocks too :
			// We hash with a character of the previous chunk (they add up at each new chunk):
			keyChunk=getHashedString(previousChars+keyChunk);
		
			previousChars+=keyChunk.charAt(keyChunk.length-1);
		}else{
			cnt++;
		}
	}
	return result;
}

// CAUTION : This method is declared this way because it's also used in a nodejs context!
// TODO : FIXME : DO THIS FOR ALL OF THESE COMMON METHODS AND FUNCTIONS !
generateRandomString=function(length,/*NULLABLE*/mode=null){
	
	// This list must be very conservative, because  we want to be able to pass it through GET URLs !
	// OLD (not enough conservative ?) : const ALLOWED_CHARS="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0912346789_-£¢¤¬²³¼½¾";
	const ALLOWED_CHARS="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0912346789!-:;";
	
	
	if(typeof(length)==="string"){
		if(contains(length,"->")){
			length=Math.getRandomInRange(length);
		}else{
			length=64;
		}
	}
	
	var result="";

	if(mode==="simple"){
		
		for(var i=0;i<length;i++){
			var	value=Math.getRandomInArray(ALLOWED_CHARS);
			result+=value;
		}
		
		return result;
	}else if(mode==="human"){
		
		const markovGraph={
			// For simplicity sakes, we ignore the consonants and vowels doubling possibilities:
			consonants:{// They always accept vowels at their following
				// Indicate ^ to specify wich of the other group letter to exclude as direct follower of the given letter :
				"b":["r","l"],
				"c":["h","t","k","l","r"],
				"d":["r"],
				"f":["r","l"],
				"g":["r","l"],
				"h":[],
				"j":["r"],
				"k":["r","l","t","h"],
				"l":["t"],
				"m":["b","p"],
				"n":["t","d"],
				"p":["h","r","s","t","l"],
				"q":["r","l"],
				"r":["t","c","d","k","q","h"],
				"s":["t","c","d","k","q","h"],
				"t":["r","h"],
				"v":["r","l"],
				"w":["r"],
				"x":["p"],
				"z":["r","h"],
			},
			vowels:{// They always accept consonants at their following
				"a":["i","u","y"],
				"e":["i","u","y"],
				"i":["e","o"],
				"o":["i","u"],
				"u":["i"],
				"y":[],
			},
		};
		
		const LETTERS_TYPES=Object.keys(markovGraph);

		const CURR_LETTER_MODE=Math.getRandomInArray(LETTERS_TYPES);
		const POSSIBLE_LETTERS=Object.keys(markovGraph[CURR_LETTER_MODE]);
		const CURR_LETTER=Math.getRandomInArray(POSSIBLE_LETTERS);
		
		var automataState={
			currentLetterMode:CURR_LETTER_MODE,
			possibleLetters:POSSIBLE_LETTERS,
			currentLetter:CURR_LETTER,
			
			calculateLetter:function(){
				
				this.currentLetter=Math.getRandomInArray(this.possibleLetters);
				
				var sameLetters=Object.keys( markovGraph[this.currentLetterMode] );
				if(!contains(sameLetters,this.currentLetter)){
					// If we picked from the same group : we don't change of group to pick
					// If we picked from the other group : we do change of group to pick
					
					var otherLetterMode=foreach(LETTERS_TYPES,(item)=>{
						if(item!==this.currentLetterMode)	return item;
					});
					
					this.currentLetterMode=otherLetterMode;
				}

			},
			
			updatePossibleLettersForLetter:function(){
				
				var nextPossibleLetters=[];
				var nextNotPossibleLetters=[];

				// We store those of the other group marked «^»
				foreach(markovGraph[this.currentLetterMode][this.currentLetter],(letter)=>{
					if(letter.charAt(0)==="^"){
						nextNotPossibleLetters.push(letter.charAt(1));
					}else{
						nextPossibleLetters.push(letter);
					}
				});
				
				var otherLetterMode=foreach(LETTERS_TYPES,(item)=>{
					if(item!==this.currentLetterMode)	return item;
				});
				var otherLetters=Object.keys(markovGraph[otherLetterMode]);
				
				// We exclude those marked «^»
				foreach( otherLetters ,(letter)=>{
					if(!contains(nextNotPossibleLetters,letter)){
						nextPossibleLetters.push(letter);
					}
				});

				this.possibleLetters=nextPossibleLetters;
			}
		};
		
		var i=0;
		do{

			result+=automataState.currentLetter;
			
			automataState.updatePossibleLettersForLetter();
			automataState.calculateLetter();
			
			i++;
		}while(i<length);
		
		return result;
	}
	
	for(var i=0;i<length;i++){
		var	value=Math.getRandomInt(255,0);
		result+=String.fromCharCode(value);
	}
	
	if(mode==="base64"){
		return btoa(result);
	}
	
	return result;
};

generateRandomStringFromOriginalString=function (str, randomFactor=1, mode=null){
	const strLength=str.length;
	const maxStrLength=strLength*randomFactor;
	const numberOfCharactersToChange=Math.getRandomInt(maxStrLength);
	const randomString=generateRandomString(numberOfCharactersToChange, mode);
	if(strLength<=maxStrLength)	return randomString;
	let strIndexes=[];
	for(let i=0;i<strLength;i++){
		strIndexes.push(i);
	}
	let chosenIndexes=getRandomsInArray(strIndexes, numberOfCharactersToChange);
	let newStrAsArray=Array.from(str);
	// chosenIndexes array size is the same as the number of characters in string randomString :
	forEach(chosenIndexes,(randomIndex,i)=>{
		newStrAsArray[randomIndex]=randomString[i];
	});
	const newStr=newStrAsArray.join("");
	return newStr;
}



// NOT AOTESTABLE !
function getUniqueIdWithDate(){
	return getHashedString(getNow() + "");
}






//================================================================
//================= Miscelanneous utility methods =================
//================================================================





// NOT AOTESTABLE !

// This function is for simple variables, like strings, booleans, or Objects you
// want to test as null:
// CAUTION : DOES ACTUALLY *NOT* HANDLES «undefined» variables !
window.nothing=function(nullable, insist=false){
	if(typeof nullable === "undefined")
		return true;
	
	if(typeof nullable === "function")
		return false;
	
	if(nullable === true)
		return false;
	
	// To avoid the case where the nullable is a number and equal to zero, in which case it is not considered as the same as null ! :
	if(nullable===0)
		return false;
	
	if((!nullable) || (typeof nullable === "undefined") || nullable === null || nullable === false){
		return true;
	}
	if(typeof nullable === "string" && insist && (nullable + "").trim() === ""){
		return true;
	}
	
	// Actually an unsafe method to determine that variable «sizable» is not a
	// classical (ie. non-associative) array...:
// NO : USE empty() instead: 	if(isArray(nullable))	return (nullable.length <= 0);
	
	return false;
}

window.getOrCreateSimpleObjectAttribute=function(parentObject, attributeNameOrAttributesName){
	if(nothing(parentObject))		return null;
	if(nothing(attributeNameOrAttributesName))	return parentObject;
	if(isString(attributeNameOrAttributesName) || (isArray(attributeNameOrAttributesName) && attributeNameOrAttributesName.length===1)){
		const attributeName=(isArray(attributeNameOrAttributesName) && attributeNameOrAttributesName.length===1)?
															attributeNameOrAttributesName[0]:attributeNameOrAttributesName;
		if(parentObject[attributeName]==null){
//		NO : parentObject[attributeName]=defaultValue;
			// BECAUSE CAUTION : if we have defaultValue as parameter, sometimes because of the recursive call, it can refer to a persisting object ! Thus, creating a strange bug.
			// (SO WE ALWAYS NEED TO START AS A BLANK STATE FOR DEFAULT VALUE OBJECTS !)
			const DEFAULT_VALUE={};
			parentObject[attributeName]=DEFAULT_VALUE;
		}
		let foundOrCreatedChild=parentObject[attributeName];
		return foundOrCreatedChild;
	}
	if(isArray(attributeNameOrAttributesName)){

		const attributeName=attributeNameOrAttributesName[0];
		if(parentObject[attributeName]==null){
//		NO : parentObject[attributeName]=defaultValue;
			// BECAUSE CAUTION : if we have defaultValue as parameter, sometimes because of the recursive call, it can refer to a persisting object ! Thus, creating a strange bug.
			// (SO WE ALWAYS NEED TO START AS A BLANK STATE FOR DEFAULT VALUE OBJECTS !)
			const DEFAULT_VALUE={};
			parentObject[attributeName]=DEFAULT_VALUE;
		}
		const sliced=attributeNameOrAttributesName.slice(1);
		let foundOrCreatedChild=getOrCreateSimpleObjectAttribute(parentObject[attributeName], sliced);
		return foundOrCreatedChild;
	}
	return null; // (case no child found nor could be created.)
}

window.nonull=function(value,defaultValue){
	if(value===false)	return value;
	if(nothing(value))	return defaultValue;
	return value;
}

window.blank=function(obj){
	return typeof(obj)==="undefined" || nothing(obj) || empty(obj,true);
}

// Actually an unsafe method to determine that variable is not a classical (ie.
// non-associative) array...:
// NOT AOTESTABLE !
window.isArray=function(arrayParam){
	return arrayParam && typeof arrayParam.length !== undefined && Array.isArray(arrayParam);
}

window.getArraySize=function(arrayOfValues){
	if(typeof(arrayOfValues)==="string")	return arrayOfValues.length;
	if(isArray(arrayOfValues)){
		return arrayOfValues.length;
	}
	return Object.keys(arrayOfValues).length;
}

window.valueAt=function(arrayOfValues,index){
	if(typeof(arrayOfValues) ==="string")	return arrayOfValues.charAt(index);
	if(index<0 || getArraySize(arrayOfValues)<=index )	return null;
	var cnt=0;
	// CAUTION : Only use this «return foreach» syntax with SINGLE-LEVEL nested loops ONLY !
	return foreach(arrayOfValues,(item)=>{
		if(index<=cnt)	return item;
		cnt++;
	});
}

window.splitIfPossible=function(valuesStr,separator){
	return !contains(valuesStr,separator)?[valuesStr]:valuesStr.split(separator);
}

window.foreachSplit=function(valuesStr,separator,doOnEachFunction){
	var arrayOfValues=splitIfPossible(valuesStr,separator);
	// CAUTION : Only use this «return foreach» syntax with SINGLE-LEVEL nested loops ONLY !
	return foreach(arrayOfValues,doOnEachFunction);
}

window.compare=function(value1,value2){
	if(value1==null && value2!=null)	return 1;
	if(value1!=null && value2==null)	return -1;
	if(value1==null && value2==null)	return 0;
	if(value1<value2)	return 1;
	if(value2<value1)	return -1;
	if(value1==value2)	return 0;
}

window.pushInArrayAsQueue=function(array, maxSize, item){
	if(maxSize<=getArraySize(array)){
		array.shift();
	}
	array.push(item);
	return item;
}



/*KNOWN LIMITATIONS : You will have an incomplete browsing loop if there are the string values "break" or "continue" in your array or associative array !*/
// CAUTION : Only use «return foreach» syntax in client code loops with SINGLE-LEVEL nested loops ONLY !
window.foreach=function(arrayOfValues,doOnEachFunction,
	/*OPTIONAL*/filterFunction=null,
	// CAUTION ! Javascript sort compare function result is the same as in Java :
//   0: if(a==b)
//  -1: if(a<b)
//   1: if(a>b)
	// for instance this is a valid javascript compare function : function(a, b){return a-b} so (100,20) returns positive result and (20,100) returns negative result !
	/*OPTIONAL*/compareFunction=null, sortKeys=false){
	
	// TODO : FIXME : Add the possibility to iterate over a string characters ?
	
	if(!arrayOfValues){
		// SILENT ERROR : 
		//		// TRACE
		//		log("ERROR : Array or associative array expected, received null, cannot iterate on its elements.");
		return null;
	}
	
	if(isPrimitive(arrayOfValues)){
		// TRACE
		log("ERROR : Array or associative array expected, received «"+(typeof(arrayOfValues))+"», cannot iterate on its elements.");
		return null;
	}
	
	if(isArray(arrayOfValues)){
		
		if(compareFunction){
			// If we have a compare function, then we have to filter the array beforehand:
			if(filterFunction){
				let arrayOfValuesOld=copy(arrayOfValues);
				arrayOfValues=[];
				for(let i=0;i<arrayOfValuesOld.length;i++){
					let item=arrayOfValuesOld[i];
					if(!filterFunction(item,i))	continue; // We have to filter the array while performing the sort.
					arrayOfValues.push(item);
				}
			}
			arrayOfValues.sort(compareFunction);
		}
		
		for(var i=0;i<arrayOfValues.length;i++){
			var item=arrayOfValues[i];
			if(!compareFunction){ // If we have a compare function, then array is already filtered ! (see previously)
				if(filterFunction && !filterFunction(item,i))	continue;
			}
			var returnedObjectOrCommand=doOnEachFunction(item,i);
			if(returnedObjectOrCommand){
				if(returnedObjectOrCommand==="continue"){
					continue;
				}else if(returnedObjectOrCommand==="break" || returnedObjectOrCommand==="return"){
					return;
				}else{
					return returnedObjectOrCommand;
				}
			}// else continue looping...
		
		}
		
		return null;
	}
	
	// Else : associative array
	
	let keysToIterateOn=[];
	const associativeArray=arrayOfValues;
	if(compareFunction){
		
//	if(!associativeArray.hasOwnProperty){
//		// TRACE
//		console.log("WARN : Object of type «"+(typeof(associativeArray))+"» has no method hasOwnProperty() method, cannot perform foreach ! ",associativeArray);
//		return null;
//	}
		
		// TODO : FIXME : Actually this is quite ineffective :
		// We completely loop through the associative array three times !
		var sortedKeysAndValues=[];
		for(var key in associativeArray){
			if(associativeArray.hasOwnProperty && !associativeArray.hasOwnProperty(key))	continue;
			var item=associativeArray[key];
			// If we have a compare function, then we have to filter the array beforehand:
			if(filterFunction && !filterFunction(item,key))	continue;
			sortedKeysAndValues.push({key:key,value:item});
		}
		sortedKeysAndValues.sort(compareFunction);
		
		// OLD : associativeArray={};
		for(var i=0;i<sortedKeysAndValues.length;i++){
			var item=sortedKeysAndValues[i];
			// OLD : associativeArray[item.key]=item.value;
			keysToIterateOn.push(item.key);
		}
		
	}else{
		keysToIterateOn=Object.keys(associativeArray);
	}

	if(!compareFunction && sortKeys){
		keysToIterateOn=keysToIterateOn.sort();
	}
		
	var cnt=0;
	for(var key of keysToIterateOn){
		if(associativeArray.hasOwnProperty && !associativeArray.hasOwnProperty(key))	continue;
		var item=associativeArray[key];
		
		if(!compareFunction){ // If we have a compare function, then array is already filtered !
			if(filterFunction && !filterFunction(item,key))	continue;
		}
		var returnedObjectOrCommand=doOnEachFunction(item,key,cnt);
		if(returnedObjectOrCommand){
			if(returnedObjectOrCommand==="continue"){
				continue;
			}else if(returnedObjectOrCommand==="break" || returnedObjectOrCommand==="return"){
				return;
			}else{
				return returnedObjectOrCommand;
			}
		}// else continue looping...
		cnt++;
	}
		
	
	
	//
	return null;
}

window.getKeyAt=function(associativeOrNormalArray,index){
	return getAt(associativeOrNormalArray,index,true);
}

window.getAt=function(associativeOrNormalArray,index,returnKey=false){
	if(empty(associativeOrNormalArray))	return null;
	let i=0;
	// CAUTION : Only use this «return foreach» syntax with SINGLE-LEVEL nested loops ONLY !
	return foreach(associativeOrNormalArray,(item, key)=>{
		if(index<=i)	return returnKey?key:item;
		i++;
	},null,null,returnKey);
}

window.getFirst=function(associativeOrNormalArray){
	if(empty(associativeOrNormalArray))	return null;
	return getAt(associativeOrNormalArray, 0);
}

window.getLast=function(associativeOrNormalArray){
	if(empty(associativeOrNormalArray))	return null;
	return getAt(associativeOrNormalArray, getArraySize(associativeOrNormalArray)-1);
}


/*
 * KEEP CODE : (interesting syntax...!) function getFunctionName(fn){ var f =
 * typeof fn == 'function'; var s=f && ((fn.name && ['', fn.name ]) ||
 * fn.toString().match(/function ([^\(]+)/)); return (!f && 'not a function') ||
 * (s && s[1] || 'anonymous'); }
 */
// NOT AOTESTABLE !
window.getFunctionName=function(fn){
	// Case not a function :
	if(typeof fn !== "function")
		return null;
	var name=fn.toString().match(/function ([^\(]+)/);
	// Case not anonymous function :
	if(!name || name.length <= 0 || !name[1])
		return null;
	return name[1].trim();
}


window.hasDelayPassed=function(timeVariable,delayMillis){
	if(!timeVariable || !delayMillis)	return true;
	return getNow()-timeVariable >= delayMillis;
}

window.getNow=function(){
	return new Date().getTime();
}



// UNUSED:
// CAUTION : this is a costly method...
window.gotoValueLinearly=function(source, destination, doOnRefreshTreatment,/*OPTIONAL*/onEndMethod,/*OPTIONAL*/refreshingRateMillisParam,/*OPTIONAL*/
	numberOfStepsParam){
	
	var numberOfSteps=numberOfStepsParam ? numberOfStepsParam : 10;
	var refreshingRateMillis=refreshingRateMillisParam ? refreshingRateMillisParam : 500;
	
	var delta=destination - source;
	var sign=delta == 0 ? 1 : delta / Math.abs(delta);
	var step=(Math.abs(delta * (1 / numberOfSteps)));
	
	var cnt=0;
	var pe=new PeriodicalExecuter(function(){
	
		if(cnt >= numberOfSteps){
			pe.stop();
			if(onEndMethod)
				onEndMethod();
			return;
		}
	
		if(doOnRefreshTreatment)
			doOnRefreshTreatment(source + sign * step, cnt / numberOfSteps);// Advancement
		// ratio
	
		cnt++;
	
	}, refreshingRateMillis / 1000);

}

// ==================== Dates management ====================
window.compareDates=function(date1=null,date2=null){
	if(date1==null && date2==null)	return 0;
	if(date1==null && date2!=null)	return 1;
	if(date1!=null && date2==null)	return -1;
	if(typeof(date1)=="Date" && typeof(date2)=="Date"){
		if(date1.getTime()==date2.getTime())	return 0;
		if(date1.getTime()<date2.getTime())		return 1;
		return -1;
	}
	if(typeof(date1)=="number" && typeof(date2)=="number"){
		if(date1==date2)	return 0;
		if(date1<date2)		return 1;
		return -1;
	}
	throw new Error(`Cannot compare dates with types : ${typeof(date1)} ; ${typeof(date2)}`);
}


window.getRandomDateFormattedString=function(daysSpan=365,format="ddMMYYYY") {
	const newDate=getRandomDate(daysSpan);
	const result=formatDate(newDate,format);
	return result;
}

window.getRandomDate=function(spansRangeDays=365, spansRangeMonths=0, spansRangeYears=0, referenceDate=new Date()) {
	const start=new Date(referenceDate);
	const end=new Date(referenceDate);
	
	if(spansRangeDays){
		const HALF_SPAN_DAYS=Math.round(spansRangeDays/2);
		start.setDate(referenceDate.getDate()-HALF_SPAN_DAYS);
		end.setDate(referenceDate.getDate()+HALF_SPAN_DAYS);
	}else if(spansRangeMonths){
		const HALF_SPAN_MONTHS=Math.round(spansRangMonthse/2);
		start.setMonths(referenceDate.getMonths()-HALF_SPAN_MONTHS);
		end.setMonths(referenceDate.getMonths()+HALF_SPAN_MONTHS);
	}if(spansRangeYears){
		const HALF_SPAN_YEARS=Math.round(spansRangeYears/2);
		start.setFullYear(referenceDate.getFullYear()-HALF_SPAN_YEARS);
		end.setFullYear(referenceDate.getFullYear()+HALF_SPAN_YEARS);
	}
	
	return getRandomDateInPeriod(start, end);
}
                                                            
window.getRandomDateInPeriod=function(start, end) {
	// Generate a random date between start and end
	const deltaTime= end.getTime() - start.getTime();
	return new Date(start.getTime() + Math.random() * deltaTime);
}

window.formatDate=function(date, format) {
	// Extract parts of the date
	const day = String(date.getDate()).padStart(2, "0");
	const month = String(date.getMonth() + 1).padStart(2, "0"); // Months are 0-based
	const year = date.getFullYear();
	const hours = String(date.getHours()).padStart(2, "0");
	const minutes = String(date.getMinutes()).padStart(2, "0");
	const seconds = String(date.getSeconds()).padStart(2, "0");
	// Replace placeholders in the format string
	return format.replace("dd", day)
	             .replace("MM", month)
	             .replace("yyyy", year)
	             .replace("HH", hours)
	             .replace("mm", minutes)
	             .replace("ss", seconds);
}


// ==================== Strings management ====================




//==================== Compression ====================

//Copyright © 2013 Pieroxy <pieroxy@pieroxy.net>
//This work is free. You can redistribute it and/or modify it
//under the terms of the WTFPL, Version 2
//For more information see LICENSE.txt or http://www.wtfpl.net/
//
//https://pieroxy.net/blog/pages/lz-string/index.html
//
//LZ-based compression algorithm, version 1.0.2-rc1

//USAGE : 
//var string="This is my compression test.";
//alert("Size of sample is: " + string.length);
//var compressed=LZString.compress(string);
//alert("Size of compressed sample is: " + compressed.length);
//string=LZString.decompress(compressed);
//alert("Sample is: " + string);

//var LZString={
window.LZWString={
	
	writeBit : function(value, data){
	data.val=(data.val << 1) | value;
	if (data.position == 15){
	 data.position=0;
	 data.string += String.fromCharCode(data.val);
	 data.val=0;
	} else {
	 data.position++;
	}
	},
	
	writeBits : function(numBits, value, data){
	if (typeof(value)=="string")
	 value=value.charCodeAt(0);
	for (var i=0 ; i<numBits ; i++){
	 this.writeBit(value&1, data);
	 value=value >> 1;
	}
	},
	
	produceW : function (context){
	if (Object.prototype.hasOwnProperty.call(context.dictionaryToCreate,context.w)){
	 if (context.w.charCodeAt(0)<256){
	   this.writeBits(context.numBits, 0, context.data);
	   this.writeBits(8, context.w, context.data);
	 } else {
	   this.writeBits(context.numBits, 1, context.data);
	   this.writeBits(16, context.w, context.data);
	 }
	 this.decrementEnlargeIn(context);
	 delete context.dictionaryToCreate[context.w];
	} else {
	 this.writeBits(context.numBits, context.dictionary[context.w], context.data);
	}
	this.decrementEnlargeIn(context);
	},
	
	decrementEnlargeIn : function(context){
	context.enlargeIn--;
	if (context.enlargeIn == 0){
	 context.enlargeIn=Math.pow(2, context.numBits);
	 context.numBits++;
	}
	},
	
	/*public*/compress: function (uncompressed){
	var context={
	 dictionary: {},
	 dictionaryToCreate: {},
	 c:"",
	 wc:"",
	 w:"",
	 enlargeIn: 2, // Compensate for the first entry which should not count
	 dictSize: 3,
	 numBits: 2,
	 result: "",
	 data: {string:"", val:0, position:0}
	}, i;
	
	for (i=0; i<uncompressed.length; i += 1){
	 context.c=uncompressed.charAt(i);
	 if (!Object.prototype.hasOwnProperty.call(context.dictionary,context.c)){
	   context.dictionary[context.c]=context.dictSize++;
	   context.dictionaryToCreate[context.c]=true;
	 }
	 
	 context.wc=context.w + context.c;
	 if (Object.prototype.hasOwnProperty.call(context.dictionary,context.wc)){
	   context.w=context.wc;
	 } else {
	   this.produceW(context);
	   // Add wc to the dictionary.
	   context.dictionary[context.wc]=context.dictSize++;
	   context.w=String(context.c);
	 }
	}
	
	// Output the code for w.
	if (context.w !== ""){
	 this.produceW(context);
	}
	
	// Mark the end of the stream
	this.writeBits(context.numBits, 2, context.data);
	
	// Flush the last char
	while (context.data.val>0) this.writeBit(0,context.data)
	return context.data.string;
	},
	
	readBit : function(data){
	var res=data.val & data.position;
	data.position >>= 1;
	if (data.position == 0){
	 data.position=32768;
	 data.val=data.string.charCodeAt(data.index++);
	}
	//data.val=(data.val << 1);
	return res>0 ? 1 : 0;
	},
	
	readBits : function(numBits, data){
	var res=0;
	var maxPower=Math.pow(2,numBits);
	var power=1;
	while (power!=maxPower){
	 res |= this.readBit(data) * power;
	 power <<= 1;
	}
	return res;
	},
	
	/*public*/decompress: function (compressed){
	var dictionary={},
	   next,
	   enlargeIn=4,
	   dictSize=4,
	   numBits=3,
	   entry="",
	   result="",
	   i,
	   w,
	   c,
	   errorCount=0,
	   literal,
	   data={string:compressed, val:compressed.charCodeAt(0), position:32768, index:1};
	
	for (i=0; i<3; i += 1){
	 dictionary[i]=i;
	}
	
	next=this.readBits(2, data);
	switch (next){
	 case 0: 
	   c=String.fromCharCode(this.readBits(8, data));
	   break;
	 case 1: 
	   c=String.fromCharCode(this.readBits(16, data));
	   break;
	 case 2: 
	   return "";
	}
	dictionary[3]=c;
	w=result=c;
	while (true){
	 c=this.readBits(numBits, data);
	 
	 switch (c){
	   case 0: 
	     if (errorCount++>10000) return "Error";
	     c=String.fromCharCode(this.readBits(8, data));
	     dictionary[dictSize++]=c;
	     c=dictSize-1;
	     enlargeIn--;
	     break;
	   case 1: 
	     c=String.fromCharCode(this.readBits(16, data));
	     dictionary[dictSize++]=c;
	     c=dictSize-1;
	     enlargeIn--;
	     break;
	   case 2: 
	     return result;
	 }
	 
	 if (enlargeIn == 0){
	   enlargeIn=Math.pow(2, numBits);
	   numBits++;
	 }
	
	 if (dictionary[c]){
	   entry=dictionary[c];
	 } else {
	   if (c === dictSize){
	     entry=w + w.charAt(0);
	   } else {
	     return null;
	   }
	 }
	 result += entry;
	 
	 // Add w+entry[0] to the dictionary.
	 dictionary[dictSize++]=w + entry.charAt(0);
	 enlargeIn--;
	 
	 w=entry;
	 
	 if (enlargeIn == 0){
	   enlargeIn=Math.pow(2, numBits);
	   numBits++;
	 }
	 
	}
	return result;
	}
};





window.startsWith=window.aotest({
	name : "startsWith",
	scenario1ok : [ [ "https://example.com", "https://" ], true ],
	scenario2ok : [ [ "https://example.com", "http://" ], false ]
}, function(str, subStr){
	return str.lastIndexOf(subStr, 0) === 0;
}, !PERFORM_TESTS_ON_LIBRARY);



window.isValidEmail=function (value){
	var re=/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

window.getSoundexFrSimple=function(strParam){
	// CODE FOUND AT SOURCE : phpjs.org/functions/soundex/
	// kevin.vanzonneveld.net
	// + original by: Jonas Raoni Soares Silva (www.jsfromhell.com)
	// + tweaked by: Jack
	// + improved by: Kevin van Zonneveld (kevin.vanzonneveld.net)
	// + bugfixed by: Onno Marsman
	// + input by: Brett Zamir (brett-zamir.me)
	// + bugfixed by: Kevin van Zonneveld (kevin.vanzonneveld.net)
	// + original by: Arnout Kazemier (www.3rd-Eden.com)
	// + revised by: Rafał Kukawski (blog.kukawski.pl)
	
	var str=(strParam + "").toUpperCase().trim();
	if(!str)
		return "";
	
	// French version : SOURCE : fr.wikipedia.org/wiki/Soundex
	var sdx=[ 0, 0, 0, 0 ], m={
		B : 1,
		P : 1,
		C : 2,
		K : 2,
		Q : 2,
		D : 3,
		T : 3,
		L : 4,
		M : 5,
		N : 5,
		R : 6,
		G : 7,
		J : 7,
		X : 8,
		Z : 8,
		S : 8,
		F : 9,
		V : 9
	};
	var i=0;
	var j;
	var s=0;
	var c;
	var p;
	
	while ((c=str.charAt(i++)) && s<4){
		if(j=m[c]){
			if(j !== p)
				sdx[s++]=p=j;
		} else {
			s += i === 1;
			p=0;
		}
	}
	sdx[0]=str.charAt(0);
	return sdx.join("");
}

window.getSoundexEn=function(strParam){
	// CODE FOUND AT SOURCE : phpjs.org/functions/soundex/
	// kevin.vanzonneveld.net
	// + original by: Jonas Raoni Soares Silva (www.jsfromhell.com)
	// + tweaked by: Jack
	// + improved by: Kevin van Zonneveld (kevin.vanzonneveld.net)
	// + bugfixed by: Onno Marsman
	// + input by: Brett Zamir (brett-zamir.me)
	// + bugfixed by: Kevin van Zonneveld (kevin.vanzonneveld.net)
	// + original by: Arnout Kazemier (www.3rd-Eden.com)
	// + revised by: Rafał Kukawski (blog.kukawski.pl)
	// * example 1: soundex('Kevin');
	// * returns 1: 'K150'
	// * example 2: soundex('Ellery');
	// * returns 2: 'E460'
	// * example 3: soundex('Euler');
	// * returns 3: 'E460'
	var str=(strParam + "").toUpperCase().trim();
	if(!str)
		return "";
	
	var sdx=[ 0, 0, 0, 0 ], m={
		B : 1,
		F : 1,
		P : 1,
		V : 1,
		C : 2,
		G : 2,
		J : 2,
		K : 2,
		Q : 2,
		S : 2,
		X : 2,
		Z : 2,
		D : 3,
		T : 3,
		L : 4,
		M : 5,
		N : 5,
		R : 6
	}, i=0, j, s=0, c, p;
	
	while ((c=str.charAt(i++)) && s<4){
		if(j=m[c]){
			if(j !== p)
				sdx[s++]=p=j;
		} else {
			s += i === 1;
			p=0;
		}
	}
	sdx[0]=str.charAt(0);
	return sdx.join("");
}

window.clearAccentuatedCharacters=function(str){
	return str.replace(/[àâä]/gim, "a").replace(/[éèêë]/gim, "e").replace(/[ìîï]/gim, "i").replace(/[òôö]/gim, "o").replace(/[ùûü]/gim, "u").replace(/[ç]/gim,
		"c");
}

window.toOneSimplifiedLine=window.aotest({
name : "toOneSimplifiedLine"
// ,scenario1:[[" test1\ntest2","compactHTML"],"test1<br />test2"]
,
scenario1 : [ [ " test1\ntest2", "compactHTML" ], "test1\ntest2" ],
scenario2 : [ [ " test1 \ntest2  ", "preserveWhitespaces" ], "test1  test2" ]
// ,scenario3:[[" test1 <br>\n\ntest2","compactHTML"],"test1 <br /><br /><br
// />test2"]
,
scenario3 : [ [ " test1  <br>\n\ntest2", "compactHTML" ], "test1 <br />\n\ntest2" ],
scenario4 : [ [ " test1  \r\r\ntest2", "compactHTML" ], "test1 \n\n\ntest2" ]
	}, function(str,/*OPTIONAL*/mode){
	
	// USE WITH CAUTION, because it will remove all your «&amp;», «&infin;» and so
	// on for all HTML characters :
	if(mode && mode == "hard")
		return str.replace(new RegExp("[^\\wàâäéèêëìîïòôöùûüç]+", "igm"), " ").trim();
	
	if(mode && mode == "preserveWhitespaces"){
		var result=str.replace(/[\s\t\n\r\v\f]/igm, " ").trim();
		return result;
	}
	
	if(mode && mode == "compactHTML"){
		var result =
		// str.replace(/[\s\t\n\r]+/igm," ").replace(/>[\s\t\n\r]+</igm,"><")
		str
		// MUST BE FIRST REPLACEMENT : because antislash-n and antislash-r are
		// considered as antislash-s characters !
		// .replace(/[\n\r]/igm,"<br />")
		.replace(/[\n\r]/igm, "§§§") // TODO : FIXME : Find a more elegant
		// solution... :-S
		.replace(/[\s\t]+/igm, " ").replace(/>[\s\t]+</igm, "><").replace(/§§§/igm, "\n")
		// TODO : FIXME : Find a more elegant solution... :-S
		.replace(/<br>/gim, "<br />")
		// dangerous substitution, but necessary, unfortunately...
		// (so we have to compensate it somewhere else IF NECESSARY, with the
		// APOSTROPHE character for apostrophe in text.)
		.replace(/'/gim, '"').trim();
	
		return result;
	}
	
	// «Soft» mode is default mode :
	return str.replace(/[\s\t\n\r]+/igm, " ").trim();
}, !PERFORM_TESTS_ON_LIBRARY);


window.dec2hex=function(d){
	return d.toString(16);
}

window.hex2dec=function(h){
	return parseInt(h, 16);
}


window.hashSafe=function(str){
	if(typeof(crypto)==="undefined" || typeof(crypto.createHash)==="undefined"){

//		// TRACE
//		console.log("ERROR : NodeJS installation does not include «crypto» node module dependency. Trying to use client-side hashing infrastructure.");

		if(typeof(getHashedString)==="undefined" || typeof(sjcl)==="undefined"){
//			// TRACE
//			console.log("ERROR : There is no client-side hashing infrastructure. Using fallback hashing.");
			
			let hashCode=function(strParam){
			  var result=0, i, chr;
			  if(strParam.length === 0) return result;
			  for (i=0; i<strParam.length; i++){
			    chr  =strParam.charCodeAt(i);
			    result =((result << 5) - result) + chr;
			    result |= 0; // Convert to 32bit integer
			  }
			  return Math.abs(result); // No negative integer value returned.
			};
			
			
			return hashCode(str);
		}
		return getHashedString(str);
	}
	return crypto.createHash("md5").update(str).digest("hex");
}


window.uncapitalize=function(str){
	if(nothing(str))	return null;
	if(empty(str))	return "";
	return str[0].toLowerCase()+str.substring(1);
}

// Arrays management :

window.copy=function(array){
	var result=null;
	
	// if(!isAssociative){
	if(array instanceof Array){
	
		result=new Array();
		for (var i=0; i<array.length; i++)
			result.push(array[i]);
	} else {
		// Case of regular objects that are like associative arrays :
	
		result=new Object();
		for (key in array){
			if(!array.hasOwnProperty(key))
				continue;
			result[key]=array[key];
		}
	}
	return result;
}



window.removeByIndex=function(array, elementIndex){
	if(array instanceof Array){
		if(empty(array))	return false;
		if(elementIndex<0)	return false;
		array.splice(elementIndex, 1);
		return true;
	} 
	let hasDeleted=false;
	// Case of regular objects that are like associative arrays :
	let i=0;
	for (key in array){
		if(!array.hasOwnProperty(key)) continue;
		if(i===elementIndex){
			delete array[key];
			hasDeleted=true;
			break;
		}
		i++;
	}
	return hasDeleted;
}


window.remove=function(array, element){
	if(array instanceof Array){
		if(empty(array))	return false;
		var i=array.indexOf(element);
		if(i<0)	return false;
		array.splice(i, 1);
		return true;
	} 
	let hasDeleted=false;
	// Case of regular objects that are like associative arrays :
	for (key in array){
		if(!array.hasOwnProperty(key)) continue;
		if(array[key] === element){
			delete array[key];
			hasDeleted=true;
			break;
		}
	}
	return hasDeleted;
}


// CAUTION : ONLY WORKS ON Array of type ARRAYS, NOT Object ARRAYS !
window.swapInArray=function (array, i1, i2){
	if(!isArray(array)){
		console.log("ERROR: Parameter array is not a plain array:",array);
		return;
	}
	var temp=array[i1];
	array[i1]=array[i2];
	array[i2]=temp;
}



window.findMatches=function(array1,array2,compareFunction){
	let resultMatches=[];
	
	foreach(array1,(element1)=>{
		foreach(array2,(element2)=>{
			if(compareFunction(element1,element2)){
				resultMatches.push( [element1,element2] );
			}
		});
	});
	return resultMatches;
}



Math.isEven=function(number){
	if(!isNumber(number,true)){
		console.log("ERROR: Parameter number is not a number:",number);
		return;
	}
	return (number%2)===0;
}

Math.sumInArray=function(arrayOfValues){
	if(empty(arrayOfValues))
		return 0;
	let sum=0;
	foreach(arrayOfValues,(val)=>{
		sum+=val;
	},(val)=>{	return isNumber(val); });
	return sum;

};

Math.productInArray=function(arrayOfValues){
	if(empty(arrayOfValues))
		return 0;
	let product=1;
	foreach(arrayOfValues,(val)=>{
		product*=val;
	},(val)=>{	return isNumber(val); });
	return product;
};

Math.averageInArray=function(arrayOfValues){
	if(empty(arrayOfValues))
		return null;
	return Math.sumInArray(arrayOfValues) / arrayOfValues.length;
};

Math.maxInArray=function(arrayOfValues, returnKey=false){
	if(empty(arrayOfValues))
		return null;
		
//	var max=arrayOfValues[0];
//	for (var i=0; i<arrayOfValues.length; i++)
//		if(max<arrayOfValues[i])
//			max=arrayOfValues[i];
	
	let returnedKey=getKeyAt(arrayOfValues,0);
	let max=getAt(arrayOfValues,0);
	foreach(arrayOfValues,(item, key)=>{
		if(max<item){
			returnedKey=key;
			max=item;
		}
	},null,null,true);
	
	return (returnKey?returnedKey:max);
};

Math.minInArray=function(arrayOfValues, returnKey=false){
	if(empty(arrayOfValues))
		return null;
		
//	var min=arrayOfValues[0];
//	for (var i=0; i<arrayOfValues.length; i++)
//		if(arrayOfValues[i]<min)
//			min=arrayOfValues[i];
	
	let returnedKey=getKeyAt(arrayOfValues,0);
	let min=getAt(arrayOfValues,0);
	foreach(arrayOfValues,(item, key)=>{
		if(item<min){
			returnedKey=key;
			min=item;
		}
	},null,null,true);
	
	return (returnKey?returnedKey:min);
};



// TODO : FIXME : Create a namespace MathUtils. and ArraysUtils. and sort out the corresponding static methods.
Math.getRandomsInArray=function(arrayOfValues, numberOfItemsToGet=1){
	
	if(numberOfItemsToGet<0 || empty(arrayOfValues))	return [];
	const size=getArraySize(arrayOfValues);
	// We skip a costly random calculation if we have only one element in array :
	if(size===1)	return [valueAt(arrayOfValues,0)];
	
	const indexesPool=[];
	for(let i=0;i<size;i++)		indexesPool.push(i);
	
	const results=[];	
	for(let i=0;i<numberOfItemsToGet;i++){
		if(empty(indexesPool))	break;
		const chosenIndex=Math.getRandomInArray(indexesPool,true);
		const item=valueAt(arrayOfValues,chosenIndex);
		results.push(item);
	}

	return results;
}

// TODO : FIXME : Create a namespace MathUtils. and ArraysUtils. and sort out the corresponding static methods.
Math.getRandomInArray=function(arrayOfValues,removeFromArray=false){
	if(empty(arrayOfValues))	return null;
	const size=getArraySize(arrayOfValues);
	// We skip a costly random calculation if we have only one element in array :
	if(size===1)	return valueAt(arrayOfValues,0);
	
	const chosenIndex=Math.getRandomInt(size-1,0);
	const result=valueAt(arrayOfValues, chosenIndex);
	if(removeFromArray)		removeByIndex(arrayOfValues, chosenIndex);
	return result;
}

// This function is for arrays, like Array or Objects used as associative arrays you want to test as holding elements :
window.empty=function(sizable,/*OPTIONAL*/insist=false){
	
	if(nothing(sizable, insist))
		return true;

// NEW :
	const result=(countKeys(sizable)==0);
	return result;

// OLD :
//	if(typeof sizable === "object"){ // for classical and associative arrays
//	
//		if(sizable instanceof Array)
//			return sizable.length <= 0;
//	
//		// Actually an unsafe method to determine that variable «sizable» is not a
//		// classical (ie. non-associative) array...:
//		if(!isArray(sizable)){
//			for (key in sizable){
//				if(!sizable.hasOwnProperty(key))
//					continue;
//				return false;
//			}
//			return true;
//		}
//	
//	}
//	
//	// Object «sizable» is not really sizable, at this point :
//	return (sizable.length <= 0);
}

window.clearArray=function(arr){
	if(!arr || !arr.length)	return;
	arr.length=0;
}

window.addInArrayInIncreasingOrder=function(arrayToAddItem,itemToAdd,methodToGetItemValueForComparison=null){
	
	
	if(empty(arrayToAddItem)){
		arrayToAddItem.push(itemToAdd);
		return;
	}
	
	if(!methodToGetItemValueForComparison)
		methodToGetItemValueForComparison=function(item){return item;};
	
	
	let minValue=methodToGetItemValueForComparison(arrayToAddItem[0]);
	let maxValue=methodToGetItemValueForComparison(arrayToAddItem[arrayToAddItem.length-1]);
	
	
	let comparisonValue=methodToGetItemValueForComparison(itemToAdd);
	if(minValue===maxValue){
		if(comparisonValue<minValue){
			arrayToAddItem.splice(0,0,itemToAdd);
			return;
		}else{
			arrayToAddItem.push(itemToAdd);
			return;
		}
	}else{
		
		if(comparisonValue<=minValue){
			arrayToAddItem.splice(0,0,itemToAdd);
			return;
		}if(maxValue<=comparisonValue){
			arrayToAddItem.push(itemToAdd);
			return;
		}else{
			
			if((minValue-comparisonValue)<(maxValue-comparisonValue)){
				// Value is closer to the min value than to the max value :
				// So we start at the min value :
				
				let indexToInsertAfter=0;
				for(var i=0;i<arrayToAddItem.length;i++){
					let currentItem=arrayToAddItem[i];
					let currentValue=methodToGetItemValueForComparison(currentItem);
					
					if(currentValue<comparisonValue){
						indexToInsertAfter++;
						continue;
					}else{
						arrayToAddItem.splice(indexToInsertAfter,0,itemToAdd);
						return;
					}
				}
				
				
			}else{
				
				let indexToInsertAfter=arrayToAddItem.length-1;
				for(var i=arrayToAddItem.length-1;i>=0;i--){
					let currentItem=arrayToAddItem[i];
					let currentValue=methodToGetItemValueForComparison(currentItem);
					
					if(comparisonValue<=currentValue){
						indexToInsertAfter--;
						continue;
					}else{
						arrayToAddItem.splice(indexToInsertAfter,0,itemToAdd);
						return;
					}
				}
				
			}
		
		}
		
	}
	

};

window.containsIgnoreCase=function(str, chunk,/*OPTIONAL*/useRegexp){
	if(nothing(str) || nothing(chunk)){
		// //TRACE
		// console.log("WARN : Container or contenainee is empty ! Cannot search ignoring
		// case.");
		return false;
	}
	
	if(typeof str !== "string"){
		// //TRACE
		// console.log("WARN : Container is not a string ! " +
		// "Cannot search ignoring case for «"+chunk+"»("+(typeof chunk)+") in
		// «"+str+"»("+(typeof str)+").");
		return false;
	}
	
	if(!useRegexp){
		// SURE WORKS :
		if(!str.toLowerCase || !chunk.toLowerCase){
			// if(nothing(str.toLowerCase) || nothing(chunk.toLowerCase)){
			// //TRACE
			// console.log("WARN : Container or contenainee is not a string ! " +
			// "Cannot search ignoring case for «"+chunk+"»("+(typeof chunk)+") in
			// «"+str+"»("+(typeof str)+").");
			return false;
		}
	
		return str.toLowerCase().indexOf(chunk.toLowerCase()) !== -1;
	}
	return new RegExp(chunk, "gim").test(str);
};

window.isString=function (str){
	return str && typeof(str)==="string";
};

window.containsOneOf=function(container, objectsToFind,/*OPTIONAL*/useRegexp=false,searchInKeysOnly=false){
	return !!foreach(objectsToFind,(objectToFind)=>{
		let result=contains(container, objectToFind,useRegexp,searchInKeysOnly);
		if(result)	return result;
	});
};


window.contains=function(container, objectToFind=null,/*OPTIONAL*/useRegexp=false,searchInKeysOnly=false){
	if(nothing(container)){
		// //TRACE
		// console.log("WARN : Container is empty ! Cannot search for «"+objectToFind+"».");
		return false;
	}
	if(!isString(container) && !objectToFind && window.contains.filterFunction && isFunction(window.contains.filterFunction)){
		const itemIsFound=!!foreach(container, (item, keyOrIndex)=>{
			if(window.contains.filterFunction(item, keyOrIndex))	return true;
		});
		delete window.contains.filterFunction;
		return itemIsFound;
	}else{
		// Note : We also allow the search of null plain items in a container !
		
		if(isArray(container)){
			// for (var i=0; i<container.length; i++){
			// if(container[i] === objectToFind) return true;
			// }
			// return false;
			return container.indexOf(objectToFind) >= 0;
		} else if((container instanceof Object) && (typeof container !== "string")){
			for (key in container){
				if(!container.hasOwnProperty(key))		continue;
				if(!searchInKeysOnly){
					if(container[key] === objectToFind)
						return true;
				}else{
					if(key === (typeof(objectToFind)!=="string"?(""+objectToFind):objectToFind)) // CAUTION : Associative arrays keys are always strings !
						return true;
				}
			}
			return false;
		}
		
		// Container is a string case :
		var str=container;
		var chunk=objectToFind;
		if(!useRegexp)
			return str.indexOf(chunk) !== -1;
		return new RegExp(chunk, "gm").test(str);
	}
};
window.contains.filter=function(filterFunctionParam){
	window.contains.filterFunction=filterFunctionParam;
	return window.contains;
}


window.hasKey=function(container, keyToFind){
	let result=foreach(container,(item,key)=>{
		if(key===keyToFind)	return true;
	});
	return !!result /*(forced to boolean)*/;
};


window.countKeys=function(container, keysToExclude){
	
	// We get all keys :
	let keys=Object.keys(container);
	let result=keys.length;

	foreach(keysToExclude,(k)=>{
		// If a key is in the keys to exclude, then we decrement the result :
		// CAUTION : We consider that an object with {key:null} and an object without the key «key» are equivalent cases.
		if(container[k])	result--;
	});

	return result;
};


(window?window:global).merge=function(array1, array2, recursive=false){
	var arePlainArrays=isArray(array1) && isArray(array2);
	var list=arePlainArrays ? [] : {};
	foreach(array1,(e,key)=>{
		if(arePlainArrays){
			if(contains(list, e))	return "continue";
			list.push(e);
		}else{ // case associative arrays :
			if(!!list[key] /*(forced to boolean)*/)	return "continue";
			if(recursive && array2[key]){
				list[key]=merge(e,array2[key],true);
			}else{
				list[key]=e;
			}
		}
	});
	
	foreach(array2,(e,key)=>{
		if(arePlainArrays){
			if(contains(list, e))	return "continue";
			list.push(e);
		}else{ // case associative arrays :
			if(!!list[key] /*(forced to boolean)*/)	return "continue";
			list[key]=e;
		}
	});
	
	return list;
};


window.aotest({
	name : "isArray",
	dummies : function(result){
		result.tab1=[ 10, "gru", "test" ];
		result.obj1={
			truc : "muche",
			attr : [ 1, 2, 3, "4" ]
		};

		return result;
	},
	scenario1 : [ [ {
		dummy : "tab1"
	} ], true ],
	scenario2 : [ [ {
		dummy : "obj1"
	} ], false ]

}, isArray, !PERFORM_TESTS_ON_LIBRARY);

window.arraysEqual=function(array1, array2){
	if(!array1 && !array2)
		return true;
	if(!array1 || !array2)
		return false;

	// Only compares traditionnal arrays, not associative ones ! (use the method
	// objectsEqual for this)
	// Actually an unsafe method to determine that variable is not a classical
	// (ie. non-associative) array...:
	if(!isArray(array1) && !isArray(array2))
		return false;
	if(typeof array1 !== typeof array2)
		return false;
	if(array1.length !== array2.length)
		return false;

	for (var i=0; i<array1.length; i++){
		var a1=array1[i];
		var a2=array2[i];

		if(typeof a1 !== typeof a2)
			return false;

		if(typeof a1 === "object" && typeof a2 === "object"){
			if(isArray(a1) && isArray(a2)){
				if(!arraysEqual(a1, a2))
					return false;
			} else {
				if(!objectsEqual(a1, a2))
					return false;
			}
		}
	}

	return true;
}

window.aotest({
	name : "arraysEqual",
	dummies : function(result){
		result.tab1=[ 10, "gru", "test" ];
		result.tab2=[ 10, "gru", "test" ];
		result.tab3=[ 10, "gru" ];
		result.tab4=[ 11, "gru", "mouarff" ];

		result.tab5=[ 11, "gru", {
			truc : "muche"
		} ];
		result.tab6=[ 11, "gru", {
			truc : "muche"
		} ];

		result.tab7=[ 11, "gru", result.obj1 ];
		result.tab8=[ 11, "gru", result.obj1 ];

		return result;
	},
	scenario1 : [ [ {
		dummy : "tab1"
	}, {
		dummy : "tab2"
	} ], true ],
	scenario2 : [ [ {
		dummy : "tab2"
	}, {
		dummy : "tab3"
	} ], false ],
	scenario3 : [ [ {
		dummy : "tab3"
	}, {
		dummy : "tab4"
	} ], false ],
	scenario4 : [ [ {
		dummy : "tab5"
	}, {
		dummy : "tab6"
	} ], true ],
	scenario5 : [ [ {
		dummy : "tab7"
	}, {
		dummy : "tab8"
	} ], true ]

}, arraysEqual, !PERFORM_TESTS_ON_LIBRARY);

window.objectsEqual=window.aotest({
	name : "objectsEqual",
	dummies : function(result){

		result.obj3={
			attr1 : "blabla",
			attr2 : -1
		};
		result.obj4={
			attr1 : "blabla",
			attr2 : -1
		};
		result.obj5={
			attr1 : "blabla",
			attr2 : true
		};
		result.obj6={
			attr1 : "blabla",
			attrOtherName2 : true
		};

		return result;
	},
	scenario1 : [ [ {
		dummy : "obj3"
	}, {
		dummy : "obj4"
	} ], true ],
	scenario2 : [ [ {
		dummy : "obj4"
	}, {
		dummy : "obj5"
	} ], false ],
	scenario2 : [ [ {
		dummy : "obj5"
	}, {
		dummy : "obj6"
	} ], false ]

}, function(obj1, obj2){

	if(!obj1 && !obj2)
		return true;
	if(!obj1 || !obj2)
		return false;

	if(typeof obj1 !== typeof obj2)
		return false;

	for (key in obj1){
		if(!obj1.hasOwnProperty(key))
			continue;

		var o1=obj1[key];
		var o2=obj2[key];

		// Can happen if key exists in object 1 but not in object 2 :
		if(!o1 && !o2)
			continue;
		if(!o1 || !o2)
			return false;
		if(typeof o1 !== typeof o2)
			return false;
		if(isArray(o1) && isArray(o2)){
			if(!arraysEqual(o1, o2))
				return false;
		}
		if(o1 !== o2)
			return false;

	}

	return true;
}, !PERFORM_TESTS_ON_LIBRARY);




window.getTextExtract=function(charactersNumberBefore, charactersNumberAfter, str, word){
	var DOTS_STRING="(...)";
	var HTML_HIGHLIGHT_TYPE="b";

	if(nothing(str))
		return "";

	var wordIndex=str.toLowerCase().indexOf(word.toLowerCase());
	if(wordIndex == -1 || charactersNumberBefore<0 || charactersNumberAfter<0){
		return str.substring(0, Math.abs(charactersNumberBefore) + Math.abs(charactersNumberAfter));
	}

	var startIndex=Math.max(0, wordIndex - charactersNumberBefore);
	var endIndex=Math.min(str.length, wordIndex + word.length + charactersNumberAfter);

	var originalWordFromContent=str.substring(wordIndex, wordIndex + word.length);
	var r=new RegExp(word, "im");
	return (startIndex <= 0 ? "" : DOTS_STRING)
			+ str.substring(startIndex, endIndex).replace(r, "<" + HTML_HIGHLIGHT_TYPE + ">" + originalWordFromContent + "</" + HTML_HIGHLIGHT_TYPE + ">")
			+ (endIndex >= str.length ? "" : DOTS_STRING);
}



//==================== Algebric methods ====================


Math.getAverageValues=function(bruteValues,/*OPTIONAL*/numberOfIterationsParam,/*OPTIONAL*/forceRoundingParam
// ,TODO : /*OPTIONAL*/sampleSizeParam
){
	var result=copy(bruteValues);
	// TODO : var sampleSizeParam=sampleSizeParam?sampleSizeParam:1;
	var numberOfIterations=numberOfIterationsParam ? numberOfIterationsParam : 1;
	var forceRounding=forceRoundingParam ? forceRoundingParam : false;

	for (var iterations=0; iterations<numberOfIterations; iterations++){

		resultForIteration=new Array();
		var oldVal=null;
		for (var i=0; i<result.length; i++){
			var val=result[i];
			if(oldVal){
				var average=(val + oldVal) / 2;
				if(forceRounding)
					average=Math.round(average);
				resultForIteration.push(average);
			}
			oldVal=val;
		}
		result=copy(resultForIteration);
	}
	return result;
};

Math.getRandomDice=function(numerator,denominator){
	var diceInt=Math.getRandomInt(denominator);
	return diceInt<=numerator;
};

Math.getRandomInt=function(ceil,/*OPTIONAL*/floor){// FYI : ceil>floor
	// (and not the contrary !)
	if(!floor && floor!==0){
		// (random integer values from 1 to ceil)
		return Math.floor(Math.random() * ceil) + 1;
	}
	// (random integer values from floor to ceil)
	return Math.floor(Math.random() * ((ceil-floor) + 1) ) + floor;
};
Math.getRandom=function(ceil,/*OPTIONAL*/floor){// FYI : ceil>floor
	// (and not the contrary !)
	if(!floor && floor!==0){
		// (random integer values from 1 to ceil)
		return (Math.random() * ceil) + 1;
	}
	// (random integer values from floor to ceil)
	return (Math.random() * ((ceil-floor) + 1) ) + floor;
};
Math.getRandomBool=function(){
	return (1<=(Math.random()*2));
};


Math.getRandomIntAroundValue=function(target, area,/*OPTIONAL*/exclusionParam){
	// TODO : FIXME : On today, exclusion param is always ignored...:
	// var exclusion=exclusionParam;
	// if(!exclusionParam) exclusion=0;
	// return Math.floor((Math.random()-0.5)*(area-exclusion)+target+exclusion);
	return Math.floor((Math.random() - 0.5) * area + target);
};


//// TODO : Develop...
//Math.cycleInRange=function(value, min, max){
//	//TODO : Develop...
//}


Math.coerceInRange=window.aotest({
	name : "coerceInRange",
	scenario1 : [ [ 70, 2, 6 ], 6 ],
	scenario2 : [ [ -1, 0, 10 ], 0 ],
	scenario3 : [ [ -1, 0, 10, true ], 1 ],
	scenario4 : [ [ -1, 0, 0 ], 0 ],
	scenario5 : [ [ -1, 10, 0 ], 0 ]
}, function(value, min, max,/*OPTIONAL*/checkAbsoluteValueOnly){
	if(min === max)
		return min;
	if(max<min){
		var temp=min;
		min=max;
		max=temp;
	}
	if(checkAbsoluteValueOnly){
		min=Math.abs(min);
		max=Math.abs(max);
		value=Math.abs(value);
	}
	return Math.min(Math.max(value, min), max);
}, !PERFORM_TESTS_ON_LIBRARY);


Math.getMinMax=function(strMinMaxRange, separator="->"){
	if(strMinMaxRange && isString(strMinMaxRange) && contains(strMinMaxRange,separator)){
		let splits=strMinMaxRange.split(separator);
		let min=parseInt(splits[0]);
		let max=parseInt(splits[1]);
		return {min:min, max:max};
	}
	return null;
}


Math.isInMinMax=function(minMaxRange,value,mode="][",separator="->"){
	let minMax=null;
	
	if(minMaxRange){
		if(isString(minMaxRange) && contains(minMaxRange,separator)){
			minMax=Math.getMinMax(minMaxRange,separator);
		}else if(minMaxRange.min!=null && minMaxRange.max!=null){
			minMax=minMaxRange;
		}
	}

	if(!minMax)	return false;
	if(minMax.min>minMax.max)	return false;
	if(minMax.min===minMax.max && value===minMax.min)	return true;
	if(mode==="]]")				return (minMax.min<value && value<=minMax.max);
	else if(mode==="[]")	return (minMax.min<=value && value<=minMax.max);
	else if(mode==="[[")	return (minMax.min<=value && value<minMax.max);
	else /*mode==="]["*/	return (minMax.min<value && value<minMax.max);

	return false;
}


Math.getRandomInRange=function(str, separator="->"){
	if(str && isString(str) && contains(str,separator)){
		let splits=Math.getMinMax(str,separator);
		let minValue=splits.min;
		let maxValue=splits.max;
		
		// TODO : FIXME : On today, it only supports integer values :
		return Math.getRandomInt(maxValue,minValue);
	}
	return null;
}

Math.roundTo=(n, numberOfDecimals)=>{
	if(!numberOfDecimals)	return Math.round(n);
	let decimalFactor=Math.pow(10,numberOfDecimals);
	let result=Math.round(n*decimalFactor);
	return (result/decimalFactor);
}

Math.demiCurve=function(min, max, value, strategy="triangle", precision=2){
	// (Only the part 1 -ascending- of the curve)
	let delta=max-min;
	if(delta<=0)	return 1;
	if(value<=min)	return 0;
	if(max<=value)	return 1;
	let cursorFactor=(value-min)/delta;
	return Math.roundTo(cursorFactor,precision);
}

// Not the same as curve, where min and max are the minimum and maximum values taken by returned value : 
Math.curveTriggered=function(minTriggerFactor, maxTriggerFactor, valueFactorParam, strategy="linear"){
	const valueFactor=Math.coerceInRange(valueFactorParam,0,1);
	let result=1;
	if(valueFactor<minTriggerFactor){
		// Ascending slope :
		if(minTriggerFactor==0)	return 1;
		result=valueFactor/minTriggerFactor;
	}else{
		if(maxTriggerFactor==0)	return 1;
		if(maxTriggerFactor<valueFactor){
		// Descending slope :
			result=1-((valueFactor-maxTriggerFactor)/(1-maxTriggerFactor));
		}else{
			// Plateau :
			return 1;
		}
	}
	return result;
}

Math.slopeTriggered=function(minTriggerFactor, maxTriggerFactor, valueFactorParam, direction="ascending", strategy="linear"){
	
	const valueFactor=Math.coerceInRange(valueFactorParam,0,1);
	
	if(valueFactor<minTriggerFactor){
		// Beginning plateau : 
		if(direction=="ascending")	return 0;
		else												return 1;
	}else if(maxTriggerFactor<valueFactor){
		// Ending plateau : 
		if(direction=="ascending")	return 1;
		else												return 0;
	}
	
	// Intermediate slope :
	const total=(maxTriggerFactor-minTriggerFactor);
	if(total==0)	return ((direction=="ascending")?1:0);
	
	const factor=((valueFactor-minTriggerFactor)/total);
	
	return ((direction=="ascending")?factor:(1-factor));
}



Math.curve=function(min, max, value, strategy="triangle",
		// It is actually where to place the «summit» of the curve, before returning to 0,
		// the factor value where the values starts decreasing, after having increased all along, and then goes back to 0:
		curveSummitPositionFactor=null, precision=2){
	
	let delta=max-min;
	if(delta<=0)	return 1;

	const MIDDLE_FACTOR=0.5;
	
	let cursorFactor=(value-min)/delta;
	
	let result=1;
	if(strategy==="triangle"){
		
		curveSummitPositionFactor=nonull(curveSummitPositionFactor, 0.5);
		
		let goBackValue=(curveSummitPositionFactor*delta)+min;
		if(cursorFactor<curveSummitPositionFactor){ // part 1 of the curve (ascending)
			if(goBackValue==min)	result=0;
			else									result=( 	 (value-min)/(goBackValue-min) );
		}else{ // part 2 of the curve (descending) 
			if(goBackValue==max)	result=1;
			else									result=( 1- (value-goBackValue)/(max-goBackValue) );
		}
		
	} else if(strategy==="trapeze"){ // In this case, then we stay at max value for a segment, not a single point value like in the triangle or cosinus strategy :
		
		curveSummitPositionFactor=nonull(curveSummitPositionFactor, 0.25);

		let curveSummitPositionFactorMin=curveSummitPositionFactor;
		let curveSummitPositionFactorMax=1-curveSummitPositionFactor;

		let goBackValueMin=(curveSummitPositionFactorMin*delta)+min;
		let goBackValueMax=(curveSummitPositionFactorMax*delta)+min;

		if(cursorFactor<curveSummitPositionFactorMin){ // part 1 of the curve (ascending) (SAME CODE)
			if(goBackValueMin==min)	result=0;
			else										result=( 	 (value-min)/(goBackValueMin-min) );
		}else{
			
			if(cursorFactor<curveSummitPositionFactorMax){ // part flat of the curve (staying at max)

				result=1;
				
			}else{ // part 2 of the curve (descending) (SAME CODE)
				if(goBackValueMax==max)	result=1;
				else										result=( 1- (value-goBackValueMax)/(max-goBackValueMax) );
			}
		}
		
		
	}else{ // «cosinus» strategy, (handles both parts of the curve)
		curveSummitPositionFactor=nonull(curveSummitPositionFactor, 0.5);

		result=Math.cos((cursorFactor-(curveSummitPositionFactor-MIDDLE_FACTOR))*Math.PI);
	}
	
	// NO : VALUE : return Math.roundTo(((result*(max-min))+min),precision);
	// YES : FACTOR :
	return Math.roundTo(result,precision);
}

//// DBG
//lognow(Math.curve(0, 1000, 1, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 10, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 20, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 50, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 90, "trapeze", 0.1));
//lognow("-----")
//lognow(Math.curve(0, 1000, 101, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 500, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 800, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 900, "trapeze", 0.1));
//lognow("-----")
//lognow(Math.curve(0, 1000, 901, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 910, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 950, "trapeze", 0.1));
//lognow(Math.curve(0, 1000, 999, "trapeze", 0.1));

Math.goToValue=function(startValue,endValue,percentFactor){
	if(startValue==endValue || percentFactor<0 || 1<percentFactor)	return startValue;
	if(percentFactor<=0)	return startValue;
	if(1<=percentFactor)	return endValue;
	return startValue+(endValue-startValue)*percentFactor;
}


// Types management :
window.isNumber=function(n, strictMode=false){
	if(typeof(n)==="object")	return false;
	if(!strictMode)	return !isNaN(parseFloat(n)) && isFinite(n);
	return !isNaN(n);
}
window.isString=function(s){
	return typeof(s)==="string";
}
window.isBoolean=function(b){
	return typeof b === "boolean";
}
window.isFunction=function(functionToCheck){
	return (functionToCheck && {}.toString.call(functionToCheck) === "[object Function]");
}
window.isPrimitive=function(p){
	return (isNumber(p) || isString(p) || isBoolean(p));
}
window.isObject=function(p){
	return !isFunction(p) && !isPrimitive(p);
}
window.getClassName=function(obj){
	if(!obj)	return null;
	if(!obj.constructor)	return null;
	return obj.constructor.name;
}
window.isNativeClass=function(obj){
	let className=getClassName(obj);
	if(!obj)	return true;
	return isNativeClassName(className);
};
window.isNativeClassName=function(className){
	return (className==="Boolean" || className==="Number" 
		 || className==="String"	|| className==="Function"
		 || className==="RegExp"  || className==="Date"
		 || className==="Array" 	|| className==="Object");
}




// Algebra
Math.makeMultipleOf=function(bruteValue, multiple,/*OPTIONAL*/roundingMode){
	if(roundingMode === "floor")
		return Math.floor(bruteValue / multiple) * multiple;
	if(roundingMode === "ceil")
		return Math.ceil(bruteValue / multiple) * multiple;
	return Math.round(bruteValue / multiple) * multiple;
}




getTimer=function(callback, delay){
	return new Timer(callback, delay);
//	let self={
//		 timerId:null,
//		 start:null,
//		 remaining:delay,
//		 callback:callback,
//		 pause:function(){
//		      window.clearTimeout(self.timerId);
//		      self.remaining -= Date.now() - self.start;
//		 },
//		 resume:function(){
//			 self.start=Date.now();
//			 window.clearTimeout(self.timerId);
//			 self.timerId=window.setTimeout(self.callback, self.remaining);
//		 },
//		 clear:function(){
//		    window.clearTimeout(self.timerId);
//		 },
//	};
//	self.resume();
//	return self;
};


//Usage :
//var timer=new Timer(function(){
//alert("Done!");
//}, 1000);
//
//timer.pause();
////Do some stuff...
//timer.resume();
var Timer=function(callback, delay){
  var timerId, start, remaining=delay;

  this.pause=function(){
      window.clearTimeout(timerId);
      remaining -= Date.now() - start;
  };

  this.resume=function(){
      start=Date.now();
      window.clearTimeout(timerId);
      timerId=window.setTimeout(callback, remaining);
  };

  this.clear=function(){
    window.clearTimeout(timerId);
  };
  
  this.resume();
};




//=================================================================================


getUUID=function(mode=null){
	
  let uuid="";
  if(!mode){
	  for (let i=0; i<32; i++){
	    let random=Math.random() * 16 | 0;
	    if(i == 8 || i == 12 || i == 16 || i == 20){
	      uuid += "-"
	    }
	    uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
	  }
  }else if(mode==="short"){
	  for (let i=0; i<8; i++){
	    let random=Math.random() * 16 | 0;
	    uuid += random.toString(16);
	  }
  	
  }
  return uuid;
}


// JSON
window.instanciate=function(className=null){
	let newObj={};
	
	if(!className || !isString(className))	return newObj;

	if(className.startsWith("HTML")){
		if(typeof(document)!=="undefined"){
			let elementName=className.replace("HTML","").replace("Element","").toLowerCase();
			return document.createElement(elementName);
		}else{
			return newObj;
		}
	}
	// If class name is a simple object, a simple «{}» is enough :
	if(className==="Object")	return newObj;

	try{
		try{
			newObj=Reflect.construct(window[className]);
		}catch(e1){
		
//			// TRACE
//			console.log("WARN : Could not instanciate class «"+className+"». (Maybe class is undefined or default constructor doesn't exist !)");
			
			// TODO : FIXME : I don't like that at all, to use eval(...), but on today I know of no other solution... :
			newObj=eval("new "+className+"();");
			
		}
	}catch(e2){
		
//		// TRACE
//		console.log("ERROR : Could not instanciate class «"+className+"» with eval. (Maybe class is undefined or default constructor doesn't exist !)");
		
		
		const classInWindow=window[className];
		
		try{

//		// TODO : FIXME : I don't like that at all, to use eval(...), but on today I know of no other solution... :
//		newObj=eval("new window."+className+"();");

			newObj=Reflect.construct(classInWindow);

		
		}catch(e3){
		
//			// TRACE
//			console.log("ERROR : Could not instanciate class «"+className+"» with eval and «window.». (Maybe class is undefined or default constructor doesn't exist !)");
//			// TRACE
//			console.log("ERROR : Could not instanciate class «"+className+"» with Reflect and «window.». (Maybe class is undefined or default constructor doesn't exist !)");

			
			// TODO : FIXME : This is the most performance-costing fallback :
			try{
				
				if(classInWindow){

					newObj.__proto__=classInWindow.prototype;
					
				}else{
				
					// TODO : FIXME : I don't like that at all, to use eval(...), but on today I know of no other solution... :
					newObj.__proto__=eval(className+".prototype");
				}
				
			}catch(e4){
				
//				// TRACE
//				console.log("ERROR : Could not instanciate class «"+className+"» with prototype affectation.");
//				console.log("ERROR : Returning plain object since all instantiation methods have failed for class «"+className+"».");
				
			}

			
		}
	}
	
	return newObj;
};







JSON.stringifyDecycle=function(obj){
	let decycled=JSON.decycle(obj,null,"$className");
	// CANNOT USE stringifyObject(...) function because we are in a common, lower-level library !
	return stringifyObject(decycled);
}


JSON.parseRecycle=function(str){
	
	const parsedDecycled=parseJSON(str);
	let restoreClass=function(objParam){
		
		if(!objParam || isPrimitive(objParam))	return objParam;

		let newObj;
		if(isArray(objParam)){
			newObj=[];
		}else{
			let className=objParam["$className"];
			if(!className || isNativeClassName(className)){
				newObj= {};
			}else{
				newObj=instanciate(className);
			}
		}
		
		// Handles both array and object, and that's beautiful :
		foreach(objParam,(itemOrAttr,k)=>{
			newObj[k]=restoreClass(itemOrAttr);
		});
		
		return newObj;
	};
	let newObjDecycled=restoreClass(parsedDecycled);
	
	
	const copyAttributes=function(source, destination){

		// Handles both array and object, and that's beautiful :
		foreach(source,(itemOrAttr,k)=>{
			
			if(!itemOrAttr || isPrimitive(itemOrAttr)){
				destination[k]=itemOrAttr;
			}else{ // case array, simple object or class object :
				copyAttributes(itemOrAttr, destination[k]);
			}
			
		}
		// Should not be useful :
		,(itemOrAttr)=>{		return !isFunction(itemOrAttr);	}
		);
		
		if(destination["$className"]){
			delete destination["$className"];
		}
		
	};
	// DBG
	copyAttributes(parsedDecycled,newObjDecycled);
	
//	// DBG
//	lognow("----------------------------");
//	lognow("after copyAttributes newObjDecycled["+newObjDecycled["$uuid"]+"]:",newObjDecycled);
//	lognow("----------------------------");
	
	let recycled=JSON.recycle(newObjDecycled);
	return recycled;
}


window.clone=function(obj){
	const str=JSON.stringifyDecycle(obj);
	const newObj=JSON.parseRecycle(str);
	return newObj;
};




//****************************************************************************************//
// KNOWN LIMITATIONS : - Only deep-copies ARRAY-TYPE ONLY attributes up to max 99 dimentions !
//										 - Not a real deep-copy
//****************************************************************************************//
// OLD AND BOGUS (SERIOUSLY, DON'T USE IT ANYMORE !) :
//// DEPRECATED use structuredClone(...) instead :
//window.cloneObjectShallow=function(obj, stateOnly=false, className=null, primitiveAttributesOnly=false
//		// If you have an array of more than 99 dimensions, then you have quite a serious problem (and in your code, too).
//		,maxDeepness=99){
//	
//	var newObj=null;
//
//	if(primitiveAttributesOnly){
//
//		// We try to set the right class :
//		newObj=instanciate(className);
//		
//		// Primitive attributes means never objects, array or function attributes, as the name smartly suggests :
//		foreach(obj,(attr,attrName)=>{
//			
//			newObj[attrName]=attr;
//		},(attr)=>{	return isPrimitive(attr); });
//		
//		return newObj;
//	}
//	
//	
////	// Case only attributes :
////	if(stateOnly){
//
//	// We try to set the right class :
//	newObj=instanciate(className);
//	
//	
//	foreach(obj,(attr,attrName)=>{
//		if(attr==null){
//			newObj[attrName]=null;
//		}else{
//			
//			if(isArray(attr)){
//				
//				newObj[attrName]=[];
//
//				foreach(attr,(item,i)=>{
//					
//					if(isArray(item)){
//						
//						if(0<maxDeepness){
//							let newItem=cloneObjectShallow(item, stateOnly, getClassName(item), primitiveAttributesOnly, maxDeepness-1);
//							newObj[attrName].push(newItem);
//						}
//						
//					}else{
//						newObj[attrName].push(item);
//					}
//				
//				},(item)=>{return item==null 
//					// We only copy the primitive objects,
//					// and the arrays objects :
//					|| !isObject(item)
//					|| isArray(item);
//				} );
//				
//			}else if(isObject(attr)){
//			
//				// THIS IS NOT A DEEP COPY !!:
//				newObj[attrName]=Object.assign({},attr);
//				
//				if(stateOnly){
//					foreach(newObj[attrName],(a,k)=>{
//						delete newObj[attrName][k]; 
//					},(a)=>{ return isFunction(a); });
//				
//				}
//
//			}else if((isFunction(attr) && !stateOnly) || isPrimitive(attr)){ // If we have state only and we have a function, we DO NOT copy it :
//				
//				newObj[attrName]=attr;
//				
//			}
//		
//		}
//		// (CAUTION : not a real deep-copy)
//	});
//	
//	return newObj;
////	}
//
////	// Case attributes + functions :
////	
////	// We try to set the right class :
////	if(className){
////		try{
////			// TODO : FIXME : I don't like that at all, to use eval(...), but on today I know of no other solution... :
////			newObj=eval("new "+className+"();");
////		}catch(e){
////			// (CAUTION : not a real deep-copy)
////			// Will (not-really deeply) copy the functions too :
////			newObj=Object.assign({},obj);
////		}
////	}else{
////		// (CAUTION : not deep-copy)
////		// Will (not-really deeply) copy the functions too :
////		newObj=Object.assign({},obj);
////	}
////
////
////	return newObj;
//};


window.isProperAttributeName=function(key){
	return key!==DEFAULT_UUID_ATTR_NAME && key!==DEFAULT_CLASSNAME_ATTR_NAME;
}


////OLD
//window.areEquivalentFlatMaps=function(o1,o2,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME){
//
//if((o1!=null)!==(o2!=null))	return false;
//
//let s1="";
//let s2="";
//
//// We ignore uuid keys :
//foreach(o1,(entry)=>{
//s1+=getSignatureOnNonCycledObject(entry,false,ignoredAttributes)+",";
//});
//foreach(o2,(entry)=>{
//s2+=getSignatureOnNonCycledObject(entry,false,ignoredAttributes)+",";
//});
//
//return s1===s2;
//}


window.areEquivalentFlatMaps=function(o1,o2,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME){

	if((o1!=null)!==(o2!=null))	return false;

	// We ignore uuid keys :
	const ignoredAttributes=[UUID_ATTR_NAME,POINTER_TO_ATTR_NAME];


	if(Object.keys(o1).length!==Object.keys(o2).length){
		return false;
	}

	let areEquivalent=true;

	
	// We do not use the getSignatureOnFlatStructure() function, 
	// to save some executions on the 2nd pass signatures computing :
	
	let signatures1=[];
	
	
	foreach(Object.keys(o1),(key)=>{
		
		let entry=o1[key];
		
		let s1=hashSafe(getSignatureOnNonCycledObject(entry,false,ignoredAttributes));
		signatures1.push(s1);
		
	},()=>{	return true; }
	,(uuid1,uuid2)=>{ return uuid1.localeCompare(uuid2) ; });
	
	
	
	foreach(Object.keys(o2),(key)=>{
		
		let entry=o2[key];

		let s2=hashSafe(getSignatureOnNonCycledObject(entry,false,ignoredAttributes));

		if(!contains(signatures1,s2)){
			
			areEquivalent=false;
			return "break";
		}
		
	},()=>{	return true; }
	,(uuid1,uuid2)=>{ return uuid1.localeCompare(uuid2) ; });

	
	return areEquivalent;
}


window.overlaps=(delimitedObj1,delimitedObj2)=>{
	return (delimitedObj2.start<=delimitedObj1.start && delimitedObj1.start<=delimitedObj2.end)
			|| (delimitedObj2.start<=delimitedObj1.end   && delimitedObj1.end  <=delimitedObj2.end);
}


window.findCommonChunks=(str1,str2,minimalLength=2,invertLogic=false,characterFilterFunction=null)=>{
	const results=[];
	
	if(empty(str1))	return results;
	if(empty(str2))	return results;
	

	// 1) We build a binary correspondances matrix :
	
	const correspondancesMatrix=[];
	
	for(let i=0;i<str1.length;i++){
		
		const currentCharAtRow=str1.charAt(i);
		
		const row=[];
		for(let j=0;j<str2.length;j++){
			
				const currentCharAtColumn=str2.charAt(j);

				if(	currentCharAtColumn==currentCharAtRow 
					&& (!characterFilterFunction || characterFilterFunction(currentCharAtColumn)))
					row.push(true && !invertLogic);
				else
					row.push(false || invertLogic);
			
		}
		correspondancesMatrix.push(row);
	}
	
	// 2) We try to walk through the diagonal of each found correspondance to create each chunk, and removing those already walked along the way :
	// This way we build the raw chunks dictionnary :
	for(let i=0;i<correspondancesMatrix.length;i++){

		for(let j=0;j<correspondancesMatrix[i].length;j++){

			if(!correspondancesMatrix[i][j])	continue;
			
			let newChunk=null;
			
			// Proximity algorithm : Here it's just a simple diagonal walking
			// (for blobs in a 2D image detection, instead of hunks in a string detection, it would be a recursive algorithm !)
			let k;
			for(k=0;(i+k)<correspondancesMatrix.length && (j+k)<correspondancesMatrix[i].length;k++){
				
				if(!correspondancesMatrix[i+k][j+k])		break;
				
				if(k==0){
					newChunk=str1[i+k];
				}else{
					newChunk+=str1[i+k];
				}
				correspondancesMatrix[i+k][j+k]=false;
			}
			if(newChunk && minimalLength<=newChunk.length){
				results.push(
							[
								{start:i+k-newChunk.length,end:i+k-1,value:newChunk,length:newChunk.length},
								{start:j+k-newChunk.length,end:j+k-1,value:newChunk,length:newChunk.length}
							]
						);
			}
			
		}
	}
	
	
	// 3) We filter all the overlapping chunks definitions, meaning when chunks overlap, only the biggest is kept !
	
	const refinedResults=[];
	for(let i=0;i<results.length;i++){
		let chunk1=results[i];
		
		for(let j=0;j<results.length;j++){
			let chunk2=results[j];
		
			if(overlaps(chunk1[0],chunk2[0]))	continue;
			
			let chosenChunk=chunk1;
			if(chunk1[0].length<chunk2[0].length)	chosenChunk=chunk2;
			
			refinedResults.push(chosenChunk);
		
		}
	}
	
	// (we need to do the adding in both directions :)
	for(let i=0;i<results.length;i++){
		let chunk1=results[i];
		
		for(let j=0;j<results.length;j++){
			let chunk2=results[j];
		
			if(overlaps(chunk1[1],chunk2[1]))	continue;
			
			let chosenChunk=chunk1;
			if(chunk1[1].length<chunk2[1].length)	chosenChunk=chunk2;
			
			refinedResults.push(chosenChunk);
		
		}
	}
	
	
	return refinedResults;
}



window.areEquivalentSimple=function(obj1Param,obj2Param, caseSensitive=true){
	const str1=(isString(obj1Param)?obj1Param:stringifyObject(obj1Param));
	const str2=(isString(obj2Param)?obj2Param:stringifyObject(obj2Param));
	if(!caseSensitive)
		return (str1.toLowerCase()==str2.toLowerCase());
	return (str1===str2);
}

// CAUTION : WAY SLOWER...(for simple objects, use areEquivalentSimple(...) instead):
window.areEquivalent=function(obj1Param,obj2Param,flattenBeforeCompute=false, UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME){

	if((obj1Param!=null)!==(obj2Param!=null) || typeof(obj1Param)!==typeof(obj2Param))	return false;
	if(obj1Param===null && obj2Param===null)	return true;
	
	let isArrayObj1=isArray(obj1Param);
	let isArrayObj2=isArray(obj2Param);
	if(isArrayObj1!==isArrayObj2)	return false;
	
	let obj1, obj2;
	if(flattenBeforeCompute){
	// NO : Because the UUIDs generation can alter the result !
//		obj1=getAsFlatStructure(obj1Param,true);
//		obj2=getAsFlatStructure(obj2Param,true);
		// YES :
			obj1=JSON.decycle(obj1Param);
			obj2=JSON.decycle(obj2Param);
	}else{
			obj1=obj1Param;
			obj2=obj2Param;
	}
	
	//We ignore uuid keys :
	const ignoredAttributes=[UUID_ATTR_NAME,POINTER_TO_ATTR_NAME];

	let s1=getSignatureOnNonCycledObject(obj1,false,ignoredAttributes);
	let s2=getSignatureOnNonCycledObject(obj2,false,ignoredAttributes);

	// DBG
	//lognow("+++++++++++++++++SIGNATURE 1:",s1);
	//lognow("+++++++++++++++++SIGNATURE 2:",s2);
	
	return (s1===s2);
}


window.calculateDelta=function(obj1,obj2,
		// TODO : Develop : ,respectAttributesOrder=false,
		flattenBeforeCompute=false
		,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME
		,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME){

	return calculateProtoDelta(obj1,obj2,flattenBeforeCompute,UUID_ATTR_NAME,POINTER_TO_ATTR_NAME);
}


window.calculateProtoDelta=function(obj1,obj2,
		// TODO : Develop : ,respectAttributesOrder=false,
		flattenBeforeCompute=false
		,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME
		,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME
		,LABEL1="1"
		,LABEL2="2"
	){
	

	// Delta will come in the form of :
	// modifications in obj2 : 
	// { 
	// 		anyObj1Attr: {"1`:<obj1 attribute content>} (if only present in obj1) ,
	// 		anyObj2Attr: {"2":<obj2 attribute content>} (if only present in obj2) ,
	//		<nothing> (if present in both AND are IDENTICAL) ,
	//		anyAttr:{ (if present in both AND are DIFFERENT)
	//			"1":<obj1 attribute content>,
	//			"2":<obj2 attribute content>
	//		},
	// }
	let result={};

	if(flattenBeforeCompute){
		obj1=getAsFlatStructure(o1,true);
		obj2=getAsFlatStructure(o2,true);
	}
	

	foreach(obj1,(attr,attrName)=>{
		
		if(attrName===UUID_ATTR_NAME 
			|| (attr && typeof(attr[POINTER_TO_ATTR_NAME])!=="undefined") )	return "continue";
		let attrInObj1=attr;
		let attrInObj2=obj2[attrName];
		if(typeof(attrInObj2)==="undefined"){
			// (only present in obj1)
			let newAttr={};
			newAttr[LABEL1]=attrInObj1;
			result[attrName]=newAttr;
		}else{
			if(areEquivalent(attrInObj1,attrInObj2,flattenBeforeCompute)){
				// (present in both AND are IDENTICAL)
				// DO NOTHING
			}else{ // (present in both AND are DIFFERENT
				let newAttr={};
				newAttr[LABEL1]=attrInObj1;
				newAttr[LABEL2]=attrInObj2;
				result[attrName]=newAttr;
			}
		}
		
	});
	
	foreach(obj2,(attr,attrName)=>{
		
		if(attrName===UUID_ATTR_NAME 
			|| (attr && typeof(attr[POINTER_TO_ATTR_NAME])!=="undefined")
			|| typeof(result[attrName])!=="undefined")											return "continue";
		
		let attrInObj1=obj1[attrName];
		let attrInObj2=attr;
		if(typeof(attrInObj1)==="undefined"){
			// (only present in obj2)
			let newAttr={};
			newAttr[LABEL2]=attrInObj2;
			result[attrName]=newAttr;
		}
	});
	
	// // DBG
	// console.log("result!!!",result);
	
	return result;
};

// CAUTION : will cause an INFINITE LOOP if the object contains a CYCLE !
/*private*/window.getSignatureOnNonCycledObject=function(obj,isHashed=true,attributesNamesToIgnore=null){
	var result="";
	if(obj==null)	return "null";
	if(isPrimitive(obj))	return ""+obj;
	
	if(isArray(obj)){
		
		result+="[";
		foreach(obj,(item)=>{
			result+=getSignatureOnNonCycledObject(item,isHashed,attributesNamesToIgnore)+",";
		});
		result+="]";
	
	}else{

		foreach(Object.keys(obj),(key)=>{
			
			let attr=obj[key];
			
			
			if(attributesNamesToIgnore && contains(attributesNamesToIgnore,key))	return "continue";
			
			result+=key+":";
			if(isArray(attr)){
	
				result+="[";
	
				foreach(attr,(item)=>{
					
					if(isPrimitive(item)){
						result+=item;
					}else{
//					OLD : result+=(item==null?"null":(typeof(item)));
						// CAUTION : will cause an INFINITE LOOP if the object contains a CYCLE !
						result+=(item==null?"null":(getSignatureOnNonCycledObject(item,isHashed,attributesNamesToIgnore)));
					}
					result+=",";
	
				});
	
				result+="],";
				
			}else{
				
				if(isPrimitive(attr)){
					result+=attr;
				}else{
//				OLD : result+=(attr==null?"null":(typeof(attr)));
					// CAUTION : will cause an INFINITE LOOP if the object contains a CYCLE !
					result+=(attr==null?"null":(getSignatureOnNonCycledObject(attr,isHashed,attributesNamesToIgnore)));
				}
				result+=",";
	
			}
			
		}, ()=>true, 
		// We must sort the object keys to be sure the comparison is order-insensitive !
		(key1,key2)=>key1.localeCompare(key2));

		
	}

	return (isHashed?hashSafe(result):result);
};

// CAUTION : will cause an INFINITE LOOP if the object contains a CYCLE !
window.getSignatureOnFlatStructure=function(objFlat,isHashed=false,attributesNamesToIgnore=null){
	
	let result="";
	
	foreach(Object.keys(objFlat),(uuid)=>{
		
		let obj=objFlat[uuid];
		result+=getSignatureOnNonCycledObject(obj,isHashed,attributesNamesToIgnore);

	},()=>{	return true; }
	,(uuid1,uuid2)=>{ return uuid1.localeCompare(uuid2) ; });

	return result;
};




window.clearAttribute=function(tree, attrName, maxLevel=null){

	if( (maxLevel!=null && maxLevel<=0) || tree==null || !isObject(tree) 
//			&& !isArray(tree)
			)	return;
	
	if(tree && tree[attrName])	
		delete tree[attrName];
	
	foreach(tree, (subTree)=>{
		clearAttribute(subTree, attrName, (maxLevel==null?null:(maxLevel-1)));
	});
	
};




// =========================================================================
// FLATTEN / UNFLATTEN JSON
// =========================================================================



// Flattening :

window.isFlatMap=function(obj){
	return typeof(obj[ROOT_UUID])!=="undefined" && obj[ROOT_UUID]!=null;
};

//CAUTION : KNOWN LIMITATIONS : - Does not handle root-level arrays flattening !
//															- Does not handle anonymous objects functions and methods population !
window.getAsFlatStructureImpl=function(rawObject, stateOnly=false
		,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME/* Yet Another UUID */
		,CLASSNAME_ATTR_NAME=DEFAULT_CLASSNAME_ATTR_NAME/* Yet Another Classname */
		,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME/* Yet Another PointerTo */
		){
	
		
		// CAUTION «()=>{}» syntax does not work with methods referring «this» in attributes or methods!
//	window.objectsRegistry={
		var objectsRegistry={
			map:{}, // This map will point to the original objects
			registerIfNeeded:function(obj){
				if(this.isRegistered(obj))	return false;
				this.registerNewObject(obj);
				return true;
			},
			/* private */isRegistered:function(obj){
				let uuid=obj[UUID_ATTR_NAME];
				if(!uuid)	return false;
				let foundObject=this.map[uuid];
				return typeof(foundObject)!=="undefined" && foundObject;
			},
			/* private */registerNewObject:function(obj){
				let newUUID=(empty(this.map) ? ROOT_UUID : getUUID());
				obj[UUID_ATTR_NAME]=newUUID;
				this.map[newUUID]=obj;
			}
		};
//	}

	// var objectsRegistry=window.objectsRegistry;
	// // We reinit the registry map :
	// objectsRegistry.map={};
	
	// 1) First, we sort up the original structure in the registry and give them all uuids :
	registerObject(objectsRegistry,rawObject, UUID_ATTR_NAME
//			,CLASSNAME_ATTR_NAME
			);
	

//	 // DBG
//	 console.log("¬¬¬¬objectsRegistry.map",objectsRegistry.map);
	
	
	// 2) Second, we flatten the javascript structure.
	let workingOriginalObjectsMap=objectsRegistry.map;

	let resultMap={};

	// We browse through the registry tree map to create the flat new copied structure :
	foreach(workingOriginalObjectsMap,(workingOriginalObject, uuid)=>{
		
		addInFlatMapAndReplaceObjectsWithPointers(resultMap,workingOriginalObject,uuid
				,stateOnly,UUID_ATTR_NAME,CLASSNAME_ATTR_NAME,POINTER_TO_ATTR_NAME);
		
	});

	
	// Then we remove the technical attributes in the original objects :
	foreach(workingOriginalObjectsMap,(workingOriginalObject)=>{
		delete workingOriginalObject[UUID_ATTR_NAME];
	});

	
// CANNOT USE stringifyObject(...) function because we are in a common, lower-level library !
	
	return resultMap;
};


// (recursively called function)
/*private*/window.registerObject=function(objectsRegistry,rawObject,UUID_ATTR_NAME
//		,CLASSNAME_ATTR_NAME
		){

	if(rawObject==null)
		return;
	
	
	if(isPrimitive(rawObject) 
			|| isArray(rawObject)
			){
		return;
	}

	
//	// DBG
//	console.log(">>>REGISTER OBJECT:",rawObject);
	
	
	// We add the object to the registry if it is encoutered for the first time :
	var hasBeenRegistered=objectsRegistry.registerIfNeeded(rawObject);
	
	// If object is already present, then we stop here.
	if(!hasBeenRegistered)	return;
	
	
	// We browse through the subObjects in the structure, to register them :
	foreach(rawObject,(subObject)=>{
		
		
		if(isArray(subObject)){
			
			foreach(subObject,(item)=>{
				
			// We add the object to the registry :
				registerObject(objectsRegistry,item, UUID_ATTR_NAME);
				
			},(item)=>{ return isObject(item); });
			

		}else{
			
			// We add the object to the registry :
			registerObject(objectsRegistry, subObject, UUID_ATTR_NAME);

		}

	}
	// DBG
	,(subObject)=>{ return isArray(subObject) || isObject(subObject); }
);
	
	
	
};

//(recursively called function)
/*private*/window.addInFlatMapAndReplaceObjectsWithPointers=function(resultMap,workingOriginalObject,uuid
		,stateOnly,UUID_ATTR_NAME,CLASSNAME_ATTR_NAME,POINTER_TO_ATTR_NAME){
	
	if(workingOriginalObject==null)
		return;
	
	let isAlreadyInNewMap=(resultMap[uuid]!=null);
	if(isAlreadyInNewMap || workingOriginalObject[POINTER_TO_ATTR_NAME]!=null)
		return;
	
	
	// The new object is added to the flat map :
	// (We don't care to set the real class here, because this object will have a JSONtype attribute in the end. This new result object will be a pure JSON information.)
	// OLD AND BOGUS (SERIOUSLY, DON'T USE IT ANYMORE !) : let result=cloneObjectShallow(workingOriginalObject, stateOnly, null, true);
	let result=structuredClone(workingOriginalObject);

	
	// We set the class name to the new result object :
	let className=getClassName(workingOriginalObject);
	if(className!=="Array"){ // No need to indicate that this JSON object is an array, since "[" indicates it by the JSON language definition.
		result[CLASSNAME_ATTR_NAME]=className;
	}
	
	
//	// DBG
//	console.log("result[CLASSNAME_ATTR_NAME]",result[CLASSNAME_ATTR_NAME]);
	
	
	// We add the result to the flat map :
	resultMap[uuid]=result;

	
	
	// Attributes processing :
	foreach(workingOriginalObject,(workingAttr, attrName)=>{
		
		
		// We replace the actual reference to a pointer to this reference :
		if(isArray(workingAttr)){
			
			result[attrName]=[];

			
			// Array items :
			foreach(workingAttr,(item,i)=>{

				if(item==null){
					result[attrName].push(null);
					return "continue";
				}
				if(isFunction(item)){
					if(stateOnly)	return "continue";
					result[attrName].push(item);
					return "continue";
				}
				
				if(isPrimitive(item)
				|| isArray(item)){

//					// DBG
//					console.log("%%% workingAttr:",workingAttr);
//					console.log("%%% result:",result);
//					console.log("%%% attrName:",attrName);
//					console.log("%%% item:",item);
					
					result[attrName].push(item);
					return "continue";
				}

				
				let attrInArrayUUID=item[UUID_ATTR_NAME];
				if(attrInArrayUUID==null){
					
//					// DBG
//					console.log("isObject(item)",isObject(item));
//					console.log("isString(item)",isString(item));
//					console.log("typeof(item)",typeof(item));
//					console.log("item",item);
					
					
					// TRACE
					console.log("ERROR : Attribute «"+item+"» in array should have a uuid under the member name «"+UUID_ATTR_NAME+"» but has none !" +
							" Complete attribute object :", workingAttr);
					return "continue";
				}


				let simpleRef={};
				simpleRef[POINTER_TO_ATTR_NAME]=attrInArrayUUID;
				
				
				
				result[attrName].push(simpleRef);
				
				
				// We add the result to the flat map :
				addInFlatMapAndReplaceObjectsWithPointers(resultMap,item,attrInArrayUUID
						,stateOnly,UUID_ATTR_NAME,CLASSNAME_ATTR_NAME,POINTER_TO_ATTR_NAME);
				
					
				
			});
			

		}else{
			
			if(workingAttr==null){
				result[attrName]=null;
				return "continue";
			}
			if(isFunction(workingAttr)){
				if(stateOnly)	return "continue";
				result[attrName]=workingAttr;
				return "continue";
			}
			if(!isObject(workingAttr)){
				result[attrName]=workingAttr;
				return "continue";
			}

	
			let attrUUID=workingAttr[UUID_ATTR_NAME];
			if(attrUUID==null){
				// TRACE
				console.log("ERROR : Attribute «"+attrName+"» should have a uuid under the member name «"+UUID_ATTR_NAME+"» but has none !"
							+" Complete attribute object :", workingAttr
						);
				return "continue";
			}
		
			let simpleRef={};
			simpleRef[POINTER_TO_ATTR_NAME]=attrUUID;
		
		
			result[attrName]=simpleRef;

			
			// We add the result to the flat map :
			addInFlatMapAndReplaceObjectsWithPointers(resultMap,workingAttr,attrUUID
					,stateOnly,UUID_ATTR_NAME,CLASSNAME_ATTR_NAME,POINTER_TO_ATTR_NAME);
			
			
		}
		
		
		
	});
	
	

	return result;
	
};


// --------------------------------
// --------------------------------

// Unflattening :


// CAUTION : KNOWN LIMITATIONS : - Does not handle root-level arrays flattening !
//															 - Does not handle anonymous objects functions and methods population !
window.getAsTreeStructureImpl=function(oldMap, stateOnly=false, removeTypeInfo=true
		,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME/* Yet Another UUID */
		,CLASSNAME_ATTR_NAME=DEFAULT_CLASSNAME_ATTR_NAME/* Yet Another Classname */
		,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME/* Yet Another PointerTo */
){
	

	// TODO : FIXME : «stateOnly=false» DOESN'T WORK and only returns an empty object «{}» !
	
	
	// We have in input a map of the following form :
	// { ("a uuid" : {(non-object attributes key-value pairs)*, (object attributes
	// as : {<POINTER_TO_ATTR_NAME> : "a uuid"} )*}) }

	if(typeof(oldMap)==="string"){
		oldMap=parseJSON(oldMap);
//	oldMap=parseJSON(oldMap.replace(/"/gim,"'"));
//	oldMap=eval(oldMap);
	}
	
	
	let resultMap={};
	
	// First we clone the existing flat map :
	foreach(oldMap,(oldObj,uuid)=>{
		
		let className=oldObj[CLASSNAME_ATTR_NAME];
		
		
//		// DBG
//		if(className["JSONref"]){
//			// DBG
//			console.log("aaaaaaaaaaaaaaa oldObj:",oldObj);
//		}
		
		// OLD AND BOGUS (SERIOUSLY, DON'T USE IT ANYMORE !) : let newObj=cloneObjectShallow(oldObj, stateOnly, className);
		let newObj=structuredClone(oldObj);
		
		
//		// DBG
//		console.log(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
//		console.log(";;;;;;;;;newObj",newObj);
//		console.log(";;;;;;;;;resultMap",resultMap);
//		console.log(";;;;;;;;;attrName",attrName);
//		console.log(";;;;;;;;;newAttr",newAttr);
		
		
		resultMap[uuid]=newObj;
		
		// Then we browse the whole flat map and we remove the technical attributes if wished :
		let objectToClean=resultMap[uuid];
		// We always remove the uuid ! (to allow other next flattenings / unflattening cycles)
		delete objectToClean[UUID_ATTR_NAME];
		if(removeTypeInfo){
			delete objectToClean[CLASSNAME_ATTR_NAME];

		}

	});
	

	
	// Second we replace all the references with pointers to the live objects :
	foreach(resultMap,(newObj,uuid)=>{
		
		// Attributes processing :
		foreach(newObj,(newAttr,attrName)=>{
						

			
			
			if(newAttr==null){
				newObj[attrName]=null;
				return "continue";
			}
			
			if(isArray(newAttr)){
				
					newObj[attrName]=[];

					
					foreach(newAttr,(itemInArray)=>{
											
						// In case non-pointer type objects are also presents in the array :
						// At this point, we should have only primitite-ish types items :
						if( itemInArray==null
								|| !isObject(itemInArray)
								|| isArray(itemInArray)
								|| itemInArray[POINTER_TO_ATTR_NAME]==null){
							
							newObj[attrName].push(itemInArray);
							return "continue";
						}
						
						
						// Pointer-type objects :
						let uuidTo=itemInArray[POINTER_TO_ATTR_NAME];
						let foundObjectToInNewMap=resultMap[uuidTo];
						
						if(!foundObjectToInNewMap){
							// If we haven't found the object in the new map yet :
							// TRACE
							console.log("ERROR : We could'nt find the object referenced  by this uuid «"+uuidTo+"» anywhere, for attribute of name «"+attrName+"». This should NEVER happen.");
						}else{
							newObj[attrName].push(foundObjectToInNewMap);
						}
					
						
						
					});
					
					
					
			}else{
			
				let uuidTo=newAttr[POINTER_TO_ATTR_NAME];
				let foundObjectToInNewMap=resultMap[uuidTo];
									
				// If object has already been entered in new map :
				if(!foundObjectToInNewMap){
					// If we haven't found the object in the new map yet :
					// TRACE
					console.log("ERROR : We could'nt find the object referenced  by this uuid «"+uuidTo+"» anywhere, for attribute of name «"+attrName+"». This should NEVER happen.");
				}else{
					//
					newObj[attrName]=foundObjectToInNewMap;
				}
			
			}
			
		},(newAttr)=>{
				return newAttr==null || isArray(newAttr) || (isObject(newAttr) && newAttr[POINTER_TO_ATTR_NAME]!=null);
		});
		
	});
	
	
	
	// (We actually only return the first object, for it is the root object)
	const r=getAt(resultMap,0);
	return r;
};




// ----------------------------------------------------------------

window.getAsFlatStructure=function(rawObject, stateOnly=false
		,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME/* Yet Another UUID */
		,CLASSNAME_ATTR_NAME=DEFAULT_CLASSNAME_ATTR_NAME/* Yet Another Classname */
		,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME/* Yet Another PointerTo */
		){
	
	// MAYBE DEPRECATED : USE JSON.decycle(...) INSTEAD !
	return getAsFlatStructureImpl(rawObject, stateOnly 
			,UUID_ATTR_NAME, CLASSNAME_ATTR_NAME, POINTER_TO_ATTR_NAME
		);
};


window.getAsTreeStructure=function(oldMap, stateOnly=false, removeTypeInfo=true
		,UUID_ATTR_NAME=DEFAULT_UUID_ATTR_NAME/* Yet Another UUID */
		,CLASSNAME_ATTR_NAME=DEFAULT_CLASSNAME_ATTR_NAME/* Yet Another Classname */
		,POINTER_TO_ATTR_NAME=DEFAULT_POINTER_TO_ATTR_NAME/* Yet Another PointerTo */
){

	// MAYBE DEPRECATED USE JSON.recycle(...) INSTEAD !
	return getAsTreeStructureImpl(oldMap, stateOnly, removeTypeInfo
			,UUID_ATTR_NAME, CLASSNAME_ATTR_NAME, POINTER_TO_ATTR_NAME
		);
};




/*
cycle.js
2018-05-15
Public Domain.
NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
This code should be minified before deployment.
See http://javascript.crockford.com/jsmin.html
USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
NOT CONTROL.
*/

//The file uses the WeakMap feature of ES6.

/*jslint eval */

/*property
$ref, decycle, forEach, get, indexOf, isArray, keys, length, push,
retrocycle, set, stringify, test
*/

if(typeof JSON.decycle !== "function"){
JSON.decycle=function decycle(object, replacer, classNameAttributeName){
    "use strict";

//Make a deep copy of an object or array, assuring that there is at most
//one instance of each object or array in the resulting structure. The
//duplicate references (which might be forming cycles) are replaced with
//an object of the form

//  {"$ref": PATH}

//where the PATH is a JSONPath string that locates the first occurance.

//So,

//  var a=[];
//  a[0]=a;
//  return stringifyObject(JSON.decycle(a));

//produces the string '[{"$ref":"$"}]'.

//If a replacer function is provided, then it will be called for each value.
//A replacer function receives a value and returns a replacement value.

//JSONPath is used to locate the unique object. $ indicates the top level of
//the object or array. [NUMBER] or [STRING] indicates a child element or
//property.

    var objects=new WeakMap();     // object to path mappings

    return (function derez(value, path){

//The derez function recurses through the object, producing the deep copy.

        var old_path;   // The path of an earlier occurance of value
        var nu;         // The new object or array

//If a replacer function was provided, then call it to get a replacement value.
        if(replacer){
            value=replacer(value);
        }

//typeof null === "object", so go on if this value is really an object but not
//one of the weird builtin objects.

        if(
            (typeof value === "object"
             //|| isFunction(value)
             ) 
            && value !== null
            && !(value instanceof Boolean)
            && !(value instanceof Date)
            && !(value instanceof Number)
            && !(value instanceof RegExp)
            && !(value instanceof String)
        ){

//If the value is an object or array, look to see if we have already
//encountered it. If so, return a {"$ref":PATH} object. This uses an
//ES6 WeakMap.

            old_path=objects.get(value);
            if(old_path !== undefined){
                return {$ref: old_path};
            }

//Otherwise, accumulate the unique value and its path.
            objects.set(value, path);

//If it is an array, replicate the array.

            if(Array.isArray(value)){
                nu=[];
                value.forEach(function (element, i){
                    nu[i]=derez(element, path + "[" + i + "]");
                });
            } else {
								//If it is an object, replicate the object.
                nu={};
                Object.keys(value).forEach(function (name){
                		// CANNOT USE stringifyObject(...) function because we are in a common, lower-level library !
                    nu[name]=derez( value[name], path + "[" + stringifyObject(name) + "]"
                    );
                });
                
                // We add a class name information if needed :
                if(classNameAttributeName)	nu[classNameAttributeName]=getClassName(value);
                
            }
            return nu;
        }
        return value;
    }(object, "$"));
};
}


// ORIGINAL NAME : JSON.retrocycle 
if(typeof JSON.recycle !== "function"){
JSON.recycle=function recycle($){
    "use strict";
	
	//Restore an object that was reduced by decycle. Members whose values are
	//objects of the form
	//  {$ref: PATH}
	//are replaced with references to the value found by the PATH. This will
	//restore cycles. The object will be mutated.
	
	//The eval function is used to locate the values described by a PATH. The
	//root object is kept in a $ variable. A regular expression is used to
	//assure that the PATH is extremely well formed. The regexp contains nested
	//quantifiers. That has been known to have extremely bad performance
	//problems on some browsers for very long strings. A PATH is expected to be
	//reasonably short. A PATH is allowed to belong to a very restricted subset of
	//Goessner's JSONPath.
	
	//So,
	//  var s='[{"$ref":"$"}]';
	//  return JSON.recycle(parseJSON(s));
	//produces an array containing a single element which is the array itself.
	
	    var px=/^\$(?:\[(?:\d+|"(?:[^\\"\u0000-\u001f]|\\(?:[\\"\/bfnrt]|u[0-9a-zA-Z]{4}))*")\])*$/;
	
	    (function rez(value){
	
	//The rez function walks recursively through the object looking for $ref
	//properties. When it finds one that has a value that is a path, then it
	//replaces the $ref object with a reference to the value that is found by
	//the path.
	
	        if(value && typeof value === "object"){
	            if(Array.isArray(value)){
	                value.forEach(function (element, i){
	                    if(typeof element === "object" && element !== null){
	                        var path=element.$ref;
	                        if(typeof path === "string" && px.test(path)){
	                            value[i]=eval(path);
	                        } else {
	                            rez(element);
	                        }
	                    }
	                });
	            } else {
	                Object.keys(value).forEach(function (name){
	                    var item=value[name];
	                    if( item !== null && (typeof item === "object" )){
	                        var path=item.$ref;
	                        if(typeof path === "string" && px.test(path)){
	                            value[name]=eval(path);
	                        } else {
	                            rez(item);
	                        }
	                    }
	                });
	            }
	        }
	        
	    }($));
	    return $;
	};
}




// CAUTION : TODO : FIXME WILL ERROR IF THERE IS A NON-ESCAPED SINGLE QUOTE ("'") IN RAW STRING !!!
// JSON parsing management :
window.parseJSON=function(strParam){
	
	if(!isString(strParam))	return strParam;
	
	if (nothing(strParam))
		return null;
	var str=toOneSimplifiedLine(strParam.trim())
					// OLD : Safer, but necessited to never use «'» in literal strings, and use «´» acute character instead !
						.replace(/'/gim, "\"");
	// DOES NOT WORK :
//					.replace(/,'/gim, ",\"")
//					.replace(/:'/gim, ":\"")
//					.replace(/{'/gim, "{\"")
//					.replace(/}'/gim, "}\"")
//					.replace(/\['/gim, "[\"")
//					.replace(/\]'/gim, "]\"");
	try {

		if((JSON && JSON.parse && typeof JSON.parse !== "undefined" && typeof JSON.parse === "function"
			) || jQuery)
			return JSON.parse(str);
		if(typeof jQuery !== "undefined" && typeof jQuery.parseJSON !== "undefined" && typeof jQuery.parseJSON === "function"){
			return jQuery.parseJSON(str);
		}
		throw new Error("ERROR : No JSON subsystem found to parse the JSON string.");
		
	} catch (error){
		// TRACE
		log("WARN : Error parsing string «" + strParam + "» as JSON. Trying last-chance fallback.");
		try {
			var obj=eval("(" + str + ")");

			return obj;
		} catch (error2){

			// TRACE
			log(error2);
			log("ERROR : Error parsing string «" + strParam + "» as JSON. Last-chance fallback failed." +
					" CHECK IF THERE IS ANY «'» SINGLE QUOTE CHARACTER IN YOUR LITERALS STRINGS, AND REPLACE IT WITH «´» ACUTE CHARACTER INSTEAD.");

		}

	}
	return null;
}

window.stringifyObject=function(objectToStringify){
	
	if(objectToStringify==null)	return null;
	
	if (JSON && JSON.stringify && typeof JSON.stringify !== "undefined" && typeof JSON.stringify === "function"){
		return JSON.stringify(objectToStringify);
	}
	if(typeof jQuery !== "undefined" && typeof jQuery.stringify !== "undefined" && typeof jQuery.stringify === "function"){
		return jQuery.stringify(objectToStringify);
	}
	log("ERROR : No JSON subsystem found to stringify the javascript object.");
	return null;
}


window.splitURL=(urlOrigin)=>{
	let protocol=null;
	let hostAndPort=urlOrigin;
	if(contains(urlOrigin,"://")){
		const hostAndPortSplits=urlOrigin.split("://");
		protocol=hostAndPortSplits[0];
		hostAndPort=hostAndPortSplits[1];
	}
	let host=null, port=null;
	if(contains(hostAndPort,":")){
		const splits=hostAndPort.split(":");
		host=splits[0];
		port=splits[1];
	}else{
		host=hostAndPort;
	}
	const isSecure=(protocol && contains(protocol,"s"));
	return {protocol:protocol, host:host, port:port, isSecure:isSecure};
};



// MUST REMAIN AT THE END OF THIS LIBRARY FILE !

AOTRAUTILS_LIB_IS_LOADED=true;



	

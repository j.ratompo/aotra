

/* ## Utility global methods in a javascript, console (nodejs) server, or vanilla javascript with no browser environment.
 *
 * This set of methods gathers utility generic-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 *  
 */



// COMPATIBILITY browser javascript / nodejs environment :
if(typeof(window)==="undefined")	window=global;	




// OLD : socket.io :
// https://stackoverflow.com/questions/31156884/how-to-use-https-on-node-js-using-express-socket-io
// https://stackoverflow.com/questions/6599470/node-js-socket-io-with-ssl
// https://nodejs.org/api/https.html#https_https_createserver_options_requestlistener
// https://socket.io/docs/v4/client-socket-instance/

// NEW : ws :
// https://github.com/websockets/ws#installing
// https://github.com/websockets/ws/blob/master/doc/ws.md#event-message
// ON BROWSER SIDE : Native Websockets :
// https://developer.mozilla.org/en-US/docs/Web/API/WebSocket
// https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications

// We have to import both implementations, regardless of which one is chosen :
//Socket=require("socket.io");
//WebSocket=require("ws");




// =================================================================================
// NODEJS UTILS

const FILE_ENCODING="utf8";

const ADD_CORS_HEADER=true;





// Nodejs filesystem utils :
if(typeof(fs)==="undefined"){
	// TRACE
	console.log("WARN : Could not find the nodejs dependency «fs», aborting persister setup.");
	getPersister=()=>{	return null;	};
	
}else{
	
	
	getPersister=function(dataDirPath,prefix=""){
		
		let self={
		
			dataDirPath:dataDirPath,
			prefix:prefix,
			
			//FILE_NAME_PATTERN:"data.clientId.repositoryName.json",
			/*private*/getPath:function(clientId="noclient", repositoryName="norepository"){
		//	let path=self.FILE_NAME_PATTERN.replace(new RegExp("@clientId@","g"),clientId);
				let path=`${self.dataDirPath}`
								+ (blank(self.prefix)?"":(self.prefix+"."))
								+`${clientId}.${repositoryName}.json`;
				return path;
			},
			
			readTreeObjectFromFile:function(clientId="noclient", repositoryName="norepository", CLASSNAME_ATTR_NAME=DEFAULT_CLASSNAME_ATTR_NAME){
				
				let path=self.getPath(clientId,repositoryName);
				
				let resultFlat=null;
		
				try{
					resultFlat=fs.readFileSync(path, FILE_ENCODING);
					
				}catch(error){
					// TRACE
					console.log("ERROR : Could not read file «"+path+"».");
		
					return null;
				}
				
				
				let resultData={};
				
				// 1)
				if(!empty(resultFlat))	resultData=parseJSON(resultFlat);
				
				// 2)
				if(!empty(resultData) && isFlatMap(resultData)){
					resultData=getAsTreeStructure(resultData,true
								// We have to keep the type information, here too ! (in the sub-objects)
								,false);
				}
				
				return resultData;
			},

			
			saveDataToFileForClient:function(clientId,repositoryName,dataFlatForClient,forceKeepUnflatten=false,doOnSuccess=null){
				
				
				
				if(!empty(dataFlatForClient) && !isFlatMap(dataFlatForClient) && !forceKeepUnflatten){
					dataFlatForClient=getAsFlatStructure(dataFlatForClient,true);
				}
				
				// reserved characters : -/\^$*+?.()|[]{}
				// CANNOT USE stringifyObject(...) function because we are in a common, lower-level library !
				let dataFlatStr=stringifyObject(dataFlatForClient)
					// We «aerate» the produced JSON :
					.replace(/":[\w]*\{/gim,"\":{\n").replace(/,"/gim,",\n\"")
					// ...except for inline, escaped JSON string representations :
					.replace(/\\\":[\w]*\{\n/gim,"\\\":{");
					// NO : .replace(/}/gim,"}\n");
				
				
				let path=self.getPath(clientId,repositoryName);
				
				fs.writeFile(path, dataFlatStr, FILE_ENCODING, (error) => {
			    if(error){
			    	// TRACE
						console.log("ERROR : Could not write file «"+path+"»:",error);
			    	throw error;
			    }
			    if(doOnSuccess)	doOnSuccess(dataFlatForClient);
				});
			}
			
		};
		
		
		return self;
	};
	
}



// Nodejs server launching helper functions :
//Networking management :
//- WEBSOCKETS AND NODEJS :

function isConnected(clientSocket){
	if(!WebsocketImplementation.useSocketIOImplementation)
		return (clientSocket.readyState===WebSocket.OPEN)
	return (clientSocket.connected);
}



// -Server :

getServerParams=function(portParam=null, certPathParam=null, keyPathParam=null, argsOffset=0){
	
	// Node dependencies :
	// https=require("https");
	// fs=require("fs");
	
	if(typeof(https)==="undefined"){
		// TRACE
		console.log("WARN : Could not find the nodejs dependency «https», aborting SSL setup.");
		return null;
	}
	if(typeof(fs)==="undefined"){
		// TRACE
		console.log("WARN : Could not find the nodejs dependency «fs», aborting SSL setup.");
		return null;
	}

	const result={};
	
	// We read the command-line arguments if needed :
	let argCLPort;
	let argCLCertPath;
	let argCLKeyPath;
	process.argv.forEach(function (val, i){
		if(i<=argsOffset+1)	return;
		else if(i==argsOffset+2)	argCLPort=val;
		else if(i==argsOffset+3)	argCLCertPath=val;
		else if(i==argsOffset+4)	argCLKeyPath=val;
	});
	
	// Console, command-line arguments OVERRIDE parameters values :
	
	result.port=nonull(argCLPort,portParam);
	result.certPath=null;
	result.keyPath=null;
	result.isSecure=!!(certPathParam || keyPathParam);
	
	if(result.isSecure){
		result.certPath=nonull(argCLCertPath,certPathParam);
		result.keyPath=nonull(argCLKeyPath,keyPathParam);
	}
	
	// Eventual encryption options :
	result.sslOptions=null;
	if(result.isSecure){
		result.sslOptions={
			cert: fs.readFileSync(result.certPath),
			key: fs.readFileSync(result.keyPath),
		};
	}
	
	return result;
}

getConsoleParam=function(index=0, argsOffset=0){
	let result=null;
	if(!process){
		throw new Error("ERROR : Cannot extract console parameter in this context !");
	}	
	process.argv.forEach((val, i)=>{
		if(i<=argsOffset+1)	return;
		else if(i==argsOffset+index+1)	result=val;
	});
	return result;
}



// NODE ONLY SERVER / CLIENTS : 
WebsocketImplementation={


		isNodeContext:true,
		useSocketIOImplementation:false,
		useFlatStrings:false,
		
		// COMMON METHODS
		/*private static*/isInRoom(clientSocket, clientsRoomsTag){
			return (!clientsRoomsTag || empty(clientsRoomsTag) || contains(clientsRoomsTag, clientSocket.clientRoomTag));
		},
		
		
		//
		// CONSOLE NODE SERVER/CLIENT
		//
		getStatic:(isNodeContext=true, useSocketIOImplementation=/*DEBUG*/false)=>{
			
			WebsocketImplementation.isNodeContext=isNodeContext;
			WebsocketImplementation.useSocketIOImplementation=useSocketIOImplementation;

			if(WebsocketImplementation.useSocketIOImplementation){
				// TRACE
				lognow("INFO : (SERVER/CLIENT) Using socket.io websocket implementation.");	
				
				if(isNodeContext){
					
					// NODE SERVER :
					// DBG
					lognow("INFO : Loading NODE SERVER socket.io libraries.")
					if(typeof(Socket)==="undefined"){
						// TRACE
						console.log("«socket.io» NODE SERVER library not called yet, calling it now.");
						Socket=require("socket.io");
					}
					if(typeof(Socket)==="undefined"){
						// TRACE
						console.log("ERROR : «socket.io» NODE CLIENT/SERVER library not found. Cannot launch nodejs server. Aborting.");
					}
					
					// NODE CLIENT :
					// DBG
					lognow("INFO : Loading NODE CLIENT socket.io libraries.")
					if(typeof(io)==="undefined"){
						// TRACE
						console.log("«socket.io-client» NODE CLIENT library not called yet, calling it now.");
						io=require("socket.io-client");
					}
					if(typeof(io)==="undefined"){
						// TRACE
						console.log("ERROR : «socket-client.io» NODE CLIENT library not found. Cannot launch nodejs server. Aborting.");
					}
				}

			}else{
				// TRACE
				lognow("INFO : (SERVER/CLIENT) Using native WebSocket implementation.");	
				
				// https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications
				if(isNodeContext){
					if(typeof(WebSocket)==="undefined"){
						// TRACE
						console.log("«ws» SERVER library not called yet, calling it now.");
						
						WebSocket=require("ws");
						if(typeof(WebSocket)==="undefined"){
							// TRACE
							console.log("ERROR : «ws» CONSOLE/BROWSER CLIENT/SERVER library not found. Cannot launch nodejs server. Aborting.");
						}
					}
				}
				
			}

			// *********************************************************************************
			
			return WebsocketImplementation;
		},
		
		
		/*private*/getMessageDataBothImplementations:(eventOrMessageParam)=>{
			
				const eventOrMessage=(!WebsocketImplementation.useSocketIOImplementation?eventOrMessageParam.data:eventOrMessageParam);
			
				let dataResult=eventOrMessage;
				
				try{
					dataResult=(WebsocketImplementation.useFlatStrings || isString(eventOrMessage)?parseJSON(eventOrMessage):eventOrMessage);
				}catch(error1){
					// TRACE
					lognow(`ERROR : Failed to parse JSON for string «${dataResult}»`,error1);
					dataResult=(isString(eventOrMessage)?eventOrMessage:stringifyObject(eventOrMessage));
				}
				
				return dataResult;
		},
		
		
		getServer:(listenableServer)=>{
			
			if(!WebsocketImplementation.isNodeContext){
				// TRACE
				throw new Error("ERROR : SERVER : Server launch is not supported in a non-nodejs context for any implementation.");
			}
			

			
			// TODO : FIXME : Use one single interface !
			// NODE SERVER MODE ONLY :
			let serverSocket;
			if(!WebsocketImplementation.useSocketIOImplementation){
				serverSocket=new WebSocket.Server({ "server":listenableServer });
			}else{
				
				// NOW : socket.io :
				// Loading socket.io
//			OLD SYNTAX : serverSocket=Socket.listen(listenableServer);
				serverSocket=new Socket.Server(listenableServer);
				
				if(listenableServer.cert || listenableServer.key){
					// TRACE :
					lognow("WARN : CAUTION ! ON TODAY (01/08/2022) socket.io SERVER LIBRARY IS BOGUS AND WON'T WORK (node clients can't connect !!) IF listenableServer IS A https SERVER !!!");
				}
				
				// Setting up the disconnect event for a client :
				serverSocket.on("endConnection",()=>{
					serverSocket.disconnect();
				});
			}
			
			serverSocket.on("error",(error)=>{
				
				// TRACE
				lognow("ERROR : An error occurred when trying to start the server : ",error);
				
			});

			// NODE SERVER INSTANCE :
			const nodeServerInstance={
					
					onClientLostListeners:[],
					
					clientPingIntervalMillis:5000,
					setPingPongTimeout:(clientPingIntervalMillis)=>{
						nodeServerInstance.clientPingIntervalMillis=clientPingIntervalMillis;
						return nodeServerInstance;
					},
					
//					clientsSockets:[],
					serverSocket:serverSocket,
					listenableServer:listenableServer,
					
					receptionEntryPoints:[],
					
					close:(doOnCloseServer)=>{
						if(nodeServerInstance.serverSocket)	return;
						nodeServerInstance.serverSocket.close(()=>{
							if(nodeServerInstance.listenableServer)	return;	
							nodeServerInstance.listenableServer.close(doOnCloseServer);
						});
					},
					
					receive:(channelNameParam, doOnIncomingMessage, clientsRoomsTag=null)=>{
						
						// DBG
						lognow("(SERVER) Registering receive for «"+channelNameParam+"»...");
						
						
						
						const receptionEntryPoint={
							channelName:channelNameParam,
							clientsRoomsTag:clientsRoomsTag, 
							execute:(eventOrMessage, clientSocketParam)=>{

							 // With «ws» library we have no choive than register message events inside a «connection» event !
								
//							// DBG
//							lognow("(SERVER) RECEIVED SOMETHING FROM CLIENT...", eventOrMessage.data);
								
//							dataWrapped=parseJSON(dataWrapped);
//							dataWrapped=getAt(dataWrapped,0);// We get the root element
								

								const dataWrapped=WebsocketImplementation.getMessageDataBothImplementations(eventOrMessage);
							
							
								// Channel information is stored in exchanged data :
								if(dataWrapped.channelName!==receptionEntryPoint.channelName) return;
								
																
//								// DBG
//								lognow("(SERVER) ENTRY POINT IS OF THE RIGHT CHANNEL:", receptionEntryPoint.channelName);
								

								// TODO : FIXME : Use one single interface !
								// Room information is stored in client socket object :
								const isClientInRoom=WebsocketImplementation.isInRoom(clientSocketParam, receptionEntryPoint.clientsRoomsTag);
								
								// DBG
								lognow("(SERVER) isClientInRoom:",isClientInRoom);
								
								if(!isClientInRoom) return;

								if(doOnIncomingMessage){
									// DBG
									lognow("(SERVER) doOnIncomingMessage:");
									doOnIncomingMessage(dataWrapped.data, clientSocketParam);
								}
								
							},
						};
						
						
						///!!!
						nodeServerInstance.receptionEntryPoints.push(receptionEntryPoint);
						
						// SPECIAL FOR THE SOCKETIO IMPLEMENTATION :
						if(WebsocketImplementation.useSocketIOImplementation){
							const channelName=receptionEntryPoint.channelName;
							clientSocket.on(channelName, (eventOrMessage)=>{
								receptionEntryPoint.execute(eventOrMessage, clientSocket);
							});
						}

						
//					// DBG
//					console.log("ADD RECEPTION ENTRY POINT channelName:«"+channelName+"»! nodeServerInstance.receptionEntryPoints.length:",nodeServerInstance.receptionEntryPoints.length);
						
						
						return nodeServerInstance;
					},
					
					
					send:(channelName, data, clientsRoomsTag=null, clientSocketParam=null)=>{
						
						// DBG
						lognow("(SERVER) SERVER TRIES TO SEND !");
						
						
						if(!clientSocketParam){
							
								// DBG
								lognow("(SERVER) (server sends to all clients)");
							
								// TODO : FIXME : Use one single interface !
								let serverClients;
								if(!WebsocketImplementation.useSocketIOImplementation)	serverClients=nodeServerInstance.serverSocket.clients;
//							OLD:	else																							serverClients=nodeServerInstance.serverSocket.sockets.clients();
								else																										serverClients=nodeServerInstance.serverSocket.sockets.sockets;
						    
						    
						    
						    serverClients.forEach((clientSocket)=>{
						    	
						    	
									if(!isConnected(clientSocket))	return;
						    	
		
									// Room information is stored in client socket object :
							   	if(!WebsocketImplementation.isInRoom(clientSocket,clientsRoomsTag)) return;
			
						      // Channel information is stored in exchanged data :
							   	let dataWrapped={channelName:channelName, data:data};
					      	
						      
									dataWrapped=stringifyObject(dataWrapped);
									
									// TODO : FIXME : Use one single interface !
					      	if(!WebsocketImplementation.useSocketIOImplementation)	clientSocket.send(dataWrapped);
						      else																										clientSocket.emit(channelName,dataWrapped);

						    });
						
						}else{
							
							// DBG
							lognow("(SERVER) (server sends to a client)");
							
							
							// TODO : FIXME : Use one single interface !
							let clientSocket=clientSocketParam;
							if(!WebsocketImplementation.useSocketIOImplementation)	if(clientSocket.readyState!==WebSocket.OPEN)	return;
							else																										if(clientSocket.connected)	return;


							// Room information is stored in client socket object :
				      if(!WebsocketImplementation.isInRoom(clientSocket,clientsRoomsTag)) return;

			      	// Channel information is stored in exchanged data :
				      let dataWrapped={channelName:channelName, data:data};
							dataWrapped=stringifyObject(dataWrapped);
							
							
							// DBG
							lognow("(SERVER) WebsocketImplementation.useSocketIOImplementation:"+WebsocketImplementation.useSocketIOImplementation);
//						lognow("(SERVER) dataWrapped:"+dataWrapped);
							
							
							// TODO : FIXME : Use one single interface !
							if(!WebsocketImplementation.useSocketIOImplementation)	clientSocket.send(dataWrapped);
			      	else																										clientSocket.emit(channelName,dataWrapped);
			      	
			      	
						}
				    
						
						return nodeServerInstance;
					},
					
					
					onConnectionToClient:(doOnConnection)=>{
						
						// «connection» is the only event fired by the serverSocket :
						nodeServerInstance.serverSocket.on("connection", (clientSocket)=>{
						
							// DBG
							console.log("SERVER : ON CONNECTION !");
							
							
//						if(contains(nodeServerInstance.clientsSockets, clientSocket))	return;
//						nodeServerInstance.clientsSockets.push(clientSocket);


							const clientId="autogeneratedid_"+getUUID();
							
							
							clientSocket.clientId=clientId;
							
							

							const doOnMessage=(eventOrMessage)=>{
								// We execute the events registration listeners entry points:
								foreach(nodeServerInstance.receptionEntryPoints,(receptionEntryPoint,i)=>{
									receptionEntryPoint.execute(eventOrMessage, clientSocket);
								});
							};

		 					if(!WebsocketImplementation.useSocketIOImplementation){	clientSocket.addEventListener("message", doOnMessage);
							// NO:	else																						clientSocket.on("message", doOnMessage);
							}
								
							doOnConnection(nodeServerInstance, clientSocket);
							
							
							// DBG
							lognow("DEBUG : Starting ping-pong with client : clientSocket.clientId:",clientSocket.clientId);
							lognow("DEBUG : WebsocketImplementation.useSocketIOImplementation:",WebsocketImplementation.useSocketIOImplementation);
							lognow("DEBUG : clientSocket.readyState:",clientSocket.readyState);
							
							
							// To make the server aware of the clients connections states :
							clientSocket.isConnectionAlive=true;
							clientSocket.stateCheckInterval=setInterval(()=>{
								
								
							    if (clientSocket.isConnectionAlive===false){
			    				 	
			    				 	
			    				 	// On today, this method is named as same for the two implementations :
			    				 	// TRACE
			    				 	lognow("(SERVER) Removing all listeners for client socket «"+clientSocket.clientId+"».");
			    				 	
			    				 	clientSocket.removeAllListeners();
			    				 	
			    				 	
	    				 			// TODO : FIXME : Use one single interface !
										if(!WebsocketImplementation.useSocketIOImplementation)	clientSocket.terminate();
										else																										clientSocket.emit("endConnection");

							    	if(!empty(nodeServerInstance.onClientLostListeners))
							    		foreach(nodeServerInstance.onClientLostListeners,l=>{l.execute(clientSocket);});

							    	// DBG
							    	lognow("DEBUG : (SERVER) Connection closed for failed ping-pong.");
							    	
							    	
			    				 	clearInterval(clientSocket.stateCheckInterval);
							    	return;
							    }
							    
							    clientSocket.isConnectionAlive=false;
								
//									// DBG
//									lognow("(SERVER) DEBUG : SENDING PING");
								
									if(!WebsocketImplementation.useSocketIOImplementation)	clientSocket.ping();
									// OLD :
//								else																										clientSocket.emit("ping");
									else 																										nodeServerInstance.send("protocol",{type:"ping"}, null, clientSocket);

							    
							}, nodeServerInstance.clientPingIntervalMillis);
							
							
							if(!WebsocketImplementation.useSocketIOImplementation){
								clientSocket.on("pong",()=>{
									clientSocket.isConnectionAlive=true;
								});
							}else{
//						// OLD :
//							clientSocket.on("ping",()=>{
//								clientSocket.isConnectionAlive=true;
//							});

								nodeServerInstance.receive("protocol",(message)=>{
									if(message.type!=="pong")	return;
									clientSocket.isConnectionAlive=true;
								});
								
							}
							
							
							
						});
						
						
						return nodeServerInstance;
					},
					
					onFinalize:(doOnFinalizeServer)=>{
					
						doOnFinalizeServer(nodeServerInstance);
					
						// TRACE
						console.log("INFO : SERVER : Node server setup complete.");
						
						return nodeServerInstance;
					},
					
					addToRoom:(clientSocket,clientRoomTag)=>{
						clientSocket.clientRoomTag=clientRoomTag;
					},
					
					
					
			};
			
			// Join room server part protocol :
			nodeServerInstance.receive("protocol",(message, clientSocket)=>{
				
				if(message.type!=="joinRoom" || !clientSocket)	return;
				nodeServerInstance.addToRoom(clientSocket, message.clientRoomTag);

			});
			
			
			
			// To make the server aware of the clients connections states :
			nodeServerInstance.serverSocket.on("close", function close() {
				
				// TODO : FIXME : Use one single interface !
				if(!WebsocketImplementation.useSocketIOImplementation)	serverClients=nodeServerInstance.serverSocket.clients;
				// OLD : else																						serverClients=nodeServerInstance.serverSocket.sockets.clients();
				else																										serverClients=nodeServerInstance.serverSocket.sockets.sockets;
				
				serverClients.forEach((clientSocket)=>{
				 	clearInterval(clientSocket.stateCheckInterval);
				});
			});
			
			

			return nodeServerInstance;
		},
		
		// DO NOT USE DIRECTLY, USE INSTEAD initClient(...) (this function uses connectToServer(...)) !
		// NODE / BROWSER CLIENT CONNECTS TO SERVER MAIN ENTRYPOINT:
		connectToServer:(serverURL, port, isSecure=false, timeout)=>{

			// TRACE
			lognow("INFO : Using socket library flavor : "+(WebsocketImplementation.isNodeContext?"node (client/server-side)":"browser (client-side only)"));

			if(WebsocketImplementation.isNodeContext)
				return WebsocketImplementation.connectToServerFromNode(serverURL, port, isSecure, timeout);
			else
				return WebsocketImplementation.connectToServerFromBrowser(serverURL, port, isSecure, timeout);
		},
		
		
	
		//
		// NODE CLIENT
		//
		/*private*/connectToServerFromNode:(serverURL, port, isSecure, timeout)=>{
			
			
			// NODE CLIENT MODE ONLY :
			let clientSocket;
			if(!WebsocketImplementation.useSocketIOImplementation){
			
				// NEW : ws :
				if(typeof(WebSocket)==="undefined"){
					// TRACE
					lognow("ERROR : CLIENT : Could not find websocket client lib, aborting client connection.");
					return null;
				}

				clientSocket=new WebSocket(serverURL+":"+port,/*WORKAROUND:*/{ rejectUnauthorized:false, secure: isSecure });
			}else{
				// NOW : socket.io :
				//client on server-side:
				
				if(WebsocketImplementation.isNodeContext && typeof(io)!=="undefined"){
					
//				OLD SYNTAX:	clientSocket=Socket.connect(serverURL + ":" + port,{timeout: timeout, secure: isSecure});
//				NO :	clientSocket=new Socket.Client(serverURL + ":" + port,{timeout: timeout, secure: isSecure});
					clientSocket=io(serverURL + ":" + port,{timeout: timeout, secure: isSecure, autoConnect:true});
					// UNUSEFUL (since we have the autoconnect:true option) : clientSocket.connect();
				}
			}
			
			// DBG
			lognow("DEBUG : CLIENT : clientSocket created:");


			// NODE CLIENT INSTANCE :
			const nodeClientInstance={
					clientSocket:clientSocket,
			
					receptionEntryPoints:[],
					
					receive:(channelNameParam, doOnIncomingMessage, clientsRoomsTag=null)=>{
						
						const receptionEntryPoint={
								channelName:channelNameParam,
								clientsRoomsTag:clientsRoomsTag, 
								execute:(eventOrMessage)=>{
										
//								dataWrapped=parseJSON(dataWrapped);
//								dataWrapped=getAt(dataWrapped,0);// We get the root element
//								const dataWrapped=(WebsocketImplementation.useFlatStrings?parseJSON(eventOrMessage):eventOrMessage);
										
									const dataWrapped=WebsocketImplementation.getMessageDataBothImplementations(eventOrMessage);
									
									// Channel information is stored in exchanged data :
									if(dataWrapped.channelName && dataWrapped.channelName!==channelNameParam) return;
									
									const clientSocket=nodeClientInstance.clientSocket;
														
									// Room information is stored in client socket object :
									if(!WebsocketImplementation.isInRoom(clientSocket,clientsRoomsTag)) return;
									
									if(doOnIncomingMessage)	doOnIncomingMessage(dataWrapped.data, clientSocket);
									
									
								}
						};
						
						
						///!!!
						nodeClientInstance.receptionEntryPoints.push(receptionEntryPoint);
						
						// SPECIAL FOR THE SOCKETIO IMPLEMENTATION :
						if(WebsocketImplementation.useSocketIOImplementation){
							const channelName=receptionEntryPoint.channelName;
							clientSocket.on(channelName, (eventOrMessage)=>{
								receptionEntryPoint.execute(eventOrMessage, clientSocket);
							});
						}
						
						
						return nodeClientInstance;
					},
					
					

					// TODO : FIXME : DUPLICATED CODE !
					sendChainable:(channelNameParam, data, clientsRoomsTag=null)=>{
						
						// DBG
						lognow(">>>>>>sendChainable NODE CLIENT ("+channelNameParam+"):data:",data);
						
						// We add a message id :
						const messageId=getUUID();
						data.messageId=messageId;
						
						// 1) We prepare the reception :
						const resultPromise={
							clientsRoomsTag:clientsRoomsTag,
							messageId:messageId,
							thenWhenReceiveMessageType:(channelNameForResponse, listenerConfig={messageType:"",condition:()=>true}, doOnIncomingMessageForResponse)=>{
								listenerConfig=nonull(listenerConfig,{messageType:"",condition:()=>true});
								const listenerId=nonull(listenerConfig.messageType,"");
								
								nodeClientInstance.receive(channelNameForResponse, (dataLocal, clientSocket)=>{
									
									
									// We check if the message matches the condition :
									if(listenerConfig.condition && !listenerConfig.condition(dataLocal, clientSocket))	return ;
									
									// We check if we have the same message id:
									if(resultPromise.messageId!==dataLocal.messageId){
										// DBG
										lognow("resultPromise.messageId:"+resultPromise.messageId+"|dataLocal.messageId"+dataLocal.messageId);
										return;
									}
									
									doOnIncomingMessageForResponse(dataLocal, clientSocket);
								}, resultPromise.clientsRoomsTag, listenerId, {destroyListenerAfterReceiving:true});
								return resultPromise;
							}
						};

						// 2) We send the data :
						nodeClientInstance.send(channelNameParam, data, clientsRoomsTag);
						

						return resultPromise;
					},
					
					
					send:(channelNameParam, data, clientsRoomsTag=null)=>{
						
						// // DBG
						// lognow("(CLIENT) (NODEJS) CLIENT TRIES TO SEND !");
						
						
						const clientSocket=nodeClientInstance.clientSocket;
						
						
//			    // DBG
//			    console.log("(NODE CLIENT) TRYING TO SEND : clientSocket.readyState:",clientSocket.readyState);

						if(!isConnected(clientSocket))	return;
						

						// Room information is stored in client socket object :
						if(!WebsocketImplementation.isInRoom(clientSocket,clientsRoomsTag)) return;
						
						// Channel information is stored in exchanged data :
						let dataWrapped={channelName:channelNameParam, data:data};


//					// DBG
//			    console.log("(NODE CLIENT) SENDING DATA ! dataWrapped:",dataWrapped);


						dataWrapped=stringifyObject(dataWrapped);
	
	
//			            // DBG
//			            console.log("(NODE CLIENT) SENDING DATA ! channelNameParam:«"+channelNameParam+"» ; clientsRoomsTag:«"+clientsRoomsTag+"»");
//			            console.log("(NODE CLIENT) SENDING DATA ! dataWrapped:",dataWrapped);
	
						// TODO : FIXME : Use one single interface !
						if(!WebsocketImplementation.useSocketIOImplementation)	clientSocket.send(dataWrapped);
		      	else																										clientSocket.emit(channelNameParam,dataWrapped);
						
						return nodeClientInstance;
					},
					
					
					join:(clientRoomTag)=>{
						// Join room client part protocol :
						const message={type:"joinRoom",clientRoomTag:clientRoomTag};
						nodeClientInstance.send("protocol",message);
					},
					
					
					onConnectionToServer:(doOnConnection)=>{
						
						// DBG
						lognow("DEBUG : CLIENT : setting up onConnectionToServer.");
						
						const doAllOnConnection=()=>{
							
							// To avoid triggering this event several times, depending on the implementation : 
							if(nodeClientInstance.hasConnectEventFired)	return;
							nodeClientInstance.hasConnectEventFired=true;
							
							// DBG
							lognow("DEBUG : CLIENT (NODEJS) : doOnConnection !");

							const doOnMessage=(eventOrMessage)=>{
								
								// We execute the listeners entry points registration :
								foreach(nodeClientInstance.receptionEntryPoints,(receptionEntryPoint)=>{
									receptionEntryPoint.execute(eventOrMessage);
								});

							};
							
							const clientSocket=nodeClientInstance.clientSocket;
							if(!WebsocketImplementation.useSocketIOImplementation){	clientSocket.addEventListener("message", doOnMessage);
							// NO:	else																						clientSocket.on("message", doOnMessage);
							}

							doOnConnection(nodeClientInstance, clientSocket);
							
						};
						

						if(!WebsocketImplementation.useSocketIOImplementation)	nodeClientInstance.clientSocket.addEventListener("open",doAllOnConnection);
						else																										nodeClientInstance.clientSocket.on("connect",doAllOnConnection);
						
						
						// DBG
						lognow("DEBUG : CLIENT : nodeClientInstance.clientSocket.on(connect)");

						
						// Node client ping handling :  (SocketIO implementation only)
						if(WebsocketImplementation.useSocketIOImplementation){
							nodeClientInstance.receive("protocol",(message)=>{
								if(message.type!=="ping")	return;
								nodeClientInstance.send("protocol",{type:"pong"});
								
								// DBG
								lognow("DEBUG : NODE CLIENT : Pong sent.");
							});
						}
						
						
					},
					
				
			};
			
			
			return nodeClientInstance;
		},
		

		
		// BROWSER CLIENT
		
		/*private*/connectToServerFromBrowser:(serverURL, port, isSecure, timeout)=>{
			
			// NEW : ws :
			if(typeof(WebSocket)==="undefined"){
				// TRACE
				lognow("ERROR : CLIENT : Could not find websocket client lib, aborting client connection.");
				return null;
			}
			
			// TODO : FIXME : Use one single interface !
			// BROWSER CLIENT MODE ONLY :
			let clientSocket;
			if(!WebsocketImplementation.useSocketIOImplementation){
				clientSocket=new WebSocket(serverURL+":"+port,["ws","wss"]);
			}else if(typeof(io)!=="undefined"){
					// OLD SYNTAX :clientSocket=io.connect(serverURL + ":" + port,{timeout: timeout, secure: isSecure});
					// ALTERNATIVE :
					clientSocket=io(serverURL + ":" + port,{timeout: timeout, secure: isSecure});
			}
			
			
			// BROWSER CLIENT INSTANCE :
			const browserInstance={
					clientSocket:clientSocket,
					
					receptionEntryPoints:[],
					
					receive:(channelNameParam, doOnIncomingMessage, clientsRoomsTag=null, receptionEntryPointId=null, listenerConfig={destroyListenerAfterReceiving:false})=>{


						// DBG
						lognow("INFO : (CLIENT-BROWSER)  SETTING UP RECEIVE for :",channelNameParam);


						const receptionEntryPoint={
								channelName:channelNameParam,
								clientsRoomsTag:clientsRoomsTag,
								// TODO : ADD TO ALL OTHER SUBSYSTEMS !
								id:receptionEntryPointId,
								listenerConfig:listenerConfig,
								execute:(eventOrMessage)=>{
										
									const dataWrapped=WebsocketImplementation.getMessageDataBothImplementations(eventOrMessage);
							
									// // DBG
									// lognow("(CLIENT) (DEBUG) CLIENT RECEIVED SOMETHING FROM SERVER :",dataWrapped);
									
									// Channel information is stored in exchanged data :
									if(dataWrapped.channelName && dataWrapped.channelName!==channelNameParam) return;
									
									const clientSocket=browserInstance.clientSocket;
														
									// Room information is stored in client socket object :
									if(!WebsocketImplementation.isInRoom(clientSocket,clientsRoomsTag)) return;
									
									if(doOnIncomingMessage)
										doOnIncomingMessage(dataWrapped.data, clientSocket);
									
									// We remove one-time usage listeners :
									if(listenerConfig && listenerConfig.destroyListenerAfterReceiving)
										remove(browserInstance.receptionEntryPoints,receptionEntryPoint);
									
										
										
								}
						};
						
						///!!!
						// TODO : ADD TO ALL OTHER SUBSYSTEMS !
						if(!contains.filter((l)=>(l.id && receptionEntryPoint.id && l.id===receptionEntryPoint.id))(browserInstance.receptionEntryPoints))
							browserInstance.receptionEntryPoints.push(receptionEntryPoint);
						
						// SPECIAL FOR THE SOCKETIO IMPLEMENTATION :
						if(WebsocketImplementation.useSocketIOImplementation){
							const channelName=receptionEntryPoint.channelName;
							clientSocket.on(channelName, (eventOrMessage)=>{
								receptionEntryPoint.execute(eventOrMessage, clientSocket);
							});
						}
								
						
						return browserInstance;
					},
					
					

					// TODO : FIXME : DUPLICATED CODE !
					sendChainable:(channelNameParam, data, clientsRoomsTag=null)=>{
						
						// DBG
						lognow(">>>>>>sendChainable BROWSER CLIENT ("+channelNameParam+"):data:",data);
						
						// We add a message id :
						const messageId=getUUID();
						data.messageId=messageId;
						
						// 1) We prepare the reception :
						const resultPromise={
							clientsRoomsTag:clientsRoomsTag,
							messageId:messageId,
							thenWhenReceiveMessageType:(channelNameForResponse, listenerConfig={messageType:"",condition:()=>true}, doOnIncomingMessageForResponse)=>{
								listenerConfig=nonull(listenerConfig,{messageType:"",condition:()=>true});
								const listenerId=nonull(listenerConfig.messageType,"");
								
								browserInstance.receive(channelNameForResponse, (dataLocal, clientSocket)=>{
									
									
									// We check if the message matches the condition :
									if(listenerConfig.condition && !listenerConfig.condition(dataLocal, clientSocket))	return ;
									
									// We check if we have the same message id:
									if(resultPromise.messageId!==dataLocal.messageId){
										// DBG
										lognow("resultPromise.messageId:"+resultPromise.messageId+"|dataLocal.messageId"+dataLocal.messageId);
										return;
									}
									
									doOnIncomingMessageForResponse(dataLocal, clientSocket);
								}, resultPromise.clientsRoomsTag, listenerId, {destroyListenerAfterReceiving:true});
								return resultPromise;
							}
						};

						// 2) We send the data :
						browserInstance.send(channelNameParam, data, clientsRoomsTag);
						

						return resultPromise;
					},
					
					
					
					
					
					
					send:(channelNameParam, data, clientsRoomsTag=null)=>{
						
						// // DBG
						// lognow("(CLIENT) (BROWSER) CLIENT TRIES TO SEND !");
						
						const clientSocket=browserInstance.clientSocket;
			    	
								
//					// DBG
//					console.log("(BROWSER) TRYING TO SEND : clientSocket.readyState:",clientSocket.readyState);

						if(!isConnected(clientSocket))	return;
						
						
						// Room information is stored in client socket object :
						if(!WebsocketImplementation.isInRoom(clientSocket,clientsRoomsTag)) return;

						// Channel information is stored in exchanged data :
					  let dataWrapped={channelName:channelNameParam, data:data};
		      	
		      	
				    // // DBG
				    // console.log("(BROWSER) SENDING... : dataWrapped :",dataWrapped);
				    // console.log("(BROWSER) SENDING... : clientSocket :",clientSocket);

            
						dataWrapped=stringifyObject(dataWrapped);

				   	
		   			// TODO : FIXME : Use one single interface !
						if(!WebsocketImplementation.useSocketIOImplementation)	clientSocket.send(dataWrapped);
		      	else																										clientSocket.emit(channelNameParam,dataWrapped);

						
						return browserInstance;
					},
					
					
					join:(clientRoomTag)=>{
						// Join room client part protocol :
						const message={type:"joinRoom",clientRoomTag:clientRoomTag};
						browserInstance.send("protocol",message);
					},
					
					
					onConnectionToServer:(doOnConnection)=>{
						const doAllOnConnection=()=>{

							// To avoid triggering this event several times, depending on the implementation : 
							if(browserInstance.hasConnectEventFired)	return;
							browserInstance.hasConnectEventFired=true;

							
							
							// DBG
							lognow("DEBUG : CLIENT (BROWSER) : doOnConnection !");
							
							const doOnMessage=(eventOrMessage)=>{
								
								// We execute the listeners entry points registration :
								foreach(browserInstance.receptionEntryPoints,(receptionEntryPoint)=>{
									receptionEntryPoint.execute(eventOrMessage);
								});

							};
						    
							const clientSocket=browserInstance.clientSocket;
						  if(!WebsocketImplementation.useSocketIOImplementation){	clientSocket.addEventListener("message", doOnMessage);
							// NO:	else																						clientSocket.on("message", doOnMessage);
							}


							doOnConnection(browserInstance, clientSocket);
							
						};
						
						if(!WebsocketImplementation.useSocketIOImplementation)	browserInstance.clientSocket.addEventListener("open",doAllOnConnection);
						else																										browserInstance.clientSocket.on("connect",doAllOnConnection);
						
						
						
						// Browser client ping handling : (SocketIO implementation only)
						if(WebsocketImplementation.useSocketIOImplementation){
							browserInstance.receive("protocol",(message)=>{
								if(message.type!=="ping")	return;
								browserInstance.send("protocol",{type:"pong"});
								
								// DBG
								lognow("DEBUG : BROWSER CLIENT : Pong sent.");
							});
						}
						
						
					},
					
				
			};
			
			
			
			
			return browserInstance;
		},

};



launchNodeHTTPServer=function(port, doOnConnect=null, doOnFinalizeServer=null, /*OPTIONAL*/sslOptions=null, httpHandlerParam=null, addCORSHeader=ADD_CORS_HEADER){
	
	const EXCLUDED_FILENAMES_PARTS=[".keyHash."];
	
	
	
	if(typeof(https)==="undefined"){
		// TRACE
		console.log("«https» SERVER library not called yet, calling it now.");
		https=require("https");
	}
	if(typeof(http)==="undefined"){
		// TRACE
		console.log("«http» SERVER library not called yet, calling it now.");
		http=require("http");
	}
	
	
	const DEFAULT_HANDLER=function(request, response){
		
		const url=request.url;

		let isURLInExclusionZone=!!foreach(EXCLUDED_FILENAMES_PARTS,(excludedStr)=>{
			if(contains(url,excludedStr)){
				return true;
			}
		});
		
		if(isURLInExclusionZone){
      	// TRACE
      	console.log("ERROR 403 forbidden access error :");
      	console.log(error);
      	
      	response.writeHead(403);
        response.end("Sorry, cannot access resource : error: "+error.code+" ..\n");
        response.end();
        return;
		}
		
		

		const urlFile="." + url;
		
		let filePath=urlFile.indexOf("?")!==-1?urlFile.split("?")[0]:urlFile;
  	if(filePath=="./") filePath="./index.html";

  	let contentType="text/html";
    
		const headers={ "Content-Type": contentType };

		// To remove the CORS error message (cf. https://medium.com/@dtkatz/3-ways-to-fix-the-cors-error-and-how-access-control-allow-origin-works-d97d55946d9)
		if(addCORSHeader)		headers["Access-Control-Allow-Origin"]="*";
		
    
    fs.readFile(filePath, function(error, fileContent){
      if(error){
          if(error.code=="ENOENT"){
          	// TRACE
          	console.log("ERROR 404 file not found :"+filePath);
          	
            fs.readFile("./404.html", function(error, fileContent){
            	response.writeHead(200, headers);
              response.end(fileContent, "utf-8");
            });
              
          }else {

        	// TRACE
          	console.log("ERROR 500 server error :");
          	console.log(error);
          	
          	response.writeHead(500);
            response.end("Sorry, check with the site admin for error: "+error.code+" ..\n");
            response.end(); 
            
          }
      }else {
    	
    	// TRACE
      	console.log("INFO 200 OK :"+filePath);


      	response.writeHead(200, headers);
        response.end(fileContent, "utf-8");
        
  		// res.writeHead(200);
  		// res.end("hello world\n");
  		// res.sendFile(__dirname + "/public/index.html");
    		
      }
    });
      
	};
	
	
	const handler=nonull(httpHandlerParam, DEFAULT_HANDLER);
	
	
	
	let listenableServer;
	if(sslOptions){
		let httpsServer=https.createServer(sslOptions, handler).listen(port);
		// TRACE
		console.log("INFO : SERVER : HTTPS Server launched and listening on port " + port + "!");
		listenableServer=httpsServer;
	}else{
		let httpServer=http.createServer(handler).listen(port);
		// TRACE
		console.log("INFO : SERVER : HTTP Server launched and listening on port " + port + "!");
		listenableServer=httpServer;
	}
	
	
	const server=WebsocketImplementation.getStatic(true).getServer(listenableServer);
	
	// When a client connects, we execute the callback :
	// CAUTION : MUST BE CALLED ONLY ONCE !
	server.onConnectionToClient((serverParam, clientSocketParam)=>{
		if(doOnConnect) doOnConnect(serverParam, clientSocketParam);
	});


	server.onFinalize((serverParam)=>{
		
		// DBG
		lognow("onFinalize() server");
		
		if(doOnFinalizeServer)	doOnFinalizeServer(serverParam);
	});
	

	// TRACE
	console.log("INFO : SERVER : Generic Nodejs server launched and listening on port:" + port + "!");
	
	
	


	return server;
}


initNodeServerInfrastructureWrapper=function(doOnClientConnection=null, doOnFinalizeServer=null, /*OPTIONAL*/portParam, /*OPTIONAL*/certPathParam, /*OPTIONAL*/keyPathParam){

	// TRACE
	console.log("Server launched.");
	console.log("Usage :													node <server.js> conf {port:[port], sslCertPath:[ssl certificate path | unsecure ], sslKeyPath:[ssl key path], serverConfig:[JSON server configuration]}");
	console.log("Or (to generate password hash) :	node <server.js> hash <clientId@repositoryName> <clearTextSecretString>");
	// 			EXAMPLE : 							node orita-srv.js hash orita.global@keyHash 1234567890
	console.log("Server launched.");
	
	
	// We read the command-line arguments if needed :

	let argCLPort;
	let argCLCertPath;
	let argCLKeyPath;
	
	let serverConfig={};
	let isForceUnsecure;

	let isHashAsked=false;
	let clearTextParam=null;
	let persisterId=null;
	
	
	process.argv.forEach(function (val, i){
		if(!val)	return;
		// 0 corresponds to «node / nodejs»
		if(i<=1)	return; // 1 corresponds to « <server.js> »
		else if(i==2){
			if(val==="hash")		isHashAsked=true;
		}else if(i==3){
			if(!isHashAsked){
				try{
					const jsonConf=parseJSON(val);	
					argCLPort=jsonConf.port;
					argCLCertPath=jsonConf.sslCertPath;
					argCLKeyPath=jsonConf.sslKeyPath;
					serverConfig=nonull(jsonConf.serverConfig,{});
				}catch(err1){
					lognow("ERROR : Cannot parse argument JSON string «"+val+"».",err1);
				}
			}	else						persisterId=val;
		}else if(i==4){
			if(isHashAsked)		clearTextParam=val;
		}
	});
	isForceUnsecure=(argCLCertPath==="unsecure");





	const aotraNodeServer={config:serverConfig};
	aotraNodeServer.serverManager={ start:()=>{/*DEFAULT START FUNCTION, WILL BE OVERRIDEN LATER*/}};

	if(isHashAsked){
		// We instanciate a temporary persister just to read the key hash file:
		const persister=getPersister("./");
		let persisterIdSplits=persisterId.split("@");
		if(empty(persisterIdSplits) || persisterIdSplits.length!=2){
			// TRACE
			console.log("ERROR : No persister repository IDs provided correctly. Cannot read key hash. Aborting hash generation.");
			return aotraNodeServer;
		}
		const persisterClientId=persisterIdSplits[0];
		const persisterRepositoryName=persisterIdSplits[1];
		let globalKeyHashObject=persister.readTreeObjectFromFile(persisterClientId, persisterRepositoryName);
		if(!globalKeyHashObject || !globalKeyHashObject.keyHash){
			// TRACE
			console.log("WARN : No key hash found. Generating one now.");
			globalKeyHashObject={keyHash:getUUID(), hashes:[]};
			persister.saveDataToFileForClient(persisterClientId,persisterRepositoryName,globalKeyHashObject,false,()=>{
				// TRACE
				console.log("INFO : Key hash generated and saved successfully.");
			});
		}
		const globalKeyHash=globalKeyHashObject.keyHash;
		
		let firstHash=getHashedString(clearTextParam);
		
		let generatedHash=getHashedString( firstHash + globalKeyHash, "SHA-256", true);// (we use the heavy treatment thing.)
		globalKeyHashObject.hashes.push(generatedHash);

		// We update the repository :
		persister.saveDataToFileForClient(persisterClientId,persisterRepositoryName,globalKeyHashObject,false,()=>{
			// TRACE
			console.log("INFO : Hash added to repository and saved successfully.");
		});
		
		// OUTPUT
		console.log("Here is your key : share it with your main clients but DO NOT LEAK IT !\n********************\n"+clearTextParam+"\n********************\n");
		
		return aotraNodeServer;
	}

	
	const DEFAULT_PORT=nonull(argCLPort,25000);
	const DEFAULT_CERT_PATH=nonull(argCLCertPath,"cert.pem");
	const DEFAULT_KEY_PATH=nonull(argCLKeyPath,"key.key");

	
	let port=portParam ? portParam : DEFAULT_PORT;
	let certPath=null;
	let keyPath=null;

	if(!isForceUnsecure){
		certPath=certPathParam?certPathParam:DEFAULT_CERT_PATH;
		keyPath=keyPathParam?keyPathParam:DEFAULT_KEY_PATH;
	}

	
	// UNUSEFUL :
//aotraNodeServer.serverManager.microClientsSockets=[];
	// UNUSEFUL :
//aotraNodeServer.serverManager.mainClientsSockets=[];

	aotraNodeServer.serverManager.start=function(){

		// Eventual encryption options : 
		let sslOptions=null; 
		if(!isForceUnsecure){
			if(typeof(fs)==="undefined"){
				// TRACE
				lognow("ERROR : «fs» node subsystem not present, cannot access files. Aborting SSL configuration of server.");
			}else{
				try{
		  		sslOptions={
		  			cert: fs.readFileSync(certPath),
		  			key: fs.readFileSync(keyPath),
		  		};
				}catch(exception){
					// TRACE
					lognow("ERROR : Could not open SSL files certPath:«"+certPath+"» or keyPath:«"+keyPath+"». Aborting SSL configuration of server.");
				}
			}
		}
		
		aotraNodeServer.server=launchNodeHTTPServer(port, doOnClientConnection, doOnFinalizeServer, sslOptions);
		

		return aotraNodeServer;
	};

	return aotraNodeServer;
}

// ========================= FUSRODA SERVER : =========================

//
// DOES NOT WORK : USE Java FusrodaServer instead :
//
///*FUSRODA server stands from FSRD SERVER, for Fucking Simple Remote Desktop SERVER*/
//createFusrodaServer=function(certPathParam=null,keyPathParam=null,portParam=6080){
//	
//	// https://www.npmjs.com/package/screenshot-desktop
//	// https://github.com/octalmage/robotjs
//	// npm install --save screenshot-desktop
//	//
//	// sudo apt-get install libxtst-dev libx11-dev
//	// npm install --save robotjs
//	
//	// http://getrobot.net/docs/usage.html
//	// 
//	// apt-get install build-essential python libxt-dev libxtst-dev libxinerama-dev -y 
//	
//	const screenshot=require("screenshot-desktop");
//
//	const REFRESH_SCREENSHOTS_MILLIS=500;
//
//	
//	const server=initNodeServerInfrastructureWrapper(
//		// On each client connection :
// //	(serverParam, clientSocketParam)=>{},
//		null,
//		// On server finalization :
//		(serverParam)=>{
//
//			serverParam.receive("protocol_fusroda", (message, clientSocket)=> {
//				serverParam.addToRoom(clientSocket,"clients");
//			});
//			
//			
//			serverParam.sendScreenshotsRoutine=setInterval(()=>{
//				if(serverParam.isScreenshotStarted)	return;
//				
//				serverParam.isScreenshotStarted=true;		
//			
//				screenshot().then((img) => {
//	
//					const data={image:img,messageType:"imageData"};
//					serverParam.send("message", data, "clients");
//					
//					serverParam.isScreenshotStarted=false;		
//
//				}).catch((error) => {
//				  // TRACE
//					lognow("ERROR : Error during screenshot :",error);
//				});
//				
//			},REFRESH_SCREENSHOTS_MILLIS);
//			
//			
//		},portParam,certPathParam,keyPathParam);
//
//
// //	const doOnConnect=(serverParam, clientSocketParam)=>{
// //	};
// //	const doOnFinalizeServer=(serverParam)=>{
// //		/*DO NOTHING*/
// //	};
// //	const server={};
// //	server.start=(port=6080)=>{
// //		server.httpServer=launchNodeHTTPServer(port, doOnConnect, doOnFinalizeServer, sslOptions);
// //	};
//
//	return server;
//}


// ========================= UTILITY SERVERSIDE METHODS : =========================

class ListManager{
	
	constructor(config){
		this.config=config;
		this.maxItemsNumber=nonull(this.config.max,999);
		this.simultaneousItemsNumber=nonull(this.config.simultaneous,1);
		this.sessionDurationSeconds=nonull(this.config.duration,null);
		this.mode=nonull(this.config.mode,"startAtConnexion");
		
		this.itemsInfos={};
		this.time=null;
		this.started=false;
	}
	
	addItem(id,item){
		if(id==null){
			// TRACE
			lognow("ERROR : Cannot add item with no id.");
			return;
		}
		if(!item){
			// TRACE
			lognow("ERROR : Cannot add null item.");
			return;
		}
		const numberOfItemsCurrently=getArraySize(this.itemsInfos);

		// DBG
		lognow(">>>>>>>>>numberOfItemsCurrently:",numberOfItemsCurrently);
		lognow(">>>>>>>>>Object.keys(arrayOfValues):",Object.keys(this.itemsInfos));

		
		if(this.maxItemsNumber<=numberOfItemsCurrently){
			// TRACE
			lognow("ERROR : Cannot add item with id «"+id+"», list already full.");
			return;
		}
		
		
		if(numberOfItemsCurrently==0 && this.mode==="startAtConnexion"){
			
			// DBG
			lognow(">>>>>>>>>START SESSION !!!!");
			
			this.startSession();
		}
		this.itemsInfos[id]={
			item:item,
			time:getNow(),
		};
	}
	
	startSession(){
		this.time=getNow();
		this.started=true;
	}
	
	stopSession(){
		this.started=false;
	}
	
	isSessionActive(){
				
		// DBG
		lognow("				!!! this.sessionDurationSeconds : "+this.sessionDurationSeconds);
		lognow("				!!! this.started : "+this.started);
		lognow("				!!! this.time : "+this.time);

		
		if(!this.sessionDurationSeconds)	return true;
		if(!this.started)									return false;
		const result=!hasDelayPassed(this.time, this.sessionDurationSeconds*1000);
		
		// DBG
		lognow("				!!! HAS DELAY PASSED : "+result);
		
		return result;
	}
	
	isClientActive(clientId){
		
		// DBG
		lognow("			this.isSessionActive()"+this.isSessionActive());
		
		if(!this.isSessionActive())	return false;
		const clientPosition=this.getItemPosition(clientId);
		//CAUTION : Client position starts at 1 !
		const clientIndex=(clientPosition-1);
		const result=(clientIndex<=this.maxItemsNumber && clientIndex<=this.simultaneousItemsNumber);
		
		return result;
	}
	
	removeItemById(id){
		if(id==null){
			// TRACE
			lognow("ERROR : Cannot remove item, no id specified.");
			return;
		}
		if(!this.itemsInfos[id]){
			// TRACE
			lognow("ERROR : Cannot remove item, item not found for id «"+id+"».");
			return;
		}
		delete this.itemsInfos[id];
	}
	
	removeItem(item){
		if(item==null){
			// TRACE
			lognow("ERROR : Cannot remove item, none specified.");
			return;
		}
		
		let id=null;
		foreach(this.itemsInfos,(itemInfos,key)=>{
			if(itemInfos.item===item
					// DEBUG ONLY :
					|| (itemInfos.item.id && item.id && itemInfos.item.id===item.id)
			){
				id=key;
				return "break";
			}
			
		});
		
		if(!id){
			// TRACE
			lognow("ERROR : Cannot remove item, item not found for id «"+id+"».");
			return;
		}
		
		this.removeItemById(id);
	}

	// Goes from 1 to <length>:
	getItemPosition(id){
		if(id==null){
			// TRACE
			lognow("ERROR : Cannot calculate item position, no id specified.");
			return null;
		}
		if(!this.itemsInfos[id]){
			// TRACE
			lognow("ERROR : Cannot calculate item position, item not found for id «"+id+"».");
			return null;
		}
		let result=0;
		foreach(this.itemsInfos, (itemInfo, key)=>{
			result++;
			if(id===key)	return "break";
		},null,(item1, item2)=>item1.time<item2.time);
		return result;
	}

	getItemsNumber(){
		return getArraySize(this.itemsInfos);
	}
	
};

getListManager=function(config){
	return new ListManager(config);
};


// CAUTION ! TODO : FIXME : We replace in response all single quotes with "`" (egrave) in order to avoid errors !
// NO : IN A NODE CONTEXT WITH require("...") WILL RESULT IN AN UNDEFINED FUNCTION ERROR !!! 
// function performHTTPRequest(...){...
// USE THIS INSTEAD :
performHTTPRequest=function(completeURL,httpMethod="GET",headers={},requestBodyOrNamedArgs=null,isNodeContext=false,addCORSHeader=ADD_CORS_HEADER){
	
	if(contains(["POST","PUT"],httpMethod)){
		headers["Content-Type"]="application/json";
		// To remove the CORS error message (cf. https://medium.com/@dtkatz/3-ways-to-fix-the-cors-error-and-how-access-control-allow-origin-works-d97d55946d9)
		if(addCORSHeader)		headers["Access-Control-Allow-Origin"]="*";
	}	else if(httpMethod==="GET" && requestBodyOrNamedArgs){
		// Not the same way to send parameters in GET http method :
		// DBG
		lognow("unformatted API URL : "+completeURL);
		
		completeURL=appendGetParameters(completeURL, requestBodyOrNamedArgs);
		
		// DBG
		lognow("formatted API URL : "+completeURL);
	}
	
	// CASE BROWSER CONTEXT :
	if(!isNodeContext || typeof(require)=="undefined"){
		// TRACE
		lognow("INFO : We are not running in a browser context (isNodeContext:"+isNodeContext+";typeof(require):"+(typeof(require))+"). Using browser library.");
		
		const body=((contains(["POST","PUT"],httpMethod) && requestBodyOrNamedArgs)?JSON.stringify(requestBodyOrNamedArgs):null);
		return new Promise((resolve,reject)=>{
			fetch(completeURL, {
			   method: httpMethod,
			   headers: headers,
			   body: body,
			  })
				// STRANGE : DOES NOT WORK !!:
		    //.then(response => response.json())
		    // STRANGE : DOES WORK :
		   .then(response => {
			
		  	 	// DBG
		  	  console.log("~~~~~~~~~~~response :",response);
		  	  
					return response.json();
				}).then(data => {
			
					if(data.error){
						const error=data.error;
						// TRACE
						console.error("Error:", error);
						reject(""+error);
						return;
					}
			
		  	 	// DBG
		  	  console.log("~~~~~~~~~~~data :",data);
					
					resolve(data);
		   }).catch(error => {
					// TRACE
					console.error("Error:", error);
					reject(`${error}`);
		   });
		});
	}// else :
	
	// CASE NODEJS CONTEXT :
	
	// TRACE
	lognow("INFO : We are running in a nodejs context (isNodeContext:"+isNodeContext+";typeof(require):"+(typeof(require))+"). Using nodejs library.");

	
	const isSecure=(!empty(completeURL) && contains(completeURL.toLowerCase(),"https://"));
	const httpHandler=isSecure?require("https"):require("http");
	
	// Options for the HTTP request
	const options = {
	  url: completeURL,
	  method: httpMethod,
	};
	
	if(contains(["POST","PUT"],httpMethod)){
		options.json=true;
	}
	
  options.headers=headers;
  
	return new Promise((resolve,reject)=>{
		
		// Create the HTTP request
		// DOES NOT WORK : const request = httpHandler.request(options, (response) => {
		// UNLESS YOU SPECIFY in options : hostname, port, path
		const request = httpHandler.request(completeURL, options, (response) => {
		
		  let responseDataStr = "";

		  // A chunk of data has been received.
		  response.on("data", (chunk) => {
		    responseDataStr += chunk;
		  });
		
		  // The whole response has been received.
		  response.on("end", () => {
			
				try{
					let responseData;
					try{
						responseData=parseJSON(responseDataStr);
					}catch(error){
						// CAUTION ! TODO : FIXME : We replace in response all single quotes with "`" (egrave) in order to avoid errors !
						// DEBUG
						responseDataStr=responseDataStr.replace(/'/gim,"`");
						responseData=parseJSON(responseDataStr);
					}
					resolve( {responseData:responseData, response:response, responseDataStr:responseDataStr} );
				}catch(error){
					// DBG
					lognow("WARN : Could not JSON parse the response data ! Must deal with the string:«"+responseDataStr+"»", error);
					resolve( {response:response, responseDataStr:responseDataStr} );
					return;
				}
				
		  });
		});
		
		// Handle errors
		request.on("error", (error) => {
		  reject(error);
		});
		

		// Not the same way to send parameters in POST http method :
		if(contains(["POST","PUT"],httpMethod)){
			// (We need to stringify parameters or else we'll have an error :)
			if(!empty(requestBodyOrNamedArgs)){
				request.write(stringifyObject(requestBodyOrNamedArgs));
			}
		}
		
		// End the request
		request.end();

	});
	
};




replacePathVariablesNamesWithValuesIfPossible=function(apiURL, namedArgs){
	let result=apiURL;
	foreach(namedArgs,(namedArgValue, namedArgKey)=>{
		result=result.replace("{"+namedArgKey+"}",namedArgValue);
	});
	return result;
};

appendGetParameters=function(apiURL, namedArgs){
	let result=apiURL;

	const paramCouples=[];
	foreach(namedArgs,(value,key)=>{paramCouples.push(key+"="+value);});

	if(!empty(paramCouples)) result+=("?"+paramCouples.join("&"));

	return result;
};



//*********************************** AUTO-ORGANIZING REAL-TIME AORTAC CLUSTERIZATION (AORTAC) *********************************** */

AORTAC_OUTCOMING_SERVERS_CONNECTION_GLOBAL_TIMEOUT=30000;
REQUESTS_IDS_HISTORY_SIZE=10;

class AORTACNode{
	
	constructor(nodeId, selfOrigin="127.0.0.1:40000",outcomingNodesOrigins=[], model, controller, allowAnonymousNodes=true, sslConfig={/*OPTIONAL*/certPath:null,/*OPTIONAL*/keyPath:null}){
		
		this.nodeId=nodeId;
		this.selfOrigin=selfOrigin;
		this.sslConfig=sslConfig;
		this.outcomingNodesOrigins=outcomingNodesOrigins;
		this.allowAnonymousNodes=allowAnonymousNodes;
		
		this.model=model;						// must implement functions
		this.controller=controller; // Must implement functions
		
		this.server=null;
		this.clients={};
		this.incomingServers={};
		this.outcomingServers={};
		this.authorizedNodesIds=[this.nodeId];
		
		this.listeners={"protocol":[],"cluster":[],"neighbors":[],"inputs":[]};
	
		this.executedRequestIdsHistory=[];
		
		this.isConnectedToCluster=false;
		
		this.modelObjectsDirectory={};
		this.objectsIndexesForClients={};

		this.addProtocolListeners();

		// TRACE
		lognow(`AORTAC node created with id ${this.nodeId}.`);
	}
	
	start(){

		const splits=splitURL(this.selfOrigin);
		let port=nonull(splits.port,"40000");
		
		const self=this;
		this.server = initNodeServerInfrastructureWrapper(
			// On each client connection :
			null,
			// On client finalization :
			function(server){
				self.server=server;
				
				// Listeners handling ;
				foreach(Object.keys(self.listeners), channelName=>{
					self.server.receive(channelName, (message, clientSocket)=>{
						
						// INCOMING NODES LISTENERS HOOK :
						// TRACE
						console.log("INFO : SERVER : Client or incoming node has sent a "+channelName+" message: ", message);
						foreach(self.listeners[channelName], listener => {
							listener.execute(self, message, self.server, clientSocket);
						},(listener)=>(message.type===listener.messageType));
						
					});
				});
				
				// OLD : self.connectToOutcomingServers(self, server).then((server)=>{	self.doOnConnectedToCluster();	});
				
				
		}, port, this.sslConfig.certPath, this.sslConfig.keyPath);
		
		this.server.serverManager.start();
		
		// TRACE
		lognow(`AORTAC node started with id ${this.nodeId}.`);


		return this;
	}
	
	
	connect(){
		
		const self=this;
		self.connectToOutcomingServers(self).then((server)=>{	self.doOnConnectedToCluster();	});

		return this;		
	}
	
	
	
	
	/*private*/addProtocolListeners(){
		
		const self=this;
		// ============== OTHER SERVER / CLIENT REGISTRATION PHASE ==============
		// - OTHER SERVER REGISTRATION PHASE :
		this.listeners["protocol"].push({
			messageType:"request.register.server.incoming",
			execute:(self, message, server, clientSocket)=>{
				const incomingServerNodeId=message.nodeId;
				
				// TRACE
				lognow(`	(${self.nodeId}) Receiving registering request from node ${incomingServerNodeId}...`);
				
				if(!self.allowAnonymousNodes && self.isInAuthorizedNodes(incomingServerNodeId)){
					// TRACE
					lognow("WARN : Cannot accept incoming server with node id «"+incomingServerNodeId+"» because it's not in the authorized nodes list.");
					return;
				}
				if(!contains(self.authorizedNodesIds,incomingServerNodeId))	self.authorizedNodesIds.push(incomingServerNodeId);
				
				// TRACE
				lognow(`	(${self.nodeId}) Adding registering node ${incomingServerNodeId} to incoming servers...`);
				
				self.incomingServers[incomingServerNodeId]={clientSocket:clientSocket};
				
				const welcomeResponse={
					nodeId:self.nodeId, // MUST BE SELF ! ELSE THE REGISTERING CLIENT WON'T KNOW WHICH ID IT HAS BEEN REGISTERED TO !
					type:"response.register.server.incoming",
					authorizedNodesIds:self.authorizedNodesIds
				};
				server.send("protocol", welcomeResponse, null, clientSocket);
				
				// TRACE
				lognow(`	(${self.nodeId}) Sent registering response to node ${incomingServerNodeId}.`);
			}
		});
		
		// - CLIENT REGISTRATION PHASE : (for clients to the server, NOT INCOMING NODES !)
		this.handleClients();
		
	}
	
	/*private*/handleClients(){
		const self=this;
		
		// --------- PROTOCOL HANDLING ---------
		 
		this.listeners["protocol"].push({
			messageType:"request.register.client",
			execute:(self, message, server, clientSocket)=>{
				const clientId=message.clientId;
				
				self.clients[clientId]={clientSocket:clientSocket};

				// TRACE
				lognow(`	(${self.nodeId}) Receiving non-cluster client registration request from client ${clientId}.`);
				
				const welcomeClientResponse={
					nodeId:self.nodeId, // MUST BE SELF ! ELSE THE REGISTERING CLIENT WON'T KNOW WHICH ID IT HAS BEEN REGISTERED TO !
					type:"response.register.client",
					// If necessary we also send them our model part :
					partialModelString:(message.isReferenceNode?null:JSON.stringifyDecycle(self.model)),
					isReferenceNode:(!!message.isReferenceNode),
				};
				server.send("protocol", welcomeClientResponse, null, clientSocket);
			}
		});
		
		this.listeners["protocol"].push({
			messageType:"request.unregister.client",
			execute:(self, message, server, clientSocket)=>{
				const clientId=message.clientId;
				
				delete self.clients[clientId];

				// TRACE
				lognow(`	(${self.nodeId}) Receiving non-cluster client unregistration request from client ${clientId}.`);
				
				const unwelcomeClientResponse={
					nodeId:self.nodeId, // MUST BE SELF ! ELSE THE REGISTERING CLIENT WON'T KNOW WHICH ID IT HAS BEEN REGISTERED TO !
					type:"response.unregister.client",
				};
				server.send("protocol", unwelcomeClientResponse, null, clientSocket);
			}
		});
		
		
		this.listeners["protocol"].push({
			messageType:"request.interrogation.client",
			execute:(self, message, server, clientSocket)=>{
				const clientId=message.clientId;
				
				// TRACE
				lognow(`	(${self.nodeId}) Receiving non-cluster client interrogation request from client ${clientId}.`);
				
				const clientBoundaries=message.boundaries;
				
				// We ask the whole cluster to find the requested objects :
				const modelSeekObjectsRequest={
					type:"request.model.seekObjects",
					originatingClientId:clientId,
					clientBoundaries:clientBoundaries,
				};
				this.propagateToCluster(modelSeekObjectsRequest);

				self.objectsIndexesForClients[clientId]={};
				
			}
		});
		
		// --------- INPUTS HANDLING --------- 
		
		this.listeners["inputs"].push({
			messageType:"request.inputs.client",
			execute:(self, message, server, clientSocket)=>{
				const clientId=message.clientId;
				
				// TRACE
				lognow(`	(${self.nodeId}) Receiving non-cluster client inputs request from client ${clientId}.`);
				
				const clientInputs=message.inputs;
				const clientSubBoundaries=message.subBoundaries;

				
				// We let the controller interpret these inputs :
				const modifiedObjects=self.controller.interpretInputs(clientInputs, clientSubBoundaries);
				

			}
		});
		
	}


	/*private*/addPropagationForClientsListeners(){
		
		const self=this;
		
		// ------------------------------------- CLIENTS PROTOCOL HANDLING -------------------------------------
		// Adding propagation listener :
		this.setPropagation("cluster","request.model.seekObjects",(self, message, server, clientSocket)=>{
			
			// If we receive this request from cluster, then we try to find the requested objects
			const relayNodeid=message.nodeId;
			const originatingClientId=message.originatingClientId;
			const clientBoundaries=message.clientBoundaries
			const objectsIds=self.model.getObjectsIdsWithinBoundaries(clientBoundaries);

			// If server has not the seeked objects, it answers with an empty array.			
			
			// TRACE
			lognow(`(${self.nodeId}) Node receiving a seek objects request from relay node ${message.nodeId}...objectsIds=`,objectsIds);

			// TRACE
			lognow(`(${self.nodeId}) Sending objects ids to relay node ${relayNodeid}...`);
			
			const modelObjectsSeekResponse={
				type:"response.model.seekObject",
				originatingClientId:originatingClientId,
				clientBoundaries:clientBoundaries,
				objectsIds:objectsIds,
				nodeServerInfo:self.selfOrigin
			};
			this.propagateToCluster(modelObjectsSeekResponse, relayNodeid/*destination node*/);

		});
		
		// Adding propagation listener :
		this.setPropagation("cluster","response.model.seekObject",(self, message, server, clientSocket)=>{
			
			// Here the relay server gathers the information from the other nodes, and sends it back to client ; 
			
			const referenceNodeId=self.nodeId;
			const originatingClientId=message.originatingClientId;
			const originatingNodeId=message.nodeId;
			const clientBoundaries=message.clientBoundaries;
			const objectsIds=message.objectsIds;
			const nodeServerInfo=message.nodeServerInfo;
			
			// TRACE
			lognow(`(${self.nodeId}) Node receiving a seek objects request from relay node ${message.nodeId}...objectsIds=`,objectsIds);
			
			// Here the currently answering node fills its information :
			const objectsIndexForClient=self.objectsIndexesForClients[originatingClientId];
			objectsIndexForClient[originatingNodeId]={nodeServerInfo:nodeServerInfo, objectsIds:objectsIds};
			
			
			// We check if all known nodes have answered :
			if(getArraySize(self.authorizedNodesIds)-1/*Because we exclude the reference node*/<=getArraySize(objectsIndexForClient)){

				// The reference node also answers to the request to its direct client :
				const objectsIdsFromReferenceNode=self.model.getObjectsIdsWithinBoundaries(clientBoundaries);
				const referenceNodeServerInfo=self.selfOrigin;
				objectsIndexForClient[referenceNodeId]={nodeServerInfo:referenceNodeServerInfo, objectsIds:objectsIdsFromReferenceNode};
				
				const client=self.clients[originatingClientId];
				const clientSocket=client.clientSocket;

				// TRACE
				lognow(`(${self.nodeId}) Sending objects ids to the client ${originatingClientId}...objectsIndexForClient=`,objectsIndexForClient);
				
				// We filter all the index entries with an empty objetcs ids array :
				const objectsIndexForClientWithoutEmptyObjects={};
				foreach(objectsIndexForClient,(nodeInfoAndObjectsIds, serverNodeId)=>{
					objectsIndexForClientWithoutEmptyObjects[serverNodeId]=nodeInfoAndObjectsIds;
				},(nodeInfoAndObjectsIds,serverNodeId)=>(!empty(nodeInfoAndObjectsIds.objectsIds)));
				
				// We send the final result information (objects ids) to client :
				const interrogationClientResponse={
					nodeId:self.nodeId,
					type:"response.interrogation.client",
					serversNodesIdsForModelObjectsForClient:objectsIndexForClientWithoutEmptyObjects
				};
				self.server.send("protocol", interrogationClientResponse, null, clientSocket);
			}
			
		});
		
		
		// ------------------------------------- CLIENTS INPUTS HANDLING -------------------------------------
		// Adding propagation listener :

		
		
		// DBG
		lognow(">>>>>>>>FOR CLIENTS this.listeners",this.listeners);
		
	}

//	/*private*/getServersNodesIdsForModelObjectsForClient(clientObjectsIds){
//		const serversNodesIdsForModelObjectsForClient={};
//		const self=this;
//		// TODO : FIXME : INEFFICIENT !!!
//		foreach(clientObjectsIds,(objectId)=>{
//			foreach(self.modelObjectsDirectory,(objectsInNode, nodeId)=>{
//				if(contains(objectsInNode,objectId))
//					serversNodesIdsForModelObjectsForClient[objectId]=nodeId;
//			});
//		});
//		
//		return serversNodesIdsForModelObjectsForClient;
//	}

	/*private*/connectToOutcomingServers(self){
		
		const server=self.server;
	
		const theoreticNumberOfOutcomingServers=getArraySize(self.outcomingNodesOrigins);
		
		return new Promise((resolve,error)=>{
			
			// In case we timeout :
			setTimeout(()=>{
				if(self.isConnectedToCluster)	return;
				self.isConnectedToCluster=true;
				resolve(server);
			},AORTAC_OUTCOMING_SERVERS_CONNECTION_GLOBAL_TIMEOUT);
		
			// We try to connect to all the other outcoming servers :
			
			// Special case : if node has no outcoming serves, (it's the seed node), then 
			// it should be considered as connected to cluster nonetheless :
			if(empty(self.outcomingNodesOrigins)){
				self.finalizeClusterConnection(self.nodeId);
				resolve(server);
				return;
			}
			
			// We try to connect to all outcoming nodes : 
			foreach(self.outcomingNodesOrigins, outcomingNodeOrigin=>{
				
				// TRACE
				lognow(`	(${self.nodeId}) Sending registering response to node ${outcomingNodeOrigin}...`);
				
				const splits=splitURL(outcomingNodeOrigin);
				const outcomingNodeProtocol=nonull(splits.protocol,"ws");
				const outcomingNodeHost=nonull(splits.host,"localhost");
				const outcomingNodePort=nonull(splits.port,"40000");
				//const isSecure=splits.isSecure; // UNUSED
				
				const clientInstance=initClient(true, false, (socketToServer)=>{
					
					// TODO : FIXME : IF CONNECTION TO SERVER FAILS, IT MUST BE ABLE TO TRY AGAIN LATER !
					
					// TRACE
					lognow(`		(${self.nodeId}) Sending registering request to outcoming node...`);
					
					const helloRequest={
						nodeId:self.nodeId,
						type:"request.register.server.incoming"
					};
					socketToServer.send("protocol", helloRequest);
					
					
					// We place a listener from outcoming node on this client instance to this outcoming node :
					socketToServer.receive("protocol",(message)=>{
						if(message.type==="response.register.server.incoming"){
							
							// TRACE
							lognow(`		(${self.nodeId}) Receving registering response from requested outcoming node...`);
							
							const welcomeNodeId=message.nodeId;
							const duplicatesFreeAuthorizedNodesIds=[...new Set([...self.authorizedNodesIds, ...message.authorizedNodesIds, welcomeNodeId ])];
							self.authorizedNodesIds=duplicatesFreeAuthorizedNodesIds;
							
							self.outcomingServers[welcomeNodeId]={clientInstance:clientInstance};
							
							// Once we have registered to all the outcoming nodes :
							const currentNumberOfOutcomingServers=getArraySize(self.outcomingServers);
							if(!self.isConnectedToCluster && theoreticNumberOfOutcomingServers<=currentNumberOfOutcomingServers){
								//self.finalizeClusterConnection(welcomeNodeId);
								self.finalizeClusterConnection(self.nodeId);
								resolve(server);
							}
						}
					});
					
					// Listeners handling ;
					foreach(Object.keys(self.listeners), channelName=>{
						socketToServer.receive(channelName, (message, clientSocket)=>{
							
							// OUTCOMING NODES LISTENERS HOOK :
							// TRACE
							console.log("INFO : SERVER : Outcoming node has sent a "+channelName+" message: ", message);
							foreach(self.listeners[channelName], listener => {
								listener.execute(self, message, self.server, clientSocket);
							},(listener)=>(message.type===listener.messageType));
							
						});
					});
					
					
				}, outcomingNodeProtocol+"://"+outcomingNodeHost, outcomingNodePort, false);
				clientInstance.client.start();
				
			});
			
		});
	
	}
	
	/*private*/finalizeClusterConnection(welcomeNodeId){
		// TRACE
		lognow(`		(${this.nodeId}) Propagating this new node to the whole cluster...`);
		
		// We propagate the new node id to the whole cluster :
		const newNodeRequest={
			nodeId:welcomeNodeId,
			type:"request.node.new"
		};
		this.propagateToCluster(newNodeRequest);
		
		this.isConnectedToCluster=true;
		
		this.addPropagationListeners();
		this.addPropagationForClientsListeners();
	}
								
	

	
	/*public*/doOnConnectedToCluster(){
			
		// TRACE
		lognow(`(${this.nodeId}) Node is connected to cluster...`);
		
		// The node asks the cluster for its model partition :
		
		// At this point, the new node does not know the state of the model in the cluster, only cluster nodes know.
		
		// TRACE
		lognow(`(${this.nodeId}) Node is asking to its model partition...`);
		
		// New node asks for its model partition :
		
		// - First we need to know which server has the biggest model :
		const modelSizeRequest={
			type:"request.model.getSize",
		};
		this.propagateToCluster(modelSizeRequest);
			
	}
	
	
	/*private*/addPropagationListeners(){
		
		// Adding propagation listener for acknowledging new node added to cluster :
		this.setPropagation("cluster","request.node.new", (self, message, server, clientSocket)=>{
			const newNodeId=message.nodeId;
			// TRACE
			lognow(`		(${self.nodeId}) Acknowledging new node ${newNodeId} added to cluster...`);
			if(!contains(self.authorizedNodesIds,newNodeId))	self.authorizedNodesIds.push(newNodeId);
		});

		// Adding propagation listener if a node receives a model size request :
		this.setPropagation("cluster","request.model.getSize",(self, message, server, clientSocket)=>{
			const modelSize=self.model.getModelSize();
			// TRACE
			lognow(`(${self.nodeId}) Node gives its model size to node ${message.nodeId}, modelSize=${modelSize}...`);
			return modelSize;
		},"response.model.getSize");
		
		// Adding propagation listener :
		this.modelSizes={};
		this.setPropagation("cluster","response.model.getSize",(self, message, server, clientSocket)=>{
			
			const modelSizes=self.modelSizes;
			const currentClusterSize=getArraySize(self.authorizedNodesIds);
			
			const concernedNodeId=message.nodeId;
			const modelSize=message.result;
			
			let currentNumberOfAnswers=getArraySize(modelSizes);

			if(currentNumberOfAnswers<=currentClusterSize-1){// -1 because we want to exclude this node also !
				modelSizes[concernedNodeId]=modelSize;
			}

			currentNumberOfAnswers=getArraySize(modelSizes);
			// TRACE
			lognow(`(${self.nodeId}) Node receiving a model size from node ${message.nodeId}...currentClusterSize=${currentClusterSize} ; modelSizes:`,modelSizes);
			if(currentClusterSize-1<=currentNumberOfAnswers){
				const nodeIdWithBiggestModel=Math.maxInArray(modelSizes, true);
				// TRACE
				lognow(`(${self.nodeId}) Node with biggest model is nodeIdWithBiggestModel=${nodeIdWithBiggestModel}...`);
				
				const modelPartitionRequest={
					type:"request.model.partition",
				};
				this.propagateToCluster(modelPartitionRequest, nodeIdWithBiggestModel);
			}
		});
		
		// Adding propagation listener :
		// If a node receives a model partition request :
		this.setPropagation("cluster","request.model.partition",(self, message, server, clientSocket)=>{
			// Each node will give a little portion of its model, according to its size :					
			// TRACE
			lognow(`(${self.nodeId}) Node splits its model for node ${message.nodeId}...`);
			
			const ownModel=self.model;
			const splittedModel=ownModel.split();
			
			// Node shares also its model objects directory with the newcomer node :
			const ownModelObjectsIds=ownModel.getAllObjectsIds();
			self.modelObjectsDirectory[self.nodeId]=ownModelObjectsIds;
			
			const modelString=JSON.stringifyDecycle(splittedModel);
			const modelPartitionResponse={
				type:"response.model.partition",
				modelString:modelString,
				modelObjectsDirectory:self.modelObjectsDirectory
			};
			this.propagateToCluster(modelPartitionResponse, message.nodeId);
			
			// This node also advertises that its model has changed to all other nodes (not the newcomer, because it's unnecessary) :
			const updatemodelObjectsDirectoryRequest={
				type:"request.update.modelObjectsDirectory",
				ownModelObjectsIds:ownModelObjectsIds
			};
			this.propagateToCluster(updatemodelObjectsDirectoryRequest, null, [message.nodeId]);
			
		});

		// Adding propagation listener :
		this.setPropagation("cluster","response.model.partition",(self, message, server, clientSocket)=>{
			// TRACE
			lognow(`(${self.nodeId}) Node receives a model partition from node ${message.nodeId}...`,message);
			
			const newModel=JSON.parseRecycle(message.modelString);
			self.model.replaceBy(newModel);
			
			// We initialize this node's model objects directory from the one froom the node tha provided it its model partition :
			self.modelObjectsDirectory=message.modelObjectsDirectory;
			
			// Once this node has received its model partition, it updates its model objects directory
			const ownModelObjectsIds=newModel.getAllObjectsIds();
			self.modelObjectsDirectory[self.nodeId]=ownModelObjectsIds;
			
			// TRACE
			lognow(`(${self.nodeId}) Node sends a model objects directory update request...ownModelObjectsIds=${ownModelObjectsIds}`);
			
			// And notifies the other nodes of the change :
			const updatemodelObjectsDirectoryRequest={
				type:"request.update.modelObjectsDirectory",
				ownModelObjectsIds:ownModelObjectsIds
			};
			this.propagateToCluster(updatemodelObjectsDirectoryRequest);

		});
		
		this.addModelObjectsDirectoryListeners();
		
		
		// DBG
		lognow(">>>>>>>>FOR OTHER NODES this.listeners",this.listeners);
		
	}
	
	
	
	/*private*/addModelObjectsDirectoryListeners(){
		
		const self=this;
		
		// Adding propagation listener :
		// If a node receives a model objects directory update request :
		this.setPropagation("cluster","request.update.modelObjectsDirectory",(self, message, server, clientSocket)=>{
			
			// TRACE
			lognow(`(${self.nodeId}) !!!!!! Node receives a model objects directory update request from node ${message.nodeId}...`,message);
			
			const modelObjectsIds=message.ownModelObjectsIds;
			self.modelObjectsDirectory[message.nodeId]=modelObjectsIds;

		});
		
				
		// Adding propagation listener :
		// If a node receives a model objects directory update request :
		this.setPropagation("cluster","request.add.modelObjectsDirectory",(self, message, server, clientSocket)=>{
			
			// TRACE
			lognow(`(${self.nodeId}) !!!!!! Node receives a model objects directory add request from node ${message.nodeId}...`,message);
			
			const newObjectsIds=message.newObjectsIds;
			self.modelObjectsDirectory[message.nodeId].push(...newObjectsIds);

		});
		
	}
	
	
	/*public*/sendUpdatedObjects(modifiedObjects, modifiedObjectsIds){
				
		if(empty(modifiedObjects))	return modifiedObjects;

		// Then we send back the modified objects to all the concerned clients : (which are all clients connected to this node)
		return this.sendObjectsUpdatesToClients(modifiedObjects);
	}
	
	/*public*/sendNewObjects(newObjects, newObjectsIds){
		
		if(empty(newObjects))	return newObjects;

		// At this point, node controller has already added the objects to its model.
		// We just want to update the objects directories of all the nodes in the cluster:
		
		// !!! CAUTION : NOTE THAT THE EMITTER NODE IS ALWAYS INCLUDED IN THE THE VISITED NODES !!!
		// So this is why ir needs to update its mode objects directory itself, before sending the add request to the cluster :
		this.modelObjectsDirectory[this.nodeId].push(...newObjectsIds);

		// This node also advertises that its model has changed to all other nodes (not the newcomer, because it's unnecessary) :
		const updatemodelObjectsDirectoryRequest={
			type:"request.add.modelObjectsDirectory",
			newObjectsIds:newObjectsIds
		};
		this.propagateToCluster(updatemodelObjectsDirectoryRequest);
		
		
		// But we also want all the concerned clients to update their local models : (which are all clients connected to this node)
		return this.sendObjectsUpdatesToClients(newObjects, true);
	}
	
	/*private*/sendObjectsUpdatesToClients(modifiedOrAddedObjects, isAddingObjects=false){
		
		const self=this;
		foreach(this.clients, client=>{
			const clientSocket=client.clientSocket;
			const objectsModifiedOrAddedClientResponse={
				nodeId:self.nodeId,
				type:"response.objectsModifiedOrAdded.client",
				objectsString:JSON.stringifyDecycle(modifiedOrAddedObjects),
				isAddingObjects:isAddingObjects,
			};
			self.server.send("inputs", objectsModifiedOrAddedClientResponse, null, clientSocket);
		});

		return modifiedOrAddedObjects;
	}
	
	
	
	
	/*private*/getNeighborsNodesIds(){
		const neighborsNodesIds=[];
		foreach(this.incomingServers,(incomingServer,nodeId)=>{
			neighborsNodesIds.push(nodeId);
		});
		foreach(this.outcomingServers,(outcomingServer,nodeId)=>{
			neighborsNodesIds.push(nodeId);
		});
		return neighborsNodesIds;
	}
	
	/*private*/getLeastOccupiedNeighborNodeId(){
		const self=this;
	
		// We send the new objects to the neighbor node with the least amount of objects :
		let leastOccupiedNodeId=null;
		foreach(this.getNeighborsNodesIds(), (nodeId)=>{
			const entry=self.modelObjectsDirectory[nodeId];
			const numberOfObjects=getArraySize(entry);
			if(!leastOccupiedNodeId || numberOfObjects<getArraySize(self.modelObjectsDirectory[leastOccupiedNodeId]))
				leastOccupiedNodeId=nodeId;
		});
		
		if(!leastOccupiedNodeId){
			// TRACE
			lognow("WARN : Cannot find the least occupied node. Aborting.");
		}
		return leastOccupiedNodeId;
	}


	// ****************************************************************************************************
	/*private*/setPropagation(channelName, requestType, doOnReception=null, responseRequestType=null, propagateToAllCluster=true){
		const self=this;
		this.setUniqueReceptionPoint(channelName, requestType, (self, message, server, clientSocket)=>{
			
			const requestId=message.requestId;
			if(contains(self.executedRequestIdsHistory, requestId)){
				// TRACE
				lognow(`WARN : Request of type ${message.type} already answered. Aborting.`);
				return;
			}
			if(message.visitedNodeIds){
				if(contains(message.visitedNodeIds, self.nodeId)){
					return;
				}
				message.visitedNodeIds.push(self.nodeId);
			}
			
			const hasNoExclusionsOrExclusionDoesNotApplyOnThisNode=(!message.excludedNodesIds || !contains(message.excludedNodesIds,self.nodeId));
			let response=null;
			if(!message.destinationNodeId && hasNoExclusionsOrExclusionDoesNotApplyOnThisNode){
				// Case broadcast message :
				if(doOnReception)
					response=doOnReception(self, message, server, clientSocket);
				pushInArrayAsQueue(self.executedRequestIdsHistory, REQUESTS_IDS_HISTORY_SIZE, requestId);
				if(propagateToAllCluster)	self.sendToOtherNodes(channelName, message);		
			}else{
				if(message.destinationNodeId===self.nodeId && hasNoExclusionsOrExclusionDoesNotApplyOnThisNode){
					if(doOnReception)
						response=doOnReception(self, message, server, clientSocket);
					pushInArrayAsQueue(self.executedRequestIdsHistory, REQUESTS_IDS_HISTORY_SIZE, requestId);
				}else{
					if(propagateToAllCluster)	self.sendToOtherNodes(channelName, message);		
				}
			}
			
			// We send back the answer to the originating node :
			if(response!=null && responseRequestType){
				// TRACE
				lognow(`(${self.nodeId}) Node sending back a response of type ${responseRequestType}...`);				

				const clusterResponse={
					type:responseRequestType,
					result:response
				};
				const originNodeId=message.nodeId;
				if(propagateToAllCluster)	self.propagateToCluster(clusterResponse, originNodeId);
				else											self.propagateToSingleNeighbor(clusterResponse, originNodeId);
			}
			
		});
	}
	// ****************************************************************************************************
		
	/*private*/propagateToCluster(request, destinationNodeId=null, excludedNodesIds=null){
		request.requestId=getUUID();
		const originNodeId=this.nodeId;
		request.nodeId=originNodeId;
		if(destinationNodeId)	request.destinationNodeId=destinationNodeId;
		if(excludedNodesIds)	request.excludedNodesIds=excludedNodesIds;
		request.visitedNodeIds=[originNodeId];
		this.sendToOtherNodes("cluster",request);
	}
	
	/*private*/propagateToNeighbors(request){
		request.requestId=getUUID();
		const originNodeId=this.nodeId;
		request.nodeId=originNodeId;
		this.sendToOtherNodes("neightbors", request);
	}
	/*private*/propagateToSingleNeighbor(request, destinationNodeId){
		request.requestId=getUUID();
		const originNodeId=this.nodeId;
		request.nodeId=originNodeId;
		request.destinationNodeId=destinationNodeId;
		this.sendToOtherSingleNode("neightbors", request, destinationNodeId);
	}
	
	// ****************************************************************************************************

	/*private*/sendToOtherNodes(channelName, message){
		let hasBeenSentIncoming=this.sendToIncomingServers(channelName, message);
		let hasBeenSentOutcoming=this.sendToOutcomingServers(channelName, message);
		return (hasBeenSentIncoming || hasBeenSentOutcoming);
	}
	/*private*/sendToIncomingServers(channelName, message){
		const self=this;
		let hasBeenSent=false;
		foreach(this.incomingServers, (client)=>{
			const clientSocket=client.clientSocket;
			self.server.send(channelName, message, null, clientSocket);
			hasBeenSent=true;
		});
		return hasBeenSent;
	}
	/*private*/sendToOutcomingServers(channelName, message){
		let hasBeenSent=false;
		foreach(this.outcomingServers, (client)=>{
			const clientInstanceToOtherServer=client.clientInstance;
			clientInstanceToOtherServer.client.socketToServer.send(channelName, message);
			hasBeenSent=true;
		});
		return hasBeenSent;
	}
	/*private*/sendToOtherSingleNode(channelName, request, destinationNodeId){
		const self=this;
		let hasBeenSent=false;
		foreach(this.incomingServers, (client)=>{
			const clientSocket=client.clientSocket;
			self.server.send(channelName, message, null, clientSocket);
			hasBeenSent=true;
		},(c, nodeId)=>(nodeId===destinationNodeId));
		if(!hasBeenSent){
			foreach(this.outcomingServers, (client)=>{
				const clientInstanceToOtherServer=client.clientInstance;
				clientInstanceToOtherServer.client.socketToServer.send(channelName, message);
				hasBeenSent=true;
			},(c, nodeId)=>(nodeId===destinationNodeId));
		}
		return hasBeenSent;
	}

	/*private*/setUniqueReceptionPoint(channelName, messageType, doOnReception){
		const self=this;
		const listeners=this.listeners[channelName];
		if(foreach(listeners,listener=>{
			if(listener.messageType===messageType)	return true;
		}))	return;
		listeners.push({
			messageType:messageType,
			execute:(self, message, server, clientSocket)=>{
				doOnReception(self, message, server, clientSocket);
			}
		});
	}
	
	// ****************************************************************************************************

	/*private*/isInAuthorizedNodes(incomingServerNodeId){
		return contains(this.authorizedNodesIds,incomingServerNodeId);
	}
	
	/*public*/traceNode(){
		// TRACE
		lognow("-------------------------------------------------------------");
		lognow(`(${this.nodeId}) :`,this);
		lognow("-------------------------------------------------------------");
	}
	
	
	
}


getAORTACNode=function(nodeId=getUUID(), selfOrigin="ws://127.0.0.1:40000", outcomingNodesOrigins=[], model, controller){
	return new AORTACNode(nodeId, selfOrigin, outcomingNodesOrigins, model, controller);
}










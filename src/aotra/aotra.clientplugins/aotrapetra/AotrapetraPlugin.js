// GLOBAL CONSTANTS :

	

if(typeof initAotrapetraPluginClass ==="undefined"){
function initAotrapetraPluginClass() {
	var initSelf = this;

	
	// CONSTANTS
	var PLUGIN_NAME_DEFAULT="aotrapetra";

	
	initSelf.AotrapetraPlugin = Class.create({

		// Constructor
		initialize : function(pluginName) {

			
			// Attributes
			this.pluginName=pluginName?pluginName:PLUGIN_NAME_DEFAULT;
			this.overrideAotraScreenDisplay=false;

			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization started...-------------------");
			
			// TODO....
			
			
			
//			// Model
//			initMultiplicityModelClass();
//			initGameClass();
//			initGridClass();
//			initTileClass();
//			initBuildingClass();
//			initConnectionSectionClass();
//			initResourceClass();
//
//			// Controller
//			initMultiplicityControllerClass();
//			initPlaygroundManagerClass();
//			initResourceManagerClass();
//			initGameImporterClass();
//			initGameExporterClass();
//
//			// View
//			initMultiplicityViewClass();
//			
//			this.model=new MultiplicityModel();
//			this.controller=new MultiplicityController(this);
//			this.view=new MultiplicityView(this);
//			
			
			
			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization done.-------------------");
			
			
		}

		// Methods

	
		/*MANDATORY METHOD*/
		/*public*/,initPlugin:function(aotraScreen){
			return true;
		}
	
		/*MANDATORY METHOD*/
		,readFromXMLAotraScreenArgs:function(eAotraScreen){
			var results="";
			return results;
		}
		
		/*MANDATORY METHOD*/		
		,getXMLFromAotraScreenArgs:function(aotraScreen){
			return "";
		}

	

	});

	return initSelf;
}
}
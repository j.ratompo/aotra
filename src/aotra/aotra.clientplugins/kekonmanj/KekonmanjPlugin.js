// GLOBAL CONSTANTS :

	

if(typeof initKekonmanjPluginClass ==="undefined"){
function initKekonmanjPluginClass() {
	var initSelf = this;

	
	// CONSTANTS
	var PLUGIN_NAME_DEFAULT="kekonmanj";
	var TAG_NAME="kekonmanj";
	var OVERRIDE=false;

	
	initSelf.KekonmanjPlugin = Class.create({

		// Constructor
		initialize : function(pluginName) {

			
			// Attributes
			this.pluginName=pluginName?pluginName:PLUGIN_NAME_DEFAULT;
			this.overrideAotraScreenDisplay=OVERRIDE;

			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization started...-------------------");
			
			
			
			// Model
			initKekonmanjProfileClass();
			initKekonmanjStockClass();
			initKekonmanjItemClass();
			initKekonmanjItemTypeClass();
			initKekonmanjRecipeClass();
			
			
			// Controller
			initKekonmanjControllerClass();
			
			
			// View
			initKekonmanjViewClass();
			
			
			// Imitialization
			this.profile=new KekonmanjProfile();
			this.controller=new KekonmanjController(this);
			this.view=new KekonmanjView(this);
			
			
			
			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization done.-------------------");
			
		}

		// Methods
		
		
		/*MANDATORY METHOD*/
		/*public*/,initPlugin:function(aotraScreen){
			
			if(!aotraScreen.pluginsArgs[this.pluginName] || aotraScreen.pluginsArgs[this.pluginName]==="false")	return false;
			

			
			
			
			return true;
		}
		
		
		/*MANDATORY METHOD*/
		/*public*/,readFromXMLAotraScreenArgs:function(eAotraScreen){
			
			var results="";
			var plugin=false;//default value is false
	
			// ability to use the kekonmanj
			var aPlugin=eAotraScreen.attributes[TAG_NAME];
			if(aPlugin && aPlugin.value)	plugin=(aPlugin.value==="true");
			
			results+=""+plugin;
			return results;
		}
		
		
		/*MANDATORY METHOD*/		
		/*public*/,getXMLFromAotraScreenArgs:function(aotraScreen){
			var args=aotraScreen.pluginsArgs[this.pluginName];
			if(args && !empty(args) && args[0]===true)	return " "+TAG_NAME+"=\"true\"";
			return "";
		}

	
	

	});

	return initSelf;
}
}
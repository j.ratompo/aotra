
//TODO...


// GLOBAL CONSTANTS :


if(typeof initProfileExporterClass ==="undefined"){
function initProfileExporterClass() {
	var initSelf = this;

	// CONSTANTS
	
	
	initSelf.ProfileExporter = Class.create({

		// Constructor
		initialize : function(mode) {


			// Attributes
			this.mode=mode;

			this.exportMethods=new Object();
			this.exportMethods[JSON]=this.exportModelJSON;

		}

		// Methods
	
		// 	- Profile :
		// TODO :......................................................................................

	
	
		/*public*/,exportModel:function(modelObject){
			var m=this.exportMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	return m(this,modelObject);
			return null;
		}
		
		/*private*/,exportModelJSON:function(self,model){
			
			
			var game=model.getGame();
			
			var grid=game.getGrid();
			
			var result='{';
				
				result+='"lastGame":{';
				
					result+='"amounts":';
					result+=self.exportGameAmountsJSON(self,game);
					result+=',';

				
					result+='"grid":{';
						result+='"mold":"'+grid.getMold()+'",';
						result+='"maxTilesWidth":'+grid.getMaxTilesX()+',';
						result+='"maxTilesHeight":'+grid.getMaxTilesY()+',';
						
						result+='"tiles":';
						
						result+=self.exportTilesJSON(self,grid);
						

					result+='}';
				result+='}';
				
			result+='}';
			
			
			return result;
		}
		
		
		/*private*/,exportGameAmountsJSON:function(self,game){
			var result='{';
			
//			result+='"capacities":{';
//			var capacities=game.getcapacities();
//			var isFirst=true;
//			for(key in capacities){
//				if(!capacities.hasOwnProperty(key))	continue;
//				// JSON don't allow trailing commas :
//				if(isFirst) isFirst=false; else result+=",";
//				result+='"'+key+'":"'+capacities[key]+'"';
//			}
//			result+='},';
			
			
			result+='"moneys":{';
			var moneys=game.getMoneys();
			var isFirst=true;
			for(key in moneys){
				if(!moneys.hasOwnProperty(key))	continue;
				// JSON don't allow trailing commas :
				if(isFirst) isFirst=false; else result+=",";
				result+='"'+key+'":"'+moneys[key]+'"';
			}
			result+='},';

			
			result+='"resources":{';
			var resources=game.getResources();
			isFirst=true;
			for(key in resources){
				if(!resources.hasOwnProperty(key))	continue;
				// JSON don't allow trailing commas :
				if(isFirst) isFirst=false; else result+=",";
				result+='"'+key+'":"'+resources[key]+'"';
			}
			result+='},';
			

			result+='"populationIndicators":{';
			var populationIndicators=game.getPopulationIndicators();
			isFirst=true;
			for(key in populationIndicators){
				if(!populationIndicators.hasOwnProperty(key))	continue;
				// JSON don't allow trailing commas :
				if(isFirst) isFirst=false; else result+=",";
				result+='"'+key+'":"'+populationIndicators[key]+'"';
			}
			result+='},';
			
			
			
			
			result+='}';
			return result;
		}

		
		/*private*/,exportTilesJSON:function(self,grid){
			
			var result='';


			var tiles=grid.getTiles();
			
			result+='[';
			
			for(var x=0;x<tiles.length;x++){
				if(!tiles[x])	continue;
				
				// JSON don't allow trailing commas :
				result+=(result.length===1?"":",")+ '[';

				var isFirstElementOfArray=true;
				for(var y=0;y<tiles[x].length;y++){
			
					var tile=tiles[x][y];
					
					// JSON don't allow trailing commas :
					result+=(isFirstElementOfArray?"":",")+
						'{"tile":{';
					
						result+='"mold":"'+tile.getMold()+'",';
					
						result+='"attributes":';
						result+=self.exportTilesAttributesJSON(self,tile);
						
						if(!empty(tile.getThingsPlacedOnIt())){
							
							result+=',';
							
							result+='"thingsPlacedOnIt":';
							result+=self.exportTilesThingsPlacedOnItJSON(self,tile);
						}
						
					
					result+='}}';
				
					isFirstElementOfArray=false;
				}
				
				result+=']';
				
			}
			
			result+=']';
			
			return result;
		}
		
		/*private*/,exportTilesAttributesJSON:function(self,tile){
			
			var result='{';

			var attributes=tile.getAttributes();
			
				for(key in attributes){
					if(!attributes.hasOwnProperty(key))	continue;
					var attribute=attributes[key];
					
					// JSON don't allow trailing commas :
					result+=(result.length===1?"":",")+ '"'+key+'":"'+attribute+'"';

				}
			
			result+='}';
			
			return result;
		}


		/*private*/,exportTilesThingsPlacedOnItJSON:function(self,tile){
			
			var result='{';

			var things=tile.getThingsPlacedOnIt();
			
				for(key in things){
					if(!things.hasOwnProperty(key))	continue;
					var thing=things[key];
					
					
					
					// JSON don't allow trailing commas :
					result+=(result.length===1?"":",")+'"'+key+'":'+self.exportThingJSON(self,thing);
					
				}
			
			result+='}';
			
			return result;
		}
		
		
		/*private*/,exportThingJSON:function(self,thing){
			
			var result='{';

			
			// Exports flatly all the attributes of the thing placed on it object,
			// as long as it has a flat structure with no objects references :
			for(key in thing){
				if(!thing.hasOwnProperty(key))	continue;
				var attr=thing[key];
				
				// JSON don't allow trailing commas :
				result+=(result.length===1?"":",")+'"'+key+'":"'+attr+'"';
			}
			
			result+='}';
			
			return result;
		}

		
		

	});

	return initSelf;
}
}
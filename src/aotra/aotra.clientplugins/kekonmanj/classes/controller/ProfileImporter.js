
//TODO...


// GLOBAL CONSTANTS :


if(typeof initProfileImporterClass ==="undefined"){
function initProfileImporterClass() {
	var initSelf = this;

	// CONSTANTS
	
	
	initSelf.ProfileImporter = Class.create({

		// Constructor
		initialize : function(mode) {


			// Attributes
			this.mode=mode;

			this.initializeProfileMethods=new Object();
			this.initializeProfileMethods[JSON]=this.initializeProfileJSON;
			
			this.initGridMethods=new Object();
			this.initGridMethods[JSON]=this.getInitedGridJSON;
			
			this.parseMethods=new Object();
			this.parseMethods[JSON]=this.parseParamsJSON;

		}

		// Methods
	
		// 	- Profile :
		// TODO :......................................................................................

	
	
		/*public*/,initializeProfile:function(view,game,paramsObject){
			var m=this.initializeProfileMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	m(this,view,game,paramsObject);
			return;
		}
		
		
		/*private*/,initializeProfileJSON:function(self,view,game,paramsObject){
			
			
			var lastProfile=paramsObject.lastProfile;
			
			
			var amounts=lastProfile.amounts;
			
//			var capacitiesParam=amounts.capacities;
//			for(key in capacitiesParam){
//				if(!capacitiesParam.hasOwnProperty(key))	continue;
//				game.setCapacity(key,parseInt(capacitiesParam[key]));
//			}
			
			var moneysParam=amounts.moneys;
			for(key in moneysParam){
				if(!moneysParam.hasOwnProperty(key))	continue;
				game.setMoney(key,parseInt(moneysParam[key]));
			}
			
			var resourcesParam=amounts.resources;
			for(key in resourcesParam){
				if(!resourcesParam.hasOwnProperty(key))	continue;
				game.setResource(key,parseInt(resourcesParam[key]));
			}
			
			var populationIndicatorsParam=amounts.populationIndicators;
			for(key in populationIndicatorsParam){
				if(!populationIndicatorsParam.hasOwnProperty(key))	continue;
				game.setPopulationIndicator(key,parseInt(populationIndicatorsParam[key]));
			}
			
		}

		
		
		// 	- Grid :
	
		/*public*/,getInitedGrid:function(view,paramsObject){
			var m=this.initGridMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	return m(this,view,paramsObject);
			return null;
		}

		
		/*private*/,getInitedGridJSON:function(self,view,paramsObject){
			
			
			var grid=null;
			
			var lastProfile=paramsObject.lastProfile;
			var lastGrid=lastProfile.grid;
			var gridMold=lastGrid.mold;
			var gridMaxTilesWidth=lastGrid.maxTilesWidth;
			var gridMaxTilesHeight=lastGrid.maxTilesHeight;
			
			
			grid=new Grid(view,gridMold,gridMaxTilesWidth,gridMaxTilesHeight);
			
			var tiles=self.getTilesArrayJSON(self,grid,lastGrid);
			grid.setTiles(tiles);
			
			return grid;
			
			
		}
		
		
		/*private*/,getTilesArrayJSON:function(self,gridObject,gridParamsObject){
			
			var results=new Array();
			
			var tilesParams=gridParamsObject.tiles;
			
			for(var x=0;x<tilesParams.length;x++){
				if(!tilesParams[x])	continue;
				results.push(new Array());
				for(var y=0;y<tilesParams[x].length;y++){
					var tileParams=tilesParams[x][y].tile;

					var tile=new Tile(gridObject,tileParams.mold,x,y);
					var attrs=self.getTilesAttributesJSON(self,tileParams);
					tile.setAttributes(attrs);
					
					var things=self.getTilesThingsPlacedOnItJSON(self,tileParams);
					tile.setThingsPlacedOnIt(things);

					results[x].push(tile);
				}
			}
			
			
			return results;
		}

		/*private*/,getTilesAttributesJSON:function(self,tileParamsObject){
			var results=new Object();
			
			var tilesAttributesParams=tileParamsObject.attributes;
			
			for(key in tilesAttributesParams){
				if(!tilesAttributesParams.hasOwnProperty(key))	continue;
				var attributeParam=tilesAttributesParams[key];
				results[key]=attributeParam;
				
			}
			
			return results;
		}
		
		/*private*/,getTilesThingsPlacedOnItJSON:function(self,tileParamsObject){
			var results=new Object();
			
			var thingsParams=tileParamsObject.thingsPlacedOnIt;
			if(!thingsParams)	return results;
			
			for(key in thingsParams){
				if(!thingsParams.hasOwnProperty(key))	continue;
				var thingParams=thingsParams[key];
				
				var thing=self.getThingPlacedOnItJSON(self,key,thingParams);
				if(!thing)	continue;
				
				results[key]=thing;
				
			}
			
			return results;
		}
		
		
		/*private*/,getThingPlacedOnItJSON:function(self,type,thingParamsObject){

			var result=null;
			
			if(type===PLACED_THING_RESOURCE){
				
				var resourceType=thingParamsObject.type;
				var amount=parseFloat(thingParamsObject.amount);
				var regenerationAmount=(thingParamsObject.regenerationAmountPer10Seconds);
				result=new Resource(resourceType,amount,regenerationAmount);
			}else if(type===PLACED_THING_BUILDING){
				
				var buildingType=thingParamsObject.buildingType;
				var level=parseInt(thingParamsObject.level);
				result= new Building(buildingType,level);
			}	
			
			
			return result;
		
		}
		
		
		
		
		/*public*/,parseParams:function(modelString){
			var m=this.parseMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	return m(this,modelString);
			return null;
		}
		

		/*private*/,parseParamsJSON:function(self,modelString){
			return parseJSON(modelString);
		}
		

	});

	return initSelf;
}
}
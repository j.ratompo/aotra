// GLOBAL CONSTANTS :

	

if(typeof initMultiplicityPluginClass ==="undefined"){
function initMultiplicityPluginClass() {
	var initSelf = this;

	
	// CONSTANTS
	var PLUGIN_NAME_DEFAULT="multiplicity";
	var TAG_NAME="multiplicity";
	var OVERRIDE=true;

	
	initSelf.MultiplicityPlugin = Class.create({

		// Constructor
		initialize : function(pluginName) {

			
			// Attributes
			this.pluginName=pluginName?pluginName:PLUGIN_NAME_DEFAULT;
			this.overrideAotraScreenDisplay=OVERRIDE;

			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization started...-------------------");
			
			// Model
			initMultiplicityModelClass();
			initMultiplicityGameClass();
			initGridClass();
			initTileClass();
			initBuildingClass();
			initConnectionSectionClass();
			initResourceClass();

			// Controller
			initMultiplicityTileManagerClass();
			initMultiplicityControllerClass();
			initPlaygroundManagerClass();
			initResourceManagerClass();
			initMultiplicityGameImporterClass();
			initMultiplicityGameExporterClass();

			// View
			initMultiplicityViewClass();
			
			// Imitialization
			this.model=new MultiplicityModel();
			this.controller=new MultiplicityController(this);
			this.view=new MultiplicityView(this);
			
			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization done.-------------------");
			
			
		}

		// Methods
	
		/*public*/,getView:function(){
			return this.view;
		}
		
		/*public*/,getController:function(){
			return this.controller;
		}
		
		/*public*/,getModel:function(){
			return this.model;
		}
		
		
		/*MANDATORY METHOD*/
		/*public*/,initPlugin:function(aotraScreen){
			
			
			if(!aotraScreen.pluginsArgs[this.pluginName] || aotraScreen.pluginsArgs[this.pluginName]==="false")	return false;
			
			var game=this.model.getGame();

			// OLD :
//			// First we put all the needed information
//			this.controller.initGame(this.view,game);
//			// Then we have to init the view
//			this.view.init(aotraScreen);

			
			// First we have to init the view
			this.view.init(aotraScreen);

			// Then we put all the needed information
			this.controller.initGame(this.view,game);
			

			
			
			// At last, we perform the first drawing of the view :
			this.view.draw();
			
			return true;
		}
		
		
		/*MANDATORY METHOD*/
		/*public*/,readFromXMLAotraScreenArgs:function(eAotraScreen){
			
			var results="";
			var plugin=false;//default value is false

			// ability to play the multiplicity game
			var aPlugin=eAotraScreen.attributes[TAG_NAME];
			if(aPlugin && aPlugin.value)	plugin=(aPlugin.value==="true");
			
			results+=""+plugin;
			return results;
		}
		
		/*MANDATORY METHOD*/		
		/*public*/,getXMLFromAotraScreenArgs:function(aotraScreen){
			var args=aotraScreen.pluginsArgs[this.pluginName];
			if(args && !empty(args) && args[0]===true)	return " "+TAG_NAME+"=\"true\"";
			return "";
		}
		

	
	
	
	

	});

	return initSelf;
}
}
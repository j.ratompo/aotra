


// GLOBAL CONSTANTS :

	var RANDOM="random";

if(typeof initMultiplicityControllerClass ==="undefined"){
function initMultiplicityControllerClass() {
	var initSelf = this;

	// CONSTANTS
	var DEFAULT_MODE="random";
	var DEFAULT_MODEL_FORMAT="JSON";
	var DEFAULT_SAVE_GAME_EVERY_MILLIS=5000;
	
	var STORED_MODEL_STRING_NAME="multiplicityModelString";
	
	
	initSelf.MultiplicityController = Class.create({

		// Constructor
		initialize : function(plugin) {


			// Attributes
			this.plugin=plugin;

			// Technical attributes
						
			this.tileManager=null;
			
			
			
		}

		// Methods

		
		// Initialization
		/*public*/,initGame:function(view,game){
			
			var modelStringFromStorage=this.getModelStringFromStorage();
			
			
			this.initializeGame(view,game,modelStringFromStorage);
			
			
			this.startSavingRoutine(DEFAULT_SAVE_GAME_EVERY_MILLIS);
		
			// Initialization of sub-modules of this controller :
			this.tileManager=new MultiplicityTileManager(this.plugin);
			

		}


		/*private*/,initializeGame:function(view,game,/*NULLABLE*/modelStringFromStorageParam){

			var grid=null;
			
			// If the map is brand new :
			var isCreateNew=empty(modelStringFromStorageParam);
			
			
			if(isCreateNew){
				
				grid=new Grid(view);
				
				this.playgroundManager=new PlaygroundManager(DEFAULT_MODE);
				this.playgroundManager.initGrid(grid);
	
				this.resourceManager=new ResourceManager(DEFAULT_MODE);
				this.resourceManager.populateGrid(grid);
	
			}else{

				this.gameImporter=new MultiplicityGameImporter(DEFAULT_MODEL_FORMAT);

				var parsedParams=this.gameImporter.parseParams(modelStringFromStorageParam);
				if(!parsedParams){
					this.initializeGame(view,game,null);
					return;
				}


				this.gameImporter.initializeGame(view,game,parsedParams);
				grid=this.gameImporter.getInitedGrid(view,parsedParams);
				
			}
			
			this.gameExporter=new MultiplicityGameExporter(DEFAULT_MODEL_FORMAT);

			game.setGrid(grid);

			
		}

		

		/*private*/,startSavingRoutine:function(savingGameEveryMillis){
			
			var self=this;
			var pe=new PeriodicalExecuter(function() {
				
				var model=self.plugin.getModel();
				var modelString=self.gameExporter.exportModel(model);
				
				self.setModelStringToStorage(modelString);
				
			}, savingGameEveryMillis*.001);
			
		}

		
		/*private*/,getModelStringFromStorage:function(){
			return getStringFromStorage(STORED_MODEL_STRING_NAME);
		}
		
		/*private*/,setModelStringToStorage:function(modelString){
			return storeString(STORED_MODEL_STRING_NAME,modelString);
		}



	});

	return initSelf;
}
}



// GLOBAL CONSTANTS :


if(typeof initMultiplicityTileManagerClass ==="undefined"){
function initMultiplicityTileManagerClass() {
	var initSelf = this;

	// CONSTANTS
	
	
	initSelf.MultiplicityTileManager = Class.create({

		// Constructor
		initialize : function(plugin) {


			// Attributes


			this.selectedTile=null;
			
			
			// Technical attributes
			this.plugin=plugin;
			
			this.initUIElements(plugin.getView());

			
		}

		// Methods
	
		// Initialization
	
		/*private*/,initUIElements:function(view){
			
			view.addAction("clearTile",i18n({fr:"défricher",en:"clear"}),this.clearTile,true);
			
			
			
		}

		
		/*public*/,clearTile:function(){
			
			// TODO ...
			
			
		}

		
		/*public*/,setSelectedTile:function(tile){
			
			var view=this.plugin.getView();
			this.selectedTile=tile;
			
			view.refreshActionsDisplays();
		}

		
		


	});

	return initSelf;
}
}
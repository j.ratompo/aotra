// GLOBAL CONSTANTS :

	var PLAYGROUND_TYPES={
         "A":{"index":"1","color":"cyan"}
		,"B":{"index":"2","color":"green"}
		,"C":{"index":"3","color":"yellow"}
		,"D":{"index":"4","color":"blue"}
	};
	

if(typeof initPlaygroundManagerClass ==="undefined"){
function initPlaygroundManagerClass() {
	var initSelf = this;

	// CONSTANTS

	
	initSelf.PlaygroundManager = Class.create({

		// Constructor
		initialize : function(mode) {


			// Attributes
			this.mode=mode;

			this.initMethods=new Object();
			this.initMethods[RANDOM]=this.initGridRandom;

			
		}

		// Methods
		/*public*/,initGrid:function(grid){
			var m=this.initMethods[this.mode];
			if(m)	m(grid);
		}

		/*private*/,initGridRandom:function(grid){
			
			var tiles=grid.getTiles();
			
			for(var x=0;x<tiles.length;x++){
				if(!tiles[x])	continue;
				for(var y=0;y<tiles[x].length;y++){
					var tile=tiles[x][y];
					
					var randInt=Math.getRandomInt(4);// (random integer values from 1 to 4)
					
					var playgroundType="";
					for(key in PLAYGROUND_TYPES){
						if(!PLAYGROUND_TYPES.hasOwnProperty(key))	continue;
						if(PLAYGROUND_TYPES[key].index===(""+randInt)){ // STRINGS ONLY
							playgroundType=key;
							break;
						}
					}
					
					tile.setAttribute("playgroundType",playgroundType);
					
				}
			}
			
		}

		

	});

	return initSelf;
}
}
// GLOBAL CONSTANTS :


if(typeof initResourceManagerClass ==="undefined"){
function initResourceManagerClass() {
	var initSelf = this;

	// CONSTANTS
	var INTERVAL_SEPARATOR="-";
	
	initSelf.ResourceManager = Class.create({

		// Constructor
		initialize : function(mode) {


			// Attributes
			this.mode=mode;

			this.populateMethods=new Object();
			this.populateMethods[RANDOM]=this.populateGridRandom;
			
			
		}

		// Methods
		/*public*/,populateGrid:function(grid){
			var m=this.populateMethods[this.mode];
			if(m)	m(this,grid);
		}

		/*private*/,populateGridRandom:function(self,grid){
			
			var tiles=grid.getTiles();
			
			for(var x=0;x<tiles.length;x++){
				if(!tiles[x])	continue;
				for(var y=0;y<tiles[x].length;y++){
					var tile=tiles[x][y];
					
					var rt=self.getRandomResourceType(self);
					if(!rt)	continue;
					var r=self.getNewResourceFromResourceType(self,rt);
					
					tile.addThingOnIt(PLACED_THING_RESOURCE,r);
					tile.setAttribute("buildable","false");
					
				}
			}
			
		}
		
		
		/*private*/,getRandomResourceType:function(self){
			
			var result=null;
			var minAbundancyPercentage=100;

			var randInt=Math.getRandomInt(100);// (random integer values from 1 to 100)

			
			for(key in BRUTE_RESOURCES_TYPES){
				if(!BRUTE_RESOURCES_TYPES.hasOwnProperty(key))	continue;
				var rt=BRUTE_RESOURCES_TYPES[key];
				
				var abundancePercentage=parseInt(rt.abundancePercentage);
				if(randInt<=abundancePercentage && abundancePercentage<minAbundancyPercentage){
					minAbundancyPercentage=abundancePercentage;
					result=rt;
				}

			}
			
			return result;
		}
		
		/*private*/,getNewResourceFromResourceType:function(self,resourceType){

			var type=resourceType.type;
			
			var amountIntervalStr=resourceType.amountInterval;
			var split=amountIntervalStr.split(INTERVAL_SEPARATOR);
			var amountIntervalMin=parseFloat(split[0]);
			var amountIntervalMax=parseFloat(split[1]);
			var amount=Math.getRandomInt(amountIntervalMax,amountIntervalMin);
			
			var regenerationAmount=parseFloat(resourceType.regenerationAmount);
			
			var result=new Resource(type,amount,regenerationAmount);
			
			return result;
			
		}


	});

	return initSelf;
}
}
// GLOBAL CONSTANTS :

	
	var POPULATION_INDICATORS_TYPES={
		"number":{},
		"education":{},
		"wealth":{},
		"health":{},
		"wellbeing":{},
		"sexEquality":{}
	};


	// Moneys :
	var DOLLEUROS="dolleuros";
	
//	// Capacities :
//	var ENERGY="energy";
//	var WATER="water";
	

if(typeof initMultiplicityGameClass ==="undefined"){
function initMultiplicityGameClass() {
	var initSelf = this;

	
	// CONSTANTS
	var FIRST_MONEY_AMOUNT_DOLLEUROS=10000;
	
	
//	var POPULATION_NUMBER="number";
//	var POPULATION_EDUCATION="education";
//	var POPULATION_HEALTH="health";
//	var POPULATION_WELLBEING="wellBeing";

	
	initSelf.MultiplicityGame = Class.create({

		// Constructor
		initialize : function() {


			// Attributes
			this.grid=null;
			
			
			// 	- player's all resources stocks, and indicators :
			
			this.moneys=new Object();
			this.moneys[DOLLEUROS]=FIRST_MONEY_AMOUNT_DOLLEUROS;
			
			this.resources=new Object();
			for(key in BRUTE_RESOURCES_TYPES){
				if(!BRUTE_RESOURCES_TYPES.hasOwnProperty(key))	continue;
				this.resources[key]=0;
			}
			for(key in TRANSFORMED_RESOURCES_TYPES){
				if(!TRANSFORMED_RESOURCES_TYPES.hasOwnProperty(key))	continue;
				this.resources[key]=0;
			}


			
			this.populationIndicators=new Object();
			for(key in POPULATION_INDICATORS_TYPES){
				if(!POPULATION_INDICATORS_TYPES.hasOwnProperty(key))	continue;
				this.populationIndicators[key]=0;
			}
			
			
			// Technical attributes :
			this.lastPlayedDate=new Date().getTime();
			
		}

	
		// Methods

		/*public*/,getMoneys:function(){
			return this.moneys;
		}
		/*public*/,getMoney:function(/*OPTIONAL*/name){
			if(!name)	return this.moneys[DOLLEUROS];
			return this.moneys[name];
		}
		/*public*/,setMoney:function(/*NULLABLE*/name,value){
			if(!name)	this.moneys[DOLLEUROS]=value;
			this.moneys[name]=value;
		}
		/*public*/,getResources:function(){
			return this.resources;
		}
		/*public*/,getResource:function(name){
			return this.resources[name];
		}
		/*public*/,setResource:function(name,value){
			this.resources[name]=value;
		}
		/*public*/,getPopulationIndicators:function(){
			return this.populationIndicators;
		}
		/*public*/,getPopulationIndicator:function(name){
			return this.populationIndicators[name];
		}
		/*public*/,setPopulationIndicator:function(name,value){
			this.populationIndicators[name]=value;
		}

	

		/*public*/,getGrid:function(){
			return this.grid;
		}
		
		/*public*/,setGrid:function(grid){
			this.grid=grid;
		}
		
	});

	return initSelf;
}
}
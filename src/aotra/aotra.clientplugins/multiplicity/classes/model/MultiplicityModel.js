// GLOBAL CONSTANTS :

	

if(typeof initMultiplicityModelClass ==="undefined"){
function initMultiplicityModelClass() {
	var initSelf = this;

	
	initSelf.MultiplicityModel = Class.create({

		// Constructor
		initialize : function() {


			// Attributes
			
			this.currentGame=new MultiplicityGame();

			
		}
	
		// Methods
	
	

		/*public*/,getGame:function(){
			return this.currentGame;
		}
		
		/*public*/,getGrid:function(){
			return this.currentGame.getGrid();
		}

	});

	
	
	return initSelf;
}
}
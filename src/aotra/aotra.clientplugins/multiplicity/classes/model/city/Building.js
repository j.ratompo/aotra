// GLOBAL CONSTANTS :

// Structure of these buildings types configurations :
//	-activitySector
//	-moneysBuildingCosts
//	-pollutionsBuildingCosts
//	-resourcesBuildingCosts
//	-capacitiesUsages
//	 capacitiesProvidings
//	-populationsBySectorProviding
//	 populationsBySectorUsage
//	-resourceConsumptionsPerCycle		(DONE)
//	 resourcesProvidingPerCycle			(DONE)
//	-pollutionsProducingPerCycle
//	 pollutionsResorbingPerCycle
//	-pollutionsProducingAreaSize
//	 pollutionsResorbingAreaSize


var MAX_BUILDINGS_LEVEL = 5;
var MULTIPLICITY_BUILDINGS_IMAGES_ROOT=MULTIPLICITY_IMAGES_ROOT+"universe/land/objects/buildings/";
var BUILDINGS_TYPES = {

	"residentialArea" : {
		"type" : "residentialArea"
		,"name" : "Residential area"
		,"imageLogo":""
		,"image" : {
			"" : "",
			"isometric_square_mold" : MULTIPLICITY_BUILDINGS_IMAGES_ROOT+"residentialArea/residentialArea.gif"
		}

		,moneysBuildingCosts : {}

		// -pollutionsBuildingCosts
		// -resourcesBuildingCosts
		// -capacitiesUsages
		// capacitiesProvidings
		// -populationsBySectorProviding
		// populationsBySectorUsage
		// -pollutionsProducingPerCycle
		// pollutionsResorbingPerCycle
		// -pollutionsProducingAreaSize
		// pollutionsResorbingAreaSize

		,"resourceConsumptionsPerCycle" : {
			1 : {
				"cereals" : "2"
				,"meat" : "0"
			},
			2 : {
				"cereals" : "4"
				,"meat" : "2"
			},
			3 : {
				"cereals" : "6"
				,"meat" : "4"
			},
			4 : {
				"cereals" : "8"
				,"meat" : "6"
			},
			5 : {
				"cereals" : "10"
				,"meat" : "8"
			}
		},
		"resourcesProvidingPerCycle" : {}

	},

	"resourceExtractor" : {
		"trees" : {
			"type" : "trees"
			,"name" : "Sawmill"
			,"imageLogo":""
			,"image" : {
				"" : "",
				"isometric_square_mold" : MULTIPLICITY_BUILDINGS_IMAGES_ROOT+"treesExploitation/treesExploitation.gif"
			}

			,
			"resourceConsumptionsPerCycle" : {}
			,"resourcesProvidingPerCycle" : {
				1 : "2",
				2 : "4",
				3 : "6",
				4 : "8",
				5 : "10"
			}
		},
		"iron" : {
			"type" : "iron"
			,"name" : "Iron mine"
			,"imageLogo":""
			,"image" : {
				"" : "",
				"isometric_square_mold" : MULTIPLICITY_BUILDINGS_IMAGES_ROOT+"mineralExploitation/ironMine/ironMine.gif"
			}

			,
			"resourceConsumptionsPerCycle" : {
				1 : {
					"oil" : "2"
				},
				2 : {
					"oil" : "3"
				},
				3 : {
					"oil" : "4"
				},
				4 : {
					"oil" : "5"
				},
				5 : {
					"oil" : "6"
				}
			},
			"resourcesProvidingPerCycle" : {
				1 : "1",
				2 : "2",
				3 : "3",
				4 : "4",
				5 : "5"
			}
		},
		"copper" : {
			"type" : "copper"
			,"name" : "Copper mine"
		   ,"imageLogo":""
			,"image" : {
				"" : "",
				"isometric_square_mold" : MULTIPLICITY_BUILDINGS_IMAGES_ROOT+"mineralExploitation/copperMine/copperMine.gif"
			}

			,
			"resourceConsumptionsPerCycle" : {
				1 : {
					"oil" : "2"
				},
				2 : {
					"oil" : "3"
				},
				3 : {
					"oil" : "5"
				},
				4 : {
					"oil" : "7"
				},
				5 : {
					"oil" : "8"
				}
			},
			"resourcesProvidingPerCycle" : {
				1 : "2",
				2 : "5",
				3 : "7",
				4 : "8",
				5 : "10"
			}
		},
		"oil" : {
			"type" : "oil"
			,"name" : "Oil extracting platform"
			,"imageLogo":""
			,"image" : {
				"" : "",
				"isometric_square_mold" : MULTIPLICITY_BUILDINGS_IMAGES_ROOT+"oilExploitation/oilExploitation.gif"
			}

			,
			"resourceConsumptionsPerCycle" : {
				1 : {
					"trees" : "1"
				},
				2 : {
					"trees" : "2"
				},
				3 : {
					"trees" : "3"
				},
				4 : {
					"trees" : "4"
				},
				5 : {
					"trees" : "5"
				}
			},
			"resourcesProvidingPerCycle" : {
				1 : "5",
				2 : "10",
				3 : "15",
				4 : "20",
				5 : "25"
			}
		},
	}
};

var ACTIVITY_SECTORS = {
	"health" : {},
	"socialServices" : {},
	"industry" : {},
	"services" : {},
	"research" : {},
};

if (typeof initBuildingClass === "undefined") {
	function initBuildingClass() {
		var initSelf = this;

		initSelf.Building = Class.create({

			// Constructor
			initialize : function(buildingType, level) {

				// Attributes
				this.buildingType = buildingType ? buildingType : "residentialArea";
				this.level = level ? Math.min(MAX_BUILDINGS_LEVEL, level) : 1;

			}

		// Methods

		});

		return initSelf;
	}
}
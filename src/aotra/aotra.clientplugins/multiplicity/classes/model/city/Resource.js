// GLOBAL CONSTANTS :

	// TODO : FIXME : At this time, and because of the random resource population implementation,
	// abundance percentages MUST NEVER BE IDENTICAL !!
	var MULTIPLICITY_RESOURCES_IMAGES_ROOT=MULTIPLICITY_IMAGES_ROOT+"universe/land/playground/resources/";
	var BRUTE_RESOURCES_TYPES={
    	 "trees": {"type":"trees","index":"1","name":"Forest area"
			 ,"imageLogo":""
    		 ,"image":{
    			"":"",
    			"isometric_square_mold":MULTIPLICITY_RESOURCES_IMAGES_ROOT+"trees/trees.gif"
    		 }
    		,"abundancePercentage":"40","amountInterval":"60-300","regenerationAmount":"0.1"}
		,"iron":  {"type":"iron","index":"2","name":"Iron deposit"
			 ,"imageLogo":""
			 ,"image":{
				"":"",
				"isometric_square_mold":MULTIPLICITY_RESOURCES_IMAGES_ROOT+"iron/iron.gif"
			}
			,"abundancePercentage":"2","amountInterval":"20-30","regenerationAmount":"0"}
		,"copper":{"type":"copper","index":"3","name":"Copper deposit"
			 ,"imageLogo":""
			 ,"image":{
				"":"",
				"isometric_square_mold":MULTIPLICITY_RESOURCES_IMAGES_ROOT+"copper/copper.gif"
			 }
			,"abundancePercentage":"1","amountInterval":"1-10","regenerationAmount":"0"}
		,"oil":   {"type":"oil","index":"4","name":"Oil layer"
			 ,"imageLogo":""
			 ,"image":{
				"":"",
				"isometric_square_mold":MULTIPLICITY_RESOURCES_IMAGES_ROOT+"oil/oil.gif"
			 }
			,"abundancePercentage":"5","amountInterval":"10-50","regenerationAmount":"0.001"}
		
	};

	var TRANSFORMED_RESOURCES_TYPES={

    	 "cereals": {"type":"cereals","index":"1","name":"Cereal"
    		 ,"imageLogo":""
    	}
		,"meat": {"type":"meat","index":"2","name":"Meat"
			 ,"imageLogo":""
		}
		,"fish": {"type":"fish","index":"3","name":"Fish"
			 ,"imageLogo":""
		}
		,"vegetables": {"type":"vegetables","index":"4","name":"Vegetables"
			 ,"imageLogo":""
		}
		,"fruits": {"type":"fruits","index":"5","name":"Fruits"
			 ,"imageLogo":""
		}
		,"dairies": {"type":"dairies","index":"6","name":"Dairies"
			 ,"imageLogo":""
		}
		,"plastic":  {"type":"paper","index":"7","name":"Paper"
		   	,"imageLogo":""
		}
		,"paper":  {"type":"paper","index":"8","name":"Paper"
		   	,"imageLogo":""
		}
			
	};

	
	
	
	
	var POLLUTIONS_TYPES={
		 "air":{"type":"air","name":"Air"}
		,"water":{"type":"water","name":"Water"}
		,"earth":{"type":"earth","name":"Earth"}
		,"radioactivity":{"type":"radioactivity","name":"Ambiant radioactivity"}
		,"sound":{"type":"sound","name":"Ambiant sound"}
		,"sight":{"type":"sight","name":"Ambiant visual environment"}
	};
	

if(typeof initResourceClass ==="undefined"){
function initResourceClass() {
	var initSelf = this;

	var DEFAULT_TYPE="trees";
	var DEFAULT_AMOUNT=0;
	var DEFAULT_REGENERATION_AMOUNT=0;
	
	
	
	initSelf.Resource = Class.create({

		// Constructor
		initialize : function(type,amount,regenerationAmountPer10Seconds) {


			// Attributes
			this.type=type?type:DEFAULT_TYPE;
			this.amount=amount?amount:DEFAULT_AMOUNT;
			this.regenerationAmountPer10Seconds=regenerationAmountPer10Seconds?regenerationAmountPer10Seconds:DEFAULT_REGENERATION_AMOUNT;
			
			
			
		}

	
		// Methods
		/*public*/,getType:function(){
			return this.type;
		}
	


	});

	return initSelf;
}
}
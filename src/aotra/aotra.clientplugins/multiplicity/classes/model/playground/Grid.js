// GLOBAL CONSTANTS :

	var ISOMETRIC_SQUARE_MOLD="isometric_square_mold";

	var LAYER_GROUND="ground";
	var LAYER_RESOURCES="resources";

if(typeof initGridClass ==="undefined"){
function initGridClass() {
	var initSelf = this;

	// CONSTANTS

	var DEFAULT_MAX_NUMBER_OF_TILES_WIDTH=10; 	// MAX IS 32
	var DEFAULT_MAX_NUMBER_OF_TILES_HEIGHT=10; 	// MAX IS 32
	
//	var DEFAULT_MAX_NUMBER_OF_TILES_WIDTH=8; 	// MAX IS 32
//	var DEFAULT_MAX_NUMBER_OF_TILES_HEIGHT=8; 	// MAX IS 32
	
	var DEFAULT_ISOMETRIC_DIRECTION=1;

	var LAYERS=[LAYER_GROUND,LAYER_RESOURCES];
	
	
	initSelf.Grid = Class.create({

		// Constructor
		initialize : function(view
							 ,mold
							 ,maxNumberOfTilesWidth,maxNumberOfTilesHeight) {

			// Attributes
			this.view=view;
			this.mold=mold?mold:ISOMETRIC_SQUARE_MOLD;
			
			this.maxNumberOfTilesWidth=maxNumberOfTilesWidth?maxNumberOfTilesWidth:DEFAULT_MAX_NUMBER_OF_TILES_WIDTH;
			this.maxNumberOfTilesHeight=maxNumberOfTilesHeight?maxNumberOfTilesHeight:DEFAULT_MAX_NUMBER_OF_TILES_HEIGHT;

			
			// Technical attributes
			this.tiles=new Array();// a N-Dimensional array where N=2
			
			// Only used if grid is isometric :
			this.isometricDirection=DEFAULT_ISOMETRIC_DIRECTION;


			this.init();
			
		}
	
		// Methods
		/*private*/,init:function(){
			for(var x=0;x<this.maxNumberOfTilesWidth;x++){
				if(!this.tiles[x])	this.tiles.push(new Array());
				for(var y=0;y<this.maxNumberOfTilesHeight;y++){
					var tile=new Tile(this,this.getMold(),x,y);
					this.tiles[x].push(tile);
					
				}
			}
		}

	
		/*public*/,deselectAll:function(){
			
			for(var x=0;x<this.tiles.length;x++){
				if(!this.tiles[x])	continue;
				for(var y=0;y<this.tiles[x].length;y++){
					var tile=this.tiles[x][y];
					if(!tile.isSelected())	continue;
					tile.toggleSelect();
				}
			}
			
		}
		
		
		
		/*public*/,getMold:function(){
			return this.mold;
		}
		/*public*/,getMaxTilesX:function(){
			return this.maxNumberOfTilesWidth;
		}
		/*public*/,getMaxTilesY:function(){
			return this.maxNumberOfTilesHeight;
		}
		
		/*public*/,getTiles:function(){
			return this.tiles;
		}
		/*public*/,setTiles:function(tiles){
			this.tiles=tiles;
		}
		
		/*public*/,getView:function(){
			return this.view;
		}
		
		/*public*/,getLastTile:function(){
			var lastX=this.tiles.length-1;
			if(lastX<0)	return null;
			var lastY=this.tiles[lastX].length-1;
			if(lastY<0)	return null;
			return this.tiles[lastX][lastY];	
		}
		
		
		
		
		
		// Only used if grid is isometric :
		/*public*/,getIsometricDirection:function(){
			return this.isometricDirection;
		}
		
		// Drawing
		,draw:function(paper){
			
			for(var i=0;i<LAYERS.length;i++){
			
				var layer=LAYERS[i];
				
				for(var x=0;x<this.tiles.length;x++){
					if(!this.tiles[x])	continue;
					for(var y=0;y<this.tiles[x].length;y++){
						var tile=this.tiles[x][y];
						tile.draw(paper,layer);
					}
				}
			}
			
		}


	});

	return initSelf;
}
}
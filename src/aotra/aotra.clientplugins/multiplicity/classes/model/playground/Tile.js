// GLOBAL CONSTANTS :

	
	var IMAGES_SPRITES_CELL_WIDTH = 100;


	var DEFAULT_TILE_SIZE_X=80;
	var DEFAULT_TILE_SIZE_Y=80;

	var PLACED_THING_RESOURCE="resource";
	var PLACED_THING_BUILDING="building";
	
	
if(typeof initTileClass ==="undefined"){
function initTileClass() {
	var initSelf = this;

	// CONSTANTS
	var DEFAULT_NO_PLAYGROUND_TYPE_COLOR="gray";
	
	
	
	initSelf.Tile = Class.create({

		// Constructor
		initialize : function(grid,mold
							  ,x,y
							  ,tileSizeAtNoZoomX,tileSizeAtNoZoomY) {

			// Attributes
			this.grid=grid;
			this.mold=mold;
			this.x=x;
			this.y=y;

			this.tileSizeW=tileSizeAtNoZoomX?tileSizeAtNoZoomX:DEFAULT_TILE_SIZE_X;
			this.tileSizeH=tileSizeAtNoZoomY?tileSizeAtNoZoomY:DEFAULT_TILE_SIZE_Y;


			this.selected=false;
			
			// Technical attributes :
			
			this.attributes=new Object();
			
			this.thingsPlacedOnIt=new Object();
			
			
		}

		// Methods
	

		/*private*/,onClick:function(paper){
		
			var grid=this.grid;
			if(!this.isSelected())	grid.deselectAll();
			
			this.toggleSelect();
			
			grid.getView().draw();
			
		}
		
		/*public*/,getSizeW:function(){
			return this.tileSizeW;
		}
		/*public*/,getSizeH:function(){
			return this.tileSizeH;
		}
		/*public*/,getMold:function(){
			return this.mold;
		}
		/*public*/,getAttributes:function(){
			return this.attributes;
		}
		
		
		
		/*public*/,setAttributes:function(attributes){
			this.attributes=attributes;
		}
		/*public*/,setThingsPlacedOnIt:function(thingsPlacedOnIt){
			this.thingsPlacedOnIt=thingsPlacedOnIt;
		}
		/*public*/,getThingsPlacedOnIt:function(){
			return this.thingsPlacedOnIt;
		}
		/*public*/,addThingOnIt:function(name,thing){
			this.thingsPlacedOnIt[name]=thing;
		}
		
		/*public*/,setAttribute:function(name,value){
			this.attributes[name]=value;
		}

		/*private*/,toggleSelect:function(){
			this.selected=!this.isSelected();
		}
		
		/*public*/,isSelected:function(){
			return this.selected;
		}
		
		/*public*/,isVisible:function(){

			
			
			var displayX=this.getDisplayCoordinates().x;
			var displayY=this.getDisplayCoordinates().y;


			var width=this.tileSizeW;
			var height=this.tileSizeH;

			var view=this.grid.getView();
			var maxWidth=view.getMaxWidth();
			var maxHeight=view.getMaxHeight();
			
			
			// Constraints check :
			var boundsX1=0,boundsY1=0;
			var boundsX2=0,boundsY2=0;

			if(this.mold===ISOMETRIC_SQUARE_MOLD){
				boundsX1=displayX;boundsY1=displayY;
				boundsX2=displayX+width-this.getIsometricShift();boundsY2=displayY+height;
			}else{// Default is flat plain square :
				boundsX1=displayX;boundsY1=displayY;
				boundsX2=displayX+width;boundsY2=displayY+height;
			}
			if(boundsX1<=0 || maxWidth<=boundsX1)	return false;
			if(boundsY1<=0 || maxHeight<=boundsY1)	return false;
			if(boundsX2<=0 || maxWidth<=boundsX2)	return false;
			if(boundsY2<=0 || maxHeight<=boundsY2)	return false;
			
			
			
			return true;
		}
		
		// Only used if grid is isometric :
		/*private*/,getIsometricShift:function(){
			var width=this.tileSizeW;
			// This single formula determines which isometry we're talking about...
			// (actually it is only a 75% of width-shifted isometry, a rare case)
			return this.grid.getIsometricDirection()*width/2;
		}
		
		/*public*/,getDisplayCoordinates:function(){
			var result=new Object();
			var displayX=0;
			var displayY=0;

			var view=this.grid.getView();
			

			var xOffset=view.getOffsetX();
			var yOffset=view.getOffsetY();
			
			var width=this.tileSizeW;
			var height=this.tileSizeH;
			

			if(this.mold===ISOMETRIC_SQUARE_MOLD){
				displayX=this.x*width+this.y*this.getIsometricShift()+xOffset*width;
				displayY=this.y*height+yOffset*height;
			}else{
				displayX=this.x*width+xOffset*width;
				displayY=this.y*height+yOffset*height;
			}
			
			result.x=displayX;
			result.y=displayY;
			return result;
		}
		
		
		// Drawing
		/*public*/,draw:function(paper,layer){

			
			var displayX=this.getDisplayCoordinates().x;
			var displayY=this.getDisplayCoordinates().y;

			var tileWidth=this.tileSizeW;
			var tileHeight=this.tileSizeH;
			
			// Constraints check :
			if(!this.isVisible())	return;

			
			
			// Shape drawing :
			var tile=this.getTileShape(paper);

			// Border drawing :
			tile.attr("stroke", "");
			
			
			// Ground drawing :
			var resourceImage=null;
			if(layer===LAYER_GROUND){
				
				if(this.mold===ISOMETRIC_SQUARE_MOLD){
					
					// Background drawing (isometric) :
					var playgroundType=this.attributes["playgroundType"];
					var color=PLAYGROUND_TYPES[playgroundType]?PLAYGROUND_TYPES[playgroundType]["color"]:DEFAULT_NO_PLAYGROUND_TYPE_COLOR;
					tile.attr("fill", color);
					
				}else{// Default is flat plain square :
					
					// Background drawing (flat) :
					var playgroundType=this.attributes["playgroundType"];
					var color=PLAYGROUND_TYPES[playgroundType]?PLAYGROUND_TYPES[playgroundType]["color"]:DEFAULT_NO_PLAYGROUND_TYPE_COLOR;
					tile.attr("fill", color);
					
				}
				
				
			}else if(layer===LAYER_RESOURCES){

				// Eventual resources drawing :
				var resource=this.thingsPlacedOnIt[PLACED_THING_RESOURCE];
				
				if(resource){
					var rt=BRUTE_RESOURCES_TYPES[resource.getType()];
					
					if(rt){
						var imageString=rt.image[this.mold];
						if(imageString){
							
							var displayOffsetX=0;// magic numbers...
							var displayOffsetY=-40;// magic numbers...
							var FACTOR=2.5;// magic numbers...
							
							
							resourceImage=paper.image(imageString
										,displayX+displayOffsetX,displayY+displayOffsetY
										,tileWidth*FACTOR,tileHeight*FACTOR);
						}
					}
				}
				
				
			}
			
			
			// Border and selection drawing :
			var tileSelection=this.getTileShape(paper);
			tileSelection.attr("fill","");
			if(!this.isSelected()){
				tileSelection.attr("stroke", "");
			}else{
				tileSelection.attr("stroke", "orange");
				tileSelection.attr("stroke-width", 8);
			}
			
			// Mouse events handlings :
			var self=this;
			tileSelection.click(function(){self.onClick(paper);});
			
		}

		
		/*private*/,getTileShape:function(paper){
			
			var result=null;
			
			var displayX=this.getDisplayCoordinates().x;
			var displayY=this.getDisplayCoordinates().y;

			var width=this.tileSizeW;
			var height=this.tileSizeH;
			
			if(this.mold===ISOMETRIC_SQUARE_MOLD){
				result=paper.path("M"+(displayX-this.getIsometricShift())+","+displayY
							  +" l"+width+",0"
							  +" l"+(this.getIsometricShift())+","+height
							  +" l"+(-width)+",0"
							  +" l"+(-this.getIsometricShift())+","+(-height)
							);
				
			}else{// Default is flat plain square :
				
				result=paper.rect(displayX,displayY,width,height);
			}
			
			return result;
		}


		
		
	});

	return initSelf;
}
}
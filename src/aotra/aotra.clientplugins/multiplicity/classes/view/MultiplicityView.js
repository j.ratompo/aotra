// GLOBAL CONSTANTS :

var MULTIPLICITY_IMAGES_ROOT="multiplicity_html/images/";	

if(typeof initMultiplicityViewClass ==="undefined"){
function initMultiplicityViewClass() {
	var initSelf = this;

	// CONSTANTS
//	var REFRESH_RATE_SECONDS=0.1;
	var NAV_BAR_THICKNESS=60;

	var DEFAULT_VISIBLE_PIXELS_WIDTH=600;
	var DEFAULT_VISIBLE_PIXELS_HEIGHT=600;
	
	var BACKGROUND_COLOR="#000000";
	
	initSelf.MultiplicityView = Class.create({

		// Constructor
		initialize : function(plugin
							 ,visiblePixelsWidth,visiblePixelsHeight) {

			// Attributes
			this.plugin=plugin;
			
			this.visiblePixelsWidth=visiblePixelsWidth?visiblePixelsWidth:DEFAULT_VISIBLE_PIXELS_WIDTH;
			this.visiblePixelsHeight=visiblePixelsHeight?visiblePixelsHeight:DEFAULT_VISIBLE_PIXELS_HEIGHT;


			// Technical attributes :

			// UI elements:
			
			//	- Grid :
			
			// UNUSED (FOR NOW...):
			this.zoomFactor=1;
			
			this.viewTileX=0;
			this.viewTileY=0;

			this.navigationBars=new Object();
			
			// 	- Information panel :
			this.infosPanel=null;
			this.amountsPanel=null;

			//	- Actions panel :
			this.actionsPanel=null;

			
			
		}

		// Methods
	
		/*public*/,init:function(aotraScreen){
			
			var mainDiv=aotraScreen.mainDiv;  
			mainDiv.setStyle("text-align:center");
			
			//	- Actions panel :
			this.initActionsPanel(mainDiv);

			
			//	- Grid :
			
			// Implementation graphic unit initialization :
			this.paper=Raphael(0, 0, this.getMaxWidth(), this.getMaxHeight());
			var paperElement=document.getElementsByTagName("svg")[0];
			// TODO : FIXME : CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
			paperElement.style.position="relative";
			mainDiv.appendChild(paperElement);

			
			this.initGridUI();

			
			//	- Information panel :
			this.initInformationPanel(mainDiv);
			
			
		}
		
		// Actions :

		/*private*/,initActionsPanel:function(mainDiv){
			this.actionsPanel=document.createElement("div");
			// TODO : FIXME : CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
			this.actionsPanel.style.display="inline-block";
			this.actionsPanel.style.marginLeft="1em";
			this.actionsPanel.style.verticalAlign="top";
			this.actionsPanel.style.textAlign="left";
			this.actionsPanel.style.padding="10px";
			this.actionsPanel.style.float="right";
			
			mainDiv.appendChild(this.actionsPanel);
			
		}

		
		/*private*/,addAction:function(actionName,actionLabel,actionMethod,displayOnlyIfTileIsSelected){

			var button=document.createElement("div");
			button.actionName=actionName;
			button.innerHTML=actionLabel;
			button.onclick=actionMethod;
			button.displayIfTileSelected=displayOnlyIfTileIsSelected;
			
			button.setStyle( "padding : 8px;"
					+ "color:white;background:#AAAAAA; border:none;"
					+"-moz-border-radius: 10px; border-radius: 10px;");
			
			
			this.actionsPanel.appendChild(button);
			
		}

		/*public*/,refreshActionsDisplays:function(){
			
			
		}
		
		
		// Informations :
		
		/*private*/,initInformationPanel:function(mainDiv){
			
		
			this.infosPanel=document.createElement("div");
			this.infosPanel.style.display="inline-block";
			this.infosPanel.style.marginLeft="1em";
			this.infosPanel.style.verticalAlign="top";
			this.infosPanel.style.textAlign="left";

			mainDiv.appendChild(this.infosPanel);
			
			this.amountsPanel=document.createElement("div");
			this.infosPanel.appendChild(this.amountsPanel);
	

		}
		
		/*private*/,getAmountsHTML:function(game){
			
			var html="";

			html+="<h6>Moneys :</h6><br/>";
			// Moneys :
			var moneys=game.getMoneys();
			for(key in moneys){
				if(!moneys.hasOwnProperty(key))	continue;
				html+=" "+key+":"+moneys[key]+"<br/>";
			}
			html+="<hr/>";

			// Resources :
			html+="<h6>Resources :</h6><br/>";
			var resources=game.getResources();
			for(key in resources){
				if(!resources.hasOwnProperty(key))	continue;
				html+=" "+key+":"+resources[key]+"<br/>";
			}
			html+="<hr/>";

			// Population indicators :
			html+="<h6>Population indicators :</h6><br/>";
			var populationIndicators=game.getPopulationIndicators();
			for(key in populationIndicators){
				if(!populationIndicators.hasOwnProperty(key))	continue;
				html+=" "+key+":"+populationIndicators[key]+"<br/>";
			}
			
			return html;
		
		}
		
		
		/*private*/,initGridUI:function(){
			var self=this;
			
			
			this.navigationBars.methodW=function(){
				if(-self.viewTileX<0)	return;
				self.viewTileX+=1;
				self.draw();
			};
			this.navigationBars.methodE=function(){
				var grid=self.plugin.getModel().getGrid();
				var lastTile=grid.getLastTile();
				if(lastTile.getDisplayCoordinates().x<self.visiblePixelsWidth-lastTile.getSizeW())	return;

				self.viewTileX-=1;
				self.draw();
			};
			this.navigationBars.methodN=function(){
				if(-self.viewTileY<0)	return;
				self.viewTileY+=1;
				self.draw();
			};
			this.navigationBars.methodS=function(){
				var grid=self.plugin.getModel().getGrid();
				var lastTile=grid.getLastTile();
				if(lastTile.getDisplayCoordinates().y<self.visiblePixelsHeight-lastTile.getSizeH())	return;
				
				self.viewTileY-=1;
				self.draw();
			};
			
		}

		
		/*public*/,getOffsetX:function(){
			return this.viewTileX;
		}
		/*public*/,getOffsetY:function(){
			return this.viewTileY;
		}
		/*public*/,getMaxWidth:function(){
			return this.visiblePixelsWidth;
		}
		/*public*/,getMaxHeight:function(){
			return this.visiblePixelsHeight;
		}
		
		
		// Drawing
		
		/*public*/,draw:function(){

			var paper=this.paper;
			var grid=this.plugin.getModel().getGrid();
			
			
			var width=paper.width;
			var height=paper.height;

			// Clear all :
			paper.clear();

			// Background color :
			var background=paper.rect(0,0,width,height);
			background.attr("fill", BACKGROUND_COLOR);

			grid.draw(paper);
			
			this.drawUI();

			this.refreshUIElements();

			
		}

		/*private*/,drawUI:function(){
			
			var paper=this.paper;
			
			var width=paper.width;
			var height=paper.height;
			
			
			// Navigation bars :
			var barN=paper.rect(0,0,width,NAV_BAR_THICKNESS);
			barN.attr("fill", "red");
			barN.click(this.navigationBars.methodN);
			
			var barS=paper.rect(0,height-NAV_BAR_THICKNESS,width,height);
			barS.attr("fill", "green");
			barS.click(this.navigationBars.methodS);
			
			var barE=paper.rect(width-NAV_BAR_THICKNESS,0,width,height);
			barE.attr("fill", "yellow");
			barE.click(this.navigationBars.methodE);
			
			var barW=paper.rect(0,0,NAV_BAR_THICKNESS,height);
			barW.attr("fill", "blue");
			barW.click(this.navigationBars.methodW);
			
			
		}
		
		/*private*/,refreshUIElements:function(){			
			
			var game=this.plugin.getModel().getGame();
			this.amountsPanel.innerHTML=this.getAmountsHTML(game);

		}


	});

	return initSelf;
}
}
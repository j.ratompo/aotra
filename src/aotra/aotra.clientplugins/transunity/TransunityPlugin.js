// GLOBAL CONSTANTS :

	

if(typeof initTransunityPluginClass ==="undefined"){
function initTransunityPluginClass() {
	var initSelf = this;

	
	// CONSTANTS
	var PLUGIN_NAME_DEFAULT="transunity";
	var TAG_NAME="transunity";
	var OVERRIDE=false;

	
	initSelf.TransunityPlugin = Class.create({

		// Constructor
		initialize : function(pluginName) {

			
			// Attributes
			this.pluginName=pluginName?pluginName:PLUGIN_NAME_DEFAULT;
			this.overrideAotraScreenDisplay=OVERRIDE;
			

			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization started...-------------------");
			
			// Model
			initTransunityModelClass();
			initTransunityGameClass();
			initTransunityBoardClass();

			
			// Controller
			initTransunityControllerClass();
//			initTransunityGameImporterClass();
//			initTransunityGameExporterClass();

			// View
			initTransunityViewClass();
			
			// Imitialization
			this.model=new TransunityModel();
			this.controller=new TransunityController(this);
			this.view=new TransunityView(this);
			
			// TRACE
			log("------------------- Plugin «"+this.pluginName+"» initialization done.-------------------");
			
			
		}

		// Methods
	
		/*public*/,getView:function(){
			return this.view;
		}
		
		/*public*/,getController:function(){
			return this.controller;
		}
		
		/*public*/,getModel:function(){
			return this.model;
		}
		
		
		/*MANDATORY METHOD*/
		/*public*/,initPlugin:function(aotraScreen){
			
			
			if(!aotraScreen.pluginsArgs[this.pluginName] || aotraScreen.pluginsArgs[this.pluginName]==="false")	return false;
			
			var game=this.model.getGame();

			
			// First we have to init the view
			this.view.init(aotraScreen);

			// Then we put all the needed information
			this.controller.initGame(this.view,game);
			
			
			// At last, we perform the first drawing of the view :
			this.view.draw();
			
			return true;
		}

		
		/*private*/,getTransunityPlane:function(eAotraScreen){
			
			var ePlanes=eAotraScreen.getElementsByTagName("plane");
			for(var i=0;i<ePlanes.length;i++){
				var ePlane=ePlanes[i];
				var ePlaneName=ePlane.attributes["name"];
				if(!ePlaneName)	continue;
				if(contains(ePlaneName,this.pluginName))	return ePlane;
			}
			
			return null;
		}
		
		
		/*MANDATORY METHOD*/
		/*public*/,readFromXMLAotraScreenArgs:function(eAotraScreen){
			
			var plugin=false;//default value is false

			// Ability to play the transunity game :
			
			// we look to the first plane, and it must be named «<pluginName>_»
			var ePlane=this.getTransunityPlane(eAotraScreen);
			if(!ePlane) return false;
			
			return true;
		}
		
		/*MANDATORY METHOD*/		
		/*public*/,getXMLFromAotraScreenArgs:function(aotraScreen){
			
			// We don't need to return any XML here, since all is defined in a plane:
			return "";
		}
	

	});

	return initSelf;
}
}
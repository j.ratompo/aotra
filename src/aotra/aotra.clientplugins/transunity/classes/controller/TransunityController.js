


// GLOBAL CONSTANTS :

	var RANDOM="random";

if(typeof initTransunityControllerClass ==="undefined"){
function initTransunityControllerClass() {
	var initSelf = this;

	// CONSTANTS
	var DEFAULT_MODE="random";
	var DEFAULT_MODEL_FORMAT="JSON";
	var DEFAULT_SAVE_GAME_EVERY_MILLIS=5000;
	
	var STORED_MODEL_STRING_NAME="multiplicityModelString";
	
	
	initSelf.TransunityController = Class.create({

		// Constructor
		initialize : function(plugin) {


			// Attributes
			this.plugin=plugin;

			// Technical attributes
			//...
			
			
			
		}

		// Methods

		
		// Initialization
		/*public*/,initGame:function(view,game){
			
			var modelStringFromStorage=this.getModelStringFromStorage();
			
			
			this.initializeGame(view,game,modelStringFromStorage);
			
			
			this.startSavingRoutine(DEFAULT_SAVE_GAME_EVERY_MILLIS);
		
			// Initialization of sub-modules of this controller :
			//...

		}


		/*private*/,initializeGame:function(view,game,/*NULLABLE*/modelStringFromStorageParam){

			var board=null;
			
			// If the map is brand new :
			var isCreateNew=empty(modelStringFromStorageParam);
			
			
			if(isCreateNew){
				
				board=new TransunityBoard(view);
				
	
			}else{

				this.gameImporter=new TransunityGameImporter(DEFAULT_MODEL_FORMAT);

				var parsedParams=this.gameImporter.parseParams(modelStringFromStorageParam);
				if(!parsedParams){
					this.initializeGame(view,game,null);
					return;
				}


				this.gameImporter.initializeGame(view,game,parsedParams);
				board=this.gameImporter.getInitedTransunityBoard(view,parsedParams);
				
			}
			
			this.gameExporter=new TransunityGameExporter(DEFAULT_MODEL_FORMAT);

			game.setBoard(board);

			
		}

		

		/*private*/,startSavingRoutine:function(savingGameEveryMillis){
			
			var self=this;
			var pe=new PeriodicalExecuter(function() {
				
				var model=self.plugin.getModel();
				var modelString=self.gameExporter.exportModel(model);
				
				self.setModelStringToStorage(modelString);
				
			}, savingGameEveryMillis*.001);
			
		}

		
		/*private*/,getModelStringFromStorage:function(){
			return getStringFromStorage(STORED_MODEL_STRING_NAME);
		}
		
		/*private*/,setModelStringToStorage:function(modelString){
			return storeString(STORED_MODEL_STRING_NAME,modelString);
		}



	});

	return initSelf;
}
}
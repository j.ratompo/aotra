

// GLOBAL CONSTANTS :


if(typeof initTransunityGameImporterClass ==="undefined"){
function initTransunityGameImporterClass() {
	var initSelf = this;

	// CONSTANTS
	
	// TODO ...
	
	initSelf.TransunityGameImporter = Class.create({

		// Constructor
		initialize : function(mode) {


			// Attributes
			this.mode=mode;

			this.initializeGameMethods=new Object();
			this.initializeGameMethods[JSON]=this.initializeGameJSON;
			
			this.initBoardMethods=new Object();
			this.initBoardMethods[JSON]=this.getInitedBoardJSON;
			
			this.parseMethods=new Object();
			this.parseMethods[JSON]=this.parseParamsJSON;

		}

		// Methods
	
		// 	- Game :
	
		/*public*/,initializeGame:function(view,game,paramsObject){
			var m=this.initializeGameMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	m(this,view,game,paramsObject);
			return;
		}
		
		
		/*private*/,initializeGameJSON:function(self,view,game,paramsObject){
			
			
			var lastGame=paramsObject.lastGame;
			
			
			var amounts=lastGame.amounts;
			
//			var capacitiesParam=amounts.capacities;
//			for(key in capacitiesParam){
//				if(!capacitiesParam.hasOwnProperty(key))	continue;
//				game.setCapacity(key,parseInt(capacitiesParam[key]));
//			}
			
			var moneysParam=amounts.moneys;
			for(key in moneysParam){
				if(!moneysParam.hasOwnProperty(key))	continue;
				game.setMoney(key,parseInt(moneysParam[key]));
			}
			
			var resourcesParam=amounts.resources;
			for(key in resourcesParam){
				if(!resourcesParam.hasOwnProperty(key))	continue;
				game.setResource(key,parseInt(resourcesParam[key]));
			}
			
			var populationIndicatorsParam=amounts.populationIndicators;
			for(key in populationIndicatorsParam){
				if(!populationIndicatorsParam.hasOwnProperty(key))	continue;
				game.setPopulationIndicator(key,parseInt(populationIndicatorsParam[key]));
			}
			
		}

		
		
		// 	- Board :
	
		/*public*/,getInitedBoard:function(view,paramsObject){
			var m=this.initBoardMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	return m(this,view,paramsObject);
			return null;
		}

		
		/*private*/,getInitedBoardJSON:function(self,view,paramsObject){
			
			
			var board=null;
			
			var lastGame=paramsObject.lastGame;
			var lastBoard=lastGame.board;
			var boardMold=lastBoard.mold;
			var boardMaxTilesWidth=lastBoard.maxTilesWidth;
			var boardMaxTilesHeight=lastBoard.maxTilesHeight;
			
			
			board=new Board(view,boardMold,boardMaxTilesWidth,boardMaxTilesHeight);
			
			var tiles=self.getTilesArrayJSON(self,board,lastBoard);
			board.setTiles(tiles);
			
			return board;
			
			
		}
		
		
		/*private*/,getTilesArrayJSON:function(self,boardObject,boardParamsObject){
			
			var results=new Array();
			
			var tilesParams=boardParamsObject.tiles;
			
			for(var x=0;x<tilesParams.length;x++){
				if(!tilesParams[x])	continue;
				results.push(new Array());
				for(var y=0;y<tilesParams[x].length;y++){
					var tileParams=tilesParams[x][y].tile;

					var tile=new Tile(boardObject,tileParams.mold,x,y);
					var attrs=self.getTilesAttributesJSON(self,tileParams);
					tile.setAttributes(attrs);
					
					var things=self.getTilesThingsPlacedOnItJSON(self,tileParams);
					tile.setThingsPlacedOnIt(things);

					results[x].push(tile);
				}
			}
			
			
			return results;
		}

		/*private*/,getTilesAttributesJSON:function(self,tileParamsObject){
			var results=new Object();
			
			var tilesAttributesParams=tileParamsObject.attributes;
			
			for(key in tilesAttributesParams){
				if(!tilesAttributesParams.hasOwnProperty(key))	continue;
				var attributeParam=tilesAttributesParams[key];
				results[key]=attributeParam;
				
			}
			
			return results;
		}
		
		/*private*/,getTilesThingsPlacedOnItJSON:function(self,tileParamsObject){
			var results=new Object();
			
			var thingsParams=tileParamsObject.thingsPlacedOnIt;
			if(!thingsParams)	return results;
			
			for(key in thingsParams){
				if(!thingsParams.hasOwnProperty(key))	continue;
				var thingParams=thingsParams[key];
				
				var thing=self.getThingPlacedOnItJSON(self,key,thingParams);
				if(!thing)	continue;
				
				results[key]=thing;
				
			}
			
			return results;
		}
		
		
		/*private*/,getThingPlacedOnItJSON:function(self,type,thingParamsObject){

			var result=null;
			
			if(type===PLACED_THING_RESOURCE){
				
				var resourceType=thingParamsObject.type;
				var amount=parseFloat(thingParamsObject.amount);
				var regenerationAmount=(thingParamsObject.regenerationAmountPer10Seconds);
				result=new Resource(resourceType,amount,regenerationAmount);
			}else if(type===PLACED_THING_BUILDING){
				
				var buildingType=thingParamsObject.buildingType;
				var level=parseInt(thingParamsObject.level);
				result= new Building(buildingType,level);
			}	
			
			
			return result;
		
		}
		
		
		
		
		/*public*/,parseParams:function(modelString){
			var m=this.parseMethods[this.mode];
			// I have to pass self reference as a parameter, because with this way to create interfaces, the «this» context is lost :
			if(m)	return m(this,modelString);
			return null;
		}
		

		/*private*/,parseParamsJSON:function(self,modelString){
			return parseJSON(modelString);
		}
		

	});

	return initSelf;
}
}
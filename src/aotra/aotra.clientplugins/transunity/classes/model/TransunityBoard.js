// GLOBAL CONSTANTS :


if(typeof initTransunityBoardClass ==="undefined"){
function initTransunityBoardClass() {
	var initSelf = this;

	// CONSTANTS

	var DEFAULT_MAX_WIDTH=1000;
	var DEFAULT_MAX_HEIGHT=1000;
	
	
	initSelf.TransunityBoard = Class.create({

		// Constructor
		initialize : function(view
							 ,maxWidth,maxHeight) {

			// Attributes
			this.view=view;
			
			this.maxWidth=maxWidth?maxWidth:DEFAULT_MAX_WIDTH;
			this.maxHeight=maxHeight?maxHeight:DEFAULT_MAX_HEIGHT;

			
			// Technical attributes
			
			// Only used if grid is isometric :


			this.init();
			
		}
	
	
		// TODO ...
	
	
		// Methods
		/*private*/,init:function(){
			
			
			
		}

		
		/*public*/,getView:function(){
			return this.view;
		}
		
		
		
		// Drawing
		,draw:function(paper){
			
//			for(var i=0;i<LAYERS.length;i++){
			

//			}
			
		}


	});

	return initSelf;
}
}
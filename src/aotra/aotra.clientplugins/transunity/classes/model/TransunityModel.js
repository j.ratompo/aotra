// GLOBAL CONSTANTS :

	

if(typeof initTransunityModelClass ==="undefined"){
function initTransunityModelClass() {
	var initSelf = this;

	
	initSelf.TransunityModel = Class.create({

		// Constructor
		initialize : function() {


			// Attributes
			
			this.currentGame=new TransunityGame();

			
		}
	
		// Methods
	
	

		/*public*/,getGame:function(){
			return this.currentGame;
		}
		
		/*public*/,getBoard:function(){
			return this.currentGame.getBoard();
		}

	});

	
	
	return initSelf;
}
}
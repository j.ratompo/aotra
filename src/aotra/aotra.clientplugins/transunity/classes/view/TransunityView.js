// GLOBAL CONSTANTS :


var TRANSUNITY_IMAGES_ROOT="transunity_html/images/";	


// TODO ...

if(typeof initTransunityViewClass ==="undefined"){
function initTransunityViewClass() {
	var initSelf = this;

	// CONSTANTS
//	var REFRESH_RATE_SECONDS=0.1;
	var NAV_BAR_THICKNESS=60;

	var DEFAULT_VISIBLE_PIXELS_WIDTH=600;
	var DEFAULT_VISIBLE_PIXELS_HEIGHT=600;
	
	var BACKGROUND_COLOR="#000000";
	
	initSelf.TransunityView = Class.create({

		// Constructor
		initialize : function(plugin
							 ,visiblePixelsWidth,visiblePixelsHeight) {

			// Attributes
			this.plugin=plugin;
			
			this.visiblePixelsWidth=visiblePixelsWidth?visiblePixelsWidth:DEFAULT_VISIBLE_PIXELS_WIDTH;
			this.visiblePixelsHeight=visiblePixelsHeight?visiblePixelsHeight:DEFAULT_VISIBLE_PIXELS_HEIGHT;


			// Technical attributes :

			// UI elements:
			
		
			// 	- Information bubble :
			this.infosBubble=null;

			
			
		}

		// Methods
	
		/*public*/,init:function(aotraScreen){
			
			var mainDiv=aotraScreen.mainDiv;  
			mainDiv.setStyle("text-align:center");
			
			//	- Information bubble :
			this.initInfosBubble(aotraScreen);

			
			//	- Board :
			
			// Implementation graphic unit initialization :
			this.paper=Raphael(0, 0, this.getMaxWidth(), this.getMaxHeight());
			var paperElement=document.getElementsByTagName("svg")[0];
			// TODO : FIXME : CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
			paperElement.style.position="relative";
			mainDiv.appendChild(paperElement);

			
			
			//	- Information panel :
			this.initInformationPanel(mainDiv);
			
			
		}
		
		
		// Informations :
		/*private*/,initInfosBubble:function(aotraScreen){
			// TODO...
			
		}



		


		/*public*/,getMaxWidth:function(){
			return this.visiblePixelsWidth;
		}
		/*public*/,getMaxHeight:function(){
			return this.visiblePixelsHeight;
		}
		
		
		// Drawing
		
		/*public*/,draw:function(){

			var paper=this.paper;
			var board=this.plugin.getModel().getBoard();
			
			
			var width=paper.width;
			var height=paper.height;

			// Clear all :
			paper.clear();

			// Background color :
			var background=paper.rect(0,0,width,height);

			board.draw(paper);
			

			
		}


	});

	return initSelf;
}
}
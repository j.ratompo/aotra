// GLOBAL CONSTANTS

if (typeof initPlaneBackgroundImageTransitionProviderClass === "undefined") {

	function initPlaneBackgroundImageTransitionProviderClass() {
		var initSelf = this;

		// CONSTANTS
		var LINEARFADE = "linearFade";
		var DEFAULT_TRANSITION_DURATION = 2000;
		// TODO : FIXME : Fade background transition just stopped working,
		// causing massive freezing of every browser on december 2016 !
		// Although no changes in aotra source code were made !!! THIS IS INSANE !
		var DEFAULT_TRANSITION_NAME = LINEARFADE;
		// var DEFAULT_TRANSITION_NAME = "instantly";

		var STEP = 0.1;

		initSelf.PlaneBackgroundImageTransitionProvider = Class.create({

			// Constructor
			initialize : function(plane,/* OPTIONAL */
			transitionConfigParam) {

				// Attributes
				this.plane = plane;

				if (transitionConfigParam)
					this.transitionConfig = transitionConfigParam;
				else
					this.transitionConfig = new Object();

				if (transitionConfigParam && !nothing(transitionConfigParam["name"]))
					this.transitionConfig["name"] = transitionConfigParam["name"];
				else
					this.transitionConfig["name"] = DEFAULT_TRANSITION_NAME;

				if (transitionConfigParam && !nothing(transitionConfigParam["duration"]) && isNumber(transitionConfigParam["duration"]))
					this.transitionConfig["duration"] = parseFloat(transitionConfigParam["name"]);
				else
					this.transitionConfig["duration"] = DEFAULT_TRANSITION_DURATION;

				// Initialization

				// TODO : FIXME : Fade background transition just stopped working,
				// causing massive freezing of every browser on december 2016 !
				// Although no changes in aotra source code were made !!! THIS IS INSANE
				// !
				// TRACE
				log("WARN : TODO : FIXME : LINEAR FADE CREATED A FREEZE ON ALL BROWSERS FROM DECEMBER 2016, BACKGROUND TRANSITIONS FORCED TO «instantly» VALUE !");
				this.transitionConfig["name"] = "instantly";

				if (this.transitionConfig["name"] === LINEARFADE) {
					this.launchTransition = this.launchTransitionLinearFade;
					this.drawBackgroundTransitioning = this.drawBackgroundTransitioningFade;
				} else {
					this.launchTransition = this.launchTransitionInstantly;
					this.drawBackgroundTransitioning = this.drawBackgroundTransitioningInstantly;
				}

				this.refreshingRate = (this.transitionConfig["duration"] / 1000) * STEP;

			}

			// Methods

			// - instant :
			,
			launchTransitionInstantly : function(aotraScreen) {
				// aotraScreen.draw();
				/* DO NOTHING */
			}

			,
			drawBackgroundTransitioningInstantly : function(ctx, oldImage, newImage, FRACTION_OF_IMAGE_USED, aotraScreen) {
				var plane = this.plane;

				if (newImage && newImage.width > 0) {
					plane.parallaxBackgroundsProvider.drawImageConstrained(ctx, newImage);
				}

			}

			// - fading :
			,
			launchTransitionLinearFade : function(aotraScreen) {

				// Value will vary from 0 to 1 :
				this.opacity = 0;

				var self = this;
				var pe = new PeriodicalExecuter(function() {

					if (1 <= self.opacity) {
						pe.stop();
						return;
					}

					self.opacity += STEP;

					aotraScreen.draw();

				}, this.refreshingRate);

			}

			/* public */,
			drawBackgroundTransitioningFade : function(ctx, oldImage, newImage, FRACTION_OF_IMAGE_USED, aotraScreen) {

				var plane = this.plane;

				// Back (old image) image drawing :
				if (oldImage && oldImage.width > 0 && this.opacity < 1) {
					ctx.globalAlpha = 1 - this.opacity;
					plane.parallaxBackgroundsProvider.drawImageConstrained(ctx, oldImage);
				}

				// Fore (new image) image appearing :
				if (newImage && newImage.width > 0) {

					ctx.globalAlpha = this.opacity;
					plane.parallaxBackgroundsProvider.drawImageConstrained(ctx, newImage);
				}

			}

		});

		return initSelf;
	}
}
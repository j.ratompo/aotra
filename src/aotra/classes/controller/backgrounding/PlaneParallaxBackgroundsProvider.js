// DEPENDENCY : PLANE MAP PROVIDER
// DEPENDENCY : PLANE BACKGROUND TRANSITIONING PROVIDER
// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider

// GLOBAL CONSTANTS

if (typeof initPlaneParallaxBackgroundsProviderClass === "undefined") {

	function initPlaneParallaxBackgroundsProviderClass() {
		var initSelf = this;

		// CONSTANTS
		var DEFAULT_FRACTION_OF_IMAGE_USED = .5;
		var LOADING_BACKGROUND_COLOR = "#888888";

		var PARALLAX_BACKGROUND_FOR_BUBBLE_AREA_SIZE = 1000;
		var LONG_DISTANCE_FACTOR = .05;
		var MULTIPLE_BACKGROUNDS = "multiple{";

		initSelf.PlaneParallaxBackgroundsProvider = Class.create({

			// Constructor
			initialize : function(plane,/* OPTIONAL */
			fractionOfImageUsed) {

				// Attributes
				this.plane = plane;
				this.backgroundSizeMode = "bubble";// TODO : Make this
				// configurable...

				this.fractionOfImageUsed = !nothing(fractionOfImageUsed) ? (fractionOfImageUsed === "noParallax" ? 1 : fractionOfImageUsed)
						: DEFAULT_FRACTION_OF_IMAGE_USED;

				// Technical attributes :
				this.oldImage = null;
				this.canvasBackgroundImage = null;

				this.parsedCanvasImagesSrcForScreenCoordinates = new Array();
				this.backgroundImageTransitionProvider = null;

				// Initialization
				this.initParallaxBackgrounds();

			}

			// Methods

			/* private */,
			initParallaxBackgrounds : function() {

				var plane = this.plane;
				var aotraScreen = plane.aotraScreen;
				var areasAndParallaxBackgroundsParameters = null;

				if (!nothing(plane.parallaxBackground)) {

					// Case of one single parallax background :
					if (!contains(plane.parallaxBackground, MULTIPLE_BACKGROUNDS)) {
						this.canvasBackgroundImage = new Image();
						// We only tolerate images hosted at the same
						// place for parallax backgrounds :
						this.canvasBackgroundImage.src = plane.parallaxBackground;

						var self = this;
						this.canvasBackgroundImage.onload = function() {
							aotraScreen.draw();
							self.canvasBackgroundImage.isLoaded = true;
						};

						// We push default parallax background :
						this.addDefaultBackground(plane.parallaxBackground);

						// Parallax transitions between backgrounds :
						// (with default transition parameters)
						this.backgroundImageTransitionProvider = new PlaneBackgroundImageTransitionProvider(plane);// Contains
						// «name»
						// and
						// optionnaly, «duration»

					} else {
						// Case of several parallax backgrounds :
						var transitionParametersStr = "{" + plane.parallaxBackground.replace(MULTIPLE_BACKGROUNDS, "");

						var transitionParameters = parseJSON(transitionParametersStr);
						if (transitionParameters["transition"])
							areasAndParallaxBackgroundsParameters = transitionParameters["backgrounds"];
						else
							areasAndParallaxBackgroundsParameters = transitionParameters;

						this.parsedCanvasImagesSrcForScreenCoordinates = this.getParsedCanvasImagesSrcForScreenCoordinates(areasAndParallaxBackgroundsParameters);

						var src = this.getCanvasImageSrcForCurrentScreenCoordinates();
						if (!nothing(src)) {
							this.canvasBackgroundImage = new Image();
							// We only tolerate images hosted at the
							// same place for parallax backgrounds :
							this.canvasBackgroundImage.src = src;

							var self = this;
							this.canvasBackgroundImage.onload = function() {
								aotraScreen.draw();
								self.canvasBackgroundImage.isLoaded = true;
							};
						}

						// Parallax transitions between backgrounds :
						this.backgroundImageTransitionProvider = new PlaneBackgroundImageTransitionProvider(plane, transitionParameters["transition"]);// this
						// contains
						// «name»
						// and
						// optionnaly,
						// «duration»

					}

				} else {

					// We push default parallax background :
					this.addDefaultBackground();

					// Parallax transitions between backgrounds : (with
					// default transition parameters)
					this.backgroundImageTransitionProvider = new PlaneBackgroundImageTransitionProvider(plane);// Contains
					// «name»
					// and
					// optionnaly,
					// «duration»
				}

			}

			/* private */,
			addDefaultBackground : function(/* NULLABLE */src) {

				// We push default parallax background :
				areasAndParallaxBackgroundsParameters = new Object();
				if (src)
					areasAndParallaxBackgroundsParameters["default"] = src;
				else
					areasAndParallaxBackgroundsParameters["default"] = null;

				this.parsedCanvasImagesSrcForScreenCoordinates = this.getParsedCanvasImagesSrcForScreenCoordinates(areasAndParallaxBackgroundsParameters);

			}

			/* private */,
			getParsedCanvasImagesSrcForScreenCoordinates : function(imgsBruteStrings) {

				// On today, background areas are squares, with a X,Y
				// and SIZE property :
				var results = new Array();
				var o = new Object();

				// Default image :
				o.src = imgsBruteStrings["default"] ? imgsBruteStrings["default"] : null;
				o.x = 0;
				o.y = 0;
				// -1 size means infinite, so only default background
				// will have this size.
				o.w = -1;
				o.h = -1;

				results.push(o);

				for (key in imgsBruteStrings) {
					if (!imgsBruteStrings.hasOwnProperty(key) || key === "default")
						continue;

					var k = key.replace(/[\s\t]+gim/, " ");
					var split = k.split(" ");

					// Malformed coordinates for a background make
					// concerned background ignored :
					if (split.length != 3 || !isNumber(split[0]) || !isNumber(split[1]) || !isNumber(split[2])) {
						// TRACE
						log("ERROR : Parsing «" + k + "» for background area informations.");
						continue;
					}

					var x = parseFloat(split[0]);
					var y = parseFloat(split[1]);
					var w = parseFloat(split[2]);
					var h = w; // w==h in this case

					o = new Object();
					o.src = imgsBruteStrings[key];
					o.x = x;
					o.y = y;
					o.w = w;
					o.h = h;

					results.push(o);

				}

				return results;
			}

			,
			getCanvasImageSrcForCurrentScreenCoordinates : function() {

				// On today, background areas are squares, with a X,Y
				// and SIZE property :
				var plane = this.plane;
				var aotraScreen = plane.aotraScreen;

				var parsedImgs = this.parsedCanvasImagesSrcForScreenCoordinates;

				var f = aotraScreen.getZoomFactor();
				for (var i = 0; i < parsedImgs.length; i++) {

					var img = parsedImgs[i];

					var x = img.x;
					var y = img.y;
					var w = img.w;
					var h = img.h;

					var src = img.src;

					// -1 size is infinite, only default background has
					// this size, hence, we ignore it :
					if (w === -1)
						continue;

					var canonicalScreenX = aotraScreen.x / f;
					var canonicalScreenY = aotraScreen.y / f;

					// if(x-size/2<canonicalScreenX &&
					// canonicalScreenX<x+size/2
					// && y-size/2<canonicalScreenY &&
					// canonicalScreenY<y+size/2){
					if (x - w / 2 < canonicalScreenX && canonicalScreenX < x + w / 2 && y - h / 2 < canonicalScreenY && canonicalScreenY < y + h / 2) {
						return src;
					}
				}

				// HERE IS THE SPECIAL CO-HABITATION MODE WITH SIMPLE
				// BACKGROUND :
				// We simply draw no image as default if we have ANY
				// simple background
				// (whether an image, a webcam or a map !).
				// So if you have a default parallax background, and a
				// simple background,
				// the simple background will be drawn, and not the
				// default parallax one.
				if (plane.simpleBackground)
					return null;

				// We draw nothing if default background is null :
				if (!parsedImgs[0].src)
					return null;

				return parsedImgs[0].src;

			}

			/* public */,
			addBubbleParallaxBackground : function(bubble) {

				if (!nothing(bubble.parallaxBackground)) {
					var img = new Object();
					img.src = bubble.parallaxBackground;
					var pxy = bubble.getOffsetBecauseOfParentXY();
					img.x = bubble.x + pxy["x"];
					img.y = bubble.y + pxy["y"];
					img.w = this.backgroundSizeMode === "bubble" ? bubble.getMaxW() : PARALLAX_BACKGROUND_FOR_BUBBLE_AREA_SIZE;
					img.h = this.backgroundSizeMode === "bubble" ? bubble.getMaxH() : PARALLAX_BACKGROUND_FOR_BUBBLE_AREA_SIZE;
					img.bubbleName = bubble.getName();
					this.parsedCanvasImagesSrcForScreenCoordinates.push(img);
				}
			}

			/* public */,
			removeBubbleParallaxBackground : function(bubble) {
				if (!nothing(bubble.parallaxBackground)) {
					var tab = copy(this.parsedCanvasImagesSrcForScreenCoordinates);
					for (var i = 0; i < tab.length; i++) {
						var bimg = tab[i];
						if (bimg.bubbleName == bubble.getName()) {
							remove(this.parsedCanvasImagesSrcForScreenCoordinates, bimg);
							break;
						}
					}
				}
			}

			// Utility methods :
			/* public */,
			draw : function(ctx) {

				var plane = this.plane;
				var aotraScreen = plane.aotraScreen;

				// TODO : FIXME : Quand on a un
				// backgroundImageTransitionProvider non null, ça freeze tout browser
				// (depuis décembre 2016)
				// // FOR DEBUG ONLY :
				// TODO : FIXME : Fade background transition just stopped working,
				// causing massive freezing of every browser on december 2016 !
				// Although no changes in aotra source code were made !!! THIS IS INSANE
				// !

				// Case of several parallax backgrounds drawing :
				// -transition launching, if necessary :
				if (this.backgroundImageTransitionProvider) {

					// A trigger for transition between multiple
					// parallax backgrounds :

					// Actually, in good computer programs designs, you
					// are not supposed to update object's data
					// (here, «this.oldImage») in its drawing (viewing)
					// method...:
					// ...so it seems that I'm a bad, bad programmer:
					// :-/

					var previousImageSrc = null;
					if (this.canvasBackgroundImage) {
						previousImageSrc = this.canvasBackgroundImage.src;
					} else {
						previousImageSrc = null;
					}

					var imageNowSrc = this.getCanvasImageSrcForCurrentScreenCoordinates();

					// If background image has changed, then we launch
					// the transition :

					// We have to standardize images file paths to
					// compare them then :
					var normalizedPreviousImageSrc = removeServerPartOfURL(getAbsoluteUrlForRelativeUrl(previousImageSrc));
					var normalizedImageNowSrc = removeServerPartOfURL(getAbsoluteUrlForRelativeUrl(imageNowSrc));

					if ((previousImageSrc == null && imageNowSrc !== null) || (previousImageSrc !== null && imageNowSrc == null)
							|| (previousImageSrc !== null && imageNowSrc !== null && normalizedPreviousImageSrc !== normalizedImageNowSrc)) {

						if (imageNowSrc !== null) {
							this.canvasBackgroundImage = new Image();
							// We only tolerate images hosted at the
							// same place for parallax backgrounds :
							this.canvasBackgroundImage.src = imageNowSrc;

							var self = this;
							this.canvasBackgroundImage.onload = function() {
								aotraScreen.draw();
								self.canvasBackgroundImage.isLoaded = true;
							};

						} else {
							this.canvasBackgroundImage = null;
						}

						this.oldImage = null;
						if (previousImageSrc) {
							this.oldImage = new Image();
							// We only tolerate images hosted at the
							// same place for parallax backgrounds :
							this.oldImage.src = previousImageSrc;
						} else {
							this.oldImage = null;
						}

						this.backgroundImageTransitionProvider.launchTransition(aotraScreen);

					}

				}

				var newImage = this.canvasBackgroundImage;

				ctx.save();
				// Case of several parallax backgrounds drawing :
				if (this.backgroundImageTransitionProvider) {

					if (newImage && !newImage.isLoaded) {
						// We color the background to some color, just
						// in case images have not loaded yet :
						var aotraScreenWidth = aotraScreen.getWidth();
						var aotraScreenHeight = aotraScreen.getHeight();
						ctx.fillStyle = LOADING_BACKGROUND_COLOR;
						ctx.fillRect(0, 0, aotraScreenWidth, aotraScreenHeight);
					}

					// Regular drawing :
					this.backgroundImageTransitionProvider.drawBackgroundTransitioning(ctx, this.oldImage, newImage, this.fractionOfImageUsed, aotraScreen);

				} else {

					// Case one single parallax background image drawing
					// :
					this.drawImageConstrained(ctx, newImage);
				}
				ctx.restore();

				// If we are in edition mode, we draw the parallax
				// backgrounds zones bounds in addition :
				if (aotraScreen.isCalculatedEditable()
				// DEPENDENCY :
				// PlaneVerticalScrollingBubblesPresentingProvider
				&& PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(plane)) {

					this.drawParallaxBackgroundsBounds(ctx);
				}

			}

			/* public */,
			drawImageConstrained : function(ctx, image) {

				var plane = this.plane;
				var aotraScreen = plane.aotraScreen;

				if (typeof image === "undefined" || image === null || getGenericFileType(image.src) !== "image")
					return;

				// Original image aspect ratio must be respected :
				var aotraScreenWidth = aotraScreen.getWidth();
				var aotraScreenHeight = aotraScreen.getHeight();
				var ratio = aotraScreenWidth / aotraScreenHeight;

				var drawX = 0;
				var drawY = 0;

				// Constraints :
				var imageWidth = image.width;
				var imageHeight = image.height;

				var clipWidth = imageWidth;
				var clipHeight = imageHeight;

				// - do not go more than :
				clipWidth = Math.floor(Math.min(imageWidth, (imageHeight * ratio) * this.fractionOfImageUsed));
				clipHeight = Math.floor(Math.min(imageHeight, (imageWidth / ratio) * this.fractionOfImageUsed));

				// - also, image clip must not be greater than screen
				// width :
				clipWidth = Math.min(clipWidth, aotraScreenWidth);
				clipHeight = Math.min(clipHeight, aotraScreenHeight);

				if (this.fractionOfImageUsed < 1) {

					var imageBackgroundXY = this.getImageBackgroundCoordinates(image);

					drawX = imageBackgroundXY.x;
					drawY = imageBackgroundXY.y;

					// We block the possible movements of the clip area
					// :
					if (imageWidth <= drawX + clipWidth)
						drawX = imageWidth - clipWidth;
					if (imageHeight <= drawY + clipHeight)
						drawY = imageHeight - clipHeight;

				} else {
					drawX = 0;
					drawY = 0;
				}

				try {

					ctx.drawImage(image, drawX, drawY, clipWidth, clipHeight, 0, 0, aotraScreenWidth, aotraScreenHeight);

				} catch (e1) {

					// TRACE
					log("==============================");
					log("ERROR WHILE DRAWING : ");
					log("image WH: " + imageWidth + " " + imageHeight);
					log("draw XY: " + drawX + " " + drawY);
					log("clip WH: " + clipWidth + " " + clipHeight);
					log("aotraScreen WH: " + aotraScreenWidth + " " + aotraScreenHeight);
				}

			}

			/* private */,
			getImageBackgroundCoordinates : function(image) {

				var plane = this.plane;
				var aotraScreen = plane.aotraScreen;

				var result = new Array();

				// We only can use aotraScreen's zoom-independent
				// (=data) coordinates here (or else the parallax effect
				// will be insensitive to zoom) :
				var imageX = (aotraScreen.canonicalX * aotraScreen.getZoomFactor() * LONG_DISTANCE_FACTOR) + image.width * (this.fractionOfImageUsed / 2);
				var imageY = (aotraScreen.canonicalY * aotraScreen.getZoomFactor() * LONG_DISTANCE_FACTOR) + image.height * (this.fractionOfImageUsed / 2);

				// Constraints :
				// - do not go more than :
				imageX = Math.min(imageX, image.width * (1 - this.fractionOfImageUsed));
				imageY = Math.min(imageY, image.height * (1 - this.fractionOfImageUsed));

				// - do not go less than :
				imageX = Math.max(imageX, 0);
				imageY = Math.max(imageY, 0);

				result.x = Math.floor(imageX);
				result.y = Math.floor(imageY);

				return result;
			}

			/* private */,
			drawParallaxBackgroundsBounds : function(ctx) {

				var aotraScreen = this.plane.aotraScreen;

				var imgs = this.parsedCanvasImagesSrcForScreenCoordinates;

				ctx.save();
				ctx.beginPath();

				for (var i = 0; i < imgs.length; i++) {
					var img = imgs[i];

					// We won't draw the «-1 sized» background bounds,
					// which is the default one:
					// if(img.size<=0) continue;
					if (img.w <= 0)
						continue; // width is the master parameter

					var f = aotraScreen.getZoomFactor();

					var w = aotraScreen.getWidth();
					var h = aotraScreen.getHeight();

					// var xCenter=(img.x-img.size/2)*f;
					// var yCenter=(img.y-img.size/2)*f;
					var xCenter = (img.x - img.w / 2) * f;
					var yCenter = (img.y - img.h / 2) * f;

					var displayX = xCenter - aotraScreen.x + w / 2;
					var displayY = yCenter - aotraScreen.y + h / 2;

					// If rectangle is out of view, we do not bother
					// with drawing it :

					// if( ((displayX<=0 && displayX+(img.size/2)*f<=0)
					// || (w<=displayX && w<=displayX+(img.size/2)*f))
					// || ((displayY<=0 && displayY+(img.size/2)*f<=0)
					// || (h<=displayY && h<=displayY+(img.size/2)*f)) )
					// continue;
					if (((displayX <= 0 && displayX + (img.w / 2) * f <= 0) || (w <= displayX && w <= displayX + (img.w / 2) * f))
							|| ((displayY <= 0 && displayY + (img.h / 2) * f <= 0) || (h <= displayY && h <= displayY + (img.h / 2) * f)))
						continue;

					ctx.fillStyle = "#888888";
					ctx.font = "bold " + (24) + "px Arial";
					var textDisplayX = displayX;
					var textDisplayY = displayY;
					textDisplayX = Math.max(displayX, 0);
					textDisplayY = Math.max(displayY, 0);
					textDisplayX = Math.min(textDisplayX, w);
					textDisplayY = Math.min(textDisplayY, h);
					ctx.fillText(img.src, textDisplayX, textDisplayY + (24));

					// If rectangle outline is too zoomed in, we do not
					// bother with drawing it :
					if (displayX <= 0 || w <= displayX || displayY <= 0 || h <= displayY)
						continue;

					// var imgSizeW=Math.min(img.size*f,w);
					// var imgSizeH=Math.min(img.size*f,h);
					var imgSizeW = Math.min(img.w * f, w);
					var imgSizeH = Math.min(img.h * f, h);

					ctx.rect(displayX, displayY, imgSizeW, imgSizeH);

					ctx.lineWidth = 8;
					ctx.strokeStyle = "#888888";
					ctx.stroke();

				}

				ctx.closePath();
				ctx.restore();

			}

		});

		return initSelf;
	}
}
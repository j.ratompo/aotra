// GLOBAL CONSTANTS

	const DEFAULT_BUBBLE_NAME="bubble";
	const EDITION_PROVIDER_HOOK_NAME="bubbleEdit";
	const SEPARATOR_CHAR=",";
	
	// Hooks constants
	const DO_ON_OPEN_EDIT="doOnOpenEdit";
	const DO_ON_SAVE_EDIT="doOnSaveEdit";
	const EDIT_WINDOW_BUTTONS="editWindowButtons";
	const EDIT_WINDOW="editWindow";
	const SNIPPETS="snippets";

//## // DEPENDENCY : SPEECH SYNTHESIS PROVIDER
// DEPENDENCY : SCREEN FILES WRAPPING PROVIDER
// DEPENDENCY : MAP PROVIDER
// DEPENDENCY : PLANE VISIBILITY LAYERS PROVIDER
// DEPENDENCY : SCREEN ADVANCED NAVIGATION PROVIDER
// DEPENDENCY : SCREEN I18N PROVIDER
// DEPENDENCY : SCREEN CAROUSEL LIST WRAPPING PROVIDER

	
if(typeof initScreenEditionProviderClass ==="undefined"){
function initScreenEditionProviderClass() {
	var initSelf = this;

	// CONSTANTS
	
	// In order to implement later, undoing and redoing... : 
	var PENDABLE_ACTIONS=new Array();
	PENDABLE_ACTIONS["none"]=new Array();PENDABLE_ACTIONS["none"]["name"]="None";// This action is, actually not pendable...it is instantly.
	PENDABLE_ACTIONS["create"]=new Array();PENDABLE_ACTIONS["create"]["name"]=i18n({"fr":"Créer","en":"Create"});
	PENDABLE_ACTIONS["delete"]=new Array();PENDABLE_ACTIONS["delete"]["name"]=i18n({"fr":"Supprimer","en":"Delete"});// This action is, actually not pendable...it is instantly.
	PENDABLE_ACTIONS["forth"]=new Array();PENDABLE_ACTIONS["forth"]["name"]=i18n({"fr":"Devant","en":"Forth"});// This action is, actually not pendable...it is instantly.
	PENDABLE_ACTIONS["back"]=new Array();PENDABLE_ACTIONS["back"]["name"]=i18n({"fr":"Derrière","en":"Back"});// This action is, actually not pendable...it is instantly.
	PENDABLE_ACTIONS["move"]=new Array();PENDABLE_ACTIONS["move"]["name"]=i18n({"fr":"Déplacer","en":"Move"});
	PENDABLE_ACTIONS["scalePlus"]=new Array();PENDABLE_ACTIONS["scalePlus"]["name"]=i18n({"fr":"Agrandir","en":"Widen"});
	PENDABLE_ACTIONS["scaleMinus"]=new Array();PENDABLE_ACTIONS["scaleMinus"]["name"]=i18n({"fr":"Réduire","en":"Thicken"});
	PENDABLE_ACTIONS["drag"]=new Array();PENDABLE_ACTIONS["drag"]["name"]=i18n({"fr":"Glisser","en":"Move"});
	PENDABLE_ACTIONS["edit"]=new Array();PENDABLE_ACTIONS["edit"]["name"]=i18n({"fr":"Modifier","en":"Edit"});
	PENDABLE_ACTIONS["addLink"]=new Array();PENDABLE_ACTIONS["addLink"]["name"]=i18n({"fr":"Ajouter un lien","en":"Add link"});
	PENDABLE_ACTIONS["makeSubBubbleOf"]=new Array();PENDABLE_ACTIONS["makeSubBubbleOf"]["name"]=i18n({"fr":"Rendre sous-bulle","en":"Make sub-bubble of"});
	PENDABLE_ACTIONS["unmakeSubBubble"]=new Array();PENDABLE_ACTIONS["unmakeSubBubble"]["name"]=i18n({"fr":"Rendre bulle autonome","en":"Make autonomous bubble"});

//	var EDITION_DIV_BACKGROUND="#1E5799;background: -moz-linear-gradient(top, #1e5799 0%, #2989d8 50%, #207cca 51%, #7db9e8 100%);background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(50%,#2989d8), color-stop(51%,#207cca), color-stop(100%,#7db9e8));background: -webkit-linear-gradient(top, #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%);background: -o-linear-gradient(top, #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%);background: -ms-linear-gradient(top, #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%);background: linear-gradient(to bottom, #1e5799 0%,#2989d8 50%,#207cca 51%,#7db9e8 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 );";
//	var EDITION_DIV_BACKGROUND="#75CEDC";
//	var EDITION_DIV_BORDER="#2178DA 5px dotted";
//	var EDITION_DIV_ADDITIONAL_STYLE="";
	
	var TEXTBOX_EDIT_CONTENT_WIDTH=400;
	var TEXTBOX_EDIT_CONTENT_HEIGHT=300;
	var TEXTBOX_EDIT_CONTENT_COLS=65;
	var TEXTBOX_EDIT_CONTENT_ROWS=20;
	var FIELD_LENGTH=95;
	
	// - magnetic positioning grid :
	var GRID_COLOR="#8888FF";
	var DOT_SIZE=20;
	var DEFAULT_GRID_SPACING=100;
//	var NUMBER_OF_ROWS_MIN=10;
//	var NUMBER_OF_ROWS_MAX=20;
	
	var MAGNETIC_THRESHOLD=90;

	var RULERS_COLOR="#8888FF";
	var RULERS_SIZE=20;
	var RULERS_PERSISTENCE_DELAY_SECONDS=2;
	var REFRESHING_RATE=0.1;
	var ADD_BACKWARDS_LINK_ANCHOR_BY_DEFAULT=true;
	
	var PLEASE_SELECT_BUBBLE_MESSAGE="Please select a bubble.";
	var NUMBER_OF_WORDS_EXTRACT=8;
	
	var AMOUNT_OF_TIME_TO_TRIGGER_DRAG_MILLIS=500;
	var MAX_BUBBLE_SIZE=10000;
	var ZOOM_INFLUENCE_FACTOR_ON_RESIZE=0.5;
	var RESIZE_STEP=20;

	// For now, since drag-and-drop works but interferes with text selection that we don't want
	// user to be deprived of (because it can be useful to copy-paste bubble text elsewhere),
	// the drag-and-drop bubble moving option remains deactivated in code :
	// For information, CSS to deactivate text selection on element is the above :
	//	<css selector> {
	//	    -webkit-touch-callout: none;
	//	    -webkit-user-select: none;
	//	    -khtml-user-select: none;
	//	    -moz-user-select: none;
	//	    -ms-user-select: none;
	//	    user-select: none;
	//	}
	var FORBID_DRAG_AND_DROP_MOVE_BUBBLE=true;
	
	var BACKWARDS_LINK_ANCHOR_STRATEGY_BY_DEFAULT="none";
	
	initSelf.AotraScreenEditionProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {

			// Attributes
			this.aotraScreen = aotraScreen;
			this.selectedBubble=null;
			this.addBackwardsLinkAnchor=config.link.forActivePlane().addBackwardsLinkAnchor===false?false:ADD_BACKWARDS_LINK_ANCHOR_BY_DEFAULT;
			this.isThoughtful=IS_BUBBLE_CONTENT_EDIT_THOUGHTFUL;
			// Where do we add backwards link in destination bubble content ?
			var strategy=config.link.forActivePlane().backwardsLinkAddingStrategy;
			this.backwardsLinkAddingStrategy=strategy?strategy:BACKWARDS_LINK_ANCHOR_STRATEGY_BY_DEFAULT;

			// Processed attributes
			this.pendingActionName=PENDABLE_ACTIONS["none"]["name"];
			this.isBubbleEdited=false;
			this.isGridDisplayed=false;
			this.gridSpacing=DEFAULT_GRID_SPACING;
//			this.gridDots=new Array();
			
			// Sub-components

			this.snippetsHooks={};
			
			this.filesWrappingProvider=null;
			this.carouselListWrappingProvider=null;
			if(aotraScreen.uploadProviders && aotraScreen.uploadProviders[EDITION_PROVIDER_HOOK_NAME]){
				// 		- Files wrapping in bubble content:
				this.filesWrappingProvider=new AotraScreenFilesWrappingProvider(this,aotraScreen.uploadProviders[EDITION_PROVIDER_HOOK_NAME]);
				
				//	 	- Carousel lists wrapping in bubble content:
				this.carouselListWrappingProvider=new AotraScreenCarouselListWrappingProvider(this);
			
			}
			
			//Hooks
			var self=this;
			foreach(initSelf.AotraScreenEditionProvider.hooks[INIT],(hook)=>{
				hook.initScreen(aotraScreen, self);
			});
			
	
			// Initialization :
			this.initUI();

			var self=this;
			this.onModelChangeEvents=new Array();
			this.onModelChangeEvents.executeAll=function(){
				var events=self.onModelChangeEvents;
				for(var i=0;i<events.length;i++)
					events[i].execute();
			};
			
			var listener={
				doAction:function(){
				// We clear the bubble selection if we click on nothing
				if(aotraScreen.isCalculatedEditable() && !self.isBubbleCurrentlyBeingEdited()){
					self.setSelectedBubble(null);
				}
			}};
			aotraScreen.getOnClickInVoidEventListeners().push(listener);
			
		}
	
		// Methods
		/*private*/,initUI:function(){
			var aotraScreen=this.aotraScreen;
			
			var width=TEXTBOX_EDIT_CONTENT_WIDTH;
			var height=TEXTBOX_EDIT_CONTENT_HEIGHT;
			
			var xCenter=(aotraScreen.getWidth()/2-width/2);
			var yCenter=(aotraScreen.getHeight()/2-height/2);
			
			var mainDiv=aotraScreen.mainDiv;
			var mainDivInnerHTML = mainDiv.innerHTML;
			
			mainDivInnerHTML+=this.getToolsBarHTMLButtons();
			mainDivInnerHTML+=this.getEditBubbleHTMLInputs(width,height,xCenter,yCenter);
			
			mainDiv.innerHTML=mainDivInnerHTML;
		}

		/*private*/,getToolsBarHTMLButtons:function(){
			
			var mainDivInnerHTML="";		

			var POSITION_X=52;
			var POSITION_Y=46;
			
			mainDivInnerHTML+="<div id='divBlockAllEditActionsStatic' class='buttonsHolder' "
				+" style='left:"+(POSITION_X)+"px;top:"+(POSITION_Y)+"px ; z-index:"+(MAX_Z_INDEX-1)+"; "
				+" max-width:"+(GLOBAL_ICONS_SIZE*2)+"px;"
				// Since this block is not a filled frame, then all clicks must pass through it :
				+" pointer-events:none;'"
				+" > ";
			
				// Help for edition features button :
				// Please see initHelp() function in help.js initialization library :
//				mainDivInnerHTML+="<button class='iconBtn help' title='"+i18n({"fr":"Aide","en":"Help"})+"'> &#9072;</button>";
				mainDivInnerHTML+="<button class='icon helpBtn help' title='"+i18n({"fr":"Aide","en":"Help"})+"'></button>";

//				mainDivInnerHTML+="<button class='iconBtn' id='buttonCreateBubble' onclick='javascript:getAotraScreen().editionProvider.createBubble(event);' title='"+PENDABLE_ACTIONS["create"]["name"]+"'> &#10010;</button>";
				mainDivInnerHTML+="<button class='icon addBubbleBtn' id='buttonCreateBubble' onclick='javascript:getAotraScreen().editionProvider.createBubble(event);' title='"+PENDABLE_ACTIONS["create"]["name"]+"'></button>";

			mainDivInnerHTML+="</div>";

			// Cancel button for floating actions (like «move bubble», «make sub-bubble of», «new bubble», «link to»...)
			mainDivInnerHTML+="<div id='divButtonCancelCurrentOperation' class='buttonsHolder' "
				+" style='left:"+(POSITION_X)+"px;top:"+(POSITION_Y)+"px ; z-index:"+(MAX_Z_INDEX-1)+";'>";
			mainDivInnerHTML+="<button id='buttonCancelCurrentOperation' "
				+" onclick='javascript:getAotraScreen().editionProvider.cancelFloatingAction()' "
				+" style='display:none;pointer-events:all; text-transform:uppercase'>"
				+i18n({"fr":"Annuler","en":"Cancel"})
				+"</button>";
			mainDivInnerHTML+="</div>";
			
			// Action on one currently edited bubble :
			mainDivInnerHTML+="<div id='divBlockAllEditActions' class='buttonsHolder standardBox drags' "
				+" style='left:"+(POSITION_X)+"px;top:"+(POSITION_Y)+"px ; z-index:"+(MAX_Z_INDEX-1)+"; "
				+" max-width:"+(GLOBAL_ICONS_SIZE*2)+"px; "
				+" pointer-events:all;"
				+" display:none ' > ";

				mainDivInnerHTML+="<button class='icon editBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.editBubble();\" title='"+PENDABLE_ACTIONS["edit"]["name"]+"'></button>";//&#9998;
				// (Mobile devices only :)
				
				mainDivInnerHTML+="<button class='icon deleteBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.deleteBubble();\" title='"+PENDABLE_ACTIONS["delete"]["name"]+"'></button>";//&#8855;
				mainDivInnerHTML+="<button class='icon makeSubBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.makeSubBubbleOf();\" title='"+PENDABLE_ACTIONS["makeSubBubbleOf"]["name"]+"'></button>";//&#10525;
				mainDivInnerHTML+="<button class='icon unmakeSubBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.unmakeSubBubble();\" title='"+PENDABLE_ACTIONS["unmakeSubBubble"]["name"]+"'></button>";//&#10525;
				mainDivInnerHTML+="<button class='icon linkBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.addLinkToBubble();\" title='"+PENDABLE_ACTIONS["addLink"]["name"]+"'></button>";//&#9741;
				var disableMoveButton=!this.isEnableMoveButton();
				mainDivInnerHTML+="<button class='icon moveBubbleBtn' id='buttonMoveBubble' "+(disableMoveButton?"disabled":"")+" onclick=\"javascript:getAotraScreen().editionProvider.moveBubble(event);\" title='"+PENDABLE_ACTIONS["move"]["name"]+"'></button>";//&#10535;
				mainDivInnerHTML+="<button class='icon pullBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.moveBubble(event,'forth');\" title='"+PENDABLE_ACTIONS["forth"]["name"]+"'></button>";//&#8670;
				mainDivInnerHTML+="<button class='icon pushBubbleBtn' onclick=\"javascript:getAotraScreen().editionProvider.moveBubble(event,'back');\" title='"+PENDABLE_ACTIONS["back"]["name"]+"'></button>";//&#8671;
				mainDivInnerHTML+="<button class='icon scalePlusBtn' onclick=\"javascript:getAotraScreen().editionProvider.scaleBubble("+RESIZE_STEP+");\" title='"+PENDABLE_ACTIONS["scalePlus"]["name"]+"'></button>";
				mainDivInnerHTML+="<button class='icon scaleMinusBtn' onclick=\"javascript:getAotraScreen().editionProvider.scaleBubble("+(-RESIZE_STEP)+");\" title='"+PENDABLE_ACTIONS["scaleMinus"]["name"]+"'></button>";

				mainDivInnerHTML+="<button class='icon closeBtn' "
					+" style='float:right;' "
					+" onclick='javascript:setElementVisible(\"divBlockAllEditActions\",false);' title='"+i18n({"fr":"Fermer","en":"Close"})+"'></button>";//&#10006;
				
			mainDivInnerHTML+="</div>";
			
			return mainDivInnerHTML;
		}

		
		// ====================================== SCALE BUBBLE ======================================
		/*private*/,scaleBubble:function(sizeDeltaAmount){
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble)	return;
			
			var aotraScreen=this.aotraScreen;
			
			var sizeDelta=sizeDeltaAmount*(1/aotraScreen.getZoomFactor())*ZOOM_INFLUENCE_FACTOR_ON_RESIZE;
			
			var newSizeW=Math.round(selectedBubble.getMaxW()+sizeDelta);
			var newSizeH=Math.round(selectedBubble.getMaxH()+sizeDelta);

			if(newSizeW<=0 || MAX_BUBBLE_SIZE<=newSizeW
			|| newSizeH<=0 || MAX_BUBBLE_SIZE<=newSizeH)	return;
			
			selectedBubble.setMaxW(newSizeW);
			selectedBubble.setMaxH(newSizeH);

			this.onModelChangeEvents.executeAll();
			aotraScreen.drawAndPlace();

		}
		
		// ====================================== EDIT BUBBLE ======================================

		/*private*/,getEditBubbleHTMLInputs:function(width,height,xCenter,yCenter){
			var aotraScreen=this.aotraScreen;
			
			var cols=TEXTBOX_EDIT_CONTENT_COLS;
			var rows=TEXTBOX_EDIT_CONTENT_ROWS;
			
			var mainDivInnerHTML="";
			
			var xUI=150;
			var yUI=20;
			
			// Bubble editing fields :
			mainDivInnerHTML+="<table id='divEditContent' class='buttonsHolder standardBox drags' " 
				+" style='display:none;"
				+" left:"+(xUI)+"px; top:"+(yUI)+"px;"
				+" z-index:"+MAX_Z_INDEX_OVERLAY+";" 
				+" width:740px ;"
				+" ' >"; 
			
				mainDivInnerHTML+="<tr>";
				
				mainDivInnerHTML+="<td>";
				
				// TODO : encapsulate in a single-line buttons group div ?
				
				// TODO : Migrate this to hook [EDIT_WINDOW_BUTTONS] ...:
				// DEPENDENCY : FILES WRAPPING PROVIDER
				// Wrapped files inserts :
				if(this.filesWrappingProvider){
					mainDivInnerHTML+=this.filesWrappingProvider.getGeneratedHTMLInputs();
				}
				
				// TODO : Migrate this to hook [EDIT_WINDOW_BUTTONS] ...:
				// DEPENDENCY : SCREEN CAROUSEL LIST WRAPPING PROVIDER
				// Carousel list wrapping inserts :
//				if(this.carouselListWrappingProvider){
//					mainDivInnerHTML+=this.carouselListWrappingProvider.getGeneratedHTMLInputs();
//				}
				
				
				// Snippets
				var snippetsIniters=AotraScreenEditionProvider.hooks[SNIPPETS];
				for(key in snippetsIniters){
					if(!snippetsIniters.hasOwnProperty(key))	continue;
					snippetsIniters[key].init(aotraScreen,this);
				}
				// Select choice for snippets insertion :
				if(!empty(this.snippetsHooks)){
					mainDivInnerHTML+="<hr><select style='width:100%;' id='snippetsSelect'>";
					for(key in this.snippetsHooks){
						if(!this.snippetsHooks.hasOwnProperty(key))	continue;
						var snippetHook=this.snippetsHooks[key];
						mainDivInnerHTML+=snippetHook.getGeneratedHTMLOption(key);
					}
					mainDivInnerHTML+="</select>";
					mainDivInnerHTML+="<button onclick='javascript:getAotraScreen().editionProvider.insertSnippet()' style='width:100%;'>OK</button>";
				}
				
				// Hooks
				var editionIniters=AotraScreenEditionProvider.hooks[EDIT_WINDOW_BUTTONS];
				for(key in editionIniters){
					if(!editionIniters.hasOwnProperty(key))	continue;
					mainDivInnerHTML+=editionIniters[key].init(aotraScreen,this);
				}


				
				
				// DEPENDENCY : MAP PROVIDER
				// Add Map marker button :
				// TODO : FIXME : Do not display if there's no map as any plane background...
				// (because, at this point, aotraScreen.getActivePlane() is still null...)
				mainDivInnerHTML+=PlaneMapProvider.getGeneratedHTMLInputsAddMarker();
				
				// Add link button :
				mainDivInnerHTML+="<button class='icon linkInsertBtn' id='buttonAddLinkInTextToBubble' onclick='javascript:getAotraScreen().editionProvider.addLinkInTextToBubble();' "
					+"title='"+i18n({"fr":"Insérer une balise de lien vers une autre bulle","en":"Add link tag"})+"'></button>";//&#10565;
				
				// This element sets the width of the edition tools column :
				mainDivInnerHTML+="<hr style='width:"+(FIELD_LENGTH*2+30)+"px;' />";
				
				// DEPENDENCY : SCREEN I18N PROVIDER
				mainDivInnerHTML+=AotraScreenI18nProvider.getGeneratedHTMLInputs();
	
				mainDivInnerHTML+="<hr/>";
				mainDivInnerHTML+=" <label id='labelParentBubbleName' style='height:20px;display:none;' title='Parent bubble name...'></label>";
				mainDivInnerHTML+=" <input id='textboxEditName' type='text' style='height:20px;width:"+FIELD_LENGTH+"px;' title='Bubble name...' "
					// FOR NOW (and probably for a long time, since it can introduce so many problems !) :
					+" disabled readOnly />";
				
				mainDivInnerHTML+=" <input id='textboxEditColor' type='text' style='height:20px;width:"+FIELD_LENGTH+"px;' title='Bubble color and opacity («:» then 0-1 float number)...' "
								+" onkeyup=\"javascript:getAotraScreen().editionProvider.changeTextboxComponentColor(this.value);\" "
								+" />";
				mainDivInnerHTML+=" <input id='textboxEditSizeW' type='number' style='height:20px;width:"+FIELD_LENGTH+"px;' title='Bubble width...' />";
				mainDivInnerHTML+=" <input id='textboxEditSizeH' type='number' style='height:20px;width:"+FIELD_LENGTH+"px;' title='Bubble height...' />";

				mainDivInnerHTML+=" <input id='textboxEditBorderStyle' type='text' style='height:20px;width:"+FIELD_LENGTH+"px;' title='Bubble border style...' />";

//				var MOLDS_LISTS=[CIRCLE_MOLD, SQUARED_MOLD, ROUNDED_MOLD, INVISIBLE_TO_VIEW_MOLD,INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE];
//				// TODO : FIXME :
//				if(isChrome())  // CAUTION : Firefox and IE currently do not support CSS shapes today (11/2015)
				var MOLDS_LISTS=[CIRCLE_MOLD, SQUARED_MOLD, ROUNDED_MOLD, INVISIBLE_TO_VIEW_MOLD,INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE,PENTAGON_MOLD,HEXAGON_MOLD,HEPTAGON_MOLD,OCTOGON_MOLD];
				
				mainDivInnerHTML+=renderSimpleSelect("selectEditMold",MOLDS_LISTS,"width:"+FIELD_LENGTH+"px;");
				mainDivInnerHTML+="&nbsp;&nbsp;"+renderSimpleSelect("selectEditOpenedOnCreate",["","true", "false", REMAIN_CLOSED, REMAIN_HIDDEN],"width:"+FIELD_LENGTH+"px;");
				mainDivInnerHTML+=renderSimpleSelect("selectEditFixedStrategy",["",FIXED, NON_FOCUSABLE],"width:"+FIELD_LENGTH+"px;");
				mainDivInnerHTML+="<hr />";
				mainDivInnerHTML+=" <input id='checkboxIsOpenableClosable' type='checkbox' /><label for='checkboxIsOpenableClosable'>"+i18n({"fr":"Ouvrable/refermable","en":"Openable/closable"})+"</label>";
				mainDivInnerHTML+="<br />";
				mainDivInnerHTML+=" <input id='checkboxIsHomeBubble' type='checkbox' /><label for='checkboxIsHomeBubble'>"+i18n({"fr":"Bulle d’accueil?","en":"Home bubble"})+"</label>";
				mainDivInnerHTML+="<br />";
				// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
				// mainDivInnerHTML+=" <input id='checkboxIsExcludingText' type='checkbox' /><label for='checkboxIsExcludingText'>"+i18n({"fr":"Exclut le texte autour d’elle?","en":"Excludes text around"})+"</label>";
				// mainDivInnerHTML+="<br />";
				mainDivInnerHTML+=" <input id='textboxEditVisibilityLayers' type='text' style='height:20px;width:"+FIELD_LENGTH+"px;' title='Bubble visibility layers which it belongs...' "
					+" placeHolder='visibility layers key names, separated by comas' />";

				mainDivInnerHTML+=" <input id='textboxEditParallaxBackground' type='text' style='height:20px;width:"+FIELD_LENGTH+"px;' title='The image displayed behind this bubble...' "
					+" placeHolder='file name' />";
				
				mainDivInnerHTML+="<hr />";
				
				mainDivInnerHTML+="<span style='display:block;margin-top:10px'>";

					// KEEP THIS CODE LINE, PLEASE :
					//mainDivInnerHTML+="<button id='buttonSaveBubble' onclick='javascript:getAotraScreen().editionProvider.saveBubble();'>Save</button>";
					mainDivInnerHTML+="<button id='buttonSaveAndCloseBubble' onclick='javascript:getAotraScreen().editionProvider.saveBubble();javascript:getAotraScreen().editionProvider.close();' "
						+" class='icon saveBtn' title='"+i18n({"fr":"Sauvegarder et fermer","en":"Save and close"})+"'></button>";

					// Close dialog button
					mainDivInnerHTML+="<button id='buttonCancelEditBubble' "
						+" class='icon closeBtn' "
						+" style='float:right' " 
						+" onclick='javascript:getAotraScreen().editionProvider.close();'"
						+" title='"+i18n({"fr":"Fermer","en":"Close"})+"'></button>";//&#10006;

				mainDivInnerHTML+="</span>";
	
				
				mainDivInnerHTML+="</td>";
				
				mainDivInnerHTML+="<td style='width:60%'>";
				mainDivInnerHTML+="<textarea id='textboxEditContent' cols='"+cols+"' rows='"+rows+"' style='position : relative;overflow:scroll; ' "
					+" placeHolder='Please enter HTML content...' "
					+" ></textarea>";
				mainDivInnerHTML+="</td>";

				mainDivInnerHTML+="</tr>";
				
			mainDivInnerHTML+="</table>";
			
			return mainDivInnerHTML;
		}

		
		// Snippets insertion :
		
		/*private*/,insertSnippet:function(){
			var selectedSnippetProvider=document.getElementById("snippetsSelect").value;
			if(nothing(selectedSnippetProvider))	return;
			var hook=this.snippetsHooks[selectedSnippetProvider];
			if(nothing(hook))	return;
  		var selectedText=getSelectedText(this.getTextboxComponent()).trim();
			var htmlToInsert=hook.getInsertedSnippet(selectedText);
			this.performSnippetInsertionInContent(htmlToInsert,hook.appendStrategy);
		}
		

		/*private*/,performSnippetInsertionInContent:function(htmlToInsert,/*OPTIONAL*/appendStrategy){
			
			var editedBubble=this.getSelectedBubble();
			
			var textboxComponent=this.getTextboxComponent();
			var selectedText=getSelectedText(textboxComponent).trim();
		
			this.commitContentToSelectedBubble();
			var content=toOneSimplifiedLine(editedBubble.getContent(),"compactHTML");

			// Case some text selected :
			if(!nothing(selectedText)){
				content=content.replace(selectedText,htmlToInsert);
			}else{
				// Case no text selected :
				if(appendStrategy && appendStrategy==="end")
					content=content+htmlToInsert;
				else
					content=htmlToInsert+content;
			}
			
			editedBubble.setContent(content);
			textboxComponent.setContent(content);
		
		}
		
		
		,editBubble:function(isNewBubble/*OPTIONAL*/){
			var self=this;

			var aotraScreen=this.aotraScreen;
			
			if(this.isBubbleCurrentlyBeingEdited())	return;
			
			//TODO : add others attributes editing
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble){
				alert(PLEASE_SELECT_BUBBLE_MESSAGE);
				return;
			}
			
			// PLEASE KEEP CODE
			// TODO : FIXME : Implement drag-and-drop on window :
			// We display the adequate windows :
			//		- Dialog dragging enabling :
//			var divEditContent=document.getElementById("divEditContent");
//			if(!divEditContent.inited){
//				jQuery(divEditContent).drags();
//				divEditContent.inited=true;
//			}

			// Open the content edition window :
			setElementVisible("divEditContent",true);
			this.isBubbleEdited=true;

			// Hide the side-bubble edition window :
			setElementVisible("divBlockAllEditActions",false);
			
			// We don't want double quotes, only single quotes :
			var editableDiv=this.initTextboxComponent(selectedBubble);
			
			// DEPENDENCY : FILES WRAPPING PROVIDER
	        // File wrapping sub-component textbox component update :
			if(this.filesWrappingProvider)
				this.filesWrappingProvider.setTextboxComponent(editableDiv);
			
			
			// Hooks
			var editionWindowIniters=AotraScreenEditionProvider.hooks[EDIT_WINDOW];
			for(key in editionWindowIniters){
				if(!editionWindowIniters.hasOwnProperty(key))	continue;
				editionWindowIniters[key].init(aotraScreen,editableDiv);
			}
			
			var parentBubble=selectedBubble.getParentBubble();
			var labelParentBubbleName=document.getElementById("labelParentBubbleName");
			if(parentBubble){
				labelParentBubbleName.style.display="block";
				labelParentBubbleName.innerHTML=i18n({"fr":"Bulle parent:","en":"Parent bubble:"})+parentBubble.getName();
			}else{
				labelParentBubbleName.style.display="none";
			}
			
			var textboxEditName=document.getElementById("textboxEditName");
			textboxEditName.value=selectedBubble.getName();
			if(isNewBubble)	textboxEditName.disabled=true;
			else textboxEditName.disabled=false;
			
			document.getElementById("textboxEditColor").value=selectedBubble.color;
			document.getElementById("textboxEditSizeW").value=Math.round(selectedBubble.maxW);
			document.getElementById("textboxEditSizeH").value=Math.round(selectedBubble.maxH);

			document.getElementById("textboxEditBorderStyle").value=selectedBubble.borderStyle;
			
			setSelectedValueInSelect("selectEditMold",selectedBubble.getMold());
			
			document.getElementById("textboxEditVisibilityLayers").value=selectedBubble.visibilityLayers?selectedBubble.visibilityLayers:"";
			document.getElementById("textboxEditParallaxBackground").value=selectedBubble.parallaxBackground?selectedBubble.parallaxBackground:"";

			setSelectedValueInSelect("selectEditOpenedOnCreate",selectedBubble.openedOnCreate?selectedBubble.openedOnCreate:"");

			document.getElementById("checkboxIsOpenableClosable").checked=selectedBubble.isOpenableClosable()?true:false;
			
			setSelectedValueInSelect("selectEditFixedStrategy",selectedBubble.fixedStrategy?selectedBubble.fixedStrategy:"");

			document.getElementById("checkboxIsHomeBubble").checked=selectedBubble.getIsHomeBubble()?true:false;

			// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
			// document.getElementById("checkboxIsExcludingText").checked=selectedBubble.getIsExcludingText()?true:false;

			if(isNewBubble){
				// We delete the bubble on cancel only if it's a newly created one :
				document.getElementById("buttonCancelEditBubble").onclick=function(){
					
					// Bubble deletion from aotraScreen :
					aotraScreen.deleteBubble(selectedBubble);
					self.setSelectedBubble(null);
					// We don't navigate to new bubble in this case 
					
					aotraScreen.drawAndPlace();
					
					self.close();
				};
				
				// // We close edition dialog on save only if it's a newly created one :
				// document.getElementById("buttonSaveBubble").onclick=function(){
				//	self.saveBubble();
				//	self.close();
				//};

			}else{
				
				// We do not delete the bubble on cancel if it's a newly created one :
				document.getElementById("buttonCancelEditBubble").onclick=function(){
					self.close();
				};
				
				// // We do not close edition dialog on save only if it's a newly created one :
				//document.getElementById("buttonSaveBubble").onclick=function(){
				//	self.saveBubble();
				//};
			}
			
			this.pendingActionName=PENDABLE_ACTIONS["edit"]["name"];

			// Hooks for other objects to trigger treatments on opening editing dialog for a bubble :
			var hooks=AotraScreenEditionProvider.hooks[DO_ON_OPEN_EDIT];
		    for (var i=0, hook; hook = hooks[i]; i++) {
				hook.execute(aotraScreen,this);
			}
			
		}
		
		
		
		/*private*/,initTextboxComponent:function(selectedBubble){
		
			var content=toOneSimplifiedLine(selectedBubble.getContent(),"compactHTML").replace(new RegExp("\"","gim"),'"');

			// Nicedit :
			this.textboxEditContentRich=new nicEditor({fullPanel : true}).panelInstance("textboxEditContent");

			
      this.changeTextboxComponentColor(selectedBubble.color);
      var editableDiv=this.getTextboxComponent();
      
      // It is important to normalize html passed to editor's textbox :
      editableDiv.innerHTML=toOneSimplifiedLine(content,"compactHTML");
      
      // We update text component's caret position :
      jQuery(editableDiv).click(function(){
      	editableDiv.lastCaretPosition=jQuery(this).caret();
      });
      
      return editableDiv;
		}
		
		/*private*/,saveBubble:function(){
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			
			// We save content :
			this.commitContentToSelectedBubble();
			
			var selectedBubble=this.getSelectedBubble();
			selectedBubble.setColor(document.getElementById("textboxEditColor").value.trim());
			selectedBubble.setMaxW(parseFloat(document.getElementById("textboxEditSizeW").value.trim()));
			selectedBubble.setMaxH(parseFloat(document.getElementById("textboxEditSizeH").value.trim()));
			
			selectedBubble.setBorderStyle(document.getElementById("textboxEditBorderStyle").value.trim());
			
			
			var newMold=getSelectedValueInSelect("selectEditMold");
			if(newMold)	selectedBubble.setMold(newMold.trim());
			
					
			selectedBubble.setVisibilityLayers(document.getElementById("textboxEditVisibilityLayers").value.trim());
			
			var textboxEditParallaxBackgroundValue=document.getElementById("textboxEditParallaxBackground").value.trim();
			if(nothing(textboxEditParallaxBackgroundValue,true)){
				if(activePlane.planeParallaxBackground)
					activePlane.parallaxBackgroundsProvider.removeBubbleParallaxBackground(selectedBubble);
			}
			selectedBubble.setParallaxBackground(textboxEditParallaxBackgroundValue.trim());
			
			var oldOpenedOnCreateValue=selectedBubble.openedOnCreate;
			var newOpenedOnCreateValue=getSelectedValueInSelect("selectEditOpenedOnCreate");
			if(newOpenedOnCreateValue)	selectedBubble.setOpenedOnCreate(newOpenedOnCreateValue.trim());
	
			var newIsOpenableClosableValue=!nothing(document.getElementById("checkboxIsOpenableClosable").checked);
			selectedBubble.setIsOpenableClosable(newIsOpenableClosableValue);
			
			
			if(oldOpenedOnCreateValue!==newOpenedOnCreateValue){
				// TRACE
				alert("Effects of some modified attributes will be visible only on next model reload (You can force it with «Edit XML source -> OK.»).");
			}

			
//			var isBubbleNewlyFixed=document.getElementById("textboxEditFixedStrategy").value.trim()==="fixed";
			var newFixedStrategy=getSelectedValueInSelect("selectEditFixedStrategy");
			
			if(!newFixedStrategy)	newFixedStrategy="";
			
			var isBubbleNewlyFixed=(newFixedStrategy+"").trim()==="fixed";
			// Coordinates adjustments if bubble is made fixed or non fixed :
			if(!selectedBubble.isFixed() && isBubbleNewlyFixed){
			// If it's made fixed whereas it was not :
				var fixedLocationX=(aotraScreen.getWidth()/2);
				var fixedLocationY=(aotraScreen.getHeight()/2);
				selectedBubble.setLocation(fixedLocationX,fixedLocationY);
			}else if(selectedBubble.isFixed() && !isBubbleNewlyFixed){
			// If it's made non fixed whereas it was :
					var nonFixedLocationX=(aotraScreen.x-aotraScreen.getWidth()/2+selectedBubble.x);
					var nonFixedLocationY=(aotraScreen.y-aotraScreen.getHeight()/2+selectedBubble.y);
					selectedBubble.setLocation(nonFixedLocationX,nonFixedLocationY);
			}
			
//			selectedBubble.setFixedStrategy(document.getElementById("textboxEditFixedStrategy").value.trim());
			selectedBubble.setFixedStrategy(newFixedStrategy.trim());
			
			selectedBubble.setIsHomeBubble(!nothing(document.getElementById("checkboxIsHomeBubble").checked));
			
			// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
			// selectedBubble.setIsExcludingText(!nothing(document.getElementById("checkboxIsExcludingText").checked));
			
			
			// DEPENDENCY : PLANE VISIBILITY LAYERS PROVIDER 
			// Eventual visibility layers treatment for bubble : (each time a bubble is edited)
			if(activePlane && activePlane.visibilityLayersProvider){
				activePlane.visibilityLayersProvider.initVisibilityLayersProviderUI(aotraScreen);
			}
			
			// Bubble name :
			var newName=document.getElementById("textboxEditName").value.trim();
			if(contains(newName,"([\\s\\t\n\r]|\\"+SEPARATOR_CHAR+")",true)){
				alert("Please choose a name without spaces, nor the separator character «"+SEPARATOR_CHAR+"».");
				return;
			}

			// Bubble name unicity constraint check :
			var oldName=selectedBubble.getName();
			if(oldName!==newName){
				if(this.aotraScreen.getBubbleByName(newName)!==null){
					alert("Please choose a unique name within aotraScreen.");
					return;
				}
				
				if(!confirm("Are you sure you want to change this bubble's name ? All links referencing it will be lost."))
					return;
				
				// All links referencing this bubble name must be deleted :
				activePlane.removeLinksWithBubble(selectedBubble);
			}
			selectedBubble.setName(newName);

			
			// Hooks for other objects to trigger treatments on saving a bubble :
			var hooks=AotraScreenEditionProvider.hooks[DO_ON_SAVE_EDIT];
		    for (var i=0, hook; hook = hooks[i]; i++) {
				hook.execute(aotraScreen,this);
			}
			
		  
			
			this.onModelChangeEvents.executeAll();
			aotraScreen.drawAndPlace();
			
		}

		// ====================================== ADD LINK TO BUBBLE ======================================
		
		/*private*/,getWrappedContentForLink:function(newSelectedBubble,link){
			
			var linkBackwardsTag=link.getHTMLTag("backwards");
			
			// At this hour, I still don't know if I should normalize HTML to this point...:
			var content=newSelectedBubble.getContent();
			
		
			if(this.backwardsLinkAddingStrategy==="both")
				return 	 "<br /><br />"+linkBackwardsTag+"<br /><br />"
						+content
						+"<br /><br />"+linkBackwardsTag+"<br /><br />";
			else if(this.backwardsLinkAddingStrategy==="before")
				return 	 "<br /><br />"+linkBackwardsTag+"<br /><br />"
						+content;
			if(this.backwardsLinkAddingStrategy==="after")
				return 	 content
						+"<br /><br />"+linkBackwardsTag+"<br /><br />";
			
			return content;
		
		}
		
		,addLinkToBubble:function(){
			var self=this;
			if(self.isOperationPending())	return;

			// We add a link HTML entry point in the bubble content :
			var adjustBubblesContentsForLinks=function(link,selectedBubble,newSelectedBubble){
				
				// We first save content modified in editor :
				self.commitContentToSelectedBubble();
				
				// At this hour, I still don't know if I should normalize HTML to this point...:
				var content=selectedBubble.getContent();
				selectedBubble.setContent(content+"<br />"+link.getHTMLTag()+"<br /><br />");
				
				// We add the backwards link anchor :
				if(self.addBackwardsLinkAnchor===true){
					newSelectedBubble.setContent(self.getWrappedContentForLink(newSelectedBubble,link));
				}
			};

			this.addLinkFromBubbleToNewlySelectedBubble(false,adjustBubblesContentsForLinks);
			
			this.pendingActionName=PENDABLE_ACTIONS["addLink"]["name"];

		}
		
		,addLinkInTextToBubble:function(){
			var self=this;
			if(self.isOperationPending() && !self.isBubbleCurrentlyBeingEdited())	return;
			

			var textboxComponent=this.getTextboxComponent();
			if(!textboxComponent){
				self.close();
				return;
			}
			
			var selectedText=getSelectedText(textboxComponent);
			
			if(nothing(selectedText)){
				alert("Please select text to be link's label.");
				return;
			}
			
			self.setEditWindowTransparent(true);
			
			// We add a link HTML entry point in the bubble content :
			var adjustBubblesContentsForLinks=function(link,selectedBubble,newSelectedBubble){
				link.label=selectedText;
				
				// We first save content modified in editor :
				self.commitContentToSelectedBubble();

				// At this hour, I still don't know if I should normalize HTML to this point...:
				var content=selectedBubble.getContent();
				
				selectedBubble.setContent(content.replace(selectedText,link.getHTMLTag()));
				self.getTextboxComponent().innerHTML=selectedBubble.getContent();
				
				// We add the backwards link anchor :
				if(self.addBackwardsLinkAnchor===true){
					newSelectedBubble.setContent(self.getWrappedContentForLink(newSelectedBubble,link));
				}

				self.setEditWindowTransparent(false);
			};

			this.addLinkFromBubbleToNewlySelectedBubble(true,adjustBubblesContentsForLinks);
			
			
			// The pending action here is still «edit», and not «addLink» :
			this.pendingActionName=PENDABLE_ACTIONS["edit"]["name"];
		}
		
	
		// This method is only used in the two methods above :
		/*private*/,addLinkFromBubbleToNewlySelectedBubble:function(preserveBubbleSelection,adjustBubblesContentsForLinks){
			var aotraScreen=this.aotraScreen;
			var self=this;
			
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble){
				alert(PLEASE_SELECT_BUBBLE_MESSAGE);
				return;
			}

			var editionProvider=this;
			
			this.oldSetSelectedBubbleFunction = editionProvider.setSelectedBubble;
			this.oldAotraScreenOnClickFunction=aotraScreen.onClickAotraScreen;
			aotraScreen.onClickAotraScreen=function(){/*DO NOTHING*/};
			
			
			editionProvider.setSelectedBubble=function(newSelectedBubble){
				
				var activePlane=aotraScreen.getActivePlane();
				var selectedBubble=editionProvider.getSelectedBubble();
				
				// We don't allow zero-sized links (=source bubble identical to destination bubble)
				if( newSelectedBubble!=null && selectedBubble.getName()!=newSelectedBubble.getName() ){
				
					if(preserveBubbleSelection==false){
						
						aotraScreen.notifyBubbleSelectionHasChanged(editionProvider.selectedBubble,newSelectedBubble);
						editionProvider.selectedBubble=newSelectedBubble;
						// Moving with drag and drop abilities initialization :
						editionProvider.initDragAndDropMoveOnBubble(newSelectedBubble);
					}
					aotraScreen.draw();

					// Link attributes :
					/*
						fromBubbleName,toBubbleName,planeName,
						color,size,mold,label
						,routingStrategy
					*/
					var defaultLinkLabel=newSelectedBubble.getName()+"---go!";
					var targetContent=newSelectedBubble.getContent();
					if(!nothing(targetContent,true)){
						defaultLinkLabel=getTextWordsExtract(targetContent,NUMBER_OF_WORDS_EXTRACT)+"...";
					}
					
					var link=new Link(selectedBubble.getName(),newSelectedBubble.getName(),activePlane.getName()
							,config.link.forActivePlane().color,config.link.forActivePlane().size,config.link.forActivePlane().mold,defaultLinkLabel
							,config.link.forActivePlane().routingStrategy,null);
					
					if(!activePlane.equivalentLinkExists(link)){
						activePlane.addLink(link);
						
						// We add a link HTML entry point in the bubble content :
						adjustBubblesContentsForLinks(link,selectedBubble,newSelectedBubble);
						
					}else{
						alert("Cannot create link : an equivalent link already exists.");
					}
				}
				
				self.cancelFloatingAction();
				
				self.onModelChangeEvents.executeAll();

			};
			

			aotraScreen.draw();
			
			this.setFloatingActionsCancelButtonVisibleForAction(true,PENDABLE_ACTIONS["addLink"]["name"]);

			
		}
		
		// ====================================== CREATE BUBBLE ======================================
		,createBubble:function(event){

			// NECESSARY TO AVOID STRANGE BUGS ON CLICK :
			event.preventDefault();
			event.stopPropagation();
			
			var self=this;
			var aotraScreen=this.aotraScreen;
			
			if(self.isOperationPending())	return;

			
			var plane=null;
			if(aotraScreen.getActivePlane()){
				plane=aotraScreen.getActivePlane();
			}else if(!empty(aotraScreen.getPlanes())){
				plane=aotraScreen.getPlanes()[0];
			}else{
				throw new Error("Screen has no plane to add this bubble to.");
			}
			
			this.oldAotraScreenOnClickFunction = aotraScreen.onClickAotraScreen;
			
			
			// FLOATING PENDABLE_ACTIONS CANCEL BUTTON :
			this.setFloatingActionsCancelButtonVisibleForAction(true,PENDABLE_ACTIONS["create"]["name"]);

			
			aotraScreen.onClickAotraScreen=function(event,bubble){
				
				aotraScreen.refreshMousePosition(event);
					
				var destinationX=self.getAotraScreenClickedDataXY()["x"];
				var destinationY=self.getAotraScreenClickedDataXY()["y"];
				
				// Bubble arguments :
				// name,x,y,w,h
				//,mold,contents,fontSize
				//,openableClosable,color
				//,openCloseAnimation,borderStyle,openedOnCreate,fixedStrategy
				//,		isHomeBubble,isExcludingText
				//,		visibilityLayers
				//,		parallaxBackground
				//,		aotraScreen
				// Of course bubble name <DEFAULT_BUBBLE_NAME> is already taken... let the plane decide how to name this new bubble !
				var configForActivePlane=config.bubble.forActivePlane();
				var newBubble=new Bubble(
						DEFAULT_BUBBLE_NAME,destinationX,destinationY,configForActivePlane.width,configForActivePlane.height
						,configForActivePlane.mold
						// NOT TRUE ANY MORE : default html content can only be placed in static javascript, not in plane model config JSON !
						,configForActivePlane.content
						,configForActivePlane.fontSize,configForActivePlane.openableClosable,configForActivePlane.color
						,configForActivePlane.openCloseAnimation,configForActivePlane.borderStyle,configForActivePlane.openedOnCreate
						,configForActivePlane.fixedStrategy,configForActivePlane.isHomeBubble//<--default is false value for this attribute, so it's OK if this configuration value does not exist...
						,configForActivePlane.isExcludingText
						,null
				);
				plane.addBubbleToPlane(newBubble);

				self.cancelFloatingAction();
				
				
				aotraScreen.notifyBubbleSelectionHasChanged(self.selectedBubble,newBubble);
				// Then we immediately edit the newly created bubble :
				self.selectedBubble=newBubble;
				// Moving with drag and drop abilities initialization :
				self.initDragAndDropMoveOnBubble(newBubble);

				aotraScreen.drawAndPlace();
				self.editBubble(true);

				
				
			};
		
			this.pendingActionName=PENDABLE_ACTIONS["create"]["name"];
			
			this.isGridDisplayed=true;
			aotraScreen.draw();

		}
		
		
		// ====================================== MOVE BUBBLE ======================================
		
	
		,initDragAndDropMoveOnBubble:function(bubble){
			var aotraScreen=this.aotraScreen;
			var div=bubble.div;
			
			// No drag-and drop if a move button exists :
			if(this.isEnableMoveButton())	return;
			

			var self=this;

			// TODO : FIXME : NOT WORKING ON FIREFOX:
//			div.draggable=true;
//			div.ondragstart=function(event){	self.dragStartMethod(event,bubble);	};
//			div.ondragend=function(event){		self.dragEndMethod(event,bubble);	};

			// TODO : FIXME : ...
			var s=null;
			var hasDraggingStarted=false;
			// Drag-And-Drop moving : (Desktop devices only)
			var dragStartMethod=function(event){
				var aotraScreen=self.aotraScreen;

				self.oldAotraScreenOnClickFunction = aotraScreen.onClickAotraScreen;
				
				// FLOATING PENDABLE_ACTIONS CANCEL BUTTON :
				self.setFloatingActionsCancelButtonVisibleForAction(true,PENDABLE_ACTIONS["drag"]["name"]);
				
				aotraScreen.onClickAotraScreen=function(){/*DO NOTHING*/};
				
				self.pendingActionName=PENDABLE_ACTIONS["drag"]["name"];
				
			};
			
			div.onmousedown=function(event){
				if(self.pendingActionName!==PENDABLE_ACTIONS["none"]["name"])	return;
				
				if(event.target.tagName==="button" || event.target.tagName==="input") return;
				
				s=setTimeout(function(){ 
								dragStartMethod(event);
								hasDraggingStarted=true;
								aotraScreen.mainDiv.setStyle("cursor:move");
							}
						,AMOUNT_OF_TIME_TO_TRIGGER_DRAG_MILLIS);
			};
			
			
			var dragEndMethod=function(event){
				var aotraScreen=self.aotraScreen;

				aotraScreen.refreshMousePosition(event);
				
				var selectedBubble=self.getSelectedBubble();
				
				aotraScreen.refreshMousePosition(event);
				var destinationX=self.getAotraScreenClickedDataXY(selectedBubble.isFixed())["x"];
				var destinationY=self.getAotraScreenClickedDataXY(selectedBubble.isFixed())["y"];
				self.setBubbleToNewPosition(aotraScreen,selectedBubble,destinationX,destinationY);
				
			};

			aotraScreen.mainDiv.onmouseup=function(event){
				
				if(event.target.tagName==="button" || event.target.tagName==="input") return;
				
				clearTimeout(s);
				
				if(hasDraggingStarted){
					aotraScreen.mainDiv.setStyle("cursor:default");
					dragEndMethod(event);
					hasDraggingStarted=false;
				}
			};
			// We duplicate this event :
			div.onmouseup=aotraScreen.mainDiv.onmouseup;
			
		}
		
		
			
		// Point-And-Click moving: (Mobile devices only :)
		,moveBubble:function(event,direction){
			
			// NECESSARY TO AVOID STRANGE BUGS ON CLICK :
			event.preventDefault();
			event.stopPropagation();

			var self=this;
			if(self.isOperationPending())	return;
			var aotraScreen=this.aotraScreen;
			
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble){
				alert(PLEASE_SELECT_BUBBLE_MESSAGE);
				return;
			}

			if(!direction){
				
				this.oldAotraScreenOnClickFunction = aotraScreen.onClickAotraScreen;
				
				// FLOATING PENDABLE_ACTIONS CANCEL BUTTON :
				this.setFloatingActionsCancelButtonVisibleForAction(true,PENDABLE_ACTIONS["move"]["name"]);
				
				
				aotraScreen.onClickAotraScreen=function(event,bubble){
					
					aotraScreen.refreshMousePosition(event);
					var destinationX=self.getAotraScreenClickedDataXY(selectedBubble.isFixed())["x"];
					var destinationY=self.getAotraScreenClickedDataXY(selectedBubble.isFixed())["y"];
					self.setBubbleToNewPosition(aotraScreen,selectedBubble,destinationX,destinationY);
					
				};
				
				this.pendingActionName=PENDABLE_ACTIONS["move"]["name"];
				
				this.isGridDisplayed=true;
				aotraScreen.draw();
				
				
			}else if(direction==="forth"){
				var STEP=1;
				if(MAX_Z_INDEX_BUBBLES<=selectedBubble.zIndex+STEP)	return;
				selectedBubble.zIndex+=STEP;
				selectedBubble.placeDiv();
			}else if(direction==="back"){
				var STEP=1;
				if(selectedBubble.zIndex-STEP<=MIN_Z_INDEX_BUBBLES)	return;
				selectedBubble.zIndex-=STEP;
				selectedBubble.placeDiv();
			}

			
		}
		
		
		/*private*/,isOnGrid:function(aotraScreen,x,y,size){
			
			// TRY (keep code)
//			var zoom=aotraScreen.getZoomFactor();
//			for(var i=0;i<this.gridDots.length;i++){
//				var dot=this.gridDots[i];
//				if(isInZone({x:x,y:y},dot,(size*2)/zoom)){
//					return dot;
//				}
//			}

			// TO COMPENSATE A STRANGE BUG :
//			x-=size*2;
//			y-=size*2;
//			size*=2;
			
			var spacing=this.gridSpacing;
			
			var modX=Math.abs(x%spacing);
			var modY=Math.abs(y%spacing);

			if(modX<=size && modY<=size){
				return {x:Math.makeMultipleOf(x,spacing,"floor")
					   ,y:Math.makeMultipleOf(y,spacing,"floor")};
			}else if(spacing-modX<=size && spacing-modY<=size){
				return {x:Math.makeMultipleOf(x,spacing,"ceil")
				   	   ,y:Math.makeMultipleOf(y,spacing,"ceil")};
			}
					   
			return null;
				
		}
		
		
		/*private*/,setBubbleToNewPosition:function(aotraScreen,selectedBubble,destinationX,destinationY){
			
			var self=this;
			
			// To allow inter-planes moving of a bubble :
			var plane=aotraScreen.getActivePlane();
			
			// «Magnetic grid» implementation :
			// (Grid is active only if bubbles are not fixed)
			if(!selectedBubble.isFixed()){
				
				// Parallax background of bubble must be updated :
				plane.parallaxBackgroundsProvider.removeBubbleParallaxBackground(selectedBubble);
				
				// Positioning grid part :
				var placedX=null;
				var placedY=null;
				var zoom=aotraScreen.getZoomFactor();
				
				
				var dot=this.isOnGrid(aotraScreen,destinationX,destinationY
						,(DOT_SIZE)*zoom);
				if(dot){
					placedX=dot.x;
					placedY=dot.y;
				}

				// Magnetic grid help :
				if(placedX && placedY){
					
					selectedBubble.setLocation(placedX!==null?placedX:destinationX
											  ,placedY!==null?placedY:destinationY);
					
					// Visual confirmation :
					self.brieflyFlashPositionVisualConfirm(aotraScreen,selectedBubble);
				}else{
					
					// Align between bubbles rulers help :
					
					placedX=plane.getNearestCoordinateXWithBubbles(selectedBubble,destinationX
										,MAGNETIC_THRESHOLD*zoom);
					placedY=plane.getNearestCoordinateYWithBubbles(selectedBubble,destinationY
										,MAGNETIC_THRESHOLD*zoom);

					selectedBubble.setLocation(placedX!==null?placedX:destinationX
			  				  ,placedY!==null?placedY:destinationY);

					// Visual confirmation :
					if(placedX || placedY){
						var xy=selectedBubble.getDisplayXY(CENTER);
						self.brieflyFlashRulers(aotraScreen
									,!placedX?null:xy.x
									,!placedY?null:xy.y);
					}

				}
				
				
				// Parallax background of bubble must be updated :
				plane.parallaxBackgroundsProvider.addBubbleParallaxBackground(selectedBubble);
				
				
			}else{
				selectedBubble.setLocation(destinationX,destinationY);
			}
			
			this.cancelFloatingAction();
			
			this.onModelChangeEvents.executeAll();
			
		}
		

		/*public*/,drawPositioningGrid:function(aotraScreen){
			var ctx=aotraScreen.ctx;

			if(!this.isGridDisplayed)	return;
			
			// (Grid is active only if bubbles are not fixed)
			if(this.getSelectedBubble() && this.getSelectedBubble().isFixed())	return;
			
			var width=aotraScreen.getWidth();
			var height=aotraScreen.getHeight();

			var viewX=aotraScreen.x;
			var viewY=aotraScreen.y;

			var zoom=aotraScreen.getZoomFactor();

			var dataRectXMin=(viewX-(width/2) )/zoom;
			var dataRectYMin=(viewY-(height/2))/zoom;
			var dataRectXMax=(viewX+(width/2 ))/zoom;
			var dataRectYMax=(viewY+(height/2))/zoom;
			
			// We limit displayed dots number :
			this.gridSpacing=DEFAULT_GRID_SPACING/zoom;
			
			var xMin=Math.makeMultipleOf(dataRectXMin,this.gridSpacing,"floor");
			var yMin=Math.makeMultipleOf(dataRectYMin,this.gridSpacing,"floor");
			var xMax=Math.makeMultipleOf(dataRectXMax,this.gridSpacing,"ceil");
			var yMax=Math.makeMultipleOf(dataRectYMax,this.gridSpacing,"ceil");
			
			
//			// We reset the dots array :
//			this.gridDots=new Array();

			for(var x= 		xMin; x<=xMax;	x+=this.gridSpacing){
				for(var y=	yMin; y<=yMax;	y+=this.gridSpacing){

					ctx.save();  
					ctx.beginPath();
					
					// From data coordinates to display coordinates :
					var displayX=Math.round( (x*zoom)-viewX+width/2 );
					var displayY=Math.round( (y*zoom)-viewY+height/2);
					
					ctx.arc(displayX,displayY,DOT_SIZE, 0, 2 * Math.PI);
					
					ctx.fillStyle=GRID_COLOR;
					ctx.fill();
					ctx.closePath();
					ctx.restore();
					
					// KEEP CODE FOR DOC : Convert from display coordinates to data coordinates :
					//					var dot={x:	(x+viewX-width /2)/zoom
					//							,y: (y+viewY-height/2)/zoom };
//					var dot={x:x,y:y};
//					this.gridDots.push(dot);
				}
			}
		}
		
		
		/*private*/,brieflyFlashPositionVisualConfirm:function(aotraScreen,bubble){
		
			if(!bubble)	return;

			
			var zoom=aotraScreen.getZoomFactor();
			var ctx=aotraScreen.ctx;

			var duration=RULERS_PERSISTENCE_DELAY_SECONDS;

			var xy=bubble.getDisplayXY(CENTER);
			
			var step=((REFRESHING_RATE)/duration);
			var alpha=1;
			var size=bubble.getDisplayW();
			
			var SPEED=0.01;
			
			var pe2=new PeriodicalExecuter(function() {
				
				if(alpha<0){
					pe1.stop();
					pe2.stop();
					return;
				}
				
				alpha-=step;
				size+=step*SPEED;
				
				aotraScreen.draw();

				ctx.save();  
				ctx.beginPath();
				
				ctx.arc(xy.x, xy.y, size/zoom, 0, 2 * Math.PI);
				
				ctx.lineWidth = DOT_SIZE;
				ctx.strokeStyle=GRID_COLOR;
				ctx.stroke();
				
				ctx.closePath();
				ctx.restore();

				
				
			},REFRESHING_RATE);
			
			var pe1=new PeriodicalExecuter(function() {
				pe1.stop();
				pe2.stop();
				aotraScreen.draw();
			},duration);
			
		}
		
		/*private*/,brieflyFlashRulers:function(aotraScreen,x,y){

			if(!x && !y)	return;
			
			var duration=RULERS_PERSISTENCE_DELAY_SECONDS;

			var MARGIN=10;
			
			var width=aotraScreen.getWidth();
			var height=aotraScreen.getHeight();
			
			var ctx=aotraScreen.ctx;
			
			var alpha=1;
			var step=((REFRESHING_RATE)/duration);

			var pe2=new PeriodicalExecuter(function() {
				
				if(alpha<0){
					pe1.stop();
					pe2.stop();
					return;
				}
				
				alpha-=step;

				aotraScreen.draw();

				
				var displayX1=0,displayY1=0;
				var displayX2=0,displayY2=0;

				if(x){

					displayX1=x;displayY1=MARGIN;
					displayX2=x;displayY2=height-MARGIN;


					ctx.save();
					ctx.beginPath();
					
					ctx.strokeStyle=getBeamLikeGradient(ctx,displayX1,displayY1,displayX2,displayY2,RULERS_SIZE,RULERS_COLOR,alpha);
					ctx.lineWidth=RULERS_SIZE;

					ctx.moveTo(displayX1,displayY1);
					ctx.lineTo(displayX2,displayY2);
					
					ctx.stroke();
					ctx.closePath();
					ctx.restore();

				}
				if(y){

					ctx.save();
					ctx.beginPath();
					
					displayX1=MARGIN;displayY1=y;
					displayX2=width-MARGIN;displayY2=y;
					
					ctx.strokeStyle=getBeamLikeGradient(ctx,displayX1,displayY1,displayX2,displayY2,RULERS_SIZE,RULERS_COLOR,alpha);
					ctx.lineWidth=RULERS_SIZE;

					ctx.moveTo(displayX1,displayY1);
					ctx.lineTo(displayX2,displayY2);
					
					ctx.stroke();
					
					ctx.closePath();
					ctx.restore();
				}
				
			},REFRESHING_RATE);
			
			var pe1=new PeriodicalExecuter(function() {
				pe1.stop();
				pe2.stop();
				aotraScreen.draw();
			},duration);
			
		}
		
		
		// ====================================== DELETE BUBBLE ======================================
		,deleteBubble:function(){
			var aotraScreen=this.aotraScreen;
			
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble){
				alert(PLEASE_SELECT_BUBBLE_MESSAGE);
				return;
			}

			// Confirmation :
			if(!confirm("Are you sure you want to delete bubble «"+selectedBubble.getName()+"» ? All its content and links to and from it will be lost."))
				return;
			
			// Bubble deletion from aotraScreen :
			aotraScreen.deleteBubble(selectedBubble);
			this.setSelectedBubble(null);
			
			
			// DEPENDENCY : SCREEN ADVANCED NAVIGATION PROVIDER
			aotraScreen.goBackBubbleIndex();

			aotraScreen.drawAndPlace();

			// Actually, that's not a real «pendable» action:
//			this.pendingActionName=PENDABLE_ACTIONS["delete"]["name"];
			
			
			this.onModelChangeEvents.executeAll();

		}
		
	
		
		// ====================================== MAKE SUB-BUBBLE OF ======================================
		,makeSubBubbleOf:function(){

			var self=this;
			if(self.isOperationPending())	return;
			
			var aotraScreen=this.aotraScreen;
			
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble){
				alert(PLEASE_SELECT_BUBBLE_MESSAGE);
				return;
			}
			
			var editionProvider=this;
			this.oldSetSelectedBubbleFunction = editionProvider.setSelectedBubble;
			
			// FLOATING PENDABLE_ACTIONS CANCEL BUTTON :
			this.setFloatingActionsCancelButtonVisibleForAction(true,PENDABLE_ACTIONS["makeSubBubbleOf"]["name"]);
			
			editionProvider.setSelectedBubble=function(newSelectedBubble){
				
				var selectedBubble=editionProvider.getSelectedBubble();
			
				// We don't allow bubble is sub-bubble of itself (=source bubble identical to destination bubble)
				if( !(selectedBubble==newSelectedBubble || newSelectedBubble==null) ){

					// We don't allow circularity :
					if(!selectedBubble.willBeAncestorOf(newSelectedBubble)){
					
						aotraScreen.notifyBubbleSelectionHasChanged(editionProvider.selectedBubble,newSelectedBubble);
						editionProvider.selectedBubble=newSelectedBubble;
						// Moving with drag and drop abilities initialization :
						self.initDragAndDropMoveOnBubble(newSelectedBubble);
						aotraScreen.draw();
						
						var parentBubble=selectedBubble.getParentBubble();
						if(parentBubble==null || newSelectedBubble.getName()!=parentBubble.getName() ){
//							if(confirm("Are you sure you want to make this bubble a sub-bubble ? You will loose position information for this sub-bubble.")){
							newSelectedBubble.addBubble(selectedBubble,true);
//							}
						}else{
							alert("Bubble already is a sub-bubble of this bubble.");
						}
					
					}else{
						alert("A circularity has been found if we set this parent bubble «"+newSelectedBubble.getName()+"» to this bubble «"+selectedBubble.getName()+"».");
					}
				}
				
				self.cancelFloatingAction();
				
				self.onModelChangeEvents.executeAll();
				
				// NO :
//				// We restore the selection to the former bubble :
//				self.setSelectedBubble(selectedBubble);
				
			};
			
			this.pendingActionName=PENDABLE_ACTIONS["makeSubBubbleOf"]["name"];

		}
		// ====================================== UNMAKE SUB-BUBBLE======================================
		/*private*/,unmakeSubBubble:function(){

			var self=this;
			if(self.isOperationPending())	return;
			
			var aotraScreen=this.aotraScreen;
			
			var selectedBubble=this.getSelectedBubble();
			if(!selectedBubble){
				alert(PLEASE_SELECT_BUBBLE_MESSAGE);
				return;
			}
			
			var editionProvider=this;

//			// Confirmation :
//			if(!confirm("Are you sure you want to remove parent from this sub-bubble ?"))	return;
			
			// Bubble parent forgetting from aotraScreen :
			selectedBubble.detachFromParentBubble(true);
			
			// Actually, that's not a real «pendable» action:
//			this.pendingActionName=PENDABLE_ACTIONS["delete"]["name"];
			
			this.onModelChangeEvents.executeAll();
			aotraScreen.drawAndPlace();

		}
		
		// ====================================== SELECT BUBBLE ======================================
		
		,setSelectedBubble:function(bubble){
			
			var aotraScreen=this.aotraScreen;

			// Case if we force selection to null :
			if(!bubble){
				aotraScreen.notifyBubbleSelectionHasChanged(this.selectedBubble,null);
				this.selectedBubble=null;
				aotraScreen.draw();
				return;
			}
			
			// We can change the bubble selection only if no other operation is pending...
			//...And specifically edit bubble is not pending too :
			
			if(!
				// To allow selection of a bubble, this condition must be matched :
				( (!this.isBubbleCurrentlyBeingEdited() && !this.isOperationPending())
					|| this.pendingActionName===PENDABLE_ACTIONS["addLink"]["name"]
				)
			){
				return;
			}
			
//			if(this.isOperationPending() && !this.isBubbleCurrentlyBeingEdited() )	return;
			if(bubble){
				this.onBubbleSelect(bubble);
			}
			
			if(		(this.selectedBubble && bubble && this.selectedBubble.getName()===bubble.getName())
				||  (!this.selectedBubble && !bubble)
				) return;
			
			aotraScreen.notifyBubbleSelectionHasChanged(this.selectedBubble,bubble);
			this.selectedBubble=bubble;
			
			
			aotraScreen.draw();
		}
		

		/*private*/,onBubbleSelect:function(bubble){

			var aotraScreen=this.aotraScreen;

			var divEditToolbar=setElementVisible("divBlockAllEditActions",true);
			
	//		// Only if we change of selected bubble :
	//		if(this.getSelectedBubble()!==bubble){
				
	//			//		- Dialog dragging enabling :
	//			if(!divEditToolbar.inited){
	//				jQuery(divEditToolbar).drags();
	//				divEditToolbar.inited=true;
	//			}
				
	//		var xEditionPanel=(aotraScreen.getWidth()/2 -bubble.getDisplayDiameter()/2- 80);
	//		var yEditionPanel=(aotraScreen.getHeight()/2-bubble.getDisplayDiameter()/4);
			var xEditionPanel=(aotraScreen.getWidth()/2 -bubble.getDisplayW()/2- 80);
			var yEditionPanel=(aotraScreen.getHeight()/2-bubble.getDisplayH()/4);
	
			
			xEditionPanel=Math.max(40,xEditionPanel);
			yEditionPanel=Math.max(40,yEditionPanel);
			
			// CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
			divEditToolbar.setStyle("left:"+xEditionPanel+"px");
			divEditToolbar.setStyle("top:"+yEditionPanel+"px");
	//		}
			
			// Buttons disabilities :
			jQuery(".unmakeSubBubbleBtn").prop("disabled",(nothing(bubble.getParentBubble())));
			
			// Moving with drag and drop abilities initialization :
			var disableMoveButton=!this.isEnableMoveButton();
			if(disableMoveButton)	jQuery(".moveBubbleBtn").prop("disabled", disableMoveButton);
			if(disableMoveButton)	this.initDragAndDropMoveOnBubble(bubble);

		}
		
		// ====================================== UTILITY METHODS ======================================

		
		/*private*/,isEnableMoveButton:function(){
			
			// Move button must be unavailable if drag and drop is forbidden 
			// or if we are in mobile device
			
			return (FORBID_DRAG_AND_DROP_MOVE_BUBBLE || IS_DEVICE_MOBILE_CALCULATED);
		}
			
		/*private*/,isOperationPending:function(){
			return	this.pendingActionName!==PENDABLE_ACTIONS["none"]["name"];
		}
		
		// This method only concerns treatments that need to be executed on content change :
		/*public*/,commitContentToSelectedBubble:function(/*OPTIONAL*/contentParam){
			var content=contentParam;
			
			if(!this.isBubbleCurrentlyBeingEdited())	return;
			
			var textboxComponent=this.getTextboxComponent();
			if(!textboxComponent)	return;
			
			//TRACE
			log("INFO : Bubble content modifications have been committed.");
			
			if(nothing(contentParam))	content=textboxComponent.innerHTML;
			else										  textboxComponent.innerHTML=content;
			
			// CONTENTS FILTERING AND NORMALIZING :
			// We don't allow single quotes nor double quotes for apostrophes, only APOSTROPHE outside tags !
			// (At this time and funnily enough, Android Firefox doesn't think that accentuated French characters are of \w category...)
			var r=/[\wàâäéèêëìîïòôöùûüç^>]+['\"][\wàâäéèêëìîïòôöùûüç^<]+/gim;
			if(r.test(content)){
				//TRACE
				if(this.isThoughtful)	alert("To preserve content’s integrity, we unfortunately don’t allow single quote as apostrophe in your text. Please use «’» instead.");
				var matches=content.match(r);
				for(var i=0;i<matches.length;i++){
					var m=matches[i];
					// TODO : FIXME : IS FUNCTION .replace(...) WORKING ON ALL LINES OF STRING WITHOUT REGEXPS ?!?!
					content=content.replace(m,m.replace("'",APOSTROPHE).replace('"',APOSTROPHE));
				}
			}
			
			content=toOneSimplifiedLine(content,"compactHTML");
			// We don't allow relative mesures : always percentages or ems !  
			if(new RegExp("<style>.*font.*:.*(px).*</style>","gim").test(content)){
				// TRACE
				if(this.isThoughtful)
					alert("To preserve the aspect of elements against the zoom, we do not allow pixel-fixed sizes for CSS font size in bubbles. Please use percentages or em unities instead.");
				return;
			}
			
			
			var selectedBubble=this.getSelectedBubble();
			
			selectedBubble.setContent(content);
			
			// DEPENDENCY : MAP PROVIDER 
			// Eventual map treatments from bubble content:
			var plane=this.aotraScreen.getPlaneForBubble(selectedBubble);
			if(plane && plane.mapProvider){
				
				plane.mapProvider.refreshMapMarkersAfterBubbleContent(selectedBubble);

				// This costly operation is only performed here :
				plane.mapProvider.removeAllOrphanMarkers();

			}
			
			this.onModelChangeEvents.executeAll();
			
		}
		
		,close:function(){
			
			// Nicedit :
      if(this.textboxEditContentRich){
       	this.textboxEditContentRich.removeInstance("textboxEditContent");
      }
			
			
      // Close the content edition window :
      setElementVisible("divEditContent",false);
	    this.isBubbleEdited=false;
			
			this.pendingActionName=PENDABLE_ACTIONS["none"]["name"];
		}
		
		/*private*/,getAotraScreenClickedDataXY:function(isFixedParam){
			var aotraScreen=this.aotraScreen;
			var result= new Array();

			var zoom=aotraScreen.getZoomFactor();
			var width=aotraScreen.getWidth();
			var height=aotraScreen.getHeight();

			var mouseX=aotraScreen.mouseX;
			var mouseY=aotraScreen.mouseY;
			
			if(!isFixedParam){
				result["x"]=(mouseX+aotraScreen.x-width/2) /zoom;
				result["y"]=(mouseY+aotraScreen.y-height/2)/zoom;
			}else{
				result["x"]=mouseX;
				result["y"]=mouseY;
			}
			
			return result;
		}

		// FLOATING PENDABLE_ACTIONS CANCEL BUTTON ACTION :
		,cancelFloatingAction:function(){
			var aotraScreen=this.aotraScreen;
			
			// We restore edition provider selection function
			if(this.oldSetSelectedBubbleFunction){
				this.setSelectedBubble=this.oldSetSelectedBubbleFunction;
				this.oldSetSelectedBubbleFunction=null;
			}
			
			// We restore aotraScreen previously disabled click capacities
			if(this.oldAotraScreenOnClickFunction){
				aotraScreen.onClickAotraScreen=this.oldAotraScreenOnClickFunction;
				this.oldAotraScreenOnClickFunction=null;
			}
			
			this.isGridDisplayed=false;

			
			aotraScreen.drawAndPlace();
			this.setFloatingActionsCancelButtonVisibleForAction(false);
			this.pendingActionName=PENDABLE_ACTIONS["none"]["name"];
			
			this.setEditWindowTransparent(false);
			
		}
		
		/*private*/,setFloatingActionsCancelButtonVisibleForAction:function(visible,/*OPTIONAL*/pendingActionName){
			if(visible){
				setElementVisible("buttonCancelCurrentOperation",true);
				setElementVisible("divBlockAllEditActions",false);
				setElementVisible("divBlockAllEditActionsStatic",false);
			}else{
				setElementVisible("buttonCancelCurrentOperation",false);
				setElementVisible("divBlockAllEditActions",true);
				setElementVisible("divBlockAllEditActionsStatic",true);
			}
			
			if(visible)	this.pendingActionName=pendingActionName;
			else 	this.pendingActionName=PENDABLE_ACTIONS["none"]["name"];
		}
		
		// This method is only to temporary hide the content edition window, not close it:
		/*private*/,setEditWindowTransparent:function(transparent){
			if(!transparent && !this.isBubbleCurrentlyBeingEdited())	return;
			setElementVisible("divEditContent",!transparent);
		}
		
		,isBubbleCurrentlyBeingEdited:function(){
			return this.isBubbleEdited;
		}
		
		,getSelectedBubble:function(){
			return this.selectedBubble;
		}
		
		/*private*/,getTextboxComponent:function(){
			// NicEdit :
			var niceEditor=this.textboxEditContentRich.nicInstances[0];
			if(!niceEditor)	return null;
			return niceEditor.elm;
		}
		
		
		,changeTextboxComponentColor:function(colorStr){

			// Nicedit :
			var div=this.getTextboxComponent();

      var cssText="background:"
      		+( !contains(colorStr,":") || empty(colorStr.split(":")) ?
      				colorStr:colorStr.split(":")[0]	)+";";
      
      cssText+="height:"+(TEXTBOX_EDIT_CONTENT_ROWS)+"em;overflow:auto;";

      div.style.cssText=cssText;

		}
		
		
	});
	
	
	
	// STATIC CODE :

	// Helper static methods
	// -UI Elements adding :
	//*************************************
	
	/*public static*/initSelf.AotraScreenEditionProvider.getGeneratedHTMLButton=function(styleClass,methodCallString,label){
		var mainDivInnerHTML="";
		
		mainDivInnerHTML+='<button class="icon '+styleClass+'" '
			+' onclick="javascript:getAotraScreen().'+methodCallString+';"'
			+' title="'+label+'"></button>';//&#10144;

		return mainDivInnerHTML;
	};

	/*public static*/initSelf.AotraScreenEditionProvider.addUIElementInContentFromButtonAction=function(styleClass,methodCallString,label,textboxComponent){
		
		initSelf.AotraScreenEditionProvider.addUIElementInContent("<span style='text-decoration:underline;cursor:pointer;' class='"+styleClass+"' "
				+" onclick='javascript:getAotraScreen()."+methodCallString+";'>"
				,label
				,textboxComponent);
	};

	
	/*private static*/initSelf.AotraScreenEditionProvider.addUIElementInContent=function(htmlToInsert,defaultLabel,textboxComponent){
		
		var editionProvider=getAotraScreen().editionProvider;
		
		// Edition provider isn't necessarily present at this point :
		if(!editionProvider)	return;

		var editedBubble=editionProvider.getSelectedBubble();
		
		var selectedText=getSelectedText(textboxComponent).trim();

		editionProvider.commitContentToSelectedBubble();
		var content=toOneSimplifiedLine(editedBubble.getContent(),"compactHTML");


		// Case some text selected :
		if(!nothing(selectedText)){
			htmlToInsert+=selectedText;
			htmlToInsert+="</span>";
			content=content.replace(selectedText,htmlToInsert);
		}else{
			// Case no text selected : we append at the end...
			htmlToInsert+=defaultLabel;
			htmlToInsert+="</span>";
			content=content+htmlToInsert;
		}
		
		editedBubble.setContent(content);
		textboxComponent.setContent(content);
	};
	//*************************************
	
	
	
	// Hooks
	/*static*/{
		// Generic hooks : 
		initSelf.AotraScreenEditionProvider.hooks=new Object();
		initSelf.AotraScreenEditionProvider.hooks[INIT]=new Array();// A simple hook
		initSelf.AotraScreenEditionProvider.hooks[DO_ON_OPEN_EDIT]=new Array();// A simple hook
		initSelf.AotraScreenEditionProvider.hooks[DO_ON_SAVE_EDIT]=new Array();// A simple hook
		initSelf.AotraScreenEditionProvider.hooks[EDIT_WINDOW_BUTTONS]=new Object();
		initSelf.AotraScreenEditionProvider.hooks[EDIT_WINDOW]=new Object();
		initSelf.AotraScreenEditionProvider.hooks[SNIPPETS]=new Object();

		
	};

	

	return initSelf;
}
}

// GLOBAL CONSTANTS
	const SAVING_PROVIDER_HOOK_NAME="mainSave";
	const PASSWORD_FIELD_ID="textboxEditWritePassword";

if(typeof initScreenSavingProviderClass ==="undefined"){
function initScreenSavingProviderClass() {
	var initSelf = this;

	// CONSTANTS
//	var SAVING_DIV_BACKGROUND="#F8FFE8;background: -moz-linear-gradient(-45deg,  #f8ffe8 0%, #e3f5ab 33%, #b7df2d 100%);background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#f8ffe8), color-stop(33%,#e3f5ab), color-stop(100%,#b7df2d));background: -webkit-linear-gradient(-45deg,  #f8ffe8 0%,#e3f5ab 33%,#b7df2d 100%);background: -o-linear-gradient(-45deg,  #f8ffe8 0%,#e3f5ab 33%,#b7df2d 100%);background: -ms-linear-gradient(-45deg,  #f8ffe8 0%,#e3f5ab 33%,#b7df2d 100%);background: linear-gradient(135deg,  #f8ffe8 0%,#e3f5ab 33%,#b7df2d 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8ffe8', endColorstr='#b7df2d',GradientType=1 );";
//	var SAVING_DIV_BORDER="#A2DA21 5px dotted";
//	var SAVING_DIV_ADDITIONAL_STYLE="";
	
	var TEXTBOX_EDIT_CONTENT_WIDTH=400;
	var TEXTBOX_EDIT_CONTENT_HEIGHT=300;
	
//	// Preview popus setup :
//	var POPUP_TITLE="Code preview window";
//	var POPUP_SETTINGS="height=600,width=600,resizable=1,scrollbars=1";
	
	initSelf.AotraScreenSavingProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen,serversURLs,/*OPTIONAL*/encryptingProvider) {

			// Attributes
			this.aotraScreen = aotraScreen;

			// Processed attributes
			this.serversURLsSet=this.calculateServersURLsSet(serversURLs);
			this.backEndFilePath=config.aotraScreen.backEndFilePath?config.aotraScreen.backEndFilePath:DEFAULT_BACK_END_FILE_PATH;

			
			this.writePassword="";
			if(encryptingProvider){
				this.encryptingProvider=encryptingProvider;
				this.encryptingProvider.setSavingProvider(this);
			}
			
			this.initUI();
			
			
			// Hooks :
			foreach(initSelf.AotraScreenSavingProvider.hooks[INIT_AFTER_READY],(h)=>{
				h.execute(aotraScreen);
			});
			
		}
	
		// Methods
		/*private*/,initUI:function(){

			var aotraScreen=this.aotraScreen;
			
			var width=TEXTBOX_EDIT_CONTENT_WIDTH;
			var height=TEXTBOX_EDIT_CONTENT_HEIGHT;
			
			var xCenter=(this.aotraScreen.getWidth()/2-width/2);
			var yCenter=(this.aotraScreen.getHeight()/2-height/2);
			
			var mainDiv=aotraScreen.mainDiv;

			var mainDivInnerHTML = mainDiv.innerHTML;
			
			mainDivInnerHTML+=this.getToolsBarHTMLButtons();
			mainDivInnerHTML+=this.getShowGeneratedHTMLInputs(xCenter,yCenter);
			
			mainDiv.innerHTML=mainDivInnerHTML;
			
		}

		/*private*/,getToolsBarHTMLButtons:function(){

			var POSITION_X=52;
			var POSITION_Y=88;

			var mainDivInnerHTML="";		

			mainDivInnerHTML+="<div id='divSaveActions' class='buttonsHolder' "
				+" style='max-width:100px; "
				+" left:"+(POSITION_X)+"px; top:"+(POSITION_Y)+"px ;z-index:"+MAX_Z_INDEX+";"
				+" pointer-events:all;"
				+" ' >";

				mainDivInnerHTML+="<button class='icon saveBtn' onclick='javascript:getAotraScreen().savingProvider.directSaveToServer();' title='"+i18n({"fr":"Sauvegarder directement","en":"Direct save"})+"'></button>";//&#10004;
				
				mainDivInnerHTML+="<button class='icon dashed saveOptionsBtn buttonLight' onclick='javascript:jQuery(\"#divAdvancedSaveOptions\").slideToggle(500);' title='"+i18n({"fr":"Plus d’options de sauvegarde...","en":"More save options..."})+"'></button>";//&#10050;

				// (width:...px to force display on two columns only:)
				mainDivInnerHTML+="<div id='divAdvancedSaveOptions' style='display:none;width:"+(GLOBAL_ICONS_SIZE*2)+"px'>";
				
					mainDivInnerHTML+="<button class='icon distantOptionsBtn' onclick='javascript:getAotraScreen().savingProvider.saveToServer();' title='"+i18n({"fr":"Configurer la sauvegarde distante","en":"Server save"})+"'></button>";//&#9993;
					mainDivInnerHTML+="<button class='icon editSourceBtn' onclick='javascript:getAotraScreen().savingProvider.editXML();' title='"+i18n({"fr":"Éditer le code source XML","en":"Edit XML"})+"'></button>";//&#9991;
		
					// ONLY IF LOCAL STORAGE IS SUPPORTED :
					if(isHTML5StorageSupported("local"))
						mainDivInnerHTML+="<button class='icon copyModelBtn' onclick='javascript:getAotraScreen().savingProvider.copyModel();' title='"+i18n({"fr":"Copier le modèle","en":"Copy model"})+"'></button>";//&#9676;

					mainDivInnerHTML+="<button class='icon viewTextBtn' onclick='javascript:getAotraScreen().savingProvider.showText();' title='"+i18n({"fr":"Montrer tout le texte","en":"Show text"})+"'></button>";//&#10002;

					// ONLY IF LOCAL STORAGE IS SUPPORTED :
					if(isHTML5StorageSupported("local"))
						mainDivInnerHTML+="<button class='icon pasteModelBtn' onclick='javascript:getAotraScreen().savingProvider.pasteModel();' title='"+i18n({"fr":"Coller le modèle","en":"Paste model"})+"'></button>";//&#9675;

					mainDivInnerHTML+="<button onclick='javascript:getAotraScreen().savingProvider.showGeneratedHTML();' "
					 		+"style='left:"+(POSITION_X)+"px; top:"+(POSITION_Y+170)+"px;'>Show HTML Code</button>";
					
				mainDivInnerHTML+="<div>";

					
			mainDivInnerHTML+="</div>";

			return mainDivInnerHTML;
		}
		
		,getServersURLsSet:function(){
			return this.serversURLsSet;
		}
		
		// Export to server inputs :
		,getShowGeneratedHTMLInputs:function(xCenter,yCenter){

			var mainDivInnerHTML="";
			
			var xUI=132;
			var yUI=0;
			
			mainDivInnerHTML+="<div id='divSaveToServer' class='standardBox buttonsHolder' " 
				+"style=' display:none;  " 
				+" left:"+(xUI)+"px;top:"+(yUI)+"px;z-index:"+MAX_Z_INDEX+"; "
//				+" border:"+SAVING_DIV_BORDER+"; "
//				+" "+SAVING_DIV_ADDITIONAL_STYLE+"; "
//				+" background:"+SAVING_DIV_BACKGROUND+"; "
				+" ' >"; 

			// Data saving to server :
			// Connection informations fields :
			mainDivInnerHTML+="<input type='password' id='textboxEditWritePassword' " 
				+"style='border:solid 2px gray;height:20px;width:200px;' placeHolder='Mot de passe pour écrire les données...' />";
			
			// Encrypting :
			if(this.encryptingProvider){
				mainDivInnerHTML+=this.encryptingProvider.getGeneratedHTMLInputs();
			}

			// Servers choice :
			if(!empty(this.getServersURLsSet())){
				mainDivInnerHTML+=i18n({"fr":"Adresses URL des serveurs","en":"Remote servers URLs"})+" : <br />";
				var isFirst=true;
				for(var i=0;i<this.getServersURLsSet().length;i++){
					var u=this.getServersURLsSet()[i];
					// TODO : Add saving to several servers simultaneously...
					mainDivInnerHTML+="<input type='radio' name='serverURLWhereToSave' "
						// We choose the first by default :
						+" value='"+u+"' "+(isFirst?"checked":"")+" /> "+u+"<br />";
					isFirst=false;
				}
			}
			
			
			// File upload to server :
			if(this.aotraScreen.uploadProviders && this.aotraScreen.uploadProviders[SAVING_PROVIDER_HOOK_NAME])
				mainDivInnerHTML+=this.aotraScreen.uploadProviders[SAVING_PROVIDER_HOOK_NAME].getUploadGeneratedHTMLInputs();
			
			
			mainDivInnerHTML+="<br />";
			
			// Save button
			mainDivInnerHTML+="<button onclick='javascript:getAotraScreen().savingProvider.onClickButtonSaveToServer();' />";
			mainDivInnerHTML+=i18n({"fr":"Sauvegarder les données sur le serveur","en":"Save data to server"})+"</button>";
			
			// Cancel button
			mainDivInnerHTML+="<button class='icon closeBtn' style='float:right'" 
				+"onclick='javascript:getAotraScreen().savingProvider.close();' title='"+i18n({"fr":"Annuler","en":"Cancel"})+"'></button>";//&#10006;
			
			mainDivInnerHTML+="</div>";

			return mainDivInnerHTML;
		}

		
		
		// Data saving :
		
		,directSaveToServer:function(){
			var self=this;
			
			var textboxEditWritePassword=document.getElementById(PASSWORD_FIELD_ID);

			var typedPassword=textboxEditWritePassword.value;
			if(nothing(typedPassword)){
				promptWindow(
				 	 i18n({"fr":"Veuillez entrer le mot de passe d'écriture","en":"Please enter write password"})
					,"password",""
				 ,function(p){
					if(!nothing(p)){
						textboxEditWritePassword.value=p;
						self.onClickButtonSaveToServer();
					}
				});
			}else{
				this.onClickButtonSaveToServer();
			}
		}
		
		,saveToServer:function(){
			setElementVisible("divSaveToServer",true);
			
			var textboxEditWritePassword=document.getElementById(PASSWORD_FIELD_ID);
			textboxEditWritePassword.value=this.writePassword;
			textboxEditWritePassword.focus();
		}

		/*private*/,onClickButtonSaveToServer:function(){

			
			// We keep a persisted copy of the model :
			if(isHTML5StorageSupported("local"))
				this.copyModel(true);
			
			var chosenServerURL=this.getChosenServerURL();
			
			// If user does not use a secure connection, then we warn the user :
			if(!isURLHttps(chosenServerURL)){
				if(!confirm(i18n({
				 "fr":"Vos informations de connexion vont transiter sur un réseau a priori non sécurisé (http au lieu de https), n'importe qui peut les intercepter, êtes-vous sûr de vouloir continuer ?"
				,"en":"Your login information travels throught a supposedly non-secure connection, are you sure you want to continue ?"
				})))	return;
			}
			
			
			if(!this.encryptingProvider){
				this.validateSaveToServer(chosenServerURL);
				return;
			}
			
			if(document.getElementById("checkboxIsSavingEncrypted")
			&& document.getElementById("checkboxIsSavingEncrypted").checked)
				this.encryptingProvider.validateEncryptingSaveToServer(chosenServerURL);
			else
				this.validateSaveToServer(chosenServerURL);

			
			
		}
		
		/*private*/,getChosenServerURL:function(){
			var slct=jQuery("input[type='radio'][name='serverURLWhereToSave']:checked");
			if(empty(slct.get()))	return null;
			return slct.val();
		}
		
		/*private*/,validateSaveToServer:function(/*OPTIONAL*/chosenServerURL){
			var self=this;

			info = this.promptGlobalConnectionInformations();
			if(info==null)	return;

			
			window.waitIndicator.start();

			
			var parameters=
				"actionType=WRITE" 
				+"&pageName=" + info.pageName
				+"&writePasswordHash=" + info.writePasswordHash 
				+"&data=" + info.transmittedData;
			
			
			// To save to another server :
			var u=info.endpointFileURL;
			if(!empty(chosenServerURL)){
				u=chosenServerURL+this.backEndFilePath;
				
				if(chosenServerURL!==getCalculatedServerURLOnly()){
					parameters+="&otherServerURL="+u;
				}
			}
			
			jQuery.ajax({
		          async: "true",
		          type: "POST",
		          url: info.endpointFileURL,
		          // Data as parameters/values pairs
		          // TODO : Compress sent data (the whole HTML code) :
		          data:parameters,
		          error:function(errorDataResponse) {
		        	  //If error
		        	  alert("ERROR RESPONSE (An Ajax error occured) : (see JS console for details)");
		        	  // TRACE
		        	  log("ERROR : "+errorDataResponse);
		        	  
		        	  window.waitIndicator.end();

		          },
		          success: function(dataResponse) {
		        	  
		        	  // If everything went OK
		        	  // TRACE
		        	  log("RESPONSE from «"+u+"» : "+dataResponse);

		        	  alert("RESPONSE from «"+u+"» : "+dataResponse);
		        	  
		        	  if(dataResponse.substring(0,2)==="00"){
		        		  self.close();
		        	  }else if(dataResponse.substring(0,4)==="0201"){
		        		  textboxEditWritePassword.value="";
		        	  }
		        	  
		        	  window.waitIndicator.end();

		          }
		    });

		}

		
		/*public*/,promptGlobalConnectionInformations:function(){
		
			var textboxEditWritePassword=document.getElementById(PASSWORD_FIELD_ID);

			// Focus on password field :
			textboxEditWritePassword.focus();

			this.writePassword=textboxEditWritePassword.value;
	
			var infoLocal = new Object();
	
			var pageName=getCalculatedPageName();
			if(!pageName)	return null;
			
			infoLocal.pageName=pageName;

			// We know that where the page is, there MUST be a aotra back-end file (typically a PHP file) :
//			infoLocal.endpointFileURL=serverURL.substring(0,serverURL.lastIndexOf("/"))+"/"+this.backEndFilePath;
			infoLocal.endpointFileURL=getCalculatedServerURLOnly()+this.backEndFilePath;
			
			infoLocal.writePasswordHash=getHashedString(this.writePassword, "SHA-256", true);
			
			var html=this.getWholeHTMLCode();
			infoLocal.transmittedData=encodeURIComponent(html);
			
			return infoLocal;
			
		}

		,editXML:function(){
			var aotraScreen=this.aotraScreen;
			var xml=new AotraScreenExporterToXML().getXMLFromAotraScreen(aotraScreen);
			
			promptWindow("Edit XML here :","textarea",xml,function(promptedValue){
				AotraScreen.updateAotraScreenFromXML(promptedValue);
			});
		}

		,showText:function(){
			var textRepresentation=this.getTextRepresentation();
			promptWindow("Text :","textarea",textRepresentation);
		}
		
		/*private*/,getTextRepresentation:function(){
			var aotraScreen=this.aotraScreen;
			var result="";
			
			var planes=aotraScreen.getPlanes();
			for(var i=0;i<planes.length;i++){
				var plane=planes[i];
				if(1<planes.length)	result+="======== Plane : «"+plane.getName()+"» ========\n";

				var bubbles=plane.getBubbles();
				for(var j=0;j<bubbles.length;j++){
					var bubble=bubbles[j];
					
					if(1<bubbles.length)	result+="---- Bubble : «"+bubble.getName()+"» ----\n";

					var content=bubble.getContent()+"";
					
					result+=removeHTML(
							content.replace(/\n{2,}/gim,"\n")
							.trim(),true)+"\n";
					
					if(1<bubbles.length)	result+="----\n";
				}
				if(1<planes.length)	result+="========\n\n";
			}
			
			return result;
		}

		
		,copyModel:function(/*OPTIONAL*/hideAlert){
			var aotraScreen=this.aotraScreen;
			var xml=new AotraScreenExporterToXML().getXMLFromAotraScreen(aotraScreen);
			
			storeString(MODEL_ID,xml);
			if(!hideAlert)
			   alert("Model has been copied.");
		} 

		,pasteModel:function(){
			
			var xml=getStringFromStorage(MODEL_ID);
			
			if(!confirm("Are you sure ? All current modifications will be lost."))	return;
			
			if(nothing(xml)){
				// TRACE
				log("WARN : Nothing to paste.");
				return;
			}
			AotraScreen.updateAotraScreenFromXML(xml);
			
			
		}

		
		/*public*/,showGeneratedHTML:function(){
			
			var POPUP_TITLE="HTML CODE POPUP.",POPUP_SETTINGS="";
			var html=this.getWholeHTMLCode();

//			if(!window.requestFileSystem && !window.webkitRequestFileSystem){
				var win=window.open("",POPUP_TITLE,POPUP_SETTINGS);
				win.document.documentElement.textContent=html;
				//TRACE
				log("WARN : window.requestFileSystem is not supported in this browser.");
				return;
//			}
			
		}
		
		/*private*/,getWholeHTMLCode:function(){
			var aotraScreen=this.aotraScreen;
			
			var newHTMLStr="";
			newHTMLStr+="<html>";
			newHTMLStr+="	<head>";
			
			jQuery("head > [data-keep='true']").each(function(){
				var e=jQuery(this).get()[0];
				var s=getAsString(e);
				if(e.tagName==="meta")	s=s.replace(">","/>");
				newHTMLStr+=s+"\n\n";
			});
			newHTMLStr+="	</head>";
			newHTMLStr+="	<body>";

			jQuery("body > [data-keep='true']").each(function(){
				let $this=jQuery(this);
				var e=$this.get()[0];
				var s="";
				if(e.id===AOTRA_SCREEN_DIV_ID){
					// We empty the super-node :
					e=e.cloneNode(false);

				}else if(e.id===MODEL_ID){
					// We empty the model node :
					e=e.cloneNode(false);

					var generatedXML="\n\n"+new AotraScreenExporterToXML().getXMLFromAotraScreen(aotraScreen)+"\n\n";
					
					e.innerHTML=generatedXML;
					
				}else{
				}
				s=getAsString(e);
				newHTMLStr+=s+"\n\n";
			});
			
			
			newHTMLStr+="	</body>";
			newHTMLStr+="</html>";

			return newHTMLStr;
		}


		// Utility Methods
		/*private*/,calculateServersURLsSet:function(serversURLs){
			var set=new Array();
			
			this.pushServerURL(set,getCalculatedServerURLOnly());
			
			if(empty(serversURLs))	return set;
			
			var URLs=serversURLs.split(",");
			for(var i=0;i<URLs.length;i++){
				var u=URLs[i];
				this.pushServerURL(set,u);
			}
			
			return set;
		}
		
		/*private*/,pushServerURL:function(set,url){
			var u=url.trim();
			if(u.lastIndexOf("/")===-1)	u=u+"/";
			if(nothing(u))	return;
			
			// TODO : FIXME : That for now, it's just too bad for https... :-/
			if(!containsIgnoreCase(u,"^http(s)?://",true))	u="http://"+u;
			if(contains(set,u))	return;
			set.push(u);
		}

		
		,close:function(){
			setElementVisible("divSaveToServer",false);
		}
		
	
	});

		
	// STATIC CODE :
	// Hooks
	/*static*/
	// Generic hooks : 
	initSelf.AotraScreenSavingProvider.hooks={};
	initSelf.AotraScreenSavingProvider.hooks[INIT_AFTER_READY]=[];


	// UNUSEFUL :
	return initSelf;
}
}
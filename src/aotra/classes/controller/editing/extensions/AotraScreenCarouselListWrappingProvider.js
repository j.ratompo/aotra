  // GLOBAL CONSTANTS
  const CAROUSEL_KEY="carousel";


// DEPENDENCY : needs AotraScreenUploadProvider
if(typeof initAotraScreenCarouselListWrappingProviderClass ==="undefined"){

	function initAotraScreenCarouselListWrappingProviderClass() {
	var initSelf = this;

	// CONSTANTS
	const CAROUSEL_CLASS_NAME="carousel";
	const AUTOSCROLL_CHANGING_RATE_MILLIS=5000;// In milliseconds
	const TRANSITION_DURATION_MILLIS=2000;// In milliseconds
	
	const DEFAULT_AUTOSCROLL=true;	// TODO...
	const DEFAULT_HEIGHT="13em";	// ALWAYS PERCENTAGES OR EMs !
	
	const DEFAULT_TRANSITION="fade";
//	const PREVNEXT_BUTTONS_CSS="font-size:16px;font-weight:bold;background:#888888;"
//							+"-moz-border-radius: 20px;border-radius: 20px;";
	const DEFAULT_BACKGROUND="#000000";
	
	const PREVNEXT_BUTTONS_CSS="font-size:16px;font-weight:bold;background:#ffffff;";
	
	initSelf.AotraScreenCarouselListWrappingProvider = Class.create({

		// Constructor
		initialize : function(editionProvider) {

			// Attributes
			
			this.autoScroll=DEFAULT_AUTOSCROLL;// TODO...
			this.height=DEFAULT_HEIGHT;
			this.transition=DEFAULT_TRANSITION;
			this.background=DEFAULT_BACKGROUND;

			// Technical attributes
			this.editionProvider=editionProvider;
			
			editionProvider.snippetsHooks[CAROUSEL_KEY]=this;
			
		}
	
		// Methods
	
	
		/*public*/,getGeneratedHTMLOption:function(key){
			return "<option value='"+key+"'>"+i18n({"fr":"Ajouter un carousel","en":"Add carousel list"})+"</option>";
		}
		
		/*public*/,getInsertedSnippet:function(selectedText){
			var html="";
			
			html+="<div class='"+CAROUSEL_CLASS_NAME+"' data-transition='"+DEFAULT_TRANSITION+"' data-height='"+DEFAULT_HEIGHT+"' data-background='"+DEFAULT_BACKGROUND+"' >";
			html+="<ol>";
			html+="<li><div class='items'>"+(nothing(exampleItemText)?"Item 1":exampleItemText)+"</div></li>";
			html+="<li><div class='items'>Item 2</div></li>";
			html+="<li><div class='items'>Item 3</div></li>";
			html+="<li><div class='items'>...</div></li>";
			html+="</ol>";
			html+="</div>";
			
			return html;
		}
		

		// UNUSED
//		/*public*/,getGeneratedHTMLInputs:function(){
//			var mainDivInnerHTML="";
//			
//			mainDivInnerHTML+="<button class='icon carouselInsertBtn' " 
//				+"onclick='javascript:getAotraScreen().editionProvider.carouselListWrappingProvider.insertCarouselSample();' "
//				+" title='"+i18n({"fr":"Ajouter un carousel","en":"Add carousel list"})+"' ></button>";
////			&#9731
//			return mainDivInnerHTML;
//		}
		
	
		
		/*private*/,getAotraScreen:function(){
			if(this.aotraScreen)	return this.aotraScreen;
			return getAotraScreen();
		}

	
	});
	
	
	// Static methods :
	
	
	/*public*//*static*/initSelf.AotraScreenCarouselListWrappingProvider.doRenderingTreatments=function(bubble,aotraScreenParam){
		
		var divBubble=bubble.div;
		
		var carouselProvider=null;
		if(aotraScreenParam.editionProvider && aotraScreenParam.editionProvider.carouselListWrappingProvider)
			carouselProvider=aotraScreenParam.editionProvider.carouselListWrappingProvider;
		
		jQuery(divBubble).find("."+CAROUSEL_CLASS_NAME).each(function(){
			
			var thisSlct=jQuery(this);
			
			if(thisSlct.hasClass(CAROUSEL_CLASS_NAME+"Rendered"))	return;
			
			thisSlct.addClass(CAROUSEL_CLASS_NAME+"Rendered");
	
			var divCarousel=thisSlct.get(0);
			
			var attrTransitionType=divCarousel.getAttribute("data-transition");
			var attrHeight=divCarousel.getAttribute("data-height");
			var attrBackground=divCarousel.getAttribute("data-background");
				
			var autoScroll=(carouselProvider?carouselProvider.autoScroll:DEFAULT_AUTOSCROLL);
			var transition=!nothing(attrTransitionType)? attrTransitionType:(carouselProvider?carouselProvider.transition:DEFAULT_TRANSITION);
			var height=!nothing(attrHeight)?attrHeight:(carouselProvider?carouselProvider.height:DEFAULT_HEIGHT);
			var background=!nothing(attrBackground)?attrBackground:(carouselProvider?carouselProvider.background:DEFAULT_BACKGROUND);
				
			
			var labels=new Object();
			labels["closeLabel"]=i18n({"fr":"Fermer","en":"Close"});
			labels["editLabel"]=i18n({"fr":"Éditer","en":"Edit"});
			
			makeCarousel(thisSlct,height,transition,background,autoScroll
					,TRANSITION_DURATION_MILLIS,AUTOSCROLL_CHANGING_RATE_MILLIS,PREVNEXT_BUTTONS_CSS
					,aotraScreenParam.isCalculatedEditable()
					
					,function (newCarouselTransitionType,newCarouselHeight,newCarouselBackground){
				
						// Temporary creation of a hidden element for bubble content:
						var bubbleContentElementTemp=document.createElement("div");
						bubbleContentElementTemp.style.cssText="display:none";
						document.body.appendChild(bubbleContentElementTemp);
						bubbleContentElementTemp.innerHTML=toOneSimplifiedLine(bubble.getContent(),"compactHTML");
						
						// To work around a firefox behavior :
						// Actually Firefox reorganizes attributes within tag, so that their order is different than original order.
						// So we force this reorganization prior to doing anything here :
						bubble.setContent(bubbleContentElementTemp.innerHTML);
						
						// Temporary creation of a hidden unrendered carousel element :
						var unrenderedElementTemp=document.createElement("div");
						unrenderedElementTemp.style.cssText="display:none";
						document.body.appendChild(unrenderedElementTemp);
						unrenderedElementTemp.innerHTML=toOneSimplifiedLine(bubble.getContent(),"compactHTML");
						var unrenderedElement=jQuery(unrenderedElementTemp).find("."+CAROUSEL_CLASS_NAME).get(0);
						if(!unrenderedElement)	return;
						
						// We append found coordinates to element and update bubble content:
						var oldHTML=toOneSimplifiedLine(unrenderedElement.outerHTML,"compactHTML");
						unrenderedElement.setAttribute("data-transition",newCarouselTransitionType);
						unrenderedElement.setAttribute("data-height",newCarouselHeight);
						unrenderedElement.setAttribute("data-background",newCarouselBackground);
						var newHTML=toOneSimplifiedLine(unrenderedElement.outerHTML,"compactHTML");
						
						// We update bubble's content :
						var content=toOneSimplifiedLine(bubble.getContent(),"compactHTML");
						bubble.setContent(content.replace(oldHTML,newHTML));
						
						// Temporary hidden elements removal :
						unrenderedElementTemp.parentElement.removeChild(unrenderedElementTemp);
						bubbleContentElementTemp.parentElement.removeChild(bubbleContentElementTemp);
						
						
						// SIMPLER, BUT DOESN'T WORK, UNFORTUNATELY :
//						var r=new RegExp("<div class=[\"']"+CAROUSEL_CLASS_NAME+"[\"'] (data-\\w+=[\"']\\w*[\"']\\s*)*\\s*>","gim");
//						var newContent=content
//						.replace(r,"<div class='"+CAROUSEL_CLASS_NAME+"' data-width='"+newCarouselWidth+"' data-height='"+newCarouselHeight+"'"
//								+">"
//								);
//						bubble.setContent(newContent);
						
					}
			,labels);
		
		});
	};
	

	return initSelf;
}
}
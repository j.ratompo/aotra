// DEPENDENCY : needs AotraScreenUploadProvider
if(typeof initAotraScreenFilesWrappingProviderClass ==="undefined"){

	function initAotraScreenFilesWrappingProviderClass() {
	var initSelf = this;

	// CONSTANTS
	//	var MEDIA_SIZE="100%";
	var CONTENT_MEDIA_CLASS="contentMedia";
	

	initSelf.AotraScreenFilesWrappingProvider = Class.create({

		// Constructor
		initialize : function(editionProvider,uploadProvider) {

			// Attributes
			this.editionProvider=editionProvider;
			this.uploadProvider=uploadProvider;
			
			// Processed attributes
			this.textboxComponent=null;
			
		}
	
		// Methods
		,getGeneratedHTMLInputs:function(){
			var mainDivInnerHTML="";
			
			
			mainDivInnerHTML+="<button class='buttonLight' " 
				+"onclick='javascript:jQuery(\"#divAllUploadActions\").slideToggle(500);'>"
				+i18n({"fr":"Toutes les options de téléversement...","en":"All upload actions..."})
				+"</button>";
			
			mainDivInnerHTML+="<div id='divAllUploadActions' style='display:none;' >"; 

				// Files upload to server :
				if(this.uploadProvider)
					mainDivInnerHTML+=this.uploadProvider.getUploadGeneratedHTMLInputs(true);

			
				mainDivInnerHTML+="<button " 
					+" onclick='javascript:getAotraScreen().editionProvider.filesWrappingProvider.uploadAndIncludeFiles();'>"
					+i18n({"fr":"Téléverser puis inclure le fichier dans le contenu","en":"Upload then include files"})+"</button>";
	
				mainDivInnerHTML+="<input type='checkbox' id='checkboxEmbed' " 
					+" title='"+i18n({"fr":"Téléverser puis incruster le fichier dans le contenu","en":"Embed then insert files"})+"' />"
					+"<label for='checkboxEmbed'>"+i18n({"fr":"Incruster ?","en":"Embed ?"})+"</label>";

				mainDivInnerHTML+="<button " 
					+"onclick='javascript:getAotraScreen().editionProvider.filesWrappingProvider.uploadAndSetParallaxBackgroundFile();'>"
					+i18n({"fr":"Téléverser puis définir comme fond autour de la bulle","en":"Upload then set file as parallax background"})+"</button>";
				
				
			mainDivInnerHTML+="</div>";
			mainDivInnerHTML+="<br />";

			return mainDivInnerHTML;
		}


	
		// Upload or embed files
		,uploadAndIncludeFiles:function(){
			if(!document.getElementById("checkboxEmbed").checked){
				this.uploadAndInsertFiles();
			}else{
				this.embedAndInsertFiles();
			}
		}
		
		
		// Upload files
		/*private*/,uploadAndInsertFiles:function(){
			var self=this;
			
//			var editedBubble=this.editionProvider.getSelectedBubble();

			
			var selectedText=getSelectedText(this.textboxComponent).trim();
			
			var doOnEndUpload=function(){

				var files=self.uploadProvider.getChosenFiles();

				var htmlToInsert="";
				var content=toOneSimplifiedLine(self.textboxComponent.innerHTML,"compactHTML");

				// Case some text selected :
				if(!nothing(selectedText)){
					htmlToInsert=self.getHTMLToInsertFromChosenFilesUpload(files,selectedText);
					content=content.replace(selectedText,htmlToInsert);
				}else{
					
					// Case no text selected :
					htmlToInsert=self.getHTMLToInsertFromChosenFilesUpload(files);
					content=self.getContentWithInsert(content,htmlToInsert);

					
				}
				
//				editedBubble.setContent(content);
				self.textboxComponent.setContent(content);
				
			};
			
			this.uploadProvider.uploadFilesToServer(doOnEndUpload);
		}

	
		/*private*/,getHTMLToInsertFromChosenFilesUpload:function(files,/*OPTIONAL*/selectedText){
			var htmlToInsert="";
	
			
			if(empty(files))	return (selectedText?selectedText:"");

			if(files.length==1){
				var file=files[0];
				var source=this.uploadProvider.getRemoteStoragePathForFile(file);
				return this.getFileHTMLWrappingByType(file,source,selectedText);
			}
			if(files.length>2){
				for(var i=0;i<files.length;i++){
					var f=files[i];
					var source=this.uploadProvider.getRemoteStoragePathForFile(f);
					htmlToInsert+=this.getFileHTMLWrappingByType(f,source);
				}
				return (selectedText?selectedText:"")+htmlToInsert;
			}
			
			// Things get crazy when files number equals 2 :
			// If possible, we get an image :
			var imageFile=null;
			for(var i=0;i<files.length;i++){
				var f=files[i];
				if(getGenericFileType(f)==="image"){
					imageFile=f;
					break;
				}
			}
			if(imageFile){
				remove(files,imageFile);
				
				var otherFile=files[0];
				
				var sourceImage=this.uploadProvider.getRemoteStoragePathForFile(imageFile);
				var sourceOther=this.uploadProvider.getRemoteStoragePathForFile(otherFile);
				
				var t=getGenericFileType(otherFile);
				if(t==="image"){
					// We make two images :

					htmlToInsert+= this.getFileHTMLWrappingByType(imageFile,sourceImage);
					htmlToInsert+= this.getFileHTMLWrappingByType(otherFile,sourceOther);
					htmlToInsert+=(selectedText?selectedText:"");
				}else if(t==="video"){
					// We make a video with the image as poster :
					var h=this.getFileHTMLWrappingByType(otherFile,sourceOther
							,selectedText
							,this.uploadProvider.getRemoteStoragePathForFile(imageFile) );
					
					htmlToInsert+= h;
					
				}else if(t==="audio"){

					// We make an image that triggers a sound playing :
					var h=this.getFileHTMLWrappingByType(otherFile,sourceOther
							,this.getFileHTMLWrappingByType(imageFile,sourceImage)+(selectedText?selectedText:"") );
					htmlToInsert+= h;
					
				}else{
					
					// We make a link with an image inside :
					htmlToInsert+= this.getFileHTMLWrappingByType(otherFile,sourceOther
							,this.getFileHTMLWrappingByType(imageFile,sourceImage)
							+(selectedText?selectedText:"") );
				}
				
				
			}else{
				var source1=this.uploadProvider.getRemoteStoragePathForFile(files[0]);
				var source2=this.uploadProvider.getRemoteStoragePathForFile(files[1]);
				
				htmlToInsert+= this.getFileHTMLWrappingByType(files[0],source1);
				htmlToInsert+= this.getFileHTMLWrappingByType(files[1],source2);
				htmlToInsert+=(selectedText?selectedText:"");
			}
			
			return htmlToInsert;
		}

		
		// Upload then set as bubble's parallax background :
		/*private*/,uploadAndSetParallaxBackgroundFile:function(){
			
			var aotraScreen=this.getAotraScreen();
			
			var self=this;
			
			var editedBubble=this.editionProvider.getSelectedBubble();

			var doOnEndUpload=function(){

				var files=self.uploadProvider.getChosenFiles();
				if(empty(files))	return;
				
				// We only use the first file :
				var file=files[0];

				var fileName=self.uploadProvider.getRemoteStoragePathForFile(file);
				
				if(editedBubble.parallaxBackground===fileName)	return;
				
				var p=aotraScreen.getPlaneForBubble(editedBubble);
				if(!p)	return; // this case should not occur, actually...
				
				p.parallaxBackgroundsProvider.removeBubbleParallaxBackground(editedBubble);
				editedBubble.setParallaxBackground(fileName);
				p.parallaxBackgroundsProvider.addBubbleParallaxBackground(editedBubble);
				
				document.getElementById("textboxEditParallaxBackground").value=fileName;

			};
			
			this.uploadProvider.uploadFilesToServer(doOnEndUpload);
			
		}

		
		
		// Embed files :
		
		/*private*/,embedAndInsertFiles:function(){
			var editedBubble=this.editionProvider.getSelectedBubble();

			
			var selectedText=getSelectedText(this.textboxComponent).trim();

			var files=this.uploadProvider.getChosenFiles();
				
			
			// For the moment, we only can embed files one at the time :
			if(files.length<1)	return (selectedText?selectedText:"");
			var file=files[0];

				
			var self=this;
			var doOnEndEmbed=function(){

				var source =self.uploadProvider.getResultingEmbeddingStringForCurrentFile();

				var htmlToInsert="";
				var content=toOneSimplifiedLine(self.textboxComponent.innerHTML,"compactHTML");

				// Case some text selected :
				if(!nothing(selectedText)){
					htmlToInsert=self.getFileHTMLWrappingByType(file,source,selectedText);
					content=content.replace(selectedText,htmlToInsert);
				}else{
					// Case no text selected :
					htmlToInsert=self.getFileHTMLWrappingByType(file,source);
					
					content=self.getContentWithInsert(content,htmlToInsert);
				}
				
//				editedBubble.setContent(content);
				self.textboxComponent.setContent(content);
				
			};
			
			
			this.uploadProvider.startReadingFile(file,doOnEndEmbed);
			
		}
		
		/*private*/,getContentWithInsert:function(contentParam,htmlToInsert){
			var textboxComponent=this.textboxComponent;
			
			var content=contentParam;
			
			// DOES NOT WORK :
//			var caretPosition=getHTMLCaretPositionInRichEditor(textboxComponent);
			 // We append new content at the end, if we have no caret detectable position :
//			if(caretPosition<=0 || content.length<=0)
			content="<br /><br />"+htmlToInsert+"<br /><br />"+content;
//			else{
//				content=content.substring(0,caretPosition)
//					+htmlToInsert
//					+content.substring(caretPosition,content.length-1);
//			}
			
			return content;
		}
		
		/*private*/,getFileHTMLWrappingByType:function(file,source,labelTextParam,/*OPTIONAL*/posterImagePath){
			
			var elementId=file.name;
			var t=getGenericFileType(file);
			
			if(t==="image"){
				var r=
					"<div>"
					+"<img class='"+CONTENT_MEDIA_CLASS+"' ";
				r+="src='"+source+"' ";
				r+="alt='"+file.name+"'/> <br />"
					+(labelTextParam?labelTextParam:"")
					+"<br />"
					+"</div>";
				
				return r;
			}else if(t==="video"){
				var r="<div>"
				  +"<video id='"+elementId+"' controls class='"+CONTENT_MEDIA_CLASS+"' "
				  +(posterImagePath?(" poster='"+posterImagePath+"' "):"")
				  +" >"
				  +"<source ";
				r+="src='"+source+"' ";
				r+="type='"+file.type+"'>"
				  +"Your browser does not support the video tag."
				  +"</video>"
				  +"<br />"
				  +(labelTextParam?("<a href=\"javascript:playMedia(document.getElementById('"+elementId+"'));\" >"+labelTextParam+"</a>"):"")
				  +"</div>";
				return r;
			}else if(t==="audio"){
				
				var r=
				  "<div>"
				  +(labelTextParam?("<a href=\"javascript:playMedia(document.getElementById('"+elementId+"'));\" >"+labelTextParam+"</a>"):"")
				  +"<audio id='"+elementId+"' controls class='"+CONTENT_MEDIA_CLASS+"'>"
				  +"<source ";
				r+="src='"+source+"' ";
				r+="type='"+file.type+"'>"
				  +"<embed ";
				r+="src='"+source+"' ";
				r+="class='"+CONTENT_MEDIA_CLASS+"'>"
				  +"</audio>"
				  +"</div>";
				  
				return r ;
			}

			return "<a href='"+(source)+"'>"
					+((labelTextParam && !nothing(labelTextParam))?labelTextParam:file.name)
					+"<a>";
		}
		
		// Accessors
		,setTextboxComponent:function(c){
			this.textboxComponent=c;
		}
		
		,getAotraScreen:function(){
			if(this.aotraScreen)	return this.aotraScreen;
			return getAotraScreen();
		}
		
	
	});

	return initSelf;
}
}
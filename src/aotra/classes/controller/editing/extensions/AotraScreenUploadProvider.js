// DEPENDENCY : needs AotraScreenSavingProvider

if(typeof initScreenUploadProviderClass ==="undefined"){
	
	
function initScreenUploadProviderClass() {
	var initSelf = this;

	// CONSTANTS

	initSelf.AotraScreenUploadProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen,usingComponentName,acceptedTypesString) {

			// Attributes
			this.aotraScreen = aotraScreen;
			this.usingComponentName=usingComponentName;
			this.acceptedTypesString=acceptedTypesString;
			
			
			this.lastAllConnectionInfo=null;
			
			// For files embedding :
			this.fileReader;
		}
	
		// Methods
		// File Upload inputs :
		,getUploadGeneratedHTMLInputs:function(noUploadButton){
			var mainDivInnerHTML="";
			
			// File upload to server :
			mainDivInnerHTML+="<div id='divUploadToServer_"+this.usingComponentName+"'>";
			mainDivInnerHTML+="<form id='formUploadFiles_"+this.usingComponentName+"' enctype='multipart/form-data' "
				+" method='POST'>";
			mainDivInnerHTML+="<input type='file' id='uploadedFilesField_"+this.usingComponentName+"' "
				+" name='uploadedFiles[]' accept='"+this.acceptedTypesString+"' multiple='multiple' " 
				+" onchange=\"validateFilesExtensions(this,'"+this.acceptedTypesString+"');\" " 
				+" style='border:solid 2px gray;height:20px;width:200px;background:#FFF' />";
			mainDivInnerHTML+="</form>";
			
			// If we are in a component that will make things with these files by itself :
			if(!noUploadButton){
				mainDivInnerHTML+="<button " 
					+"onclick='javascript:getAotraScreen().uploadProviders."+this.usingComponentName+".uploadFilesToServer();'>"+i18n({"fr":"Envoyer les fichiers sur le serveur","en":"Send files to server"})+"</button>";
				//mainDivInnerHTML+="<progress id='progressHandlingFunction'></progress>";
				
				mainDivInnerHTML+="<br />"; 

				// File embedding to page :
				mainDivInnerHTML+="<button " 
					+"onclick='javascript:getAotraScreen().uploadProviders."+this.usingComponentName+".alertData64StringFromFile();'>"+i18n({"fr":"Montrer la chaîne en base 64 représentant l’image","en":"Show data base 64 String"})+"</button>";
				
			}
			

			var iframeId = "iframeUploadResponse_"+this.usingComponentName;
			mainDivInnerHTML+="<iframe id='"+iframeId+"' name='"+iframeId+"' "
			+" seamless='seamless' "
			+" style='display:none; "
			+"'>Iframes not supported.</iframe>";
			
			mainDivInnerHTML+="</div>";
			
			return mainDivInnerHTML;
		}
	
		// Files upload :
	
		,uploadFilesToServer:function(doOnEndUpload){
			
			var files=this.getChosenFiles();
			if(empty(files)){
				alert("Please choose files to upload.");
				return;
			}
			
			var info = this.promptAllConnectionInformations(this,"uploadFilesToServer",doOnEndUpload);
			if(info==null)	return;			

			let formId="formUploadFiles_"+this.usingComponentName;
			let form=document.forms[formId];
			
			let paramActionTypeId=formId+"_actionType";
			let paramActionType = document.getElementById(paramActionTypeId);
			if(!paramActionType){
				paramActionType = document.createElement("input");
				paramActionType.id=paramActionTypeId;
				paramActionType.type = "hidden";
				paramActionType.name = "actionType";
	      form.appendChild(paramActionType);
			}
			paramActionType.value = "UPLOAD";

			let paramPageNameId=formId+"_pageName";
			let paramPageName = document.getElementById(paramPageNameId);
			if(!paramPageName){
				paramPageName = document.createElement("input");
				paramPageName.id=paramPageNameId;
				paramPageName.type = "hidden";
				paramPageName.name = "pageName";
	      form.appendChild(paramPageName);
	     }
			paramPageName.value = info.pageName;
			
			let paramWritePasswordHashId=formId+"_writePasswordHash";
			let paramWritePasswordHash = document.getElementById(paramWritePasswordHashId);
			if(!paramWritePasswordHash){
				paramWritePasswordHash = document.createElement("input");
				paramWritePasswordHash.id=paramWritePasswordHashId;
				paramWritePasswordHash.type = "hidden";
				paramWritePasswordHash.name = "writePasswordHash";
	      form.appendChild(paramWritePasswordHash);
			}
			paramWritePasswordHash.value = info.writePasswordHash;

			// TODO : FIXME : DEBUG : we always have a 0100 error here !!!
			
			var actionURL=info.endpointFileURL;

			form.action = actionURL;
			form.target = "iframeUploadResponse_"+this.usingComponentName;
			form.submit();
			
			var REPEAT_SECONDS=0.5;
			
			var self=this;
			var pe= new PeriodicalExecuter(function() {
				var iframeUploadResponse = document.getElementById("iframeUploadResponse_"+self.usingComponentName);
				
				var b=iframeUploadResponse.contentWindow.document.body;
				if(b!==null){
					var response=b.innerHTML;
					if(!nothing(response)){
						// TRACE
						log(response+" (number of uploaded files : "+files.length+" )");

						alert(response+" (number of uploaded files : "+files.length+" )");
						if(response.substring(0,2)=="00"){
							if(response.substring(0,4)==="0002"){
								if(doOnEndUpload)	doOnEndUpload();
							}
//							self.closeParent();
							
						}else if(response.substring(0,4)=="0201"){
							// If password is incorrect, then we don't keep it :
							self.lastAllConnectionInfo.password="";
							document.getElementById("textboxEditWritePassword").value="";
						}
						pe.stop();
						return;
					}
					// TRACE
					log("waiting for server response...");
					return;
				}
				
				// TRACE
				alert("IFRAME is non-existent");
				pe.stop();
				return;

			},REPEAT_SECONDS);
			
		}

		

		,promptAllConnectionInformations:function(obj,callerFunctionName,doOnEndUpload){
			var aotraScreen=this.aotraScreen;
			
			
			// DEPENDENCY : needs AotraScreenSavingProvider in order to work !
			if(aotraScreen.savingProvider){
				var typedPassword=document.getElementById("textboxEditWritePassword").value;
				if(nothing(typedPassword)){
					
					promptWindow("Please enter write password","password",
					"",function(p){
						if(!nothing(p)){
							document.getElementById("textboxEditWritePassword").value=p;
							obj[callerFunctionName](doOnEndUpload);
						}
					});
					return null;
					
				}
				this.lastAllConnectionInfo=aotraScreen.savingProvider.promptGlobalConnectionInformations();
				return this.lastAllConnectionInfo;
			}
			alert("Error : Upload provider requires saving provider for password settings in order to work !");
			return null;
		}
		
		/*public*/,getRemoteStoragePathForFile:function(file){
			var result=this.getRemoteStorageDirectoryForFile(file)+file.name;
			
			//TODO : FIXME : sometimes, site url is appended at the beginning of the source of image...
			
			return result;
		}
		
		/*private*/,getRemoteStorageDirectoryForFile:function(file){
			if(!this.lastAllConnectionInfo)
				this.lastAllConnectionInfo=this.aotraScreen.savingProvider.promptGlobalConnectionInformations();
			
			var pageName=this.lastAllConnectionInfo.pageName;
			var directory=pageName.replace(".","_")+"/";
			
			// MUST MATCH SERVER BACK-END FILES REMOTE STORING CONTRACT :
			var IMAGES_TYPE_DIRECTORY="images";
			var VIDEOS_TYPE_DIRECTORY="videos";
			var AUDIOS_TYPE_DIRECTORY="audios";
			var OTHERS_TYPE_DIRECTORY="";
			
			if(getGenericFileType(file)=="image"){
				directory+=IMAGES_TYPE_DIRECTORY+"/";
			}else if(getGenericFileType(file)=="video"){
				directory+=VIDEOS_TYPE_DIRECTORY+"/";
			}else if(getGenericFileType(file)=="audio"){
				directory+=AUDIOS_TYPE_DIRECTORY+"/";
			}else{
				directory+=OTHERS_TYPE_DIRECTORY;
			}
			
			return directory;
		}


		// Files reading and encoding for embedding :
		,alertData64StringFromFile:function(){
			
			var file = this.getChosenFiles()[0];
			
			var self=this;
			this.startReadingFile(file,function() {
				 var result = self.getResultingEmbeddingStringForCurrentFile();
				 
				 promptWindow("Output base64 encoded string for file «"+(file.name)+"» :",null,result);
				 
				 // TRACE
				 log(result);
			});
		}
		
		// DOC FOR SYNCHRONOUS MODE FOR READING FILES :
		// (PLEASE KEEP THIS CODE)
		// SOURCE : www.html5canvastutorials.com/advanced/html5-canvas-invert-image-colors-tutorial/
		//	var fs = require('fs');
		//	var base64_data = fs.readFileSync('sample.png').toString('base64');
		//	console.log('<img alt="sample" src="data:image/png;base64,' + base64_data + '">');

		// ASYNCHRONOUS MODE  FOR READING FILES (=without blocking browser) : 
		,startReadingFile:function(file,callback){
			
			// Files reading initialization :
			this.fileReader = new FileReader();
			this.fileReader.readAsDataURL(file);
			
			this.fileReader.onload=callback;
			
		}
		
		,getResultingEmbeddingStringForCurrentFile:function(){
			return this.fileReader.result;
		}
		

		
		// Utility methods :
		,getChosenFiles:function(){
			var uploadedFilesField=document.getElementById("uploadedFilesField_"+this.usingComponentName);
			var filesList=uploadedFilesField.files;
			
			var files=new Array();
			
			// Iceweasel hack :
			if(filesList.length==1 && !filesList[0].name && uploadedFilesField.value){
				var f=new Object();
				f.name=uploadedFilesField.value;
				var s=f.name.split(".");
				f.type=getGenericFileType(f)+"/"+s[s.length-1];
				f.size=0;
				files.push(f);
				return files;
			}
			
			for(var i=0;i<filesList.length;i++){
				var f=filesList[i];
				files.push(f);
			}
			
			return files;
		}
		
		
		,closeParent:function(){
			setElementVisible(document.getElementById("divUploadToServer_"+this.usingComponentName).parentNode,false);
		}
	});

	return initSelf;
}
}
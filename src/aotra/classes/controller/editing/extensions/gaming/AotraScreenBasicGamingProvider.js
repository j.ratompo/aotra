
	// GLOBAL CONSTANTS
	var BASIC_GAMING_KEY="basicGaming";
	
// This class is just here to provide a handful of snippets for the aotrautils basic gaming library :

if(typeof initScreenBasicGamingProviderClass ==="undefined"){
function initScreenBasicGamingProviderClass() {
	var initSelf = this;

	// CONSTANTS
	var MODULE_KEY="AotraScreenBasicGamingProvider";
	
	initSelf.AotraScreenBasicGamingProvider = Class.create({

		// Constructor
		initialize : function(editionProvider) {
			// Attributes

			// Technical attributes
			this.editionProvider=editionProvider;

  		// Processed attributes
  		var self=this;
  		
  		
			editionProvider.snippetsHooks[BASIC_GAMING_KEY+"_rand"]={
			  	/*public*/getGeneratedHTMLOption:function(key){
						return "<option value='"+key+"'>"+i18n({"fr":"Jeu : alea","en":"Game : rand"})+"</option>";
			  	}
			  	/*public*/,getInsertedSnippet:function(selectedText){
						return "<div class='game group rand'>"
										+	"<div data-value='1'>"+selectedText+"</div><div data-value='1'>value 2</div><div data-value='1'>value 3</div>"
										+"</div>";
			  	}
			};
			
			editionProvider.snippetsHooks[BASIC_GAMING_KEY+"_if_something"]={
			  	/*public*/getGeneratedHTMLOption:function(key){
						return "<option value='"+key+"'>"+i18n({"fr":"Jeu : si existe","en":"Game : if something"})+"</option>";
			  	}
			  	/*public*/,getInsertedSnippet:function(selectedText){
						// As for now, requires an execution blocking prompt :
			  		var key=prompt(i18n({"fr":"Entrez le nom-clef de la variable","en":"Please enter variable key name"}));
						return "<div class='game if something' data-key='"+(nothing(key)?"KEY":key)+"'>"	+ selectedText+"</div>";
			  	}
			};

			editionProvider.snippetsHooks[BASIC_GAMING_KEY+"_if_nothing"]={
			  	/*public*/getGeneratedHTMLOption:function(key){
						return "<option value='"+key+"'>"+i18n({"fr":"Jeu : si n'existe pas","en":"Game : if nothing"})+"</option>";
			  	}
			  	/*public*/,getInsertedSnippet:function(selectedText){
			  		var key=prompt(i18n({"fr":"Entrez le nom-clef de la variable","en":"Please enter variable key name"}));
			  		return "<div class='game if nothing' data-key='"+(nothing(key)?"KEY":key)+"'>"	+ selectedText+"</div>";
			  	}
			};
			
			
		}

  	
		// Methods
  	//UI Elements adding :
  	//*************************************

	
		
	});
	
	
	//Static initialization :
	/*static*/{
		
		
		var basicGamingProviderScreenIniter=new Object();
		AotraScreenEditionProvider.hooks[SNIPPETS][MODULE_KEY]=basicGamingProviderScreenIniter;
		basicGamingProviderScreenIniter.init=function(aotraScreen,editionProvider){

			if(nothing(editionProvider) || nothing(editionProvider.snippetsHooks) )	return;
			
			// We init a new provider for this aotraScreen :
			aotraScreen.basicGamingProvider=new AotraScreenBasicGamingProvider(editionProvider);
			
		};
		
		
		
	}

	return initSelf;
}
}
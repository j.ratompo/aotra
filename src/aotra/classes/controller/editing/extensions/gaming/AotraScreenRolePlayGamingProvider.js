
	// GLOBAL CONSTANTS
	var ROLE_PLAYING_GAMING_KEY="rolePlayingGaming";
	
// This class is just here to provide a handful of snippets for the aotrautils role playing gaming library :

if(typeof(initScreenRolePlayingGamingProviderClass)==="undefined" && typeof(AotraScreenRolePlayingGamingProvider)==="undefined"){
	function initScreenRolePlayingGamingProviderClass() {

	// CONSTANTS
	var MODULE_KEY="AotraScreenRolePlayingGamingProvider";
	
	AotraScreenRolePlayingGamingProvider = class{

			// Constructor
			constructor(editionProvider) {
				// Attributes
	
				// Technical attributes
				this.editionProvider=editionProvider;
	
	  		// Processed attributes
	  		var self=this;
	  		
	  		
				editionProvider.snippetsHooks[ROLE_PLAYING_GAMING_KEY+"_spec"]={
				  	/*public*/getGeneratedHTMLOption:function(key){
							return "<option value='"+key+"'>"+i18n({"fr":"JDR : spécification","en":"RPG : specification"})+"</option>";
				  	}
				  	/*public*/,getInsertedSnippet:function(selectedText){
							return "<div class='game rpg spec'>"
											 +"<span class='params'>rpg."+selectedText + ":100/100:-30,-20,-10,0,+10,+20,+30:dice:#FFAA00"+"</span>"
										+"</div>";
				  	}
				};
	
				editionProvider.snippetsHooks[ROLE_PLAYING_GAMING_KEY+"_amount_gauge"]={
			  	/*public*/getGeneratedHTMLOption:function(key){
						return "<option value='"+key+"'>"+i18n({"fr":"JDR : montant : gauge","en":"RPG : amount : gauge"})+"</option>";
			  	}
			  	/*public*/,getInsertedSnippet:function(selectedText){
						return "<div class='game rpg amount'>" // gauge, because there is a total value
										+"<span class='params'>rpg."+selectedText + ":1000/1000:+-:#FFAA00"+"</span>"
									+"</div>";
			  	}
				};
				
				editionProvider.snippetsHooks[ROLE_PLAYING_GAMING_KEY+"_amount_quantity"]={
				  	/*public*/getGeneratedHTMLOption:function(key){
							return "<option value='"+key+"'>"+i18n({"fr":"JDR : montant : quantité","en":"RPG : amount : amount"})+"</option>";
				  	}
				  	/*public*/,getInsertedSnippet:function(selectedText){
							return "<div class='game rpg amount'>" // quantity, because there is no total value
											 +"<span class='params'>rpg."+selectedText + ":1000:+-:#FFAA00"+"</span>"
										+"</div>";
				  	}
				};
				
				editionProvider.snippetsHooks[ROLE_PLAYING_GAMING_KEY+"_inventory"]={
			  	/*public*/getGeneratedHTMLOption:function(key){
						return "<option value='"+key+"'>"+i18n({"fr":"JDR : inventaire","en":"RPG : inventory"})+"</option>";
			  	}
			  	/*public*/,getInsertedSnippet:function(selectedText){
						return "<div class='game rpg inventory' data-config='{}'>"
										 +"<span class='params'>rpg."+selectedText+":5x5</span>"
									+"</div>";
			  	}
				};

				
				
				editionProvider.snippetsHooks[ROLE_PLAYING_GAMING_KEY+"_diceTest"]={
			  	/*public*/getGeneratedHTMLOption:function(key){
						return "<option value='"+key+"'>"+i18n({"fr":"JDR : Test de dés","en":"RPG : Dice throw"})+"</option>";
			  	}
			  	/*public*/,getInsertedSnippet:function(selectedText){
						return "<div class='game rpg diceTest' data-config='{}'>"
										 +"<span class='params'>rpg."+selectedText+"</span>"
										 +"<div class='success'>If succeed...</div>"
										 +"<div class='fail'>If failed...</div>"
									+"</div>";
			  	}
				};

				
				
				
	
			}
	
			
		
			/*private*/doOnSaveBubble(bubble){
				
				var self=this;
				
				var textDiv=bubble.getTextDiv();
				var $textDiv=jQuery(textDiv);
				
				var $rpg=$textDiv.find(".game.rpg");
				if(!$rpg.length)	return;
				
				initRPGGaming($rpg,textDiv);				
				
			}
		
			
			/*private*/doOnContentRenderForBubble(bubble){
		
				var textDiv=bubble.getTextDiv();
				var $textDiv=jQuery(textDiv);
				
				var $rpg=$textDiv.find(".game.rpg");
				if(!$rpg.length)	return;

				initRPGGaming($rpg,textDiv);				

			}
		
		
		}
	
	
		//Static initialization :
		/*static*/{
			
			var rolePlayingGamingProviderScreenIniter=new Object();
			AotraScreenEditionProvider.hooks[SNIPPETS][MODULE_KEY]=rolePlayingGamingProviderScreenIniter;
			rolePlayingGamingProviderScreenIniter.init=function(aotraScreen,editionProvider){
	
				if(nothing(editionProvider) || nothing(editionProvider.snippetsHooks) )	return;
				
				// We init a new provider for this aotraScreen :
				aotraScreen.rolePlayingGamingProvider=new AotraScreenRolePlayingGamingProvider(editionProvider);
				
			};
			
			AotraScreenEditionProvider.hooks[DO_ON_SAVE_EDIT].push({
				execute:function(aotraScreen,editionProvider){
					if(nothing(editionProvider) || nothing(aotraScreen.rolePlayingGamingProvider))
						return;
					aotraScreen.rolePlayingGamingProvider.doOnSaveBubble(editionProvider.getSelectedBubble());	
				}
				
			});
		
			Bubble.hooks[DO_ON_CREATE_TEXT_DIV].push({
				execute:function(aotraScreen,bubble){
					if(nothing(aotraScreen.rolePlayingGamingProvider))
						return;
					aotraScreen.rolePlayingGamingProvider.doOnContentRenderForBubble(bubble);	
				}
				
			});
		
		
		}

	}
	
}
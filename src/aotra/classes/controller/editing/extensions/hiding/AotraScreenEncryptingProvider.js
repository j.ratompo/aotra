	// GLOBAL CONSTANTS
	const TEXTBOX_ENCRYPT_PASSWORD_ID="textboxEncryptPassword";
	const TEXTBOX_CLUE_ID="textboxClueForPassword";
	const TEXTBOX_DECRYPT_PASSWORD_ID="textboxDecryptPassword";


if(typeof initScreenEncryptingProviderClass ==="undefined"){
function initScreenEncryptingProviderClass() {
	var initSelf = this;

	// CONSTANTS
	const ENCRYPTING_FIELDS_DIV_ID="encryptingFieldsDiv";
	const ACTIVATE_CONFIRMATION=false;
	
	
	initSelf.AotraScreenEncryptingProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {
			// Attributes
			this.aotraScreen = aotraScreen;
			
			// Processed attributes
			this.backEndFilePath=config.aotraScreen.backEndFilePath?config.aotraScreen.backEndFilePath:DEFAULT_BACK_END_FILE_PATH;
			
		}
	
		// Methods
	
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// DEPENDENCY : AotraScreenSavinigProvider !
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		,setSavingProvider:function(savingProvider){
			this.savingProvider=savingProvider;
		}
	
		,getGeneratedHTMLInputs:function(){
			var mainDivInnerHTML="<div id='"+ENCRYPTING_FIELDS_DIV_ID+"' style='display:none;'>";
			
			mainDivInnerHTML+= "<div style='display:flex;flex-direction:row;'>";
			
			mainDivInnerHTML+= "<span><input type='password' id='"+TEXTBOX_ENCRYPT_PASSWORD_ID+"' " 
			+"style='border:solid 2px gray;height:20px;width:200px;'"
			+"placeHolder='Encrypting password...' /></span>";
			
			if(ACTIVATE_CONFIRMATION){
				mainDivInnerHTML+= "<span style='50%'><input type='password' id='"+TEXTBOX_ENCRYPT_PASSWORD_ID+"Confirm' " 
				+"style='border:solid 2px gray;height:20px;width:200px;'"
				+"placeHolder='Encrypting password confirmation...' /></span>";
			}

			mainDivInnerHTML+= "</div>";

			
			mainDivInnerHTML+= "<br /><input type='text' id='"+TEXTBOX_CLUE_ID+"' " 
			+"style='border:solid 2px gray;height:20px;width:200px;'"
			+"placeHolder='Password clue...(optional)' />";

			mainDivInnerHTML+="</div>";
			mainDivInnerHTML+=
				"<input id='checkboxIsSavingEncrypted' type='checkbox' "
				+" onclick=\"javascript:if(this.checked){ "
				+"    setElementVisible('"+ENCRYPTING_FIELDS_DIV_ID+"',true); "
				+"		document.getElementById('"+TEXTBOX_ENCRYPT_PASSWORD_ID+"').focus();"
				+"    alert('"+i18n({"fr":"Attention : Veuillez noter deux choses:\\n" 
					+"1) Si vous perdez le mot de passe d’encryptage, vous ne pourrez plus récupérer votre contenu ! Utilisez cette option à vos risques et périls...\\n"
					+"2) Une fois une page cryptée, si vous voulez la faire exister en mode non-crypté la rendra non-sécurisée ! Vous devrez donc entrer vos de mot de passe de décryptage à chaque fois que vous voudrez consulter son contenu, à moins que vous ne copiiez-colliez le code de son modèle dans une autre page, non cryptée.\\n"
					,"en":"Caution : Please note two important points:\\n" 
						+"1) If you loose your encryption password, there will be no way to recover your content ! Use this feature at your own risk.\\n"
						+"2) Once you encrypt a page, if you want to publish it unencrypted again, it will compromise your encryption key. So you will have to enter the decrypting password everytime you want to decrypt your content, or you will have to copy-paste the model code in another page, unencrypted.\\n"
				})
				+"');"
				+" } else "
				+"    setElementVisible('"+ENCRYPTING_FIELDS_DIV_ID+"',false); "
				+"\" "
				+" />"
				+"<label for='checkboxIsSavingEncrypted'>"
				+i18n({"fr":"Crypter les données ?","en":"Encrypt data ?"})+"</label><br/>";
			return mainDivInnerHTML;
		}

		// ENCRYPT
		,validateEncryptingSaveToServer:function(/*OPTIONAL*/chosenServerURL){
			var savingProvider=this.savingProvider;
			
			if(document.getElementById("checkboxIsSavingEncrypted")
			&& !document.getElementById("checkboxIsSavingEncrypted").checked){
				savingProvider.validateEncryptingSaveToServer();
				return;
			}
			
			info = savingProvider.promptGlobalConnectionInformations();
			if(info==null)	return;

			var connectionInfo=this.savingProvider.promptGlobalConnectionInformations();
			var writePasswordHash=connectionInfo.writePasswordHash;
			
			var encryptPasswordHash=this.getEncryptingPasswordHash(TEXTBOX_ENCRYPT_PASSWORD_ID);
			if(!encryptPasswordHash)	return;

			// If write password is null, because none was set on the file, then we accept whatever encrypting password :
			if(nothing(writePasswordHash) || writePasswordHash===encryptPasswordHash){
				alert(i18n({"fr":"Vous devez choisir un mot de passe d'encryptage différent de votre mot de passe d'écriture."
									 ,"en":"You must choose an encrypting password that is different from your write password."}));
				document.getElementById(TEXTBOX_ENCRYPT_PASSWORD_ID).value="";
				return;
			}
			
			if(ACTIVATE_CONFIRMATION){
				// Password confirmation check :
				var encryptPasswordHashConfirm=this.getEncryptingPasswordHash(TEXTBOX_ENCRYPT_PASSWORD_ID+"Confirm");
				if(!encryptPasswordHashConfirm || encryptPasswordHash!==encryptPasswordHashConfirm ){
					alert(i18n({"fr":"Vous devez entrer le même mot de passe de confirmation."
						 ,"en":"You must enter the same confirmation password."}));
					document.getElementById(TEXTBOX_ENCRYPT_PASSWORD_ID+"Confirm").value="";
					return;
				}
			}
			
			var passwordClue=nonull(document.getElementById(TEXTBOX_CLUE_ID).value,"").trim();

			var parameters=
				"actionType=WRITEENCRYPTED" 
				+"&pageName=" + info.pageName
				+"&writePasswordHash=" + writePasswordHash
				+"&encryptPasswordHash=" + encryptPasswordHash
				+"&passwordClue=" + passwordClue
				+"&data=" + info.transmittedData;
			
			// To save encrypted to another server :
			var u=info.endpointFileURL;
			if(!nothing(chosenServerURL)){
				u=chosenServerURL+"/"+this.backEndFilePath;
				if(chosenServerURL!==getCalculatedServerURLOnly()){
					parameters+="&otherServerURL="+u;
				}
			}
			
			jQuery.ajax({
		          async: "true",
		          type: "POST",
		          url: info.endpointFileURL,
		          // Data as parameters/values pairs
		          // TODO : Compress sent data (the whole HTML code) :
		          data:parameters,
		          error:function(errorDataResponse) {
		        	  //If error
		        	  alert("NO RESPONSE (An Ajax error occured) : (see JS console for details)");
		        	  // TRACE
		        	  log("ERROR : "+errorDataResponse);
		          },
		          success: function(dataResponse) {
		        	  //If everything went OK
		        	  // TRACE
		        	  console.log("RESPONSE from «"+u+"»");

		        	  alert("RESPONSE from «"+u+"» : "+dataResponse);
		        	  if(dataResponse.substring(0,2)=="00"){
		        		  savingProvider.close();
		        	  }
		          }
		    });

		}

		/*private*/,getEncryptingPasswordHash:function(textboxId){
			var encryptingPassword=document.getElementById(textboxId).value;
			if(nothing(encryptingPassword,true)){
				alert(i18n({"fr":"Merci d’entrer un mot de passe valide, c’est-à-dire non vide.","en":"Please enter a valid password (non-empty)."}));
				return;
			}
			return getHashedString(encryptingPassword, "SHA-256", true);
		}

		
		
		// DECRYPT
		,sendDecryptRequest:function(){
//			var self=this;

			var decryptPassword=document.getElementById(TEXTBOX_DECRYPT_PASSWORD_ID).value;
			if(!decryptPassword)	return;
			var decryptPasswordHash=getHashedString(decryptPassword, "SHA-256", true);
			
			var pageName=getCalculatedPageName();
			
//			var endpointFileURL=serverURL.substring(0,serverURL.lastIndexOf("/"))+"/"+this.backEndFilePath;
			var endpointFileURL=getCalculatedServerURLOnly()+this.backEndFilePath;
			
			var parameters=
				"actionType=DECRYPT" 
				+"&pageName=" + pageName
				+"&decryptPasswordHash=" + decryptPasswordHash;
			
			
			jQuery.ajax({
		          async: "true",
		          type: "POST",
		          url: endpointFileURL,
		          // Data as parameters/values pairs
		          // TODO : Compress sent data (the whole HTML code) :
		          data:parameters,
		          error:function(errorDataResponse) {
		        	  //If error
		        	  alert("NO RESPONSE (An Ajax error occured) : (see JS console for details)");
		        	  // TRACE
		        	  log(errorDataResponse);
		        	  
		          },
		          success: function(dataResponse) {
		          	
		        	  //If everything went OK
		        	  if(containsIgnoreCase(dataResponse,"ERROR")===false){

			        	  // TRACE
			        	  console.log("SUCCESS : ");

		        	  	var XMLCode=dataResponse;
		        		  AotraScreen.updateAotraScreenFromXML(XMLCode);

		        	  }else{
			        	  // TRACE
			        	  console.log("ERROR : ",dataResponse);

		        		  alert(dataResponse);
		        		  // We empty password, if error is not due to freeze time (ie. password is incorrect) :
		        		  if(!contains(dataResponse,"0205"))
		        			  document.getElementById(TEXTBOX_DECRYPT_PASSWORD_ID).value="";
		        	  }
		        	  
		          }
		    });
			
			
		}
		
		
	});
	
	
		// STATIC CODE :
		/*static*/{
		initSelf.AotraScreenEncryptingProvider.initPasswordField=function(){
			
			var $td=jQuery("#"+TEXTBOX_DECRYPT_PASSWORD_ID);
			
		  initCalligraphicPassword($td);
		  
			// Focus on password field :
		  $td.focus();
		  
		};
		
		// Hooks 
		window.AotraScreenSavingProvider.hooks[INIT_AFTER_READY].push({
			execute:function(aotraScreen){
				
				jQuery(document).ready(function(){
				
					var $te=jQuery("#"+TEXTBOX_ENCRYPT_PASSWORD_ID);
				  initCalligraphicPassword($te);
				  addPasswordVisibleButton($te);
				  
				  if(ACTIVATE_CONFIRMATION){
					  var $tec=jQuery("#"+TEXTBOX_ENCRYPT_PASSWORD_ID+"Confirm");
					  initCalligraphicPassword($tec);
					  addPasswordVisibleButton($tec);
				  }
				  
				});

			}
			
		});
	};
	

	return initSelf;
}
}
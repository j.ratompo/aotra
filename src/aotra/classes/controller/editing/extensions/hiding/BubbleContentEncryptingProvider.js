  // GLOBAL CONSTANTS
  const CONTENT_ENCRYPTING_KEY="content_encrypting";

  
if(typeof(initBubbleContentEncryptingProviderClass)==="undefined" && typeof(BubbleContentEncryptingProvider)==="undefined"){
	function initBubbleContentEncryptingProviderClass() {
		
		// PRIVATE CONSTANTS
		const GENERATE_LABEL=true;
		const DEFAULT_LABEL="[please change this encrypted content string]";
		const DEFAULT_PREFIX=null; // null value will mean no prefix will be added to inserted encrypted content
		
		const RANDOM_STRING_SIZES_RANGE="17->33";
		
		
		BubbleContentEncryptingProvider = class {
			
		
				// Constructor
				constructor() {
		
					// Attributes
					
					// Technical attributes
					this.appendStrategy="end";
					
				}
			
				// Methods
			
				/*public*/getGeneratedHTMLOption(key){
					return "<option value='"+key+"'>"
										+i18n({"fr":"Ajouter un contenu crypté","en":"Add encrypted content"})
								+"</option>";
				}
				
				/*public*/getInsertedSnippet(selectedText){
					var html="";
					if(selectedText && !empty(selectedText)){
						html+=selectedText+"<br />";
					}else if(DEFAULT_PREFIX){
						html+=DEFAULT_PREFIX+"<br />";
					}
					html+="<div class='cryptographied' data-encrypted='false'>";
					html+=GENERATE_LABEL?generateRandomString(RANDOM_STRING_SIZES_RANGE,"simple"):DEFAULT_LABEL;
					html+="</div><br />";
					
					return html;
				}
				
				/*private*/doOnSaveBubble(bubble){
					
					var self=this;
					
					if(!contains(bubble.getContent(),"cryptographied"))	return;
					
					// We encrypt all that needs to be encrypted :
					promptWindow(
					 	 i18n({"fr":"Veuillez entrer le mot de passe de cryptage","en":"Please enter encrypting password"})
						,"password",
						"",function(p){
						if(nothing(p)) 	return;
		
						let textDiv=bubble.getTextDiv();
						let encryptedContentsCouples=calculateEncryptedContentCouples(textDiv,p);
						let content=bubble.getContent();

						// This is quite an expensive treatment :
						foreach(encryptedContentsCouples,(c)=>{
							content=content.replace(new RegExp(c.oldContent,"gim"),c.newContent);
						});
						
						bubble.setContent(content);
						
						self.addToggleEncryptionButtonsOnBubble(bubble);

					});
					
				}
		
				
				/*private*/doOnContentRenderForBubble(bubble){
					// CAUTION : DO NEVER USE «bubbleDiv» AS A LOCAL VARIABLE NAME !
					// CAN PROVOKE STRANGE AND UNUNDERSTANBALE BUGS !
					var textDiv=bubble.getTextDiv();

					var $textDiv=jQuery(textDiv);
					
					var $decryptedDivs=$textDiv.find(".cryptographied[data-encrypted='false']");
					var hasElementsToEncrypt = (0<$decryptedDivs.get().length);
					if(hasElementsToEncrypt){
						// TRACE
						log("WARN : A quantity of "+$decryptedDivs.get().length+" content elements have been detected to have not completed their encryption. That is to say they are marked has 'has to be encrypted' but have not been encrypted. Your model is incorrect, for this is not normal. You should re-save each of their enclosing bubbles and then save your model in order to complete their encryption process.")
					}
					
					var $encryptedDivs=$textDiv.find(".cryptographied[data-encrypted='true']");
					var hasElementsToDecrypt = (0<$encryptedDivs.get().length);
					if(!hasElementsToDecrypt)	return;

					this.addToggleEncryptionButtonsOnBubble(bubble);
					
				}

				
				/*private*/addToggleEncryptionButtonsOnBubble(bubble){
					var containerBubbleDiv=bubble.getDiv();
					var $containerBubbleDiv=jQuery(containerBubbleDiv);

					var textDiv=bubble.getTextDiv();
					var $textDiv=jQuery(textDiv);

					var bubbleName=bubble.getName();
					var $controlsDiv=jQuery(".cryptographyControls#cryptographyControls_"+bubbleName);

					/*private inner function*/const updateLabel=function(decryptEncryptButton){
						
						let textDiv=bubble.getTextDiv();
						
						var $encryptedDivs=$textDiv.find(".cryptographied[data-encrypted='true']");
						var $decryptedDivs=$textDiv.find(".cryptographied[data-encrypted='false']");
						var hasElementsToDecrypt = (0<$encryptedDivs.get().length);
						var hasElementsToEncrypt = (0<$decryptedDivs.get().length);
						
						
						if(hasElementsToDecrypt){
							decryptEncryptButton.innerHTML="DECRYPT";
						}else if(hasElementsToEncrypt){
							decryptEncryptButton.innerHTML="ENCRYPT";
						}
						
					};
					
					var decryptEncryptButton=$containerBubbleDiv.find("#cryptographyControls_"+bubbleName+">.decryptEncryptButton").get(0);
					var $encryptedOrDecryptedDivs=$containerBubbleDiv.find(".cryptographied");
					
					if($controlsDiv.get().length<=0 && 0<$encryptedOrDecryptedDivs.get().length){

						var controlsDiv=document.createElement("span");
						controlsDiv.id="cryptographyControls_"+bubbleName;
						controlsDiv.className="cryptographyControls";
						controlsDiv.style.position="sticky";
						controlsDiv.style.top="0";
						controlsDiv.style.float="right";
						containerBubbleDiv.insertBefore(controlsDiv, containerBubbleDiv.firstChild);
						
						// encrypt / decrypt button :
						decryptEncryptButton=document.createElement("button");
						decryptEncryptButton.className="decryptEncryptButton";
						controlsDiv.appendChild(decryptEncryptButton);
						
						decryptEncryptButton.onclick=function(){
							
							var $containerBubbleDiv=jQuery(containerBubbleDiv);

							var $encryptedDivs=$textDiv.find(".cryptographied[data-encrypted='true']");
							var $decryptedDivs=$textDiv.find(".cryptographied[data-encrypted='false']");
							var hasElementsToDecrypt = (0<$encryptedDivs.get().length);
							var hasElementsToEncrypt = (0<$decryptedDivs.get().length);

							if(hasElementsToDecrypt){
								//DECRYPT
								
								promptWindow(
								 	 i18n({"fr":"Veuillez entrer le mot de passe de décryptage","en":"Please enter decrypting password"})
									,"password",
									"",function(p){
									if(nothing(p)) 	return;
								
								
									var $encryptedDivs=$textDiv.find(".cryptographied[data-encrypted='true']");
									$encryptedDivs.each(function(index){
	
										var $this=jQuery(this);
										var thisDiv=this;
										
										var $this=jQuery(thisDiv);

										$this.attr("data-encrypted",false);
										var contentSignature=$this.attr("data-signature");
										var encryptedContent=thisDiv.innerHTML;
										$this.attr("data-encryptedcontent",encryptedContent);
										
										var decryptedContent=decryptContent(encryptedContent,p)
										var signature=getHashedString(decryptedContent);

										if(contentSignature!==signature){
											//TRACE
											log(i18n({"fr":"WARN: Le mot de passe est incorrect.","en":"WARN: Password is incorrect."}));
											return;
										}
										
										$this.attr("data-signature",getHashedString(decryptedContent));
										$this.html(decryptedContent);
										
									});
									decryptEncryptButton.innerHTML="ENCRYPT";
										
								});
								

							}else if(hasElementsToEncrypt){
								//ENCRYPT
								
								$decryptedDivs.each(function(index){
									var $this=jQuery(this);
									var thisDiv=this;

									var encryptedContent=$this.attr("data-encryptedcontent");
									
									if(nothing(encryptedContent)){
										//TRACE
										log(i18n({"fr":"WARN: Aucun contenu précédement crypté trouvé.","en":"WARN: No previously encrypted content found."}));
										return;
									}
									
									$this.attr("data-encrypted",true);
									$this.html(encryptedContent);
									
								});
								decryptEncryptButton.innerHTML="DECRYPT";
							}
							
							
						};
						
//					// TODO : Develop....
//					// clue div :
//					var clueDiv=document.createElement("div");
//					controlsDiv.appendChild(clueDiv);
//					clueDiv.innerHTML="CLUE TEXT";
//					clueDiv.style="display:none";
//
//					// clue button :
//					var clueButton=document.createElement("button");
//					controlsDiv.appendChild(clueButton);
//					clueButton.innerHTML="CLUE";
//					clueButton.onclick=function(){
//						jQuery(clueDiv).toggle(500);
//					};
						
						
					}
					updateLabel(decryptEncryptButton);
					
				}

				/*private*/getAotraScreen(){
					if(this.aotraScreen)	return this.aotraScreen;
					return getAotraScreen();
				}
			
		}
		
		
		// Static methods :
		// Hooks use
		//	 	- Content encrypting in bubble content:
		
		/*static*/
		
		AotraScreen.hooks[INIT][CONTENT_ENCRYPTING_KEY]={
			initScreen:function(aotraScreen){
				// We have to init it even in no edition node :
				aotraScreen.bubbleContentEncryptingProvider=new BubbleContentEncryptingProvider();
			}
		};
		
		AotraScreenEditionProvider.hooks[INIT].push({
			initScreen:function(aotraScreen, editionProvider){
				editionProvider.snippetsHooks[CONTENT_ENCRYPTING_KEY]=aotraScreen.bubbleContentEncryptingProvider;
			}
		});
		
		AotraScreenEditionProvider.hooks[DO_ON_SAVE_EDIT].push({
			execute:function(aotraScreen,editionProvider){
				if(nothing(editionProvider) || nothing(aotraScreen.bubbleContentEncryptingProvider))
					return;
				aotraScreen.bubbleContentEncryptingProvider.doOnSaveBubble(editionProvider.getSelectedBubble());	
			}
			
		});
	
		Bubble.hooks[DO_ON_CREATE_TEXT_DIV].push({
			execute:function(aotraScreen,bubble){
				if(nothing(aotraScreen.bubbleContentEncryptingProvider))
					return;
				aotraScreen.bubbleContentEncryptingProvider.doOnContentRenderForBubble(bubble);	
			}
			
		});
		
	}
	
}

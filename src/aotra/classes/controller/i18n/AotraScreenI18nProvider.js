// GLOBAL CONSTANTS
	var DEFAULT_LANGUAGE="";

	
// DEPENDENCY : needs AotraScreen...
if(typeof initAotraScreenI18nProviderClass ==="undefined"){

	function initAotraScreenI18nProviderClass() {
	var initSelf = this;

	// CONSTANTS
	var DEFAULT_LANGUAGE_LABEL=config.aotraScreen.forActivePlane().defaultLanguageLabel?config.aotraScreen.forActivePlane().defaultLanguageLabel:"Default";
	var PLEASE_TRANSLATE_MESSAGE=i18n({en:"PLEASE TRANSLATE...",fr:"À TRADUIRE..."});
	var GLOBAL_SELECT_ID="selectGlobalLanguageSwitch";
	var SELECT_ID="selectLanguageForEditedBubble";
	
	initSelf.AotraScreenI18nProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {

			// Attributes
			this.aotraScreen=aotraScreen;
			
			// Processed attributes
			this.languages=new Array();
//			this.refreshAllDefaultLanguages();
			
			
			this.currentLanguage=DEFAULT_LANGUAGE;

			this.div=null;
			
//		this.initUI();

		}
	
		// Methods
		,initUI:function(){
	
			var mainDiv=this.aotraScreen.mainDiv;

			var POSITION_X=4;
			var POSITION_Y=22;
			
			this.div=document.createElement("div");
			mainDiv.appendChild(this.div);
			this.div.id="divGlobalLanguagesSwitch";
			this.div.className="buttonsHolder";
			this.div.setStyle("left:"+(POSITION_X)+"px; top:"+(POSITION_Y)+"px ;z-index:"+(MAX_Z_INDEX-1)+";");
			
			// We update the global language select :
			this.refreshGlobalLanguagesSelect();

		}
		
		// Accessors
		,getCurrentLanguage:function(){
			return this.currentLanguage;
		}
		
		/*private*/,setCurrentLanguage:function(language){
			var aotraScreen=this.aotraScreen;
			
			if(this.currentLanguage===language)	return;
			
			this.currentLanguage=language;
			
			
			var editionProvider=this.aotraScreen.editionProvider;

			if(editionProvider && editionProvider.isBubbleCurrentlyBeingEdited()
				&& editionProvider.getSelectedBubble() && editionProvider.getSelectedBubble().hasContent(language) ){
				var c=editionProvider.getSelectedBubble().getContent(language);
				
				editionProvider.commitContentToSelectedBubble(c);
				this.refreshEditBubbleLanguagesSelect(editionProvider.getSelectedBubble());
			}
			
			aotraScreen.drawAndPlace(true);

		}
		
		// Internationalization

		/*private*/,addNewLanguageForBubble:function(selectedBubble){
			
			var editionProvider=this.aotraScreen.editionProvider;
			
			
			var self=this;
			promptWindow("Please enter language label","textbox",this.getCurrentLanguage()
					,function(value){
				
				var languageName=value;

				if(languageName===DEFAULT_LANGUAGE 
				|| languageName===DEFAULT_LANGUAGE_LABEL		
				|| contains(selectedBubble.getLanguages(),languageName)){
					
					//TRACE
					alert(i18n({fr:"La langue «"+languageName+"» existe déjà."
							   ,en:"Language «"+languageName+"» already exists."}));
					
					return;
				}
				
				editionProvider.commitContentToSelectedBubble();
				
				if(!contains(self.languages,languageName)){
					self.languages.push(languageName);
				}
				
				// We initialize new content to default language : 
				selectedBubble.setContent(PLEASE_TRANSLATE_MESSAGE+" "+selectedBubble.getContent(DEFAULT_LANGUAGE),languageName);
				
				self.setCurrentLanguage(languageName);
				self.refreshEditBubbleLanguagesSelect(editionProvider.getSelectedBubble());
				self.refreshGlobalLanguagesSelect();
				
			});

			
		}

		/*private*/,removeLanguageForBubble:function(selectedBubble,languageName){

			if(!languageName)	return;
			
			if(languageName===DEFAULT_LANGUAGE
			|| languageName===DEFAULT_LANGUAGE_LABEL){
				alert("You cannot delete default language content.");
				return;
			}
			
			if(!confirm("Are you sure you want to remove language «"+languageName+"» ? All content in this bubble with this language will be lost."))	return;
			
//			var editionProvider=this.aotraScreen.editionProvider;
			
			if(languageName===DEFAULT_LANGUAGE)	return;
			
			if(contains(this.languages,languageName))	remove(this.languages,languageName);

			selectedBubble.removeContent(languageName);
			
			this.refreshEditBubbleLanguagesSelect(selectedBubble);
			
			this.refreshGlobalLanguagesSelect();
			if(this.getCurrentLanguage()===languageName){
				this.setCurrentLanguage(DEFAULT_LANGUAGE);
			}
			
		}

		
		/*public*/,refreshAllDefaultLanguages:function(){
			var aotraScreen=this.aotraScreen;
			
			if(!contains(this.languages,DEFAULT_LANGUAGE))	this.languages.push(DEFAULT_LANGUAGE);
			
			for(var k=0;k<aotraScreen.getPlanes().length;k++){
				var plane=aotraScreen.getPlanes()[k];
				for(var i=0;i<plane.getBubbles().length;i++){
					var b=plane.getBubbles()[i];
					var bubbleLanguages=b.getLanguages();
					if(bubbleLanguages.length<2)	continue;
					this.languages=merge(this.languages,bubbleLanguages);
				}
			}
			
			this.initUI();
			
		}
		
		
		/*private*/,refreshGlobalLanguagesSelect:function(){
			var isMoreThanOneLanguage=(this.languages.length>1);
			
			setElementVisible(this.div,isMoreThanOneLanguage);
			
			// We read the eventual aotra-core set language (used for UI translations)
			if(isMoreThanOneLanguage && !nothing(window.language) && contains(this.languages,window.language)){
				this.currentLanguage=window.language;
			}
			
			this.div.innerHTML=AotraScreenI18nProvider.renderLanguagesSwitch(this.languages,this.getCurrentLanguage(),GLOBAL_SELECT_ID);
		}
		
		/*public*/,refreshEditBubbleLanguagesSelect:function(selectedBubble){
			var select=document.getElementById(SELECT_ID);
			if(!select)	return;
			var languages=selectedBubble.getLanguages();
			
			select.innerHTML=AotraScreenI18nProvider.renderLanguagesOptions(languages,this.getCurrentLanguage());
		}
	
	});
	
	// static methods :
	
	
	/*private*/initSelf.AotraScreenI18nProvider.renderLanguagesOptions=function(languages,defaultSelectedLanguage){
		
		var html="";
		
		for(var i=0;i<languages.length;i++){
			var l=languages[i];
			html+="<option value='"+l+"' ";
			html+=(l===defaultSelectedLanguage?" selected ":"");
			html+=" >";
			html+=(l===DEFAULT_LANGUAGE?DEFAULT_LANGUAGE_LABEL:l);
			html+="</option>";
		}
		
		return html;
	};
	
	/*private*/initSelf.AotraScreenI18nProvider.renderLanguagesSwitch=function(/*NULLABLE*/languages,/*NULLABLE*/defaultSelectedLanguage,/*OPTIONAL*/selectId){
		
		var onchangeStr="if(getAotraScreen().editionProvider) getAotraScreen().editionProvider.commitContentToSelectedBubble();"
					   +"getAotraScreen().i18nProvider.setCurrentLanguage(getSelectedValueInSelect(this));";
		
		var html="";
		html+="<select style='width:44px;display:table-cell;vertical-align:top;' onchange=\""+onchangeStr+"\" "+(selectId?(" id=\""+selectId+"\" "):"")+" >";
		
		if(!empty(languages)){
			html+=AotraScreenI18nProvider.renderLanguagesOptions(languages,defaultSelectedLanguage?defaultSelectedLanguage:DEFAULT_LANGUAGE);
		}
		
		html+="</select>";
		
		
		return html;
	};
	
	
	/*public*/
	initSelf.AotraScreenI18nProvider.getGeneratedHTMLInputs=function(){

		var mainDivInnerHTML="";
		
		mainDivInnerHTML+="<div style='display:table;'>";
		
		mainDivInnerHTML+="<label style='text-transform:capitalize;display:table-cell;vertical-align:top;'>"+i18n({"fr":"langue","en":"language"})+":</label>";
		mainDivInnerHTML+=AotraScreenI18nProvider.renderLanguagesSwitch(null,null,SELECT_ID);
		mainDivInnerHTML+="<button class='icon plusBtn' " 
			+" onclick=\"getAotraScreen().i18nProvider.addNewLanguageForBubble(getAotraScreen().editionProvider.getSelectedBubble());\" " 
			+" title='"+i18n({"fr":"Ajouter une langue...","en":"Add a language"})+"' ></button>";

		mainDivInnerHTML+="<button class='icon minusBtn' " 
			+" onclick=\"getAotraScreen().i18nProvider.removeLanguageForBubble(getAotraScreen().editionProvider.getSelectedBubble(),getSelectedValueInSelect('"+SELECT_ID+"'));\" " 
			+" title='"+i18n({"fr":"Enlever une langue...","en":"Remove a language"})+"' ></button>";

		
		mainDivInnerHTML+="</div>";
		
		
		return mainDivInnerHTML;
	};

	
	// STATIC CODE :
	// Hooks use
	/*static*/{
		
		var editorHook=new Object();
		AotraScreenEditionProvider.hooks[DO_ON_OPEN_EDIT].push(editorHook);
		editorHook.execute=function(aotraScreen,editionProvider){
			if(!aotraScreen.i18nProvider)	return;
			aotraScreen.i18nProvider.refreshEditBubbleLanguagesSelect(editionProvider.getSelectedBubble());	
		};

	};
	
	

	return initSelf;
}
}
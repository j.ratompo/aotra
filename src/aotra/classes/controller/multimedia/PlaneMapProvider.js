// GLOBAL CONSTANTS
	var MAP="map{";
	var DIV_MAP_CONTAINER_ID="divMapContainer";
	var MAP_MARKER_TAG_NAME="mapmarker";
	var MARKER_CLASS_NAME="marker";
	
// Resources : www.w3schools.com/googleAPI/google_maps_basic.asp ; https://developers.google.com/maps/documentation/javascript/styling?hl=fr#styling_the_default_map
// Another possible track (ABANDONNED): www.mapsjs.com/intro.html

	// DEPENDENCY : AOTRASCREEN EDITION PROVIDER
	// DEPENDENCY : PLANE VISIBILITY LAYERS PROVIDER

	
if(typeof initPlaneMapProviderClass ==="undefined"){
	

function initPlaneMapProviderClass() {
	var initSelf = this;

	// CONSTANTS
	var GOOGLE_ZOOM_DEFAULT_VALUE=10;
	var GOTO_BUBBLE_LABEL="Details...";
	var DEFAULT_COLOR="#44FF44";
	var DEFAULT_MAP_SATURATION=-20;
	var DEFAULT_INFO_WINDOW=true;
	var MAX_ICON_WIDTH=200;
	var MAX_ICON_HEIGHT=200;
	var AUTO="auto";
	var MAP_MARKER_ON_CLICK_ZOOM_LEVEL=15;
	
	initSelf.PlaneMapProvider = Class.create({

		// Constructor
		initialize : function(plane,
				/*OPTIONAL*/color,/*OPTIONAL*/infoWindow,/*OPTIONAL*/defaultMarkerIconURL,/*OPTIONAL*/mapStyleConfigStr,
				/*OPTIONAL*/zoomLevel,/*OPTIONAL*/startingLocation) {

			// Attributes
			this.plane = plane;
			this.color=color && !nothing(color,true)?color:
						(config.map.forPlane(plane).color 
						&& !nothing(config.map.forPlane(plane).color)?config.map.forPlane(plane).color:DEFAULT_COLOR);
			this.infoWindow=infoWindow && !nothing(infoWindow)?infoWindow.toLowerCase()==="true":DEFAULT_INFO_WINDOW;
			this.defaultMarkerIconURL=defaultMarkerIconURL;
			this.zoomLevel=zoomLevel;
			this.startingLocation=startingLocation;
			this.mapStyleConfigStr=mapStyleConfigStr;
			
			// An associative array for holding ("<bubble name>",<marker>) couples:
			this.markers=new Array();
			
			
			// Initialization
			this.initMap(plane.getAotraScreen());

			
			// Hooks using :
			var self=this;
			/*static*/var hook=function(aotraScreen,bubble){
				var activePlane=aotraScreen.getActivePlane();
				if(activePlane && activePlane.mapProvider){
					activePlane.mapProvider.doGotoBubbleMarkerLocation(bubble);
				}
			}
			AotraScreen.hooks[DO_ON_ARRIVAL_HOOK_NAME].push(hook);
			
		}
	
		// Methods
		,initMap:function(aotraScreen){
			
			var mapContainer=document.getElementById(DIV_MAP_CONTAINER_ID);
			if(mapContainer==null){
				var aotraScreenHolder=document.getElementById(AOTRA_SCREEN_DIV_ID)?document.getElementById(AOTRA_SCREEN_DIV_ID):document.body;
				
				mapContainer=document.createElement("div");
				mapContainer.id=DIV_MAP_CONTAINER_ID;
				var ADJUSTEMENT=10;
				mapContainer.setStyle("position: absolute;left:"+(ADJUSTEMENT)+"px;top:"+(ADJUSTEMENT)+"px;width: 100%;height: 100%;");
				mapContainer.setStyle("z-index:"+MIN_Z_INDEX+";overflow:hidden;");
				appendAsFirstChildOf(mapContainer,aotraScreenHolder);
				
			}
		
			var canvas=document.getElementById(MAIN_CANVAS_ID);
			canvas.setStyle("pointer-events:none;");
			
			var mainDiv=aotraScreen.mainDiv;
			mainDiv.setStyle("pointer-events:none;");

			// AotraScreen mouse events transfer :
			var clickableElement=mainDiv;
			var mapContainer=document.getElementById(DIV_MAP_CONTAINER_ID);
			if(mapContainer!=null){
				mapContainer.onclick=clickableElement.onclick;
				// UNUSED (for now...):				
				// mapContainer.onmousedown=clickableElement.onmousedown;
				// mapContainer.onmouseup=clickableElement.onmouseup;
			}
			
		
			// FOR GOOGLE MAPS :
			var self=this;
			try{
					// Map :
					window.initializeGoogleMap=function(){
			
						if(google==null){
							// TRACE
							log("WARN : Google Maps API could not be loaded");
							return;
						}
						
						self.DEFAULT_LOCATION=new google.maps.LatLng(48.856638,2.352241);// Paris is default result, if none is found...(because it's a very beautiful city...okay, actually that's only because I'm from this this place.)
						self.NOWHERE_LOCATION=new google.maps.LatLng(32.339941,-64.767151);// Bermuda Triangle (today's world's «/dev/null»)
			
						
						var mapOptions = {
							center:		self.DEFAULT_LOCATION,
							zoom:		GOOGLE_ZOOM_DEFAULT_VALUE,
							mapTypeId:	google.maps.MapTypeId.ROADMAP,
			//				disableDefaultUI:true,
							draggable: true,
							zoomControl: true,
							scrollwheel: true,
							disableDoubleClickZoom: true,
						};
						
						self.map=new google.maps.Map(mapContainer,mapOptions);
			
						// Map zoom level :
						if(!nothing(self.zoomLevel)){
							self.map.setZoom(self.zoomLevel);
						}
						
						// Map starting location :
						if(self.startingLocation){
							// LatLng coordinates :
							self.map.setCenter(self.startingLocation);
						}
						
						// Map graphical style :
						var dominantColor=self.color;
						// 		- default map styling :
						var stylesArray = parseJSON(
											"[ {'stylers' : [ {'hue' : '"+dominantColor+"'}, {'saturation' : '"+DEFAULT_MAP_SATURATION+"'} ]},"
					                		+"{'featureType' : 'road','elementType' : 'geometry','stylers' : [ {'lightness' : '100'}, {'visibility' : 'simplified'} ]},"
					                		+"{'featureType' : 'road','elementType' : 'labels',  'stylers' : [ {'visibility' : 'on'} ]} ]");
						if(!empty(this.mapStyleConfigStr)){
							stylesArray = parseJSON(this.mapStyleConfigStr);
						}
						self.map.setOptions({styles: stylesArray});
						
						
					};
					
		
				// We initialize the google map implementation :
				jQuery(document).ready(window.initializeGoogleMap);
				
						
			}catch(e){
		
				// TRACE
				log(e);
				log("WARN : Maps sub-system could not be loaded, falling in black aotraScreen background mode...");
				mapContainer.setStyle("background: #4C4C4C;background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMTAwJSI+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjNGM0YzRjIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTIlIiBzdG9wLWNvbG9yPSIjNTk1OTU5IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMjUlIiBzdG9wLWNvbG9yPSIjNjY2NjY2IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMzklIiBzdG9wLWNvbG9yPSIjNDc0NzQ3IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNTAlIiBzdG9wLWNvbG9yPSIjMmMyYzJjIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNTElIiBzdG9wLWNvbG9yPSIjMDAwMDAwIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNjAlIiBzdG9wLWNvbG9yPSIjMTExMTExIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNzYlIiBzdG9wLWNvbG9yPSIjMmIyYjJiIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iODQlIiBzdG9wLWNvbG9yPSIjMWMxYzFjIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iIzhjOGM4YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);background: -moz-linear-gradient(-45deg,  #4c4c4c 0%, #595959 12%, #666666 25%, #474747 39%, #2c2c2c 50%, #000000 51%, #111111 60%, #2b2b2b 76%, #1c1c1c 84%, #8c8c8c 100%);background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#4c4c4c), color-stop(12%,#595959), color-stop(25%,#666666), color-stop(39%,#474747), color-stop(50%,#2c2c2c), color-stop(51%,#000000), color-stop(60%,#111111), color-stop(76%,#2b2b2b), color-stop(84%,#1c1c1c), color-stop(100%,#8c8c8c));background: -webkit-linear-gradient(-45deg,  #4c4c4c 0%,#595959 12%,#666666 25%,#474747 39%,#2c2c2c 50%,#000000 51%,#111111 60%,#2b2b2b 76%,#1c1c1c 84%,#8c8c8c 100%);background: -o-linear-gradient(-45deg,  #4c4c4c 0%,#595959 12%,#666666 25%,#474747 39%,#2c2c2c 50%,#000000 51%,#111111 60%,#2b2b2b 76%,#1c1c1c 84%,#8c8c8c 100%);background: -ms-linear-gradient(-45deg,  #4c4c4c 0%,#595959 12%,#666666 25%,#474747 39%,#2c2c2c 50%,#000000 51%,#111111 60%,#2b2b2b 76%,#1c1c1c 84%,#8c8c8c 100%);background: linear-gradient(135deg,  #4c4c4c 0%,#595959 12%,#666666 25%,#474747 39%,#2c2c2c 50%,#000000 51%,#111111 60%,#2b2b2b 76%,#1c1c1c 84%,#8c8c8c 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4c4c4c', endColorstr='#8c8c8c',GradientType=1 );");
			}
		}
		

	
	
		// Map methods :
		/*public*/,refreshMapMarkersAfterBubbleContent:function(bubble){ // Must always be called after DOM is fully loaded (ie. in a jQuery(document).ready(function(){...}); )
			var self=this;
			
			// Only one marker by bubble allowed !
			var marker=this.getMarkers()[bubble.getName()];
			if(marker!=null)	this.removeMarker(marker);

			
			var ms=this.getMarkerSelectorForBubble(bubble);
			if(!ms || empty(ms.get()))	return;
			
			
			var element=ms.get()[0];
			if(!element)			return;
			
			var markerContentHTML=element.innerHTML;
			// «marker»-CSS class elements parameters : 
			var addressStr=element.getAttribute("data-address");
			var locationStr=element.getAttribute("data-location");
			var markerIconURL=element.getAttribute("data-icon");
			
			// We place once the edit button, that was eventually deleted by bubble content refresh :
			self.appendQuickEditButton(bubble);

			if((!addressStr || nothing(addressStr)) && (!locationStr || nothing(locationStr)))	return;
			
			var location=null;
			if(locationStr && !nothing(locationStr)){
				location=getMapPositionFromLatLngStr(locationStr);
			}

			
			var centerMapOnLocationAndCloseBubble=function(location,bubble){
				if(!location)	return;
				self.map.setCenter(location);
				self.map.setZoom(MAP_MARKER_ON_CLICK_ZOOM_LEVEL);
				bubble.doClose();
			};
			
			//--------------------------------------------------------------------------------------------------
			// CAUTION : BECAUSE THERE IS AN AJAX CALL, THIS BLOCK MUST REMAIN LAST MEMBER OF ITS PARENT!
			// FOR THIS REASON WE CANNOT BREAK THIS BLOCK OF CODE...:
			// If we only have an address, we fetch a location.
			if(nothing(location)){
				if(!nothing(addressStr)){
					
					// Ajax call :
					PlaneMapProvider.getMapPositionFromLiteralAddress(addressStr,1,function(results){
						var firstFoundResult=results[0];

						if(!empty(results) && firstFoundResult){
							location=firstFoundResult;
							
							// We append found coordinates to element and update bubble content:
							var oldHTML=toOneSimplifiedLine(element.outerHTML,"compactHTML");
							element.setAttribute("data-location",location.lat()+" "+location.lng());
							var newHTML=element.outerHTML;// No need to compact HTML here...
							
							// We update bubble's content :
							var content=toOneSimplifiedLine(bubble.getContent(),"compactHTML");
							bubble.setContent(content.replace(oldHTML,newHTML));

							
							var ms=self.getMarkerSelectorForBubble(bubble);
							if(ms && !empty(ms.get())){
							
								// Repeated section of code :
								// A direct placing :
								ms.click(function(){
									centerMapOnLocationAndCloseBubble(location,bubble);
								});
								self.addMarker(bubble,location,markerContentHTML,markerIconURL);
							}
							
						}else{
							//TRACE
							log("WARN : No location found for address «"+addressStr+"».");
						
						}
						
						// We place again the edit button, that was eventually deleted by bubble content refresh :
						self.appendQuickEditButton(bubble);
						
					});
					
					return;
				}
				location = this.NOWHERE_LOCATION;
			}
			
			
			// Repeated section of code :
			// A direct placing :
			ms.click(function(){
				centerMapOnLocationAndCloseBubble(location,bubble);
			});
			self.addMarker(bubble,location,markerContentHTML,markerIconURL);
			//--------------------------------------------------------------------------------------------------


		}
		
		
		/*private*/,appendQuickEditButton:function(bubble){
		
			var self=this;
			
			// SINCE BUBBLE CONTENT MAY HAVE CHANGED OR HAVE BEEN RENEWED (AND RENDERED WITH placeDiv() FUNCTION),
			// THEN THE JQUERY SELECTOR MUST BE REFRESHED ! :
			var ms=this.getMarkerSelectorForBubble(bubble);
			if(!ms || empty(ms.get()))	return;
			
			var element=ms.get()[0];
			if(!element)			return;
			
			
			var markerContentHTML=element.innerHTML;
			// «marker»-CSS class elements parameters : 
			var addressStr=element.getAttribute("data-address");
			var markerIconURL=element.getAttribute("data-icon");
			
			
			var mustAppendQuickEditButton=
							empty(ms.parent().find(" .markerEditButton").get()) 
							&& getAotraScreen().isCalculatedEditable()
//							&& !getAotraScreen().editionProvider.isBubbleCurrentlyBeingEdited()
							;
			

			// A direct edit button : (only out of the edition window)
			if(mustAppendQuickEditButton){
				
			
				ms.parent().prepend("<button class='icon editBtn markerEditButton' title='"+i18n({"fr":"Éditer","en":"Edit"})+"'></button>");//&#9998;
				ms.parent().find(" .markerEditButton").click(function(){
					
					var markerContentHTMLLocal=toOneSimplifiedLine(markerContentHTML).replace(/\"/gim,"'");
					
					var SPECIAL_SEPARATOR="@@@";
	
					promptWindow("Please edit this marker"
							,"form{'address':'textbox:Adresse','icone':'textbox:Icône','HTMLText':'textarea:Texte HTML'}"
							,addressStr+SPECIAL_SEPARATOR
							+(!nothing(markerIconURL)?markerIconURL:self.defaultMarkerIconURL)+SPECIAL_SEPARATOR
							+markerContentHTMLLocal
							,function(returnValue){
								if(nothing(returnValue))	return;
								var s=returnValue.split(SPECIAL_SEPARATOR);
								
								// We append found coordinates to element and update bubble content:
								var oldHTML=toOneSimplifiedLine(element.outerHTML,"compactHTML");
								element.setAttribute("data-address",s[0]);
								element.setAttribute("data-location",""); // Since we cannot edit the location directly, we make sure the system will ask Google for it after the address string.
								element.setAttribute("data-icon",s[1]);
								element.innerHTML=toOneSimplifiedLine(s[2],"compactHTML");
								var newHTML=element.outerHTML;
								
								// We update bubble's content :
								var content=toOneSimplifiedLine(bubble.getContent(),"compactHTML");
								bubble.setContent(content.replace(oldHTML,newHTML));
								
								// Then we refresh markers after this bubble content !
								self.refreshMapMarkersAfterBubbleContent(bubble);
								
					});
					
				
				});
				
			}
		}

		

		
		/*private*/,addMarker:function(bubble,location,markerContentHTML,/*OPTIONAL*/markerIconURL){
			
			var self=this;
//			var aotraScreen=this.plane.getAotraScreen();
			var plane=this.plane;
			var bubbleName=bubble.getName();
			
			var marker=this.getMarkerInstance(bubble,location,markerContentHTML);
			
		
			// Info window determination for this marker :
			if(this.infoWindow){
				var gotoBubbleJSCode="<br /><a onclick=\"javascript:if(getAotraScreen().getActivePlane().mapProvider) getAotraScreen().getActivePlane().mapProvider.doOnClickGotoBubble('"+bubbleName+"')\" style='cursor:pointer'>"+GOTO_BUBBLE_LABEL+"</a>";
				
				var infoWindowContent = markerContentHTML;
				if(nothing(markerContentHTML))	infoWindowContent="";
				
				var infoWindow = new google.maps.InfoWindow({
					content: "<div class='"+MARKER_CLASS_NAME+"Text'>"+infoWindowContent+gotoBubbleJSCode+"</div><br /><br />"
				});
				marker.infoWindow=infoWindow;
				google.maps.event.addListener(marker, "click", function() {
//					self.map.setCenter(location);
					infoWindow.open(self.map,marker);
				});
				
			}else{
				google.maps.event.addListener(marker, "click", function() {
					
					self.map.setCenter(location);
					
					var aotraScreen=getAotraScreen();
					var b=marker.bubble;
					if(!b)	return;
					aotraScreen.doGotoBubble(b,true);
					
					
				});
			}
			
			// Icon determination for this marker :
			var chosenIconURL=(this.defaultMarkerIconURL?this.defaultMarkerIconURL:null);
			if(markerIconURL===AUTO){
				// DEPENDENCY : PLANE VISIBILITY LAYERS PROVIDER
				if(plane.visibilityLayersProvider){
					var ic=plane.visibilityLayersProvider.getLastVisibleMarkerIcon(bubble);
					if(ic)	chosenIconURL=ic;
				}
			}else if(!nothing(markerIconURL)){
				chosenIconURL=markerIconURL;
			}
			
			if(chosenIconURL){

				// TODO : FIXME : Define a temporary loading image.
				var tempImage=null;
				asyncPreloadImage(chosenIconURL,function(tempImage){
					
					if(typeof tempImage==="undefined" || tempImage===null)	return;
					
					//If image is not found, we don't apply icon image:
					if(tempImage.width!=0){
						var size=new google.maps.Size(Math.min(tempImage.width,MAX_ICON_WIDTH),Math.min(tempImage.height,MAX_ICON_HEIGHT));
						var image = {
								url: chosenIconURL,
								size: size,
								// The origin for this image is 0,0.
								origin: new google.maps.Point(0,0),
								// The anchor for this image is the base of the flagpole at 0,0.
								anchor: new google.maps.Point(0, 0)
						};
						marker.setIcon(image);
					}else{
						//TRACE
						log("WARN : Image «"+chosenIconURL+"» not found for marker icon.");
					}
					
				});
				
			}
			
			
			// We replace potentially already present markers in this plane map provider:
			this.getMarkers()[bubbleName]=marker;


		}
		
		/*private*/,doOnClickGotoBubble:function(bubbleName){
			var aotraScreen=getAotraScreen();
			var m=this.getMarkers()[bubbleName];
			var b=m.bubble;
			aotraScreen.doGotoBubble(b,true);
			m.infoWindow.close();
			
		}

		
		/*private*/,insertMarkerCodeInSelectedBubble:function(){
			var aotraScreen=getAotraScreen();
			if(!aotraScreen.getActivePlane().mapProvider)	return;
			
			// DEPENDENCY : AOTRASCREEN EDITION PROVIDER
			if(!aotraScreen.editionProvider)	return;
			var selectedBubble=aotraScreen.editionProvider.getSelectedBubble();
			if(!selectedBubble)	return;
			
			if( !empty(jQuery("#"+selectedBubble.div.id).find(" .marker").get())
				&& !confirm("This bubble already holds a marker div in its content. If you continue you will override it. Are you sure ?"))
				return;
			
				promptWindow("Please enter address :","textbox","",function(addressStr){

					if(!nothing(addressStr)){
						
						// Ajax call :
						PlaneMapProvider.getMapPositionFromLiteralAddress(addressStr,1,function(results){
							if(empty(results) || results[0]===null){
								alert("No location found for address «"+addressStr+"». Please retry another address.");
								return;
							}
							var firstFoundResult=results[0];
	
							// We have to first be sure to work  on the newest copy of bubble's content :
							aotraScreen.editionProvider.commitContentToSelectedBubble();
							aotraScreen.editionProvider.commitContentToSelectedBubble(
									 '<'+MAP_MARKER_TAG_NAME+'>'
									+'<div class="'+MARKER_CLASS_NAME+'" '
									+' data-address="'+(addressStr)+'" '
									+' data-location="'+firstFoundResult.lat()+' '+firstFoundResult.lng()+'" '
									+'>'+(addressStr)+'</div>'
									+'</'+MAP_MARKER_TAG_NAME+'><br /><br />'
									+selectedBubble.getContent());
							
	
						});
						
						return;
					}
					
				});
//			};

			
		}
	
		
		/*public*/,doGotoBubbleMarkerLocation:function(bubble){
			var marker=this.getMarkers()[bubble.getName()];
			if(!marker)	return;
			marker.centerMap();
		}
		
		// Utility methods :
		,removeAllOrphanMarkers:function(){
			
			var markers=copy(this.getMarkers());
			for(key in markers){
				if(!markers.hasOwnProperty(key))	continue;
				var m=markers[key];
				
				var b=m.bubble;
				var bs=this.getMarkerSelectorForBubble(b).get()[0];
				// It is enough, actually :
				if(!b || !bs ){
					remove(this.getMarkers(),m);
				}
			}
		}
		
		/*private*/,getMarkerSelectorForBubble:function(bubble){
			if(!bubble)	return null;
			var selector=jQuery("#"+bubble.div.id+" "+MAP_MARKER_TAG_NAME+" ."+MARKER_CLASS_NAME);
			return selector;
		}
	
		,removeMarker:function(marker){
			remove(this.getMarkers(),marker);
			marker.setMap(null);
		}
		
		,getMarkers:function(){
			return this.markers;
		}
		
		// -------------------------------------------------------------------------------
		// MARKER JAVASCRIPT PSEUDO-CLASS :
		/*private*/,getMarkerInstance:function(bubble,location,markerContentHTML){
			var self=this;

			// Attributes :
			var marker = new google.maps.Marker({
				position: location,
				map: self.map,
				title: getTextWordsExtract(removeHTML(markerContentHTML),15)
			});
			
			marker.bubble=bubble;
			marker.infoWindow=null;
			marker.icon=null;
			
			// Methods :
			marker.centerMap=function(){
				if(!marker.position)	return;
				marker.map.setCenter(marker.position);
				marker.map.setZoom(MAP_MARKER_ON_CLICK_ZOOM_LEVEL);
				if(marker.infoWindow)	marker.infoWindow.open(marker.map,marker);
			};

			return marker;
		}
		// -------------------------------------------------------------------------------

		
		
	});


	
	// static methods :
	/*public*/initSelf.PlaneMapProvider.getGeneratedHTMLInputsAddMarker=function(){
		var html="";
		html+="<button class='icon mapMarkerInsertBtn' onclick=\"javascript:if(getAotraScreen().getActivePlane().mapProvider) getAotraScreen().getActivePlane().mapProvider.insertMarkerCodeInSelectedBubble()\" title='"+i18n({"fr":"Ajouter un marqueur de carte","en":"Add map marker"})+"'></button>";//&#10037;
		return html;
	};
	
	// (asynchronous)
	initSelf.PlaneMapProvider.getMapPositionFromLiteralAddress=function(addressString,maxResults,callBackFunction){
		
		new google.maps.Geocoder().geocode({address: addressString},
			function(foundResults,status) {
				var tab=new Array();
				
				if(status!==google.maps.GeocoderStatus.OK || foundResults===null || empty(foundResults)){

					// TRACE
					log("ERROR : Google map error: Geocoder status:"+status);
					
				}else{
					for(var i=0;i<maxResults && i<foundResults.length;i++){
						var loc=foundResults[i].geometry.location;
						
						tab.push(new google.maps.LatLng(loc.lat(), loc.lng())); //LatLng
					}
				}
				callBackFunction(tab);
			});
	};
	
	
	
	
	
	
	
	return initSelf;
}
}
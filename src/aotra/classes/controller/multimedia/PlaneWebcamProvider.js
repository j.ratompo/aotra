// GLOBAL CONSTANTS
	var WEBCAM="webcam";


if(typeof initPlaneWebcamProviderClass ==="undefined"){
	

function initPlaneWebcamProviderClass() {
	var initSelf = this;

	// CONSTANTS
	const WEBCAM_DRAWING_REFRESHING_RATE=0.1;
//	const VIDEO_CAM_ID="videoWebcam";
	
	initSelf.PlaneWebcamProvider = Class.create({

		// Constructor
		initialize : function(plane) {

			// Attributes
			this.plane = plane;
			
			
			// Initialization
			
			// TRACE
			log("INFO : OK for plane«"+this.plane.name+"» to be drawn with special webcam method.");
			
			this.webcamHandler=getWebcamHandler(
//				VIDEO_CAM_ID
			);
			
			const pe=new PeriodicalExecuter(function() {
				getAotraScreen().draw();
			},WEBCAM_DRAWING_REFRESHING_RATE);  
			

			// Display image filter hook for special possible webcam vizualisations :
			this.filter=null;
			
//			// TODO : Make this configurable in plane tag !
//			this.isMirrored=true; // true is default
			
			
//			// DEBUG : TESTS ONLY :
//			this.filter=Filters.areaPixellationForAotraMarkFilter;

			
			// UNUSED FOR NOW :
//			// If needed, the aotraMark reader:
//			// TODO : Make this configurable in plane tag !
//			this.aotraMarkReader=new PlaneAotraMarkReaderForWebcamProvider();
//			if(this.aotraMarkReader)	this.filter=this.aotraMarkReader;
			
			
			// UNUSED FOR NOW :
			// Some possible filters : (cf. aotrautils.commons.js library)


			
			
			
		}
	
		// Methods
	
		// 	- webcam plane background drawing :
		,drawWebcam:function(ctx){
			
			ctx.save();  

			
			const self=this;
			
			const aotraScreen=self.plane.aotraScreen;
	
			if(!self.webcamHandler || !self.webcamHandler.isReady()) return;
	
			
			const image=self.webcamHandler.video;
	
			
			// SPECIAL DRAWING TREATMENTS:
			self.doSpecialTreatments(aotraScreen,ctx,image);
			
			
			ctx.restore();
			
		}
		
		// Webcam methods :
	
		,doSpecialTreatments:function(aotraScreen,ctx,imageObj){
			if(!this.filter){
				// TODO : FIXME: webcam image is stretched :
				//ctx.drawImage(imageObj,x,y,aotraScreen.getWidth(),aotraScreen.getHeight());
				return;
			}

			// Covering all aotrascreen :
//			var x=0;
//			var y=0;
//			var width=aotraScreen.getWidth();
//			var height=aotraScreen.getHeight();
			
			
			// Covering only the smallest size of image :
			var x=aotraScreen.getWidth()/2-imageObj.getWidth()/2;
			var y=aotraScreen.getHeight()/2-imageObj.getHeight()/2;
			var width=imageObj.getWidth();
			var height=imageObj.getHeight();

			
			// TODO : FIXME: webcam image is stretched :
			ctx.drawImage(imageObj,x,y,width,height);

			
			//ctx.drawImage(imageObj,x,y,imageObj.getWidth(),imageObj.getHeight());
	        //var imageData = ctx.getImageData(x, y, imageObj.getWidth(), imageObj.getHeight());
			
			var imageData;
//			if(!this.isMirrored)	
			imageData = ctx.getImageData(x, y, width, height);
//			else					imageData = mirrorImage(ctx.getImageData(x, y, width, height),width,height);

	        this.filter.filter(imageData,width, height);
			
	        var treatedImageData = imageData;
	        
	        // Overwrite original image if needed :
			// TODO : FIXME: webcam image is stretched :
			if(!empty(treatedImageData))       ctx.putImageData(treatedImageData, x, y);
	        
		}
	
	});

	return initSelf;
}
}
if(typeof initScreenExporterToXMLClass ==="undefined"){
function initScreenExporterToXMLClass() {
	var initSelf = this;

	// CONSTANTS
	
	
	initSelf.AotraScreenExporterToXML = Class.create({

		// Constructor
		initialize : function() {
			// Attributes
		
		}
	
		// Conversion javascript model to BBXML :
	
		// AotraScreen :
		,getXMLFromAotraScreen:function(aotraScreen){
			var aotraScreenXML="<aotraScreen";
	
			if(!nothing(aotraScreen.title))	aotraScreenXML+=" title=\""+aotraScreen.title+"\"";
			if(!nothing(aotraScreen.background))	aotraScreenXML+=" background=\""+aotraScreen.background+"\"";
			if(!nothing(aotraScreen.borderStyle))	aotraScreenXML+=" borderStyle=\""+aotraScreen.borderStyle+"\"";
			if(!nothing(aotraScreen.moveAnimation))	aotraScreenXML+=" moveAnimation=\""+aotraScreen.moveAnimation+"\"";
			if(!nothing(aotraScreen.navigationMode))	aotraScreenXML+=" navigationMode=\""+aotraScreen.navigationMode+"\"";
			if(!nothing(aotraScreen.editable))	aotraScreenXML+=" editable=\""+aotraScreen.editable+"\"";
			if(!nothing(aotraScreen.zoomingAnimation))	aotraScreenXML+=" zoomingAnimation=\""+aotraScreen.zoomingAnimation+"\"";
			if(!nothing(aotraScreen.startingPlaneName))	aotraScreenXML+=" startingPlaneName=\""+aotraScreen.startingPlaneName+"\"";
			//##if(!nothing(aotraScreen.voiceName))	aotraScreenXML+=" voiceName=\""+aotraScreen.voiceName+"\"";
			// (Search is activated by default in AotraScreen factory, so we want to use this attribute only to force it to not be activated :)
			if(!aotraScreen.searchProvider)	aotraScreenXML+=" searchable=\"false\"";
			if(!nothing(aotraScreen.serversURLs))	aotraScreenXML+=" serversURLs=\""+aotraScreen.serversURLs+"\"";
			if(!nothing(aotraScreen.speed) && aotraScreen.speed!==DEFAULT_SCREEN_SPEED)	aotraScreenXML+=" speed=\""+aotraScreen.speed+"\"";
			if(!nothing(aotraScreen.mobile))	aotraScreenXML+=" mobile=\""+aotraScreen.mobile+"\"";
			// (Filters are activated by default in AotraScreen factory, so we want to use this attribute only to force it to not be activated :)
			if(!(aotraScreen.canFilter))	aotraScreenXML+=" canFilter=\"false\"";
			// (AotraMarkink is activated by default in AotraScreen factory, so we want to use this attribute only to force it to not be activated :)
			if(!(aotraScreen.aotraMarking))	aotraScreenXML+=" aotraMarking=\"false\"";
			// (MiniMap is activated by default in AotraScreen factory, so we want to use this attribute only to force it to not be activated :)
			if(!(aotraScreen.miniMap))	aotraScreenXML+=" miniMap=\"false\"";
			
			// Plugins overriding management :
			for(key in window.globalPlugins){
				if(!window.globalPlugins.hasOwnProperty(key))	continue;
				aotraScreenXML+=window.globalPlugins[key].getXMLFromAotraScreenArgs(aotraScreen);
			}

			// Hooks
			// TODO : FIXME : migrate all external direct exportations to hooks exportations :
			var hooksExporters=AotraScreenExporterToXML.screenExporters;
			for(key in hooksExporters){
				if(!hooksExporters.hasOwnProperty(key))	continue;
				aotraScreenXML+=hooksExporters[key].getXML(aotraScreen);
			}
			
			aotraScreenXML+=">\n";

			for(var i=0;i<aotraScreen.getPlanes().length;i++){
				aotraScreenXML+="\n";
				var plane=aotraScreen.getPlanes()[i];
				aotraScreenXML+=this.getXMLFromPlane(aotraScreen,plane);
			}

			aotraScreenXML+="\n</aotraScreen>\n";

			return aotraScreenXML;
		}
		
		// Plane :
		,getXMLFromPlane:function(aotraScreen,plane){
			var planeXML="<plane";
			
			planeXML+=" name=\""+plane.name+"\"";
			if(!nothing(plane.parallaxBackground))		planeXML+=" parallaxBackground=\""+plane.parallaxBackground+"\"";
			if(!nothing(plane.simpleBackground))			planeXML+=" simpleBackground=\""+plane.simpleBackground+"\"";
			if(!nothing(plane.visibilityLayersConfig))	planeXML+=" visibilityLayersConfig=\""+plane.visibilityLayersConfig+"\"";
			if(!nothing(plane.visibilityLayersJunctionLogic) && plane.visibilityLayersJunctionLogic!==AND_JUNCTION_LOGIC)
				planeXML+=" junctionLogic=\""+plane.visibilityLayersJunctionLogic+"\"";
			if(!nothing(plane.fractionOfImageUsed))		planeXML+=" fractionOfImage=\""+plane.fractionOfImageUsed+"\"";
			if(!nothing(plane.linksButtonsDisplayStrategy))	planeXML+=" linksButtons=\""+plane.linksButtonsDisplayStrategy+"\"";
			
			// Hooks
			// TODO : FIXME : migrate all external direct exportations to hooks exportations :
			var hooksExporters=AotraScreenExporterToXML.planeExporters;
			for(key in hooksExporters){
				if(!hooksExporters.hasOwnProperty(key))	continue;
				planeXML+=hooksExporters[key].getXML(plane);
			}
			
			planeXML+=">\n";
			
			
			// Configuration
			if(!nothing(plane.configuration)){
				var configuration=plane.configuration;
				planeXML+="<config>\n";
				planeXML+=configuration;
				planeXML+="\n</config>\n";
			}
			
			// Bubbles
			for(var i=0;i<plane.getBubbles().length;i++){
				var bubble=plane.getBubbles()[i];
				// We only want root bubbles :
				if(bubble.parentBubble)	continue;
				planeXML+=this.getXMLFromBubble(aotraScreen,bubble);
				planeXML+="\n";
			}
			
			planeXML+="\n</plane>\n\n";
			return planeXML;
		}
		
		// Bubble :
		,getXMLFromBubble:function(aotraScreen,bubble){			
			var bubbleXML="<bubble";
			bubbleXML+=" name=\""+bubble.getName()+"\"";

			var parentBubble=bubble.getParentBubble();
			bubbleXML+=" position=\""+bubble.canonicalX+" "+bubble.canonicalY+"\"";

			
			bubbleXML+=" size=\""+bubble.maxW+" "+bubble.maxH+"\"";
			
			if(!nothing(bubble.getMold()))	bubbleXML+=" mold=\""+bubble.getMold()+"\"";
			if(!nothing(bubble.fontSize))	bubbleXML+=" fontSize=\""+bubble.fontSize+"\"";
			if(!nothing(bubble.color))	bubbleXML+=" color=\""+bubble.color+"\"";
			if(!nothing(bubble.borderStyle))	bubbleXML+=" borderStyle=\""+bubble.borderStyle+"\"";
			if(!nothing(bubble.openCloseAnimation))	bubbleXML+=" openCloseAnimation=\""+bubble.openCloseAnimation+"\"";
			if(!bubble.isOpenableClosable())	bubbleXML+=" openableClosable=\"false\"";// TRUE BY DEFAULT
			if(!nothing(bubble.openedOnCreate))	bubbleXML+=" openedOnCreate=\""+bubble.openedOnCreate+"\"";
			if(!nothing(bubble.fixedStrategy))	bubbleXML+=" fixedStrategy=\""+bubble.fixedStrategy+"\"";
			//if(!nothing(bubble.isHomeBubble) && bubble.getIsHomeBubble()===true)	bubbleXML+=" homeBubble=\""+bubble.getIsHomeBubble()+"\""; // FALSE BY DEFAULT
			if(bubble.getIsHomeBubble())	bubbleXML+=" homeBubble=\""+bubble.getIsHomeBubble()+"\""; // FALSE BY DEFAULT
			if(bubble.getIsExcludingText())	bubbleXML+=" excludeText=\""+bubble.getIsExcludingText()+"\""; // FALSE BY DEFAULT
			if(!nothing(bubble.zIndex))	bubbleXML+=" zIndex=\""+bubble.zIndex+"\"";
			if(!nothing(bubble.visibilityLayers))	bubbleXML+=" visibilityLayers=\""+bubble.visibilityLayers+"\"";
			if(!nothing(bubble.parallaxBackground))	bubbleXML+=" parallaxBackground=\""+bubble.parallaxBackground+"\"";
			
			// Hooks
			// TODO : FIXME : migrate all external direct exportations to hooks exportations :
			var hooksExporters=AotraScreenExporterToXML.bubbleExporters;
			for(key in hooksExporters){
				if(!hooksExporters.hasOwnProperty(key))	continue;
				bubbleXML+=hooksExporters[key].getXML(bubble);
			}
			
			bubbleXML+=">\n";

			var contents=bubble.getContents();
			for(keyLanguage in contents){
				if(!contents.hasOwnProperty(keyLanguage))	continue;
				
				var content=toOneSimplifiedLine(contents[keyLanguage],"compactHTML");
				bubbleXML+="<content";
				if(keyLanguage!==DEFAULT_LANGUAGE)
					bubbleXML+=" language=\""+keyLanguage+"\"";
				bubbleXML+=">\n";
				bubbleXML+=content;
				bubbleXML+="\n</content>\n";
			}
			
			if(!nothing(bubble.serverCode)){
				var serverCode=bubble.serverCode;
				bubbleXML+="<code>\n";
				bubbleXML+=serverCode;
				bubbleXML+="\n</code>\n";
			}

			
			
			
			bubbleXML+="\n";
			for(var i=0;i<bubble.getSubBubbles().length;i++){
				var subBubble=bubble.getSubBubbles()[i];
				bubbleXML+=this.getXMLFromBubble(aotraScreen,subBubble);
				bubbleXML+="\n";
			}
			
			bubbleXML+="\n</bubble>\n";
			return bubbleXML;
		}
		
		
	});
	
	
	
	
	// Static methods :
	
	// Hooks :
	{
		// TODO : FIXME : migrate all external direct exportations to hooks exportations :
		/*public*//*static*/initSelf.AotraScreenExporterToXML.screenExporters=new Object();
		/*public*//*static*/initSelf.AotraScreenExporterToXML.planeExporters=new Object();
		/*public*//*static*/initSelf.AotraScreenExporterToXML.bubbleExporters=new Object();
	};	

	return initSelf;
}
}

// GLOBAL, Static methods :
	/*public*/function seekActivePlaneNameFromAnchor(){
		var URLAnchor=getURLAnchor();
		if(!nothing(URLAnchor)){
			var splittedURLAnchor=URLAnchor.split(SEPARATOR_CHAR);
//			if(splittedURLAnchor.length>=2){
//				return splittedURLAnchor[1];
//			}
			// Format for anchor : #planeName,bubbleName(OPTIONAL)
			// We determine the plane to go to :
			if(splittedURLAnchor.length>=1){
				return splittedURLAnchor[0];
			}
		}
		return null;			
	}
	
	/*public*/function seekActivePlaneNameFromXMLModel(){
		
		var XMLCode=document.getElementById(MODEL_ID).textContent;
		var xmlText=normalizeXMLContentsTagsInBubbles(XMLCode);
		
		var xmlDoc = parseXMLString(xmlText);
		var eRoot=xmlDoc.documentElement;
		
		if(!eRoot	|| !empty(eRoot.getElementsByTagName("parsererror"))
					|| eRoot.nodeName=="encryptedAotraScreenContent")	return null;
		
		var eAotraScreen=eRoot.getElementsByTagName("aotraScreen")[0];
		if(!eAotraScreen)	return null;

		var aStartingPlaneName=eAotraScreen.attribute("startingPlaneName");
		if(!aStartingPlaneName || !aStartingPlaneName.value)	return null;

		return aStartingPlaneName.value;
	}


if(typeof initScreenImporterFromXMLClass ==="undefined"){
function initScreenImporterFromXMLClass() {
	var initSelf = this;

	// CONSTANTS
	var REGEXP_FOR_NAMES=new RegExp(".*\\s+.*","gim");
	
	
	initSelf.AotraScreenImporterFromXML = Class.create({

		// Constructor
		initialize : function() {
			// Attributes
			this.registeredLinks=new Array();
		
		}
	
		// Conversion BBXML to javascript model :
		,getAotraScreenFromXML:function(eAotraScreen,forceIsEditable){
			
			// Default values :
			var title=config.aotraScreen.forActivePlane(aotraScreen).title;
			var background=config.aotraScreen.forActivePlane(aotraScreen).background;
			var borderStyle=config.aotraScreen.forActivePlane(aotraScreen).borderStyle;
			var moveAnimation=config.aotraScreen.forActivePlane(aotraScreen).moveAnimation;
			var navigationMode=config.aotraScreen.forActivePlane(aotraScreen).navigationMode;
			//to enable planes different configurations for edition : var editable=config.aotraScreen.forActivePlane(aotraScreen).isEditable;
			var editable=config.aotraScreen.isEditable;
			var zoomingAnimation=config.aotraScreen.forActivePlane(aotraScreen).zoomingAnimation;
			var startingPlaneName="";
			//##var voiceName=config.aotraScreen.forActivePlane(aotraScreen).voiceName;
			// (Search is activated by default:)
			var searchable=(config.aotraScreen.forActivePlane(aotraScreen).searchable!==null && config.aotraScreen.forActivePlane(aotraScreen).searchable!=="true");
			var serversURLs="";// empty by default
			var speed=config.aotraScreen.forActivePlane(aotraScreen).speed;
			//var mobile=isDeviceMobile();// default value is automatically defined
			var mobile=IS_DEVICE_MOBILE_CALCULATED;// default value is automatically defined
			var canFilter=true;//default value is true
			var aotraMarking=true;//default value is true
			var miniMap=true;//default value is true
			var pluginsArgs=new Object();
			
			// title
			var aTitle=eAotraScreen.attributes["title"];
			if(aTitle && aTitle.value)	title=aTitle.value;

			// background
			var aBackground=eAotraScreen.attributes["background"];
			if(aBackground && aBackground.value)	background=aBackground.value;
			
			// borderStyle
			var aBorderStyle=eAotraScreen.attributes["borderStyle"];
			if(aBorderStyle && aBorderStyle.value)	borderStyle=aBorderStyle.value;
			
			// mode animation
			var aMoveAnimation=eAotraScreen.attributes["moveAnimation"];
			if(aMoveAnimation && aMoveAnimation.value)	moveAnimation=aMoveAnimation.value;

			// navigation mode
			var aNavigationMode=eAotraScreen.attributes["navigationMode"];
			if(aNavigationMode && aNavigationMode.value)	navigationMode=aNavigationMode.value;

			// is editable
			var aEditable=eAotraScreen.attributes["editable"];
			if(aEditable && aEditable.value)	editable=(aEditable.value==="true");
			
			// zooming animation
			var aZoomingAnimation=eAotraScreen.attributes["zoomingAnimation"];
			if(aZoomingAnimation && aZoomingAnimation.value)	zoomingAnimation=aZoomingAnimation.value;
			
			// starting plane
			var aStartingPlaneName=eAotraScreen.attributes["startingPlaneName"];
			if(aStartingPlaneName && aStartingPlaneName.value)	startingPlaneName=aStartingPlaneName.value;
			

			//##// aotraScreen voiceName
			//##var aVoiceName=eAotraScreen.attributes["voiceName"];
			//##if(aVoiceName && aVoiceName.value)
			//##	voiceName=aVoiceName.value;
			
			// is searchable
			var aSearchable=eAotraScreen.attributes["searchable"];
			if(aSearchable && aSearchable.value)	searchable=(aSearchable.value==="true");

			// available server URLS
			var aServersURLs=eAotraScreen.attributes["serversURLs"];
			if(aServersURLs && aServersURLs.value)
				serversURLs=aServersURLs.value;

			// speed configuration
			var aSpeed=eAotraScreen.attributes["speed"];
			if(aSpeed && aSpeed.value)
				speed=aSpeed.value;
			
			// mobile configuration
			var aMobile=eAotraScreen.attributes["mobile"];
			if(aMobile && aMobile.value)	mobile=aMobile.value;
			
			// ability to filter
			var aCanFilter=eAotraScreen.attributes["canFilter"];
			if(aCanFilter && aCanFilter.value)	canFilter=(aCanFilter.value==="true");
			
			// ability to aotraMark
			var aAotraMarking=eAotraScreen.attributes["aotraMarking"];
			if(aAotraMarking && aAotraMarking.value)	aotraMarking=(aAotraMarking.value==="true");
			
			// ability to have a mini-map
			var aMiniMap=eAotraScreen.attributes["miniMap"];
			if(aMiniMap && aMiniMap.value)	miniMap=(aMiniMap.value==="true");

			
			// Plugins overriding management :
			for(key in window.globalPlugins){
				if(!window.globalPlugins.hasOwnProperty(key))	continue;
				pluginsArgs[key]=window.globalPlugins[key].readFromXMLAotraScreenArgs(eAotraScreen);
			}
			
			// Hooks
			// TODO : FIXME : migrate all external direct importations to hooks importations :
			var hooksParameters=new Object();
			var hooksImporters=AotraScreenImporterFromXML.screenImporters;
			for(key in hooksImporters){
				if(!hooksImporters.hasOwnProperty(key))	continue;
				hooksExporters[key].importFromXML(hooksParameters,eAotraScreen);
			}
			
			
			// **********************************************
			
			var aotraScreen = new AotraScreen(
					 title,background,borderStyle,moveAnimation,navigationMode
					,editable,zoomingAnimation,startingPlaneName
					,searchable,serversURLs,speed
					,canFilter,aotraMarking,miniMap,hooksParameters
					,forceIsEditable,false,mobile,pluginsArgs);

			
			// -----------------------------------------------------------------------
			// Nothing of the above is done, if aotraScreen is overridden
			if(aotraScreen.isOverridden)	return aotraScreen;
			// -----------------------------------------------------------------------

			var ePlanes=eAotraScreen.getElementsByTagName("plane");
			for(var i=0;i<ePlanes.length;i++){
				var plane=this.getPlaneFromXML(aotraScreen,ePlanes[i]);
				aotraScreen.addPlane(plane);
			}
			
			
			// aotraScreen starting plane adjustment :
			// 		(anchor-specified plane overrides all)
			var anchoredPlane=aotraScreen.getPlaneByName(seekActivePlaneNameFromAnchor());
			if(anchoredPlane){
				aotraScreen.setActivePlane(anchoredPlane);
			}else{
				// aotraScreen starting plane name
				var aStartingPlaneName=eAotraScreen.attributes["startingPlaneName"];
				if(aStartingPlaneName && aStartingPlaneName.value)
					startingPlaneName=aStartingPlaneName.value;

				var startingPlane=null;
				startingPlane=aotraScreen.getPlaneByName(startingPlaneName);
				if(startingPlane!==null){
					aotraScreen.setActivePlane(startingPlane);
				}else if(!empty(aotraScreen.getPlanes())){
					aotraScreen.setActivePlane(aotraScreen.getPlanes()[0]);
				}else{
					// Screen has no plane to display, then we create a default one :
					var defaultPlane=getDefaultPlane(aotraScreen);
					aotraScreen.addPlane(defaultPlane);
					var defaultBubble=getDefaultBubble("#888888"
							,"<div style='text-align: center;'><strong><font face='helvetica'><br></font></strong></div><strong><font face='helvetica'><div style='text-align: center;'><strong><br></strong></div><div style='text-align: center;'><strong><font face='helvetica'>Nothing.</font></strong></div></font></strong>"
							,aotraScreen);
					defaultPlane.addBubbleToPlane(defaultBubble);
					// By default, we'll choose the first (and only) default plane as active plane :
					aotraScreen.setActivePlane(defaultPlane);
					
					// Treatments to do once plane is fully populated :
					defaultPlane.doAfterPlaneReady();

				}
			}
			
						
			// Links adding in all planes :
			var planes=aotraScreen.getPlanes();
			for(var j=0;j<planes.length;j++){
				var plane=planes[j];
				var rLinks=this.getRegisteredLinks();
				var rLinksForPlane=rLinks[plane.getName()];
				if(!rLinksForPlane || typeof rLinksForPlane==="undefined")	continue;
				for(var i=0;i<rLinksForPlane.length;i++){
					var link=rLinks[plane.getName()][i];
					
					plane.addLink(link);
				}
			}
			
			
			aotraScreen.doAfterAotraScreenPopulation();
			
			return aotraScreen;
		}
		
		
		/*private*/,getPlaneFromXML:function(aotraScreen,ePlane){
			
			var name="";
			var parallaxBackground="";
			var simpleBackground="";
			var visibilityLayersConfig="";
			var visibilityLayersJunctionLogic=null;
			var fractionOfImageUsed="";
			var linksButtonsDisplayStrategy="";
			var configuration="";
			
			// name
			var aName=ePlane.attributes["name"];
			if(!aName || !aName.value)	throw new Error("Plane must have a name.");
			name = aName.value;
			if(REGEXP_FOR_NAMES.test(name)) throw new Error("Plane name «"+name+"» is invalid : it must be a valid javascript variable identifier and no white spaces are allowed.");
			if(aotraScreen.getPlaneByName(name)!=null) throw new Error("A plane named «"+name+"» already exists within aotraScreen.");
	
			// parallax background
			var aParallaxBackground=ePlane.attributes["parallaxBackground"];
			if(aParallaxBackground && aParallaxBackground.value)	parallaxBackground=aParallaxBackground.value;

			// simple background
			var aSimpleBackground=ePlane.attributes["simpleBackground"];
			if(aSimpleBackground && aSimpleBackground.value)	simpleBackground=aSimpleBackground.value;

			// visibilityLayersConfig
			var aVisibilityLayersConfig=ePlane.attributes["visibilityLayersConfig"];
			if(aVisibilityLayersConfig && aVisibilityLayersConfig.value)	visibilityLayersConfig=aVisibilityLayersConfig.value;

			// visibilityLayersJunctionLogic
			var aVisibilityLayersJunctionLogic=ePlane.attributes["junctionLogic"];
			if(aVisibilityLayersJunctionLogic && aVisibilityLayersJunctionLogic.value)	visibilityLayersJunctionLogic=aVisibilityLayersJunctionLogic.value;
			
			// fractionOfImageUsed
			var aFractionOfImageUsed=ePlane.attributes["fractionOfImage"];
			if(aFractionOfImageUsed && aFractionOfImageUsed.value)	fractionOfImageUsed=aFractionOfImageUsed.value;

			// linksButtonsDisplayStrategy
			var aLinksButtonsDisplayStrategy=ePlane.attributes["linksButtons"];
			if(aLinksButtonsDisplayStrategy && aLinksButtonsDisplayStrategy.value)	linksButtonsDisplayStrategy=aLinksButtonsDisplayStrategy.value;

			// configuration
			var eConfigurations=ePlane.getElementsByTagName("config");
			if(!empty(eConfigurations)){
				// We only allow one :
				var eConfiguration=eConfigurations[0];
				if(!nothing(eConfiguration)){
					
					// CAUTION : Here we get the content of the config tag as an html tag content !
					// So additionnal normalizing things like double quotes may be added by browser :
					// So we cannot use this :	var c=(""+eConfiguration.textContent).trim();
					// because it would remove all HTML tags in the default content configuration !
					var c=(""+eConfiguration.innerHTML).trim();
					
					if(!empty(c)){
						configuration=c;
					}
				}
			}
			
			// Hooks
			// TODO : FIXME : migrate all external direct importations to hooks importations :
			var hooksParameters=new Object();
			var hooksImporters=AotraScreenImporterFromXML.planeImporters;
			for(key in hooksImporters){
				if(!hooksImporters.hasOwnProperty(key))	continue;
				hooksImporters[key].importFromXML(hooksParameters,ePlane);
			}
			
			var plane=new Plane(
					 name,parallaxBackground,simpleBackground
					,visibilityLayersConfig,visibilityLayersJunctionLogic,fractionOfImageUsed
					,linksButtonsDisplayStrategy
					,configuration
					,hooksParameters
					,aotraScreen
					);
			
			for(var i=ePlane.firstChild; i!=null ;i=i.nextSibling){
				if(i.toString()!=="[object Element]" || i.tagName!=="bubble")	continue;
				var eBubble=i;
				var bubble=this.getBubbleFromXML(aotraScreen,plane,eBubble);
				plane.addBubbleToPlane(bubble);
			}
			
			// Treatments to do once plane is fully populated :
			plane.doAfterPlaneReady();
			
			return plane;
		}
			
		/*private*/,getBubbleFromXML:function(aotraScreen,plane,eBubble){

			var bubbleName ="";
			var x =0;
			var y =0;
//			var diameter =config.bubble.forActivePlane(aotraScreen).size;
			var w=config.bubble.forActivePlane(aotraScreen).width;
			var h=config.bubble.forActivePlane(aotraScreen).height;

			var mold =config.bubble.forActivePlane(aotraScreen).mold;
			
//			var content =""; //(configured default content is only while creating new bubbles...)
			var contents =new Object();
			var serverCode="";
			
			var fontSize =config.bubble.forActivePlane(aotraScreen).fontSize;
			var color =config.bubble.forActivePlane(aotraScreen).color;
			var borderStyle =config.bubble.forActivePlane(aotraScreen).borderStyle;
			var openCloseAnimation =config.bubble.forActivePlane(aotraScreen).openCloseAnimation;
			var openableClosable = (config.bubble.forActivePlane(aotraScreen).openableClosable==="undefined"?
							true: (config.bubble.forActivePlane(aotraScreen).openableClosable!=="false"
								|| config.bubble.forActivePlane(aotraScreen).openableClosable!==false ) );
			var openedOnCreate =config.bubble.forActivePlane(aotraScreen).openedOnCreate;
			var fixedStrategy =config.bubble.forActivePlane(aotraScreen).fixedStrategy;
			var isHomeBubble=false;
			var isExcludingText=false;
			var zIndex=null;
			var visibilityLayers =config.bubble.forActivePlane(aotraScreen).visibilityLayers;
			var parallaxBackground=config.bubble.forActivePlane(aotraScreen).parallaxBackground;

			// bubbleName
			var aName=eBubble.attributes["name"];
			if(!aName || !aName.value)	throw new Error("Bubble must have a name.");
			bubbleName = aName.value;
			if(REGEXP_FOR_NAMES.test(bubbleName)) throw new Error("Bubble name «"+bubbleName+"» is invalid : it must be a valid javascript variable identifier and no white spaces are allowed.");
			if(plane.getBubbleByName(bubbleName)!=null) throw new Error("A Bubble named «"+bubbleName+"» already exists within plane «"+plane.name+"».");

			
			var aPosition=eBubble.attributes["position"];
			if(aPosition && aPosition.value){
				var positionStr=aPosition.value;
				var split=positionStr.trim().replace("\\s{2,}"," ").split(" ");
				
				var positionErrorMsg="Position attribute is malformed : expected «x y» or «orientation distance»";
				if(split.length!=2)	throw new Error(positionErrorMsg);
				if(!isNumber(split[1])) throw new Error(positionErrorMsg);

				if(isNumber(split[0])){
					x=parseFloat(split[0]);
					y=parseFloat(split[1]);
				}else{
					var point2D=this.getCartesianFromPolarPosition(positionStr);
					x=point2D["x"];
					y=point2D["y"];
				}
			}

//			// diameter
//			var eSize = eBubble.attributes["size"];
//			if (eSize && eSize.value) {
//				var trim=eSize.value.trim();
//				var split=trim.replace(new RegExp("\\s+","gim")," ").split(" ");
//				if(split.length>1){
//					//TODO : handle rectangle shape...
//				}else{
//					diameter = parseFloat(trim);
//				}
//			}
			// width
			var eSize = eBubble.attributes["size"];
			if (eSize && eSize.value) {
				var trim=eSize.value.trim();
				var split=trim.replace(new RegExp("\\s+","gim")," ").split(" ");
				if(1<split.length){
					//Rectangle shape...:
					w = parseFloat(split[0].trim());
					h = parseFloat(split[1].trim());
				}else{
					w = parseFloat(trim);
					h = w;
				}
			}

			
			// mold
			var aMold=eBubble.attributes["mold"];
			if(aMold && aMold.value)	mold = aMold.value;
			
			
			// contents
//			var eContent = eBubble.getElementsByTagName("content")[0];
//			if (eContent && eContent.textContent){
//				content = eContent.textContent;
//				this.registerLinksFromContent(aotraScreen,plane,bubbleName,content);
//			}
			
			var eContents=eBubble.getElementsByTagName("content");
			if(!empty(eContents)){

				for(var i=0;i<eContents.length;i++){
					var eContent = eContents[i];
					if(eContent.parentElement!==eBubble)	continue;
				
				// NOT WORKING :
//				for(var i=eBubble.firstChild;i!=null;i=i.nextSibling){
//					var eContent=i;

					var content ="";
					if (eContent && eContent.textContent){
						
						content = eContent.textContent;
						this.registerLinksFromContent(aotraScreen,plane,bubbleName,content);
	
						var language=DEFAULT_LANGUAGE;
						
						if(!empty(eContent.attributes)){
							var aLanguage=eContent.attributes["language"];
							if(aLanguage && aLanguage.value)	language = aLanguage.value;
						}
						
						contents[language]=content;
					}
				}
			}else{
				contents[DEFAULT_LANGUAGE]="";
			}
			if(typeof contents[DEFAULT_LANGUAGE]==="undefined"
//			|| typeof contents[window.DEFAULT_LANGUAGE_LABEL]==="undefined"
				){
				contents[DEFAULT_LANGUAGE]="";
			}
			
			
			// serverCode
			var eServerCodes=eBubble.getElementsByTagName("code");
			if(!empty(eServerCodes)){
				// We only allow one :
				var eServerCode=eServerCodes[0];
				if(!nothing(eServerCode)){
					var c=(""+eServerCode.textContent).trim();
					if(!empty(c)){
						serverCode=c;
					}
				}
			}
			
			
			
			// fontSize
			var aFontSize=eBubble.attributes["fontSize"];
			if(aFontSize && aFontSize.value)	fontSize = aFontSize.value;
			
			// color
			var aColor=eBubble.attributes["color"];
			if(aColor && aColor.value)	color = aColor.value;
			
			// borderStyle
			var aBorderStyle=eBubble.attributes["borderStyle"];
			if(aBorderStyle && aBorderStyle.value)	borderStyle = aBorderStyle.value;

			// openCloseAnimation
			var aOpenCloseAnimation=eBubble.attributes["openCloseAnimation"];
			if(aOpenCloseAnimation && aOpenCloseAnimation.value)	openCloseAnimation = aOpenCloseAnimation.value;

			// openableClosable (default value is true)
			var aIsOpenableClosable=eBubble.attributes["openableClosable"];
			if(aIsOpenableClosable && aIsOpenableClosable.value)	openableClosable = aIsOpenableClosable.value.toLowerCase()!=="false";// DEFAULT VALUE IS TRUE
			
			// openedOnCreate (default value seeked="true")
			var aOpenedOnCreate=eBubble.attributes["openedOnCreate"];
			if(aOpenedOnCreate && aOpenedOnCreate.value)	openedOnCreate = aOpenedOnCreate.value;
			// NO : //If is not openableClosable, then we force it open in create.
			// NO : if(!openableClosable)	openedOnCreate="true";
			
			// fixedStrategy (default value seeked="")
			var aFixedStrategy=eBubble.attributes["fixedStrategy"];
			if(aFixedStrategy && aFixedStrategy.value)	fixedStrategy = aFixedStrategy.value;

			// isHomeBubble
			var aIsHomeBubble=eBubble.attributes["homeBubble"];
			if(aIsHomeBubble && aIsHomeBubble.value)	isHomeBubble = (aIsHomeBubble.value==="true"); // DEFAULT VALUE IS FALSE

			// isExcludingText
			var aIsExcludingText=eBubble.attributes["excludeText"];
			if(aIsExcludingText && aIsExcludingText.value)	isExcludingText = (aIsExcludingText.value==="true"); // DEFAULT VALUE IS FALSE

			// zIndex
			var aZIndex=eBubble.attributes["zIndex"];
			if(aZIndex && aZIndex.value)	zIndex = aZIndex.value; // Default value is null

			// visibilityLayers
			var aVisibilityLayers=eBubble.attributes["visibilityLayers"];
			if(aVisibilityLayers && aVisibilityLayers.value)	visibilityLayers = aVisibilityLayers.value;

			// parallaxBackground
			var aParallaxBackground=eBubble.attributes["parallaxBackground"];
			if(aParallaxBackground && aParallaxBackground.value)	parallaxBackground = aParallaxBackground.value;
			
			// Hooks
			// TODO : FIXME : migrate all external direct importations to hooks importations :
			var hooksParameters=new Object();
			var hooksImporters=AotraScreenImporterFromXML.bubbleImporters;
			for(key in hooksImporters){
				if(!hooksImporters.hasOwnProperty(key))	continue;
				hooksImporters[key].importFromXML(hooksParameters,eBubble);
			}
			
			// AotraScreen :
			var aotraScreen=plane.aotraScreen;
			
			// Bubble arguments :
			// name,x,y,w,h
			//,mold,contents,fontSize
			//,openableClosable,color
			//,openCloseAnimation,borderStyle,openedOnCreate,fixedStrategy
			//,		isHomeBubble,isExcludingText,zIndex,hooksParameters
			//,		serverCode
			//,		visibilityLayers
			//,		parallaxBackground
			//,		aotraScreen
			
			var bubble = new Bubble(bubbleName,x,y,w,h
					,mold
					,contents
					,fontSize,openableClosable,color
					,openCloseAnimation,borderStyle,openedOnCreate
					,fixedStrategy,isHomeBubble,isExcludingText,hooksParameters
					,zIndex
					,serverCode,visibilityLayers,parallaxBackground,aotraScreen);
			
			// Sub-bubbles import :
			for(var i=eBubble.firstChild;i!=null;i=i.nextSibling){
				if(i.toString()!=="[object Element]" || i.tagName!=="bubble")	continue;
				var eSubBubble=i;
				var subBubble=this.getBubbleFromXML(aotraScreen,plane,eSubBubble);
				bubble.addBubble(subBubble);
			}
			
			return bubble;
		}
		
		
		/*private*/,registerLinksFromContent:function(aotraScreen,plane,bubbleName,contentParam) {

			var contentForXMLParsing = 
				("<root>"+
					(
					// To render single right quotes, 
					// we just double single quotes :
					contentParam.trim().replace(new RegExp("''","gim"), APOSTROPHE)
					)
				+"</root>");

			
			// We get absolutely all the xml elements links :
			var eLinksTexts=this.getELinksTexts(contentForXMLParsing); 
			for(var i=0;i<eLinksTexts.length;i++) {
				var eLinkText=eLinksTexts[i];
				var link=this.getLinkFromHTMLTagsInContent(aotraScreen,plane,bubbleName,eLinkText);
				if(!link)	continue;
				this.tryRegisteringLink(link);
			}
		}
		
		,getELinksTexts:function(contentParam){
			
			// We get absolutely all the xml elements links :
			
			var list=new Array();
			
			// We create a temporary div that we will remove once treatment is done :
			var rootContainerElement=document.getElementById(AOTRA_SCREEN_DIV_ID)?document.getElementById(AOTRA_SCREEN_DIV_ID):document.body;

			
			var tempDiv = document.createElement("div");
			rootContainerElement.appendChild(tempDiv);
			tempDiv.id="tempDivForParsing";
			tempDiv.innerHTML=contentParam;
			
			var elementsWithClass=getAllHTMLElementsWithClass(LINK_CSS_CLASS_NAME,tempDiv.id);
			
			for(var i=0;i<elementsWithClass.length;i++){
				var e=elementsWithClass[i];
				var to=e.getAttribute("data-to");
				// We ignore tags that do not provide minimal information :
				if(!to)	continue;
				// We ignore backwards-navigation tags, 
				// IMPORTANT : SO ONLY DIRECT NAVIGATION TAGS WILL ALLOW TO CREATE A LINK !:
				if(contains(e.className,LINK_CSS_CLASS_NAME_BACKWARDS))	continue;
				list.push(e);
			}
			
			
			// Removal of the temporary div created before treatment :
			rootContainerElement.removeChild(tempDiv);
			
			return list;
		}
		
		/*private*/,getLinkFromHTMLTagsInContent:function(aotraScreen,plane,fromBubbleName,eHtmlTagLink){

			// CAUTION !: «data-...» attributes values are read directly, not through a «.value» attribute !

			
			// source bubble name
			if(!fromBubbleName) {
				// TRACE
				log("WARN : Missing link source bubble or bubble name.");
				return null;
			}
			
			
			// destination bubble
			var toBubbleName=null;
			aToBubbleName= eHtmlTagLink.getAttribute("data-to");
			if(!aToBubbleName){
				// TRACE
				log("Missing link destination bubble name.");
				return null;
			}
			toBubbleName=aToBubbleName;
			if(fromBubbleName===toBubbleName){
//				// TRACE
//				log("Link cannot link the same bubble : fromBubbleName:«"+fromBubbleName+"» ; toBubbleName:«"+toBubbleName+"».");
				return null;
			}
			
			// label
			var label=config.link.forActivePlane(aotraScreen).label+"Link to bubble "+toBubbleName;
			if(eHtmlTagLink.innerHTML) label=eHtmlTagLink.innerHTML;
			
			// mold
			var size=config.link.forActivePlane(aotraScreen).size;
			var aSize=eHtmlTagLink.getAttribute("data-size");
			if(aSize) size=parseFloat(aSize);
			
			// mold
			var mold=config.link.forActivePlane(aotraScreen).mold;
			var aMold=eHtmlTagLink.getAttribute("data-mold");
			if(aMold) mold=aMold
		
			// color
			var color=config.link.forActivePlane(aotraScreen).color;
			var aColor=eHtmlTagLink.getAttribute("data-color");
			if(aColor) color=aColor;

//			// borderStyle
//			var borderStyle=config.link.forActivePlane(aotraScreen).color;
//			var aBorderStyle=eHtmlTagLink.getAttribute("data-borderStyle");
//			if(aBorderStyle) borderStyle=aBorderStyle;

			// routingStrategy
			var routingStrategy=config.link.forActivePlane(aotraScreen).routingStrategy;
			var aRoutingStrategy=eHtmlTagLink.getAttribute("data-routingStrategy");
			if(aRoutingStrategy) routingStrategy=aRoutingStrategy;
			
			// Hooks
			// TODO : FIXME : migrate all external direct importations to hooks importations :
			var hooksParameters=new Object();
			var hooksImporters=AotraScreenImporterFromXML.linkImporters;
			for(key in hooksImporters){
				if(!hooksImporters.hasOwnProperty(key))	continue;
				hooksExporters[key].importFromXML(hooksParameters,eHtmlTagLink);
			}
			
			// Link attributes :
			/*
			fromBubbleName,toBubbleName,planeName,
			color,size,mold,label
			,routingStrategy
			*/
			var link =new Link(
					fromBubbleName,toBubbleName,plane.getName()
					,color,size,mold,label
					,routingStrategy
					,hooksParameters); 
			
			return link;
		}
		
		/*private*/,tryRegisteringLink:function(link){
			// The first link found will be the one in the good direction,
			// all other link tags will be reversed navigation :
			if(this.isEquivalentLinkRegistered(link)===false){
				var rLinks=this.getRegisteredLinks();
				var planeName=link.planeName;
				if(!rLinks[planeName])	rLinks[planeName]=new Array();
				rLinks[planeName].push(link);
				return true;
			}
			return false;
		}
				
		
		// Utility methods
		/*private*/,isEquivalentLinkRegistered:function(link){
			var rLinks=this.getRegisteredLinks();
			for(planeName in rLinks){
				if(!rLinks.hasOwnProperty(planeName))	continue;
				for(var i=0;i<rLinks[planeName].length;i++){
					var l=rLinks[planeName][i];
					if(l.equals(link,planeName))	return true;
				}
				return false;
			}
			return false;
		}
		
		,getRegisteredLinks:function(){
			return this.registeredLinks;
		}

		
		,getCartesianFromPolarPosition:function(polarPositionStr){
			var split = polarPositionStr.split(" ");

			var angleDegrees = 0;
			var distance = 0;
			
			if (split.length == 2) {

				var orientation = split[0].trim();
				if(!isNumber(orientation)) {

					if (orientation.toLowerCase()=="n") {			angleDegrees = 0 - 90;
					} else if (orientation.toLowerCase()=="e") {	angleDegrees = 90 - 90;
					} else if (orientation.toLowerCase()=="s") {	angleDegrees = 180 - 90;
					} else if (orientation.toLowerCase()=="w") {	angleDegrees = 270 - 90;
					} else if (orientation.toLowerCase()=="ne") {	angleDegrees = 45 - 90;
					} else if (orientation.toLowerCase()=="se") {	angleDegrees = 135 - 90;
					} else if (orientation.toLowerCase()=="sw") {	angleDegrees = 225 - 90;
					} else if (orientation.toLowerCase()=="nw") {	angleDegrees = 305 - 90;
					}

				}else{
					angleDegrees = parseFloat(orientation);
				}
				
				if(!isNumber(distance)) {
					throw new Error("Polar position must have a numeric position.");
				}
				distance = parseFloat(split[1].trim());

			}

			var point2D = polarPositionDegreesToCartesianPosition(angleDegrees, distance);
			
			return point2D;
		}
		
	
		
	});
	
	// Hooks :
	{
		// TODO : FIXME : migrate all external direct importations to hooks importations :
		/*public*//*static*/initSelf.AotraScreenImporterFromXML.screenImporters=new Object();
		/*public*//*static*/initSelf.AotraScreenImporterFromXML.planeImporters=new Object();
		/*public*//*static*/initSelf.AotraScreenImporterFromXML.bubbleImporters=new Object();
		/*public*//*static*/initSelf.AotraScreenImporterFromXML.linkImporters=new Object();
	};
	

	return initSelf;
}
}

	// GLOBAL CONSTANTS
	const DEFAULT_SCREEN_SPEED="10;0.01";// First number is number of steps, second (OPTIONAL) is refreshing rate in seconds.
	const HEUROPTIC_MODE="heuroptic";
	
	// Hooks constants
	const INIT="init";
	const DO_ON_ARRIVAL_HOOK_NAME="doOnBubbleArrival";
	const INIT_AFTER_READY="initAfterReady";
	const MODEL_ID="xmlModel";	

	// DEPENDENCY : SCREEN ADVANCED NAVIGATION PROVIDER
	// DEPENDENCY : AotraScreenEditionProvider
	// DEPENDENCY : SCREEN UPLOAD PROVIDER
	// DEPENDENCY : SCREEN MINIMAP PROVIDER
	// DEPENDENCY : SCREEN I18N PROVIDER
	// DEPENDENCY : PLANE MAP PROVIDER
	// DEPENDENCY : PLANE ADVANCED NAVIGATION PROVIDER
	// DEPENDENCY : SCREEN BUBBLE LINKS NAVIGATION BUTTONS PROVIDER
	// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider

	
if(typeof initAotraScreenClass ==="undefined"){
function initAotraScreenClass() {

	// CONSTANTS
	var NO_ZOOM="noZoom";
	
	window.AotraScreen = Class.create({

		// Constructor
		initialize : function(
				 title,background,borderStyle,moveAnimation,navigationMode
				,editable,zoomingAnimation,startingPlaneName
				,isSearchable,serversURLs,speed,canFilter
				,aotraMarking,miniMap,hooksParameters
				,/*OPTIONAL*/forceIsEditable,/*OPTIONAL*/isEncrypted,/*OPTIONAL*/mobile,/*OPTIONAL*/pluginsArgs) {

			// Attributes
			this.title=title;
			this.background=background;
			this.borderStyle=borderStyle;
			this.moveAnimation=moveAnimation;
			this.navigationMode=navigationMode;
			this.editable=editable;
			this.zoomingAnimation=zoomingAnimation;
			this.startingPlaneName=startingPlaneName;
			if(!empty(serversURLs))	this.serversURLs=serversURLs;
			this.speed=nothing(speed)?DEFAULT_SCREEN_SPEED:speed;
			// This attribute is indicative and user-alterable in XML source, if you want non-negociable 
			// (ie. non alterable by user in XML source, use IS_DEVICE_MOBILE_CALCULATED global variable instead:
			this.mobile=mobile;
			this.canFilter=canFilter;
			this.aotraMarking=aotraMarking;
			this.miniMap=miniMap;
			
			// Hooks
			this.hooksParameters=hooksParameters?hooksParameters:new Object();
			var screenIniters=AotraScreen.hooks[INIT];
			for(key in screenIniters){
				if(!screenIniters.hasOwnProperty(key))	continue;
				screenIniters[key].initScreen(this);
			}
			
			// Processed attributes
			this.mainDiv=document.getElementById(MAIN_DIV_ID);
			this.canvas=document.getElementById(MAIN_CANVAS_ID);

			// We clear any previous aotraScreen artifacts :
			this.clearPreviousScreenArtifacts();
			
			// -----------------------------------------------------------------------
			// Plugins overriding management section :
			this.pluginsArgs=pluginsArgs?pluginsArgs:new Object();// An associative array, too
			// If only one plugin is activated and is overriding aotraScreen, then we deactivate all features of this aotraScreen:
			this.isOverridden=false;
			for(key in window.globalPlugins){
				if(!window.globalPlugins.hasOwnProperty(key))	continue;
				var p=window.globalPlugins[key];
				
				// Plugin initialization :
				var pluginInitResult=p.initPlugin(this);
				if(pluginInitResult){
				
					// Plugin potential overriding of the AotraScreen :
					if(p.overrideAotraScreenDisplay){
						this.isOverridden=true;
						break;
					}
				}
			}
			
			
			// Nothing of the above is done, if aotraScreen is overridden
			if(this.isOverridden)	return;
			// -----------------------------------------------------------------------
			
			
			//	planes :
			this.planes = new Array();
			this.activePlane = null;
			
			//	canvas :
			this.initCanvas();
			var self = this;

//			var isEditionCalculatedToActive=(this.editable===true || forceIsEditable===true);
			
			// Onclick events technical handling :
			this.onClickInVoidEventListeners=new Array();
			var clickableElement=this.mainDiv;
			clickableElement.onclick = function(event) {
				
				
				self.onClickAotraScreen(event,null);
				// NO : Or it will interfere, for instance, with normal checkbox inputs click behavior !
//				event.preventDefault();
//				event.stopPropagation();
			};
			
			
//			// UNUSED : NOT WORKING
//			// Escape key using :
//			addFireOnAnyKey(this.onEscape,clickableElement);
//			this.onEscapeListeners=new Array();
			
			// UNUSED (for now...):
			//	clickableElement.onmousedown = function(event) {
			//		self.onDrag(event,"start");
			//	};
			//	clickableElement.onmouseup = function(event) {
			//		self.onDrag(event,"stop");
			//	};
			
			// AotraScreen moves :
			this.isOnClickEnabled=true;
			this.isOnDragEnabled=true;
			
			// DO NOT CALL DIRECTLY FOR SCREEN MOVING (ALWAYS CALL this.moveOnClick() INSTEAD,
			// OR ELSE YOU WILL BYPASS SCREEN MOVING POLICY ("bubbles","point","drag",etc.) !) :
			this.moveToPosition=new AotraScreenPositionMovingMethods(this,this.moveAnimation).moveToPosition;

			// AotraScreen navigation mode : point / scroll / bubbles (default)
			this.aotraScreenPositionMovingPolicies=new AotraScreenPositionMovingPolicies(this,this.navigationMode);
			
			this.moveOnClick=this.aotraScreenPositionMovingPolicies.moveOnClick;
			// TODO : (This method is currently empty...)
			this.moveOnHover=this.aotraScreenPositionMovingPolicies.moveOnHover;
			// TODO : (This method is currently empty...)
			this.moveOnDrag=this.aotraScreenPositionMovingPolicies.moveOnDrag;
		
			// AotraScreen encrypting capacities :
			this.encryptingProvider=new AotraScreenEncryptingProvider(this);

			// AotraScreen zooming capacities :
			this.zoomingProvider=null;
			var isZoomSetToActive=!nothing(this.zoomingAnimation) && this.zoomingAnimation!=NO_ZOOM;
			if(isZoomSetToActive || isEditionCalculatedToActive){
				this.zoomingProvider = new AotraScreenZoomingProvider(this);
				var aotraScreenZoomingMethods=new AotraScreenZoomingMethods(this,this.zoomingAnimation);
				this.zoomTo = aotraScreenZoomingMethods.zoomTo;
				this.zoomToFitBubble = aotraScreenZoomingMethods.zoomToFitBubble;
			}
			
			// AotraScreen bubbles searching capacities :
			this.searchProvider;
			if(isSearchable===true && !isEncrypted){
				this.searchProvider = new AotraScreenSearchProvider(this);
			}
	
			//##AotraScreen speech synthesis capacities :
			//##this.voiceName=voiceName;
			//##this.speechSynthesisProvider;
			//##if(window.isSpeechAbilityAvailable && voiceName && !nothing(voiceName,true)){
			//##	this.speechSynthesisProvider=new AotraScreenSpeechSynthesisProvider(this,voiceName);
			//##}else{
			//##	// TRACE
			//##	log("INFO: AotraScreen did not load speech synthesis provider.");
			//##}
			
			// AotraScreen bubbles visibility layers usage capacities :
			this.canFilterBubbles=this.canFilter;
			
			// Internationalization management :
			this.i18nProvider=new AotraScreenI18nProvider(this);
			
			
			// Advanced navigation management :
			// (A direct link to last visible bubble 
			// and a button to go to last added bubble in bubble content, if edition is active)
			// DEPENDENCY : MUST BE CALLED BEFORE EDITION PROVIDER, ALWAYS !
			if(!isEncrypted)	this.advancedNavigationProvider=new AotraScreenAdvancedNavigationProvider(this);
			
			
			// AotraScreen edition capacities :
			this.editionProvider=null;
			this.savingProvider=null;
			this.uploadProviders;
			
			var isEditionCalculatedToActive=(this.editable===true || forceIsEditable===true);
			if(isEditionCalculatedToActive){
				var ACCEPTED_FILES_TYPES_STRING=
					"image/png,image/jpeg,image/gif,"
					+"video/avi,video/mp4,"
					+"audio/mp3,audio/wav,audio/ogg,audio/x-vorbis+ogg,audio/mpeg,"
					+"text/css,text/pdf,text/txt,text/plain";

				// Upload :
				// DEPENDENCY : uploadProviders need savingProvider to work !
				this.uploadProviders=new Array();
				this.uploadProviders[SAVING_PROVIDER_HOOK_NAME]=new AotraScreenUploadProvider(this,SAVING_PROVIDER_HOOK_NAME,ACCEPTED_FILES_TYPES_STRING);
				this.uploadProviders[EDITION_PROVIDER_HOOK_NAME]=new AotraScreenUploadProvider(this,EDITION_PROVIDER_HOOK_NAME,ACCEPTED_FILES_TYPES_STRING);
				
//				// Files embedding :
//				// AotraScreen files embedding capacities :
//				// DEPENDENCY : filesEmbeddingProviders need savingProvider to work !
//				this.filesEmbeddingProviders=new Array();
//				this.filesEmbeddingProviders[EDITION_PROVIDER_HOOK_NAME]=new FilesEmbeddingProvider(this,EDITION_PROVIDER_HOOK_NAME,ACCEPTED_FILES_TYPES_STRING);

				// Saving features :
				this.savingProvider = new AotraScreenSavingProvider(this,this.serversURLs,this.encryptingProvider);
		
				// Basic edition :
				this.editionProvider = new AotraScreenEditionProvider(this);

			}
			
			
			// AotraScreen aotramarking capacities :
			if(this.aotraMarking)
				this.aotraMarkingProvider=new ScreenAotraMarkReaderProvider(this);
			
			if(this.miniMap)
				this.miniMapProvider=new AotraScreenMiniMapProvider(this);

			
			// -----------------------------------------------------------------
			
			// Technical attributes :
			
			this.bubbleIndex=0;
			
			// Those coordinates are for display (zoom-dependent coordinates) :
			this.x=0;
			this.y=0;
			// Those coordinates are for data (zoom-independent coordinates) :
			this.canonicalX=this.x;
			this.canonicalY=this.y;

			// Zoom is a simple ratio 1x number,
			this.zoomFactor=1;
			
			this.initialModelSignature=null;
			
			// DEPENDENCY : SCREEN ADVANCED NAVIGATION PROVIDER
			// After bubble going to treatments :
			// Eventual advanced navigation provider treatments :
			if(this.advancedNavigationProvider){
				this.advancedNavigationProvider.restoreAllLastVisitedBubblesFromStorage();
			}

						
		}

		// Methods
		/*private*/,clearPreviousScreenArtifacts:function(){

			// To clear mainDiv element :
//			jQuery(window).remove(".bubbleLinksButton");
			
			// DOES NOT WORK ON FIREFOX (due to broken Firefox DOM management) :
			// this.mainDiv.innerHTML="";
			var oldMainDiv=this.mainDiv;
			var parent=this.mainDiv.parentNode;
			parent.removeChild(this.mainDiv);
			this.mainDiv=document.createElement("div");
			this.mainDiv.id=oldMainDiv.id;
			this.mainDiv.style.cssText=oldMainDiv.style.cssText;
			parent.appendChild(this.mainDiv);
			
			
			if(this.ctx)	delete this.ctx;
			this.ctx=null;
			this.width = 0;
			this.height = 0;
			this.mouseX = 0;
			this.mouseY = 0;

		}
	
	
		,initCanvas : function() {

			this.refreshSizeAfterCanvasSize();

			if (!this.canvas.getContext) {
				alert(MEAN_IE_MESSAGE_SHORT);
				return;
			}

			this.ctx = this.canvas.getContext("2d");
			if (!this.ctx) {
				alert(MEAN_IE_MESSAGE_SHORT);
				return;
			}
			
//			// We pre-load the image :
//			if(IMAGE_FILENAME){
//				var allImages=new Image();
//				allImages.src=IMAGE_FILENAME;
//			} 
			
		}
		
		/*public*/,getMainDiv:function(){
			return this.mainDiv;
		}
		
	
		/*public*/,doAfterAotraScreenPopulation:function(){
			
			var plane=this.getActivePlane();
			
			var self=this;
			
			jQuery(document).ready(function(){

				
				// DEPENDENCY : PLANE MAP PROVIDER CODE HERE :
				// Eventual map treatments from bubbles contents:
				if(plane.mapProvider){
					for(var i=0;i<plane.getBubbles().length;i++){
						var bubble=plane.getBubbles()[i];
						plane.mapProvider.refreshMapMarkersAfterBubbleContent(bubble);
					}
				}
			
			});
			
			
			// DEPENDENCY : I18N PROVIDER CODE HERE :
			// Eventual i18n treatments from bubbles contents:
			if(self.i18nProvider){
				self.i18nProvider.refreshAllDefaultLanguages();
			}
			
			// DEPENDENCY : PLANE ADVANCED NAVIGATION PROVIDER
			if(this.advancedNavigationProvider){
				this.advancedNavigationProvider.determineHomeBubbleOnInit();
			}
			
			// Scrolling modes common elements :
			// UI elements adding :
			var mainDiv=this.getMainDiv();
			var divScrollerUIs=document.getElementById("divScrollingMode");
			var selectScrollerUIs=document.getElementById("selectScrollingMode");
			if(!divScrollerUIs){
				var UI_X=0;
				var UI_Y=0;
				divScrollerUIs=document.createElement("div");
				divScrollerUIs.id="divScrollingMode";
				divScrollerUIs.style.cssText="position:fixed;left:"+UI_X+"px;top:"+UI_Y+"px;";
				selectScrollerUIs=document.createElement("select");
				selectScrollerUIs.id="selectScrollingMode";
				selectScrollerUIs.style.cssText="width:48px;";
				divScrollerUIs.appendChild(selectScrollerUIs);
				mainDiv.appendChild(divScrollerUIs);

			}
			// (default scroll mode option adding)
			var verticalScrollOption=document.createElement("option");
			verticalScrollOption.id="optionScroll"+HEUROPTIC_MODE;
			verticalScrollOption.innerHTML=HEUROPTIC_MODE;
			verticalScrollOption.value=HEUROPTIC_MODE;
			verticalScrollOption.onChangeOption=this.onChangeScrollMode;
			
			selectScrollerUIs.onchange=function(){
				var selectedIndex=selectScrollerUIs.selectedIndex;
				var selectedOption=selectScrollerUIs.options[selectedIndex];
				if(!selectedOption)	return;
				selectedOption.onChangeOption(self);
			};
			selectScrollerUIs.appendChild(verticalScrollOption);
			
			// Hooks
			var screenIniters=AotraScreen.hooks[INIT_AFTER_READY];
			for(key in screenIniters){
				if(!screenIniters.hasOwnProperty(key))	continue;
				screenIniters[key].initScreen(this);
			}
			
			// ----------------------------------------------------------------------------------------
			// MUST REMAIN THE LAST MEMBER OF THIS CONSTRUCTOR :
			// Eventual map container click events adjustement :
			// A NEW WAY (cf init.js globalStyle variable) :
			// PLEASE KEEP CODE
//			jQuery(this.mainDiv).find(" *").css("pointer-events","all");
			
			

		}
		
		// Scroll management
		/*public*/,onChangeScrollMode:function(aotraScreen){
			
			var plane=aotraScreen.getActivePlane();
			
			PlaneVerticalScrollingBubblesPresentingProvider.setScrollMode(plane,HEUROPTIC_MODE);

		}
		
		
		// Mouse concerns :
		,refreshMousePosition : function(eventParam) {
			var event=eventParam;
			
			if (!event)	event = window.event;
			if (event.pageX || event.pageY) {

				this.mouseX = event.pageX + window.pageXOffset;
				this.mouseY = event.pageY + window.pageYOffset;
				
			} else if (event.clientX || event.clientY) {
				
				var backgroundHolderTag=document.getElementById(AOTRA_SCREEN_DIV_ID)?document.getElementById(AOTRA_SCREEN_DIV_ID):document.body;
				
				this.mouseX = event.clientX + backgroundHolderTag.scrollLeft
					+ document.documentElement.scrollLeft;
				this.mouseY = event.clientY + backgroundHolderTag.scrollTop
					+ document.documentElement.scrollTop;
		
				// WORKING :
//				this.mouseX = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
//				this.mouseY = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
			}

		}
		
		,getOnClickInVoidEventListeners:function(){
			return this.onClickInVoidEventListeners;
		}
		
		// This method must remain private, since it is the only central click interception point.
		// Or else it will be called more that once, what we don't want.
		/*private*/,onClickAotraScreen:function(event,bubble){

			var clickedElement=event.target;

			// We ignore bubbleDiv or bubbleText onClick events, since it is handled il Bubble.onFocus() method :
			if(nothing(bubble) 
					&& (contains(clickedElement.className,BUBBLE_DIV_CLASSNAME) || contains(clickedElement.className,BUBBLE_TEXT_CLASSNAME)))
					return;
			
			
			
			
			// Absolute priority : if aotraScreen is overridden by a plugin :
			if(this.isOverridden)	return;
			
			
			
			// Priority 0 : Anchors
			// Hyperlinks have priority over aotraScreen for onClick event :
			if(clickedElement.nodeName.toLowerCase()==="a"){
				return;
			}
			

			// Priority 1 : clickables
			// Things that have an onclick inline method :
			if(!nothing(clickedElement.onclick) && !contains(clickedElement.id,BUBBLE_DIV_PREFIX)
				// and are not the aotraScreen itself :
				&& clickedElement.id!==this.mainDiv.id && clickedElement.id!==this.canvas.id 
			){
				return;
			}

			
			// Priority 2 : Links entry/out points
			// DEPENDENCY : EDITION PROVIDER
  		// CAUTION : Links from the currently edited bubble are deactivated for navigation.
			if(!this.isCalculatedEditable() || !this.editionProvider.isBubbleCurrentlyBeingEdited()){
				// Links entry/out points have priority over aotraScreen for onClick event :
  //			containsIgnoreCase(clickedElement.className,LINK_CSS_CLASS_NAME_BACKWARDS)
  			var foundAncestorForLinkBackwards=getAncestorsOrItselfContainsClassName(clickedElement,LINK_CSS_CLASS_NAME_BACKWARDS);
  			var foundAncestorForLink=getAncestorsOrItselfContainsClassName(clickedElement,LINK_CSS_CLASS_NAME);
  			if(foundAncestorForLinkBackwards){
  				this.getActivePlane().getLink(foundAncestorForLinkBackwards).onClickBackwards();
  				return;
  //			}else if(containsIgnoreCase(clickedElement.className,LINK_CSS_CLASS_NAME)){
  			}else if(foundAncestorForLink){
  				this.getActivePlane().getLink(foundAncestorForLink).onClick();
  				return;
  			}
			}else{
				// TRACE
				log("WARN: Links are deactivated on currently edited bubble.");
			}
			
			
			//##// Priority 3 : Speechables
			//##// Speechable CSS-ed elements have priority over aotraScreen for onClick event :
			//##////containsIgnoreCase(clickedElement.className,SPEECHABLE_CSS_CLASS_NAME)
			//##var foundAncestorForSpeechable=getAncestorsOrItselfContainsClassName(clickedElement,SPEECHABLE_CSS_CLASS_NAME);
			//##if(foundAncestorForSpeechable
			//##		&& this.speechSynthesisProvider){
			//##	// DEPENDENCY : SPEECH SYNTHESIS PROVIDER  
			//##	this.speechSynthesisProvider.speakHTMLElement(foundAncestorForSpeechable);
			//##	return;
			//##}
			
			
			// Priority 4: AotraScreen treatments
			if(this.isOnClickEnabled==false)	return;
			
			
			// Priority 5: AotraScreen on click in void event listeners :
			if(!bubble){
				var listeners=this.getOnClickInVoidEventListeners();
				for(var i=0;i<listeners.length;i++){
					listeners[i].doAction();
				}
				// We stop here if there are «click in void» listeners, because they override default behavior :
				if(!empty(listeners))		return;
			}
			
			// Priority 6 : A bubble:
			this.refreshMousePosition(event);
			var destinationX=this.mouseX;
			var destinationY=this.mouseY;

			if(bubble && bubble.isFocusable() && !bubble.isFixed() ){
				this.moveOnClick(bubble,destinationX,destinationY);
				return;
			}
			
			// Priority 7 : Everything else that is not the void of the mainDiv nor a bubble...:
			if(clickedElement.id!==this.mainDiv.id ) {
				return;
			}
			
			// We force «no bubble clicked» behavior if really nothing is clickable...:
			this.moveOnClick(null,destinationX,destinationY);
			return;
			
			
		}
		
		
		// cf. luke.breuer.com/tutorial/javascript-drag-and-drop-tutorial.aspx
		,onDrag:function(event,phase/*«start»,«while»,«stop»*/){

			// Absolute priority : if aotraScreen is overridden by a plugin :
			if(this.isOverridden)	return;
			
			
			if(this.isOnDragEnabled==false)	return;

			
			if(phase=="start"){
				this.isDragStarted=true;
				this.refreshMousePosition(event);
				this.dragX=this.mouseX;
				this.dragY=this.mouseY;
				return;
			}
			
			if(phase=="stop" && this.isDragStarted==true){
				
				this.isDragStarted=false;
				
				this.refreshMousePosition(event);

				var destinationX=this.x-(this.mouseX-this.dragX);
				var destinationY=this.y-(this.mouseY-this.dragY);
				
				this.moveOnDrag(destinationX,destinationY);
			
			}

			
		}
		
//		// UNUSED: NOT WORKING
//		,onEscape:function(){
//			for(var i=0;i<this.onEscapeListeners;i++)
//				this.onEscapeListeners[i].onEscape();
//		}
		
		,enableOnClick:function(value){
			this.isOnClickEnabled=value;
		}

		,enableOnDrag:function(value){
			this.isOnDragEnabled=value;
		}
		
		
		/*public*/,refreshSizeAfterCanvasSize:function(){
			if (this.canvas.getWidth)
				this.width = this.canvas.getWidth();
			if (this.canvas.getHeight)
				this.height = this.canvas.getHeight();
		}
		
		,getWidth:function(){
			return this.width;
		}
		
		,getHeight:function(){
			return this.height;
		}
	
		// DO NOT CALL DIRECTLY FOR SCREEN MOVING (ALWAYS CALL this.moveOnClick() INSTEAD,
		// OR ELSE YOU WILL BYPASS SCREEN MOVING METHOD ("linear", "instant", etc.) !) :
		,setScreenLocation:function(destinationX,destinationY){

			this.x=destinationX;
			this.y=destinationY;
			
			this.canonicalX=this.x;
			this.canonicalY=this.y;
			
			this.drawAndPlace();
		}
		
//		,getLocation:function(){
//			var location=new Object();
//			location.["x"]=this.x;
//			location.["y"]=this.y;
//			return location;
//		}
		
		
		,moveToMousePosition:function(destinationX,destinationY){
			
			var xOffset=destinationX-this.width/2;
			var yOffset=destinationY-this.height/2;
			
			this.setScreenLocation(this.x+xOffset,this.y+yOffset);
		}
		
		,getSpeedParam:function(paramName){
			var SEPARATOR=";";
			var results=new Array();
			
			var defaultSplit=DEFAULT_SCREEN_SPEED.split(SEPARATOR);
			results["numberOfSteps"]=parseFloat(defaultSplit[0]);
			results["refreshingRate"]=parseFloat(defaultSplit[1]);
			
			if(!nothing(this.speed) && contains(this.speed,SEPARATOR)){
				var split=this.speed.trim().split(SEPARATOR);
				var s0=split[0];
				var s1=split[1];
				if(!nothing(s0) && isNumber(s0))
					results["numberOfSteps"]=parseFloat(s0);
				if(!nothing(s1) && isNumber(s1))
					results["refreshingRate"]=parseFloat(s1);
			}
			
			if(!results[paramName]){
				//TRACE
				log("ERROR : Screen speed parameter «"+paramName+"» not found.");
				return 1;
			}
			return results[paramName];
			
		}
		
		// Zoom management
		,getZoomingProvider:function(){
			return this.zoomingProvider;
		}
		
		/*public*/,getZoomFactor:function(){
			return this.zoomFactor;
		}		

//		,getZoomFactorForDisplay:function(){
//			return this.zoomFactor/(ZOOM_EXPANSION/2);
//		}
		
		// THIS METHOD MUST BE CALLED ONLY BY AotraScreenZoomingMethods CLASS !!! 
		,setZoom:function(destinationZoomFactor){
			
			if(!this.zoomingProvider)	return;

			var oldZoomFactor=this.getZoomFactor();
			var zoomFactorDelta=destinationZoomFactor-oldZoomFactor;

			// CAUTION : canonical coordinates must not be updated at this point !
			this.x+=(this.x*zoomFactorDelta)/oldZoomFactor;
			this.y+=(this.y*zoomFactorDelta)/oldZoomFactor;

			this.zoomFactor+=zoomFactorDelta;

			this.drawAndPlace();

//			// Maps management :
//			var activePlane=this.getActivePlane();
//			if(activePlane){
//				mapProvider=activePlane.getMapProvider();
//				if(mapProvider){
//					mapProvider.refreshZoom(this);
//				}
//			}
			
		}
		
		// Planes management
		,getPlaneByName : function(planeName) {
			
			for ( var i = 0; i < this.getPlanes().length; i++) {
				var p = this.getPlanes()[i];
				
				if (p.getName() === planeName)
					return p;
			}

			return null;
		}

		
		,setActivePlane: function(plane) {
			if((!plane && !empty(this.getPlanes()) ) || !this.getPlaneByName(plane.getName())){
				this.activePlane = this.getPlanes()[0];
				return;
			}
			this.activePlane = plane;
		}

		// UNUSED
//		,setActivePlaneByName : function(planeName) {
//			if(!planeName && !empty(this.getPlanes())){
//				this.activePlane = this.getPlanes()[0];
//				return;
//			}
//			this.activePlane = this.getPlaneByName(planeName);
//		}
		
		,getActivePlane : function() {
			return this.activePlane;
		}
		
		,getCanFilterBubbles:function(){
			return this.canFilterBubbles;
		}
		
		
		,addPlane : function(plane) {
			this.getPlanes().push(plane);
		}
		
		,getPlanes:function(){
			return this.planes;
		}
		
		,getPlaneForBubble:function(bubble){
			for(var i=0;i<this.getPlanes().length;i++){
				var plane = this.getPlanes()[i];
				if(plane.getBubbleByName(bubble.getName())!=null)
					return plane;
			}
			return null;
		}

	
		// Bubbles management :
		,updateAllBubblesContentWithNewBubbleName:function(oldBubbleName,newBubbleName){
			// TODO : Develop...
		}
		
		
		/*public*/,doGotoBubbleByName:function(bubbleName,/*OPTIONAL*/forceOpeningOnGoto){
			this.doGotoBubble(this.getBubbleByName(bubbleName),forceOpeningOnGoto);
		}
		
		// This method is a bit quicker than the previous one (because it won't look for a named bubble)...:
		/*public*/,doGotoBubble:function(bubble,/*OPTIONAL*/forceOpeningOnGoto){
			var aotraScreen=this;
			
			
			
			if(!bubble || !aotraScreen.isOnClickEnabled){
				//TRACE
				log("WARN : Cannot go to bubble : bubble does not exist or click is not enabled.");
				return;
			}
			
			if(		bubble.isFixed()
// NOT THIS:	|| !bubble.isFocusable()
				|| !bubble.isNavigable()
			){
				//TRACE
				log("WARN : Cannot go to bubble : bubble is fexed or not navigable.");
				return;
			}
			
			var activePlane=this.getActivePlane();
			if(!activePlane.isBubbleInPlane(bubble))	return;
			
			var isVerticalMode=!PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(activePlane);
			
			
			var doOnArrival=function(){

				var activePlane=aotraScreen.getActivePlane();

				// In default (heuroptic) mode, first we land to the bubble, then we open it, if needed
				// In vertical mode, it is the contrary !
				// Bubble opening :
				if(!isVerticalMode){
					if(forceOpeningOnGoto && !bubble.isOpen()){
						bubble.doOpen();
					}
				}
				
				// After going to bubble treatments :
				var doOnBubbleArrivalHooks=AotraScreen.hooks[DO_ON_ARRIVAL_HOOK_NAME];
				for(var i=0;i<doOnBubbleArrivalHooks.length;i++){
					var doOnBubbleArrivalHook=doOnBubbleArrivalHooks[i];
					doOnBubbleArrivalHook(aotraScreen,bubble);
				}
				
//				// DEPENDENCY : SCREEN ADVANCED NAVIGATION PROVIDER
//				// Eventual advanced navigation provider treatments :
//				if(aotraScreen.advancedNavigationProvider){
//					aotraScreen.advancedNavigationProvider.setLastVisitedBubbleForPlane(activePlane.getName(),bubble);
//				}
				
//				// DEPENDENCY : PLANE MAP PROVIDER
//				if(activePlane && activePlane.mapProvider){
//					activePlane.mapProvider.doGotoBubbleMarkerLocation(bubble);
//				}
				
//				// DEPENDENCY : AOTRASCREEN POSITION MOVING POLICIES
//				if(aotraScreen.zoomingProvider && aotraScreen.aotraScreenPositionMovingPolicies.fitZoomToBubbles){
//					aotraScreen.zoomToFitBubble(bubble);
//				}
				
			};
			
			
			
			// AotraScreen position moving policies BYPASS
			// (because we already know what we want to do : go to a specific bubble !) :
			
			// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider
			if(isVerticalMode){

				// CAUTION : Handles opening the bubble, and then going to bubble :
				activePlane.verticalScrollingProvider.doGotoBubble(bubble,doOnArrival,forceOpeningOnGoto);
				
			}else{

				// Default mode :
				var xy=bubble.getDisplayXY(CENTER);
				aotraScreen.moveToPosition(xy["x"],xy["y"],doOnArrival);
			}
			
		}

		,nextBubbleIndex:function(){
			this.bubbleIndex++;
			return this.bubbleIndex;
		}
		
		,goBackBubbleIndex:function(){
			this.bubbleIndex--;
			return this.bubbleIndex;
		}
		
		,isBubbleExistsByName:function(bubbleName){
			return !nothing(this.getBubbleByName(bubbleName));
		}
		
		,getBubbleByName:function(bubbleName){
			if(nothing(bubbleName))	return null;
			for(var i=0;i<this.getPlanes().length;i++){
				var bubble=this.getPlanes()[i].getBubbleByName(bubbleName);
				if(bubble!=null)	return bubble;
			}
			return null;
		}
		
		// UNUSED FOR NOW...
		,getBubblesInActivePlaneByNameContaining:function(searchString){
			if(this.activePlane==null)	return new Array();
			return this.activePlane.getBubblesInActivePlaneByNameContaining(searchString);
		}
		
		,getBubblesInActivePlaneByContentContaining:function(searchString,/*OPTIONAL*/isClearAccentuatedCharacters){
			if(!this.getActivePlane())	return new Array();
			return this.activePlane.getBubblesInActivePlaneByContentContaining(searchString,isClearAccentuatedCharacters);
		}
		
		,deleteBubble:function(bubble){

			var b=document.getElementById(BUBBLE_DIV_PREFIX+"_"+bubble.getName());
			removeElementFromParent(b);

			// We remove it from all planes :
			for(var i=0;i<this.getPlanes().length;i++){
				var plane=this.getPlanes()[i];
				var bb=plane.getBubbleByName(bubble.getName());
				if(bb!==null)	plane.removeBubble(bb);
				
			}

			// DEPENDENCY : SCREEN ADVANCED NAVIGATION PROVIDER
			if(!empty(bubble.previousBubbleButtonSlct)){
				var previousBubbleButton=bubble.previousBubbleButtonSlct.get()[0];
				removeElementFromParent(previousBubbleButton);
			}
		}
		
		
		// Model changes management :
		
		,isCalculatedEditable:function(){
			return (this.editionProvider!==null);
		}
		
		,isModelChanged:function(){
			if(this.initialModelSignature){
				var newModelSignature=this.getCalculatedModelSignature();
				return (this.initialModelSignature != newModelSignature);
			}
			return false;
		}
		
		,calculateModelSignature:function(){
			this.initialModelSignature=this.getCalculatedModelSignature();
		}

		,getCalculatedModelSignature:function(){
			return getHashedString(new AotraScreenExporterToXML().getXMLFromAotraScreen(this));
		}
		
		,isMobile:function(){
			return this.mobile==="mobile";
		}
		
		// DEPENDENCY : Edition provider code
		/*public*/,notifyBubbleSelectionHasChanged:function(oldBubble,newBubble){
			
			if(oldBubble)	oldBubble.hideSelectionMark();
			
			if(newBubble)	newBubble.showSelectionMark();
		}


		// Drawing
		,clear:function(){
			var ctx=this.ctx;
			ctx.clearRect(0, 0, this.width, this.height);
		}

		/*public*/,draw:function(){
			
			// First, we clear the whole screen :
			this.clear();
			
			var activePlane=this.getActivePlane();
			
			// First, we draw active plane background :
			if(activePlane && activePlane.draw){
				activePlane.draw(this);

				// ...then bubbles links :
				// (We have no choice to draw them here, because screen may not be fully populated when plane.draw() is called...
				// thus causing bugs : I know because I tried it already !)
				activePlane.drawLinks(this);

			}
			
			//...then selection mark :
			/* OLD WAY :
			// DEPENDENCY : AotraScreenEditionProvider code here : 
			if(this.editionProvider && !nothing(this.editionProvider.getSelectedBubble())){
				var selectedBubble=this.editionProvider.getSelectedBubble();
				selectedBubble.drawSelectionMark();
			}
			*/
			
			// TODO : FIXME : On today, there's no way to display the grid positioned consistently :
			// DEPENDENCY : AotraScreenEditionProvider 
			//...then positioning grid (if needed) :
			if(this.editionProvider)
				this.editionProvider.drawPositioningGrid(this);

			
			
			// DEPENDENCY : AotraScreenMiniMapProvider code here : 
			if(this.miniMapProvider)
				this.miniMapProvider.refreshViewMark();
			
		}
		
		/*public*/,drawAndPlace:function(/*OPTIONAL*/refreshContent){
			
			// -----------------------------------------------------------------------
			// Nothing of the above is done, if aotraScreen is overridden
			if(this.isOverridden)	return;
			// -----------------------------------------------------------------------
			
		// Caution : forcing content refresh is time and resources-consuming
			
//			for ( var i = 0; i < this.getPlanes().length; i++) {
//				this.getPlanes()[i].placeDivs();
//			}
			this.getActivePlane().placeDivs(refreshContent);
			this.draw();
		}
	});
	
	// GLOBAL STATIC METHODS
	window.AotraScreen.updateAotraScreenFromXML=function(/*OPTIONAL*/bruteXMLParam){
		
		window.waitIndicator.start();
		
		// We clear the current model tag :
		var modelElement=document.getElementById(MODEL_ID);
		modelElement.innerHTML=bruteXMLParam;
		
		var unNormalizedXMLText="";
		if(bruteXMLParam){
			unNormalizedXMLText=bruteXMLParam;
		}else{
			unNormalizedXMLText=new AotraScreenExporterToXML().getXMLFromAotraScreen(getAotraScreen());
		}

		// We add CDATA tags :
		var xmlText=normalizeXMLContentsTagsInBubbles(unNormalizedXMLText);

		var xmlDoc=parseXMLString(xmlText);
		var eRoot=xmlDoc.documentElement;
		
		var isErroneous= !empty(eRoot.getElementsByTagName("parsererror"));
		if(isErroneous){
			var errorMessage=eRoot.getElementsByTagName("parsererror")[0].textContent;
			// TRACE
			log(i18n({"fr":"ERREUR DE COMPILATION XML : "+errorMessage,"en":"ERROR IN XML PARSING : "+errorMessage}));
			
			alert(i18n({"fr":"ERREUR DE COMPILATION XML : "+errorMessage,"en":"ERROR IN XML PARSING : "+errorMessage}));
			window.waitIndicator.end();
			if(getAotraScreen) return getAotraScreen();
			return null;
		}
		
		var eRoot=xmlDoc.documentElement;
		var aotraScreenFactoryXML = new AotraScreenImporterFromXML();
		var aotraScreen=aotraScreenFactoryXML.getAotraScreenFromXML(eRoot,forceIsEditable);
		
		// Do on end :
		afterAotraScreenInit(aotraScreen);
		setScreen(aotraScreen);
		aotraScreen.draw();
		
		window.waitIndicator.end();
		
		return aotraScreen;
	};
	
	// STATIC CODE :
	// Hooks
	/*static*/
	// Generic hooks : 
	window.AotraScreen.hooks=new Object();
	window.AotraScreen.hooks[DO_ON_ARRIVAL_HOOK_NAME]=new Array();
	window.AotraScreen.hooks[INIT]=new Object();
	window.AotraScreen.hooks[INIT_AFTER_READY]=new Object();


}
}

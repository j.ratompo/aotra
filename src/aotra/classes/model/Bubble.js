
// DEPENDENCY : PLANE ADVANCED NAVIGATION PROVIDER
// DEPENDENCY : BUBBLE LINKS NAVIGATION PROVIDER

// GLOBAL STATIC METHODS
	function normalizeXMLContentsTagsInBubbles(xmlText){
		
		var result=xmlText.trim();
	
		result=result.replace(new RegExp("<content\\s*>","gim"),"<content><![CDATA[");
		
		// OK, now with language content tags that's a little bit more complicated :
		var r=new RegExp("<content\\s+language=['\"](\\w+)['\"]\\s*>","gim");
	
		var matches=r.exec(result);
		if(!empty(matches))	result=result.replace(r,matches[0]+"<![CDATA[");
		
		result=result.replace(new RegExp("</content>","gim"),"]]></content>");
		
		return result;
	}

// GLOBAL CONSTANTS :
	const BUBBLE_DIV_CLASSNAME="bubbleDiv";
	const BUBBLE_TEXT_CLASSNAME="bubbleText";
	const DEFAULT_BUBBLE_WIDTH=300;
	const DEFAULT_BUBBLE_HEIGHT=300;
	const DEFAULT_BUBBLE_COLOR="#888888";
	const DEFAULT_BUBBLE_BORDER_STYLE="none";
	const BUBBLE_DIV_PREFIX="divBubble";
	const CENTER="center";
	const LEFTUP_CORNER="leftupCorner";
	
	const CIRCLE_MOLD="circle";
	const SQUARED_MOLD="squared";
	const ROUNDED_MOLD="rounded";
	const INVISIBLE_TO_VIEW_MOLD="invisible";
	const INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE="invisibleNavigable";
	const PENTAGON_MOLD="pentagon";
	const HEXAGON_MOLD="hexagon";
	const HEPTAGON_MOLD="heptagon";
	const OCTOGON_MOLD="octogon";

	const REMAIN_CLOSED="remainClosed";
	const REMAIN_HIDDEN="remainHidden";

	//	const NON_FIXED="nonFixed";
	const FIXED="fixed";
	const NON_FOCUSABLE="nonFocusable";
	
	// Hooks names
	const DO_ON_CONTENT_RENDER="doOnContentRender";
	const DO_ON_CREATE_TEXT_DIV="doOnCreateTextDiv";
	

if(typeof initBubbleClass ==="undefined"){
function initBubbleClass(){
	var initSelf=this;
	// CONSTANTS
	const MIN_W=40;
	const MIN_H=40;
	const TOOLTIP="Double-click to open or close bubble.";
	const TOOLTIP_MOBILE="Tap to open bubble.";

	const THICKNESS=60;
	const SELECTION_MARK_COLOR="#8888FF";
	const CLOSED_OPACITY=0.5;
	const TEXT_COVERAGE_AREA_PERCENT=.80;

	// «selected bubble» CSS class name :
	const SELECTED_CLASS_NAME="selected";

	
	initSelf.Bubble = Class.create({
	
		// Constructor
		initialize: function(
				name,x,y,w,h
				,mold
				,contents // (May be an associative array or a single string)
				,fontSize,openableClosable,color
				,openCloseAnimation,borderStyle,openedOnCreate
				,fixedStrategy,isHomeBubble,isExcludingText,hooksParameters
				,/*OPTIONAL*/zIndex
				,/*OPTIONAL*/serverCode
				,/*OPTIONAL*/visibilityLayers
				,/*OPTIONAL*/parallaxBackground
				,/*OPTIONAL*/aotraScreen
		) {
			
			// Attributes
			this.aotraScreen=aotraScreen;
			var aotraScreen=this.getAotraScreen();
			
			
			this.x=x;
			this.y=y;
//			// (default diameter is max diameter for an opened bubble)
			this.w=(!w||w===0)?DEFAULT_BUBBLE_WIDTH:w;
			this.h=(!h||h===0)?this.w:h;
			this.mold=mold;
			
			// Content is processed at last (because then Bubble div is placed) :
			this.fontSize=fontSize;
			this.color=color?color:DEFAULT_BUBBLE_COLOR;
			this.borderStyle=borderStyle?borderStyle:DEFAULT_BUBBLE_BORDER_STYLE;
			this.openCloseAnimation=openCloseAnimation;
			// CAUTION : This attribute only stores whether the bubble is intended to be openable/closable,
			// but this attribute does not make the bubble to be effectively openable/closable.
			// Only the non-nullity or nullity of attribute openCloseProvider does and must do this !
			this.openableClosable=null;
			this.openedOnCreate=openedOnCreate;
			// Default behavior is bubble is not fixed :
			this.fixedStrategy=!fixedStrategy?"":fixedStrategy;
			this.isHomeBubble=isHomeBubble;
			this.isExcludingText=isExcludingText;
			
			// Hooks
			this.hooksParameters=hooksParameters?hooksParameters:new Object();
			var bubbleIniters=Bubble.hooks[INIT];
			for(key in bubbleIniters){
				if(!bubbleIniters.hasOwnProperty(key))	continue;
				bubbleIniters[key].initBubble(this,aotraScreen);
			}
			
			// Other attributes
			this.serverCode=serverCode;
			this.visibilityLayers=visibilityLayers;
			this.parallaxBackground=parallaxBackground;
			
			// ------------ Processed (ie. technical) attributes ------------ 
			this.visible=true;
			// (Caution : «zIndex» attribute, is different from z attribute in a 3D coordinates system)
			this.zIndex=zIndex?zIndex:MIN_Z_INDEX_BUBBLES; // just above 0...
			// These two set of coordinates have the only difference that they are never affected by zoom factor,
			// contrary to the actual set of coordinates «this.x ; this.y»
			this.canonicalX=x;
			this.canonicalY=y;
			
			this.minW=MIN_W;
			this.maxW=this.w;
			this.minH=MIN_H;
			this.maxH=this.h;
			
			// Diameter an opening adjustment :
			// This attribute is only here (at this time...) to signify the non-persistable capacity state of the bubble to be openable and closable or to be not.
			this.openCloseProvider=null;
			// (we effectively set the attribute, and instanciate the openCloseProvider :)
			this.setIsOpenableClosable(openableClosable);
			// (bubbles that are not openableClosable are always open)
			if(this.openableClosable===false){
				// We cannot call method setOpen(...) at this point because this.div is not initialized yet:
				this.open=true;
			}else{
				// We cannot call method setOpen(...) at this point because this.div is not initialized yet:
				this.open=this.canOpenBubble();
				if(!this.open){
					this.w=MIN_W;
					this.h=MIN_H;
				}
			}
			
			// a technical-only computed attribute :
			this.borderOffset=0;
			if(aotraScreen && aotraScreen.borderStyle && containsIgnoreCase(aotraScreen.borderStyle,"px")){
				var aotraScreenBorderStyle=aotraScreen.borderStyle.toLowerCase();
				this.borderOffset=(function(){
					var split=aotraScreenBorderStyle
							.replace(new RegExp("\\s+","gim"),"")
							.split(" ");
					for(var i=0;i<split.length;i++){
						var s=split[i].replace("px","");
						if(isNumber(s))	return parseFloat(s);
					}
				})();
			}
			
			this.parentBubble;
			this.subBubbles = new Array();
			this.div=null;
			this.textDiv=null;
			this.ready=false;
			
			// UI elements close to the bubble :
			this.openCloseButtonSlct=null;
			// UNUSED FOR NOW :
			/*this.previousBubbleButtonSlct=null;*/

			// -----------------------------------------------------
			
			// Opening animations :
			this.doOpen=new BubbleOpeningMethods(openCloseAnimation).doOpen;
			// Closing animations :
			this.doClose=new BubbleClosingMethods(openCloseAnimation).doClose;
			
			// Name : (will serve as unique identifier)
			var generatedName=DEFAULT_BUBBLE_NAME+"_"+generateRandomString(32,"simple");
			if(name!==null && name.trim()!==""){
				if(aotraScreen.isBubbleExistsByName(name)){
					// TRACE
					log("WARN : A name collision was found for bubble : «"+name+"», we generated a new name : «"+generatedName+"»");
					name=generatedName;
				}
				this.setName(name);
			}else{
				this.setName(generatedName);
			}

			
			// Initialization :
			this.initContentDiv();

			
			// -----------------------------------------------------------------------------------------
			// MUST REMAIN THE LAST MEMBER OF THIS CONTRUCTOR : because it calls this.placeDiv() function :
			// See : andrewdupont.net/2006/05/18/javascript-associative-arrays-considered-harmful/
			this.contents=new Object();
			this.mashedContents=new Object();
			if(!contents || typeof contents==="string"){
				this.setContent(contents?contents:"");
			}else{
				for(keyLanguage in contents){
					if(!contents.hasOwnProperty(keyLanguage))	continue;
					this.setContent(contents[keyLanguage],keyLanguage);
				}
			}
		}
		
	
		// Initialization :
		/*private*/,initContentDiv:function(){
			var self=this;
			
			var aotraScreen=this.getAotraScreen();
			var mainDiv=aotraScreen.mainDiv;
			var isScreenMobile=aotraScreen.isMobile();
			
			// content :
			this.div=document.createElement("div");
			this.div.className=BUBBLE_DIV_CLASSNAME+(isScreenMobile?" mobile":"");

			if(!isScreenMobile)	this.div.title=this.getName()+" - "+TOOLTIP;
			else							  this.div.title=this.getName()+" - "+TOOLTIP_MOBILE;
			
			mainDiv.appendChild(this.div);
			
			// ------------------------------------------------------------------------------------------------------------
			// Click reactions behavior :
			if(nothing(this.openedOnCreate)
					|| (this.openedOnCreate!==REMAIN_CLOSED && this.openedOnCreate!==REMAIN_HIDDEN)){
				// Default behavior :
				if(!isScreenMobile){
					this.div.ondblclick=function(event){
						self.onPoint();
					};
					this.div.onclick=function(event){
						self.onFocus(event);
					};
				}else{
					// A bit special behavior if we are on mobile platforms
					// (we cannot close a bubble that is not «remainClosed/Hidden», once open !) :
					this.div.onclick=function(event){
						self.onFocus(event);
						if(!self.isOpen())	self.doOpen();
					};
				}
			}else if(this.openedOnCreate===REMAIN_CLOSED || this.openedOnCreate===REMAIN_HIDDEN){
				this.div.onclick=function(event){
					self.onFocus(event);
					if(!self.isOpen())	self.doOpen();
				};

				var listener=new Object();
				listener.doAction=function(){
					if(self.isOpen())	self.cascadeClose();
				};
				aotraScreen.getOnClickInVoidEventListeners().push(listener);
				
			}
			// ------------------------------------------------------------------------------------------------------------

			
			// To compensate potential map background provider effects :
			this.div.setStyle("pointer-events:all");
			
			
			// An alternative way to open or close bubbles on mobile devices :
			if(		(isScreenMobile || IS_DEVICE_MOBILE_CALCULATED) && 
					this.openCloseProvider){
				// Button class is set in refreshOpenCloseButtonClass(...) method
				this.openCloseButtonSlct=jQuery("<button class='icon'></button>").css("position","absolute");
				this.openCloseButtonSlct.click(function(event){
					if(self.isOpen())		self.doClose();
					else								self.doOpen();
				});
				this.openCloseButtonSlct.hide();
				this.refreshOpenCloseButtonClass(this.openedOnCreate==="true" || this.isOpen());
				jQuery(aotraScreen.mainDiv).append(this.openCloseButtonSlct);

			}

			// UNUSED FOR NOW :
			// DEPENDENCY : AotraScreenAdvancedNavigationProvider code in this class !
			// Bubble history shortcut
			/*if(aotraScreen.advancedNavigationProvider && !this.previousBubbleButtonSlct){
				this.previousBubbleButtonSlct=jQuery(aotraScreen.advancedNavigationProvider.getPreviousBubbleButtonHtml()).css("position","absolute");
				jQuery(aotraScreen.mainDiv).append(this.previousBubbleButtonSlct);
			}*/

			// DEPENDENCY : BubbleLinksNavigationProvider code in this class !
			this.linksNavigationProvider=new BubbleLinksNavigationProvider(aotraScreen,this);
			
		}
		
		/*private*/,refreshOpenCloseButtonClass:function(open){
			if(open)	this.openCloseButtonSlct.addClass("invisibleBtn").removeClass("visibleBtn");
			else		this.openCloseButtonSlct.addClass("visibleBtn").removeClass("invisibleBtn");
		}
		
		/*private*/,doOnceReady:function(aotraScreen,isScreenMobile,isScreenEditable){

			var bubbleIniters=Bubble.hooks[INIT_AFTER_READY];
			for(key in bubbleIniters){
				if(!bubbleIniters.hasOwnProperty(key))	continue;
				bubbleIniters[key].initBubble(this,aotraScreen,isScreenMobile,isScreenEditable);
			}
			
			// This hook is special, it is intented to be used and declared super simply, and only once :
			if(!nothing(window.afterInit)){
				window.afterInit(this.getDiv());
			}
			
		}
		
		// Here, we render bubble's information and content to final display on aotraScreen :
		/*public*/,placeDiv:function(/*OPTIONAL*/refreshContent){
			var aotraScreen=this.getAotraScreen();
			var activePlane=aotraScreen.getActivePlane();
			
			// THIS SHOULD NEVER BE THE CASE WHERE activePlane IS NULL !
			// TODO : FIXME : Determine where are done the call to placeDiv(...) whereas activePlane is still null, and cut it !
			if(!activePlane)	return;
			
			var isScreenMobile=aotraScreen.isMobile();
			var isScreenEditable=(aotraScreen && aotraScreen.isCalculatedEditable()===true);
			
			// Even if bubble is closed or invisible, or not in active plane, we must load the content anyway
			// (even if it will not be visible) for possible special treatments :
			
			// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider
			if(	 activePlane.verticalScrollingProvider 
				&& activePlane.hooksParameters[SCROLL_HOOK_PARAM]===VERTICAL_SCROLLING_MODE
				){
				// Vertical scrolling displaying :
				// CAUTION : canShowBubble attribute is inapplicable here, because of outOfView calculated characteristic :
				activePlane.verticalScrollingProvider.placeDivScroll(this,refreshContent);
			
			}else{
				// Heuroptic displaying : (default)
				this.placeDivHeuroptic(refreshContent);
			}
			
			// We indicate that bubble is ready :
			if(!this.ready)	this.doOnceReady(aotraScreen,isScreenMobile,isScreenEditable);
			else this.ready=true;

		}
		
		// Heuroptic displaying : (default)
		/*public*/,placeDivHeuroptic:function(/*OPTIONAL*/refreshContent){
			
			var aotraScreen=this.getAotraScreen();
			var activePlane=aotraScreen.getActivePlane();
			
			var isScreenMobile=aotraScreen.isMobile();
			var isScreenEditable=(aotraScreen && aotraScreen.isCalculatedEditable()===true);
			
			// Even if bubble is closed or invisible, or not in active plane, we must load the content anyway
			// (even if it will not be visible) for possible special treatments :
			
			
			var displayX=this.getDisplayXY(LEFTUP_CORNER)["x"];
			var displayY=this.getDisplayXY(LEFTUP_CORNER)["y"];
			
			var canShowBubble = this.canShowBubble();
			
			var style="";
			
			
			style += this.visibilityDisplayCSS();
			
			
			// UNUSED FOR NOW :
			// DEPENDENCY : AotraScreenAdvancedNavigationProvider code in this class !
			// Bubble history shortcut button :
			/*if(aotraScreen.advancedNavigationProvider && this.previousBubbleButtonSlct){
				if(canShowBubble	
						&& this.isNavigable() && !this.isFixed() && !this.getIsHomeBubble()){
					// Bubble history button visibility and position refresh
					this.previousBubbleButtonSlct.show();
					this.previousBubbleButtonSlct
					// LEFT :
					.css("left",(displayX-GLOBAL_ICONS_SIZE)+"px")
					.css("top", (displayY+this.h*aotraScreen.getZoomFactor()/2-(GLOBAL_ICONS_SIZE/2))+"px");
				}else{
					// Bubble history shortcut button visibility refresh
					if(this.previousBubbleButtonSlct.css("display")!=="none") this.previousBubbleButtonSlct.hide();
				}
			}*/
			
			// Position displaying
			style+=this.positionDisplayCSS(displayX,displayY);
			// Opacity management when opening / closing
			style+=this.opacityAsOpenedClosedDisplayCSS();
			// Size displaying
			style+=this.sizeDisplayCSS();
			// Mold displaying
			style+=this.moldDisplayCSS();
			
			// DO NOT FIX ME (this not a challenge...so bad these treatments really actually need to be 
			// done only when bubble is created, and when it's modified, but it introduced really too
			// much complexity and added too much instability for little benefits, 
			// only to make it be executed only when necessary. So please do never fix this !)
			// Background color and opacity displaying :
			style+=this.backgroundDisplayCSS();
			// Border :
			style+=this.borderDisplayCSS();
			
			this.applyStyle(style,true);
			
			// Text container div refresh :
			this.displayContent(refreshContent);
			
			// DEPENDENCY : BubbleLinksNavigationProvider code in this class !
			// Bubble links navigation buttons :
			this.displayBubbleLinksNavigationButtons(canShowBubble);
			
		}
		
		
		// ----------------------- Visibility functions : -----------------------

		/*private*/,visibilityDisplayCSS:function(){
			var aotraScreen=this.getAotraScreen();
		
			var displayX=this.getDisplayXY(LEFTUP_CORNER)["x"];
			var displayY=this.getDisplayXY(LEFTUP_CORNER)["y"];
			
			var style="";
			
  		// Main bubble visibility displaying :
  		// (there is no return if we cannot display bubble, because
  		// if we cannot display bubble, there are treatments to do to hide elements)
  		var canShowBubble = this.canShowBubble();

  		if(canShowBubble){
  			
  			style+=" display:block !important;";
  			
  			if(this.openCloseButtonSlct){
  				if(this.openCloseButtonSlct.css("display")==="none")	this.openCloseButtonSlct.show();
  				this.openCloseButtonSlct
  						// RIGHT:
  						.css("left",(displayX+this.w*aotraScreen.getZoomFactor()+(GLOBAL_ICONS_SIZE/2))+"px")
  						.css("top", (displayY+this.h*aotraScreen.getZoomFactor()/2-(GLOBAL_ICONS_SIZE/2))+"px")
  				;
  				this.refreshOpenCloseButtonClass(this.isOpen());
  			}
  
  		}else{
  			
  			style+=" display:none !important;";
  			
  			if(this.openCloseButtonSlct){
  				if(this.openCloseButtonSlct.css("display")!=="none")
  					this.openCloseButtonSlct.hide();
  			}
  			
  			// We cannot have a return statement here, because elements must be removed below.
  			// (cf. previous comment about this)
  		}
  		
  		return style;
		}
		
		// Open/close management :
		/*public*/,canOpenBubble:function(){
			return this.openedOnCreate!=="false" 
				 && (this.openedOnCreate!==REMAIN_CLOSED && this.openedOnCreate!==REMAIN_HIDDEN);
		}
		 
		// Visibility management :
		// These methods and attributes concern the visible display zone only :
		// DEPENDENCY : Vertical scroll provider here !
		/*public*/,canShowBubble:function(isVerticalMode){
			return !this.isMustNeverBeShown(isVerticalMode) && !this.isClosedAndRemainHiddenMold();	
		}
		
		/*private*/,isClosedAndRemainHiddenMold:function(){
			// If bubble is closed and is in «remain hidden» mode :
			return ( !this.open && this.openedOnCreate===REMAIN_HIDDEN
// NOT ANY MORE :					 // An exception is made to this rule if screen is editable :
//					 							 && !isScreenEditable
					 );
		}
		
		// DEPENDENCY : Vertical scroll provider here !
		/*private*/,isMustNeverBeShown:function(isVerticalMode){
			return (this.isVisible()===false 
					// Because the out-of-view notion only concerns the (default) heuroptic mode :
					|| (this.isOutOfView() && !isVerticalMode)
					|| !this.isInActivePlane());
		}
		
		// These attributes concern the bubble even out of the display zone :
		
		// This visibility is of the plane visibility layers AND for invisible mold to non-editors :
		,setIsVisible:function(visible){
			this.visible=visible;
			this.getAotraScreen().drawAndPlace();
		}
		
		/*public*/,isVisible:function(){
			
			// CAUTION : Sub-bubbles inherit their parent's visibility,
			// and that overrides their own visibility calculation :
			if(!this.isFirstLevelBubble()){
				return this.getParentBubble().isVisible();
			}
			
			var mold=this.getMold();
			// If bubble is with mold «invisible to view» but screen is in edition mode :
			return this.visible
			&& (this.getAotraScreen().isCalculatedEditable() 
					|| (mold!==INVISIBLE_TO_VIEW_MOLD && mold!==INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE)		
			);
		}
		
		// There's a slight difference between focusable and navigable bubbles :
		// Focusable is, when clicking specifically on the bubble, then screen focuses on it by centering the view on the bubble,
		/*public*/,isFocusable:function(){
			var result= this.fixedStrategy!==NON_FOCUSABLE && !this.isFixed();
			return result;
		}
		
		// Whereas Navigable means, that we can go to the bubble by clicking on a link anchor, or anything that centers the view on the bubble.
		// So to sum up, navigable is a set that contains entirely the focusable set (which so is a sub-set of navigable set).
		/*public*/,isNavigable:function(){
			var mold=this.getMold();
			// so a non-focusable bubble is necessarily non navigable :
			return this.isFocusable() && 
				(this.isVisible() || mold===INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE);
		}
		
		// ----------------------- Display methods : -----------------------

		,sizeDisplayCSS:function(){
			
			var w=this.getDisplayW();
			var h=this.getDisplayH();
			var aotraScreen=this.getAotraScreen();
//			var isScreenMobile=aotraScreen.isMobile();
	
	//		if(!isScreenMobile){
			var style= " width:"+Math.round(w)+"px; height:"+Math.round(h)+"px;";
	//		}else{
	//			var PERCENT=0.9;
	//			style+=
	//				 " width: "+(PERCENT*100)+"%;" 
	//				+" height: "+(PERCENT*100)+"%;";
	//		}
				
			return style;
		}
		
		,positionDisplayCSS:function(displayX,displayY){
			
			var style=
			 " position:"+(this.isFixed()?"fixed":"absolute")+";" 
			+" left:"+Math.round(displayX)+"px;"
			+" top:"+Math.round(displayY)+"px;"
			+" z-index:"+this.zIndex+";";
		
			return style;
		}	
		
		,backgroundDisplayCSS:function(){
			var colorStyle=this.color;
			var split=colorStyle.split(":");
			if(!empty(split) && isNumber(split[1])){
				var colors=htmlColorCodeToDecimalArray(colorStyle);
				// fail-safe :
				colorStyle=" rgb("+colors[0]+","+colors[1]+","+colors[2]+");";
				colorStyle+=" background-color:rgba("+colors[0]+","+colors[1]+","+colors[2]+","+parseFloat(split[1])+")";
			}
			
			var style=
			 " background-color:"+colorStyle+";" 
	//		+" display: block; "
			 ;
			
			return style;
		}
		
		,borderDisplayCSS:function(){
			var style="";
			var mold=this.getMold();
			
			var isPolygonShapesSupported=isChrome();

			// (TODO : FIXME : since we use clip today, then border is not applied if we are in polygon :)
			// (since a polygonial clip is only a part of the original rectangular div)
			if( !((mold===PENTAGON_MOLD 
				|| mold===HEXAGON_MOLD
				|| mold===HEPTAGON_MOLD
				|| mold===OCTOGON_MOLD)
				// TODO : FIXME :
				&& isPolygonShapesSupported ) // CAUTION : Firefox and IE currently do not support CSS shapes today (11/2015) 
				)	style+=" border:"+this.borderStyle+";";
			
			return style;
		}
		
		,opacityAsOpenedClosedDisplayCSS:function(){
			var style="";
			if(this.open)	style= " filter:alpha(opacity=100);opacity:1;";
			else			style= " filter:alpha(opacity="+(CLOSED_OPACITY*100)+");opacity:"+CLOSED_OPACITY+";";
		
			return style;
		}
		
		,moldDisplayCSS:function(){
		
			var style="";
			
			var w=this.getDisplayW();
			var h=this.getDisplayH();
			var mold=this.getMold();

			
			var isPolygonShapesSupported=isChrome();
			// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
			// var isExcludingText=this.getIsExcludingText() && this.isFirstLevelBubble();
			
			if(mold===SQUARED_MOLD 
					// Invisible bubbles are always square :
					|| mold===INVISIBLE_TO_VIEW_MOLD
					|| mold===INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE){
				style+=" text-align:justify; "
					 +" overflow-x:hidden; overflow-y:auto; word-wrap:break-word; ";
				style+=" -moz-border-radius: 0px; border-radius: 0px;";
				
				// ...But invisible bubbles have a nice shadow :
				if(mold===INVISIBLE_TO_VIEW_MOLD){
					// SHADOW DOESN'T WORK ON OLD CHROME :
					style+=" box-shadow: 0px 0px 20px 20px rgba(0, 0, 0, 0.8); ";
				}else if(mold===INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE){
					// SHADOW DOESN'T WORK ON OLD CHROME :
					style+=" box-shadow: 0px 0px 20px 20px rgba(255, 255, 255, 0.8); ";
				}
				
				// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
				// if(isExcludingText)	style+=" shape-outside: polygon(<TODO...>); ";
				
			}else if(mold===ROUNDED_MOLD){
				
				var moyWH=((w+h)/2);
				
				style+=" text-align:left;"
					 +" overflow-x:hidden; overflow-y:auto; word-wrap:break-word; ";
				style+=" -moz-border-radius: "+( moyWH /4)+"px; border-radius: "+( moyWH /4)+"px;";
			
				// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
				// if(isExcludingText)	style+=" shape-outside: polygon(<TODO...>); ";
				
			}else 
				// Polygon molds are treated exactly like circle mold :
				if(mold===CIRCLE_MOLD
					 ||	(mold===PENTAGON_MOLD 
					 || mold===HEXAGON_MOLD
					 || mold===HEPTAGON_MOLD
					 || mold===OCTOGON_MOLD)		
				){
				
				var radius=((w!==0?w:h)/2);// w==h in these cases of molds
				
				
				// In circular (or polygonial) bubbles, text is always centered by default :
				style+=" text-align:center; overflow:hidden; ";
				
				 if( (mold===PENTAGON_MOLD 
						 || mold===HEXAGON_MOLD
						 || mold===HEPTAGON_MOLD
						 || mold===OCTOGON_MOLD)
					// TODO : FIXME :
					&& isPolygonShapesSupported // CAUTION : Firefox and IE currently do not support CSS shapes today (11/2015) 
					){
					 
					// CAUTION : USING POLYGON MOLDS WILL MAKE BORDER NOT APPEAR
					// (since a polygonial clip is only a part of the original rectangular div)
	
					var numberOfPoints=5;	// Default is pentagon
					
					if(mold===HEXAGON_MOLD)	numberOfPoints=6;
					else if(mold===HEPTAGON_MOLD)	numberOfPoints=7;
					else if(mold===OCTOGON_MOLD)	numberOfPoints=8;
					
					var coordString=getPointsOnCircle(w/2,h/2,radius,numberOfPoints,"cssString");
					
					var polygonString="polygon("+coordString+")";
					style+=" clip: "+polygonString + "; "
						 +" clip-path: "+polygonString + "; "
						 +" -webkit-clip-path :"+polygonString + "; "
						 +" -moz-clip-path :"+polygonString + "; "
						 +" shape-inside: "+polygonString + "; ";
					
					// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
					// if(isExcludingText)	style+=" shape-outside: "+polygonString + "; ";
					 
				 }else{
					 
					// Case circle mold, or circle mold forced if polygons are not supported by browser :
	//				style+=" -moz-border-radius: "+radius+"px; border-radius: "+radius+"px;";
					style+=" -moz-border-radius: 50%; border-radius: 50%;";
					
					// TODO FIXME : Unfortunately, today (11/2015), CSS does not work as so it could work that easily...:
					// if(isExcludingText)	style+=" shape-outside: circle(<TODO...>); ";
					 
				 }
				
			}
			// TODO : FIXME :
			// CAUTION : Firefox and IE currently do not support CSS shapes today (11/2015)
			// TRACE
			if(	 (mold===PENTAGON_MOLD 
				|| mold===HEXAGON_MOLD
				|| mold===HEPTAGON_MOLD
				|| mold===OCTOGON_MOLD) && !isPolygonShapesSupported)
				log("WARN : Currently, your prehistoric browser does not support CSS shapes.");// CAUTION : Firefox and IE currently do not support CSS shapes today (11/2015) 
			
			
			// Box shadow for invisible molds :
			if(	 mold!==INVISIBLE_TO_VIEW_MOLD
				&& mold!==INVISIBLE_TO_VIEW_MOLD_BUT_NAVIGABLE){
				// We remove the "invisible-indicating" nice shadow :
				style+=" box-shadow: none;";
			}
			
			return style;
		}
		
		,displayContent:function(/*OPTIONAL*/refreshContent){
		
			var aotraScreen=this.getAotraScreen();
			var isScreenMobile=aotraScreen.isMobile();

			var fontSize=this.getDisplayFontSize();

			// jQuery sometimes just doesn't work...
			//var textContainerSelector=jQuery("#"+this.div.id+">.bubbleText");
			var textContainer=this.textDiv;
			
			// First time content rendering is done :
//			if(empty(textContainerSelector.get())){
			if(!textContainer){

				this.doFirstTimeContentRendering(aotraScreen,isScreenMobile,fontSize);
				
				// Hooks
				var hooks=Bubble.hooks[DO_ON_CREATE_TEXT_DIV];
			    for (var i=0, hook; hook = hooks[i]; i++) {
			    	hook.execute(aotraScreen,this);
				}
				
			}else{
				// All other later times :
				
//				if(this.open)	textContainerSelector.css("display","block");
//				else 			textContainerSelector.css("display","none");
//				textContainerSelector.css("font-size",(fontSize)+"px");// important css attribute to override content eventual sizing, even if «px» is not forbidden
				
				// CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
				
				if(this.open)	textContainer.setStyle("display:block");
				else 					textContainer.setStyle("display:none");
//				textContainer.style.fontSize=fontSize+"px !important";// important css attribute to override content eventual sizing, even if «px» is not forbidden
//				textContainer.style.fontSize=Math.round(fontSize)+"px !important";// important css attribute to override content eventual sizing, even if «px» is not forbidden
				textContainer.setStyle("font-size:"+(fontSize)+"px !important");// important css attribute to override content eventual sizing, even if «px» is not forbidden
				
				// CAUTION : forcing content refresh is time and resources-consuming
				if(refreshContent){
//					textContainerSelector.html(this.getContent());
					textContainer.innerHTML=this.getContent();

	//				// We want the carousel appear differently to the final screen display than in the bubble content editor :
	//				// That's an exception to the WYSYWYG principle :
	//				AotraScreenCarouselListWrappingProvider.doRenderingTreatments(this,aotraScreen);
					
					this.doOnContentRender(aotraScreen);
					
	//				// DEPENDENCY : BubbleLinksNavigationProvider code in this class !
	//				// Bubble links navigation buttons :
	//				if(this.linksNavigationProvider){
	//					if(canShowBubble	&& this.isNavigable() && !this.isFixed()){
	//						// Bubble links navigation buttons visibility and position refresh
	//						this.linksNavigationProvider.refreshLinksNavigationButtons();
	//					}
	//				}
					
				}
					
			}
		}
		
		
		/*private*/,displayBubbleLinksNavigationButtons:function(canShowBubble){
		
			// DEPENDENCY : BubbleLinksNavigationProvider code in this class !
			// Bubble links navigation buttons :
			if(this.linksNavigationProvider){
				if(canShowBubble && !this.isFixed()){
					// Bubble links navigation buttons visibility and position refresh
					this.linksNavigationProvider.showAllButtons();
				}else{
					// Bubble links navigation buttons visibility refresh
					this.linksNavigationProvider.hideAllButtons();
				}
			}

		}
		
		// ----------------------------------------------

		
		/*private*/,doOnContentRender:function(aotraScreen){
			// We want the carousel appear differently to the final screen display than in the bubble content editor :
			// That's an exception to the WYSYWYG principle :
			AotraScreenCarouselListWrappingProvider.doRenderingTreatments(this,aotraScreen);
		
			// Hooks
			var hooks=Bubble.hooks[DO_ON_CONTENT_RENDER];
		    for (var i=0, hook; hook = hooks[i]; i++) {
		    	hook.execute(aotraScreen,this);
			}

			
		}
		
		/*private*/,doFirstTimeContentRendering:function(aotraScreen,isScreenMobile,fontSize){
			
			var style="";
	
			style+=" word-wrap:break-word;"
			+" display:inline-block;"
			+" width:"+(TEXT_COVERAGE_AREA_PERCENT*100)+"%;"
			+" overflow:hidden;";
			
		
			//				// A nice CSS3 attribute : NOT IMPLEMENTD YET BY BROWSERS !
			//				var radius=(diameter/2);
			//				style+="shape-inside:circle(50% at "+radius+"px);";
			
			// Necessary for text to not be displayed outside bubble :
			style+=" margin:auto;";
			
			if(this.open)	style+=" display:block;";
			else 			style+=" display:none;";
			
			style+=" font-size:"+fontSize+"px !important;";// important css attribute to override content eventual sizing, even if «px» is not forbidden
			
			
//			var textContainerDivHTML="<div class='bubbleText"+(isScreenMobile?" mobile":"")+"' ";
//			textContainerDivHTML+=" style='"+style+"'>";
//			textContainerDivHTML+=this.getContent();
//			textContainerDivHTML+="</div>";
	
			var textDiv=document.createElement("div");
			textDiv.className= BUBBLE_TEXT_CLASSNAME +(isScreenMobile?" mobile":"");
			textDiv.style.cssText=style;
			textDiv.innerHTML=this.getContent();
			this.textDiv=textDiv;
			this.div.appendChild(this.textDiv);
			
//			this.div.innerHTML = textContainerDivHTML;
			
	
//			// We want the carousel appear differently to the final screen display than in the bubble content editor :
//			// That's an exception to the WYSYWYG principle :
//			AotraScreenCarouselListWrappingProvider.doRenderingTreatments(this,aotraScreen);
		
			this.doOnContentRender(aotraScreen);

			
		}
	
		,isOpen:function(){
			return this.open;
		}
		
		// Bubble coordinates :
		
		,getOffsetBecauseOfParentXY:function(){
			var result=new Array();
			
			var parentBubble=this.getParentBubble();
			if(parentBubble && parentBubble!=null){
				var pxy=parentBubble.getOffsetBecauseOfParentXY();
				result["x"]=pxy["x"]+parentBubble.x;
				result["y"]=pxy["y"]+parentBubble.y;
			}else{
				// If bubble is first level bubble :
				result["x"]=0;
				result["y"]=0;
			}
			return result;
		}

		,getDisplayXY:function(displayMode){
			var aotraScreen=this.getAotraScreen();
			
			var result=new Array();
			
			if(this.isFixed()===false){
				
				if(displayMode==LEFTUP_CORNER){
					result["x"]=(this.x-this.w/2);
					result["y"]=(this.y-this.h/2);
				}else if(displayMode==CENTER){
					result["x"]=this.x;
					result["y"]=this.y;
				}else{
					alert("Error calling bubble method getDisplayXY : no display mode specified.");
					return result;
				}
				
				// Coordinates update if bubble has parents :
				var pxy1=this.getOffsetBecauseOfParentXY();
				result["x"]=(result["x"]+pxy1["x"])
							*aotraScreen.getZoomFactor()-aotraScreen.x+aotraScreen.getWidth()/2;
				result["y"]=(result["y"]+pxy1["y"])
							*aotraScreen.getZoomFactor()-aotraScreen.y+aotraScreen.getHeight()/2;
				
			}else{	// Fixed bubble positioning :
				
				if(displayMode==LEFTUP_CORNER){
					result["x"]=(this.x-this.w/2);
					result["y"]=(this.y-this.h/2);
				}else if(displayMode==CENTER){
					result["x"]=this.x;
					result["y"]=this.y;
				}else{
					// TRACE
					log("ERROR : Error calling bubble method getDisplayXY : no display mode specified.");
					return result;
				}

				// Coordinates update if bubble has parents :
				var pxy2=this.getOffsetBecauseOfParentXY();
				result["x"]=(result["x"]+pxy2["x"]);
				result["y"]=(result["y"]+pxy2["y"]);

			}

			return result;
		}
		
		
		,isEquilateral:function(){
			return this.w===this.h;
		}
		,getDisplayW:function(/*OPTIONAL*/ignoreZoomFactor){
			var aotraScreen=this.getAotraScreen();
			if(this.isFixed()===true || ignoreZoomFactor)	return this.w;
			return this.w*aotraScreen.getZoomFactor();
		}
		,getDisplayH:function(/*OPTIONAL*/ignoreZoomFactor){
			var aotraScreen=this.getAotraScreen();
			if(this.isFixed()===true || ignoreZoomFactor)	return this.h;
			return this.h*aotraScreen.getZoomFactor();
		}
		
		
		/*private*/,getDisplayFontSize:function(){
			var aotraScreen=this.getAotraScreen();
			if(this.isFixed()===true)
				return this.fontSize;
			return this.fontSize*aotraScreen.getZoomFactor();
		}

		
			
		// Mouse events
		// META BUBBLE EVENT onFocus :
		// - a meta-event for focusing on a bubble (typically centering it in view)
		,onFocus:function(event){
			var aotraScreen=this.getAotraScreen();

			// NO : This method must never be called outsite the AotraScreen and must remain private, since it is the only central click interception point.
			// Or else it will be called more that once, what we don't want.
			
			// Must be placed before .setSelectedBubble to allow temporary aotraScreen onClick de-activation :
			// CAUTION: It is NOT handled in aotraScreen.onClickAotraScreen section «bubble priority»
			if(this.isFocusable())		aotraScreen.onClickAotraScreen(event,this);
			
			var editionProvider=aotraScreen.editionProvider;
			if(editionProvider){
				editionProvider.setSelectedBubble(this);
			}
			
		}
		// META BUBBLE EVENT onPoint :
		// - a meta-event for making specific point action on a bubble (typically opening and closing it)		
		,onPoint:function(){
			
			if(!this.openCloseProvider)	return;
			
			if(this.open==false){

				this.doOpen();
			}else{

				this.cascadeClose();
			}
		}
		
		
		// Methods
		
		// UNUSED
		,getRandomCoordinatesAround:function(area,/*OPTIONAL*/exclusionArea){
			var result=new Array();
			result.x=Math.getRandomIntAroundValue(this.x,area,exclusionArea);
			result.y=Math.getRandomIntAroundValue(this.y,area,exclusionArea);
			return result;
		}
		
		,cascadeClose:function(){
			
			if(!this.openCloseProvider)	return;
			
			this.doClose();
			var subBubbles=this.getSubBubbles();
			for(var i=0;i<subBubbles.length;i++)
				subBubbles[i].cascadeClose();			
		}
		
		// Attributes management
		/*public*/,getContents:function(){
			return this.contents;
		}
		
		/*public*/,getContent:function(/*OPTIONAL*/languageParam){
			
			var language=this.getCurrentLanguage(languageParam);
			
			var content=this.contents[language];
			if(nothing(content)){
				content=this.contents[DEFAULT_LANGUAGE];
			}
			
			return content;
		}
		
		/*public*/,getMashedContent:function(/*OPTIONAL*/languageParam){
			
			var language=this.getCurrentLanguage(languageParam);
			
			
			var mashedContent=this.mashedContents[language];
			if(nothing(mashedContent)){
				mashedContent=this.mashedContents[DEFAULT_LANGUAGE];
			}
			
			return mashedContent;
		}
		
		/*private*/,getCurrentLanguage:function(/*OPTIONAL*/languageParam){
			var aotraScreen=this.getAotraScreen();

			if(!aotraScreen.i18nProvider)	return DEFAULT_LANGUAGE;
			
			var language=aotraScreen.i18nProvider.getCurrentLanguage();
			if(typeof languageParam!=="undefined" && languageParam!==null)	language=languageParam;
			
			return language;
		}
		
		,setContent:function(contentParam,/*OPTIONAL*/languageParam){

			// TO CORRECT A NICEEDIT-CAUSED BUG :
			var content=contentParam.replace(/\<br>/gim,"<br />");

			
			var language=this.getCurrentLanguage(languageParam);


			this.contents[language]=content;
			this.mashedContents[language]=this.mashContent(content);
			
			
			this.placeDiv(true);

		}
		
		/*public*/,hasContent:function(language){
			return typeof this.contents[language]!=="undefined" && this.contents[language]!==null;
		}

		
		,removeContent:function(language){
			// We cannot delete default content :
			if(language===DEFAULT_LANGUAGE)	return;
			delete this.contents[language];
			delete this.mashedContents[language];
		}


		/*private*/,mashContent:function(content){
			var result="";
			
			var sanitizedContent=toOneSimplifiedLine(removeHTML(content.toLowerCase()),"hard");

			var r=/[\s\r\n]+/igm;
			var splittedContent=sanitizedContent.split(r);
			
			var factorizedContent=new Array();
			for(var i=0;i<splittedContent.length;i++){
				var e=splittedContent[i];
				if(factorizedContent.indexOf(e)==-1){
					var str=e.trim();
					factorizedContent.push(str);
					result+=(nothing(result)?"":" ")+str;
				}
			}
			
			return result;
		}

		/*public*/,getLanguages:function(){
			var list=new Array();
			for(key in this.contents){
				if(!this.contents.hasOwnProperty(key))	continue;
				list.push(key);
			}
			
			// At this point, this array is supposed to always have default language,
			// since every bubble at least has a content with default language as language. 
			return list;
		}
		
		
		
		/*public*/,applyStyle:function(cssStyleStr,/*OPTIONAL*/overwrite){
			// We cannot use Prototype .setStyle(...) method here, because the style needs to be completely replaced here :
			if(overwrite)	this.div.style.cssText=cssStyleStr;
			else			jQuery(this.div).css(cssStringToJQueryObject(cssStyleStr));
		}
		
		,setColor:function(color){
			this.color=color;
			this.placeDiv();
		}
		,setBorderStyle:function(borderStyle){
			this.borderStyle=borderStyle;
			this.placeDiv();
		}
		/*public*/,getBorderStyle:function(){
			return this.borderStyle;
		}
		/*public*/,getColor:function(){
			return this.color;
		}
		
		,setMaxW:function(w){
			this.maxW=w;
			if(!this.open)		this.doOpen();
			this.w=w;
			this.placeDiv();
		}
		,setMaxH:function(h){
			this.maxH=h;
			if(!this.open)		this.doOpen();
			this.h=h;
			this.placeDiv();
		}
		
		,getMaxW:function(){
			return this.maxW;
		}
		,getMaxH:function(){
			return this.maxH;
		}
	
		,setMold:function(mold){
			this.mold=mold;
			this.placeDiv();
		}
		,setVisibilityLayers:function(visibilityLayers){
			this.visibilityLayers=visibilityLayers;
			this.placeDiv();
		}
		,setOpenedOnCreate:function(openedOnCreate){
			this.openedOnCreate=openedOnCreate;
		}
		
		
		// CAUTION : This attribute only stores whether the bubble is intended to be openable/closable,
		// but this attribute does not make the bubble to be effectively openable/closable.
		// Only the non-nullity or nullity of attribute openCloseProvider does and must do this !
		/*public*/,isOpenableClosable:function(){
			return this.openableClosable;
		}
		,setIsOpenableClosable:function(openableClosable){
			if(openableClosable && !this.openCloseProvider)	this.openCloseProvider=new Object();
			this.openableClosable=openableClosable;
		}

		// We have to name these accessor that way, or the method name will shadow the attribute name, <
		// as in javascript methods are first-class object attributes.
		// TODO : FIXME : rename boolean  attributes to drop the «is-»" prefix
		
		,setIsHomeBubble:function(isHomeBubble){
			var aotraScreen=this.getAotraScreen();
			// DEPENDENCY : PLANE ADVANCED NAVIGATION PROVIDER
			if(isHomeBubble && aotraScreen.advancedNavigationProvider){
				aotraScreen.advancedNavigationProvider.setHomeBubble(this);
			}
			this.isHomeBubble=isHomeBubble;
		}
		,getIsHomeBubble:function(){
			return this.isHomeBubble;
		}
		
		,setIsExcludingText:function(isExcludingText){
			this.isExcludingText=isExcludingText;
		}
		,getIsExcludingText:function(){
			return this.isExcludingText;
		}
		
		,setParallaxBackground:function(parallaxBackground){
			this.parallaxBackground=parallaxBackground;
		}
		
		/*public*/,getMold:function(){
			return this.mold;
		}
		
		/*public*/,equals:function(otherBubble){
			if(nothing(otherBubble) || nothing(otherBubble.getName))	return false;
			return this.getName()===otherBubble.getName();
		}
		
		,isFixed:function(){
			var result=this.fixedStrategy===FIXED;
			return result;
		}
		
		,setFixedStrategy:function(fixedStrategy){
			// Default behavior is non fixed :
			this.fixedStrategy=!fixedStrategy?"":fixedStrategy;
			this.placeDiv();
		}
		
		,getFixedStrategy:function(){
			return this.fixedStrategy;
		}

		,getName:function(){
			return this.name;
		}
		
		,setName:function(name,aotraScreen){
			this.name=name;
			if(this.div)
				this.div.id=this.getBubbleDivId();
		}
		
		/*private*/,getBubbleDivId:function(){
			return BUBBLE_DIV_PREFIX+"_"+this.getName();
		}
		
		/*public*/,getDiv:function(){
			return this.div;
		}
		
		/*public*/,getTextDiv:function(){
			return this.textDiv;
		}
		
		// THESE METHODS ACTUALLY EXIST FOR Bubble CLASS: (but they are defined elsewhere :)
		//
		// doOpen();
		//
		// doClose();
		
		// Only a refreshing method, DO NOT USE IT TO TRIGGER OPENING OR CLOSING,
		// use doOpen() or doClose() instead (these actually use this method):
		,setOpen:function(open){
			
			// Bubbles that are not openableClosable are always open :
			if(!this.openCloseProvider && open===false)	return;

			this.open=open;
			this.getAotraScreen().draw();
			this.placeDiv();
		}

		// Sub-bubbles management
		/*public*/,addBubble:function(subBubble,/*OPTIONAL*/updateNewSubBubleCoordinates){
			// To compensate an unwanted coordinates shift :
			var newParentBubble=this;
			
			subBubble.setParentBubble(newParentBubble);

			if(updateNewSubBubleCoordinates){

				var psxy=subBubble.getOffsetBecauseOfParentXY();
				var x = newParentBubble.canonicalX + (subBubble.canonicalX-psxy.x);
				var y = newParentBubble.canonicalY + (subBubble.canonicalY-psxy.y);
				
				subBubble.setLocation(x,y);
			}
			
		}
		
		/*public*/,detachFromParentBubble:function(/*OPTIONAL*/updateNewSubBubleCoordinates){
			
			var oldParentBubble=this.getParentBubble();
			if(!oldParentBubble || oldParentBubble===null)	return;
			
			subBubble=this;
			subBubble.setParentBubble(null);

			// To compensate an unwanted coordinates shift :
			if(updateNewSubBubleCoordinates){
				// NOTE : on today, we actually always update coordinates :
				var x = oldParentBubble.canonicalX + subBubble.canonicalX;
				var y = oldParentBubble.canonicalY + subBubble.canonicalY;
				subBubble.setLocation(x,y);
			}
			

		}
		
		
		/*private*/,removeBubble:function(subBubble){
			var parentBubble=this;

			if(empty(parentBubble.getSubBubbles()))	return;
			
			remove(parentBubble.getSubBubbles(),subBubble);
			
		}
		/*public*/,setParentBubble:function(wishedParentBubble){
			// sub-bubble is this bubble :

			var currentParentBubble=this.getParentBubble();

			// It's not that complicated !
			// 	if bubble is sub-bubble : 		coords sub-bubble = canonic coords + parent's coords
			// 	if bubble is not sub-bubble : 	coords sub-bubble = canonic coords
			// transformations :
			// 	sub bubble -> bubble : new canonic coords bubble = coords bubble + parent's coords
			// 	bubble -> sub bubble : new canonic coords sub-bubble = coords bubble - parent's coords 
			
			var cx=this.canonicalX;
			var cy=this.canonicalY;
			
			// If we want this bubble to have no parent :
			if(!wishedParentBubble){
				// If bubble already has no parent, then nothing to do.
				if(!currentParentBubble)	return;
				currentParentBubble.removeBubble(this);
				
				// Must be done before, or else setLocation(...) method will behave as if this bubble is still a sub-bubble :
				this.parentBubble = null; // wishedParentBubble has always null here

				// All children bubbles must have their coordinates offset updated, to avoid them to be misplaced.
				// (at this point, we know bubble has had a parent and now wants to have none :)
				
				// Case «sub bubble -> bubble»
				var pxy=currentParentBubble.getOffsetBecauseOfParentXY();
				this.setLocation(cx+(pxy.x),cy+(pxy.y));
				
			}else{

				// If we want this bubble to have a parent :
				
				// If bubble already has wished parent as parent, then nothing to do.
				if(currentParentBubble && wishedParentBubble.getName()===currentParentBubble.getName())		return;	
				
				// If bubble was child of another bubble, then we tell the current parent that it no longer has this child :
				if(currentParentBubble)	currentParentBubble.removeBubble(this);

				// Then we make the link between the new wished parent and this bubble :
				wishedParentBubble.getSubBubbles().push(this);

				// All children bubbles must have their coordinates offset updated, to avoid them to be misplaced.
				
				// If bubble was a sub-bubble and remains one
				// or if bubble had no parent, and now has one :
				
				var pxy=wishedParentBubble.getOffsetBecauseOfParentXY();
				this.setLocation(cx+(pxy.x),cy+(pxy.y));

				this.parentBubble = wishedParentBubble;
				
			}

		}
		
		,setLocation:function(destinationXParam,destinationYParam){

			var destinationX=Math.round(destinationXParam);
			var destinationY=Math.round(destinationYParam);

			if(!this.getParentBubble()){

				// These coordinates are those affected by zoom factor :
				this.x=destinationX;
				this.y=destinationY;
				
				// These coordinates are those unaffected by zoom factor, and those exported in XML :
				this.canonicalX=destinationX;
				this.canonicalY=destinationY;

			}else{
				
				// If this bubble is sub-bubble, then we substract parents coordinates from destination coordinates,
				// Because set destination coordinates are the «real» coordinates, not the XML exported (ie. canonical) ones :
				var pxy=this.getOffsetBecauseOfParentXY();

				this.x=destinationX-pxy["x"];
				this.y=destinationY-pxy["y"];
				
				this.canonicalX=destinationX-pxy["x"];
				this.canonicalY=destinationY-pxy["y"];
			}
			
			this.placeDiv();

		}
		
		,getSubBubbles:function(){
			return this.subBubbles;
		}
		/*public*/,isFirstLevelBubble:function(){
			return !this.getParentBubble();
		}
		,getParentBubble:function(){
			return this.parentBubble;
		}
		
		
		
		,willBeAncestorOf:function(potentialParent){
			if(!potentialParent.parentBubble){
				if(potentialParent.getName() == this.getName())
					return true;
				return false;
			}
			return this.willBeAncestorOf(potentialParent.parentBubble);
		}
		
		,distanceTo:function(otherBubble){
//			return Math.sqrt(Math.pow(Math.abs(this.getDisplayXY(CENTER)["x"]-otherBubble.getDisplayXY(CENTER)["x"]),2)
//			+Math.pow(Math.abs(this.getDisplayXY(CENTER)["y"]-otherBubble.getDisplayXY(CENTER)["y"]),2));
			return Math.sqrt(Math.pow(Math.abs(this.canonicalX-otherBubble.canonicalX),2)
					+Math.pow(Math.abs(this.canonicalY-otherBubble.canonicalY),2));

		}
		
		
		
		/*private*/,isOutOfView:function(){
			
			var aotraScreen=this.getAotraScreen();
			
			var aotraScreenDisplayWidth=aotraScreen.getWidth();
			var aotraScreenDisplayHeight=aotraScreen.getHeight();
			
			var displayX=this.getDisplayXY(CENTER)["x"];
			var displayY=this.getDisplayXY(CENTER)["y"];

			var result=false;
			if(	(displayX<0 || aotraScreenDisplayWidth<displayX ) 
				|| (displayY<0 || aotraScreenDisplayHeight<displayY ))
				result=true;

			
			return result;
		}
		
		/*private*/,isInActivePlane:function(){
			var aotraScreen=this.getAotraScreen();
			var activePlane=aotraScreen.getActivePlane();
			if(!activePlane)	return false;
			return activePlane.isBubbleInPlane(this);
			
		}
		
		,getAotraScreen:function(){
			if(this.aotraScreen)	return this.aotraScreen;
			return getAotraScreen();
		}
		
		// Drawing
		
		// Selection mark :
		/*public*/,showSelectionMark:function(){
			var divSlct=jQuery(this.div);
			if(!divSlct.hasClass(SELECTED_CLASS_NAME))
				divSlct.addClass(SELECTED_CLASS_NAME);
		}
		/*public*/,hideSelectionMark:function(){
			var divSlct=jQuery(this.div);
			if(divSlct.hasClass(SELECTED_CLASS_NAME))
				divSlct.removeClass(SELECTED_CLASS_NAME);
		}
	});
	
	
	// STATIC CODE :
	// Hooks
	/*static*/{
		// Generic hooks : 
		initSelf.Bubble.hooks=new Object();
		initSelf.Bubble.hooks[INIT]=new Object();
		initSelf.Bubble.hooks[INIT_AFTER_READY]=new Object();
		initSelf.Bubble.hooks[DO_ON_CONTENT_RENDER]=new Array();// A simple hook
		initSelf.Bubble.hooks[DO_ON_CREATE_TEXT_DIV]=new Array();// A simple hook

	};
	
	
	return initSelf;
}
}
// GLOBAL CONSTANTS :

	var LINK_CSS_CLASS_NAME="link";
	var LINK_CSS_CLASS_NAME_BACKWARDS=LINK_CSS_CLASS_NAME+"Backwards";
	
	// Hooks constants
	const ONCLICK="onclick";
	const ONCLICK_BACKWARDS="onclickBackwards";

	
if(typeof initLinkClass ==="undefined"){
function initLinkClass() {
	const initSelf = this;

	// CONSTANTS
	const MOLD_LINE="line";
	const MOLD_INVISIBLE="invisible";
	const MOLD_LITTLEBEAM="littleBeam";
	const MOLD_DOTTED="dotted";
	const MOLD_SHAPE_CURVED="curved";
	const MOLD_SHAPE_ORTHOGONAL="orthogonal";
	const CONVOLUTED_PATH_ORIENTATION="vertical";
	const DASH_PIXEL_GAP_POSITIVE=5; 
	const DASH_PIXEL_GAP_NEGATIVE=5;
	const DEFAULT_BACKWARDS_LABEL=i18n({"fr":"Retour...","en":"Back..."});
	
	
	initSelf.Link = Class.create({

		// Constructor
		initialize : function(fromBubbleName,toBubbleName,planeName,
				color,size,mold,label
//				,borderStyle
				,routingStrategy
				,hooksParameters) {


			// Attributes
			this.fromBubbleName = fromBubbleName;
			this.toBubbleName = toBubbleName;
			this.planeName=planeName;
			
			this.label=label;
			this.size=size;
			this.mold=mold;
			this.color=color;
//			this.borderStyle=borderStyle;
			this.routingStrategy=routingStrategy; // TODO : develop...
			
			// Hooks
			this.hooksParameters=hooksParameters?hooksParameters:new Object();
			var linkIniters=Link.hooks[INIT];
			for(key in linkIniters){
				if(!linkIniters.hasOwnProperty(key))	continue;
				linkIniters[key].initLink(this);
			}
			
			
			// Processed attributes :
			// TODO : develop :
			this.backwardsLabel=(!config.link.forActivePlane().backwardsLabel 
						|| nothing(config.link.forActivePlane().backwardsLabel))? 
								DEFAULT_BACKWARDS_LABEL
								:config.link.forActivePlane().backwardsLabel;
			
			
		}

		// Methods

		,equals : function(otherLink,/*OPTIONAL*/planeName) {
			var result=this.equalNames(otherLink.fromBubbleName,otherLink.toBubbleName);
			if(!planeName)	return result;
			return result && this.planeName===planeName;
		}

		/*public*/,equalNames : function(fromBubbleName,/*OPTIONAL*/toBubbleName) {
			if(!toBubbleName)
				return this.fromBubbleName===fromBubbleName;
			else 
				return (this.fromBubbleName===fromBubbleName && this.toBubbleName===toBubbleName)
				|| (this.fromBubbleName===toBubbleName && this.toBubbleName===fromBubbleName);
		}
		
		,onClick:function(){
			var aotraScreen=getAotraScreen();
			
			// Hooks
			var self=this;
			foreach(initSelf.Link.hooks[ONCLICK],(i)=>{
				i.execute(self);
			});
			
			aotraScreen.doGotoBubbleByName(this.toBubbleName,true);
		}

		,onClickBackwards:function(){
			var aotraScreen=getAotraScreen();
			
			// Hooks
			var self=this;
			foreach(initSelf.Link.hooks[ONCLICK_BACKWARDS],(i)=>{
				i.execute(self);
			});
			
			aotraScreen.doGotoBubbleByName(this.fromBubbleName,true);
		}
		

		/*public*/,getEndsCanonicalCoordinates:function(aotraScreen){
			
			var result=new Object();
			
			var activePlane=aotraScreen.getActivePlane();
			
			var fromBubble=activePlane.getBubbleByName(this.fromBubbleName);
			var toBubble=activePlane.getBubbleByName(this.toBubbleName);
			
			
			result["x1"]=fromBubble.canonicalX;
			result["y1"]=fromBubble.canonicaly;
			result["x2"]=toBubble.canonicalX;
			result["y2"]=toBubble.canonicaly;
			
			return result;
		}
		
		/*public*/,getFromBubble:function(aotraScreen){
			var activePlane=aotraScreen.getActivePlane();
			return activePlane.getBubbleByName(this.fromBubbleName);
		}
		/*public*/,getToBubble:function(aotraScreen){
			var activePlane=aotraScreen.getActivePlane();
			return activePlane.getBubbleByName(this.toBubbleName);
		}
		
		/*public*/,getFromBubbleName:function(){
			return  this.fromBubbleName;
		}
		/*public*/,getToBubbleName:function(){
			return this.toBubbleName;
		}

		/*public*/,isInvisible:function(){
			return (this.mold===MOLD_INVISIBLE);
		}
		
		// Drawing
		,draw:function(aotraScreen){

			if(this.isInvisible())	return;
			
			var activePlane=aotraScreen.getActivePlane();
			
			// This should never happen :
			if(activePlane.getName()!==this.planeName)	return;
		
			var fromBubble=activePlane.getBubbleByName(this.fromBubbleName);
			var toBubble=activePlane.getBubbleByName(this.toBubbleName);

			if(!fromBubble || !toBubble)	return;
			
			if(fromBubble.isVisible()===false || toBubble.isVisible()===false)	return;
			
			var ctx=aotraScreen.ctx;

			var xy1=fromBubble.getDisplayXY(CENTER);
			var displayX1=xy1["x"];
			var displayY1=xy1["y"];
			
			var xy2=toBubble.getDisplayXY(CENTER);
			var displayX2=xy2["x"];
			var displayY2=xy2["y"];
			
			
			ctx.save();  
			ctx.beginPath();
			
			
			if(contains(this.mold,MOLD_LITTLEBEAM)){
				// CAUTION : «littleBeam» mold is always a only STRAIGHT LINE !

				ctx.strokeStyle=getBeamLikeGradient(ctx,displayX1,displayY1,displayX2,displayY2,this.size,this.color,1,aotraScreen.getZoomFactor());
				ctx.lineWidth=this.size*aotraScreen.getZoomFactor();
				
				ctx.moveTo(displayX1,displayY1);
				ctx.lineTo(displayX2,displayY2);

			}else{
				
				if(contains(this.mold,MOLD_DOTTED)){
				
				ctx.strokeStyle=this.color;	
				ctx.lineWidth=this.size*aotraScreen.getZoomFactor();
				
				// OLD : drawDashedLine(ctx,displayX1, displayY1, displayX2, displayY2);
				
				ctx.setLineDash([DASH_PIXEL_GAP_POSITIVE, DASH_PIXEL_GAP_NEGATIVE]);
				
					
				}else{
					// Case mold === «line» (default) or others :
				
					ctx.strokeStyle=this.color;	
					ctx.lineWidth=this.size*aotraScreen.getZoomFactor();
	//				if(this.borderStyle)	ctx.borderStyle=this.borderStyle;
	//				if(this.borderStyle)	ctx.strokeStyle=this.color+" "+this.borderStyle;
	//				else	ctx.strokeStyle=this.color;
					
					
				}

				// Link shape :
				ctx.moveTo(displayX1,displayY1);
				if(contains(this.mold,MOLD_SHAPE_CURVED)){
					
					let controlPoint1X, controlPoint1Y, controlPoint2X, controlPoint2Y;
					
					if(CONVOLUTED_PATH_ORIENTATION==="horizontal"){
						controlPoint1X=displayX2;
						controlPoint1Y=displayY1;
						controlPoint2X=displayX1;
						controlPoint2Y=displayY2;
					}else{
						controlPoint1X=displayX1;
						controlPoint1Y=displayY2;
						controlPoint2X=displayX2;
						controlPoint2Y=displayY1;
					}
					ctx.bezierCurveTo(controlPoint1X, controlPoint1Y, controlPoint2X, controlPoint2Y, displayX2, displayY2);
	
				}else if(contains(this.mold,MOLD_SHAPE_ORTHOGONAL)){
	
					let quadrant=Math.getQuadrant({x:displayX1,y:displayY1},{x:displayX2,y:displayY2});
					if(quadrant==="W" || quadrant==="E"){
						if(CONVOLUTED_PATH_ORIENTATION==="horizontal")	ctx.lineTo(displayX2,displayY1);
						else																						ctx.lineTo(displayX1,displayY2);
					}else{
						if(CONVOLUTED_PATH_ORIENTATION==="horizontal")	ctx.lineTo(displayX1,displayY2);
						else																						ctx.lineTo(displayX2,displayY1);
					}
					ctx.lineTo(displayX2,displayY2);
					
				}else{ // Default case is simple straight line :
					ctx.lineTo(displayX2,displayY2);
				}
				
			
			}
				
			ctx.stroke();

			ctx.closePath();
			ctx.restore();
			
			
		}

		,getHTMLTag:function(/*OPTIONAL*/isBackwardsNavigation){
			
			var A_TAG_PART_1="<span ";
			var A_TAG_PART_2=" >";
			var A_TAG_PART_3="</span>";
			
			var result="";
			
//			// OLD : We add a HTML new line just before link class, if it's a backwards link :
//			if(isBackwardsNavigation) result+="<br />";
			// We add a HTML new line just before link class, in all cases :
			result+="<br />";

			
			result+=A_TAG_PART_1;
			if(!isBackwardsNavigation)
				result+=' class="'+LINK_CSS_CLASS_NAME+'" ';
			else
				result+=' class="'+LINK_CSS_CLASS_NAME_BACKWARDS+'" ';

			if(!isBackwardsNavigation){
				result+=' data-to="'+this.toBubbleName + '" ';
			}else{
				// We invert link invocation (even if it will point to only one same direct link) to allow backwards navigation to work :
				result+=' data-to="'+this.fromBubbleName + '" ';
			}

			result+=A_TAG_PART_2;
			if(nothing(isBackwardsNavigation)){
				result+=this.label;
			}else{
				result+=this.backwardsLabel;
			}
			
			result+=A_TAG_PART_3;
			
			return result;
		}


	});

	
	// STATIC CODE :
	// Hooks
	/*static*/{
		// Generic hooks : 
		initSelf.Link.hooks={};
		initSelf.Link.hooks[INIT]={};
		initSelf.Link.hooks[ONCLICK]={};
		initSelf.Link.hooks[ONCLICK_BACKWARDS]={};
		
	};
	
	return initSelf;
}
}
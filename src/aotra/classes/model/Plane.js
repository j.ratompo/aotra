	// GLOBAL CONSTANTS


if(typeof initPlaneClass ==="undefined"){
function initPlaneClass() {
	var initSelf = this;

	// CONSTANTS
	
	initSelf.Plane = Class.create({
		
		// Constructor
		initialize : function(
				 name,parallaxBackground,simpleBackground
				,visibilityLayersConfig,visibilityLayersJunctionLogic,fractionOfImageUsed
				,linksButtonsDisplayStrategy
				,configuration
				,hooksParameters
				,aotraScreen) {

			// SELF
			var self=this;
			
			this.aotraScreen=aotraScreen;
			
			// Attributes
			if(name && name.trim()!="")
				this.name=name;
			else
				// TODO : FIXME : OPTIMIZATION :
				// Actually it will work, but it would probably be better to have a distinct planes index :
				// OLD : this.name="plane_"+this.getAotraScreen().nextBubbleIndex();
				this.name="plane_"+generateRandomString(12,"simple");
			
			this.simpleBackground=simpleBackground;
			this.parallaxBackground=parallaxBackground;
			
			this.bubbles = new Array();
			this.links = new Array();
			
			this.visibilityLayersConfig=visibilityLayersConfig;
			this.visibilityLayersJunctionLogic=visibilityLayersJunctionLogic;
			// Plane bubbles visibility layers usage capacities :
			this.visibilityLayersProvider;
			
			
			// TODO : pass this as a parameter to make aotraScreen customizable
			if(this.aotraScreen.getCanFilterBubbles()===true){
				this.visibilityLayersProvider = new PlaneBubblesFilteringProvider(this,this.visibilityLayersConfig,this.visibilityLayersJunctionLogic);
			}
			
			// This is a bare string :
			this.configuration=configuration;
			if(!empty(configuration))	this.overrideDefaultConfig();
			
			
			this.linksButtonsDisplayStrategy=linksButtonsDisplayStrategy;
			
			// Hooks
			this.hooksParameters=hooksParameters?hooksParameters:new Object();
			
			var planeIniters=Plane.hooks[INIT];
			for(key in planeIniters){
				if(!planeIniters.hasOwnProperty(key))	continue;
				planeIniters[key].initPlane(this);
			}
			
			
			// Technical attributes :


			// Background image initialization : MUST REMAIN LAST MEMBER OF THIS CONSTRUCTOR
			this.webcamProvider=null;
			
			// UNUSED
//			this.qrCodeReaderProvider=null;
			

			// Parallax background(s) initialization:
			this.fractionOfImageUsed=fractionOfImageUsed;
			this.parallaxBackgroundsProvider=new PlaneParallaxBackgroundsProvider(this,this.fractionOfImageUsed);

			// The simple background has a special co-habitation mode with the parallax one(s), if both are specified.
			if(this.simpleBackground){
			
				// Case webcam or map background :
				if(containsIgnoreCase(this.simpleBackground,WEBCAM) || containsIgnoreCase(this.simpleBackground,MAP)){

//						// It only works if plane is the aotraScreen active plane :
//						if(this.aotraScreen.getActivePlane().name!==this.name){
				
						// Case webcam background :
						if(containsIgnoreCase(this.simpleBackground,WEBCAM)){
							
							this.webcamProvider=new PlaneWebcamProvider(this);
							
//							if(containsIgnoreCase(this.simpleBackground,QRCODE_READER)){
//								// If needed, the QRCode reader:
//								this.qrCodeReaderProvider=new PlaneQRCodeReaderForWebcamProvider(this);
//							}
							
						}else if(containsIgnoreCase(this.simpleBackground,MAP)){
							
							// Case map background :
							var split=this.simpleBackground.split("{");

							
							if(empty(split)){
								this.mapProvider=new PlaneMapProvider(this);
							}else{
								
								// JSON parser standard is very strict and doesn't stand simple quotes !
								split[1]="{"+split[1];
								
								
								var mapParameters=parseJSON(split[1]);
								
								if(!mapParameters){
									this.mapProvider=new PlaneMapProvider(this);

								}else{
									
									var zoom=mapParameters.zoom;
									var startAddressStr=mapParameters.address;
									var startLocationStr=mapParameters.location;
									var startLocation=null;
									
									// Style optional parameters :
									var color=mapParameters.color;
									var infoWindow=mapParameters.infoWindow;
									var defaultMarkerIconURL=mapParameters.defaultMarkerIcon;
									var mapStyleConfigStr=mapParameters.mapStyleConfig;
									
									
									if(zoom)	zoom=parseFloat(zoom);
									
									if(startLocationStr){
										startLocationStr=startLocationStr.trim().replace(/[\s\t]+/," ");
										startLocation=getMapPositionFromLatLngStr(startLocationStr);
										
									}
									
//--------------------------------------------------------------------------------------------------
// CAUTION : BECAUSE THERE IS AN AJAX CALL, THIS BLOCK MUST REMAIN LAST MEMBER OF ITS PARENT!
									if(startLocation){
										this.mapProvider=new PlaneMapProvider(this,color,infoWindow,defaultMarkerIconURL,mapStyleConfigStr,
												zoom,startLocation);
										

										
									}else if(startAddressStr){
										
										
										// Ajax call :
										PlaneMapProvider.getMapPositionFromLiteralAddress(startAddressStr,1,function(results){
											var firstFoundResult=results[0];
											
											self.mapProvider=new PlaneMapProvider(self,color,infoWindow,defaultMarkerIconURL,mapStyleConfigStr,
													zoom,firstFoundResult);
											
											
											if(firstFoundResult){
												self.simpleBackground=self.simpleBackground.replace("}","")
												+",'location':'"+firstFoundResult.lat()+" "+firstFoundResult.lng()+"'}";
											}


											
										});
								}else{
									this.mapProvider=new PlaneMapProvider(this,color,infoWindow,defaultMarkerIconURL,mapStyleConfigStr);
								}
									// --------------------------------------------------------------------------------------------------
							}
						}
					}
				}
			}

			
		}

	
		
		
			
		// Methods
	
	
		// Treatments to do once plane is fully populated :
		/*public*/,doAfterPlaneReady:function(){
			
			
			// Hooks
			var planeIniters=Plane.hooks[INIT_AFTER_READY];
			for(key in planeIniters){
				if(!planeIniters.hasOwnProperty(key))	continue;
				planeIniters[key].initPlane(this);
			}
			
		}
	
		/*public*/,placeDivs:function(/*OPTIONAL*/refreshContent){
		// Caution : forcing content refresh is time and resources-consuming
			
			for(var i = 0; i < this.getBubbles().length; i++) {
				var bubble=this.getBubbles()[i];				
				bubble.placeDiv(refreshContent);
			}
		}
		
		,getMapProvider:function(){
			return this.mapProvider;
		}
		
		,overrideDefaultConfig:function(){
			
			var planeName=this.name;
			
			var configurationObject=parseJSON(this.configuration);
			if(!configurationObject)	return;

			// Plane config overrides default window.config:
			for(key in configurationObject){
				if(!configurationObject.hasOwnProperty(key))	continue;
				var planeConfigurationObject=configurationObject[key];
				
				if(!planeConfigurationObject)	continue;
				
				if(!window.config[key])	window.config[key]=new Object();
				if(!window.config[key][planeName])	window.config[key][planeName]=new Object();
				
				for(key2 in planeConfigurationObject){
					if(!planeConfigurationObject.hasOwnProperty(key2))	continue;
					
					window.config[key][planeName][key2]=planeConfigurationObject[key2];
					
				}
			}
			
			// Lenient behavior for default bubble size window.config :
			// IMPORTANT NOTICE :
			// If bubble mold is circle, then if we have width and width, height is ignored !
			// (ie. width is predominant on height)
			if(window.config.bubble){
				var cb=window.config.bubble;
				if(!cb.width && !cb.height){
					if(cb.size && isNumber(cb.size) && 0<cb.size){
						cb.width=cb.size;
						cb.height=cb.size;							
					}else{
						cb.width=DEFAULT_BUBBLE_WIDTH;
						cb.height=DEFAULT_BUBBLE_HEIGHT;
					}
				}else{
					var sizeWIsOK=cb.width && isNumber(cb.width) && 0<cb.width;
					var sizeHIsOK=cb.height && isNumber(cb.height) && 0<cb.height;
					if( sizeWIsOK && !sizeHIsOK ){
						cb.height=cb.width;
					}else if( !sizeWIsOK && sizeHIsOK ){
						cb.width=cb.height;
					}else{
						cb.width=DEFAULT_BUBBLE_WIDTH;
						cb.height=DEFAULT_BUBBLE_HEIGHT;
					}
				}
			}
			
		}
		
		// Bubbles management
		
		,addBubbleToPlane : function(bubble) {
			var aotraScreen=this.getAotraScreen();

			// If this bubble is already present :
			if(this.getBubbles().indexOf(bubble)!=-1)	return;
			
			// If another bubble with same name is already present in this screen, we rename it:
			var newName=bubble.getName();
			while(newName==DEFAULT_BUBBLE_NAME || aotraScreen.getBubbleByName(newName)!==null){
				newName=bubble.getName()+"_"+aotraScreen.nextBubbleIndex();
			}
			bubble.setName(newName);
			
			var plane=aotraScreen.getPlaneForBubble(bubble);
			if(plane)	plane.removeBubble(bubble);
			
			this.getBubbles().push(bubble);
			
			// CAUTION : If a z-index that is not default has been specified for this bubble already,
			// Then we do not re-calculate it :
			var numberOfBubbles=this.getBubbles().length;
			if(nothing(bubble.zIndex)){
				bubble.zIndex=MIN_Z_INDEX_BUBBLES+numberOfBubbles;
			}else if(bubble.zIndex===MIN_Z_INDEX_BUBBLES){
				bubble.zIndex+=numberOfBubbles;
			}

			// We add all sub-bubbles for this bubble : 
			var cp=copy(bubble.getSubBubbles());
			for(var i=0;i<cp.length;i++){
				var subBubble=cp[i];
				this.addBubbleToPlane(subBubble);
			}
			

			// After bubble adding treatments :

			// Eventual advanced navigation provider treatments :
			if(aotraScreen.advancedNavigationProvider){
				aotraScreen.advancedNavigationProvider.setLastBubbleForPlane(this.getName(),bubble);
			}
			
			// Eventual parallax background adding :
			if(this.parallaxBackgroundsProvider)
				this.parallaxBackgroundsProvider.addBubbleParallaxBackground(bubble);
			
		}
		

		// Removes bubble from plane
		,removeBubble:function(bubble){
			var aotraScreen=this.getAotraScreen();

			// We remove the bubble from the Plane :
			remove(this.getBubbles(),bubble);

			// We unlink this bubble :
			// -links must be deleted from this bubble
			this.removeLinksWithBubble(bubble);
			

			// Other related bubbles must forget this bubble :
			// 		-parent must forget this bubble :
			var parentBubble=bubble.getParentBubble();
			if(parentBubble){
				remove(parentBubble.getSubBubbles(),bubble);
			}
			
			// 		-children must forget this bubble :
			var subBubbles=bubble.getSubBubbles();
			for(var i=0;i<subBubbles.length;i++){
				var subBubble=subBubbles[i];
				subBubble.detachFromParentBubble(true);
			}


			// After bubble removing treatments :

			// Eventual advanced navigation provider treatments :
			if(aotraScreen.advancedNavigationProvider){
				aotraScreen.advancedNavigationProvider.removeBubbleInOrderedListForPlane(this.getName(),bubble);
			}
			
			// Eventual parallax background removing :
			if(this.parallaxBackgroundsProvider)
				this.parallaxBackgroundsProvider.removeBubbleParallaxBackground(bubble);
			
		}

		,isBubbleInPlane:function(bubble){
			var bubbles=this.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				var b=bubbles[i];
				if(bubble===b)	return true;
			}
			return false;
		}
		
		,getBubbleByName:function(bubbleName){
			for(var i=0;i<this.getBubbles().length;i++){
				var bubble=this.getBubbles()[i];
				if(bubble.getName()==bubbleName)	return bubble;
			}
			return null;
		}
		
		,getHomeBubble:function(){
			for(var i=0;i<this.getBubbles().length;i++){
				var bubble=this.getBubbles()[i];
				if(bubble.getIsHomeBubble())	return bubble;
			}
			return null;
		}
		
		
		/*public*/,getBubbles:function(){
			return this.bubbles;
		}
		// UNUSED
		/*public*/,getVisibleBubbles:function(){
			var list=new Array();
			for(var i=0;i<this.getBubbles().length;i++){
				var bubble=this.getBubbles()[i];
				if(!bubble.isVisible())	continue;
				list.push(bubble);
			}
			return list;
		}
		/*public*/,getVisibleAndNavigableBubbles:function(){
			var list=new Array();
			for(var i=0;i<this.getBubbles().length;i++){
				var bubble=this.getBubbles()[i];
				if(!bubble.isVisible() || !bubble.isNavigable())	continue;
				list.push(bubble);
			}
			return list;
		}
		/*private*/,getFirstLevelBubbles:function(){
			var list=new Array();
			for(var i=0;i<this.getBubbles().length;i++){
				var bubble=this.getBubbles()[i];
				if(!bubble.isFirstLevelBubble())	continue;
				list.push(bubble);
			}
			return list;
		}
		
		
		// UNUSED (FOR NOW...)
		,getVisibleBubbles:function(){
			var resultList=new Array();
			for(var i=0;i<this.getBubbles().length;i++){
				var bubble=this.getBubbles()[i];
				if(!bubble.isVisible())	continue;
				resultList.push(bubble);
			}
			return resultList;
		}
		

		,getName:function(){
			return this.name;
		}
		
		// UNUSED FOR NOW...
		,getBubblesInActivePlaneByNameContaining:function(searchString){
			var list=new Array();
			for(var i=0;i<this.getBubbles().length;i++){
				var b=this.getBubbles()[i];
				if(contains(b.getName(),searchString))
					list.push(b);
			}
			return list;
		}
		
		,getBubblesInActivePlaneByContentContaining:function(searchStringParam,/*OPTIONAL*/isClearAccentuatedCharacters){
			var list=new Array();

			// We get rid of accentuated characters :
			var searchString=searchStringParam.toLowerCase();
			if(isClearAccentuatedCharacters)
				searchString=clearAccentuatedCharacters(searchString);
			
			var searchStringWords=searchString.split(/[\s\t\n\r]+/igm);
			
			// First, we perform a wide search to all the bubbles in their mashed content only :
			var firstSearchList=new Array();
			
			// (We process in the search string words appearance order)
			for(var j=0;j<searchStringWords.length;j++){
				var searchStringWord=searchStringWords[j];
				
				for(var i=0;i<this.getBubbles().length;i++){
					var b=this.getBubbles()[i];
					
					// We don't want duplicates:
					if(firstSearchList.indexOf(b)!=-1)	continue;
					
					// We get rid of accentuated characters :
					var mashedContent=b.getMashedContent();
					
					
					if(isClearAccentuatedCharacters)
						mashedContent=clearAccentuatedCharacters(mashedContent);
					
					// (mashed content has already been generated in lower case...)
					if(contains(mashedContent,searchStringWord)){
						firstSearchList.push(b);
					}
				}				
			}
			
			// Then among these first search bubbles list, we filter then 
			// looking for the exact expression in the normal content :
			for(var i=0;i<firstSearchList.length;i++){
				var b=firstSearchList[i];
				
				// We don't want duplicates:
				if(list.indexOf(b)!=-1)	continue;
				
				// We get rid of accentuated characters :
				var content=b.getContent();
				if(isClearAccentuatedCharacters)
					content=clearAccentuatedCharacters(content);
				
				if(containsIgnoreCase(content,searchString)){
					list.push(b);
				}
			}				
			
			
			return list;
		}
		
		/*public*/,getNearestCoordinateXWithBubbles:function(selectedBubble,x,threshold){
			var bubbles=this.sortBubblesByDistance(selectedBubble);
			for(var i = 0; i < bubbles.length; i++) {
				var bubble=bubbles[i];
				if(bubble.isFixed()!==selectedBubble.isFixed())	continue;
				var bubbleX=bubble.canonicalX;
				if(Math.abs(bubbleX-x)<=threshold){
					return bubbleX;
				}
			}
			return null;
		}
		/*public*/,getNearestCoordinateYWithBubbles:function(selectedBubble,y,threshold){
			var bubbles=this.sortBubblesByDistance(selectedBubble);
			for(var i = 0; i < bubbles.length; i++) {
				var bubble=bubbles[i];
				if(bubble.isFixed()!==selectedBubble.isFixed())	continue;
				var bubbleY=bubble.canonicalY;
				if(Math.abs(bubbleY-y)<=threshold){
					return bubbleY;
				}
			}
			return null;
		}

		// CAUTION :
		// This method will only sort bubbles in the same container
		// (ie. if they have no parent, only with other 1st-level bubbles,
		// and if they have a parent, only with other bubbles that have the same parent)
		/*private*/,sortBubblesByDistance:function(selectedBubble){
			
			var c;
			if(selectedBubble.isFirstLevelBubble()){
				c=copy(this.getFirstLevelBubbles());
			}else{
				c=copy(selectedBubble.getParentBubble().getSubBubbles());
			}
			
			// We exclude the bubble from which we are sorting distances itself:
			remove(c,selectedBubble);
			return c.sort(function(b1,b2){
				var q1=selectedBubble.distanceTo(b1);
				var q2=selectedBubble.distanceTo(b2);
				// We sort bubbles only once, nor if their fixation is different :
				if( q1===q2 
				 || b1.isFixed()!==b2.isFixed()
				 || b1.isMustNeverBeShown() || b2.isMustNeverBeShown())	return 0;
				// REVERSE ORDER
				if(q1<q2)	return -1;
				if(q1>q2)	return 1;
			});
			
		}

		
		
		// Links management
		/*private*/,getLinks:function(){
			return this.links;
		}
		
		/*public*/,getOutgoingLinks:function(fromBubbleName){
			var links=new Array();
			for(var i=0;i<this.links.length;i++){
				var link=this.links[i];
				if(link.getFromBubbleName()!==fromBubbleName)	continue;
				links.push(link);
			}
			return links;
		}
		/*public*/,getIngoingLinks:function(toBubbleName){
			var links=new Array();
			for(var i=0;i<this.links.length;i++){
				var link=this.links[i];
				if(link.getToBubbleName()!==toBubbleName)	continue;
				links.push(link);
			}
			return links;
		}

		/*public*/,addLink : function(link) {
			// We don't allow duplicate links :
			if(this.equivalentLinkExists(link))	return;
			this.getLinks().push(link);
		}
		/*public*/,removeLinksWithBubble:function(bubble){
			
			var cp=copy(this.getLinks());
			for(var i=0;i<cp.length;i++){
				var link=cp[i];
				if(link.fromBubbleName==bubble.getName() || link.toBubbleName==bubble.getName()){
					remove(this.getLinks(),link);
				}
			}
		}
		/*public*/,equivalentLinkExists:function(link){
			var linkOrMock=this.getLinkByBubblesNames(link.fromBubbleName,link.toBubbleName);
			return !linkOrMock.isMock;
		}
		/*public*/,getLink:function(htmlElement){
			var toBubbleName=htmlElement.getAttribute("data-to");
			var fromBubbleName=this.getContainingBubbleName(htmlElement).replace(BUBBLE_DIV_PREFIX+"_","");
			if(!fromBubbleName)	return this.getMockLink("",toBubbleName);
			return this.getLinkByBubblesNames(fromBubbleName,toBubbleName);
		}
		/*private*/,getContainingBubbleName:function(htmlElement){
			var p=htmlElement.parentNode;
			if(!p)	return null;
			var r=new RegExp("^"+BUBBLE_DIV_PREFIX);
			if(!p.id || !r.test(p.id))	return this.getContainingBubbleName(p);
			return p.id;
		}
		
		/*public*/,getLinkByBubblesNames:function(fromBubbleName,/*OPTIONAL*/toBubbleName){
			for(var i=0;i<this.getLinks().length;i++){
				var l=this.getLinks()[i];
				if(l.equalNames(fromBubbleName,toBubbleName))
					return l;
			}
			return this.getMockLink(fromBubbleName,toBubbleName);
		}
		/*private*/,getMockLink:function(fromBubbleName,toBubbleName){
			var mock=new Object();
			mock.onClick=function(){
				alert("Link from bubble ["+fromBubbleName+"] to bubble ["+toBubbleName+"] does not exist for navigation.");
			};
			mock.onClickBackwards=function(){
				alert("Link from bubble ["+fromBubbleName+"] to bubble ["+toBubbleName+"] does not exist for backwards navigation.");
			};
			mock.isMock=true;
			return mock;
		}
		
		// UNUSED : TODO : USE ! Develop :
		,updateLinksForBubbleNameChange:function(oldName,newName){
			if(this.getBubbleByName(oldName)==null)	return;
			for(var i=0;i<this.getLinks().length;i++){
				var l=this.getLinks()[i];
				if(l.fromBubbleName!==oldName && l.toBubbleName!==oldName )
					continue;
				if(l.fromBubbleName==oldName)
					l.fromBubbleName=newName;
				if(l.toBubbleName==oldName)
					l.toBubbleName=newName;
			}
			
		}
		
		
		// UNUSED 
//		/*private*/,isSimpleBackgroundSpecial:function(){
//			return this.simpleBackground===WEBCAM || containsIgnoreCase(this.simpleBackground,MAP);
//		}
		
		// Drawing
		,draw:function(aotraScreen){
			
			//SELF
			var self=this;
			
			
			// There is background drawing :
			var ctx=aotraScreen.ctx;
			

			// Webcam background overrides parallax background(s) drawing :
			// DEPENDENCY : PlaneWebcamProvider code here :
			// Webcam drawing :
			if(this.webcamProvider)
				this.webcamProvider.drawWebcam(ctx);
			
			// Parallax backgrounds drawing :
			if(this.parallaxBackgroundsProvider){
				this.parallaxBackgroundsProvider.draw(ctx);
			}
			
			
			
		}
		
		/*public*/,drawLinks:function(aotraScreen){
			
			// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider
			// Bubble links are not displayed, if plane display mode is default (heuroptic):
			if(this.verticalScrollingProvider 
			&& this.hooksParameters[SCROLL_HOOK_PARAM]===VERTICAL_SCROLLING_MODE
			){
				return;
			}
			
			// ...then bubbles links :
			var links=this.getLinks();
			for(var i=0;i<links.length;i++){
				links[i].draw(aotraScreen);
			}
		}
		
		
		
		,getAotraScreen:function(){
			if(this.aotraScreen)	return this.aotraScreen;
			return getAotraScreen();
		}
		
		
		
		
	});
	
	// STATIC CODE :
	// Hooks
	/*static*/{
		// Generic hooks : 
		initSelf.Plane.hooks=new Object();
		initSelf.Plane.hooks[INIT]=new Object();
		initSelf.Plane.hooks[INIT_AFTER_READY]=new Object();

	};

	return initSelf;
}
}

if(typeof initBubbleClosingMethodsClass ==="undefined"){
function initBubbleClosingMethodsClass() {

	var initSelf = this;

	// CONSTANTS
	var REFRESHING_RATE = .01;
	
	initSelf.BubbleClosingMethods = Class.create({

		// Constructor
		initialize : function(openCloseAnimation) {

			// Attributes

			this.doClose=this.doCloseInstantly;
			if(openCloseAnimation=="linear")
				this.doClose=this.doCloseLinearly;			
		}

		// Methods
		,doCloseLinearly:function(){
			var bubble=this;
			if(bubble.isOpenableClosable()===false)	return;
			
			var STEP=20;
			var pe=new PeriodicalExecuter(function() {

//				bubble.diameter-=STEP;
				bubble.w-=STEP;
				bubble.h-=STEP;

				bubble.placeDiv();
				
//				var minDiameter = bubble.minDiameter;
				var minW = bubble.minW;
				var minH = bubble.minH;

				
//				if(bubble.diameter<=minDiameter){
				if(bubble.w<=minW){ // width is always the master parameter

					pe.stop();
					
//					bubble.diameter=minDiameter;
					bubble.w=minW;
					bubble.h=minH;

					bubble.setOpen(false);
					
					return;
				}
					

			},REFRESHING_RATE);
		}
		
		,doCloseInstantly:function(){
			var bubble=this;
			if(bubble.isOpenableClosable()===false)	return;
			
//			var minDiameter =bubble.minDiameter;
//			bubble.diameter = minDiameter;
			var minW=bubble.minW;
			bubble.w = minW;
			var minH=bubble.minH;
			bubble.h = minH;
		
			
			bubble.setOpen(false);
		}
		
	});

	return initSelf;
}
}
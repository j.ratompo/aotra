if(typeof initBubbleOpeningMethodsClass ==="undefined"){
function initBubbleOpeningMethodsClass() {
	var initSelf = this;
	
	// CONSTANTS
	var REFRESHING_RATE = .01;
	
	initSelf.BubbleOpeningMethods = Class.create({

		// Constructor
		initialize : function(openCloseAnimation) {

			// Attributes
			this.doOpen=this.doOpenInstantly;
			if(openCloseAnimation=="linear")
				this.doOpen=this.doOpenLinearly;
		}

		// Methods
		,doOpenLinearly:function(){
			var bubble=this;
			if(bubble.isOpenableClosable()===false)	return;

			
			var STEP=20;
			var pe=new PeriodicalExecuter(function() {
				
				bubble.w+=STEP;
				bubble.h+=STEP;

				bubble.placeDiv();

				var maxW = bubble.maxW;
				var maxH = bubble.maxH;

				if(maxW-STEP<=bubble.w){ // width is always the master parameter
					
					pe.stop();
					
					bubble.w=maxW;
					bubble.h=maxH;
					
					bubble.setOpen(true);
					return;
				}
			},REFRESHING_RATE);
		}
		
		,doOpenInstantly:function(){
			var bubble=this;
			if(bubble.isOpenableClosable()===false)	return;

			var maxW =bubble.maxW;
			bubble.w= maxW;
			var maxH =bubble.maxH;
			bubble.h= maxH;
			
			bubble.setOpen(true);
		}
		
	});

	return initSelf;
}
}

if(typeof initAotraScreenAdvancedNavigationProviderClass === "undefined"){

	function initAotraScreenAdvancedNavigationProviderClass() {
	var initSelf = this;
	
	// CONSTANTS
	var MODULE_KEY="AotraScreenAdvancedNavigationProvider";

	var GOTO_LAST_BUBBLE_BUTTON_LABEL_DEFAULT=
		i18n({"fr":"Aller à la dernière bulle ajoutée...","en":"Goto last created bubble..."});
	
	var GOTO_RANDOM_BUBBLE_BUTTON_LABEL_DEFAULT=
		i18n({"fr":"Aller à une bulle au hasard...","en":"Goto random bubble..."});

	// PROD
	var HISTORY_SIZE=20;

	
	initSelf.AotraScreenAdvancedNavigationProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {

			// Attributes
			this.aotraScreen=aotraScreen;
			this.homeBubblesByPlanes=new Object();

			// Processed attributes
			this.lastBubblesByPlane=new Object(); // An associative array of arrays
			
			this.bubblesHistoryByPlane=new Object(); // An associative array of arrays
			this.historyIndex=-1;// Because when there is one element, it will be 0 !
			this.isGoing=false; // A simple mechanism to avoid history modification while we're still going to a bubble...
			this.preventAddInHistory=false;

			this.lastVisitedBubblesNamesByPlane=new Object(); // An associative array of arrays
			this.beforeLastVisitedBubble=null;
			this.lastVisitedBubble=null;
			this.textboxComponent=null;
			
			this.initUI();
			
			// Hooks using :
			var self=this;
			/*static*/var hook=function(aotraScreen,bubble){
				if(aotraScreen.advancedNavigationProvider){
					self.setLastVisitedBubbleForPlane(aotraScreen.getActivePlane().getName(),bubble);
				}
			}
			AotraScreen.hooks[DO_ON_ARRIVAL_HOOK_NAME].push(hook);
			
		}
	
		/*private*/,initUI:function(){
			var aotraScreen=this.aotraScreen;
			
			var mainDiv=aotraScreen.mainDiv;
			var mainDivInnerHTML = mainDiv.innerHTML;
			
			mainDivInnerHTML+=this.getToolsBarHTMLButtons();
			
			mainDiv.innerHTML=mainDivInnerHTML;
		}

		/*private*/,getToolsBarHTMLButtons:function(){
			
			var mainDivInnerHTML="";		
	
			var POSITION_X=316;
			var POSITION_Y=0;
			
			mainDivInnerHTML+="<div id='divBlockAllAdvancedNavigationActionsStatic' class='buttonsHolder' "
				+" style='left:"+(POSITION_X)+"px;top:"+(POSITION_Y)+"px ; z-index:"+(MAX_Z_INDEX-1)+"; "
				+" max-width:200px;"
				// Since this block is not a filled frame, then all clicks must pass through it :
				+" pointer-events:none;"
				+" ' > ";
			
				mainDivInnerHTML+="<button class='icon homeBubbleBtn' "
					+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoHomeBubbleInActivePlane();' "
					+" title='"+i18n({"fr":"Aller à la bulle d’accueil...","en":"Go to home bubble"})+"'></button>";//&#8962;

				mainDivInnerHTML+="<button class='icon lastBubbleBtn' "
					+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoLastBubbleInActivePlane();' "
					+" title='"+i18n({"fr":"Aller à la dernière bulle...","en":"Go to last bubble"})+"'></button>";//&#10144;
	
				mainDivInnerHTML+="<button class='icon lastVisitedBubbleBtn' id='buttonGotoLastVisitedBubble' "
					+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoLastVisitedBubbleInActivePlane();' "
					+" title='"+i18n({"fr":"Aller à la dernière bulle visitée...","en":"Go to last visited bubble"})+"'></button>";//&#9055;


				mainDivInnerHTML+=this.getPreviousBubbleButtonHtml("buttonGotoPreviousBubble");
				
				mainDivInnerHTML+=this.getNextBubbleButtonHtml("buttonGotoNextBubble");

			mainDivInnerHTML+="</div>";
			
			return mainDivInnerHTML;
		}
		
		/*public*/,getPreviousBubbleButtonHtml:function(/*OPTIONAL*/elementId){
			return "<button class='icon previousBubbleBtn'"+(elementId?" id='"+elementId+"'":"")
				+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoPreviousBubbleInActivePlane();' "
				+" title='"+i18n({"fr":"Aller en arrière dans l´historique...","en":"Go to previous bubble in history"})+"'></button>";//&lt;
		}
		
		/*private*/,getNextBubbleButtonHtml:function(/*OPTIONAL*/elementId){
			return "<button class='icon nextBubbleBtn'"+(elementId?" id='"+elementId+"'":"")
				+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoNextBubbleInActivePlane();' "
				+" title='"+i18n({"fr":"Aller en avant dans l´historique...","en":"Go to next bubble in history"})+"'></button>"//;&gt;
			
		}
	
		// Methods
		
		// Goto last created bubble :

		
		/*private*/,gotoLastBubbleInActivePlane:function(){
			
			var aotraScreen=this.aotraScreen;
			var activePlaneName=aotraScreen.getActivePlane().getName();
			
			var orderedBubbles=this.lastBubblesByPlane[activePlaneName];
			if(!orderedBubbles){
				// TRACE
				log("WARN : No last bubbles history found for this current plane.");
				return;
			}
			if(empty(orderedBubbles)){
				// TRACE
				log("WARN : Last bubbles history found for this current plane is empty.");
				return;
			}
			
			var lastBubble=null;
			for(var i=orderedBubbles.length-1;i>=0;i--){
				var b=orderedBubbles[i];
				// Bubbles where we can't go for a reason or another are not taken into account :
				if(!b.isNavigable())	continue;
				lastBubble=b;
				break;
			}
			
			if(lastBubble){
				aotraScreen.doGotoBubble(lastBubble,true);
			}else{
				// TRACE
				log("WARN : No last bubble for this current plane.");
				return;
			}
			
		}			

		/*public*/,setLastBubbleForPlane:function(planeName,bubble){
			
			// Last bubble by plane :
			var orderedBubbles=this.lastBubblesByPlane[planeName];
			if(!orderedBubbles){
				orderedBubbles=new Array();
				this.lastBubblesByPlane[planeName]=orderedBubbles;
			}
			if(contains(orderedBubbles,bubble))	return;
			orderedBubbles.push(bubble);
			
		}
		
		// Goto random bubble :
		/*private*/,gotoRandomBubbleInActivePlane:function(){
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();

			var visibleNavigableBubbles=activePlane.getVisibleAndNavigableBubbles();
			if(empty(visibleNavigableBubbles))	return;
			
			// Exclusions :
			var list=new Array();
			for(var i=0,b;b=visibleNavigableBubbles[i];i++){
				// We exclude the bubble itself :
				if(this.lastVisitedBubble && b.getName()===this.lastVisitedBubble.getName())	continue;
				// We exclude the previous random chosen bubble :
				if(this.beforeLastVisitedBubble && b.getName()===this.beforeLastVisitedBubble.getName())	continue;
				// We exclude sub-bubbles :
				if(!b.isFirstLevelBubble())	continue;
				list.push(b);
			}
			

			var randomIndex=Math.getRandomInt(list.length)-1;// From (1 to length)-1 = (0 to length-1)
			var bubble=list[randomIndex];
			
			
			aotraScreen.doGotoBubble(bubble,true);
		}

		
		
		
		
		
		// TODO : EXTRACT THIS !
		// UI Elements adding :
		//*************************************
		
		/*private static*/,getGeneratedHTMLInputs:function(){
			var mainDivInnerHTML="";
			
			/*
			mainDivInnerHTML+='<button class="icon lastBubbleInsertBtn" '
				+' onclick="javascript:getAotraScreen().advancedNavigationProvider.addGotoLastBubbleNavigableUIElement();"'
				+' title="'+i18n({"fr":"Insérer dans le contenu un bouton «Aller à la dernière bulle».","en":"Add «goto last bubble» button in content."})+'"></button>';//&#10144;

			mainDivInnerHTML+='<button class="icon randomBubbleInsertBtn" '
				+' onclick="javascript:getAotraScreen().advancedNavigationProvider.addGotoRandomBubbleVisibleNavigableUIElement();"'
				+' title="'+i18n({"fr":"Insérer dans le contenu un bouton «Aller à une bulle au hasard».","en":"Add «goto random bubble» button in content."})+'"></button>';//&#10144;
			*/
			
			mainDivInnerHTML+=AotraScreenEditionProvider.getGeneratedHTMLButton("lastBubbleInsertBtn","advancedNavigationProvider.addGotoLastBubbleNavigableUIElement()"
					,i18n({"fr":"Insérer dans le contenu un bouton «Aller à la dernière bulle».","en":"Add «goto last bubble» button in content."}));

			mainDivInnerHTML+=AotraScreenEditionProvider.getGeneratedHTMLButton("randomBubbleInsertBtn","advancedNavigationProvider.addGotoRandomBubbleVisibleNavigableUIElement()"
					,i18n({"fr":"Insérer dans le contenu un bouton «Aller à une bulle au hasard».","en":"Add «goto random bubble» button in content."}));
			
			return mainDivInnerHTML;
		}
		
		/*private static*/,addGotoLastBubbleNavigableUIElement:function(){
			/*
			this.addUIElement("<span style='text-decoration:underline;cursor:pointer;' class='gotoLastBubble' "
					+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoLastBubbleInActivePlane();'>"
					,GOTO_LAST_BUBBLE_BUTTON_LABEL_DEFAULT);
			*/
			
			AotraScreenEditionProvider.addUIElementInContentFromButtonAction(
					"gotoLastBubble","advancedNavigationProvider.gotoLastBubbleInActivePlane()",GOTO_LAST_BUBBLE_BUTTON_LABEL_DEFAULT,this.textboxComponent);
		}

		/*private static*/,addGotoRandomBubbleVisibleNavigableUIElement:function(){
			/*
			this.addUIElement("<span style='text-decoration:underline;cursor:pointer;' class='gotoLastBubble' "
					+" onclick='javascript:getAotraScreen().advancedNavigationProvider.gotoRandomBubbleInActivePlane();'>"
					,GOTO_RANDOM_BUBBLE_BUTTON_LABEL_DEFAULT);
			*/
			AotraScreenEditionProvider.addUIElementInContentFromButtonAction(
					"gotoLastBubble","advancedNavigationProvider.gotoRandomBubbleInActivePlane()",GOTO_RANDOM_BUBBLE_BUTTON_LABEL_DEFAULT,this.textboxComponent);
		}
		

		
		/*private static*/
//		,addUIElement:function(htmlToInsert,defaultLabel,textboxComponent){
//			
//			// DEPENDENCY : EDITION PROVIDER
//			var editionProvider=getAotraScreen().editionProvider;
//			
//			// Edition provider doesn't have to be present at this point :
//			if(!editionProvider)	return;
//
//			var editedBubble=editionProvider.getSelectedBubble();
//			
//			var selectedText=getSelectedText(textboxComponent).trim();
//
//			editionProvider.commitContentToSelectedBubble();
//			var content=toOneSimplifiedLine(editedBubble.getContent(),"compactHTML");
//
//
//			// Case some text selected :
//			if(!nothing(selectedText)){
//				htmlToInsert+=selectedText;
//				htmlToInsert+="</span>";
//				content=content.replace(selectedText,htmlToInsert);
//			}else{
//				// Case no text selected : we append at the end...
//				htmlToInsert+=defaultLabel;
//				htmlToInsert+="</span>";
//				content=content+htmlToInsert;
//			}
//			
//			editedBubble.setContent(content);
//			textboxComponent.setContent(content);
//		}
		//*************************************
		
		
		
		
		/*public*/,removeBubbleInOrderedListForPlane:function(planeName,bubble){
			
			var orderedBubbles=this.lastBubblesByPlane[planeName];
			if(!orderedBubbles)	return;
			
			remove(orderedBubbles,bubble);

		}

		
		// Go to last visited bubble :

		/*private*/,gotoLastVisitedBubbleInActivePlane:function(){
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			var activePlaneName=activePlane.getName();
			
			var bubbleName=this.lastVisitedBubblesNamesByPlane[activePlaneName];
			if(!bubbleName)	return;
			var bubble=activePlane.getBubbleByName(bubbleName);
			if(!bubble)	return;

			aotraScreen.doGotoBubble(bubble,true);
		}
		
		/*private*/,getLastVisitedBubbleForActivePlane:function(){
			var aotraScreen=this.aotraScreen;
			var activePlaneName=aotraScreen.getActivePlane().getName();
			
			return this.lastVisitedBubblesNamesByPlane[activePlaneName];
		}
		
		/*public*/,restoreAllLastVisitedBubblesFromStorage:function(){
			
			var lastVisitedBubblesArrayStr=getStringFromStorage("lastVisitedBubblesNamesByPlaneStr");
			if(nothing(lastVisitedBubblesArrayStr))	return;
			
			this.lastVisitedBubblesNamesByPlane=getArrayFromJSONString(lastVisitedBubblesArrayStr);
			
		}
		
		/*private*/,setLastVisitedBubbleForPlane:function(planeName,bubble,/*OPTIONAL*/isHomeBubbleInitialization){
			
			if(!isHomeBubbleInitialization){
				// Last visited bubble persistence :
				this.lastVisitedBubblesNamesByPlane[planeName]=bubble.getName();
				var lastVisitedBubblesArrayStr=getArrayAsJSONString(this.lastVisitedBubblesNamesByPlane);
				storeString("lastVisitedBubblesNamesByPlaneStr",lastVisitedBubblesArrayStr);
			}
			
			// Bubble history by plane :
			this.addHistoryBubble(planeName,bubble);
			this.beforeLastVisitedBubble=this.lastVisitedBubble;
			this.lastVisitedBubble=bubble;

			// A simple mechanism to avoid history modification while we're still going to a bubble...:
			this.isGoing=false; // Because it's done on arrival only.
			
		}

		
		// History :
		
		// UNUSED
		/*private*/,isHistoryEmpty:function(){
			var history=this.bubblesHistoryByPlane[planeName];
			if(!history)	return true;
			return empty(history);
		}

		/*private*/,addHistoryBubble:function(planeName,bubble){
			
			if(!bubble)	return;
			if(this.preventAddInHistory){
				this.preventAddInHistory=false;// One-shot only...
				return;
			}
			
			var history=this.bubblesHistoryByPlane[planeName];
			if(!history){
				history=new Array();
				this.bubblesHistoryByPlane[planeName]=history;
			}
			
			if(!empty(history)){
				// We don't allow consecutive bubbles in history to be the same :
				var otherBubble=history[this.historyIndex];
				
				if(otherBubble && otherBubble.getName()===bubble.getName())
					return;
				
			} 
			
			if((HISTORY_SIZE-1) <= history.length)	history.shift();
			else this.historyIndex++;
			
			var bubbleInPlace=history[this.historyIndex];
			// We clear the «tail» of history if we go in a different track after being in the «past» :
			if(	bubbleInPlace && bubbleInPlace.getName()!==bubble.getName()){
				history=history.slice(0,this.historyIndex);
			}
			
			history.push(bubble);
			
			this.bubblesHistoryByPlane[planeName]=history;


		}
		
		/*private*/,gotoPreviousBubbleInActivePlane:function(){
			
			if(this.isGoing===true)	return; // A simple mechanism to avoid history modification while we're still going to a bubble...
			
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			var activePlaneName=activePlane.getName();
			
			
			var history=this.bubblesHistoryByPlane[activePlaneName];
			if(!history)	return;
			
			
			if(this.historyIndex<1)	return;
			
			var bubble=history[this.historyIndex-1];
			if(!bubble)	return;
			this.historyIndex--;

			this.preventAddInHistory=true;
			this.isGoing=true; // A simple mechanism to avoid history modification while we're still going to a bubble...
			aotraScreen.doGotoBubble(bubble,true);

		}
		
		/*private*/,gotoNextBubbleInActivePlane:function(){
			
			if(this.isGoing===true)	return; // A simple mechanism to avoid history modification while we're still going to a bubble...
			
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			var activePlaneName=activePlane.getName();
			
			var history=this.bubblesHistoryByPlane[activePlaneName];
			if(!history)	return;
			
			
			if((HISTORY_SIZE-1)-1 <= this.historyIndex)	return;

			var bubble=history[this.historyIndex+1];
			if(!bubble)	return;
			this.historyIndex++;

			
			this.preventAddInHistory=true;
			this.isGoing=true; // A simple mechanism to avoid history modification while we're still going to a bubble...
			aotraScreen.doGotoBubble(bubble,true);
			
		}
		
		
		// Go to home bubble :
		/*private*/,gotoHomeBubbleInActivePlane:function(){
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			var activePlaneName=activePlane.getName();
			
			var bubble=this.homeBubblesByPlanes[activePlaneName];
			if(!bubble)	return;
			
			aotraScreen.doGotoBubble(bubble,true);
			
		}
		
		
		/*public*/,determineHomeBubbleOnInit:function(){
			var aotraScreen=this.aotraScreen;

			var activePlane=aotraScreen.getActivePlane();
			
			var b=this.getCurrentHomeBubble();
			if(b){
				
				// Advanced navigation provider has to know which is the home bubble on its initialization:
				this.setHomeBubble(b);
			
				// We push home bubble into history on advanced navigation initialization :
				// IMPORTANT : If and only if last visited bubble is null (to avoid interferences with visit last bubble feature) :
				//if(!this.getLastVisitedBubbleForActivePlane())
				this.setLastVisitedBubbleForPlane(activePlane.getName(),b,true);
				
			}else{
				
				// If no home bubble is found on first try, 
				// then advanced navigation provider will not alter the model, 
				// but we will push forced the first bubble in history.
				var bubbles=activePlane.getBubbles();
				if(!empty(bubbles)){
					b=bubbles[0];
					
					// We push home bubble into history on advanced navigation initialization :
					// IMPORTANT : If and only if last visited bubble is null (to avoid interferences with visit last bubble feature) :
					//if(!this.getLastVisitedBubbleForActivePlane())
					this.setLastVisitedBubbleForPlane(activePlane.getName(),b,true);
				}
				
			}
			
		}
		
		/*private*/,getCurrentHomeBubble:function(){
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			
			var bubbles=activePlane.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				var b=bubbles[i];
				// We stop on first declared home bubble found:
				if(b.getIsHomeBubble())	return b;
			}
			return null;
			
		}
		
		/*public*/,setHomeBubble:function(bubble){
			
			var aotraScreen=this.aotraScreen;
			var activePlane=aotraScreen.getActivePlane();
			
			var activePlaneName=activePlane.getName();
			
			var previousHomeBubble=this.homeBubblesByPlanes[activePlaneName];
			if(previousHomeBubble){
				// We unset every previous home bubble as normal in activePlane :
				previousHomeBubble.setIsHomeBubble(false);
			}
			
			this.homeBubblesByPlanes[activePlaneName]=bubble;
			
		}
		
		// Accessors
		,setTextboxComponent:function(c){
			this.textboxComponent=c;
		}
		
	
	});


	// Static initialization :
	/*static*/{
		
		// Use hooks for edition provider initialization
		var editionProviderIniter=new Object();
		AotraScreenEditionProvider.hooks[EDIT_WINDOW_BUTTONS][MODULE_KEY]=editionProviderIniter;
		editionProviderIniter.init=function(aotraScreen,editionProvider){

			// Goto last added bubble button inserts :
			if(!aotraScreen.advancedNavigationProvider)	return;
			
			return aotraScreen.advancedNavigationProvider.getGeneratedHTMLInputs();
			
		};
		
		var editionProviderWindowIniter=new Object();
		AotraScreenEditionProvider.hooks[EDIT_WINDOW][MODULE_KEY]=editionProviderWindowIniter;
		editionProviderWindowIniter.init=function(aotraScreen,editableDiv){

			// Goto last added bubble button inserts :
			if(!aotraScreen.advancedNavigationProvider)	return;

	        // Goto last bubble button adding sub-component textbox component update :
			aotraScreen.advancedNavigationProvider.setTextboxComponent(editableDiv);
			
		};
		
		
		
		
	}
	
	
	return initSelf;
}
}

if(typeof initScreenPositionMovingMethodsClass ==="undefined"){
function initScreenPositionMovingMethodsClass() {
	var initSelf = this;

	// CONSTANTS
//	var REFRESHING_RATE = .01;
//	var SPEED_FACTOR = 0.05; // corresponds to 20 steps.

	initSelf.AotraScreenPositionMovingMethods = Class.create({

		// Constructor
		initialize : function(aotraScreen,moveAnimation) {

			// Attributes
			this.aotraScreen = aotraScreen;

			this.moveAnimation = moveAnimation;

			// Default move method = "instantly"
			
			this.moveToPosition = this.moveToPositionInstantly;
			if (moveAnimation === "linear")
				this.moveToPosition = this.moveToPositionLinearly;
			
		}

		// Methods
		// - instantly :
		,moveToPositionInstantly : function(destinationX, destinationY, onEndMethod) {
			var aotraScreen = this;
			aotraScreen.moveToMousePosition(destinationX, destinationY);
			if(onEndMethod)	onEndMethod();
			aotraScreen.drawAndPlace();
		}

		// - linear :
		,moveToPositionLinearly : function(destinationX, destinationY, onEndMethod) {
			
			var aotraScreen = this;

			var numberOfSteps=aotraScreen.getSpeedParam("numberOfSteps");

			var sourceX = Math.round(aotraScreen.getWidth() / 2);
			var sourceY = Math.round(aotraScreen.getHeight() / 2);

			var deltaX = destinationX - sourceX;
			var deltaY = destinationY - sourceY;
			
			var signX = deltaX==0?1: deltaX/Math.abs(deltaX);
			var signY = deltaY==0?1: deltaY/Math.abs(deltaY);
			
			var xStep = (Math.abs(deltaX * (1/numberOfSteps)));
			var yStep = (Math.abs(deltaY * (1/numberOfSteps)));

			
//			// We temporarily disable onClick() function on aotraScreen 
//			// (for all the time took to go to bubble position) :
			aotraScreen.enableOnClick(false);
			aotraScreen.enableOnDrag(false);

			
			var cnt=0;
			var pe= new PeriodicalExecuter(function() {

				if (cnt>=numberOfSteps) {
					pe.stop();

					aotraScreen.enableOnClick(true);
					aotraScreen.enableOnDrag(true);

					if(onEndMethod)	onEndMethod();
					
					aotraScreen.drawAndPlace();
					return;
				}
				
				aotraScreen.moveToMousePosition(sourceX + signX*xStep, sourceY + signY*yStep);
				cnt++;
				
			}, aotraScreen.getSpeedParam("refreshingRate"));

		}


	});

	return initSelf;
}
}
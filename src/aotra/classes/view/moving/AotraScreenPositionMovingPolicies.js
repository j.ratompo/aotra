if(typeof initScreenPositionMovingPoliciesClass ==="undefined"){
function initScreenPositionMovingPoliciesClass() {
	var initSelf = this;
	
	// CONSTANTS
	var REFRESHING_RATE = .01;
	
	initSelf.AotraScreenPositionMovingPolicies = Class.create({

		// Constructor
		initialize : function(aotraScreen,movePolicy) {
			
			// Attributes
			this.aotraScreen = aotraScreen;
			
			this.movePolicy=movePolicy;
			
			this.fitZoomToBubbles=false;

			// Default move policy = «linksOnly»
			this.moveOnClick=function(){/*DO NOTHING*/};
			this.moveOnHover=function(){/*DO NOTHING*/};
			this.moveOnDrag=function(){/*DO NOTHING*/};
			
			if(movePolicy=="pointOnly"){
				this.moveOnClick=this.moveOnClickPolicyPoint;			
			}else if(movePolicy=="pointAndBubbles"){
				this.moveOnClick=this.moveOnClickPolicyPointAndBubbles;			
			}else if(movePolicy=="dragAndBubbles"){
				this.moveOnClick=this.moveOnClickPolicyBubbles;
				this.moveOnDrag=this.moveOnDragPolicyDrag;
			}else if(movePolicy=="pointAndBubblesFit"){
				this.moveOnClick=this.moveOnClickPolicyPointAndBubbles;			
				this.fitZoomToBubbles=true;
			}else if(movePolicy=="dragAndBubblesFit"){
				this.moveOnClick=this.moveOnClickPolicyBubbles;
				this.moveOnDrag=this.moveOnDragPolicyDrag;
				this.fitZoomToBubbles=true;
			}else if(movePolicy=="dragOnly"){
				this.moveOnDrag=this.moveOnDragPolicyDrag;			
			}else if(movePolicy=="scroll"){
				this.moveOnHover=this.moveOnHoverPolicyScroll;			
			}else if(movePolicy=="bubblesOnly"){
				this.moveOnClick=this.moveOnClickPolicyBubbles;
			}else if(movePolicy=="bubblesOnlyFit"){
				this.moveOnClick=this.moveOnClickPolicyBubbles;
				this.fitZoomToBubbles=true;
			}


			// Hooks using :
			var self=this;
			/*static*/var hook=function(aotraScreen,bubble){
				if(aotraScreen.zoomingProvider && aotraScreen.aotraScreenPositionMovingPolicies.fitZoomToBubbles){
					aotraScreen.zoomToFitBubble(bubble);
				}
			}
			AotraScreen.hooks[DO_ON_ARRIVAL_HOOK_NAME].push(hook);

				
		}

		// Methods
		,moveOnClickPolicyBubbles : function(bubble,/*OPTIONAL>*/destinationX,destinationY/*<OPTIONAL*/) {
			var aotraScreen=this;
			if(bubble){
//				var xy=bubble.getDisplayXY(CENTER);
//				aotraScreen.moveToPosition(xy["x"],xy["y"],function(){
//					if(aotraScreen.zoomingProvider && aotraScreen.aotraScreenPositionMovingPolicies.fitZoomToBubbles){
//						aotraScreen.zoomToFitBubble(bubble);
//					}
//				});
				aotraScreen.doGotoBubble(bubble,true);
			}
		}

		,moveOnClickPolicyPoint : function(/*OPTIONAL>*/bubble/*<OPTIONAL*/,destinationX,destinationY) {
			var aotraScreen=this;
			
			if(!bubble){
				aotraScreen.moveToPosition(destinationX,destinationY);
			}
		}
		
		,moveOnClickPolicyPointAndBubbles : function(/*OPTIONAL>*/bubble/*<OPTIONAL*/,destinationX,destinationY) {
			var aotraScreen=this;
			if(!bubble){
				aotraScreen.moveToPosition(destinationX,destinationY);
			}else{
				
//				var xy=bubble.getDisplayXY(CENTER);
//				aotraScreen.moveToPosition(xy["x"],xy["y"],function(){
//					if(aotraScreen.zoomingProvider && aotraScreen.aotraScreenPositionMovingPolicies.fitZoomToBubbles){
//						aotraScreen.zoomToFitBubble(bubble);
//					}
//				});
				aotraScreen.doGotoBubble(bubble,true);
			}
		}
		
		
		,moveOnDragPolicyDrag : function(destinationX,destinationY) {
			let aotraScreen=this;
			aotraScreen.setScreenLocation(destinationX,destinationY);
		}
		
		
		,moveOnHoverPolicyScroll:function(direction,step){	
			// TODO
		}

		
	});

	return initSelf;
}
}

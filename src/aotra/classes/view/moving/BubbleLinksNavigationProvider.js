// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider


	// GLOBAL CONSTANTS
	
	var LINKS_DISPLAY_STRATEGY_NO_RETURN="noReturn";

	
if(typeof initBubbleLinksNavigationProviderClass === "undefined"){

	function initBubbleLinksNavigationProviderClass() {
	var initSelf = this;
	
	// CONSTANTS
	var LINKS_BUTTONS_MAX_Z_INDEX=Z_INDEX_LOW_UI_ELEMENTS;
	var BUTTONS_LINKS_TRANSPARENCY=0.7;
	
	initSelf.BubbleLinksNavigationProvider = Class.create({

		// Constructor
		// (This object is for a bubble only)
		initialize : function(aotraScreen,bubble) {

			// Attributes
			this.aotraScreen=aotraScreen;
			this.bubble=bubble;

			// Processed attributes
			this.areAllVisible=false;
		
			// Initialization
			this.linksButtonsSlct=new Array();
			
		}
	
	
	
		// Methods
	
		/*public*/,refreshLinksNavigationButtons:function(){
			
			var aotraScreen=this.aotraScreen;
			var plane=aotraScreen.getActivePlane();
			var bubble=this.bubble;
			
			if(bubble.getIsHomeBubble() || bubble.isMustNeverBeShown())
				return;
			if(!plane)	return; // This case can happen...

			
			var bubbleName=bubble.getName();
			
			// Forward links buttons :
			var bubbleLinks=plane.getOutgoingLinks(bubbleName);
			for(var i=0;i<bubbleLinks.length;i++){
				var link=bubbleLinks[i];
				
				if(link.isInvisible())	continue;
				
				var toBubbleName=link.getToBubbleName();
				var toBubble=plane.getBubbleByName(toBubbleName);
				
				if(nothing(toBubble)){
					//TRACE
					log("WARN : toBubble attribute for link was not found (toBubbleName:«"+toBubbleName+"»), skipping link navigation button creation.");
					continue;
				}
				
				if(bubble.isFixed() || toBubble.isFixed() || !toBubble.isNavigable() )	continue;

				var idLinkButton="linkButton_"+bubbleName+"-"+toBubbleName;
				var oldLinkButton=document.getElementById(idLinkButton);
				if(!oldLinkButton){
				
					var linkButtonSlctForward=jQuery(this.getLinkButtonHtmlForward(idLinkButton,toBubbleName)).css("position","absolute")
	//					.click(function(){jQuery(this).link.onClick();})
					;
					jQuery(aotraScreen.mainDiv).append(linkButtonSlctForward);
					this.linksButtonsSlct.push(linkButtonSlctForward);
					
					linkButtonSlctForward.get(0).otherBubble=link.getToBubble(aotraScreen);
					linkButtonSlctForward.link=link;

				}
					
			}
			
			// Backwards links buttons :
			if(plane.linksButtonsDisplayStrategy!==LINKS_DISPLAY_STRATEGY_NO_RETURN){
				
				var bubbleLinks=plane.getIngoingLinks(bubbleName);
				for(var i=0;i<bubbleLinks.length;i++){
					var link=bubbleLinks[i];
					if(link.isInvisible())	continue;
					
					var fromBubbleName=link.getFromBubbleName();
					var fromBubble=plane.getBubbleByName(fromBubbleName);
					if(bubble.isFixed() || fromBubble.isFixed() || !fromBubble.isNavigable() )	continue;
					
					var idLinkButton="linkButtonBackwards_"+bubbleName+"-"+fromBubbleName;
					var oldLinkButton=document.getElementById(idLinkButton);
					if(!oldLinkButton){
					
						var linkButtonSlctBackwards=jQuery(this.getLinkButtonHtmlBackwards(idLinkButton,fromBubbleName)).css("position","absolute")
		//					.click(function(){jQuery(this).link.onClickBackwards();})
						;
						jQuery(aotraScreen.mainDiv).append(linkButtonSlctBackwards);
						this.linksButtonsSlct.push(linkButtonSlctBackwards);
	
						linkButtonSlctBackwards.get(0).otherBubble=link.getFromBubble(aotraScreen);
						linkButtonSlctBackwards.link=link;
						
					}
				
				}
				
			}
			
		}

		
		/*private*/,getLinkButtonHtmlForward:function(idLinkButton,toBubbleName){
			return "<button id='"+idLinkButton+"' class='icon playBtn bubbleLinksButton'"
			// TODO : FIXME : Ideally, we should NEVER use directly aotraScreen.doGotoBubbleByName(...) function with a link, but link.onClick() and link.onClickBackwards() functions instead !
			+" onclick='javascript:getAotraScreen().doGotoBubbleByName(\""+toBubbleName+"\");' "
			+" title='"+i18n({"fr":"Aller à la bulle «"+toBubbleName+"»...","en":"Go to bubble «"+toBubbleName+"»"})+"'"
			+" style='opacity:"+BUTTONS_LINKS_TRANSPARENCY+"' ></button>";//&lt;
		}
		/*private*/,getLinkButtonHtmlBackwards:function(idLinkButton,fromBubbleName){
			return "<button id='"+idLinkButton+"' class='icon playReverseBtn bubbleLinksButton'"
			// TODO : FIXME : Ideally, we should NEVER use directly aotraScreen.doGotoBubbleByName(...) function with a link, but link.onClick() and link.onClickBackwards() functions instead !
			+" onclick='javascript:getAotraScreen().doGotoBubbleByName(\""+fromBubbleName+"\");' "
			+" title='"+i18n({"fr":"Retourner à la bulle «"+fromBubbleName+"»...","en":"Return to bubble «"+fromBubbleName+"»"})+"' "
			+" style='opacity:"+BUTTONS_LINKS_TRANSPARENCY+"' ></button>";//&lt;
		}
		
	
		/*public*/,showAllButtons:function(){
			
			if(!this.areAllVisible){
				this.refreshLinksNavigationButtons();
				this.areAllVisible=true;
			}
			for(var i=0;i<this.linksButtonsSlct.length;i++){
				var linkButtonSlct=this.linksButtonsSlct[i];
				this.placeNavigationButton(linkButtonSlct);
			}
		}
		/*public*/,hideAllButtons:function(){
			if(this.areAllVisible){
				this.areAllVisible=false;
			}
			for(var i=0;i<this.linksButtonsSlct.length;i++){
				var linkButtonSlct=this.linksButtonsSlct[i];
				if(linkButtonSlct.css("display")!=="none") linkButtonSlct.hide();
			}
		}
//		/*public*/,refreshOutGoingButtons:function(){
//		}

		
		/*private*/,placeNavigationButton:function(linkButtonSlct){
			
			var aotraScreen=this.aotraScreen;
			var zoomFactor=aotraScreen.getZoomFactor();
			
			var bubble=this.bubble;
			var bubbleDisplayX=bubble.getDisplayXY(CENTER)["x"];
			var bubbleDisplayY=bubble.getDisplayXY(CENTER)["y"];
			
			var otherBubble=linkButtonSlct.get(0).otherBubble;
			var offsets=this.getOffsets(bubble,otherBubble);
			var xOffset=offsets.x;
			var yOffset=offsets.y;
			
			linkButtonSlct.show();
			linkButtonSlct
			.css("left",(bubbleDisplayX-GLOBAL_ICONS_SIZE/2+xOffset*zoomFactor)+"px")
			.css("top", (bubbleDisplayY-GLOBAL_ICONS_SIZE/2+yOffset*zoomFactor)+"px")
			.css("z-index",LINKS_BUTTONS_MAX_Z_INDEX);
			
		}
		
		
		
		/*private*/,getOffsets:aotest(
		{	name:"getOffsets"
			,dummies:function(dummies){
				dummies.bubble0=new Object();
				dummies.bubble0.getDisplayXY=function(){ 	return {x:0,y:0};		};
				dummies.bubble0.getDisplayW=function(){	return 100; };
				dummies.bubble0.getDisplayH=function(){ return 100; };
				dummies.bubble0.isEquilateral=function(){	return false;	};
				dummies.bubble0.getMold=function(){	return "undetermined";	};
				dummies.bubble0Circle=new Object();
				dummies.bubble0Circle.getDisplayXY=function(){	return {x:0,y:0};	};
				dummies.bubble0Circle.getDisplayW=function(){	return 100; };
				dummies.bubble0Circle.getDisplayH=function(){ return 100; };
				dummies.bubble0Circle.isEquilateral=function(){	return true;	};
				dummies.bubble0Circle.getMold=function(){	return "circle";	};
				dummies.bubbleNE=new Object();
				dummies.bubbleNE.getDisplayXY=function(){ 	return {x:500,y:500};	};
				dummies.bubbleNE.isEquilateral=function(){	return false;	};
				dummies.bubbleNE.getMold=function(){	return "undetermined";	};
				dummies.bubbleNES=new Object();
				dummies.bubbleNES.getDisplayXY=function(){ 	return {x:500,y:250};	};
				dummies.bubbleNES.isEquilateral=function(){	return false;	};
				dummies.bubbleNES.getMold=function(){	return "undetermined";	};
				dummies.bubbleE=new Object();
				dummies.bubbleE.getDisplayXY=function(){ 	return {x:500,y:0};		};
				dummies.bubbleE.isEquilateral=function(){	return false;	};
				dummies.bubbleE.getMold=function(){	return "undetermined";	};
				dummies.bubbleN=new Object();
				dummies.bubbleN.getDisplayXY=function(){ 	return {x:0,y:500};		};
				dummies.bubbleN.isEquilateral=function(){	return false;	};
				dummies.bubbleN.getMold=function(){	return "undetermined";	};
				dummies.bubbleNW=new Object();
				dummies.bubbleNW.getDisplayXY=function(){  	return {x:-500,y:500};	};
				dummies.bubbleNW.isEquilateral=function(){	return false;	};
				dummies.bubbleNW.getMold=function(){	return "undetermined";	};
				dummies.bubbleSE=new Object();
				dummies.bubbleSE.getDisplayXY=function(){	return {x:500,y:-500};	};
				dummies.bubbleSE.isEquilateral=function(){	return false;	};
				dummies.bubbleSE.getMold=function(){	return "undetermined";	};
				dummies.bubbleSW=new Object();
				dummies.bubbleSW.getDisplayXY=function(){	return {x:-500,y:-500};	};
				dummies.bubbleSW.isEquilateral=function(){	return false;	};
				dummies.bubbleSW.getMold=function(){	return "undetermined";	};
				return dummies;
			}
			,scenarioNE:[ [ {dummy:"bubble0"},{dummy:"bubbleNE"} ],function(result){ return result.x===50 && result.y===50; } ]
			,scenarioNES:[ [ {dummy:"bubble0"},{dummy:"bubbleNES"} ],function(result){ return result.x===50 && result.y===25; } ]
			,scenarioE:[ [ {dummy:"bubble0"},{dummy:"bubbleE"} ],function(result){ return result.x===50 && result.y===0; } ]
			,scenarioN:[ [ {dummy:"bubble0"},{dummy:"bubbleN"} ],function(result){ return result.x===0 && result.y===50; } ]
			,scenarioNW:[ [ {dummy:"bubble0"},{dummy:"bubbleNW"} ],function(result){ return result.x===-50 && result.y===50; } ]
			,scenarioSE:[ [ {dummy:"bubble0"},{dummy:"bubbleSE"} ],function(result){ return result.x===50 && result.y===-50; } ]
			,scenarioSW:[ [ {dummy:"bubble0"},{dummy:"bubbleSW"} ],function(result){ return result.x===-50 && result.y===-50; } ]
			,scenarioSWCircle:[ [ {dummy:"bubble0Circle"},{dummy:"bubbleSW"} ],function(result){
					return result.x==Math.round(-50*Math.cos(Math.PI/4)) && result.y==Math.round(-50*Math.sin(Math.PI/4)); 
				} ]
		}
		,function(fromBubble,toBubble){
			var aotraScreen=this.aotraScreen;
			var offsets=new Object();

			var toBubbleCoords=toBubble.getDisplayXY(CENTER);
			var fromBubbleCoords=fromBubble.getDisplayXY(CENTER);

			var deltaX=toBubbleCoords["x"]-fromBubbleCoords["x"];
			var deltaY=toBubbleCoords["y"]-fromBubbleCoords["y"];

			var demiWidth=fromBubble.getDisplayW(true)/2;
			var demiHeight=fromBubble.getDisplayH(true)/2;
			
			var signDeltaX=deltaX<0 ? -1:1 ;
			var signDeltaY=deltaY<0 ? -1:1 ;
			
			var xOffset=0;
			var yOffset=0;
			
			if(	fromBubble.getMold()===CIRCLE_MOLD && fromBubble.isEquilateral()){
				var radius=demiWidth;
				var distance=Math.sqrt(Math.pow(deltaX,2)+Math.pow(deltaY,2));
				
				xOffset=distance===0?0:Math.round((radius*deltaX)/distance);
				yOffset=distance===0?0:Math.round((radius*deltaY)/distance);
				
			}else{
				
				xOffset=deltaY===0 ? signDeltaX*demiWidth:(deltaX*demiHeight)/Math.abs(deltaY);
				yOffset=deltaX===0 ? signDeltaY*demiHeight:(deltaY*demiWidth)/Math.abs(deltaX);
				
			}
			
			offsets.x=signDeltaX*Math.coerceInRange(xOffset,0,demiWidth,true);
			offsets.y=signDeltaY*Math.coerceInRange(yOffset,0,demiHeight,true);
			
			return offsets;
		})
		
		
		
		
		
	
	});
	
	
	
	
	
	// Static initialization :

	/*static*/{
		
		// Use hooks for bubble initialization
		var bubbleHook=new Object();
		Bubble.hooks[DO_ON_CONTENT_RENDER].push(bubbleHook);
		bubbleHook.execute=function(aotraScreen,bubble){
		
			var plane=aotraScreen.getActivePlane();
			
			if(!PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(plane)) return;
			// DEPENDENCY : PlaneVerticalScrollingBubblesPresentingProvider
			
			// DEPENDENCY : BubbleLinksNavigationProvider code in this class !
			// Bubble links navigation buttons :
			if(!bubble.linksNavigationProvider)	return;
			
			if( bubble.isNavigable() && !bubble.isFixed()){
				// Bubble links navigation buttons visibility and position refresh
				bubble.linksNavigationProvider.refreshLinksNavigationButtons();
			}
			
		};
	}
	
	
	
	
	
	
	
	

	return initSelf;
}
}

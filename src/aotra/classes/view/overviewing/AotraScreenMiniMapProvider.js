// GLOBAL CONSTANTS


if(typeof initAotraScreenMiniMapProviderClass ==="undefined"){
	

function initAotraScreenMiniMapProviderClass() {
	var initSelf = this;

	// CONSTANTS
	var MINI_MAP_SIZE=250;
	var ADJUSTEMENT_FACTOR=0.60;
	var MIN_CLICK_AREA_SIZE=25;
	var MINI_MAP_BACKGROUND="#FFFFFF";
	var MARGIN_ADJUSTMENT_FACTOR=1.2;
	
	initSelf.AotraScreenMiniMapProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {

			// Attributes
			this.aotraScreen = aotraScreen;
			
			
			// Technical attributes :
			this.paper=null;
//			this.paperElement=null;
			
			// Initialization
			this.initUI();
			
			
			// We use the aotraScreen edition provider hook «onModelChangeEvents» :
			var self=this;
			if(this.aotraScreen.editionProvider){
				var event=new Object();
				event.execute=function(){
					self.refreshMiniMap();
				};
				this.aotraScreen.editionProvider.onModelChangeEvents.push(event);
			}
			
		}
	
		// Methods
		/*private*/,initUI:function(){
	
			var mainDiv=this.aotraScreen.mainDiv;
			var mainDivInnerHTML = mainDiv.innerHTML;
			
			mainDivInnerHTML+=this.getToolsBarHTMLButtons();
			mainDivInnerHTML+=this.getGeneratedMiniMapProviderHTMLDialog();
			mainDiv.innerHTML=mainDivInnerHTML;
			
			// Implementation graphic unit initialization :
			this.paper=Raphael(0, 0, MINI_MAP_SIZE, MINI_MAP_SIZE);
			var paperElement=document.getElementsByTagName("svg")[0];
			// DOES NOT APPLY TO PAPER ELEMENT : CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
			paperElement.style.position="relative";
			document.getElementById("miniMapSlot").appendChild(paperElement);
			
			
		}
		

		// Mini-map provider toolbar :
		/*private*/,getToolsBarHTMLButtons:function(){
			
			var mainDivInnerHTML="";		
		
			var UI_X=10;
			var UI_Y=46;
			
			mainDivInnerHTML+="<div id='divMinimapButton' class='buttonsHolder' style='left:"+UI_X+"px; top:"+UI_Y+"px ; z-index:"+(MAX_Z_INDEX-1)+";' >"
			mainDivInnerHTML+="<button id='minimapButton' onclick='javascript:getAotraScreen().miniMapProvider.toggleMiniMap();' class='icon miniMapBtn'"
				+" title='"+i18n({"fr":"Mini-carte","en":"Mini-Map"})+"'></button>";
			mainDivInnerHTML+="</div>";
		
			return mainDivInnerHTML;
		}
		
		/*public*/,removeUI:function(){
			document.getElementById("minimapButton").style.display="none";
			document.getElementById("divMiniMap").style.display="none";
		}
		
		
		// Mini-map dialog :
		/*private*/,getGeneratedMiniMapProviderHTMLDialog:function(){
			var mainDivInnerHTML="";
			
			mainDivInnerHTML+="<div id='divMiniMap' class='buttonsHolder' "
							+"style='display:none;"
							+"left:75px;top:40px;z-index:"+MAX_Z_INDEX_OVERLAY+";"
							+"background:#2CFFF5' >";
			mainDivInnerHTML+="<div style='margin:5px;background:"+MINI_MAP_BACKGROUND+"' id='miniMapSlot'></div>";
			mainDivInnerHTML+="<button style='float:right;' class='icon closeBtn' onclick='javascript:getAotraScreen().miniMapProvider.close();' title='"+i18n({"fr":"Fermer","en":"Close"})+"'></button>";
			mainDivInnerHTML+="</div>";
			
			
			return mainDivInnerHTML;
		}
		
		/*private*/,toggleMiniMap:function(){
			var visible=!isElementVisible("divMiniMap");
			setElementVisible("divMiniMap",visible);
			if(visible)	this.refreshMiniMap();
		}

		/*private*/,close:function(){
			setElementVisible("divMiniMap",false);
		}
		
		
		/*public*/,refreshMiniMap:function(){
			if(!this.isDialogVisible())	return;

			var aotraScreen=this.aotraScreen;
			var plane=aotraScreen.getActivePlane();
			var paper=this.paper;
			paper.clear();
			
			
			// First of all, we draw the mark, to ensure it will not be clickable :
			this.mark=null;
			this.refreshViewMark();

			// TODO : FIXME : Factorize this duplicate code...
			var extrema=this.getAllBubbleCoordinatesExtrema();
			var xMin=extrema["min"]["x"]?extrema["min"]["x"]:-10;
			var yMin=extrema["min"]["y"]?extrema["min"]["y"]:-10;
			var xMax=extrema["max"]["x"]?extrema["max"]["x"]:10;
			var yMax=extrema["max"]["y"]?extrema["max"]["y"]:10;
			
			var realWidth=xMax-xMin;
			var realHeight=yMax-yMin;
			
			// We want a square to fit !
			realWidth=Math.max(realWidth,realHeight)*MARGIN_ADJUSTMENT_FACTOR;
			realHeight=realWidth*MARGIN_ADJUSTMENT_FACTOR;
			
			var xFactorScreenToMini=(MINI_MAP_SIZE/realWidth)*ADJUSTEMENT_FACTOR;
			var yFactorScreenToMini=(MINI_MAP_SIZE/realHeight)*ADJUSTEMENT_FACTOR;
			
			var deltaX=realWidth*xFactorScreenToMini/2  *(1/ADJUSTEMENT_FACTOR);
			var deltaY=realHeight*yFactorScreenToMini/2 *(1/ADJUSTEMENT_FACTOR);
			
			
			// sub-bubbles links
			var bubbles=plane.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				var b=bubbles[i];
				if(!b.isVisible() || b.isFixed())	continue;
				
				var pb=b.getParentBubble();
				if(!pb)	continue;
				if(!pb.isVisible() || pb.isFixed())	continue;

				var b1=b;
				var b2=pb;

				// TODO : FIXME : Resolve code duplication here...
				var offset1=b1.getOffsetBecauseOfParentXY();
				var offset2=b2.getOffsetBecauseOfParentXY();
				var miniX1=(b1.canonicalX+offset1["x"])*xFactorScreenToMini+deltaX;
				var miniY1=(b1.canonicalY+offset1["y"])*yFactorScreenToMini+deltaY;

				var miniX2=(b2.canonicalX+offset2["x"])*xFactorScreenToMini+deltaX;
				var miniY2=(b2.canonicalY+offset2["y"])*yFactorScreenToMini+deltaY;

				var color=b1.getColor();
				
				var line=paper.path("M"+Math.floor(miniX1)+","+Math.floor(miniY1)+"L"+Math.floor(miniX2)+","+Math.floor(miniY2));
				line.attr("stroke", color);
				
				var miniW=(b1.w/2)*xFactorScreenToMini;	// because we want the radius, actually...
				var miniH=(b1.h/2)*xFactorScreenToMini;	// because we want the radius, actually...

				line.attr("stroke-width", (Math.max(miniW,miniH)*2)+"px"); // «*2» because we used the radius, actually...
				line.attr("stroke-linecap", "round");

				
			}
			
			// links
			var links=plane.getLinks();
			for(var i=0;i<links.length;i++){
				var l=links[i];
				
				var b1=l.getFromBubble(aotraScreen);
				var b2=l.getToBubble(aotraScreen);

				if(!b1){
					// TRACE
					log("WARN : Link (generated html direct tag would be : «"+l.getHTMLTag()+"») has no «from» bubble «"+l.fromBubbleName+"» !");
					continue;
				}
				if(!b2){
					// TRACE
					log("WARN : Link (generated html direct tag would be : «"+l.getHTMLTag()+"») has no «to» bubble «"+l.toBubbleName+"» !");
					continue;
				}
				
				if(!b1.isVisible() || !b2.isVisible())	continue;
				
				// TODO : FIXME : Resolve code duplication here...
				var offset1=b1.getOffsetBecauseOfParentXY();
				var offset2=b2.getOffsetBecauseOfParentXY();
				var miniX1=(b1.canonicalX+offset1["x"])*xFactorScreenToMini+deltaX;
				var miniY1=(b1.canonicalY+offset1["y"])*yFactorScreenToMini+deltaY;

				var miniX2=(b2.canonicalX+offset2["x"])*xFactorScreenToMini+deltaX;
				var miniY2=(b2.canonicalY+offset2["y"])*yFactorScreenToMini+deltaY;

				var color=l.color;
				
				var line=paper.path("M"+Math.floor(miniX1)+","+Math.floor(miniY1)+"L"+Math.floor(miniX2)+","+Math.floor(miniY2));
				line.attr("stroke", color);
				line.attr("stroke-width", "3px");
				
			}
			
			// bubbles
			var bubbles=plane.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				var b=bubbles[i];

				if(!b.isVisible() || b.isFixed())	continue;
				
				var offset=b.getOffsetBecauseOfParentXY();
				var miniX=(b.canonicalX+offset["x"])*xFactorScreenToMini+deltaX;
				var miniY=(b.canonicalY+offset["y"])*yFactorScreenToMini+deltaY;

//				var miniDiameter=(b.diameter/2)*xFactorScreenToMini;// because we want the radius, actually...
				var miniW=(b.w/2)*xFactorScreenToMini;	// because we want the radius, actually...
				var miniH=(b.h/2)*xFactorScreenToMini;	// because we want the radius, actually...
				
				var mold=b.getMold();
				var shape=null;
				if(mold===SQUARED_MOLD || mold===ROUNDED_MOLD ){
//					shape=paper.rect(miniX-miniDiameter/2,miniY-miniDiameter/2,miniDiameter,miniDiameter);
					shape=paper.rect(miniX-miniW/2,miniY-miniW/2,miniW,miniW);	// w==h in this case
				}else{
//					shape=paper.circle(miniX,miniY,miniDiameter);
					shape=paper.circle(miniX,miniY,miniW); // w==h in this case
				}
				
				shape.attr("fill", b.color);
				shape.attr("stroke", "#000000").attr("stroke-width", "2px");
				
				// Click events :
//				var clickAreaSize=Math.max(MIN_CLICK_AREA_SIZE,miniDiameter*2);
//				var clickArea=paper.rect(miniX-clickAreaSize/2,miniY-clickAreaSize/2,clickAreaSize,clickAreaSize);
				var clickAreaSizeW=Math.max(MIN_CLICK_AREA_SIZE,miniW*2);
				var clickAreaSizeH=Math.max(MIN_CLICK_AREA_SIZE,miniH*2);
				var clickArea=paper.rect(miniX-clickAreaSizeW/2,miniY-clickAreaSizeH/2,clickAreaSizeW,clickAreaSizeH);
				
				clickArea.attr("stroke","none");
				clickArea.attrs["bubble"]=b;
				clickArea.click(function(event){
					aotraScreen.doGotoBubble(this.attrs["bubble"]);
				});
				
			}
			
		}

		/*public*/,refreshViewMark:function(){
			
			if(!this.isDialogVisible())	return;
			
			var aotraScreen=this.aotraScreen;
			var paper=this.paper;
			
			// TODO : FIXME : Factorize this duplicate code...
			var extrema=this.getAllBubbleCoordinatesExtrema();
			var xMin=extrema["min"]["x"]?extrema["min"]["x"]:-10;
			var yMin=extrema["min"]["y"]?extrema["min"]["y"]:-10;
			var xMax=extrema["max"]["x"]?extrema["max"]["x"]:10;
			var yMax=extrema["max"]["y"]?extrema["max"]["y"]:10;
			
			var realWidth=xMax-xMin;
			var realHeight=yMax-yMin;

			// We want a square to fit !
			realWidth=Math.max(realWidth,realHeight)*MARGIN_ADJUSTMENT_FACTOR;
			realHeight=realWidth*MARGIN_ADJUSTMENT_FACTOR;

			var xFactorScreenToMini=(MINI_MAP_SIZE/realWidth)*ADJUSTEMENT_FACTOR;
			var yFactorScreenToMini=(MINI_MAP_SIZE/realHeight)*ADJUSTEMENT_FACTOR;
			
			var deltaX=realWidth*xFactorScreenToMini/2  *(1/ADJUSTEMENT_FACTOR);
			var deltaY=realHeight*yFactorScreenToMini/2 *(1/ADJUSTEMENT_FACTOR);
			
			var screenMiniX=aotraScreen.x*(1/aotraScreen.getZoomFactor())*xFactorScreenToMini;
			var screenMiniY=aotraScreen.y*(1/aotraScreen.getZoomFactor())*yFactorScreenToMini;

			var screenMiniWidth=aotraScreen.getWidth()*(1/aotraScreen.getZoomFactor())*xFactorScreenToMini;
			var screenMiniHeight=aotraScreen.getHeight()*(1/aotraScreen.getZoomFactor())*yFactorScreenToMini;

			var miniMarkX=screenMiniX-screenMiniWidth/2+deltaX;
			var miniMarkY=screenMiniY-screenMiniHeight/2+deltaY;
			
			if(!this.mark){
				this.mark=paper.rect(miniMarkX,miniMarkY,screenMiniWidth,screenMiniHeight);
				this.mark.attr("stroke", "#888888").attr("stroke-width", "2px");
			} else{
				this.mark.attr("x",miniMarkX).attr("y",miniMarkY)
						 .attr("width",screenMiniWidth).attr("height",screenMiniHeight);
			}
			

			
		}
		
		/*private*/,getAllBubbleCoordinatesExtrema:function(){
			var xs=new Array();
			var ys=new Array();
			var result=new Object();
			var plane=this.aotraScreen.getActivePlane();
			var bubbles=plane.getBubbles();
			
			if(1<bubbles.length){
				for(var i=0;i<bubbles.length;i++){
					var b=bubbles[i];
					var offset=b.getOffsetBecauseOfParentXY();
					xs.push(b.x+offset["x"]);
					ys.push(b.y+offset["y"]);
				}
				result["max"]=new Object();
				result["max"]["x"]=Math.maxInArray(xs);
				result["max"]["y"]=Math.maxInArray(ys);
				result["min"]=new Object();
				result["min"]["x"]=Math.minInArray(xs);
				result["min"]["y"]=Math.minInArray(ys);
			}else if(bubbles.length===1){
				// Default values if there is only one bubble :
				var singleBubble=bubbles[0];
				var w=singleBubble.getDisplayW(true);
				var h=singleBubble.getDisplayH(true);
				result["max"]=new Object();
				result["max"]["x"]=w;
				result["max"]["y"]=h;
				result["min"]=new Object();
				result["min"]["x"]=-w;
				result["min"]["y"]=-h;
			}else{
				// Default values if there is no bubble :
				result["max"]=new Object();
				result["max"]["x"]=10;
				result["max"]["y"]=10;
				result["min"]=new Object();
				result["min"]["x"]=-10;
				result["min"]["y"]=-10;
			}
		
			return result;
		}
		
		/*private*/,isDialogVisible:function(){
			return isElementVisible("divMiniMap");
		}
		
		
	});

	return initSelf;
}
}
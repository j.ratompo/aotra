
	// GLOBAL CONSTANTS
	var SCROLL_HOOK_PARAM="scroll";
	var VERTICAL_SCROLLING_MODE="vertical";
	var SCROLL_OPTIONS_HOOK_PARAM="scrollOptions";
	var ONLY_ONE_BUBBLE_DISPLAYED_AT_A_TIME_OPTION="onlyOneBubble";
	var VERTICAL_FOR_FRONT_END_ONLY_OPTION="frontEndOnly";

if(typeof initPlaneVerticalScrollingBubblesPresentingProviderClass === "undefined"){

	function initPlaneVerticalScrollingBubblesPresentingProviderClass() {
	var initSelf = this;
	
	// CONSTANTS
	var MODULE_KEY="PlaneVerticalScrollingBubblesPresentingProvider";
	var DEFAULT_STEP_FACTOR=10;
	var SCROLL_GOTO_DURATION=800;

	
	var VERTICAL_MARGINS="verticalMargins";
	var HORIZONTAL_MARGINS="horizontalMargins";
	// TODO : Being able to parametrize this...
	var VERTICAL_MARGINS_SIZE_PERCENT_DEFAULT=10;
	// TODO : Being able to parametrize this...
	var HORIZONTAL_MARGINS_SIZE_PERCENT_DEFAULT=10;
	
	
	initSelf.PlaneVerticalScrollingBubblesPresentingProvider = Class.create({

		// Constructor
		initialize : function(plane,/*OPTIONAL*/scrollPresentationMode) {

			// Attributes
			this.plane=plane;
			this.stepFactor=DEFAULT_STEP_FACTOR;
			this.scrollPresentationMode=scrollPresentationMode?scrollPresentationMode:VERTICAL_MARGINS;// This is the default value
			
			// Processed attributes
			var self=this;
//			this.viewY=0;
			this.bubblesHeights=new Object();
			// This attribute is the width percentage for the set of sub-bubbles of a given bubble
			this.subBubblesWidthPercent=new Object();
			// This attribute is the max height for the set of sub-bubbles of a given bubble			
			this.subBubblesMaxHeight=new Object();
			
			var options=plane.hooksParameters[SCROLL_OPTIONS_HOOK_PARAM];
			this.isOnlyOneBubbleDisplayedAtATime = !nothing(options) && contains(options,ONLY_ONE_BUBBLE_DISPLAYED_AT_A_TIME_OPTION);

			
			// Initialization :
//			this.initWheel();
			this.refreshBubblesCalculatedInformation();
			var aotraScreen=plane.getAotraScreen();
			if(aotraScreen.editionProvider){
				
				var event=new Object();
				event.execute=function(){
					self.refreshBubblesCalculatedInformation(true);
				};
				aotraScreen.editionProvider.onModelChangeEvents.push(event);	
			}
			
			// Technical attributes :
			this.currentlyCenteredBubble=plane.getHomeBubble();
			this.isAutoScrolling=false;

			
			// Must be done after the cumulated heights calculation :
			this.initScreen();

		}
	
		// Methods
		/*private*/,initScreen:function(){
			// Some aotraScreen container elements tweaking :
			var plane=this.plane;
			var aotraScreen=plane.getAotraScreen();
			
			var self=this;
			mainDiv.onscroll=function(){
				self.onScroll();
			};
			
		}
		
	
		/*public*/,refreshBubblesCalculatedInformation:function(/*OPTIONAL*/recalculate){
			var plane=this.plane;
			
			var heightsSum=0;
			var bubbles=plane.getBubbles();
			
			// Starting from 1, because we want to skip height adding for first encountered bubble
			for(var i=1;i<bubbles.length;i++){
				var b=bubbles[i-1];
				
				// We ignore visible, but fixed bubbles
				if(b.isFixed())	continue;
				
				var h=b.getDisplayH(true);
				heightsSum+=h;
				this.bubblesHeights[b.getName()]=heightsSum;
				
			}
			
			// All bubbles :
			for(var i=0;i<bubbles.length;i++){
				var b=bubbles[i];

				// We ignore visible, but fixed bubbles
				if(b.isFixed())	continue;
				
				if(!b.isFirstLevelBubble()){
					
					
					var parentBubble=b.getParentBubble();
					var parentBubbleName=parentBubble.getName();

					// This attribute is the width percentage for the set of sub-bubbles of a given bubble
					if(!this.subBubblesWidthPercent[parentBubbleName] || recalculate){
						
						var subBubblesNumber=parentBubble.getSubBubbles().length;

						var basePercent=100;
						if(this.scrollPresentationMode===HORIZONTAL_MARGINS){
							/*DO NOTHING*/
						}else{ // Vertical margins case (default case)
							basePercent=100-VERTICAL_MARGINS_SIZE_PERCENT_DEFAULT;
						}
						
						var widthPercent=Math.round(basePercent/subBubblesNumber);

						this.subBubblesWidthPercent[parentBubbleName]=widthPercent;
						
					}
					
					// This attribute is the max height for the set of sub-bubbles of a given bubble			
					if(!this.subBubblesMaxHeight[parentBubbleName] || recalculate){

						var maxHeight=0;
						var subBubbles=parentBubble.getSubBubbles();
						for(var j=0;j<subBubbles.length;j++){
							var subBubbleHeight=subBubbles[j].getDisplayH(true);
							if(maxHeight<subBubbleHeight)
								maxHeight=subBubbleHeight;
						}
						this.subBubblesMaxHeight[parentBubbleName]=maxHeight;
						
					}
				}
			
			}
			
		}
	
		// This method is called each time we go to a vertical bubble for the first time if the bubble content has to be displayed at first :
		/*public*/,placeDivScroll:function(bubble,/*OPTIONAL*/refreshContent){
			
			// We ignore visible, but fixed bubbles
			if(bubble.isFixed()){
				bubble.placeDivHeuroptic(refreshContent);
				return;
			}
			
			var style="";
			
			
			// Position displaying : is different
			// Size displaying : is different
			//NO : (vertical scrollable bubble is no more openable / closable)
			// (vertical scrollable bubble does not display links nor links navigationButtons)

			// Position displaying AND visibility management
			style+=this.positionDisplayCSS(bubble);
			
			// Size displaying
			style+=this.sizeDisplayCSS(bubble);
			
			// Mold displaying
			style+=this.moldDisplayCSS(bubble);
			
			// Background :
			style+=this.borderDisplayCSS(bubble);				
			
			// Border :
			/*DO NOTHING*/
			
			
			// Opacity management when opening / closing
			// TODO.
			
			// CANNOT MANAGE visibilityDisplayCSS HERE, cf. method this.doOnCenteredBubbleChange(...) to implement visibility management instead !
			

			// Background color and opacity displaying : is the same
			style+=bubble.backgroundDisplayCSS();				
			
			// We cannot use Prototype .setStyle(...) method here, because the style needs to be completely replaced here :
			bubble.applyStyle(style,true);

			// Text container div refresh : is the same
			bubble.displayContent(refreshContent);

		}
		
		/*private*/,borderDisplayCSS:function(bubble){
			var style="";

			style+="border:"+bubble.getBorderStyle()+";";
			
			return style;
		}		

		/*private*/,positionDisplayCSS:function(bubble){
			
			var canShowBubble=bubble.canShowBubble(true);
			
			if(!canShowBubble){
				return "display:none !important;";
			}
			
			var style="";
			var height=bubble.getDisplayH(true);

			// TODO : FIXME : WORKAROUND to compensate a graphical strange bug...:
			style+="margin-top:-6px;";

			
			// Case bubble is a root parent bubble :
			if(bubble.isFirstLevelBubble()){
				
				// «flex» CSS mode cannot work in this special context :
				style+="display:block !important;";
				
				style+="position:relative;";

			}else{
				// Sub-bubbles display
				var parentBubble=bubble.getParentBubble();
				
				// «flex» CSS mode cannot work in this special context :
				style+="display:inline-block !important;";
				
				style+="position:relative;";

				// TODO : FIXME : Do not make this calculus too each time for each sub-bubble :
				// If this is the first sub-bubble : then margin left to center it :
				var isFirstSubBubble=parentBubble.getSubBubbles()[0].getName()===bubble.getName();
				if(isFirstSubBubble)
					style+="margin-left:"+(Math.round(VERTICAL_MARGINS_SIZE_PERCENT_DEFAULT/2))+"%;";

			}
			
			return style;
		}
		
		/*private*/,sizeDisplayCSS:function(bubble){
			var style="";
			
			
			// Case bubble is NOT a root parent bubble :
			if(bubble.isFirstLevelBubble()){
				
				if(this.scrollPresentationMode===HORIZONTAL_MARGINS){
					style+="margin-bottom:"+(VERTICAL_MARGINS_SIZE_PERCENT_DEFAULT)+"%;";
				}else{ // Vertical margins case (default case)
					style+="margin-left:"+(VERTICAL_MARGINS_SIZE_PERCENT_DEFAULT/2)+"%;";
					style+="margin-right:"+(VERTICAL_MARGINS_SIZE_PERCENT_DEFAULT/2)+"%;";
				}
			
				style+="height:auto;";

			}else{
				
				// Sub-bubbles display
				var parentBubble=bubble.getParentBubble();
				var parentBubbleName=parentBubble.getName();
				
				var widthPercent=this.subBubblesWidthPercent[parentBubbleName];
				style+="width:"+(widthPercent)+"%;";
				
				var height=this.subBubblesMaxHeight[parentBubbleName];
				style+="height:"+(height)+"px;";
				
				// Apparently, graphic designers say they prefer when overflowing content is hidden O_o ...:
//				style+="overflow-y:auto;";
				style+="overflow-y:hidden;";
				
			}
					
			
			return style;

		}
		
		
		/*private*/,moldDisplayCSS:function(bubble){
			var style="";
			// TODO : Develop.
			return style;
		}

		
		/*private*/,onScroll:function(){
			
			if(this.isAutoScrolling===true)	return;
			
			var centeredBubble=this.getCenteredBubble();
			
			if(nothing(centeredBubble))	return;
			
			if(!nothing(this.currentlyCenteredBubble) && !this.currentlyCenteredBubble.equals(centeredBubble)){
//				this.doOnCenteredBubbleChange(this.currentlyCenteredBubble,centeredBubble);
			
				// We force bubble opening, because it is the same bahavior as links clicking :
				this.manageOpenClose(centeredBubble,true);

				this.currentlyCenteredBubble=centeredBubble;

			}

		}

//		// UNUSED
//		/*private*/,doOnCenteredBubbleChange:function(oldBubble,newBubble){
//			/*UNUSED*/
//		}
		
		/*private*/,getCenteredBubble:function(){
			var plane=this.plane;

			var bubbles=plane.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				var b=bubbles[i];
				var isBubbleInCenter=this.isScrolledIntoView(b);
				if(isBubbleInCenter)	return b;
			}
			return null;
		}
		
		
		
		/*private*/,isScrolledIntoView:function(bubble){
			var plane=this.plane;
			var aotraScreen=plane.getAotraScreen();

			var height=aotraScreen.getHeight();
			
			var divSlct=jQuery(bubble.div);
			var y=divSlct.outerHeight();
			
			var scrollCursorY=jQuery(window).scrollTop();
			
		    var centerY = Math.max(0,(height-y / 2)+scrollCursorY);

		    var elementTop = divSlct.offset().top;
		    var elementBottom = elementTop + divSlct.height();

		    var result=elementTop <= centerY && centerY <= elementBottom;
		    
		    
		    // OTHER POSSIBLE WAY :
//		    function isScrolledIntoView(element) {
//		        var elemTop = element.getBoundingClientRect().top;
//		        var elemBottom = element.getBoundingClientRect().bottom;
//
//		        var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
//		        return isVisible;
//		    }
		    
		    return result;
		}
		
		
		/*private*/,manageOpenClose:function(bubble,/*OPTIONAL*/forceOpeningOnGoto){
			
			// This mode overrides all other behaviors :
			// We close the bubble we just left and open the one we just landed on if we are in this overriding configuration,
			// no matter the other concerns :
			var isOnlyOneBubbleDisplayedAtATime=this.isOnlyOneBubbleDisplayedAtATime;

			// We close the previous (those we just left) bubble :
			var oldBubble=this.currentlyCenteredBubble;
			if(!nothing(oldBubble) ){
				
				if( (isOnlyOneBubbleDisplayedAtATime || !oldBubble.canOpenBubble()) && oldBubble.isOpen()){
					// This is a special case, here, in default heuroptic mode, this method MUST NEVER be used directly :
					oldBubble.setOpen(false);
					oldBubble.placeDiv();
				}
			}
			
			// In vertical mode, first we open the bubble, and then we open it, if needed :
			// In default (heuroptic) mode, it's the contrary !
			// Bubble opening :
			if((isOnlyOneBubbleDisplayedAtATime || forceOpeningOnGoto) && !bubble.isOpen()){
				// This is a special case, here, in default heuroptic mode, this method MUST NEVER be used directly :
				bubble.setOpen(true);
				bubble.placeDiv();
			}

		
		}
		
		
		/*public*/,doGotoBubble:function(bubble,doOnArrival,/*OPTIONAL*/forceOpeningOnGoto){
			// (http://stackoverflow.com/questions/5309483/scrollto-function-horizontally)
			// https://github.com/flesler/jquery.scrollTo

			var self=this;
			var plane=this.plane;
			var aotraScreen=plane.getAotraScreen();
			
			// 1) We open the bubble :
			
			this.manageOpenClose(bubble,forceOpeningOnGoto);
			
			// 2) We go to the bubble :
			
			this.currentlyCenteredBubble=bubble;

			// We do not perform the jQuery scroll if it is not actually really needed 
			// (for example if a bubble goes in the same place as the old one that is closed)
			if(this.isScrolledIntoView(bubble))	return;
			
			
			var mainDiv=aotraScreen.getMainDiv();	//OLD : document.getElementById(MAIN_DIV_ID)
			var bubbleDiv=bubble.getDiv();
			
			// Caution : this jQuery onScoll will trigger the onScroll detection :
			this.isAutoScrolling=true;
			jQuery(mainDiv).scrollTo(bubbleDiv,SCROLL_GOTO_DURATION, {axis:"y",onAfter:	function(){
					doOnArrival();
					self.isAutoScrolling=false;
				}
			});
			
		}
		
		
		/*public*/,onChangeScrollMode:function(aotraScreen){
			var plane=aotraScreen.getActivePlane();			
			
			PlaneVerticalScrollingBubblesPresentingProvider.setScrollMode(plane,VERTICAL_SCROLLING_MODE);
		
		}
		
		
		
	});
	
	
	
	// Static initialization :
	/*static*/{
		
		
		initSelf.PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive=function(plane){
			
			var scrollParam=plane.hooksParameters[SCROLL_HOOK_PARAM];
			
			// TODO : FIXME : We should instead be able to handle an arbitrary list of modes !...
			var isModeUnrecognized=(scrollParam!==HEUROPTIC_MODE && scrollParam!==VERTICAL_SCROLLING_MODE)

			//TRACE
			if(isModeUnrecognized) log("WARN : Scrolling mode was not recognized, assuming default, heuroptic mode.");
			
			return (!plane.verticalScrollingProvider
			|| nothing(scrollParam)	|| scrollParam===HEUROPTIC_MODE || isModeUnrecognized);
		};
		

		initSelf.PlaneVerticalScrollingBubblesPresentingProvider.setScrollMode=function(plane,mode,/*OPTIONAL*/skipXMLRefresh){
			
			plane.hooksParameters[SCROLL_HOOK_PARAM]=mode;
			var scrollOptionId="optionScroll"+mode;
			document.getElementById(scrollOptionId).selected="selected";

			if(!skipXMLRefresh)
				AotraScreen.updateAotraScreenFromXML();
		
		};

		initSelf.PlaneVerticalScrollingBubblesPresentingProvider.isSpecialMode=function(plane){

			var result=false;
			// This mode overrides all other behaviors :
			var options=plane.hooksParameters[SCROLL_OPTIONS_HOOK_PARAM];
			if(!nothing(options)){

  			var isVerticalScrollModeOnlyForFrontEnd=!nothing(options) && contains(options,VERTICAL_FOR_FRONT_END_ONLY_OPTION);
  			
  			// This mode overrides all other behaviors :
  			// We force the default (ie. heuroptic, ie. non-vertical scrolling) mode at startup
  			// if we are in this overriding configuration :
  			if(isVerticalScrollModeOnlyForFrontEnd){
  				result=true;
  			}
  		}
			
			return result;
			
		};

		
		// Use hooks for plane import/export
		var importer=new Object();
		AotraScreenImporterFromXML.planeImporters[MODULE_KEY]=importer;
		importer.importFromXML=function(hooksParameters,ePlane){
			// Default scrolling mode is «heuroptic»:
			var scrollingMode=HEUROPTIC_MODE;

			var aScrollingMode=ePlane.attributes[SCROLL_HOOK_PARAM];
			if(aScrollingMode && aScrollingMode.value)	scrollingMode=aScrollingMode.value;

			// If we have «heuroptic» as a configured value, then we put the default as the scroll mode, even if we have forced vertical mode :
			if(scrollingMode===VERTICAL_SCROLLING_MODE){
  			// Here we check if we have any scrolling as a start :
  			if (!empty(FORCED_SCROLL_MODE_WHEN_MOBILE) && isDeviceMobile()){
  				scrollingMode=FORCED_SCROLL_MODE_WHEN_MOBILE;
  			}
  		}
			hooksParameters[SCROLL_HOOK_PARAM]=scrollingMode;
			
			// Scroll options :
			if(scrollingMode===VERTICAL_SCROLLING_MODE){
				var scrollingModeOptions="";
				var aScrollingModeOptions=ePlane.attributes[SCROLL_OPTIONS_HOOK_PARAM];
				if(aScrollingModeOptions && aScrollingModeOptions.value)	scrollingModeOptions=aScrollingModeOptions.value;
				hooksParameters[SCROLL_OPTIONS_HOOK_PARAM]=scrollingModeOptions;
			}
			
		};
		
		var exporter=new Object();
		AotraScreenExporterToXML.planeExporters[MODULE_KEY]=exporter;
		exporter.getXML=function(plane){
			
			if(!nothing(plane.hooksParameters)){
				var result="";

				// If we are at this point to export in XML, then it implies we *are* in edition mode !
				
				if(!PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(plane)
						|| PlaneVerticalScrollingBubblesPresentingProvider.isSpecialMode(plane)){
					result+=" "+SCROLL_HOOK_PARAM+"=\""+VERTICAL_SCROLLING_MODE+"\"";
				}else{
					result+=" "+SCROLL_HOOK_PARAM+"=\""+(plane.hooksParameters[SCROLL_HOOK_PARAM])+"\"";
				}
				
				if(!nothing(plane.hooksParameters[SCROLL_OPTIONS_HOOK_PARAM],true))
					result+=" "+SCROLL_OPTIONS_HOOK_PARAM+"=\""+(plane.hooksParameters[SCROLL_OPTIONS_HOOK_PARAM])+"\"";
				
				return result;
			}
			return "";
		};
		
		
		
		// Use hooks for screen initialization
		var screenIniter=new Object();
		AotraScreen.hooks[INIT_AFTER_READY][MODULE_KEY]=screenIniter;
		screenIniter.initScreen=function(aotraScreen){
			var plane=aotraScreen.getActivePlane();

						
			// Scrolling UI elements are always displayed :
			// UI elements adding :
			var selectScrollerUIs=document.getElementById("selectScrollingMode");
			var verticalScrollOption=document.createElement("option");
			verticalScrollOption.id="optionScroll"+VERTICAL_SCROLLING_MODE;
			verticalScrollOption.innerHTML=VERTICAL_SCROLLING_MODE;
			verticalScrollOption.value=VERTICAL_SCROLLING_MODE;
			
			verticalScrollOption.onChangeOption=plane.verticalScrollingProvider.onChangeScrollMode;

			selectScrollerUIs.appendChild(verticalScrollOption);
			
			if(plane.hooksParameters[SCROLL_HOOK_PARAM]===VERTICAL_SCROLLING_MODE)
				verticalScrollOption.selected="selected";
			
			selectScrollerUIs.appendChild(verticalScrollOption);
			

			// This mode overrides all other behaviors :
			// We force the default (ie. heuroptic, ie. non-vertical scrolling) mode at startup
			// if we are editable and if we are in this overriding configuration :


			var selectScrollerUIs=document.getElementById("selectScrollingMode");
			if(PlaneVerticalScrollingBubblesPresentingProvider.isSpecialMode(plane)){
				
				var isAotraScreenInEditionMode=plane.getAotraScreen().isCalculatedEditable();
				if(isAotraScreenInEditionMode){
					
					delete selectScrollerUIs.disabled;
					
					PlaneVerticalScrollingBubblesPresentingProvider.setScrollMode(plane,HEUROPTIC_MODE,true);
					
					return;
				}else{
					
					// We deactivate the user choice :
					// (Only in non-editable mode, in edition mode, modes switch remains active !)
					selectScrollerUIs.disabled="disabled";
					
					PlaneVerticalScrollingBubblesPresentingProvider.setScrollMode(plane,VERTICAL_SCROLLING_MODE,true);

					// We close all bubbles except home on startup in this mode :
					var bubbles=plane.getBubbles();
					for(var i=0;i<bubbles.length;i++){
						var b=bubbles[i];
						if(b.getIsHomeBubble()){
							continue;
						}
						// This is a special case, here, in default heuroptic mode, this method MUST NEVER be used directly :
						b.setOpen(false);
						b.placeDiv();
					}
					
				}

			}
			
			// Plane configuration reading:
			if(PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(plane)){
				PlaneVerticalScrollingBubblesPresentingProvider.setScrollMode(plane,HEUROPTIC_MODE,true);
				return;
			}

			// Screen behavior overridings :
			
			var mainDiv=aotraScreen.mainDiv;
			// To add a vertical scroll bar, in case it is needed :
			mainDiv.setStyle("overflow-y:scroll;");

			
			// (zoom provider is deactivated)
			if(aotraScreen.zoomingProvider)	aotraScreen.zoomingProvider.removeUI();
			delete aotraScreen.zoomingProvider;
			
			if(aotraScreen.miniMapProvider)	aotraScreen.miniMapProvider.removeUI();
			delete aotraScreen.miniMapProvider;
			
		};
		
		// Use hooks for plane initialization
		var planeIniter=new Object();
		Plane.hooks[INIT_AFTER_READY][MODULE_KEY]=planeIniter;
		planeIniter.initPlane=function(plane){
		
			if(  !nothing(plane.hooksParameters)
				&& !nothing(plane.hooksParameters[SCROLL_HOOK_PARAM])
				&& contains(plane.hooksParameters[SCROLL_HOOK_PARAM],VERTICAL_SCROLLING_MODE)){

				var options=plane.hooksParameters[SCROLL_OPTIONS_HOOK_PARAM];			
				var scrollPresentationMode=VERTICAL_MARGINS;// the default mode
				if(!nothing(options) && contains(options,HORIZONTAL_MARGINS)){
					scrollPresentationMode=HORIZONTAL_MARGINS;
				}
			}

			// A vertical scroll provider is always instantiated, to allow to switch back to it :
			plane.verticalScrollingProvider=new PlaneVerticalScrollingBubblesPresentingProvider(plane,scrollPresentationMode);
			
			
		};
		
		// Use hooks for bubble initialization
		var bubbleIniter=new Object();
		Bubble.hooks[INIT_AFTER_READY][MODULE_KEY]=bubbleIniter;
		bubbleIniter.initBubble=function(bubble,aotraScreen,isScreenMobile,isScreenEditable){
			var plane=aotraScreen.getActivePlane();
			
			if(PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(plane))
				return;

			// Bubble behavior overridings :
			// Position displaying : is different
			// Size displaying : is different
			// NO : (vertical scrollable bubble is still more openable / closable)
			// NO :	 delete bubble.openCloseProvider;
			// (vertical scrollable bubble does not display links nor links navigationButtons)
			
		};
	}
	

	
	

	return initSelf;
}
}

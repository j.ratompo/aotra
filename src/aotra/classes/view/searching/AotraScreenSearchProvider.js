if(typeof initScreenSearchProviderClass ==="undefined"){
function initScreenSearchProviderClass() {
	var initSelf = this;

	// CONSTANTS
	var EXTRACT_LENGTH=200;
	var MINIMAL_CHARACTERS_NUMBER=1;
	var SELECTION_BG_COLOR="#AAF";
	var SELECTION_TEXT_COLOR="#FFF";
	var UNSELECTED_DEFAULT_BG_COLOR="#FFF";
	var UNSELECTED_TEXT_COLOR="#888";
	var IS_CLEAR_ACCENTUATED_CHARACTERS=true;
	
	initSelf.AotraScreenSearchProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {

			// Attributes
			this.aotraScreen = aotraScreen;
		
			// Initialization
			this.initUI();
		}
	
		// Methods
		,initUI:function(){
			
			var mainDiv=this.aotraScreen.mainDiv;

			var mainDivInnerHTML = mainDiv.innerHTML;
			mainDivInnerHTML+=this.getToolsBarHTMLInputs();
			
			mainDiv.innerHTML=mainDivInnerHTML;
		}

		/*private*/,getToolsBarHTMLInputs:function(){
			var mainDivInnerHTML="";		

			var POSITION_X=52;
			var POSITION_Y=0;
			var SEARCHBOX_WIDTH=250;
			
			mainDivInnerHTML+="<div id='divButtonToggleSearch' class='buttonsHolder' "
				+" style='left:"+(POSITION_X)+"px; top:"+(POSITION_Y)+"px ;z-index:"+(MAX_Z_INDEX-1)+"; '>";
			mainDivInnerHTML+="<button id='buttonToggleSearch' "
				+" class='icon searchBtn buttonLight' "
				+" onclick='javascript:getAotraScreen().searchProvider.executeToggleJQuery(this);' "
				+" data-target='divWholeSearch' "
				+" title='"+i18n({"fr":"Rechercher...", "en":"Search...."})+"'></button>";//&#8981;
			mainDivInnerHTML+="</div>";

			
			mainDivInnerHTML+="<div id='divWholeSearch' class='buttonsHolder' "
			+" style='left:"+(POSITION_X+40)+"px; top:"+(POSITION_Y)+"px ;z-index:"+(MAX_Z_INDEX-1)+"; "
			+" max-height:100%;overflow:hide;"
			+" pointers-events:none;"
			+" display:none;"
			+" '>";
			
			mainDivInnerHTML+=
				"<input id='textboxSearchBubble' type='text' "
				+" placeHolder='"+i18n({"fr":"Veuillez taper ici...","en":"Please type here..."})+"' "
				+" onkeyup=\"javascript:getAotraScreen().searchProvider.searchForBubble(this.value);\" "
				+" style='width:"+(SEARCHBOX_WIDTH)+"x;z-index:inherit; ' />";

			mainDivInnerHTML+="<input type='button' "
					+" title='Clear search' "
					+" style='z-index:inherit; ' "
					+" onclick='javascript:getAotraScreen().searchProvider.clearSearch();' value='X'/>"; 
			
			mainDivInnerHTML+="<style>.searchDivStyleClass:hover{cursor:pointer;background-color:"+SELECTION_BG_COLOR+"!important;color:"+SELECTION_TEXT_COLOR+"}</style>";
			mainDivInnerHTML+="<style>.searchDivStyleClass{background-color:"+UNSELECTED_DEFAULT_BG_COLOR+";color:"+UNSELECTED_TEXT_COLOR+"}</style>";
			
			mainDivInnerHTML+="<div id='foundBubblesList' "
				+"style='position:relative; left:"+(POSITION_X-70)+"px; top:"+(POSITION_Y)+"px ;z-index:inherit;"
				// containing div max-height must be a multiple of a cell height :
				+" width:500px;max-height:320px;overflow:auto;'></div> "; 

			mainDivInnerHTML+="</div>";
			
			return mainDivInnerHTML;
		}
		
		/*private*/,executeToggleJQuery:function(trigger){
			
			// Whole search div show/hide animations :
					
			var triggerSlct=jQuery(trigger);
			var targetId=triggerSlct.attr("data-target");
			
			if(!targetId){
				//TRACE
				log("ERROR : Missing target for trigger «"+triggerSlct.get()[0]+"» element");
				return;
			}
			
			jQuery("#"+targetId).slideToggle(500);
				
			document.getElementById("textboxSearchBubble").focus();
			
		}
		
		
		/*private*/,searchForBubble:function(searchStringParam){
			
			var searchString=searchStringParam;
			var aotraScreen=this.aotraScreen;
			var foundBubblesList=document.getElementById("foundBubblesList");

			if(searchString.length<=MINIMAL_CHARACTERS_NUMBER){
				foundBubblesList.innerHTML="";
				return;
			}
			
			var isClearAccentuatedCharacters=IS_CLEAR_ACCENTUATED_CHARACTERS;
			var bubblesList=aotraScreen.getBubblesInActivePlaneByContentContaining(searchString,isClearAccentuatedCharacters);
			if(isClearAccentuatedCharacters)
				searchString=clearAccentuatedCharacters(searchString);

			
			
			var foundHTML="";
			for(var i=0;i<bubblesList.length;i++){
				var b = bubblesList[i];
				
				// We exclude bubbles that are never visible (if their attribute is set so), wherever they are placed :
				if(b.isVisible()===false)	continue;
				// We exclude bubbles that are fixed :
				if(b.isFixed()===true)	continue;
				// We exclude bubbles that are not navigable :
				if(b.isNavigable()===false)	continue;
				
				var foundBubbleName = b.getName();
				
				var content=b.getContent();
				if(isClearAccentuatedCharacters)
					content=clearAccentuatedCharacters(content);
					
				foundHTML += "<div class='searchDivStyleClass' "
				// cell height must be a factor of containing div max-height :
				+" style='background-color:"+b.color+";height:80px;' "
				+" onclick=getAotraScreen().searchProvider.doGotoBubble('"+foundBubbleName+"') >"
				+getTextExtract(
						Math.round(EXTRACT_LENGTH/2),Math.round(EXTRACT_LENGTH/2)
						,removeHTML(content),searchString)
				+"</div>";
			}
			foundBubblesList.innerHTML=foundHTML;
			
		}
		
		/*private*/,doGotoBubble:function(selectedBubbleName){
			var aotraScreen=this.aotraScreen;
			
			var bubble=aotraScreen.getBubbleByName(selectedBubbleName);
			if(bubble){
				aotraScreen.doGotoBubble(bubble,true);
			}
			
			this.clearSearch();
			this.executeToggleJQuery(document.getElementById("buttonToggleSearch"));
		}		

		/*private*/,clearSearch:function(){
			document.getElementById("textboxSearchBubble").value="";
			document.getElementById("foundBubblesList").innerHTML="";
		}
	
		
	});

	return initSelf;
}
}
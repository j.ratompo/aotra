// DEPENDENCY : PLANE MAP PROVIDER


// GLOBAL CONSTANTS
	var AND_JUNCTION_LOGIC="and"; // Default value
	var OR_JUNCTION_LOGIC="or";


if(typeof initPlaneBubblesFilteringProviderClass ==="undefined"){
	
function initPlaneBubblesFilteringProviderClass() {
	var initSelf = this;

	// CONSTANTS
	var SEPARATOR=",";
	var COLOR_FOR_AND_LOGIC="rgba(126,251,160,0.9);";
	var COLOR_FOR_OR_LOGIC="rgba(126,160,251,0.9);";
	
	
	initSelf.PlaneBubblesFilteringProvider = Class.create({

		// Constructor
		initialize : function(plane,visibilityLayersConfig,junctionLogic) {

			// Attributes
			this.plane = plane;
			this.junctionLogic=junctionLogic?junctionLogic:AND_JUNCTION_LOGIC;

			// Initialization
			this.visibilities=new Object();// (an associative array)
			this.iconsURLs=parseJSON(visibilityLayersConfig);// (an associative array)

		}
	
		// Methods
		/*public*/,initVisibilityLayersProviderUI:function(aotraScreen){
			var mainDiv=aotraScreen.mainDiv;
			
			var MAX_WIDTH=1000;
			var POSITION_X=316;
			var POSITION_Y=40;
			
			var div=document.getElementById("divPlaneVisibilityLayers");
			var divButtonShowFilters=document.getElementById("divButtonShowPlaneVisibilityLayers");
			if(!div){
				
				if(!divButtonShowFilters){
					
					divButtonShowFilters=document.createElement("div");
					divButtonShowFilters.id="divButtonShowPlaneVisibilityLayers";
					divButtonShowFilters.className="buttonsHolder";
					divButtonShowFilters.setStyle("left:"+(POSITION_X)+"px; top:"+(POSITION_Y)+"px ;z-index:"+(MAX_Z_INDEX-1)+"; pointer-events:all; ");
					
					var buttonShowFilters=document.createElement("button");
					buttonShowFilters.id="buttonShowPlaneVisibilityLayers";
					buttonShowFilters.className="buttonLight";
					buttonShowFilters.setAttribute("data-target","divPlaneVisibilityLayers");
					buttonShowFilters.setStyle("pointer-events:all; ");
					buttonShowFilters.innerHTML=i18n({"fr":"Filtrer...","en":"Filter..."});
	//				buttonShowFilters.onclick=this.executeToggleJQuery(buttonShowFilters);
					divButtonShowFilters.appendChild(buttonShowFilters);

					mainDiv.appendChild(divButtonShowFilters);
					this.executeToggleJQuery(buttonShowFilters);
					
				}

				
				div=document.createElement("div");
				div.id="divPlaneVisibilityLayers";
				div.setStyle("display:none; "
						+" background:"+(this.junctionLogic===AND_JUNCTION_LOGIC?COLOR_FOR_AND_LOGIC:COLOR_FOR_OR_LOGIC)+";"
						+" maxWidth:"+(MAX_WIDTH)+"px; "
						+" position : absolute ; left:"+(POSITION_X+68)+"px;top:"+(POSITION_Y)+"px; "
						+" z-index:"+(MAX_Z_INDEX-1)+"; ");
				div.className="buttonsHolder filtersWindow";

				mainDiv.appendChild(div);
			}
			
			var divInnerHTML="";
			
			// We initialize visibilities :
			this.visibilities=this.getVisibilityLayersKeys(); // (an associative array)
			var visible=false;
			for(var k in this.visibilities){
				if(!this.visibilities.hasOwnProperty(k))	continue;
				divInnerHTML+=this.getInputElementStringForVisibilityLayerKey(k);
				// There's no way I know to get an associative array length and so know its emptiness... :-/
				visible=true;
			}
			div.innerHTML=divInnerHTML;
			
			// We hide the visibility layers panel if we don't find any visibility layer :
			setElementVisible(divButtonShowFilters,visible);
			
			
		}
		
		/*private*/,executeToggleJQuery:function(trigger){
			
			// Whole search div show/hide animations :
			var triggerSlct=jQuery(trigger);
			
			triggerSlct.click(function(){
				
				var targetId=triggerSlct.attr("data-target");
				
				if(!targetId){
					//TRACE
					log("ERROR : Missing target for trigger «"+triggerSlct.get()[0]+"» element");
					return;
				}
				
				var collapsibleContainer=jQuery("#"+targetId);
				collapsibleContainer.slideToggle(500);
			
			});
		
		}
		
	
		/*private*/,getVisibilityLayersKeys:function(){

			var list=new Object();
			
			var bubbles=this.plane.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				var bubble=bubbles[i];
				var visibilityLayersStr=bubble.visibilityLayers;
				if(nothing(visibilityLayersStr))	continue;
				
				var keys=bubble.visibilityLayers.split(SEPARATOR);
				for(var j=0;j<keys.length;j++){
					
					var key=this.simplifyKey(keys[j]);
					
					if(nothing(key))	continue;
					var k=this.simplifyKey(key);
					if(!list[k]){
						list[k]=true;
					}
				}
			}
			
			
			return list;
		}
		
		/*private*/,getInputElementStringForVisibilityLayerKey:function(visibilityLayerKeyParam){
			
			var visibilityLayerKeyParamId=visibilityLayerKeyParam.replace(" ","_");
			
			var result="<span style='z-index:inherit;display:inline-block;' class='visibilityLayerCheckbox' >";
			result+="<input type='checkbox' style='pointer-events:all;' "
					+" onclick=\"javascript:if(getAotraScreen().getActivePlane().visibilityLayersProvider) getAotraScreen().getActivePlane().visibilityLayersProvider.applyVisibility('"+visibilityLayerKeyParam+"',this.checked)\" " 
					+" "+(this.visibilities[visibilityLayerKeyParam]?"checked":"")
					+" id='"+visibilityLayerKeyParamId+"' />";
			result+="<label for='"+visibilityLayerKeyParamId+"' class='visibilityLayerLabel' >";
			
			// Eventual icon :
			if(this.iconsURLs){
				var iconURL =this.iconsURLs[visibilityLayerKeyParam];
				if(iconURL){
					result+="<img src='"+iconURL+"' ";
					result+=" width='32' height='32' style='display:inline'/>";
				}
			}
			result+=visibilityLayerKeyParam;
			result+="</label>";
			result+="</span>";
			result+="<br />";
			
			return result;
		}

		/*private*/,applyVisibility:function(visibilityLayerKeyParam,setToVisible){
			
			var simplifiedKey=this.simplifyKey(visibilityLayerKeyParam);
			this.visibilities[simplifiedKey]=setToVisible;
			
			var bubbles=this.plane.getBubbles();
			for(var i=0;i<bubbles.length;i++){
				
				var bubble=bubbles[i];
				
				var visibilityLayersStr=bubble.visibilityLayers;
				// Bubbles with nothing specified are always displayed :
				if(nothing(visibilityLayersStr))	continue;
				
				
				var split=bubble.visibilityLayers.split(SEPARATOR);
				
				// OR junction logic:
				if(this.junctionLogic===OR_JUNCTION_LOGIC){
					
					// If only one visibility layer is activated for a bubble,
					// then the bubble is showed.
					// If no visibility layer is activated for bubble,
					// then bubble is hidden.
					// (bVisibility = l1Visible OR l2Visible OR l3Visible OR ...) :
					
					if(setToVisible){
						
						// If a layer has been made visible,
						// and we see that this bubble is concerned by this layer made visible :
						// We show the bubble
						var isConcerned=contains(bubble.visibilityLayers,simplifiedKey);
						if(isConcerned){
							// DEPENDENCY : PLANE MAP PROVIDER
							this.setBubbleAndMarkerVisibility(bubble,true);
	
							// DEPENDENCY : PLANE MAP PROVIDER
							this.updateMarkerIcon(simplifiedKey,bubble);
						}
	
					}else{
	
						// If a layer has been made invisible,
						// and we see that the bubble is so no more visible :
						// We hide the bubble

						var isDisplayableAnyway=false;
						for(var j=0;j<split.length;j++){
							var visibility=this.visibilities[this.simplifyKey(split[j])];
							if(visibility===true){
								isDisplayableAnyway=true;
								break;
							}
						}
						if(!isDisplayableAnyway)
							// DEPENDENCY : PLANE MAP PROVIDER
							this.setBubbleAndMarkerVisibility(bubble,false);
	
						// DEPENDENCY : PLANE MAP PROVIDER
						this.updateMarkerIcon(simplifiedKey,bubble);
					}
					
				}else{
					// AND junction logic:

					// If a layer has been made visible,
					// and we see that this bubble is concerned by this layer made visible :
					// We show the bubble
					
					if(setToVisible){

						var isMadeDisplayable=true;
						for(var j=0;j<split.length;j++){
							var visibility=this.visibilities[this.simplifyKey(split[j])];
							if(visibility===false){
								isMadeDisplayable=false;
								break;
							}
						}
						if(isMadeDisplayable)
							// DEPENDENCY : PLANE MAP PROVIDER
							this.setBubbleAndMarkerVisibility(bubble,true);
	
						// DEPENDENCY : PLANE MAP PROVIDER
						this.updateMarkerIcon(simplifiedKey,bubble);
						
	
					}else{
	
						// If a layer has been made invisible,
						// and we see that the bubble is so no more visible :
						// We hide the bubble
						
						var isConcerned=contains(bubble.visibilityLayers,simplifiedKey);
						if(isConcerned){
							// DEPENDENCY : PLANE MAP PROVIDER
							this.setBubbleAndMarkerVisibility(bubble,false);
	
							// DEPENDENCY : PLANE MAP PROVIDER
							this.updateMarkerIcon(simplifiedKey,bubble);
						}
						
					}
					
				}
				

			}

		}


		// Utility methods :
		,setBubbleAndMarkerVisibility:function(bubble,setToVisible){
			
			bubble.setIsVisible(setToVisible);
		
			// DEPENDENCY : PLANE MAP PROVIDER
			var mp=this.plane.mapProvider;
			if(mp){
				var m=mp.getMarkers()[bubble.name];
				if(m)	m.setIsVisible(setToVisible);
			}
			
		}
		
		,updateMarkerIcon:function(simplifiedKey,bubble){
		
			// DEPENDENCY : PLANE MAP PROVIDER
			var mp=this.plane.mapProvider;
			if(mp){
				var m=mp.getMarkers()[bubble.name];
				if(m){
					
					// If bubble marker icon is set to «auto»
					var dataIcon=jQuery(bubble.div)
								 .find(" "+MAP_MARKER_TAG_NAME)
								 .find(">*."+MARKER_CLASS_NAME)
								 .attr("data-icon");
					
					var isAuto=(dataIcon==="auto");
					
					if(isAuto){
						
						// OLD :
//						var bubbleVisibilitiesAtOn=new Array();
//						var split=bubble.visibilityLayers.split(SEPARATOR);
//						for(var i=0;i<split.length;i++){
//							var key=this.simplifyKey(split[i]);
//							if(this.visibilities[key]===true)
//								bubbleVisibilitiesAtOn.push(key);
//						}

						
						var iconURL=this.getLastVisibleMarkerIcon(bubble);

						// If we can change icon, we will try :
						if(iconURL){
							// We change icon :
							if(iconURL)	m.setIcon(iconURL);
							
						}					
					}

				}
			}
		}
		
		/*public*/,getLastVisibleMarkerIcon:function(bubble){
			
			var bubbleVisibilitiesAtOn=this.getBubbleVisibilitiesAtOn(bubble);

			// If we can change icon, we will try :
			if(!empty(bubbleVisibilitiesAtOn)){
				var key=bubbleVisibilitiesAtOn[0];
				var ic=this.iconsURLs[key];
				return ic;
			}
			
			return null;
		}
		
		/*private*/,getBubbleVisibilitiesAtOn:function(bubble){
			var bubbleVisibilitiesAtOn=new Array();
			
			var split=bubble.visibilityLayers.split(SEPARATOR);
			
			for(var i=0;i<split.length;i++){
				var key=this.simplifyKey(split[i]);
				if(this.visibilities[key]===true)
					bubbleVisibilitiesAtOn.push(key);
			}
			
			return bubbleVisibilitiesAtOn;
		}
		
		,simplifyKey:function(keyParam){
			return toOneSimplifiedLine(keyParam.trim().toLowerCase());
		}
		

	});


	
	// static methods :
	

	
	
	return initSelf;
}
}
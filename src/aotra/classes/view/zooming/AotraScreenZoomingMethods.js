

if(typeof initScreenZoomingMethodsClass ==="undefined"){
function initScreenZoomingMethodsClass() {
	var initSelf = this;
	
	// CONSTANTS
	var REFRESHING_RATE = .01;
	
	var INSTANTLY="instantly";
	var LINEAR="linear";
	
	
	initSelf.AotraScreenZoomingMethods = Class.create({

		// Constructor
		initialize : function(aotraScreen,zoomingAnimation) {
			
			// Attributes
			this.aotraScreen=aotraScreen;
			
			// Default zoom method is always "instantly"
			this.zoomTo=this.zoomToInstantly;			
			this.zoomToFitBubble=this.zoomToFitBubbleInstantly;
			
			// TODO :
//			if(zoomingAnimation=="linear"){
//				this.zoomTo=this.zoomToLinearly;
//				this.zoomToFitBubble=this.zoomToFitBubbleLinearly;
//			}

		}

		// Methods
	
	// TODO : Develop...:
//		,zoomToFitBubbleLinearly:function(destinationX,destinationY){
//			var aotraScreen=this;
//			var SPEED_FACTOR=0.05;
//
//			var sourceX=aotraScreen.getWidth()/2;
//			var sourceY=aotraScreen.getHeight()/2;
//			
//			var deltaX=destinationX-sourceX;
//			var deltaY=destinationY-sourceY;
//
//			var distanceX=Math.abs(deltaX);
//			var distanceY=Math.abs(deltaY);
//			
//			var pe=new PeriodicalExecuter(function() {
//				
//				var xStep = Math.abs(deltaX * SPEED_FACTOR);
//				var yStep = Math.abs(deltaY * SPEED_FACTOR);
//
//				if (distanceX <= 0 && distanceY <= 0) {
//					pe.stop();
//					return;
//				} else {
//					if (deltaX > 0 && deltaY > 0) {
//						aotraScreen.setScreenLocation(sourceX + xStep, sourceY + yStep);
//					} else if (deltaX <= 0 && deltaY > 0) {
//						aotraScreen.setScreenLocation(sourceX - xStep, sourceY + yStep);
//					} else if (deltaX <= 0 && deltaY <= 0) {
//						aotraScreen.setScreenLocation(sourceX - xStep, sourceY - yStep);
//					} else if (deltaX > 0 && deltaY <= 0) {
//						aotraScreen.setScreenLocation(sourceX + xStep, sourceY - yStep);
//					} else {
//						pe.stop();
//					}
//					distanceX -= xStep;
//					distanceY -= yStep;
//				}
//					
//				
//				
//			}, REFRESHING_RATE);
//		}
		
		// - instant :
		,zoomToFitBubbleInstantly:function(bubble){
			var aotraScreen=this;
			
			var MARGIN_PERCENT=.1;// To avoid the bubble touch the sides...
			
			var aotraScreenWidth=aotraScreen.getWidth();
			var aotraScreenHeight=aotraScreen.getHeight();
			
			var maxW=bubble.maxW;
			var maxH=bubble.maxH;

			
			var targetZoomFactorToFitBubble=0;
			if(aotraScreenWidth<aotraScreenHeight)	targetZoomFactorToFitBubble=(aotraScreenWidth/maxW);
			else																		targetZoomFactorToFitBubble=(aotraScreenHeight/maxH);
			
			aotraScreen.zoomTo(targetZoomFactorToFitBubble*(1-MARGIN_PERCENT));
			aotraScreen.draw();
			
		}
		
		
		
		,zoomToInstantly:function(destinationZoom){
			var aotraScreen=this;
			aotraScreen.setZoom(destinationZoom);
		}
		
		
		
		// - linear :
		,zoomToFitBubbleLinearly:function(bubble){
			var aotraScreen=this;
						
			// TODO : ...
			
		}
		
		,zoomToLinearly:function(destinationZoom){
			var aotraScreen=this;
			
			var SPEED_FACTOR = 0.005;

			var sourceZoom = aotraScreen.zoom;
			var deltaZoom = destinationZoom - sourceZoom;
			var sign = deltaZoom==0?1: deltaZoom/Math.abs(deltaZoom);
			
			var step = Math.abs(deltaZoom * SPEED_FACTOR);
			var distance = Math.abs(deltaZoom);
			
			var pe = new PeriodicalExecuter(function() {
				
				if (distance<=0) {
					pe.stop();
					return;
				}
					
				distance-=step;
				aotraScreen.setZoom(aotraScreen.getZoomFactor()+step*sign);

			}, REFRESHING_RATE);

		}

	});

	return initSelf;
}
}
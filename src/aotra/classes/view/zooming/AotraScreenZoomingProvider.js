//	GLOBAL CONSTANTS :
//	var ZOOM_STEP=1000;
//	var ZOOM_EXPANSION=20000;

//	var ZOOM_STEP=10000000;
//	var ZOOM_EXPANSION=1000000000;

	//Zoom step corresponds to ZOOM_STEP/(ZOOM_EXPANSION/2)*100 zoom percentage 
//	var ZOOM_STEP=10; // <-- here, 10% actual zoom factor !
//	var ZOOM_EXPANSION=180;
	var ZOOM_STEP=0.2;
	
if(typeof initScreenZoomingProviderClass ==="undefined"){
function initScreenZoomingProviderClass() {
	var initSelf = this;

	// CONSTANTS
//	var BUTTONS_WIDTH="60px";
//	var BUTTONS_HEIGHT="50px";

	initSelf.AotraScreenZoomingProvider = Class.create({

		// Constructor
		initialize : function(aotraScreen) {

			// Attributes
			this.aotraScreen = aotraScreen;

			this.initWheel();
			
			this.initUI();
			
		}
	
	
		// Methods
	
		/*private*/,initWheel:function(){
			
			// Wheel events management :
			if(IS_DEVICE_MOBILE_CALCULATED)	return;

			var self=this;
			initMouseWheel(function(delta){
				if(delta<0) self.zoomOut(ZOOM_STEP);
				else 		self.zoomIn(ZOOM_STEP);
			},function(div){

				// We restrain these wheel events effects to mainDiv only :
				// (for this reason, among others, canvas div must have a weaker z-index than main div)
				if(div.id!==MAIN_DIV_ID)	return false;
				return true;
				
			});
			
		}
	
		,initUI:function(){
			
			var mainDiv=this.aotraScreen.mainDiv;

			var mainDivInnerHTML = mainDiv.innerHTML;
			
			// We let these buttons to all devices :
			var POSITION_X=10;
			var POSITION_Y=88;
			
			mainDivInnerHTML+="<div id='divZoomButtons' class='buttonsHolder' "
				+" style='left:"+POSITION_X+"px;top:"+POSITION_Y+"px;z-index:"+(MAX_Z_INDEX-1)+";' >";

			
			mainDivInnerHTML+="<button id='zoomInButton' onclick='javascript:getAotraScreen().zoomingProvider.zoomIn("+ZOOM_STEP+");' "
//				+" width:"+BUTTONS_WIDTH+"; height:"+BUTTONS_HEIGHT+"; "
				+" ' class='icon zoomInBtn'></button>";
				
					
			mainDivInnerHTML+="<button id='zoomOutButton' onclick='javascript:getAotraScreen().zoomingProvider.zoomOut("+ZOOM_STEP+");' "
				+" style='position:absolute;top:"+(GLOBAL_ICONS_SIZE)+"px;left:0px;"
//				+" width:"+BUTTONS_WIDTH+"; height:"+BUTTONS_HEIGHT+"; "
				+" ' class='icon zoomOutBtn'></button>";
				
//			}
			
			mainDivInnerHTML+="</div>"

			
			mainDiv.innerHTML=mainDivInnerHTML;
			
		}
		
		/*public*/,removeUI:function(){
			document.getElementById("divZoomButtons").style.display="none";
		}


		,zoomIn:function(zoomFactorPercentage){
			this.zoom("in",zoomFactorPercentage);
		}
		,zoomOut:function(zoomFactorPercentage){
			this.zoom("out",zoomFactorPercentage);
		}
		
		,zoom:function(zoomType,zoomStep){
			var aotraScreen=this.aotraScreen;
			
			// LINEAR MODE :
//			var newZoom=aotraScreen.getZoomFactor();
//			if(zoomType==="out"){
//				newZoom-=zoomStep;
//			}else if(zoomType==="in"){
//				newZoom+=zoomStep;
//			}
//			if(newZoom <= 0)			newZoom=zoomStep;
//			if(ZOOM_EXPANSION<=newZoom)	newZoom=ZOOM_EXPANSION-zoomStep;
//			this.aotraScreen.zoomTo(newZoom);

			var newZoom=1;
			if(zoomType==="out"){
				newZoom-=zoomStep;
			}else if(zoomType==="in"){
				newZoom+=zoomStep;
			}
			this.aotraScreen.zoomTo(aotraScreen.getZoomFactor()*newZoom);

		}

		
	
	});

	return initSelf;
}
}
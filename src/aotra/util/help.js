

function initHelp(maxZIndexHoverLay){
	
	var LABELS=new Object();
	LABELS["incompleteActionMessage"]=
		i18n({"fr":"Vous devez compléter cette étape avant de continuer. Veuillez recommencer."
		  ,"en":"You have to complete previous step first. Please start this step over again."});
	LABELS["closeLabel"]=i18n({"fr":"Terminé","en":"Finished"});
	
	jQuery(".help")// See help button in EditionProvider class...
	.click(function(){
		
		
		promptWindow(i18n({"fr":"Veuillez choisir un tutorial à vous montrer...","en":"Please choose tutorial to show..."}),"uniqueChoice",
		[i18n({"fr":"Comment créer une bulle?","en":"How to create a bubble?"}),
		 i18n({"fr":"Comment déplacer une bulle?","en":"How to move a Bubble?"}),
		 i18n({"fr":"À propos d’aotra.","en":"About aotra CMS."})
		 ]

		,function(selectedValue){
			
			if(nothing(selectedValue))	return;
			
			if(selectedValue==="0"){
				
				executeTutorialMacro(
					["designate:#buttonCreateBubble",
				     "advice:#buttonCreateBubble;"
					 +i18n({"fr":"Premièrement, cliquez ici.","en":"First, click here."}),
				     "designate:#mainDiv",
				     "advice:#mainDiv;"
				     +i18n({"fr":"Ensuite, cliquez n’importe où sur l'écran.","en":"Then click anywhere on the screen."})
				     ],maxZIndexHoverLay,LABELS);

			}else if(selectedValue==="1"){
				
				executeTutorialMacro(
						["designate:#mainDiv",
					     "advice:#mainDiv;"
						 +i18n({"fr":"D’abord, sélectionnez une bulle.","en":"First, select a bubble."}),
					     "designate:#buttonMoveBubble",
					     "advice:#buttonMoveBubble;"
					     +i18n({"fr":"Puis, cliquez ici.","en":"Then, click here."})
					     ],maxZIndexHoverLay,LABELS);

			}else if(selectedValue==="2"){
				window.open("https://alqemia.com#blog");
			}
			
		});
		
	});
	
}

/*
 // TODO : Create all these tutorials...
 
		Comment insérer un carousel dans une bulle ?

		Veuillez sélectionner une bulle.
		Ensuite cliquez sur le bouton d’insertion de carousel («+C»).
		Personnalisez la liste ajoutée.
		Vous pouvez par exemple insérer un fichier en utilisant l’outil de téléversement.
		Cliquez enfin sur le bouton de sauvegarde et de fermeture pour sauvegarder et voir le résultat.



		Commet téléverser un fichier et l’intégrer dans une bulle ? :

		Veuillez sélectionner une bulle.
		Ensuite cliquez sur le bouton des options de téléversement.
		Cliquez sur le bouton Parcourir et choisissez un fichier sur votre ordinateur.
		Cliquez sur le bouton de téléversement et d’insertion puis entrez s’il y a lieu votre mot de passe en écriture.
		Le bouton de téléversement et d'incrustation fait la même chose, il permet d’intégrer le média à même la page, mais alourdit la page HTML.
		Cliquez enfin sur le bouton de sauvegarde et de fermeture pour sauvegarder et voir le résultat.



		Comment ajouter un marqueur de carte (ne fonctionne que s’il y a une carte en arrière-plan) ?

		Veuillez sélectionner une bulle.
		Ensuite cliquez sur le bouton d’insertion de marquer de carte («+M») puis entrez une adresse jusqu’à obtenir un emplacement.
		Cliquez enfin sur le bouton de sauvegarde et de fermeture pour sauvegarder et voir le résultat.



		Comment ajouter une balise de lien dans la bulle ?

		Veuillez sélectionner une bulle.
		Ensuite sélectionnez du texte dans votre bulle.
		Puis cliquez sur le bouton d’insertion de lien vers une bulle («+l»).
		Ensuite sélectionnez une autre bulle.
		Cliquez enfin sur le bouton de sauvegarde et de fermeture pour sauvegarder et voir le résultat.
		
*/
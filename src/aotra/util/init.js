// GLOBAL CONSTANTS :

	// GLOBAL CONSTANTS
	var MEAN_IE_MESSAGE_SHORT = "Your (prehistorical) browser doesn't support HTML5.";
	var MEAN_IE_MESSAGE_LONG = "Well...Here's the problem : on today, IE still doesn't want to comply with HTML5 standard. And this page needs HTML5 in order to work...So you can whether download and then use a browser that can render HTML5 correctly or bear the bugs that may occur in this page, directly caused or not caused by your browser.";

	// Global non user-configurable configuration : (prioritary config)
	var IS_BUBBLE_CONTENT_EDIT_THOUGHTFUL=false;
	var FORCED_SCROLL_MODE_WHEN_MOBILE="vertical";// At this point plane vertical scroll provider constants are not reachable.
	
	// Back-end file :
	var DEFAULT_BACK_END_FILE_PATH="aotra.php";
	
	
//	var BACKGROUND_CONTAINER_ID="divBackgroundContainer";
	var AOTRA_SCREEN_DIV_ID="aotraScreenDiv";
	var MAIN_CANVAS_ID="mainCanvas";
	var MAIN_DIV_ID="mainDiv";
	

	var MIN_Z_INDEX=0;// Chrome really doesn't like a negative z-index !
	var MAX_Z_INDEX=9999;
	var Z_INDEX_LOW_UI_ELEMENTS=MAX_Z_INDEX-999;
	var MAX_Z_INDEX_OVERLAY=99999;
	var Z_INDEX_INTERMEDIATE_UI_ELEMENTS=MAX_Z_INDEX_OVERLAY-999;
	var MIN_Z_INDEX_BUBBLES=MIN_Z_INDEX+999;
	var MAX_Z_INDEX_BUBBLES=MAX_Z_INDEX-999;// So that max bubbles z-index is always below max-z-index
	
	// empirical 0.8 ratio : 40px for 50px wide images (50 * 0.8 = 40) , so 27px for 33px wide images (33 * 0.8 = 27)
	// (The actual size of source image does absolutely not matter !)
	var GLOBAL_ICONS_SIZE=50*0.8;
	var ICON_BORDER_THICKNESS=1;

	// A global value, calculated just once :
	var IS_DEVICE_MOBILE_CALCULATED=isDeviceMobile();
	var XML_ERRORS_MESSAGE="There are parse errors in the XML code, please review it before leaving.";
	var CONFIRM_MESSAGE="Are you sure you want to leave this page ? All unsaved modifications will be lost.";
	
	// Icon data :A file name or a base64 encoded string (will be used in css url('...') function ) :
	// -Text editor icons (currently NicEdit)
	// ********************************************************************************************************************************
	// (cf. in file *_nicEditMOD.js)
	// ********************************************************************************************************************************
	// -All aotracodex icons : (you can use for example «b64.io/» website to encode your all icons image)
	// ********************************************************************************************************************************
	var IMAGE_SOURCE_ALLICONS="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4AEMAh0WpRoqCQAAIABJREFUeNrtXXeYVUXyPTUDExADihhQMKxpVxcVc45r+hlXXXNOq2t2zboGDChrzmLOERPimhOKCobFnBMSVfLMMOH8/ujT+5rLve/dF2YEufV97xt4795bt7u6T1dVV1cBGWWUUUZzCFm+H0nCzPy/1wOwHIAqAG0AvjCzN6LXlUNJz6nU8zP67SitDDNZZ1QOYK0C4DQA/wegRR9PnfR5CsBFZjay3AENYH4AzwNYGEA9gCkAXgZwm5kN7ajB7PmQXMrMvi2GL8kFAKxvZoOzSThL39wDYMU8Y/ErAPeY2WOV7DOSywFYSuN1GoDXzIyV4BGMFQOwIYBa/TTezN7P5N8Bk5VkNcmbSE4mOZrkmDyf0bruJt1XKs/5SP5AcuXIb1uSfJDkOyQX76D2g+RnatcnKe7ZmOQa+vdhJBtJ9g7+v3g2sgCSv5JcXzKN+5xKciLJ50nOU8pYCngtRXIAyfF65s8kJ+jvVJLDSe4VLJalLLAguSvJYXrmz8HnV/G72Y+FMtqyA8kDSe6hBTHTsAIhLADgFQCLlPC8sQA2BjCxCI0EAOYDMALAEDM7JknbAfAagCNC7SXPc/8JoI/M1zRUA+BiM3uf5LMAVlH/VAE40czuysPrTQArAdgRwCkA1gAwAEArgDMBnGlm17YzGKwIoNrMPpqNAWuKmc1b4JrJ0oKmAtgAwNhixpLGymkATgXQoJ9qJV/TeGgE0AygGsA4ALua2adFtmVxAE8A6C3Lo5Osgipd0gRghvjNA+AWMzu+SG19CwD36P2b9Ox6APea2WFzo+YWBaxqAO8C6FHIXEzqYw2A1c2stUiwWlAD56WEQej/jgRwnJm9kOe5i8i8mFrEuz9jZgeQvBjAgWoLNNjWNbMP8/j3DgRwsQZtiwZpZz2jBsCKZja6PcwDkocAuEj8oEF9vZmdMrsNaA9YJI8A0DNmjM0AcCJyftKxANYqcgEcAqCv7jcAl2sh/EC/G4C/ANhXro4GAF0AbG5mw1PyWAvAYMm6DsDrAAZqDDXpmlUBrK8FrCYweTcA0FaoPQKrx/T+LwF4MOKSed/Mtp7bNaybAOyQ59o6+ZU6SwiNCcD2hJkdVgRYdQVQZWaLkOwZCKbZzH6J3NMJwDcAVgYwKcFJvzSA97Uqvar3zEfNZnYgyS0BPByszCb/2akJfpEPNZjaAKye8OxqAIMArAmgu5ktWUEAGADggMhA9u89wsy2TamRLATg52hfklxS71+Iqszs6yIA6ykAfwq0kXDBW0TjqwXArwA+NbNNU/bH7QC21ju/BGBvM2vO0+4/SUvqos9aAD5LAhONvyUAvBN8vZeZvZRvcSB5NYC9pNW9bWY7pGjLGM231czsGy3UC4d9DuAEAHfPdf4x+WxWkc8myU91cmjrk1xNfp44H9dEPa+Qz+oL3T+W5Bkke9LRdJLTSL6QcO/GJB/K056lSU7SZ8mUfbBYxGc3WqZh0vU1JN+OtHsCyXdJ3i5/XPjbLyRPLlNO/1RfjSJ5hNqX5FucQHLnQv4ZkoNJfil5RH/7JejHfB+mfP8pKa65nuTjJD+SPCaQ3CkfC42JLeQ3GkvyxjS+Kd03L8mvJe9vUrzfUF07muQSRcjuHPnTxpM8vMC126rdoyWb59Wu6Jx8fq7VsEjeB2CTBI2pBcCqZjaB5AYA3tBq2AXAj9Jkoivly2a2ZwrNqh7AS2a2s/wCPwAYr2e8bWY7Jgj1UwB9zWxaHg0LWqG+jjPlIoP6cwDzqv2UObkcgLxmFcmftXJWAbjKzPoFvz0vTcLgdqZ2KwOsNgbwuLRFSotozWO6E8BwM9u+gNnxaOAz6gtgcmDq/pJSw5rPUizzgYa1okztWG1XGusveqcrAfQws+0KPPsT+V9/MLO1inSerwDgPfE71cxuTbhuBwC3StZ7mdkzRcrwEQDrydfYI891RwA4N3BLJFGbmc1VGzqdgn9vB2B6HrPmefmGaiWw7mY2jeTrMneiQLhdHrD6QOrufXIgDi9ygEG2/Z4kBxaYK51J/gfAOgAGmdkBM72oMw2e0nv5iV4Pt0WdFBfWBUA/ANtogkGDsF/kPU8F8Kx8MxvKOX+fmV1VTHv1Dn1kqlrgPLYCi9GC+fxmZvY8yf0B3K6vRgDoS9KD1qppTcIix90gJIc3eBovP9e/ALxdoI82lSlpAA4t0nfX1cw+E4heC+BkgRJixsmfATyjjYBnAv7zm9mkAmYhZML9F8AMkrsCeDjh+sZg4cxHk+c2DauTOnPdGD9IdPAvGjhGuwLoDmBM4OydRSsjua6ZvRkZ2O8BONnMHvKrvLQ2g4vDCn0O85JcLViNx5rZF7rvVQCnmNnAAm08VBOvAcC2JC8wszOCgXSBfBd+N7EOwEFm9iXJeaR9RGl+OW1r9bvJ3xId4FODyWxwO4/jAVxVBDgvQPJhLQoNRcp3OZLfAdheEyUOtAaJTxxofd9O425HLQr5aKqZNWsjqKbAtZtqXM7wzvUiaArJo83sGgDbk9yOZL2ZNcT0Vb+EZ6xJ8lYAB2lhnwW49P/vpAn2lpP/4YTnPQPg6oSxF9LTcyVgAVi+AGCFVAPgPDMbQ7KbNJfGBDNyWQAhYB0G4BIPVqLnAsF4U8BrESvD7cBQn7sAHKlrv43R7OJookCjVc8/kuSLZvYCyc31vKYAVG4xs0dJ3gK3+/ZlzMAdDWBhkgvKlGwBsBDJPpEJc3DQN/9zgqfVAAR672pyNySYfdUyZafEmIhtktdr2tn6LEHTygdaHjx3AbBMwqvWmtkFRYy7CXkWOt+uliKet0YwlkLA94HPSc9qAfAzgLNIHg/g0LiQGZI7A9g5D//uWrweIPmegCsJ7B8DcCzcbmGS3MeQHAS3m5lvHp4/twJWdQr1019/sZldogC2IYGZEjfooitjN5mDs6ymCeZFmyaif96MQKgN2qIuRLcD2FwmiAlAHia5GdyOYKh+f2JmJ5HcD25Xp1+BZx8S9FsjgBdI9gcwGsBGAP4aaIxrk1xD5u968gMW0q4Ok3bJhP7tAuBoaa2raVWeHpGH6bvzzOxveSbJIJKLyQTrCuC/ZrZUcMkuALZI8mEBKAawntf75qMRARAVom76+2FkMVgUwFYRrT0OtKC+fFi7cdtGfKPLFgAPL49m+Sw/InmnmR2VANZV3lzP4wbYj+QwLRJxvkpqHA2eGwGrNaXN/LHAqg7AcE0my2NGRmOxJgFYNEbDqA6E0BrcH/2tKhDqUnAO+jQa4V+kCdUFoPV8BKwaAfxFWuM1ITjm8Zucq/tagsEfxhE1I+fAXwjAKyQX1d/d5VPLp139IcFX5cFqNwA7Caju0f8fSgCtFQqAo/cXtUmjizrrf4ILJ4mjeYscd/sKFCvln/HjYr6YcVUf8dVGaXpw7Sg4p/u0mHmSz4RtDbT0egD/AXB+wgZPfWScJ8keZrYOyb0B/ENmZJXes0Xj83qSvc2Mcxtgfa5/zyhwvbe5D08x4DrpuSGNBHBAZBfmnsD/Uwtgd+R2wsZJza/V/98KBL8RgKdTmFd+5dsawLAApJoDsKqTk70Zbqep4ABQ7M0uZjZEg+qywHQNo+u7ALgNLk5rgtowQX34ugByEiIR3WrjqBRgtZu0XL8DGQdalOmTD6zeDcBqi3AjRH18ciGNMK2jux2i8T8H0AsuUDOkF+VWSJLnVIHwdPlDb0u47gYAD+Thv54WuR8AHG5mbyb5seTcB4CvC7kD/Pwws3vUx13l/9xBY7UGbhf1mLnNNESeGKzwM5Tknfpb6NrJCXx+ikyW8LceJFuDOJPHEiYYSD5FcpkEHmEc1tLB9/sptigaH7VfoXsL9N1des5FJEfqubern8aRfCmIoxmveCF/BnMiybsSnruAzqiNjrzv9iSv07PCtozX99vrPFsYF7dtTH9DsW9jxWNicCay2PGT5pop+vs+C9N7er+lFV6RyJfkQWrDpBLG/UCSNfr3MiSH57m2p84odot8vwHJf6Tph0AmZ5Y4TxcnuSPJbwLZ/rmcc5dzooYF2cKbIP9W+dJwp94LLaXMY1vfQfJAM7stZkXuFGNWxgmtF4BeaaKrI6vWnXI+76Z3NLiwijvL7McfAAwwswu1M3gS3Bb5cLiwhhcB/Fk8j5ZGcL60rmoAhyWsyBNlej4dmNg/yv+xa4xZ0arv34I71rKwtMezzCxuR2l5uPOZs2hW0gZeLeAc/59JaGYrF9Ff52LmqO04GutNo0KaCMkHAFwBoIHkyWZ2SVqQNbND9O8b5Ld8JM8tpwDYX/3VLXiH1+E2hwrF7B0os64O7qhN0WRmPwF4XDF022hM3Vtk/8/x2lWhSHf/OVrXH11Iu0qKdBevUQmR1T7S/RdpCE8nvO+bJPvkiaTPqyWRvFvveXex96boy74+Cl9t7UryafEbEHy/uHj8kvK525E8mOTeuvdbkh9E+v0Dfb+wrttNZkTSM7vrtMGvXrOK/D4qhSY9huT0lG2YUkJ/Lp2mj0jeFmiJSxSjcZC8VBraZJK984yr+YPTBXcVOb+6BpbDE+XOV/19XM8cqwPfc4eGpVVhJMn7kXyWEABW0nm7lQo89/6k/FhaEXcHMBTAKhHNYhLcNrTfNRwd9ZEo3ODZIuJtWmLeYZ9iri9yBRwBYLfgfUfJIduKmXeGummlbiF5hZkdV+C50fxaS5E8Tqu+98XdZmZX6Pd7UrzuBLiwlD8kBO/+G+mCQmuLnHC3ILdrm0Qfe+0n5QQ+Ci7UpQEuh9om0kbz+tx0XOpg+W9vMbPv8shgknaBjwOwNcnrzOzIFP67bsiF53RB8cGtSf6tzZHbRT+N5B3SwH63VGy2Br9L0Yr4tC2pszUotqU/gLXMbGIaZy7JxwGMMbNCZ7HCw8+voLiAy3q4FDm1cMeRvimng0l2l8P/WoHhPerjc8TDHwda1szGFPnsjQQqPr3JiWb26mysyfujOcfDHSLON2O/M7MrJcsRZrZgiuevCbdJ0yA5nmNmV8RtCihH1ZVw8VAtAH40szVStuMpuBhAA/ARgCPN7JOEMXsAXCYPD1Z7pUmPlPI9/qLNgCaZ7ruY2StzE2ABHZgPi+QKcFvAd8NlLZ2WMLgOBHAhgOPN7P4Uz+2lgTStjL7pKhAZW6HBdQtyx5X80ZoJAFaZW07bp8mHFTMeUwNWAFqDtahWwe2kPa1FYhLcjugW8ilOF9APM7Pt02g9gWZ2B1zoR5OA6FOB5Q9w4T4rwu3iet9iHYDdzey5CvbnmnBZKZoBdDazrr/3MdQpxlybKGfw9QD2QO7oSWK/aTV7sISkYp/JtNkHwBskOwto3lLqjz/AxZ/cCKC3mc1I81Az+57keXBHckrZPjEA71UKrES9pFHdrfdaDjFn1n7nNIPknpq8aWkhpE/CCDN7RzF6N8u9MV2m02bIxcdBJmAT3IbEwGJOH+jv/iT/Dy6coUbj9GDx8GOuTWD2MlwUfSXHE8zsHQBdlXvrT3PDAEqT0/10aQY+p7vfXfM53QcDuLCcnO4RFXpNaXlNcNkaGn8PHa3dvrFm9rH+/wCAG83sxbkFrbRpU1/sbQAm5ImRyqcFLSrQ2kELRj1cPNowAK+a2aMVate2ANYVKPaQSfoTgBcAPGJmX2d53TsGsEIgWRduG9z7sD73B5szYZQO0BllfZ1RRhlllFFGGWWUUUYZdaiJUMz3GWWU0exDVsQkr4fbBZkXLsfU+5HkfJUAk2MAnK3nzwMXCHg3gIG+GkkFwGq2KNY6h4D7dQD+Hnw1De4IzJVmNn5O4RE3DoqVsc6tLlvBefdhNMjz98KjPalTykk+H1zOpUvg8lktCpcI7xG4fFOnAyhroivieGMz6x58txhcdsrvSQ4ws0tLBZSgHR8C2CYs26Xo/WNJXiF+P81mwNEL6VIV56NGX2osJc+74PLhh3n/V4OLLRpH8kYzO6IcgO8gHoALIbkELqj5FX1GFfncXeFOFcSygQvcTPMwn1rmFLjQi98jj98GsCI52MO0xvApYhTUOQpu63hEGWDVD0D3cBBpgt0A4AaSFyqh2bokWcwAjhS+GGRmH5LcEUC9AlGfM7PnFL/zFsm0xVoXAHAEyjzOA+ANM8uX0O/9CgDWN3DxX2mBZB8AS0Xk8Z4Wrn+RfJTkWADLkpxagsaSlsdPcMkPfygFtBTeMC9c6pkquIPvE1R0pT+S6xhEyec3i6MauMy141I8pwfcqYfWCvCogSscUg3gY7hsvZXmUUo7Oh6wIpO8LkxrHA4cM7tNmtZQkmeb2aASwOpUuJiryUnqu5mdruM875tZnxLBakEAg5RO5DoAbUpF683Nb81sSaWIacxXrFXUDe7sY0OZchiA/BlIffxbKeTvbSgSSKYUOFe3i6prjy4mer0EHs/CpRteJOVEiiMf8T5Dn/ngckhtrMwh35Qpv2otOmnKhC1d4uIzCw+VsHsELiB5Vx9FX0ke7dCOygNWTCmu+xKuW0mT7RC4AgufkfwYeYpRJoBVM4CbSC4PF5Xsc7r/18xeC4BrkE7vX2ZmJxQJVl0BtCjx3uUAfHmk831yuiCdyWoAviGZWKw1xpb/vkhNiwCWREz6lhhTZZsSB4fBnVlcrUggaQPQr5DJJBN9eZJP5isnVioPkv8CsCWAT+BOO+xcop9qDFxW1rXhjuRUC8D/COBJkuunlHMhnvUke+XJAPpLJSYtyU5qg8nfWwd3iD6tOTfHUqcUk7w+DrB0UPoyuLzbQ+Eqz6wDd6RlqSLAqkl8OknF7AKXHrcGwCkkD5PpNkiD8DJpQEuZ2bdFgFUVctVqXoNL09umd4+aEC2aVAOjtQRVO3EbPW+Yvq4DsGOxzkeSbyCmsIPeIbVWlIeehMt4sVoRQNKqyXAlgG4yeztpURnrq8kEmu+hJGfEVEgqi4fA6hy4A8znpuURvhvJPuLXV9rZs3BHaf6h72bAbb5caWYHVGDDZXVp5dMS5tr10uzLpVPhcne1aAw3wB2wPkf+199tBtKqFJP8JZ1ZCq/7s/xaB8k30h0uNeyvAO4huU+BSr2hZjWP/FQvwKWa3d7MtoE7oNoswFyW5F8DDehEfdKCVb1U3Qs0KB+FS2EzDa446SyAoVPvq6jUV0grS6u8DDMfEK+NvMOpJN8g+TLJ11QlevEYDSiJGuAO607Tv4v5TBHoT0+z4ka0Hg8kj8kv8on+fgxXXGEgybW9LNTXR8AlLawUj1c1+c4WWKXiEQGr0+Vc30wa1kIAztOidZPGWye9z64kV6/A7nB4ZK06+Pjvqio0b7vBnV3sGci3h75bbK7QsGJ8PS1wlYwv8OCjgbA1XIqUJq1YWwndb9CjLgDwppndXcDB/rYGzp+lrv8IYA0z+0W8qKyKE+Ayd54Gdy4LAJ4leS9c9s58GwW+WOt9OhSLSBbLtoTB7r+KK9bqc7a3FfArPQQXLjEQLhf+c0jIq15APv0RX2kon8/q5BLMwCnSPjaEC1+Zqvb9ily+rVooj7wydJ4iWd2qbBTl8mgTsCwt4K0Lxt5MPPIUh4WKw56u51+vMdAn8PdcKw1lXQHKZGnc76Yw4+MqTFXp/T8HcECCa6BKwFxVJg/AheVMUVsO0PVPiv+kFP7ONDxmX8AK4pM+hKvKO8jMXoo62VUa616BlS+ueo2Z7RJcO93nyI4ZuIfDpZ/p7h3sysL5OFza3OEk6zVwq6SybwLgDMy6A/k2yYVjYnXiirWuE7YjBKXIBgJI9lUSvmKKtcZNnK90eHwxuBJZ55ewo1IF4IMwJYkq6SwQcZQOD/OPkTw4JVgNELAtZWbfkXwB7rzo1MCPGGoPYcWWI+FySF2t37+PqctYDA/Cxfh11W9TZNqMieMhWW0OF1PESJ8dI1O4s1wLnaVl+wpH88Ml4btB17YB2Ibkp8EiNhWuMnO4o1avdtfFzKNRihW8O4WzOh/l5aGxNQTAEFViP0Dte0DfV4THbA1YEv7TiMQnRbSOvwC4A7kdtRppPvvGrHYfk1wzakoCONfMFo2Yi7do9b1JE3o6cqWtuugdiZmDCwHgC7g0OP+JfP+/Yq3yYVwDYDWSR0UmRxe4dCf7BateC4AbVVL+BPFYs9hOVfs6SZOcIdA6qpgS9Xlocbj8Tn4ivWpmO5b4rBN9DJT6asM8WqDXKKv0mQIXu3azMmoMk0n/QQk8KKDqKm1nWkoeh8qf2BIB18lw8UbT9Qy/7b+drntQQPeYeE7RInBR8D4T1M9hYYsaM7s3QeYrK1TifrkfXirRJ5aaB1w17wYUv4PcEe1od6d7jyhYBWC2FYAnNNDMr/waLG0x5anejGgBCFatqFbziVT9BeBKdy2k62q1Ij4EYAnF54Q0JUFQ3QC8r+wSQ4MBeHvMBDSBcPT7zWSyrlpKp6rPtpUD1FcA3kSBkGVF7JvZq8oJvpm+OqZCA6pHZOIzkHWdwPsbmfCLCRRqBfyNmuBtJfBAAFZfSTvuk5JHM3Lpp6M0RmPEZ8ltCrSHVj23OgJ0TZF/swi5fKhqOkcDOJnkBWmLYZTKQ+6XlTSPG9uDR3u0o1KANX8C4h4Gl+3Tg1UtXJbDv0XBKpisK2vCR2mehPfoIifr4QB2MLO1SF4IF+JwqvwQUVoe8bXiJgFYTPUCV5cfalX5NMJJcr4G5fnBoG2TQ32Y/DONyJMXPEJNkX54QiBfyJdQlOam/j4OLpg0bw7yIjXCociVu68K5HWWTIhV5OR9Qb6eXeAyy/qt+o0QKRWfgkdoBn4sjX2pInhMkv8rrIxseuZxev7zGou9AOwtGc8HF2LgY7MQ46/7tQTN5Rm4LKQzUH4wcSoeWgCbKnwWtiPaUTZgNUQGmBf8ecgVHa0G8CpcCfZ8q/pyCYA1iuRCZvZzjLZUBbfr5h86DW7LuSviI3M3N7PdY74fKbv+VjN7H8CmJFeOHMUxtetXM7sx0u7hZvau/r0f0hVrbYQrvVRsHNYSJWhu58msMrhCCJvBlVafVIL8m0j+UUkFpyvz64PIpf09GS4EoHug2SwHYD/5f84JnrWymT1WBI/GiM9qIeTKwaflcRxmjk/zWuEjcjN0kob7lp5/AHLHT+6Uue8j3f+rDYW2YAOguQTQH6sNhTNJdqqkdpKPR6VMto5oRzkU7lg8R3LjAKlr4bbu19Pg8prVHgXACgCWTMgUehtcZec4Wiyinv8knl0RBFdqK305AWfcpH5NpmqolXwYWYGqA39MVIN5N7h2dwBPphgMPgh06SI+y2iFL5buhysp/ze40I7Tkf94yQxNvLj00v/28lTfPSetdoYm7IJaRNoC7cWk2ewJ7eIJND9O4B/HY2Npb12lSYV+mNQ8zKzFzJqCzwxpHIcht5PbHS7V9hLSFuoEYG/DnRv1oQjPmVmjf4aZNRcDArr2DjM7Ay7GqxnAqdoVD8GgHHPtd8GjbA1LL3AbgGODqhtrSoWeR4I93swOLKRt6GzhjQk/3wyXpD9q4o2XOTgqmBwLaHVcRwM71DIuBHB1nneZpVhrsNMZ9VdFheWvS1OsdTRcdonmEvq+JtBw38jj6F41xQDZJLimRdoE4AoTHBhjUnq6Ai6g8uyI2b4QgCHShI4WsLcGoFID4B241MUgeQmA/gnyiONxkLSrtwVmb5bJI7ro/ERyPY23IwTsJjPwFrjA4WsDq4JwcWHlTnY/nkbBbQi9q8neJp/a3RmPCgCWBsBQklcGEeTbaaXtBqBnIbAKfusnDSKuI2aQHEDyQjM7PfjpMbjD01sDmJfkkRrIS8qHcmTAZ324sIh8udBPk/n5CIDJMe+8h1ba+UluHC2NFFQT3jdPmxvlsC/Fxq8C0D9FTnEfU1VswGHs2cMYf+N4kjeQfFTn9uZT32xnZi+rvzeH2z1bURrvDJnwOytWbncAiyZVz47h0Q/AsXAxfsdWgkdcG83sW20WrR5o7wvLn3lt4EOr1UJdMV9NUI/ST/ZTZCX8DFddJ+NRCduV5DIkv9f/l9fZpNRqoCrR7pHiujd1kHkmnxnJRUmuoSrH6+jcYBjc2lOVbusLvQ/J9XWEBzEVpm9njs6ItpHkLSTPTXjulqpKPaaMz1iSBxV4f5KcXubn0ZSyH62DzEiSN8k6kvOQ7ELyCI2PFfSei6aQxyiSz+r6c9qDR4I/BiRXJPmV+v0XtXe05HhGmvFN8qS0Fa4j9/1DfMeQPJ/kUiSnxMn/98KjQ5zuWpm+Jvl3kiPhgv1aklbnqGal0IQxheoGamCsBxd6sJSZXa5nUOrmmIRBt76cqWsAaCjkX1AyvrPhon/Xgjsk6p93ghysY0IzQ+3wxVr/lfDoMdqpKmdFroJLTpivT61SC1EhE0pO7tEkl0sq1eZ9kvrtBpK7woWcrKT+KsRjHZn4H5vZOZXmkU/bgtvlrA1kViO/X/+gUna76QIyQbeQ3/HqjEflta3NSf6gysKxq08AJL1IvpWkkRRY+fxB5r/kuXY5kg+SfCWNZhVz/wokvyXZLzwXKGAOnfMHatXdA3MZBfJ4UoeMD85z7WYkh5P8sRitR5puD5KD2otHnuetLs3qZ423G0muWIwDWZpJQ5nvsQbJBQtoP3M8jw7RsCIr0wuKpRpI8iYAl5N8Ga58eKMc0huT/BtcfMu+0SMZKf0MJyhp3ok6GzgMwJdw29zLwe1YvQbgajN7scSVMKlY65twZ+JORwnFWn9PFMhjewXcnkRyIFyg6FtwMUnrwQV0fizN5M5i5KHrxgHYub145KHv4eKKpgP4KSxoWuSzJ5H8odzuRv4sHL8XHu0zVguZEtJK9gSwAdzOocHt9D0Nt+VfsSKRJBeWg49wieFGpjFrijWNfq/FWttB8+ojEGmDS/vyWKXk0ZE8KvSeu8AFslaCquBSF734e+Qx25gMhb7LKKOMMsooo4wyyiijjDLKKKOMMsooo4wyyiijuZnCApYLRr9LSU1mNlXPKBSkOMfzIOkPZLPi0uI6AAAgAElEQVQIHn6H4lcdNSm4A6bDvr0rKOvvwt2c9tiFa4fd3LXN7K1smhbXdwXGb7vPwXYHLJIbwqXyKKWUVCNc0rUbzWxYUmNS8mhN+L2SPMptx6Nw5aFKoR/hDhVfr1TS+QbWvXBRxYDbPq4tgV8zctHdz5vZXhEe9wM40ueb0hnO01I++yIzuy6YBB+b2aJlTriFAPwfXDmvTQF8YWZrZFBUFFjdDuB4LY4dPj/am3zg6IJw6VwuxMzBpD5Z/XyaMM1wQZ2NyB3IrYc7kb+Lig2cSLIppjFJPDx1BnArXGrb+shvleLhcxx1xqwHitPyWB3u9P/kPNpUWwA0/gEtcPEv/wLwd5IHmdmreWTTFjznVQ3CTkXK92C4mpFAfDbQZgA/ByEqDXnaFaUBJK+NgHFJWhRc9tTd4YKFpyFXuqo5g6KiwOppuMIab/vFJOX8aIYLAF8TLtnBdIHTZ8EYTjs/OgSwCODnsPCEOmI9uCRpK8OlA5kBl8b2PgB3+RPuinzfQ9e8BmBLktHClIQ75f0KXOL/6pgJ2h+u2GWUGuAyge6Vhke0HXrHGrhiFqvCnTzvFxZuKKIdnQG8ZmbjEgbPfAAGw6XmeBDAYJ8WmeQwuFxdtwF4mOQZZnZzmtXNzH7QM3aCy0DxYoqBPLHAJfPApcYer+v/AeDMlGPnZDO7RvctrMGdTq13k+tGuAwdPjsDtBiWYq7k64N14dI6h9WpW+DOcr5jZqMqBBYGl1ixVtrN8HYAJM+jDq7oxUdBf3qwuigBrGLnB8meArAtNLb96rUjXHqfM5UIM+386BDACs1DCEz+CZcczufMpq7vA2B9ANuRPMTMJsKltj1cmsd5AF4HsDrJuCRoq8Hlop6cYKIy5rs6uCIU86fgYVFha2X5NNB61pGWswqA8cH9aduRT0LVcGlxVgSwLYBPSB5gZp9IMx0ucN4YwCskp5vZPSkH7gNwx5WqSV5rZmeWOxcwc+Xp6mDx8FVmLE87QxAvlv5Pz67oUSjJuwYuXc1+es9m5DKJhu8/vyrl3GdmF5Xoa9uL5AmSt0+TXaXss/cCOMtXiCrDnxfHo1oAthJcKTkPVoUyg4Z+6/XhMq92jWj0ntYA8AzJk1S2r5h53i5UFbf6wSVY+xdc1seWQOBELt/1lgCuU/qXjzRo5zWzI+FSrN4cqfGnx9sIuCyQkwNTzCJ+rFYNZEo1XRKuFFgaHnHtuUv/vUMrxyNq+7WRjv6oFB7BADC4Qgs3w+USnwqXo3wIyd7KSPE6gFUEYBsAuIRkWp/YtpJHk7TN/w1skgsr/U6xfonBJEeQHAHgqACwVgj6I+6hRwX3DUaFiiBoPMyQr6WuaKesk+cA5CpM+3FkMWN/JzP7M4CRJCcpL1daWp/kVzKTeiJXOMW7HFrhshqMInlxAKaoAI9GuBTWe6qf1gVwvpldUsRh7iUEVl0wc8GR5uD/fr5fKnD7sJz50S6AJbWvn8Cqi1arSXAFHzpJEJ3g0rXsJrV+mr4bSPI/6txdSO6SUOyyIeiMveFKfPkO+xC5IqtT4RLtT9HAS80jQqsKLP8JYKg6uwtcYYWo6VkSD5LLwxWduEf9Ui8Td4p4XaZLJwDYluRTAC4V+N9MsiaF0N8I+unZYBLMp99eAjBfEYOnsyb3qfo8Hyws48xsC0385pgJ/3xw34AStSyfprqrTLT+cOXee5nZOSVuNCCywPoUyCzgL2wFMIjkIQW0N5C8FO4sbRfdWyu599DGQw/9v0Zz4wCSX2sRSKMhFuLR08xqZHVsCJf3vkXgtmLKPrpA/e7l2iSAXQbAn+DK53n/lcEVcylnDlbcJPS0nTrpSuSKQ0wVgMwPl0dqhla/KRrQg+CySNbrmUvCFVk9huSTkWKUoWo6zMwGq7qNH8Be4+oRMTtaS+ARahJdghXY84+eNk/Lw4LEgr7yyzO6Zx246i53CBCvk19oA5K94TJSHKU+nEfm9a4AtjCzpwtoD1tpJ69RlZA9WI1ALo30CAB9SaYxQ6rhas99o0nSJ+aaIWbWi+QpAidvknztC7yqcGd1CWD1kibl3RUIh9hA5vL9cJkxdxdgnK0NgWPly/I0A8A9SmbXFW53tA7AVSTfAjAypnxdtfKlramx4y+4zcxOJLkjyU0BvKT/Qwuy1xzfJnmCmd2SB6wK8fDXep/VuerLcySbt/Lx0L0LwBX8CM1kX9X7DJmYx8AV5qjR85eRiVjqHGw3wFpTQHRzWEJKHXUSXCHPlZErBX4hgHozGxzplCcAvCjzb3TCgPW7gRcL8W+WqdkqU6o2mKyU6VEMj1Az2Zzka3KE7y2T9N0IIKTlsYZs+IYAsIbKfG2FS5nyL/HpIe1hcZm1vtbedmY2AcDtypX9F03eQj6N6yKa1YjIStnVg1YK+bcA+CvJnwNNNM7Egpn1V3n6f0urXlX5+wGXA77YhIZtZrZXhWK3NoALF5mqMXqxrITTADyqNM1byP8aFrtoEtDeEvTrugBOiMuFr6rVK0a0ta4AziK5oxapBmlU+8Ol9z4cuSrX02Ve/dnMjo3slqbhEedgv4JkoyyeWXgkzLt1Amsp/H4Pff8H5Arm+r5qBbCJmV1Y4hxsN8CaN1BBowP3V5L3CH3vM7MWpdatSlDLOxVYefsqp9ZzmmDbBo7g2hS7RWl4QNriThLEOYGGeHuKCRPHY16pzdMC0/oi+cbmBfCpmb0lu38ZDabF4Sq3+C3iushq3yWFvOYjeY6ZnZAAVn5wdQXwFOLrNkZNofUC/9PSBZy/feF2cVvhKid7udchvphpWp9TubRuAArTND47C5C+lDY8DcmFXvcnOU4a7+ZwRWNnekflol8pxrScqpi6TQONqAHApmb2OMmpEdnMgKsV8I2ZXZGSxyQzmx7RrEIH+8QEHp+Y2Q0xgLVQwuaWL5HWX5ZB6L+kQKnUOdhugDVBzFtIehuWZtam8lonwZV+OpdkE1xeqeYCqn++Ff46Day6yK7UTDuGBYCFBSbFcPkQFgn8Md+b2etFmjCevpJG6B2UPeEKsh4CYH8Aa5I8S99fBle5xWSufC8wjg4yFtAi6gVQ8yvafosYsAqf1xmFC4HWwBVg8CETx8ucilIvJXJcS7IyAA+a2eW6b0nMWqK+I6kh0g/UpE2TZK4zXPLGcYHJOyEGsM9U4GXUD9WVZBeZtweIXz2Al/S9r7vo36sWLiToihgN60zlausTacvCSWBVgMcNMfPGg3rcuGmSX6y/FviWyHycWuI8b1fAek0d3z8YnL+qgOd1moTNalgNgIeKLb9O8nW4UANfsNLvqsQ5btcheY+Z7V1mW++CC9Xwjt6bynjWd2Z2XqRNj0jVXlGa3EpwAZ8LSRuaDuD0Ere4GwE8idz2866YdZu+JAUnsrvXHGlTjUz+QzQRZwQ8myPvV65ZV462dZsWjMYi+qSL2jPOzE6Xid0sbXxg+D7BcZeNSQ6WS8BP0KkAzjOzk2QGeh/W4yQHRICki8zNW6LaZcBjKwFTX8m4C4CdSD6J+NCF89LyCOitBICp0rzfLgJW3t/5Cn5jigOsIfK5bBys/L/AlUrqI6F6R/zCctAVawZsEDNga+CCEfu1w4CGmQ1QLAsANJvZtZXqRA32veBq+J0lH+DV6rsDJey7S43H0XNqggFEVCa4chqAcTGR7jNI7qWdISYAwSUkw0IEP5bZf71IXqUNC2iXqiGlbKcpR/xtWhii2laz5LGePs/BFSJZA8D9itj/WA7np81sUJzpqjG4nYDo0IDXQWrDvwRU8+iag5CLM2uQD+izfOaxeGyr+4+Cq7J+BFwIzLnejFQm4HOL5eHY2GiS78T4OdukKW8CF/Ee9uFYJNfP7DCKi8P6VTsBddKwWqQhrC8tyPRddzlgh1agSIAHz8OC/7eHr+NyrUDnVrIT9W5NZjZAfrif9fdgqecvAzhnNszS6iPdTVV6fHXfemkabXnuPTm4r4eeVU7/PSD/2Ax9Ni3GL6Yaj3/FrNW0q+GONV1qZjvr9yEK23haY3wXaRYPmNk+hfxtZnaS5NugOdQEt1s+luQYTe59kNsJvsPMlkGK0wARHj0FVj50oY1kY7k8RGfGmHKMwQR/PO98hSPNXoAlhH8WwL4asPMITHz8j89WcCmAc3x5rAqYAqcDaCB5envFdJjZpXBHEwZWGjyCd66F2+lcUn12E4C9VAK95G6SuVzsJ+1zw8ntqRWFI/pDP1ChBcnXHUwKBn0pAD3Tv+8qUgbPyxIYGLxXG9zxsp1JXqbF9hqSL6kGZx1cBPe6ZnZ8EeyGCiCOgzuf11XA1Rm5eKWHACxhZqcUs+gGY/NugdWFcDvP/xZIls3DzN6TiyQErRq4mLolImA10Mzunx0W3E4RB3iI8I8rinlXqemLy9R5C8BjKc5KzUhwssepwbcAmGhmK6gE08VmdmqK90/FI0J9itTYZsT4fWYUcABfBBcQ+nxYoSUPtRSY9NVaQBYsQrZTpdoX8o0NVlgFMPN5u0J0FMn9goWvMc/k6y2/TG9991lUe5If6V69cxuAV83swxIWjhnSHs5Uip6V4DYozlA/fyn5vQHgdgAPy6QsdYG618zu1SmHjTTpJ5nZ2+Us4BEH+6Ue8OGOtnUrg0dYa/Qukp8AuApuNxuB78rganD+y8wezzNnZvwWgGUANiH5IuIPJftJWiPTcAMJKN/KXY9Zzyom8ZgCYCmSr8jf0KR/o4I84lYwlMBjEoCnSeYTlO+zw1LwapOT/vEY7df38c7IBSAWQ00BkMSFnnSF28X0xWt31ycNPQkX0wa44MEb8vhlHhTYeudwT+RCQsJrP4Q76VAprfdFki+m8VeWqv0GCzwr4ZRWfz0S52DXYf1yDlWH8yOM/m/UHFwUuYKz4+TPPC7w/aaZH+1K/sDzQuqgSlMT3I5Jy++IxwZwoRyVIn+ofKSZfR2A6mZwwbOVom9jSj4tAnf4u03/XyDibM1Hv+rgOxT+0svMvk1YILaHC+nwJdLrAJxkZjcio7jFdCkAu3nNqoLPbvf50SGAlVFGHTARu8Kl1gGc03tq1iv5zcKMMsooo4wyyiijjDLKKKOMMsqocrZ21o6MMsqoo8jyTWJtsS4DFwm8NVyRgCq4rfiP4DICPGJmY0t1FEYOgK4LlwWgBi5O53MzezN6XSnP7+B2rBfl4ePWynGoFuDxhZm9US6PjDJ5zKnCWITkUySnkByT5zOZ5KCg3lmxfFYheZ+e8wvJcSTH6u8v+v4+5V+f3dtxb6Qd/vMryYkkbyK5UhkyycfD99W9pfbV7KDVzkna7u9JHnO0hkVyD7iUG9NjrqnGzNHkYcbCfc3sqZSDtRrA9XCBig3IH2JBuKMa98NliWhNs2IVaEf4bN8OwsUIldKOPZCctiPk1QXu3NcxKQuqlsKj6L6K0Ui7Atge7hxpM4Bv4qoRlQFWh8MdGF8WLsp/Mlzg5Z1m9molNFGS66jPtgva8S1cwOuNxWYZSSEPxsyrsuSRUQHAInkUXK6nOBD5Ce4w5uPIlf36CC7wcWm4qNd/mNldBQS+gAbnIiW881i4TBITC1SAzteOkXCZFU7SpAlTp3hQqVQ7/IHSqOrwg5mtWaCYaj4ePjWPldtXEZ4rwx18XkeT0b+3j4y+0szOL8N03h7ALYg/WO0XjDfgovuLPn8ZgNVguFMZcfL3/z/SzB4pEqyi8vDyrUHu7GIzZk7FU7I8MkoALAljPbg0vXHnwqbAHcQkXG73tyS8P8BFRw9G7rD0OnCVgOOEXg2XmrgHSgtcJVwOoz55BlVSO6h3PRQuB/sicEcdaqWFTYmsiuW2g9Ic7oJLZzIj8ttEM1spzwRJ4lEDd6RmX2kO+fiPgyvs0JpiQh4Cl9Fiap5n+jNmG8GdZysGTI6CO8jrI979YfqqyCSnrlkFwPQiDw13hstYUB88KwSTpgAs6+EO8F+R8vlRefhkefNj1qNPTXDnSa1UeWQ0K/2vkzUoHsTMOYh83qXJcNV5vZB+FXi1YOZ0vz6b4X/yDLLrBRRJFzSnANlFlAFz1h+T2wENrCa4zACd4HJQf6zffOZGVqgd1MRfUzm+LsbMqU8MQDflSS+GRw2Ai/XMNcWD+fpKzyk0GfeEO6k/LZjMnwF4VP0Vaoo9ALyJ+POJsUAi8+zCQC4z4ApErAl38HYnuEo8dYE8nisGEHXti8jVCqjVM7eGy+LwJ7iD0X6MNcKl/Vk/pe8sKo9OWgDjUrLU6jeWIo+MCgCWDjh2jtjjVRrAm2qA+ZQjJvV2TeQS+oUHqWtJHhQzaFeR3Z8PjM6FyydVaATtLqdndHJE2+HbuYAG8qtw6Yl94chnguu6VKgdHqz6CuyhQ6wXYNZD2XuR7B22Iw+PagAXBAdiJ4vH1AL9FdtXAb8ucNlkfVGNsQBWNrONzexIM9vTzBaDy+rpwWBBaWNpgeQmaU2ESxO9rJndaGZfqBjHa2a2L1xCOp/PfwWSO6WsCQmS2wH4I3J58/+uZ54k4Hoc7sxmLwDf6V0a4NKnFHp2VB7e0shnudTqw2LkkVEKwJKZFF0NJkqTCmu8IQCtn+CyF/gk9P5+Xy4rOmhPC1bwuPxNPqvm+gl+n5Aa4FIOR78P2zEZrpLNp/JbfSDzrC4wFZ7SNcPhcqZ/AGB8gXacjphsA3FgFSkyuTFmTUo3DS7FbhoerXpGuFWeBrSS+spTWL6r0cxWBfBDDPBcDZfz3YP6gXLOFwKT1ZHLD9YFwF+iB2WDrAePwlVM9tr28Wm0LF1zZGB232tm9+vfveRjXToA3K01DrzGvlGBZ8fJo1DOMQ9q9RHZFJJHRvlWApLd4RKEhQIZB7dL1Ij8KXn9IHw5YvLMC2CR8JCrauVN1wp+NGYtlGlw6UV+kv/iNeTfQexiZvMFzw/b0SbH7YeR++sCcPWO64aYQTZcoJmvHanAKshvtHpCOzqZWffIJE/iAQDvKo0uAkBMqqIT21cRXt8G/XIEXExaPgAaCZceuwou68KdBQDrWC1WAPACXCqawxMc7y1wGUJ/UPu7mlnXNIOZ5CTkikCsBFfqq0qL7jzqlxFwKb9vgsvYub2+vxjAFXk2QKLyqFIfpKVfkauRkFceGSWTXyk3jDiox2oFIgrnD2/TJN0KwGOBmtwIl8rCF9tcD7lwiOlm9mqBdxtJ8mi45GJNgYl6sVa2k+Eq+6zrg0sj7TANqE6RFS5sk/8bZrpsgssLhIR2rIv4JIGlghUAVJFc38yGFuDhaXWST0dAy2taSaAV7auQFpGmPE/KXbM7ZGa1IqE0WMzzPb0BF2C5ZYJG2Gpmk0j6RaSN5CKFEiEqx7m3GGhmP5HcKmKCU3Iw+bqGA9hR3/8xD1jFyaPYVCrzYuZNl3zyyKiASbhgZLUz+XaqUHgnrwpup/COyLWtcFlKPS1XrJAVVnBL4JPYwMwulw9ncw3G5YNbwnaY/DJ/l8ZVFQBU1LT1YDgJLmbn/qBvou1YPqYd5YCVB/3uBXgkgVZa87AFLuYpOhm7B07oSSlFMyEYGyukuL4pMl4MM5dBDz9VEXOLSFGy3symBfeGf6sicrLIO4TpgZMoTh5pSohFlYOQX6w8MkqnYUWpRzCRopVaohqXCTh6xUzizgn+smJA6ySS+8JVCv4i+P4DbTPnK+DYWZrAjnAZO6fFvD+DgX07XIpe5mlHdQwY1Em7mxwpDdVPAFLIu9oaAaxqpKvz1pdkPzM7M+A5GS7v1DsRoGDcpDSzCapYNB2uUGua+Ko/BL64b1O857hg7GwlXxMS2thCcoVARm1m9n3K4eIBpFXxZKeIxynq304AbgXwuZzwYTjD23naniSPYsd0VdBvLACSGeUBrF8inRkOpioAwzSAauB28SZK2B8B2C2Y8G2RZ38X0SKKJpU78mbTch60SPbR+4bvHG2HSXtYRc71VRBfvbZB7dodLsbI8rSjFbMWPm2EK4/Wl2SoYZ0ph/PqKQbyxAI84miEeER9WUMwa2kuQ3IVmpFyik+Fq0FYqHTbPoGG8lEKMQ4GcIkWjA2kLd+SxwR7ODDti0k7PAQuf3sjXJWXnfW8IyX3KgHVcwLdzST7Orgc8vkWlKg8OqfR/BKeU0geGRUwCV/DzKXTQ18P4IqBLiHNq0aCWjhwOlqM6VgHF6vj6QsUmftZmtXByG0/v07yeJIny3nbqtUSedoB+Q5uQS4uhhFQrtEgbotZNaPt+DymHb48/IhQS/E15uCCDfNRNXJ51ZN4RKlYx3unSF+FdB9yGxAXkVwoTwjEAOR22mrM7J4UWvL3Ah4vx//A5XUPn+v/9oMLozGNswFpwxoEil107waqcwjJfgBcKa/P5aN8Cjkn+vsFCl5E5RHVutNQU8yC/nkGQUVaXIHAP8HM+bzb4Cp0VAtsuuj7E+Ww3AwuXmoI3I7MQ4FAYqPR23uXMKEd3gT0MWMU0FUjV1jDT9b3Ypy0+dqBCvmyftNdQvEbG8ivCsDB4XlKkvNJq94h8Oe8b2ZbpwSTngA+wcyBqXdo4ZkGV1HnJGlCoR/yUTP7exGL3O1wG0b+/p8FVt/Bba5sDlfZPDxf2gJ3quHHlLuE/iRE2ipDbXqPUMPKdgnL0LAA4OZ8wBD8ux4u8O82rVLeFGREY7gy5jlP6bpFAAySczv8PCCn9wJS3fOBFWVqROnmGFPoDZmL0KDbGMDbMn98uMJEuIhupGjH4ARzrZCm9XYR7Uji8XYJYJXEIwSULXS/n2C3KWvGGyQ/gKvsvG3E+dw36cRBRMOCmY2C2xnsEpjRf5NP6UG40mjdIloIAeyahkfA6wC4WDpvwnXTsx8Ur7/FjKtq9V/PPNpcKI9Cpd6i2n0UrPLKI6MUgGVmlwUaCILJPRm5oLtl4LIfNEkQjVodp0cGWZOZ3RozKS5CLoSgOeYzQ/cPReGDvfMAuDA6wGLaYdKcGPiqHobbJLhEQNkmFX9IoGHla8eFSK50HAtagT+mOqYdZ0cj3RN4VHufThFgldhXEUAZKa25OvCvUDJfBLko9WiA8M5pAcXM3oIrD/+j+psCQC+vKgHahFJ5qL+3AnCNFle/oDaLlzfnJki7DuPx3o0DrQR5tCD/Ti71zImYtShtXnlklM4kBGY+NEwA76vTO6mTw4BLP5im6/c/BqryunBJ6+IG1E0yK5KoOaV/4AkzOyxBW4gefm6Q5rWF/COIANpX0hj3lElSiXb8zzw0s8nyu50aszI/ZmZHJEy+OB7+LOElMtMKgVViXyXwrIGr6r27Jnxr4Mv7FMBZcGcAewc8qwEMSsMj0Ay3kjxWlWk1AcATAB40s4kk30YuOr4oHgGvbnBnFLfR+7bIH/mimT1NchfJfTpmDm9ZHcCoqNwj8vC7fN3y9H00WLRoeWSUAFiBUHxalsYYbaw+AJOmiCMxbVqWimRrQIET7zHpZcKQjC4x7UBg2laqHXNUtoYY/itoorcCeN3MmrQgGFy2jrIAJQ+YVYxHoTANkjvDhbNEQatv1KeVkK2hE1ywdOeI9jURuSrKFZFHRsUl8IsmJysp8V1H5cOajdoxR+XDSgsCCYBSJUA5vJ14UNrkmmb2aQXbEwUtABhqZn9NKY8wZ1hLnrmV5cOqoNM99DXcDxfdOyJwklqMM9trK28B+EOaLJ0S1ESp3U/o/kLGvNd6ntB9E1MeiJ0d2hHG7/jS3g8VAqsUPJjHwV4ns6NPe0wOPY8A1oY788fARLqlHXnUA9i0kmAlXoPgdg79GHnXzP4a4x9NkocFfj+L0araVR4ZYaa4mGVInkTyOZLfkxylv0NIHkVy0fD6Evi0d0732aUdE0neTPKPZcikXfuqlL4laSTfVvvWaEcew9vj+RFeu5D8T9pxMLvJY641CSvlHyj2GTpkurx8FRWpmjObtONjM3unXF6zQ18lLAid5Jd5e3aVT3vwmx3lkdFvpNlllFFGGc0poDU/yeoMvDLKKKPZWrvS59Z8GSA74D02lIqfaXxzx7jbU/na2s1ayMbR79RMI7m1nJZPk6ztYN4g2ZfkNDmR7+vod8joNxnf36mYbvdKylpWwockPybZM+vp8qkqRad3AfAPkoNJfkNyEsl3SQ5QtHJFAE1g0QUuwLIN7rDyX0upS6e/VSSXSvlZhmRn8VoKLti0AS431rDMYfq7BapdSY4H8H9woQdXVFjWm8Ad7r4DLg9YRu2oaRjJa+iojWQLyVZ9/L+p8ID1K8T3KG0H/0RyNMkRJBcq8r3rSb5IcrrANc3nPd1/MsljBXYPqMT8jXOCtptpgUX3104k+0jGd5C8UGEIy1aY/wSSjTrylFE7gdWiUpE9UFEAcD/Jc0h+GgEykrywXL5tbW3LkzxYg+glkuvqPFgxz7lLYDc+JVg1kFyWZA8N2HECyqVJbkJy3tlQRpMUQ+Y/E7ORm3cBu1NjahzJq0h2FYjcSbI/ySkkVyU5VXUEKvkOx5G8OpNG+wl4XgnOa1OTSS6XcP2NJJsDbevCCrzD9tKyhujsVrH3fy+wvVDm3tIFPovovlcFdGP0dzrJDWZTOU3Xe/rP9PbQSjpaa2sPviSHBXL1n1tJXqpx3kdjfG8txC+0Q7sWzNClHXxYst+fQy6j5C/y5fwzBth6wuVKH49ckdXTSK5d5oAr91CoT4/ypZl9a2bfFPiMJbkPXBLC0IEx1Mxen0vHxcok7+ho350OPt8BYOUKAcVacEUyog3ZFcCTcL5SX7hkGbgkfwtXEKiuIPk+gFel2WXO0DKpU2R12wHu7FYbXAUVf0ZsZ0XrHhZkj3wXucO3EyXoNgD3m9nSc0LjgyjtKwW6fkDVw+Utn6uilLXpcSVcnv5vFVpSq4XtE7iUK63l9EskOrxaY2kljZ0mAH3gUmE/BOBYMytHe1wwYQH0WRZM10yHy482Ru9SlEYYtKcGLgHlOnBO9rFw+eI6w5W2yxyNlQIsrW6nIpfXvIs82qEAAA3ZSURBVC9c8cudkUuiBrgKuB6svJb2J7gUKn8BsBTJDc3stQLC7mxmzQlaX5ioP26QVJlZW1NTE2pra0tuvNp8WwSsDEB/aV6Xmtk/C028UsEyRbaJpGuswP+L5kHyFOQqQDfC5T0fFEzwzgA6kZwGV7TiAZIPmtnEItq8AFyZ9r/BpcCeB7kEfv5Fm8V/e7hsoxebWf8S+/s/eu/w+dQC20XjegxcepgRAPaHS8+drw09Bei94BIRTiK5NtxO42S4/PhDzOyUDF7aV8Oq1spAuNQn3wDwGpUHrV0AHCgz0FdNWR0u+dqtWlUo4HotYWLMD+A4kp9LuFHy6TmmRcuZB/Tvtra2h6qqqt4oU7taW1plQzCYJ5jZRST/jkihhBiwm4T0tfxCms/MFkgJqFEejOE5ieR3EeDKyyMAgC20E9oVM5cF8+ARlQu0QJ0PVyDiVjM7LglQArlfAeAg9bXPxjmlQBc0ATiB5GFwlaKfLwa4zIwkDwRwpzQdX5JuR303RRpkC1wtgT8mjMmQfpTm5GVRBeBuM1s8g5MOBCy4FMg+FcozgdA9aO2CXHGH+bWa/C8zI8mXkcvRtKK//7vvvkPv3r0BoI7kjiSPN7PlzOyJtra2ejMLE5+1adC0AliM5OGIFD8l2WZm25jZPiQfB3B2U1PTT3V1daVoV/dh5hxI9QD2F6heiMJ5tzujtNpyxdyTlkdtkTyWEVCtjlxq6qIwX323h8Ja1kkwv6pJDoPLaVWKeedTCz1A8l0B19dF9vXaAtkZ0qAWE1jtDeBuLQB14vVYAVDsA5cXfimBb18AB5DsDJct9fUMVjoGsJYNwOHDSPbH06VZ+QFUB+A+FRbwADA+cLb/rxpz79690draWgvg3wD2NrNGqfw7mNmumDVfVKsGwnK6h1Gg0Wo5A8AuJDeqra3dHzOX4kqjYV0gkyR8/j1mNoLkdZg14+rvyVc1vzTgqLlUKi0JlwH12JjfLtPv5ZCvL9kHwGsk/2hmk1K081C40l4+h3stcmXANoJLA72Y/E4DAUwys2cKLHT/BfDfCJ81ABwmDXA0XDrugWY2LsvU0H6A9VUAHqsFYOUd7OOlWdXBVQHZUdrOYYFt7yf/5xHTqwnAOQKiPTRwRgL4DLldPe/g76lV6ycA75CsCpK5eclvDJf58X0VnRhR5ISt0krdEgAlABzlzakKTOLZmSaZWU+SR2hCT69Aew9MAKwDS9Ss4rSsU8zshpS5qhbQgjddi5vf+fb1Cp4ysyVJjhJgLSqTtRQabmbDZbr+HcAxAM5RXvqbSN4ZSbWcgVgFAOubADQ213fR3UDvVNwRgSNeoLVesBp+FtGIIMA7SdvW/4SrtDswZqDtCHeU4TMzOzTmd4PL9jhYvoOpJYRRbGlm3VVUYDe17+ASBlF9xM9TzH3l8IgCjJ/QqXj4dmry3wLgKgB7lQlcbST/YGZfBrL6A0qs+B1p170AjvGbNCnldFweoDS5KHY3s+tkaj6uzKPFq3+5/gSA6wFcLzP5UADXAriK5BPIbSgNI/mkmX2bQVCJgGVmrfI1rA1gSZJ/BXAdZt4NXN3MRpFsw8y7h41wcSzeCfmfhFUPZjaS5AHSYuLI18aryrMSHWhmE2JAMS3to6rAfwVwNYDjzOyxEvpv2RInZFUZPKbAxQtNDb6bF65YxLzF8hAI/J3k5TKLvK+nBa7+5GLyAy2ImQt2VIJq9flFPEfD7b51Es+PABxiZqVUSN6pwO8tcGXNHgSwQZnhE3E01MyGarPhRbiNKE+bA/g3yT5m9kkGQyUAlrSU/gAe0wR5EG43JNwNHKVBHt093E8ARADfJYU0BMDSplJOcWDDiF8r+gzKH1EOraD3HQbg32Z2cClqupl93d4CiuMhk3YmADSzn2Wql0qfm9lGJLeBK9zxk5mtH+G7KlxprtNitD4LtSt98WVCsGRnuBqVz5vZ+xEewwAsDuAgMxtSRhBymjOoK+s9KwZWwTjqSnI/uGK9M+RKadVnNIBLM7AqT8MCgMdld/vc2d00CdZEpE6bQKtVjst5BXLVAP6WsrhCIYBozyA7SlM43cxuy3wKM8liiJn1khYcveZ9AO/LP3RIpD9fTnj0ywDWipia15vZgITrbzCz26NBmSXQ3RrHq2HWgqcNApHr26Erd1DfbS5/WR2AVzXWRmaQUzkfVliyfLR8IDUCrB5m9mPM/WORiyauBnCRqvuWNXc6oN2Xq6JOuZPidwtcHjQSnMV7RsBqXgD/jLsezl/5rkxZ/+OeAM5MeP7tlZCJmZ2m3dAfYgCrppKnMUiuJitjT82pZs2JbwAcbmYfZNk02gGwtDM4BS6k4D24gpF/AjBCfqrB8p9sIhORAVhdbGanlyF008DvpOdWt7W1dWlra2uqqqpqrfCkvH8Ol1vUEV/fnuAV0GYkBwb8vFM81s+k+z8neQhmrg/ZheTX0tJebMeFY2qCL89KGJ9+fiwMt0vdFW5ncX/5+XxAbJXmyGlm9lIFNMWM0ghH+bCuTZEPa3QF82FdQvIrPfNb5bVapMhnjFKppf3KeI/bdar/ntlUPstEExC2M7+zSH5G8mfJxqfv+T5tamGS6+n68cEzftZzz2rHdx8XZGnwWRveKPFZjymzw3g9N5oFYhjJTQPtMqNKL6IphNQJwBEAtoU7GLogXKTxC3CxLC9XygfU1tb2JzN7Grno7nPM7JoiB9VQ5HYsS9XMOuv+O8zs1GwBYwtcNHhnmVcvwkV1P1hsSSySu8P5PTcLzKf5zaxTO737zXDn/H6Gq9bcBqCfmV1d5HNWAjAcLrQn2mDKyugH4KpMm/oNASviu2hXrU4Duh9cAOe7cOf8pqXlrVWtD4ChMj9YRr+0AFjOzKZlgMVt4E4XfGpm48oZE5FsDT3gjnHNY2ZD2mNMyc3wBFwp+uUBLG1me5X4vPUB/AMu5rAHcseTvgJwq5ndmEHKbABYHTw5egB4Fm5X5ekSn7EAXBxOVYmv0QIX1tGYrZZzvnsjKsNKgG1GGYUDoy/JmswPkFFGGc32K2JGGWWUUUYZZZRRRhlllFFGGWWUUUYZ5aOO8CHl41GpUk9x91e6bcW8a+abyyij8impeMGXKC31bz1cCpofUkzgOB4z5SFXgdDJZfAYiFxaj2fN7JB8gKI4sJMBwMwuSQlaL8CdvH+m0LZ30OYquKMbd82FC+LJAJrN7PLfMkwgkPfxADqnlXdGvy0lxSnVamKV8klb/DSJR0jl8sj37KjmsznJrwCciOJyXHUBcA/J5+Dy4qdtc6e5dMy1AThVfb15R2ufMfI+FeUlGcxoNgCsuYmWFNg8JPApZfY0w+VW+oDkdTrONMe5AToQOPyh6YfU90vOYfLOKAOsDp+knUleBuBjgc0MlBf5b3DHNHYGMEb50tsNBEj2J7l0AvDsRXKn1C/uTKMz4I41zfLOJHcjuWeF22Pq85UBfEzyMlWemVPknVEGWB0GVofB5fzaC7l6dZWciC0AziP5GYANK6wF/Y3k93DFDhgBkZWVieAa5EqypaUVALyinPvR9NVdAVxL8q0kUCuzv6ZJFqMlm4o8P+iz9pR3RhlgtQvNILk2yY/hTtU3tzO/Nk38J0g+SbJbOZNOYPQ2XFGDzhFTpp7knQBeh/OjtZT4zo1wxXC/IXlmhH8LXL711xJArRLUDKCfZLRWqcAV3LNWB8o7owywKkpbwhWI7YZcFaD2JoNLxbwGgC9L5BmC0ZICDv+c6dp1GwW3G9pYoXY1AzhamtzWyFWf8WZvEqhVor/aJKNnST4MlxKmWFpE9z7bwfLOKAOsilGtwOO3GLi+EGhRzniSm8AV3NgyBowaAbwPt6s5oz0sZ2ly9wC4FLPupHlQ+xUzV+upVH81AlhfwHhgEX12IFxq4vUrCOAZZYDV4VT9G/JmKfzN7GW45HMNMROvDi6H+HtwYRJsh3fuCuBOAJdExgrFcySAP5vZlHbqt2a4km63pQ3ONbPb4Iq3ZiZgBlhzNA2B8//Mg99mK/sQAU+x9JKZLQPgfIFU+O7fmdnWcPUVJ1VIm/Bg9AGAP5rZ8XCOakTMwt3MbAu4Ig+VBsp5ANxkZr3M7AGBdxqA938fMLNeAG76DeWdUQZYZdoZZufDFRx9JWbyt4dW1UUTbzEzexwlBIsGk/AaAN0BPIVZi068ZGYrBqBWjinmwWjrGDCqBdDfzJaFS5FdyUh16t1fAdDbzM6ugLzP7kB5Z5QBVrvQL2a2B5wzebz6oJID2U+8oXApls+u1IPNrMXMDoVz4n/mzcwEUCs2pqkmDxh1AvAkgJ5mdlk7AFWVZLG1ZPPLHCTvjDqQ5rrjIcFEG2Zmf5aTtj/KP57h/VSj4WrRDWvHyPEvzGytJFADcFBRL+58P3vE9JGngWZ2czu1pRrAKaGfqpLnC9tR3hllGlbHA5ectD3hKgWXYzZUATjLzPoAGFbpiZcwCTvkeWbWHg79OvV5T8mgXQ9Ct4O8M5qNNKwuJWpf9Ujv+I3jUR/z/+YyeNQGz6zNM5ibAZxE8io4R20x5lRXAPcBOFnPyTfxfJurMPcefu4Mt7N5hJl9/xstVqG8byjBfM7ot1I0EkyEJVFaGIDB7VwVVLcTeFSZ2dfBNcvEqO7F8OguQAGAqWY2IYVpBJILmlkqPwrJ+cxscsprwzaPM7Ppc9uA8307O1SgKUXeGWWUUUYZZZRRRhlllFFGGWWUUUYZZZRRRvno/wGU2DmQbZG/IQAAAABJRU5ErkJggg==";
	// ********************************************************************************************************************************
	
	
// GLOBAL CLASSES:
	function Config(){
		var self=this;
		self.forPlaneName=function(planeName){
			if(self[planeName])	return self[planeName];
			return self;
		};
		self.forPlane=function(plane){
			return self.forPlaneName(plane.getName());
		};
		self.forActivePlane=function(/*OPTIONAL*/aotraScreenParam){
			var aotraScreen=aotraScreenParam;
			if(!aotraScreen){
				if(typeof getAotraScreen==="undefined" || !getAotraScreen)	return self;
				aotraScreen=getAotraScreen();
			}
			if(!aotraScreen)	return self;
			var activePlane=aotraScreen.getActivePlane();
			if(!activePlane){
				var activePlaneNameFromAnchor=seekActivePlaneNameFromAnchor();
				if(activePlaneNameFromAnchor && self[activePlaneNameFromAnchor])	return self[activePlaneNameFromAnchor];
				var activePlaneNameFromXMLModel=seekActivePlaneNameFromXMLModel();
				if(activePlaneNameFromXMLModel && self[activePlaneNameFromXMLModel])	return self[activePlaneNameFromXMLModel];
				return self;
			}
			return self.forPlane(activePlane);
		};
		

	}
	
	
// ===============================================INIT SCRIPTS PART =============================================== 

	
function initAllClasses(){
	
	// CLASSES STATIC INITIALIZATION :

	// Must always be before all others classes initialization, because all other classes need static hooks from AotraScreen:
	initAotraScreenClass();
	initLinkClass();
	initBubbleClass();
	initBubbleOpeningMethodsClass();
	initBubbleClosingMethodsClass();
	// Must always be before Plane class initialization, because Plane class need static methods from PlaneMapProvider :
	initPlaneMapProviderClass();
	initPlaneClass();
	initScreenPositionMovingMethodsClass();
	initScreenPositionMovingPoliciesClass();
	initScreenSearchProviderClass();
	initScreenEditionProviderClass();
	initScreenSavingProviderClass();
	initScreenEncryptingProviderClass();
//	initScreenHidingProviderClass(); // This class is OFF, for now.
	initScreenUploadProviderClass();
	initAotraScreenFilesWrappingProviderClass();
	initAotraScreenCarouselListWrappingProviderClass();
	initAotraScreenAdvancedNavigationProviderClass();
	initScreenZoomingProviderClass();
	initScreenZoomingMethodsClass();
	initScreenImporterFromXMLClass();
	initScreenExporterToXMLClass();
	//##//initScreenSpeechSynthesisProviderClass();
	initPlaneWebcamProviderClass();
	initScreenAotraMarkReaderProviderClass();
	initScreenBasicGamingProviderClass();
	initScreenRolePlayingGamingProviderClass();
//	initPlaneAotraMarkReaderForWebcamProviderClass();
//	initPlaneQRCodeReaderForWebcamProviderClass();
	initPlaneBubblesFilteringProviderClass();
	initPlaneBackgroundImageTransitionProviderClass();
	initAotraScreenI18nProviderClass();
	initPlaneParallaxBackgroundsProviderClass();
	initAotraScreenMiniMapProviderClass();
	// Hooks using classes must be below :
	initPlaneVerticalScrollingBubblesPresentingProviderClass();
	// This initBubbleLinksNavigationProviderClass(...) class initer function MUST be AFTER initPlaneVerticalScrollingBubblesPresentingProviderClass(...) class initer function,
	// because of a static function («PlaneVerticalScrollingBubblesPresentingProvider.isScrollInactive(...)») dependency :
	initBubbleLinksNavigationProviderClass();
	initBubbleContentManagingProviderClass();
	initBubbleContentEncryptingProviderClass();
	
	// CLIENT PLUGINS SECTION :
	window.globalPlugins=new Object();// An associative array

		// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
			// Activate or deactivate plugins here :
			// - aotrapetra :
			 initAotrapetraPluginClass();	window.globalPlugins["aotrapetra"]=new AotrapetraPlugin();

	
			// - multiplicity :
			// initMultiplicityPluginClass();	window.globalPlugins["multiplicity"]=new MultiplicityPlugin();
			// // - kekonmanj :
			// initKekonmanjPluginClass();	window.globalPlugins["kekonmanj"]=new KekonmanjPlugin();
			// - transunitity :
			// initTransunityPluginClass();	window.globalPlugins["transunity"]=new TransunityPlugin();
			// - linkitty :
			// initLinkittyPluginClass();	window.globalPlugins["linkitty"]=new LinkittyPlugin();

		// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
			
}

function aotraScreenInit(){

	// AotraScreen pre-initialization ------------------------------------------------------------------------
	
	
	// DOM Dynamic modifications :
	var NUMBER_OF_ICONS_COLUMNS=11;
	var NUMBER_OF_ICONS_ROWS=6;
	var css_stepX=GLOBAL_ICONS_SIZE;
	var css_xs=new Array();
	for(var i=1;i<NUMBER_OF_ICONS_COLUMNS;i++)	css_xs.push(i*css_stepX+ICON_BORDER_THICKNESS);// Caution, offsets will be negative in CSS !
	var css_stepY=css_stepX;// square icons
	var css_ys=new Array();
	for(var i=1;i<NUMBER_OF_ICONS_ROWS;i++)	css_ys.push(i*css_stepY+ICON_BORDER_THICKNESS);// Caution, offsets will be negative in CSS !

	
	// BUILT-IN ELEMENTS GLOBAL CSS STYLE :
	var globalStyle=document.createElement("style");
	globalStyle.innerHTML=""
		// Map provider
		+""+MAP_MARKER_TAG_NAME
		+"{cursor:pointer;}"
		// Media files
		+".contentMedia"
		+"{width:100%;height:auto;}"
		// Bubbles filtering
		+".filtersWindow"
		+"{-moz-border-radius: 10px; border-radius: 10px;border:2px solid white}"
		+".filtersWindow span"
		+"{width:100%;}"
		+".filtersWindow span:hover"
		+"{background:white;}"
		+".filtersWindow label"
		+"{font-family:sans-serif;padding:.3em;font-size:1em;}"
		+".filtersWindow input[type='checkbox']"
		+"{vertical-align: text-bottom;}"
		// Carousel (UNUSED CSS FOR NOW...)
		+".carousel"
		+"{}"
		// Generic UI elements :
		+".link"
		+"{cursor:pointer;color:#444444;background:none;text-decoration:underline;}"
		+".selected"
		+"{	background-image: radial-gradient(#fff 10%, #000 20%,rgba(0,0,0,0.1) 30%) !important;"
		+"	background-size: 5px 5px !important;"
//	+"	border : #fff dotted 4px !important;"
		+"	box-shadow: inset 0.5em 0.5em 0px #888,inset -0.5em -0.5em 0px #888,"
		+"              0.5em 0.5em 0px #888, -0.5em -0.5em 0px #888;"
		+"	opacity : .95 !important;"
		+"}"
		+".buttonsHolder"
		+"{position:fixed!important;}"
		+"button,input[type=button],input[type=submit]"
		+"{color:#FFFFFF!important;background-color:#888888!important;border:none;font-weight:bold;border:none;-moz-border-radius: 10px; border-radius: 10px;}"
		+"button:hover,input[type=button]:hover,input[type=submit]:hover"
		+"{color:#888888!important;background-color:#AAAAAA!important}"// ;border:solid 2px #888888!important; This border for an unexcpetd yet enjoyable effect on buttons hovering !
		+".buttonLight"
		+"{color:#FFFFFF!important;background-color:#AAAACC!important;}"
		+".standardBox"
		+"{background:#83D1DC; -moz-border-radius: 20px; border-radius: 20px; padding:10px;}"
		// A NEW WAY : (cf. in aotrascreen doAfterAotraScreenPopulation(...), at its very end)
		+"#"+MAIN_DIV_ID+" *"
		+"{pointer-events:all;}"
		+"."+BUBBLE_DIV_CLASSNAME+".mobile"
		+"{padding:0;}"
		+"."+BUBBLE_TEXT_CLASSNAME+".mobile"
		+"{font-size:2em!important;}"
		// Icon classes:
		+".icon.dashed{border:dashed 1px #000 !important;}"
		+".icon"
		+"{border:solid "+(ICON_BORDER_THICKNESS)+"px #FFF;background:url('"+IMAGE_SOURCE_ALLICONS+"');width:"+GLOBAL_ICONS_SIZE+"px;height:"+GLOBAL_ICONS_SIZE+"px;background-size:"+(GLOBAL_ICONS_SIZE*(NUMBER_OF_ICONS_COLUMNS+1))+"px;}"
		+".helpBtn{background-position:"+(-ICON_BORDER_THICKNESS)+"px "+(-ICON_BORDER_THICKNESS)+"px;}							.addBubbleBtn{background-position:-"+css_xs[0]+"px 0;}						.saveBtn{background-position:-"+css_xs[1]+"px 0;}								.saveOptionsBtn{background-position:-"+css_xs[2]+"px 0;}					.distantOptionsBtn{background-position:-"+css_xs[3]+"px 0;}				.editSourceBtn{background-position:-"+css_xs[4]+"px 0;}						.viewTextBtn{background-position:-"+css_xs[5]+"px 0;}						.copyModelBtn{background-position:-"+css_xs[6]+"px 0;}					.pasteModelBtn{background-position:-"+css_xs[7]+"px 0;}					.encryptBtn{background-position:-"+css_xs[8]+"px 0;}"
		+".editBubbleBtn{background-position:"+(-ICON_BORDER_THICKNESS)+"px -"+css_ys[0]+"px;}			.deleteBubbleBtn{background-position:-"+css_xs[0]+"px -"+css_ys[0]+"px;}	.moveBubbleBtn{background-position:-"+css_xs[1]+"px -"+css_ys[0]+"px;}			.pullBubbleBtn{background-position:-"+css_xs[2]+"px -"+css_ys[0]+"px;}		.pushBubbleBtn{background-position:-"+css_xs[3]+"px -"+css_ys[0]+"px;}	.makeSubBubbleBtn{background-position:-"+css_xs[4]+"px -"+css_ys[0]+"px;}	.unmakeSubBubbleBtn{background-position:-"+css_xs[5]+"px -"+css_ys[0]+"px;}	.linkBubbleBtn{background-position:-"+css_xs[6]+"px -"+css_ys[0]+"px;}	.scaleMinusBtn{background-position:-"+css_xs[7]+"px -"+css_ys[0]+"px;}	.scalePlusBtn{background-position:-"+css_xs[8]+"px -"+css_ys[0]+"px;}	.scaleBtn{background-position:-"+css_xs[9]+"px -"+css_ys[0]+"px;}"
		+".lastBubbleInsertBtn{background-position:"+(-ICON_BORDER_THICKNESS)+"px -"+css_ys[1]+"px;}	.carouselInsertBtn{background-position:-"+css_xs[0]+"px -"+css_ys[1]+"px;}	.mapMarkerInsertBtn{background-position:-"+css_xs[1]+"px -"+css_ys[1]+"px;}		.linkInsertBtn{background-position:-"+css_xs[2]+"px -"+css_ys[1]+"px;}		.imageInsertBtn{background-position:-"+css_xs[3]+"px -"+css_ys[1]+"px;}	.videoInsertBtn{background-position:-"+css_xs[4]+"px -"+css_ys[1]+"px;}		.soundInsertBtn{background-position:-"+css_xs[5]+"px -"+css_ys[1]+"px;}		.colorBtn{background-position:-"+css_xs[6]+"px -"+css_ys[1]+"px;}		.visibleBtn{background-position:-"+css_xs[7]+"px -"+css_ys[1]+"px;}		.invisibleBtn{background-position:-"+css_xs[8]+"px -"+css_ys[1]+"px;} 	.randomBubbleInsertBtn{background-position:-"+css_xs[9]+"px -"+css_ys[1]+"px;}"
		+".homeBubbleBtn{background-position:"+(-ICON_BORDER_THICKNESS)+"px -"+css_ys[2]+"px;}			.lastBubbleBtn{background-position:-"+css_xs[0]+"px -"+css_ys[2]+"px;}		.lastVisitedBubbleBtn{background-position:-"+css_xs[1]+"px -"+css_ys[2]+"px;}	.previousBubbleBtn{background-position:-"+css_xs[2]+"px -"+css_ys[2]+"px;}	.nextBubbleBtn{background-position:-"+css_xs[3]+"px -"+css_ys[2]+"px;}	.searchBtn{background-position:-"+css_xs[4]+"px -"+css_ys[2]+"px;}			.zoomOutBtn{background-position:-"+css_xs[5]+"px -"+css_ys[2]+"px;}			.zoomInBtn{background-position:-"+css_xs[6]+"px -"+css_ys[2]+"px;}		.voidBtn{background-position:-"+css_xs[7]+"px -"+css_ys[2]+"px;}"
		+".closeBtn{background-position:"+(-ICON_BORDER_THICKNESS)+"px -"+css_ys[3]+"px;}				.moveBtn{background-position:-"+css_xs[0]+"px -"+css_ys[3]+"px;}			.editBtn{background-position:-"+css_xs[1]+"px -"+css_ys[3]+"px;}				.minusBtn{background-position:-"+css_xs[2]+"px -"+css_ys[3]+"px;}			.plusBtn{background-position:-"+css_xs[3]+"px -"+css_ys[3]+"px;}		.undoBtn{background-position:-"+css_xs[4]+"px -"+css_ys[3]+"px;}			.redoBtn{background-position:-"+css_xs[5]+"px -"+css_ys[3]+"px;}			.speechBtn{background-position:-"+css_xs[6]+"px -"+css_ys[3]+"px;}		.miniMapBtn{background-position:-"+css_xs[7]+"px -"+css_ys[3]+"px;}"
		+".stopBtn{background-position:"+(-ICON_BORDER_THICKNESS)+"px -"+css_ys[4]+"px;}				.pauseBtn{background-position:-"+css_xs[0]+"px -"+css_ys[4]+"px;}			.backBtn{background-position:-"+css_xs[1]+"px -"+css_ys[4]+"px;}				.forthBtn{background-position:-"+css_xs[2]+"px -"+css_ys[4]+"px;}			.backwardsBtn{background-position:-"+css_xs[3]+"px -"+css_ys[4]+"px;}	.forwardBtn{background-position:-"+css_xs[4]+"px -"+css_ys[4]+"px;}			.playBtn{background-position:-"+css_xs[5]+"px -"+css_ys[4]+"px;}			.playReverseBtn{background-position:-"+css_xs[6]+"px -"+css_ys[4]+"px;}"
		// CSS for blink :
//		+"@keyframes blinker{ from{opacity: 1.0;} to{opacity: 0.0;} }"
//		+".blink{"
//		+"  text-decoration:blink;"
//		+"  -webkit-animation-name:blinker; -webkit-animation-duration:0.6s; -webkit-animation-iteration-count:infinite; -webkit-animation-timing-function:ease-in-out; -webkit-animation-direction:alternate;"
//		+"}"
		+".blink{ animation:blinkingText 0.8s infinite; }"
		+"@keyframes blinkingText{"
		+"    0%{     color: #000;    }"
		+"    49%{    color: transparent; }"
		+"    50%{    color: transparent; }"
		+"    99%{    color:transparent;  }"
		+"    100%{   color: #000;    }"
		+"}"
		;
	appendAsFirstChildOf(globalStyle,document.head);

	
	// TO PREVENT ZOOMING ON MOBILE DEVICES:
	var meta1=document.createElement("meta");
	meta1.name="viewport";
	meta1.content="width=device-width, width=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no";
	appendAsFirstChildOf(meta1,document.head);

	var meta2=document.createElement("meta");
	meta2.name="HandheldFriendly";
	meta2.content="true";
	appendAsFirstChildOf(meta2,document.head);

	
	// NOT WORKING :
//	var script1=document.createElement("script");
//	script1.type="text/javascript";
//	script1.src="https://maps.google.com/maps/api/js?sensor=false&language=fr&key=AIzaSyB_8Lfntusgu8fI4-nvVQMYQ43WOp47-c8";
//	appendAsFirstChildOf(script1,document.head);

	
	// COMPATIBILITY TWEAK : 
	// IE special treatments :
	if(isIE()){
		var script2=document.createElement("script");
		script2.type="text/javascript";
		script2.src="https://html5shiv.googlecode.com/svn/trunk/html5.js";
		appendAsFirstChildOf(script2,document.head);
		
		// Here's a mean message for MS Internet Explorer users :
		alert(MEAN_IE_MESSAGE_LONG);
		// If we really want to be mean, we will do this : 	
		// window.location='https://www.mozilla.org/en-US/firefox/new';	return;
	}

	
	// DEFAULT BEHAVIOR PROCESSING :
	if(!window.config){
		window.config=new Config();
	    config.aotraScreen=new Config();
	    config.bubble = new Config();
	    config.link = new Config();
	}
	
	// We check if parameter in URL allows edition (it will override default behavior):
	var editableParam=getURLParameter("editable");
	window.forceIsEditable = config.aotraScreen.isEditable; // you can set aotraScreen edition capabilities in configuration.
	if(editableParam){
		window.forceIsEditable=(editableParam==="true");// if present parameter overrides configuration
	}
	
	// We check if parameter in URL asks for another language than default language :
	var languageParam=getURLParameter("lang");
	window.language = config.aotraScreen.language; // you can set aotraScreen language in configuration
	if(!nothing(languageParam)){
		window.language=languageParam;// if present parameter overrides configuration
	}

	// CLASSES STATIC INITIALIZATION :
	initAllClasses();
	
	// AotraScreen initialization ------------------------------------------------------------------------
	var bruteXMLCode=document.getElementById(MODEL_ID).textContent.trim().replace(/\n/gim,"");
	// Notice : Since we are in XHTML, there is theoretically no need to remove all eventual CDATA tags
	// <[!CDATA[...]]>
	var XMLCode;
	var XMLCodeLength=bruteXMLCode.length;
	
	if(bruteXMLCode.substr(0,9)==="<[!CDATA["
	    && bruteXMLCode.substr(XMLCodeLength-3,3)==="]]>"  ){
	    XMLCode=bruteXMLCode.substring(9,XMLCodeLength-3);
	}else if(bruteXMLCode.substr(0,4)==="<!--"
	    && bruteXMLCode.substr(XMLCodeLength-3,3)==="-->"  ){
	    XMLCode=bruteXMLCode.substring(4,XMLCodeLength-3);
	}else{
	    XMLCode=bruteXMLCode;
	}
	
	
	var xmlText=normalizeXMLContentsTagsInBubbles(XMLCode);
	var xmlDoc = parseXMLString(xmlText);
	var eRoot=xmlDoc.documentElement;
	

	var aotraScreenLocal;
	
	var defaultPlane=null;
	
	var isEncrypted= eRoot.nodeName==="encryptedAotraScreenContent";
	var isErroneous= !empty(eRoot.getElementsByTagName("parsererror"));
					 
	if(!eRoot || isErroneous || isEncrypted){

		// Default content if no XML model has been set in the page :
		aotraScreenLocal=new AotraScreen(
				config.aotraScreen.title,config.aotraScreen.background,config.aotraScreen.borderStyle,config.aotraScreen.moveAnimation,config.aotraScreen.navigationMode
				,false,config.aotraScreen.zoomingAnimation,"defaultPlaneName"
				,true,"",config.aotraScreen.speed,false
				,"","",null
				,false,isEncrypted
				);

		defaultPlane=getDefaultPlane(aotraScreenLocal);
		aotraScreenLocal.addPlane(defaultPlane);
		// By default, we'll choose the first (and only) default plane as active plane :
		aotraScreenLocal.setActivePlane();
		
		
		var defaultBubble=null;
		var DEFAULT_BUBBLE_NAME="defaultBubbleName";
		
		
		if(eRoot){
			
			// If we have a parse error :
			if(isErroneous){
				
				
				defaultBubble=getDefaultBubble("#c4b99b"
						,"<br /><br />XML PARSE ERROR :<div> <br /><p style='font-style:italic'>Could not load XML Bubble model...</p> <br /><p> Error : </p><p style='font-weight:bold'>"+eRoot.getElementsByTagName("parsererror")[0].textContent+"</p></div> "
						,aotraScreenLocal);
		
			}else if(isEncrypted){
			// If we have an encrypted content :
				
				
				defaultBubble=getDefaultBubble("#FFF751"
						,"<br /><br />THIS PAGE IS ENCRYPTED :<div> <br />"
						+"<p style='font-style:italic'>Please enter view password :</p>"
						+"<p><input id='"+TEXTBOX_DECRYPT_PASSWORD_ID+"' type='password' /></p>"
						+"<p><button onclick='javascript:getAotraScreen().encryptingProvider.sendDecryptRequest()'>OK</button></p>"
						+"</div> "
						,aotraScreenLocal);
					
				AotraScreenEncryptingProvider.initPasswordField();
	
			}else{
				
				defaultBubble=getDefaultBubble("#c4b99b"
						,"<br /><br />UNKNOWN MODEL ERROR :<div> <br /><p style='font-style:italic'>Could not load XML Bubble model...</p></div> "
						,aotraScreenLocal);
			}
			
		}else{
			
			defaultBubble=getDefaultBubble("#c4b99b"
					,"<br /><br />EMPTY MODEL ERROR :<div> <br /><p style='font-style:italic'>Could not load XML Bubble model...</p></div> "
					,aotraScreenLocal);
			
		}

		if(defaultBubble)	defaultPlane.addBubbleToPlane(defaultBubble);
		
		
	}else{
		
		// The «most normal» case :
		
		var aotraScreenFactoryXML = new AotraScreenImporterFromXML();
		aotraScreenLocal=aotraScreenFactoryXML.getAotraScreenFromXML(eRoot,window.forceIsEditable);
		
	}
	
		
	// Treatments to do once plane is fully populated,
	// (and since no XML importer did the job) :
	if(defaultBubble){
		defaultPlane.doAfterPlaneReady();
	}
	
	window.waitIndicator.init();
	
	return aotraScreenLocal;
}

function afterAotraScreenInit(aotraScreenLocal){
	
	// AotraScreen post-initialization ------------------------------------------------------------------------

	var activePlane=aotraScreenLocal.getActivePlane();
	
	// We call all processes that need DOM fully loaded :
	
	// AotraScreen title :
	var titleTag=document.getElementsByTagName("title")[0];
	if(!titleTag){
		titleTag=document.createElement("title");
		document.head.appendChild(titleTag);
	}
	
	if(aotraScreenLocal.title && !nothing(aotraScreenLocal.title,true)){
		titleTag.innerText=aotraScreenLocal.title;
	}else{
		titleTag.innerText=config.aotraScreen.forPlane(activePlane).title;
	}

	// We place the aotraScreen at the right place first :
	var initX=aotraScreenLocal.width/2;
	var initY=aotraScreenLocal.height/2;

	// We place aotraScreen at the bubble which name is passed as anchor in the URL :
	var URLAnchor=getURLAnchor();
	if(!nothing(URLAnchor)){
		var anchoredBubbleName=URLAnchor;
		var splittedURLAnchor=URLAnchor.split(SEPARATOR_CHAR);
		// Format for anchor : #planeName,bubbleName(OPTIONAL)
		// We determine the bubble to go to :
		if(splittedURLAnchor.length>=2){
			anchoredBubbleName=splittedURLAnchor[1];
		}
		var anchoredBubble=activePlane.getBubbleByName(anchoredBubbleName);
		if(anchoredBubble!=null){
			var xy=anchoredBubble.getDisplayXY(CENTER);
			initX=xy["x"];
			initY=xy["y"];
		}
	}
	
	aotraScreenLocal.moveToMousePosition(initX,initY);

	
	// We adjust page attributes to aotraScreen :

	// We adjust page background :
	var backgroundHolderTag=document.getElementById(AOTRA_SCREEN_DIV_ID)?document.getElementById(AOTRA_SCREEN_DIV_ID):document.body;
	
	if(activePlane 
			&& !nothing(activePlane.simpleBackground)
			&& activePlane.simpleBackground!==WEBCAM
			&& !contains(activePlane.simpleBackground,MAP)
			){
		// Static plane background initialization :
		var simpleBackground=activePlane.simpleBackground;
		jQuery(backgroundHolderTag).css("background","url('"+simpleBackground+"') no-repeat");
		jQuery(backgroundHolderTag).css("background-size","100%");

	}else if(aotraScreenLocal.background && !nothing(aotraScreenLocal.background,true) ){
		// At background initialization, static plane background has priority over aotraScreen background :
		if(getGenericFileType(aotraScreenLocal.background)==="image"){
			// Case image as background :
			jQuery(backgroundHolderTag).css("background","url('"+aotraScreenLocal.background+"') no-repeat");
			jQuery(backgroundHolderTag).css("background-size","100%");
		}else{
			// Case not an image as background :
			backgroundHolderTag.setStyle("background:"+aotraScreenLocal.background);
		}
	}
	
	// We adjust page border style :
	if(aotraScreenLocal.borderStyle && !nothing(aotraScreenLocal.borderStyle,true)){
		// CAUTION : div.style.cssAttr....=...; DOES NOT WORK (because we use Prototype library) ! USE .setStyle(...); INSTEAD !
		aotraScreenLocal.mainDiv.setStyle("border:"+aotraScreenLocal.borderStyle);
		//aotraScreenLocal.mainDiv.style.border=aotraScreenLocal.borderStyle;
	}
	
	
	// AotraScreen is singleton in page :
	window.getAotraScreen=function (){
		return aotraScreenLocal;
	};
	window.setScreen=function(s){
		aotraScreenLocal=s;
	};

	
	// Eventual Help initialization :
	initHelp(MAX_Z_INDEX_OVERLAY);

	
	// We add a CSS clue to tell that this aotraScreen is in editable mode :
	if(aotraScreenLocal.isCalculatedEditable())
		jQuery(aotraScreenLocal.getMainDiv()).addClass("editable");
	else
		jQuery(aotraScreenLocal.getMainDiv()).addClass("notEditable");
	
	
	
	// Plugins overriding management :
	// -----------------------------------------------------------------------
	// Nothing of the above is done, if aotraScreen is overridden
	if(aotraScreenLocal.isOverridden)	return aotraScreenLocal;
	// -----------------------------------------------------------------------
	
	// We calculate initial aotraScreenLocal model signature :
	aotraScreenLocal.calculateModelSignature();
	
	// Eventual visibility layers treatment for bubble : (once all bubbles have been added)
	var plane=aotraScreenLocal.getActivePlane();
	if(plane.visibilityLayersProvider){
		plane.visibilityLayersProvider.initVisibilityLayersProviderUI(aotraScreenLocal);
	}
	
	
	// Wait div (only for mobile devices, actually) :
	var divWait=document.getElementById("divWait");
	if(divWait)	divWait.parentNode.removeChild(divWait);

	
	return aotraScreenLocal;
}


// **************************************************************************************************************************

// Main init method :
function aotraScreenJSFooterInit(){

	// AotraScreen pre-DOM init treatments ------------------------------------------------------------------------

	// AotraScreen initialization :
	var aotraScreenDiv=document.getElementById(AOTRA_SCREEN_DIV_ID)?document.getElementById(AOTRA_SCREEN_DIV_ID):document.body;
	aotraScreenDiv.setStyle("position:fixed;top:0;left:0;z-index:"+(MIN_Z_INDEX)+"; overflow:hidden;");

	// We clear potential HTML in main div :
	aotraScreenDiv.innerHTML="";
	
	
	// Wait div (only for mobile devices, actually) :
	var divWait=document.getElementById("divWait");
	// We check in case there is no static divWait already present statically in the html page ! 
	if(!divWait){
		divWait=document.createElement("div");
		divWait.id="divWait";
		divWait.setStyle("width:100%;height:100%;display:flex;justify-content:center;align-items:center;z-index:2;font-family:helvetica;font-weight:bold;font-size:10em;color:gray");
	//	DOESN'T WORK :	divWait.innerHTML="<span style='text-decoration:blink'>Loading...</span>";
		divWait.innerHTML="<span>Loading<span class='blink'>...</span></span>";
		aotraScreenDiv.appendChild(divWait);
	}
	
	
	
	// Treatments done only once by page loading :
	var mainDivElement=document.createElement("div");
	aotraScreenDiv.appendChild(mainDivElement);
	mainDivElement.id=MAIN_DIV_ID;
	mainDivElement.setStyle("position:fixed;top:0;left:0;z-index:"+(MIN_Z_INDEX+20)+"; overflow:hidden;");
	
	var mainCanvasElement=document.createElement("canvas");
	aotraScreenDiv.appendChild(mainCanvasElement);
	mainCanvasElement.id=MAIN_CANVAS_ID;
	mainCanvasElement.setStyle("position:fixed;top:0;left:0;z-index:"+(MIN_Z_INDEX+10)+";opacity:0.99;");// opacity forced to 0.99 to correct a stupid Chrome bug...

	// Post-DOM loading treatments :
	// Start of the main method :
	jQuery(document).ready(function(){
		
		try{
			// Main method :
			// (AotraScreen is singleton in page)
			var aotraScreenLocal=aotraScreenInit();
			
			// All treatments after initialization must be in the following method :
			afterAotraScreenInit(aotraScreenLocal);

			// DOESN'T WORK (YET) (cf. *_jquery.drags.js) :
//			// Draggable elements initialization :
//			initDrags(jQuery);
			
			
			// Unit tests (to ensure minimal regressions in code) :
			if(config.runTests)
				aotest.run();
			
		}catch(e1){ alert(e1);log(e1); }
	});
	
	
	// This resize must be done at this precise moment :
	
	// (since center calculus are done after this, but before init)
	// Width and height html main components adjusting :
	var width=getWindowSize("width");
	var height=getWindowSize("height");
	// Set default screen containers size :
	// For weird reasons, we have to invert width and height on mobile devices :
	if(isDeviceMobile())		setHTMLDisplaySize(height,width);
	else										setHTMLDisplaySize(width,height);


	// Mobile devices management
	var supportsOrientationChange = "onorientationchange" in window;
	var orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";
	window.addEventListener(orientationEvent, function() {
		var aotraScreen=getAotraScreen();
		
		if(!isDeviceMobile())	return;
		
		// View mode hack to force desktop view on mobile devices :
		let viewMode = getStringFromStorage("view-mode",true);
		if(viewMode == "desktop"){
		    viewport.setAttribute('content', 'width=1024');
		}else if (viewMode == "mobile"){
		    viewport.setAttribute('content', 'width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no');
		}
		
		
		// We re-calculate :
		var width=getWindowSize("width");
		var height=getWindowSize("height");
		setHTMLDisplaySize(width,height);
		// OLD : NO : For weird and unknown reasons, we have to invert width and height on mobile devices :
		//setHTMLDisplaySize(height,width);
		
		aotraScreen.refreshSizeAfterCanvasSize();
		aotraScreen.drawAndPlace();
		
	}, false);
	
	
	// Stay on page protection if there are modifications :
	// TODO : FIXME : make it compatible with Firefox
//	document.body.onbeforeunload=(function(event){
	window.onbeforeunload=(function(event){
		var aotraScreen=getAotraScreen();
		
		if(forceIsEditable===false && aotraScreen.isEditable===false)	return null;
		if(!empty(aotraScreen.getBubblesInActivePlaneByContentContaining("PARSE ERROR"))) 
			return XML_ERRORS_MESSAGE;
		if(aotraScreen.isModelChanged()==false)	return null;
		return CONFIRM_MESSAGE;
	});
	
	
}


// This function must be executed AFTER all the document frame is loaded (ie. in footer javascript) :
// This method must not need an initialized aotraScreen :
/*private*/function setHTMLDisplaySize(width,height){
	
	// ADJUSTMENT : We have to reduce a bit... (to avoid sensible mouse-related coordinates problems) :
	width-=1;
	height-=1;
	//	width-=20;
	//	height-=20;
	
	var mainCanvas=document.getElementById(MAIN_CANVAS_ID);
	
//	// COMPATIBILITY TWEAK :
//	// Mobile Chrome treatments :
//	if(isDeviceMobile() && isChrome()){
//		var CHROME_TWEAK_MARGIN=70;
//		var temp=width;
//		width=height;
//		height=temp;
//		if(width<height)	width+=CHROME_TWEAK_MARGIN;
//		else 				height+=CHROME_TWEAK_MARGIN;
//	}
	
	mainCanvas.width=width;
	mainCanvas.height=height;
	
	var backgroundHolderTag=document.getElementById(AOTRA_SCREEN_DIV_ID);
	// We only adjust if holder element (root) is not directly the body (which I do not recommend, but you do whatever you want...) :
	if(backgroundHolderTag){
		//backgroundHolderTag.setStyle("width:"+(width)+"px;height:"+(height)+"px;");
		backgroundHolderTag.setStyle("width:100%;height:100%;");
	}

	//document.getElementById(MAIN_DIV_ID).setStyle("width:"+(width)+"px;height:"+(height)+"px;");
	document.getElementById(MAIN_DIV_ID).setStyle("width:100%;height:100%;");

	// Eventual map container dimension adjustement :
	var mapContainer=document.getElementById(DIV_MAP_CONTAINER_ID);
	if(mapContainer!=null){
		//mapContainer.setStyle("width:"+(width)+"px;height:"+(height)+"px;");
		mapContainer.setStyle("width:100%;height:100%;");
	}

}




// ============================ Utility methods ============================


function getDefaultPlane(aotraScreenParam){

//Parameters : (name,parallaxBackground,simpleBackground
//	 ,visibilityLayersConfig,visibilityLayersJunctionLogic,fractionOfImageUsed
//	 ,linksButtonsDisplayStrategy
//	 ,configuration
//	 ,hooksParameters
//	 ,aotraScreen)

	return new Plane("defaultPlaneName","",""
				,"",null,"noParallax"
				,""
				,""
				,null
				,aotraScreenParam
				);
	
};

function getDefaultBubble(colorParam,contentParam,aotraScreenParam){

//Parameters : (name,x,y,w,h
//	,mold
//	,contents
//	,fontSize,openableClosable,color
//	,openCloseAnimation,borderStyle,openedOnCreate
//	,fixedStrategy,isHomeBubble,isExcludingText,hooksParameters
//	,/*OPTIONAL*/zIndex
//	,/*OPTIONAL*/serverCode
//	,/*OPTIONAL*/visibilityLayers
//	,/*OPTIONAL*/parallaxBackground
//	,/*OPTIONAL*/aotraScreen)
	
	return new Bubble(
		 DEFAULT_BUBBLE_NAME,0,0,500,500
		,"rounded"
		,contentParam
		,config.bubble.fontSize,false,colorParam
		,"linear","ridge 5px #8e8e8e","true"
		,"false",true,false,null
		,null
		,""
		,""
		,""
		,aotraScreenParam);

}
